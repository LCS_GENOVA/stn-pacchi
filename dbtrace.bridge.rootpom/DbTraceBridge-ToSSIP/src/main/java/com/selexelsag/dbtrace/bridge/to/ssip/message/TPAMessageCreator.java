/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.ssip.message;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author arodriguez
 *
 */
public class TPAMessageCreator extends SSIPAbstractMessageCreator implements Serializable {

	
	/**
	 * @param textMessage
	 */
	public TPAMessageCreator(String textMessage) {
		super(textMessage);
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.SSIPAbstractMessageCreator#getSentDate()
	 */
	@Override
	protected String getSentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		return sdf.format(new Date());
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.SSIPAbstractMessageCreator#getSourceOfficeCode()
	 */
	@Override
	protected String getSourceOfficeCode() {
		return DESTINATION_DBTRACE;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.SSIPAbstractMessageCreator#getDestinationOfficeCode()
	 */
	@Override
	protected String getDestinationOfficeCode() {
		return "SSIP";
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.SSIPAbstractMessageCreator#getMessageType()
	 */
	@Override
	protected String getMessageType() {
		return "TPA";
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.SSIPAbstractMessageCreator#isTestMessage()
	 */
	@Override
	protected boolean isTestMessage() {
		return false;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.SSIPAbstractMessageCreator#getContainerCode()
	 */
	@Override
	protected String getContainerCode() {
		return null;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.SSIPAbstractMessageCreator#isCompressed()
	 */
	@Override
	protected boolean isCompressed() {
		return false;
	}

}
