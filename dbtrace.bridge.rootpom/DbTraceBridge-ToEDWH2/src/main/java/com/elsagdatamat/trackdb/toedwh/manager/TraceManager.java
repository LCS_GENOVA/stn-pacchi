package com.elsagdatamat.trackdb.toedwh.manager;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elsagdatamat.trackdb.toedwh.dao.IEdwhTracesReadyToSendVWDAO;
import com.elsagdatamat.trackdb.toedwh.dao.ITraceDAO;
import com.elsagdatamat.trackdb.toedwh.dao.ITraceDetailDAO;
import com.elsagdatamat.trackdb.toedwh.entities.EdwhTracesReadyToSendVW;
import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSend;
import com.elsagdatamat.trackdb.toedwh.vo.TraceVO;

/*
 * @Author AM,  5/2010
 * @Author AM, 11/2010
 * @Author RL, 01/2011
 */
@Service
public class TraceManager implements ITraceManager {

	private static final Log log = LogFactory.getLog(TraceManager.class);

	@Autowired
	ITraceDAO traceDAO;
	
	@Autowired
	IEdwhTracesReadyToSendVWDAO edwhTraceDAO;

	@Autowired
	ITraceDetailDAO traceDetailDAO;

	@Override
	public List<TraceVO> find(Long id, Long maxRows, String traceType) {

		// check if there is anything to elaborate in the interval
		String query = "select t1 from Trace t1 where t1.id in (select min(t.id) from Trace t where  t.id>" + id + " and t.tracedEntity='"
				+ traceType + "' )";
		List<TraceVO> foundTraces = traceDAO.executeTraceQuery(query);
		if (foundTraces.size() == 0)
			return foundTraces;
		else {
			id = foundTraces.get(0).getIdTrace() - 1;
		}
		Long maxId = id + maxRows;

		query = "select t,d from Trace t left join t.details d where  t.id in (select t1.id from Trace t1 where t1.id>" + id
				+ " and t1.id<=" + maxId + " and t1.tracedEntity='" + traceType + "' )";
		return traceDAO.executeQuery(query);

	}

	@Override
	public int deleteTraces(Integer timeHorizon) {
		return traceDAO.deleteTraces(timeHorizon);
	}

	@Override
	public int bulkDeleteTraces(Integer timeHorizon, String traceType) {
		return traceDAO.bulkDeleteTraces(timeHorizon, traceType);
	}

	@Override
	public List<TraceVO> findByListOfType(Long id, List<TraceTypesToSend> findByDestinationTracedEntityChannel, Long maxRows) {
		StringBuffer conditions = new StringBuffer();
		boolean first = true;
		conditions.append("(");
		for (TraceTypesToSend traceTypesToSend : findByDestinationTracedEntityChannel) {
			if (!first) {
				conditions.append(" or ");
			}
			conditions.append("(");
			conditions.append("t.tracedEntity='" + traceTypesToSend.getTracedEntity() + "'");
			// if no channel is specified, just consider all channels
			if (traceTypesToSend.getChannel() != null) {
				conditions.append("and t.channel='" + traceTypesToSend.getChannel() + "'");
			}
			conditions.append("and t.whatHappened='" + traceTypesToSend.getWhatHappened() + "'");
			conditions.append(")");
			first = false;
		}
		conditions.append(")");

		// check if there is anything to elaborate in the interval
		//String query = "select t1 from Trace t1 where t1.id in (select min(t.id) from Trace t where  t.id>" + id + " and "
		//		+ conditions.toString() + "" + " and not exists ( select st.traceId from SentTrace st where st.id = t.id)))";
		
		List<EdwhTracesReadyToSendVW> edwhFoundTraces = edwhTraceDAO.executeTraceQuery(id);
		List<TraceVO> foundTraces = new ArrayList<TraceVO>();
		if (edwhFoundTraces.size() == 0) {
			return foundTraces;
		} else {
			id = edwhFoundTraces.get(0).getId() - 1;
		}
		Long maxId = id + maxRows;

		String query = "select t,d from Trace t left join t.details d where  t.id>" + id + "and t.id<=" + maxId + " and " + conditions.toString()
				+ " and not exists ( select st.traceId from SentTrace st where st.id = t.id))";
		foundTraces = traceDAO.executeQuery(query);
		return foundTraces;
	}

}
