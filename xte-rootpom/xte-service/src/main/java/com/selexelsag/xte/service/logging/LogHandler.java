package com.selexelsag.xte.service.logging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.xml.DOMConfigurator;

import com.selexelsag.xte.service.exceptions.ServiceException;

public class LogHandler implements ILogHandler 
{
	private static Log log = LogFactory.getLog(LogHandler.class); 
	
	private String logFile; 
	public String getLogFile() {return logFile;}
	public void setLogFile(String logFile) {this.logFile = logFile;}

	@Override
	public void init() throws ServiceException 
	{
		try
		{
			if (log.isDebugEnabled())
				log.debug("Loading log4j configuration");
			DOMConfigurator.configure(logFile);
			if (log.isDebugEnabled())
				log.debug("Loaded: " + logFile);
		}
		catch (Exception e)
		{
			log.error("Error loading log4j configuration", e);
			throw new ServiceException(e, "Error loading log4j configuration", true);
		}
	}
}
