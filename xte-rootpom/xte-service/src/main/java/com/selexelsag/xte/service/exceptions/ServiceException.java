package com.selexelsag.xte.service.exceptions;

/**
 * Classe di eccezione per i servizi generici
 *
 */
public class ServiceException extends BaseException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4417590156799491263L;

	public ServiceException(Exception exception,String message, final boolean copyStack)
	{
		super(exception, message, copyStack);
	}
}
