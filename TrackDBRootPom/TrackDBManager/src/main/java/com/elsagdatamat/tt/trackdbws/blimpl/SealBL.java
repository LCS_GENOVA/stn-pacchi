package com.elsagdatamat.tt.trackdbws.blimpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.framework.search.Search;
import com.elsagdatamat.tt.trackdbws.beans.Seal;
import com.elsagdatamat.tt.trackdbws.bl.ISealBL;
import com.elsagdatamat.tt.trackdbws.dao.ISealDao;

public class SealBL implements ISealBL {

	@Autowired
	private ISealDao sealDao;
	
	public ISealDao getSealDao() {
		return sealDao;
	}
	
	public void setSealDao(ISealDao sealDao) {
		this.sealDao = sealDao;
	}



	/* (non-Javadoc)
	 * @see com.elsagdatamat.tt.trackdbws.blimpl.ISealBL#getSealByNumberDate(java.lang.Long, java.util.Date)
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public Seal getSealByNumberDate(String sealNumber, Date dateFrom)
	{
		Search s=new Search();
		s.addFilterEqual("number", sealNumber);
		s.addFilterGreaterOrEqual("lastUse", dateFrom);
		s.addSort("lastUse", true);
		List<Seal> searchRes = sealDao.search(s);
		if (searchRes==null || searchRes.size()==0)
			return null;
		return searchRes.get(0);
		
	}
	
	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public void saveSeal(String sealNumber)
	{
		Search s=new Search();
		s.addFilterEqual("number", sealNumber);
		s.addSort("lastUse", true);
		List<Seal> searchRes = sealDao.search(s);
		if (searchRes==null || searchRes.size()==0)
		{
			Seal seal=new Seal();
			seal.setNumber(sealNumber);
			seal.setLastUse(new Date());
			sealDao.insert(seal);			
		}
		else
		{
			searchRes.get(0).setLastUse(new Date());
			sealDao.update(searchRes.get(0));
		}
		sealDao.flush();
		
	}
}
