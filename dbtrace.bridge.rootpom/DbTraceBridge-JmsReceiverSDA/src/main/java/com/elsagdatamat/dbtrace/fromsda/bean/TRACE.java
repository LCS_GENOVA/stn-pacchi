package com.elsagdatamat.dbtrace.fromsda.bean;


import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
"TRACE"
})
public class TRACE {

/**
* 
* (Required)
* 
*/
@JsonProperty("TRACE")
private TRACE_ tRACE;

/**
* 
* (Required)
* 
*/
@JsonProperty("TRACE")
public TRACE_ getTRACE() {
return tRACE;
 }

/**
* 
* (Required)
* 
*/
@JsonProperty("TRACE")
public void setTRACE(TRACE_ tRACE) {
this.tRACE = tRACE;
 }

}