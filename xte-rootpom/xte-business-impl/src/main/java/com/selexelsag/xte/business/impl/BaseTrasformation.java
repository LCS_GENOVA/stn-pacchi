package com.selexelsag.xte.business.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.xte.business.exceptions.BusinessMessageProcessorException;
import com.selexelsag.xte.business.exceptions.BusinessTrasformationException;
import com.selexelsag.xte.business.tracking.IMessageProcessorFactory;
import com.selexelsag.xte.business.tracking.ITrasformation;
import com.selexelsag.xte.business.utility.XmlUtils;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.model.flows.FlowResponse;
import com.selexelsag.xte.model.message_processor.MessageProcessor;
import com.selexelsag.xte.service.cache.LRUCacheManager;
import com.selexelsag.xte.service.context.SpringApplicationContext;
import com.selexelsag.xte.service.resource.IMessageManager;

public class BaseTrasformation implements ITrasformation {

	private static Logger logger = Logger.getLogger(BaseTrasformation.class);
	
	@Autowired
	private LRUCacheManager transformationCache;
	
	@Autowired
	private IMessageProcessorFactory processorFactory;
		
	@Override
	public StringBuffer requestTrasformation(FlowParams flowParams, StringBuilder data
			, HashMap<String, String> parameter) throws BusinessTrasformationException{

		File fileXsl = null;
		StringBuffer newXml = null;
		try{

			fileXsl =(File)transformationCache.getItem(flowParams.getRequestTrasformationModel());
			newXml =	XmlUtils.xslTransform(data.toString(), FileUtils.readFileToString(fileXsl),parameter);

		}catch(BusinessTrasformationException ex){
			
			throw new BusinessTrasformationException("Problem to transform file", ex);

		} catch (IOException ex) {

			throw new BusinessTrasformationException("problem to read file", ex);
			
		} catch (Exception ex) {

			throw new BusinessTrasformationException("problme to get file from transformation cache", ex);
			
		} 

		return newXml;
	}
	
	@Override
	public StringBuffer responseTrasformation(FlowParams flowParams, StringBuffer data) {
		// TODO Auto-generated method stub
		System.out.println("TODO: BaseTrasformation.responseTrasformation(" + flowParams + ", " + data);
		return null;
	}

	@Override
	public void buildResponse(FlowParams flowParams, StringBuilder data, FlowResponse flowResponse) throws BusinessTrasformationException{
		String unescapedRespose = null;
		logger.debug("Escaped response:\n" + data.toString());
		unescapedRespose = StringEscapeUtils.unescapeXml(data.toString());
		logger.debug("unescaped response:\n" + unescapedRespose);
		// XPathFactory factory = XPathFactory.newInstance();
		String result = "";
		String desc = "";
		String code = "";
		String reiteration = "";
		
		boolean parsingOk = false;
		try {
			result = XmlUtils.extractValue(flowParams.getQueriesCallResult().getCallResultXpath(), unescapedRespose);
			desc = XmlUtils.extractValue(flowParams.getQueriesCallResult().getCallResultDescXpath(), unescapedRespose);
			code = XmlUtils.extractValue(flowParams.getQueriesCallResult().getCallResultCodeXpath(), unescapedRespose);
			reiteration = XmlUtils.extractValue(flowParams.getQueriesCallResult().getcallResultReiteration(), unescapedRespose);
			parsingOk = true;
		}catch (BusinessTrasformationException bte) {
			if (logger.isDebugEnabled()) {
				logger.debug("Response parser gives error: " + unescapedRespose);
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				PrintWriter out = new PrintWriter(outStream);
				bte.printStackTrace(out);
				logger.debug(outStream.toString());
			}
			parsingOk = false;
		}
		
		String originalResponse = data.toString();
		
		if (!parsingOk) try {
			/* MOTTA if response tranformation fails, try to patch in the case 
			 * the response was not escaped. Even, the response data will be escaped;
			 */
			logger.debug("try to fix avoiding unescaping");
			result = XmlUtils.extractValue(flowParams.getQueriesCallResult().getCallResultXpath(), originalResponse);
			desc = XmlUtils.extractValue(flowParams.getQueriesCallResult().getCallResultDescXpath(), originalResponse);
			code = XmlUtils.extractValue(flowParams.getQueriesCallResult().getCallResultCodeXpath(), originalResponse);
			reiteration = XmlUtils.extractValue(flowParams.getQueriesCallResult().getcallResultReiteration(), originalResponse);
			
			String escapedFixedResponse = StringEscapeUtils.escapeXml(originalResponse);
			logger.debug("Modified escaped response is: " + escapedFixedResponse);
			data.delete(0, data.length());
			data.append(escapedFixedResponse);
			parsingOk = true;
		}catch (BusinessTrasformationException bte) {
			if (logger.isDebugEnabled()) {
				logger.debug("Response parser gives error: " + originalResponse);
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				PrintWriter out = new PrintWriter(outStream);
				bte.printStackTrace(out);
				logger.debug(outStream.toString());
			}
			parsingOk = false;
		}
		if (!parsingOk) try {
			/* MOTTA if response tranformation fails even more, try to manually substitute characters on unescaped response
			 */	
			
			logger.debug("retry to fix it changing characters in unescaped message");
			String xmlHeader = "";
			if (unescapedRespose.startsWith("<?xml")) {
				String closingTag = "?>";
				int xmlTagEnd=unescapedRespose.indexOf(closingTag) + closingTag.length();
				xmlHeader = unescapedRespose.substring(0, xmlTagEnd);
				unescapedRespose = unescapedRespose.substring(xmlTagEnd);
			}
			unescapedRespose = unescapedRespose.replaceAll("((&\\#)(?!([0-9]{1,3})(\\;)))", "&amp;#");
			unescapedRespose = unescapedRespose.replaceAll("(&)(?!([\\Qamp;\\E])|([\\Q#\\E]))", "$1amp;");
			unescapedRespose = unescapedRespose.replaceAll("(<)(?!(([a-zA-Z]{1,30})|(\\/([a-zA-Z]{1,30})\\>)))", "&lt;");
			unescapedRespose = unescapedRespose.replaceAll("(([^\1a-zA-Z][^\1\\Q/\\E]))(\\>)", "$1&gt;");
			
			unescapedRespose = xmlHeader + unescapedRespose;
			
			logger.debug("Modified unescaped response is: " + unescapedRespose);
			
			result = XmlUtils.extractValue(flowParams.getQueriesCallResult().getCallResultXpath(), unescapedRespose);
			desc = XmlUtils.extractValue(flowParams.getQueriesCallResult().getCallResultDescXpath(), unescapedRespose);
			code = XmlUtils.extractValue(flowParams.getQueriesCallResult().getCallResultCodeXpath(), unescapedRespose);
			reiteration = XmlUtils.extractValue(flowParams.getQueriesCallResult().getcallResultReiteration(), unescapedRespose);
			
			String escapedFixedResponse = StringEscapeUtils.escapeXml(unescapedRespose);
			logger.debug("Modified escaped response is: " + escapedFixedResponse);
			data.delete(0, data.length());
			data.append(escapedFixedResponse);
			parsingOk = true;
		}catch (BusinessTrasformationException bte) {
			if (logger.isDebugEnabled()) {
				logger.debug("Response parser gives error: " + unescapedRespose);
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				PrintWriter out = new PrintWriter(outStream);
				bte.printStackTrace(out);
				logger.debug(outStream.toString());
			}
			parsingOk = false;
		}
		
		if (!parsingOk) {
			/* MOTTA if response tranformation fails even more, try to manually substitute characters on unescaped response
			 */	
			logger.debug("retry to fix it changing characters in original message");
			String xmlHeader = "";
			if (originalResponse.startsWith("<?xml")) {
				String closingTag = "?>";
				int xmlTagEnd=originalResponse.indexOf(closingTag) + closingTag.length();
				xmlHeader = originalResponse.substring(0, xmlTagEnd);
				originalResponse = originalResponse.substring(xmlTagEnd);
			}
			originalResponse = originalResponse.replaceAll("((&\\#)(?!([0-9]{1,3})(\\;)))", "&amp;#");
			originalResponse = originalResponse.replaceAll("(&)(?!([\\Qamp;\\E])|([\\Q#\\E]))", "$1amp;");
			originalResponse = originalResponse.replaceAll("(<)(?!(([a-zA-Z]{1,30})|(\\/([a-zA-Z]{1,30})\\>)))", "&lt;");
			originalResponse = originalResponse.replaceAll("(([^\1a-zA-Z][^\1\\Q/\\E]))(\\>)", "$1&gt;");
			
			originalResponse = xmlHeader + originalResponse;
			
			logger.debug("Modified original response is: " + originalResponse);
			
			result = XmlUtils.extractValue(flowParams.getQueriesCallResult().getCallResultXpath(), originalResponse);
			desc = XmlUtils.extractValue(flowParams.getQueriesCallResult().getCallResultDescXpath(), originalResponse);
			code = XmlUtils.extractValue(flowParams.getQueriesCallResult().getCallResultCodeXpath(), originalResponse);
			reiteration = XmlUtils.extractValue(flowParams.getQueriesCallResult().getcallResultReiteration(), originalResponse);
			
			String originalFixedResponse = StringEscapeUtils.escapeXml(originalResponse);
			logger.debug("Modified escaped response is: " + originalFixedResponse);
			data.delete(0, data.length());
			data.append(originalFixedResponse);
			parsingOk = true;
		}

		flowResponse.setFlowResult(result);
		flowResponse.setFlowCodeResult(code);
		flowResponse.setFlowDescResult(desc);
		flowResponse.setFlowReiteration(reiteration);
		
		logger.debug("Sending response:\n" + data.toString());
	}
	


	@Override
	public void buildAckResponse(FlowParams flowParams, Map<String,String> headerData, StringBuilder dataResult, FlowResponse flowResponse) throws BusinessTrasformationException {

		IMessageManager messageManager = (IMessageManager)SpringApplicationContext.
				getBean(IMessageManager.TRACK_MSG_MANAGER_BEAN);

			
		logger.debug("buildAckResponse(" + flowParams + ", " + dataResult + ", " + flowResponse);
		
		String result = messageManager.getMessageText(IMessageManager.RESULT_POSITIVO_KEY);
		String descResult = messageManager.getMessageText(IMessageManager.RESULT_OK_KEY);
		String flowReiteration = messageManager.getMessageText(IMessageManager.RESULT_REITERATION_N_KEY);
		
		flowResponse.setFlowResult(result);
		flowResponse.setFlowDescResult(descResult);
		flowResponse.setFlowCodeResult("PA000");
		flowResponse.setFlowReiteration(flowReiteration);
		
		MessageProcessor processor;
		try {
			processor = processorFactory.getMessageProcessor(flowParams.getFlowId());
		} catch (BusinessMessageProcessorException e) {
			BusinessTrasformationException bte = new BusinessTrasformationException("errore nella composizione della risposta", e);
			throw bte;
		} 
		String responseMessage = processor.buildAckResponse(headerData);
		
		dataResult.append(responseMessage);
	}

	@Override
	public StringBuilder trasformationAfterValidation(StringBuilder data)
			throws BusinessTrasformationException {
		
		String strReplacedCap = StringUtils.replace(data.toString(), "<CAP>", "<Cap>");
		return new StringBuilder((StringUtils.replace(strReplacedCap, "</CAP>", "<Cap>")));

		
	}
	
		
}
