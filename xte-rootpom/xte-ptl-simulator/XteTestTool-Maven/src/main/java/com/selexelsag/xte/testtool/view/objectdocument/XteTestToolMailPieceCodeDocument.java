/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.objectdocument;

/**
 *
 * @author Da Procida
 */
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;


public class XteTestToolMailPieceCodeDocument extends PlainDocument {
     public static final int FIELD_MAX_LENGHT = 72;

    public XteTestToolMailPieceCodeDocument() {
        super();
    }

    @Override
    public void insertString(int offs, String str, AttributeSet a)
            throws BadLocationException {

        if (str == null) {
            return;
        }

        char[] buffer = str.toCharArray();
        char[] digit = new char[buffer.length];
        int j = 0;

        for (int i = 0; i < buffer.length; i++) {
                        
            if (Character.isDigit(buffer[i]) || buffer[i]=='-' || buffer[i] == ' ') {                
                digit[j++] = buffer[i];
            }            
            if (Character.isLetter(buffer[i])) {                
                digit[j++] = Character.toUpperCase(buffer[i]);
            }
        }

        String added = new String(digit, 0, j);

        if ((getLength() + added.length()) <= FIELD_MAX_LENGHT) {
            super.insertString(offs, added, a);
        }

    }
    
}
