package com.elsagdatamat.dbtrace.bridge.engine.ws;

import java.net.URL;

import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.selexelsag.tracktrace.trackdbws.TrackDBRWService;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;

public class TrackDBRWServiceManager implements ITrackDBRWServiceManager
{
	private Log log = LogFactory.getLog(this.getClass());
	
	private TrackDBRWService tws = null;
	private String wsdlLocation = null;
	private String serviceName = null;
	
    public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public void setWsdlLocation(String wsdlLocation) {
		this.wsdlLocation = wsdlLocation;
	}

	public TrackDBRWServiceManager() {}
	
	public TrackDBRWServiceManager(String wsdlLocation, String serviceName) {
    	this.wsdlLocation = wsdlLocation;
        this.serviceName = serviceName;
        
        try{
        	tws = createNewTrackDBRWServiceIstance(wsdlLocation, serviceName);
        }catch (Exception e) {
        	log.fatal("initialization TrackDBRWService failed", e);
		}
    }
	
	private TrackDBRWService createNewTrackDBRWServiceIstance(String newWsdlLocation, String newServiceName) {
		if(newWsdlLocation == null)
			throw new RuntimeException("URL wsdlLocation not configured");
		if(newServiceName  == null)
			throw new RuntimeException("QName serviceName not configured");
    
		TrackDBRWService trackDBRWService = null;
		try {
			URL location = new URL(newWsdlLocation);
			QName qName = new QName(newServiceName,"TrackDBRWService");
			trackDBRWService = new TrackDBRWService(location, qName);
		} catch (Exception e) {
			log.fatal("Error connecting Service: new TrackDBRWService(location, qName)", e);
		}
		return trackDBRWService;
	}

	@Override
	public TrackDBRWServicePortType getServicePort() {
		
		TrackDBRWServicePortType trackDBRWServicePort = null;
		try{
			trackDBRWServicePort = this.getTrackDBRWService().getTrackDBRWServicePort();
		} catch (Exception e) {
			log.error("Error connecting Service: getTrackDBRWServicePort()", e);
			tws = null;
		}
		return trackDBRWServicePort;
	}

	private TrackDBRWService getTrackDBRWService() {
		if(tws == null){
			log.info("TrackDBRWService's instance is null; creating new one!");
			tws = createNewTrackDBRWServiceIstance(wsdlLocation, serviceName);
		}
		return tws;
	}
    

}
