package com.elsagdatamat.trackdb.toedwh.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EDWH_TRACES_READY_TO_SEND_VW")
public class EdwhTracesReadyToSendVW implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "LABEL_TRACED_ENTITY", nullable = true)
	private String labelTracedEntity;

	@Column(name = "TRACED_ENTITY", nullable = false)
	private String tracedEntity;

	@Column(name = "ID_TRACED_ENTITY", nullable = false)
	private String idTracedEntity;

	@Column(name = "WHEN_HAPPENED", nullable = false)
	private Date whenHappened;

	@Column(name = "WHEN_REGISTERED", nullable = false)
	private Date whenRegistered;

	@Column(name = "WHERE_HAPPENED", nullable = false)
	private String whereHappened;

	@Column(name = "CHANNEL", nullable = false)
	private String channel;

	@Column(name = "ID_CHANNEL", nullable = false)
	private String idChannel;

	@Column(name = "WHAT_HAPPENED", nullable = false)
	private String whatHappened;

	@Column(name = "SERVICE_NAME", nullable = false)
	private String serviceName;

	public long getId() {
		return id;
	}

	public void setTracedEntity(String tracedEntity) {
		this.tracedEntity = tracedEntity;
	}

	public String getTracedEntity() {
		return tracedEntity;
	}

	public void setIdTracedEntity(String idTracedEntity) {
		this.idTracedEntity = idTracedEntity;
	}

	public String getIdTracedEntity() {
		return idTracedEntity;
	}

	public void setWhenHappened(Date whenHappened) {
		this.whenHappened = whenHappened;
	}

	public Date getWhenHappened() {
		return whenHappened;
	}

	public void setWhenRegistered(Date whenRegistered) {
		this.whenRegistered = whenRegistered;
	}

	public Date getWhenRegistered() {
		return whenRegistered;
	}

	public void setWhereHappened(String whereHappened) {
		this.whereHappened = whereHappened;
	}

	public String getWhereHappened() {
		return whereHappened;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getChannel() {
		return channel;
	}

	public void setIdChannel(String idChannel) {
		this.idChannel = idChannel;
	}

	public String getIdChannel() {
		return idChannel;
	}

	public void setWhatHappened(String whatHappened) {
		this.whatHappened = whatHappened;
	}

	public String getWhatHappened() {
		return whatHappened;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabelTracedEntity() {
		return labelTracedEntity;
	}

	public void setLabelTracedEntity(String labelTracedEntity) {
		this.labelTracedEntity = labelTracedEntity;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
