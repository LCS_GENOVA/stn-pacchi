package com.elsagdatamat.dbtrace.bridge.toedwh.test;

import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elsagdatamat.dbtrace.bridge.toedwh.EDWHFattPock;
import com.elsagdatamat.dbtrace.bridge.toedwh.EDWHFatturazioni;
import com.elsagdatamat.dbtrace.bridge.toedwh.EDWHFooter;
import com.elsagdatamat.dbtrace.bridge.toedwh.EDWHHeader;
import com.elsagdatamat.framework.io.StringInputStream;
import com.elsagdatamat.framework.utils.SimpleFtpClient;

@RunWith(SpringJUnit4ClassRunner.class)	
@ContextConfiguration(locations = {"classpath:/applicationContext-test.xml"})
public class EDWHMessageTest {

	private String ftpFileName = "FileDiProva.txt";
	private String ftpWorkingDirectory = "/logs/test";

	@Resource
	private SimpleFtpClient ftpClient;
	
	@Test
	public void createMessageTest(){
		
		try {
			InputStream inputStream = new StringInputStream(readFile("EsiPak1272844438.txt"));
//			InputStream inputStream = ClassLoader.getSystemResourceAsStream("EsiPak1272844438.txt");

			Calendar calendar = GregorianCalendar.getInstance();
		  	String strUuid = String.valueOf(calendar.getTimeInMillis()/1000);
		  	
			String tmpUuidFileName = ftpFileName.substring(0, ftpFileName.lastIndexOf("."))	+ strUuid;
			String uuidFileName = tmpUuidFileName + ftpFileName.substring(ftpFileName.lastIndexOf("."));

			System.out.println(uuidFileName);
//			ftpClient.createNewDefaultConnectionFromParameters();1278509489666
//			Assert.assertTrue(ftpClient.isConnected());
//			
//			ftpClient.upload(inputStream, uuidFileName, ftpWorkingDirectory);
//			Assert.assertTrue(ftpClient.destroyConnection());
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	private String readFile(String fileName){
		InputStream is = ClassLoader.getSystemResourceAsStream(fileName);
		StringBuffer sb = new StringBuffer();
		Scanner s = new Scanner(is);
		while(s.hasNext())
			sb.append(s.nextLine());
		return sb.toString();
	}
	
	@Test
	public void csvTest(){
	
		EDWHFattPock f = new EDWHFattPock();
		f.header = new EDWHHeader();
		f.header.data = "oggi";
		f.header.descrizione= "prova";
		f.header.descrizioneFile = "prova";

		f.fatturazioni = new LinkedList<EDWHFatturazioni>();
		f.fatturazioni.add(new EDWHFatturazioni());
		f.fatturazioni.add(new EDWHFatturazioni());
		f.fatturazioni.add(new EDWHFatturazioni());
	
		Map<String,String> m = new HashMap();
		EDWHFatturazioni e = new EDWHFatturazioni();
		e.cap="";
		e.provincia="GE";
		e.via=m.get("ZAGO");
		f.fatturazioni.add(e);
		f.footer = new EDWHFooter();
		f.footer.data = "oggi";
		f.footer.descrizione = "prova";
		
		
		System.out.println(f.toCsv());
		
		
	}
	
}
