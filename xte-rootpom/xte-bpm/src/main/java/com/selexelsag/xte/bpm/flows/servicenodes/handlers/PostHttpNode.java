package com.selexelsag.xte.bpm.flows.servicenodes.handlers;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.drools.process.instance.WorkItemHandler;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemManager;

import com.selexelsag.xte.business.tracking.IServiceRequest;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.service.context.SpringApplicationContext;

public class PostHttpNode implements WorkItemHandler {

	private Logger logger = LogManager.getLogger(PostHttpNode.class.getName());
	
	@Override
	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
		
	}

	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
	
		Map<String, Object> results = null;
		StringBuilder dataReturn = (StringBuilder)workItem.getParameter("dataResult");
		FlowParams params = (FlowParams)workItem.getParameter("flowParams");
		StringBuilder data = (StringBuilder)workItem.getParameter("data");
		
		try{
			
			IServiceRequest sr = (IServiceRequest)SpringApplicationContext.
					getBean(IServiceRequest.SERVICE_REQUEST_BEAN_NAME);
			StringBuilder returns = sr.getHttpResponse(params, data);
			dataReturn.append(returns);
			results = new HashMap<String, Object>();
			results.put("dataResult", dataReturn);
		}catch (Throwable t){
			
			//TODO gestione eccezioni
			// logger.error(t , t);
			results = new HashMap<String, Object>();
			results.put("error", t);
			results.put("dataResult", dataReturn);
		}finally {
			manager.completeWorkItem(workItem.getId(), results);
		}
	}

}
