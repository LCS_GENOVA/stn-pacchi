-- Categoria: Configurazione - STN-Pacchi
-- File: 162_STNP_InsertToken.sql
-- Revisione: 1.0.0
-- Data modifica: 27.05.2016
-- ----------------------------------------------------------------------------

 insert into DBTRACE_CONSUMER_TOKEN (SERVICE_ID, WS_TOKEN) values ('DBTraceToTGCBridge', '0;0');
 commit;

-- ----------------------------------------------------------------------------
-- [EOF]
-- ----------------------------------------------------------------------------
