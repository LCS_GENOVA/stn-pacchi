package com.elsagdatamat.dbtrace.dao;

import com.elsagdatamat.dbtrace.entity.PkConServiziAggiuntivi;
import com.elsagdatamat.dbtrace.entity.PkConServiziAggiuntiviPk;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface IPkConServiziAggiuntiviDAO extends IGenericDAO<PkConServiziAggiuntivi, PkConServiziAggiuntiviPk>{

}
