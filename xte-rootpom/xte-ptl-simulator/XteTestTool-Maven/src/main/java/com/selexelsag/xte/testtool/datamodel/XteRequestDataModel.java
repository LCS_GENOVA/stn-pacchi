/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.datamodel;

/**
 *
 * @author xte-user
 */
public class XteRequestDataModel {
    
    private String serviceName;
    
    private String channel;
    
    private String event;
    
    private String channelId;
    
    private String frazionari;
    
    private String version;
    
    private String xmlData;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
    
    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getFrazionari() {
        return frazionari;
    }

    public void setFrazionari(String frazionari) {
        this.frazionari = frazionari;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getXmlData() {
        return xmlData;
    }

    public void setXmlData(String xmlData) {
        this.xmlData = xmlData;
    }
}
