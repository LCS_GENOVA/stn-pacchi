package com.elsagdatamat.dbtrace.bridge.totgc.dao;

import java.util.List;

import com.elsagdatamat.dbtrace.bridge.totgc.vo.AcceptanceInfoVO;
import com.elsagdatamat.framework.dao.IGenericDAO;
import com.elsagdatamat.tt.trackdbws.beans.Trace;

public interface ITraceDAO extends IGenericDAO<Trace, Long> {

	List<AcceptanceInfoVO> getAcceptanceInfo(String mpCode);

}