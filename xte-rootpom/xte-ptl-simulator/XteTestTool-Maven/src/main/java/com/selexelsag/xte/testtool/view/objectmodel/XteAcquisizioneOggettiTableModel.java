/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectmodel;

 
import com.selexelsag.xte.testtool.datamodel.XteAcquisizioneOggetto;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Tassara
 */
public class XteAcquisizioneOggettiTableModel extends DefaultTableModel {

    private String[] columnNames = new String[]{
        ResourceBundle.getBundle("bundles/messages").getString("mailpiece.table.header.tipo"),
        ResourceBundle.getBundle("bundles/messages").getString("mailpiece.table.header.codice"),
        ResourceBundle.getBundle("bundles/messages").getString("mailpiece.table.header.dataora"),
        ResourceBundle.getBundle("bundles/messages").getString("mailpiece.table.header.ufficio"),
    };

    private Map<String, XteAcquisizioneOggetto> map = new HashMap<String, XteAcquisizioneOggetto>();
    
    public XteAcquisizioneOggettiTableModel() {

        super();
        this.setColumnCount(columnNames.length);
        this.setColumnIdentifiers(columnNames);

    }

    public void addAcquisizioneOggettoItem(XteAcquisizioneOggetto item) throws XteTestToolDuplicateKeyException{
        if(map.containsKey(item.getAcquisizioneOggettoCode()))
        {
            throw new XteTestToolDuplicateKeyException(Group.GENERIC, Severity.WARNING, "Acquisizione Oggetti Table Model duplicate key exception");
        }
        else
        {
            addRow(new Object[]{item.getAcquisizioneOggettoType(), item.getAcquisizioneOggettoCode(), item.getAcquisizioneOggettoDateTime(), item.getAcquisizioneOggettoOffice()});
            map.put(item.getAcquisizioneOggettoCode(), item);            
        }        
    }
    

    public void setModelData(Map<String, XteAcquisizioneOggetto> items) {
        getDataVector().clear();
        map.clear();                
        for (XteAcquisizioneOggetto item : items.values()) {
            addRow(new Object[]{item.getAcquisizioneOggettoType(), item.getAcquisizioneOggettoCode(), item.getAcquisizioneOggettoDateTime(), item.getAcquisizioneOggettoOffice()});
            map.put(item.getAcquisizioneOggettoCode(), item);
        }
    }

    public XteAcquisizioneOggetto getSelectedItem(int rowIndex) {
        
        String AcquisizioneOggettoCode = (String) getValueAt(rowIndex, 0);
        if(map.containsKey(AcquisizioneOggettoCode))
        {
            return map.get(AcquisizioneOggettoCode);
        }
        else
        {
            return null;
        }
    }

    @Override
    public void removeRow(int row) {
        String AcquisizioneOggettoCode = (String) getValueAt(row, 1);
        if(map.containsKey(AcquisizioneOggettoCode))
        {
            map.remove(AcquisizioneOggettoCode);
        }        
        getDataVector().removeElementAt(row);
        fireTableDataChanged();
    }

    public void clearModel() {
        getDataVector().clear();
        map.clear();
    }

    public Map<String, XteAcquisizioneOggetto> getMap() {
        return map;
    }

    
    @Override
    public boolean isCellEditable(int row, int column) {
        return (false);
    }
    
}
