package com.elsagdatamat.dbtrace.bridge.tosda;

import javax.xml.bind.annotation.XmlElement;

public class SDAOffice {
	public SDAOffice(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}

	public SDAOffice() {
	}

	@XmlElement(name = "WHERE_ID")
	public String id;

	@XmlElement(name = "WHERE_DESCRIPTION")
	public String desc;
}
