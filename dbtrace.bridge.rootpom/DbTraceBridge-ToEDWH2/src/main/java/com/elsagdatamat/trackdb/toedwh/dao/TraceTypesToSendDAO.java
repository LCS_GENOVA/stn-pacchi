package com.elsagdatamat.trackdb.toedwh.dao;

import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSend;
import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSendPK;


public class TraceTypesToSendDAO extends SpringHibernateJpaDAOBase<TraceTypesToSend, TraceTypesToSendPK> implements ITraceTypesToSendDAO {

}
