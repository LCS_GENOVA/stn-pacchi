package com.selexelsag.xte.persistence.test;

import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.selexelsag.xte.persistence.XteDBAccess;
import com.selexelsag.xte.persistence.dao.EventsDAO;
import com.selexelsag.xte.persistence.exceptions.PersistenceException;
import com.selexelsag.xte.service.context.SpringApplicationContext;

@ContextConfiguration(locations = { "/applicationContext_test.xml", "/dbConf_test.xml" })
public class TestPersistence extends AbstractTransactionalJUnit4SpringContextTests {
	
	private void printMap(Map<String, String> map) {
		Iterator<String> itKey = map.keySet().iterator();
		Iterator<String> itValues = map.values().iterator();
		while (itKey.hasNext()) {
			logger.debug(itKey.next() + " " + itValues.next());
		}
	}
	
	@Test
	public void testEventsDAO() {
		DOMConfigurator.configure("C:/development/log4j_conf/log4jConfig.xml");
		// ApplicationContext appContext = getSpringApplicationContext();
		EventsDAO eventsDao = (EventsDAO)SpringApplicationContext.getBean("EventsDAO");
		XteDBAccess xteDBAccess = (XteDBAccess)SpringApplicationContext.getBean("dbAccess");
		// XteDBAccess xteDBAccess = new XteDBAccess();
		// xteDBAccess.setEventsDAO(eventsDao);
		Map<String, String> result;
		try {
			result = xteDBAccess.getFromSERVICES("DP");
			printMap(result);
			
			result = xteDBAccess.getFromOFFICES("06206");
			printMap(result);
			
			result = xteDBAccess.getFromEVENTS("DP", "TP", "NM", "1.1");
			printMap(result);
			
		} catch (PersistenceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
