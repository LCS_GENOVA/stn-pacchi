package com.selexelsag.xte.business.impl;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeFactory;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.messagetobedelivered.entity.MessageToBeDelivered;
import com.selexelsag.messagetobedelivered.entity.MessageToBeDelivered.Status;
import com.selexelsag.messagetobedelivered.vo.CompressionType;
import com.selexelsag.messagetobedelivered.vo.MessageDestinationType;
import com.selexelsag.messagetobedelivered.vo.TrackingMessage;
import com.selexelsag.xte.business.tracking.IInteropForward;
import com.selexelsag.xte.business.xml.entities.ObjectFactory;
import com.selexelsag.xte.business.xml.entities.TMessage;
import com.selexelsag.xte.business.xml.entities.TPayload;
import com.selexelsag.xte.business.xml.entities.TrackMessage;
import com.selexelsag.xte.business.xml.entities.TrackMessageResponse;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.model.track.TrackResponse;
import com.selexelsag.xte.persistence.dao.MessageToBeDeliveredDao;
import com.selexelsag.xte.service.system.IAppConfig;

public class BaseInteropForward  implements IInteropForward  
{
	private Logger logger = LogManager.getLogger(BaseInteropForward.class.getName());

	private static final String flowId = "FID_INTEROP";
	
	private Properties properties;
	
	@Autowired
    private MessageToBeDeliveredDao messageToBeDeliveredDao;

	@Autowired
	private IAppConfig appConfig;
	public IAppConfig getAppConfig(){return appConfig;}
	public void setAppConfig(IAppConfig appConfig) {this.appConfig = appConfig;}
	
	KnowledgeBase knowledgeBase = null;
	
	public void init() 
	{
		logger.debug("BaseInteropForward initialize");
		
		properties = appConfig.getMessageProcessorProperties(); 
		
		String flowResolver = appConfig.getGeneralParam(IAppConfig.INTEROP_RESOLVER);

		logger.debug("flowResolver file name : " + flowResolver);
		
		KnowledgeBuilder builder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		builder.add(ResourceFactory.newFileResource(flowResolver),ResourceType.DRL);
		if (builder.hasErrors()) 
		{
			throw new RuntimeException(builder.getErrors().toString());
		}
		knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
		knowledgeBase.addKnowledgePackages(builder.getKnowledgePackages());		
	}
	
	@Override
	public boolean checkforInterop(FlowParams flowParams) throws Exception 
	{
		logger.debug("BaseInteropForward checkforInterop");

		String processId = null;
		
		StatefulKnowledgeSession session = knowledgeBase.newStatefulKnowledgeSession();
		try 
		{
			StringBuilder procId = new StringBuilder();
			session.insert(flowParams);

			session.insert(procId);
			session.fireAllRules();
			
			processId = procId.toString();
			logger.debug("BaseInteropForward.checkforInterop() : processId = " + processId);

		}
		catch (Throwable e) 
		{
			throw new Exception("Errore in recupero del processId");
		}
		finally 
		{
			session.dispose();
		}
		
		if ((processId==null) || (processId.equals("")))
		{
			return false;
		}
		
		return true;
	}
	
	@Override
	public void manageInterop(TrackRequest trackRequest, TrackResponse trackResponse) throws Exception 
	{
		logger.debug("BaseInteropForward manageInterop");
		logger.debug("BaseInteropForward manageInterop - trackRequest : " + trackRequest);
		logger.debug("BaseInteropForward manageInterop - trackResponse : " + trackResponse);

		// INIZIO TAPULLO
		logger.debug("INIZIO TAPULLO");
		logger.debug("trackData prima : " + trackRequest.getTrackData().toString());
		logger.debug("outputParam prima : " + trackResponse.getOutputParam());

		String trackData = trackRequest.getTrackData().toString();
		if (trackData.contains("<?xml"))
		{
			trackData = trackData.substring(trackData.indexOf("?>")+2);
		}
		trackRequest.setTrackData(new StringBuilder(trackData));
		
		String outputParam = trackResponse.getOutputParam();
		if (outputParam.contains("<?xml"))
		{
			outputParam = outputParam.substring(outputParam.indexOf("?>")+2);
		}
		trackResponse.setOutputParam(outputParam);
		
		logger.debug("trackData dopo : " + trackRequest.getTrackData().toString());
		logger.debug("outputParam dopo : " + trackResponse.getOutputParam());
		logger.debug("FINE TAPULLO");
		// FINE TAPULLO
		
		MessageDestinationType destType = MessageDestinationType.INTEROPERABILITA;
		
		String queueName = properties.getProperty(flowId + ".queueName"); 
		String messageType = properties.getProperty(flowId + ".trackingMessageType");
		boolean compressed = properties.getProperty(flowId + ".compression").equals("1");

		String processedMessage = getProcessedMessage(trackRequest, trackResponse);
		
		TrackingMessage msg = new TrackingMessage(	destType,
													queueName,
													messageType,
													processedMessage);

		logger.debug("processedMessage : " + processedMessage);

		
		msg.setCompressionType(compressed ? CompressionType.STANDARD_COMPRESSION : CompressionType.NOT_COMPRESSED);
		
		logger.debug("TrackingMessage Body : " + msg.getBody());
		logger.debug("TrackingMessage Destination : " + msg.getDestination());
		logger.debug("TrackingMessage DestinationType : " + msg.getDestinationType());
		logger.debug("TrackingMessage Frazionario : " + msg.getFrazionario());
		logger.debug("TrackingMessage tMessageType : " + msg.getMessageType());
		logger.debug("TrackingMessage ObjectCode : " + msg.getObjectCode());
		logger.debug("TrackingMessage CompressionType : " + msg.getCompressionType());
	
		MessageToBeDelivered entity = new MessageToBeDelivered();
		BeanUtils.copyProperties(msg, entity);
		entity.setCreationDate(new Date());
		entity.setStatus(Status.T);
		switch (msg.getCompressionType()) {
			case NOT_COMPRESSED:
				entity.setCompressed('N');
				break;
			case LEGACY_COMPRESSION:
				entity.setCompressed('L');
				break;
			case STANDARD_COMPRESSION:
				entity.setCompressed('S');
				break;
		}
		messageToBeDeliveredDao.insert(entity);
	}
	
	private String getProcessedMessage(TrackRequest trackRequest, TrackResponse trackResponse) throws Exception
	{
		ObjectFactory of = new ObjectFactory();
		
		// TMessage element
		TMessage tMessage = new TMessage();
		tMessage.setData(DatatypeFactory.newInstance().newXMLGregorianCalendar(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(new Date())));

		// TPayload element
		TPayload tPayload = new TPayload();

		// TrackMessage element		
		TrackMessage trackMessage = new TrackMessage();
		
		if(trackRequest.getChannel()!=null){
			trackMessage.setChannel(trackRequest.getChannel().toString());
			
			logger.debug("getProcessedMessage() trackRequest.getChannel() = " + trackRequest.getChannel());
		}else{
			trackMessage.setChannel(null);
			logger.debug("getProcessedMessage() trackRequest.getChannel() = null");
		}
		
		if(trackRequest.getSourceOfficeId()!=null){
			trackMessage.setChannelID(trackRequest.getSourceOfficeId().toString());
			
			logger.debug("getProcessedMessage() trackRequest.getSourceOfficeId() = " + trackRequest.getSourceOfficeId());
		}else{
			trackMessage.setChannelID(null);
			logger.debug("getProcessedMessage() trackRequest.getSourceOfficeId() = null");
		}
		
		if(trackRequest.getEventName()!=null){
			trackMessage.setEventName(trackRequest.getEventName().toString());
			
			logger.debug("getProcessedMessage() trackRequest.getEventName() = " + trackRequest.getEventName());
		}else{
			trackMessage.setEventName(null);
			logger.debug("getProcessedMessage() trackRequest.getEventName() = null");
		}
		
		if(trackRequest.getService()!=null){
			trackMessage.setServiceName(trackRequest.getService().toString());
			
			logger.debug("getProcessedMessage() trackRequest.getService() = " + trackRequest.getService());
		}else{
		trackMessage.setServiceName(null);
			logger.debug("getProcessedMessage() trackRequest.getService() = null");
		}
		
		if(trackRequest.getTrackData()!=null){
			trackMessage.setTrackData(trackRequest.getTrackData().toString());
			
			logger.debug("getProcessedMessage() trackRequest.getTrackData() = " + trackRequest.getTrackData());
		}else{
		trackMessage.setTrackData(null);
			logger.debug("getProcessedMessage() trackRequest.getTrackData() = null");
		}
		
		if(trackRequest.getVersion()!=null){
			trackMessage.setVersion(trackRequest.getVersion().toString());
			
			logger.debug("getProcessedMessage() trackRequest.getVersion() = " + trackRequest.getVersion());
		}else{
		trackMessage.setVersion(null);
			logger.debug("getProcessedMessage() trackRequest.getVersion() = null");
		}
		
		logger.debug("trackMessage: " + trackMessage);
		tPayload.setTrackMessage(trackMessage);
		
		// TrackMessageResponse element
		TrackMessageResponse trackMessageResponse = new TrackMessageResponse();
		
		com.selexelsag.xte.business.xml.entities.TrackResponse tr = new com.selexelsag.xte.business.xml.entities.TrackResponse();
		tr.setChannel(trackResponse.getChannel());
		tr.setChannelId(trackResponse.getSourceOfficeId());
		tr.setErrorCode(trackResponse.getErrorCode());
		tr.setErrorDesc(trackResponse.getErrorDesc());
		tr.setErrorId(trackResponse.getErrorId());
		tr.setEventName(trackResponse.getEventName());
		tr.setRetryFlag(trackResponse.getRetryFlag());
		tr.setServiceName(trackResponse.getServiceName());
		//tr.setOutputParam(trackResponse.getOutputParam());
		tr.setOutputParam(StringEscapeUtils.unescapeXml(trackResponse.getOutputParam()));
				
		trackMessageResponse.setReturnTrackMessage(tr);
	
		tPayload.setTrackMessageResponse(trackMessageResponse);
		
		tMessage.setPayload(tPayload);
		
		JAXBElement<TMessage> jb = of.createInputMessage(tMessage);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JAXBContext jc;
		try 
		{
			 jc = JAXBContext.newInstance(TMessage.class);
			 Marshaller marshaller = jc.createMarshaller();
			 marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			 marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			 marshaller.marshal(jb, baos);
		}
		catch (JAXBException e) 
		{
			//e.printStackTrace();
			logger.error("JAXBException : " + e.getMessage());
			throw e;
		}
		
		logger.debug("baos: " + baos.toString());
		return baos.toString();
	}
}
