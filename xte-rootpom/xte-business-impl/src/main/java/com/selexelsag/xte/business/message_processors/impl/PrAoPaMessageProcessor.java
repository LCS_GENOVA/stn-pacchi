package com.selexelsag.xte.business.message_processors.impl;

import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;

import javax.xml.xpath.XPathFactory;

import com.leonardo.core.log.Bpm;
import com.selexelsag.tracktrace.extension.objectmodel.message.xte.PreaccettazioneHeader;
import com.selexelsag.tracktrace.extension.objectmodel.message.xte.PreaccettazioneObj;
import com.selexelsag.tracktrace.extension.objectmodel.message.xte.PreaccettazionePezzi;
import com.selexelsag.tracktrace.extension.objectmodel.message.xte.PreaccettazionePezziSaxParser;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.selexelsag.messagetobedelivered.vo.AnagBodyFormat;
import com.selexelsag.messagetobedelivered.vo.AnagHeaderConstants;
import com.selexelsag.messagetobedelivered.vo.AnagMessageType;
import com.selexelsag.messagetobedelivered.vo.AnagSistema;
import com.selexelsag.messagetobedelivered.vo.MessageDestinationType;
import com.selexelsag.xte.business.exceptions.BusinessMessageProcessorException;
import com.selexelsag.xte.business.exceptions.BusinessTrasformationException;
import com.selexelsag.xte.business.tracking.IMessageDBManager;
import com.selexelsag.xte.business.utility.XmlUtils;
import com.selexelsag.xte.model.message_processor.MessageProcessor;
import com.selexelsag.xte.persistence.XteMessagesDBAccess;
import com.selexelsag.xte.persistence.entities.AsyncMessages;
import com.selexelsag.xte.persistence.exceptions.PersistenceException;
import com.selexelsag.xte.service.context.SpringApplicationContext;
import com.selexelsag.xte.service.resource.IMessageManager;

public class PrAoPaMessageProcessor implements MessageProcessor {

	private static Logger logger = Logger.getLogger(PrAoPaMessageProcessor.class);

	private static final String CHANNEL_ID_PARAM_NAME = "channelID";

	private static final String NEWREG_XSL_NAME = "newreg";

	private static final String flowId = "FID_PR_AO_PA";

	private Properties properties;

	private XteMessagesDBAccess dbAccess;

	private IMessageDBManager messageManager;

	@Override
	public void setMessageSender(IMessageDBManager messageManagerBase) {
		this.messageManager = messageManagerBase;
	}

	@Override
	public String getFlowId() {
		return flowId;
	}
  
	private void bpmLogOk(PreaccettazionePezzi preaccettazionePezzi, Date dataTracce) {

		bpmLogError(preaccettazionePezzi, dataTracce, Bpm.Esito.OK);

	}

	private void bpmLogError(PreaccettazionePezzi preaccettazionePezzi, Date dataTracce, String errorMessage) {
		if (preaccettazionePezzi == null) return;
		List<PreaccettazioneObj> objs = preaccettazionePezzi.getObjs();
		PreaccettazioneHeader header = preaccettazionePezzi.getHeader();
		String frazionario = header.getFrazionario();
		for (PreaccettazioneObj obj : objs) {
			Bpm.logInvioInfo(Bpm.Evento.PRE_ACCETTAZIONE_XTE, frazionario, dataTracce, obj.getCodice(), errorMessage);
		}
	}
	
	@Override
	public void processMessage(AsyncMessages message)  throws BusinessMessageProcessorException{
		logger.debug("processMessage() - processing id: " + message.getId() );

		StringBuffer newXml = null;

		String typeMsgQuery = properties.getProperty(flowId + ".parseTypeMsg");
		String officeIdQuery = properties.getProperty(flowId + ".parseOfficeId");
		
		PreaccettazionePezziSaxParser parser = new PreaccettazionePezziSaxParser();
		PreaccettazionePezzi preacettazionePezzi = parser.parse(message.getOriginalMessage());
		Date dataTracce = new Date();

		// XPathFactory factory = XPathFactory.newInstance();
		String typeMsg = "";
		String officeId = "";
		try {
			typeMsg = XmlUtils.extractValue(typeMsgQuery, message.getOriginalMessage());
			officeId = XmlUtils.extractValue(officeIdQuery, message.getOriginalMessage());
		} catch (BusinessTrasformationException e) {
			try {
				//sposto il record nei messaggi scartati per evitare errori di riprocessamento
				messageManager.moveToDiscardedMessages(message, "Error parsing message content");
				BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("Error saving to db", e);
				throw bmpe;
			} catch (Exception ee) {
				BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("Error saving to db", ee);
				throw bmpe;
			}
		}


		//#######################################################################################
		//CREA l'xml di output tramite TRASFORMAZIONE XSLT:
		//#######################################################################################

		//recupero l'xsl
		String pathXslt = String.format("%1$s/%2$s.xsl",
				properties.getProperty(flowId + ".pathXslt"),
				typeMsg
				);

		File fileXsl =new File(pathXslt);

		String error = null;
		HashMap<String, String> param = null;
		try {

			if(typeMsg.equals(NEWREG_XSL_NAME)){
				param = new HashMap<String, String>();
				param.put(CHANNEL_ID_PARAM_NAME, officeId);
			}

			newXml = XmlUtils.xslTransform(message.getOriginalMessage().toString(), FileUtils.readFileToString(fileXsl), param);
			logger.info("selectOldest() - trasformazione completata");
		} catch (Exception ex) {
			error = "message not sent: transformation failed (ID: " + message.getId() + ") - " + ex.getMessage();
			logger.error(error);
		}

		if(error != null){
			try {
				//sposto il record nei messaggi scartati per evitare errori di riprocessamento
				messageManager.moveToDiscardedMessages(message, error.toString());
			} catch (Exception e) {
				BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("Error saving to db", e);
				throw bmpe;
			}
			bpmLogError(preacettazionePezzi, dataTracce, error.toString());

			return;

		}

		try {
			boolean anagraficaEnabled = properties.getProperty(flowId + ".anagrafica.enable").equals("1");
			
			boolean cdgCompressed = properties.getProperty(flowId + ".compression").equals("1");
			boolean anagraficaCompressed = properties.getProperty(flowId + ".anagrafica.compression").equals("1");

			String cdgQueueName = properties.getProperty(flowId + ".queueName");
			String anagraficaQueueName = properties.getProperty(flowId + ".anagrafica.queueName");
			String cdgMessageType = properties.getProperty(flowId + ".trackingMessageType");
			String anagraficaMessageType = properties.getProperty(flowId + ".anagraficaMessageType");
			
			HashMap<String, Serializable> additionalData = new HashMap<String, Serializable>();
			additionalData.put(AnagHeaderConstants.ANAG_BODY_FORMAT, AnagBodyFormat.XML.value());
			additionalData.put(AnagHeaderConstants.ANAG_MESSAGE_TYPE, AnagMessageType.M3.value());
			additionalData.put(AnagHeaderConstants.ANAG_SISTEMA, AnagSistema.XTE.value());

			messageManager.sendMessage(message, MessageDestinationType.CENTRALE, newXml.toString(), cdgMessageType, cdgQueueName, cdgCompressed, null);
			if (anagraficaEnabled)
				messageManager.sendMessage(message, MessageDestinationType.ANAGRAFICA, newXml.toString(), anagraficaMessageType, anagraficaQueueName, anagraficaCompressed, additionalData);
			bpmLogOk(preacettazionePezzi, new Date());
		} catch (Exception e) {
			String errorMessage = "Error sending multiple message, async_message id: " + message.getId();
			logger.error("[" + new Date() + "]" + e.getMessage());
			message.setErrDescr(errorMessage);

			//AGGIORONO il campo "receivedTimestamp" per fare andare in coda il messaggio se in errore e permettere l'elaborazione degli altri
			message.setReceivedTimestamp(dataTracce);
			message.setSendTimestamp(null);
			
			bpmLogError(preacettazionePezzi, dataTracce, errorMessage);

			try {
				dbAccess.updateMessages(message);
			} catch (PersistenceException ee) {
				BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("Error saving to db", ee);
				throw bmpe;
			}
		}
		logger.debug("OldestMessageSender.SendOldest() - end");
	}

	@Override
	public void setDBAccess(XteMessagesDBAccess dbAccess) {
		this.dbAccess = dbAccess;
	}

	@Override
	public void setMessageProcessorProperties(Properties properties) {
		this.properties = properties;
	}

	@Override
	public String buildAckResponse(Map<String, String> extractedData) {
		IMessageManager messageManager = (IMessageManager)SpringApplicationContext.
				getBean(IMessageManager.TRACK_MSG_MANAGER_BEAN);

		logger.debug("buildAckResponse(" + extractedData + ")");

		StringBuilder resultString = new StringBuilder();

		String result = messageManager.getMessageText(IMessageManager.RESULT_POSITIVO_KEY);
		String descResult = messageManager.getMessageText(IMessageManager.RESULT_OK_KEY);
		String flowReiteration = messageManager.getMessageText(IMessageManager.RESULT_REITERATION_N_KEY);

		resultString.append("<Risultato Esito=\"" + result + "\" ");
		resultString.append("DescrizioneEsito=\"" + descResult + "\" ");
		resultString.append("CodiceEsito=\"PA000\" ");
		resultString.append("Reiterabilita=\"" + flowReiteration + "\">");

		resultString.append("<NSPED>"	+ extractedData.get("NSPED")	+ "</NSPED>");
		resultString.append("<CUSTCODE>"+ extractedData.get("CUSTCODE") + "</CUSTCODE>");
		resultString.append("<DSPED>"	+ extractedData.get("DSPED") + "</DSPED>");
		resultString.append("<OFFID>"	+ extractedData.get("OFFID") + "</OFFID>");
		resultString.append("<TYPEMSG>" + extractedData.get("TYPEMSG") + "</TYPEMSG>");

		resultString.append("</Risultato>");

		return resultString.toString();
	}


}
