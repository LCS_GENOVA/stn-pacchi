/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectrenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

/**
 *
 * @author Da Procida
 */
public class XteTableHeaderRenderer extends DefaultTableCellRenderer {

    private JTableHeader header = null;
    private JLabel renderer = null;
    private List<String> columnWidthValues;
         
    public XteTableHeaderRenderer(List<String> initialColumnWidthValues) {

        this.columnWidthValues = initialColumnWidthValues;
        setHorizontalAlignment(JLabel.CENTER);
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        defineTableFocus(table);

        for (int i = 0; i < table.getModel().getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setMinWidth(Integer.parseInt(columnWidthValues.get(i)));
            table.getColumnModel().getColumn(i).setMaxWidth(Integer.parseInt(columnWidthValues.get(i)));
            table.getColumnModel().getColumn(i).setPreferredWidth(Integer.parseInt(columnWidthValues.get(i)));
        }

        renderer = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if(table.isEnabled())
        {
            renderer.setBackground(new Color(220,220, 255));
            renderer.setForeground(new Color(51,51,51));
        }
        else
        {
            renderer.setBackground(new Color(240,240,240));
            renderer.setForeground(new Color(204,204,204));
        }

        renderer.setFont(new Font("Tahoma", Font.BOLD, 14));
        renderer.setText((value == null) ? "" : value.toString());
        renderer.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
        renderer.setPreferredSize(new Dimension(renderer.getWidth(), 22));

        return renderer;
    }

    private void defineTableFocus(JTable table) {
        Action tabOut = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
            }
        };

        Action stabOut = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent();
            }
        };

        table.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabOut");
        table.getActionMap().put("tabOut", tabOut);

        table.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, java.awt.event.InputEvent.SHIFT_DOWN_MASK), "stabOut");
        table.getActionMap().put("stabOut", stabOut);


    }
}
