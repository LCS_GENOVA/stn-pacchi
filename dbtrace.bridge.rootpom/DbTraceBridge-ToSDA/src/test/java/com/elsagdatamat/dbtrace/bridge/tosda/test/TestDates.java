package com.elsagdatamat.dbtrace.bridge.tosda.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

public class TestDates {

	private static final Logger log = Logger.getLogger(TestDates.class);
	
	public static void main(String[] args) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date d = new Date();
		
		log.fatal(sdf.format(d));
		
		java.sql.Date sqld = new java.sql.Date(d.getTime());

		log.fatal(sdf.format(sqld));
		
		
//		for (String localeId : TimeZone.getAvailableIDs()) {
//			now(localeId);
//		}
	}

	private static void now(String localeId) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone(localeId));
		String now = sdf.format(c.getTime());
		log.fatal(String.format("Now : %s", now));
		
	}
	
//	private static void pippo() { //Java 8
//		Set<String> allZones = ZoneId.getAvailableZoneIds();
//		LocalDateTime dt = LocalDateTime.now();
//
//		// Create a List using the set of zones and sort it.
//		List<String> zoneList = new ArrayList<String>(allZones);
//		Collections.sort(zoneList);
//
//
//		for (String s : zoneList) {
//		    ZoneId zone = ZoneId.of(s);
//		    ZonedDateTime zdt = dt.atZone(zone);
//		    ZoneOffset offset = zdt.getOffset();
//		    int secondsOfHour = offset.getTotalSeconds() % (60 * 60);
//		    String out = String.format("%35s %10s%n", zone, offset);
//
//		    // Write only time zones that do not have a whole hour offset
//		    // to standard out.
//		    if (secondsOfHour != 0) {
//		        System.out.printf(out);
//		    }
//		}
//	}
}
