package com.elsagdatamat.dbtrace.bridge.jms.bl;

public enum WhatHappenedFowardAnagraphic {
	// L.C. - 04042017 - Tracce che vanno inseriti in Anagrafica
	PRE_SDA,
	RIT_SDA,
//	TRA_SDA, spegnimento dei TRA_SDA da STN-Pacchi in quanto non portano alcun contenuto informativo
	ESI_SDA,
	ACC_SDA;

	public static boolean contains(String value) {
	    for (WhatHappenedFowardAnagraphic c : WhatHappenedFowardAnagraphic.values()) {
	        if (c.name().equals(value)) {
	            return true;
	        }
	    }
	    return false;
	}
}
