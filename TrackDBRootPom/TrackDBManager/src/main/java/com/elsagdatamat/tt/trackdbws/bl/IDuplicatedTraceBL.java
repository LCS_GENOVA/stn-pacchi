package com.elsagdatamat.tt.trackdbws.bl;

import com.elsagdatamat.tt.trackdbws.beans.DuplicatedTrace;
import com.elsagdatamat.tt.trackdbws.beans.Trace;

public interface IDuplicatedTraceBL {

	public void singleInsertDuplicatedTrace(DuplicatedTrace duplicatedTrace);

	public void singleInsertDuplicatedTrace(Trace trace);

}
