package com.elsagdatamat.dbtrace.bridge.engine.dao;

import java.sql.SQLException;

import javax.persistence.EntityManager;

import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.transaction.TransactionDefinition;

class CustomHibernateJpaDialect extends HibernateJpaDialect {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7951538657691109551L;
	
	private int defaultTimeout = TransactionDefinition.TIMEOUT_DEFAULT;

	public void setDefaultTimeout(final int defaultTimeout) { 
		this.defaultTimeout = defaultTimeout; 
	}
	
	@Override
	public Object beginTransaction(final EntityManager entityManager, final TransactionDefinition definition) throws SQLException {
		
		//Sel per la transazione � stato settato un timeout specifico uso quello
		 
		
		if(definition.getTimeout() != TransactionDefinition.TIMEOUT_DEFAULT)
		{
			//Se per la transazione � stato settato un timeout specifico uso quello
			getSession(entityManager).getTransaction().setTimeout( definition.getTimeout()); 
		}
		else if (defaultTimeout != TransactionDefinition.TIMEOUT_DEFAULT) 
		{ 
			//altrimenti, se � stato impostato un timeout di default specifico per questo bean utilizzo quello
			getSession(entityManager).getTransaction().setTimeout( defaultTimeout); 
		}
		return super.beginTransaction(entityManager, definition);
	}





}
