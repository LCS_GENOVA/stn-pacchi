package com.elsagdatamat.dbtrace.bridge.totgc.dao;

import com.elsagdatamat.dbtrace.bridge.totgc.entities.TraceTypesToSend;
import com.elsagdatamat.dbtrace.bridge.totgc.entities.TraceTypesToSendPK;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class TraceTypesToSendDAO extends SpringHibernateJpaDAOBase<TraceTypesToSend, TraceTypesToSendPK> implements ITraceTypesToSendDAO {

}
