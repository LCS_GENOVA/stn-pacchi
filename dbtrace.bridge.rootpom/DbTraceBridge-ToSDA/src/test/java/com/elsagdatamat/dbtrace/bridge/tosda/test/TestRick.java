package com.elsagdatamat.dbtrace.bridge.tosda.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.dbtrace.bridge.engine.bl.BLToken;
import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.elsagdatamat.dbtrace.bridge.engine.entities.Token;
import com.elsagdatamat.dbtrace.bridge.tosda.ToSdaWsToJms;
import com.elsagdatamat.dbtrace.bridge.tosda.manager.ITraceManager;
import com.elsagdatamat.dbtrace.bridge.wstojms.WsToJmsBridge;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:rick-applicationContext.xml")
@Transactional(propagation = Propagation.REQUIRED)
@TransactionConfiguration(defaultRollback = true)
public class TestRick {

	private final Logger log = Logger.getLogger(getClass());
	@Autowired
	public ITraceManager traceManager;
	
	@Autowired
	public BLToken<ToToken> blToken;
	
	@Autowired
	public ToSdaWsToJms dbTracceToSDABridge;
	
	@Test
	public void testApplicationContext() {
		assertNotNull(blToken);
		assertNotNull(dbTracceToSDABridge);
	}

	@Test
	public void testBLToken() {
		Token t = blToken.findByID("DBTraceToSDABridge");
		assertNotNull(t);
		
		Date accessDate = t.getAccessDate();
		Date releaseDate = t.getReleaseDate();
		String serviceId = t.getServiceId();
		String token = t.getWsToken();
		
		String[] parts = token.split(";");
		assertEquals(2, parts.length);
		
		for (Locale locale : DateFormat.getAvailableLocales()) {
			log.fatal(locale.toString());
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String strAccessDate = sdf.format(accessDate);
		String strReleasesDate = sdf.format(releaseDate);
		
//		Long l = Long.valueOf(parts[0]);
//		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
////		c.add(Calendar.MINUTE, -20);
//		c.setTimeInMillis(l);
//
//		String strTokenDate = sdf.format(c.getTime());
		
		log.fatal(String.format("Service id : %s", serviceId));
		log.fatal(String.format("Access date : %s", strAccessDate));
		log.fatal(String.format("Release date : %s", strReleasesDate));
//		log.fatal(String.format("Token date : %s", strTokenDate));
		
		
		
		
		Long lastTokenId = Long.valueOf(parts[1]);
		Long epoch = Long.valueOf(parts[0]);
		if (epoch < 1451606400000L)
			epoch = 1451606400000L;
		Date lastTokenDate = new Date(epoch);
		log.fatal(String.format("Splitted token : id = '%s' - date = '%s'", lastTokenId.toString(), lastTokenDate.toString()));

//		Calendar c = Calendar.getInstance();
//		c.setTime(lastTokenDate);
////		c.add(Calendar.DATE, 1);
//		c.add(Calendar.HOUR, getQueryHoursInterval());
		for (String timeZoneID : TimeZone.getAvailableIDs()) {
			log.fatal(timeZoneID);
		}
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		log.fatal(c.toString());
		c.add(Calendar.MINUTE, -20);
		String nowmeno20 = sdf.format(c.getTime());
		log.fatal(String.format("Now - 20 : %s", nowmeno20));
	}
	
	@Test
	public void testAppContext() {
		
	}
	
	@Test
	public void testLoop() {
		dbTracceToSDABridge.unicoMetodoDaChiamareDaQuartz();
	}
}
