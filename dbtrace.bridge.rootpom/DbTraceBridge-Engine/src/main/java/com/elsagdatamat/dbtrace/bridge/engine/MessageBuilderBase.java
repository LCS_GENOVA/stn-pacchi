/**
 *
 */
package com.elsagdatamat.dbtrace.bridge.engine;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.elsagdatamat.dbtrace.bridge.engine.bl.IBLToken;
import com.elsagdatamat.dbtrace.bridge.engine.bl.IDiscardedTraceManager;
import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TokenTilltoDateByFiltersList;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

/**
 * @author Salaris
 *
 */
public abstract class MessageBuilderBase implements IMessageBuilder, IMessageSender,ITracesReader {

	protected IDiscardedTraceManager discardedTraceManager;
	protected static final Log log = LogFactory.getLog(MessageBuilderBase.class);

	protected ToToken token;

	public ToToken getToken() {
		return token;
	}

	public void setToken(ToToken token) {
		this.token = token;
	}

	@Override
	public String getserviceId(){
	    return token.getServiceId();
	}
	public void setDiscardedTraceManager(IDiscardedTraceManager discardedTraceManager) {
		this.discardedTraceManager = discardedTraceManager;
	}


	protected IBLToken<ToToken> blToken;

	public void setBlToken(IBLToken<ToToken> blToken) {
		this.blToken = blToken;
	}

	protected boolean saveTraces;

	public boolean isSaveTraces() {
		return saveTraces;
	}

	public void setSaveTraces(boolean saveTraces) {
		this.saveTraces = saveTraces;
	}

	String taskId;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public abstract TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate);


	protected XMLGregorianCalendar getGCalendar(Date date) {
		if (date == null)
			return null;
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);

		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		} catch (DatatypeConfigurationException e) {
			log.error(e.getMessage());
			return null;
		}
	}

//	protected void logToken(IToToken token) {
//		log.debug("TokenID=" + token.getServiceId());
//		log.debug("TokenWS=" + token.getWsToken());
//	}

	// ITracesReader
		@Override
		public Object getToken(TrackDBRWServicePortType twsp) {
			ToToken foundToken=null;
			try {
				try {
					log.debug("Token: "+token);
					foundToken = blToken.testAndLockWork(token);

				} catch (Exception ex) {
					log.debug("Token locked by another process: " + ex.getMessage(),ex);
					return null;
				}
				if(foundToken==null){
					log.debug(String.format("Token NOT found for service '%s' (wsToken='%s');", token.getServiceId(), token.getWsToken()));
					return null;
				}
				log.debug(String.format("Token found for service '%s' (wsToken='%s');", token.getServiceId(), token.getWsToken()));
				if (foundToken != null && foundToken.getWsToken() == null) {
					Calendar c = Calendar.getInstance();
					c.set(Calendar.HOUR_OF_DAY, 0);
					c.set(Calendar.MINUTE, 0);
					c.set(Calendar.SECOND, 0);
					c.set(Calendar.MILLISECOND, 0);

					try {
						foundToken.setWsToken(twsp.getToken(getGCalendar(c.getTime())));
						log.info(String.format("WSToken found for service '%s': %s;", token.getServiceId(), token.getWsToken()));
					} catch (Exception ex) {
						blToken.releaseWork(foundToken);
						log.debug("Error invoking WS Method 'getToken': " + ex.getMessage(), ex);
						return null;
					}
					// Rick fidandosi di SCOTT: lo scrivo se no da qualche parte fa
					// casino...
					blToken.updateWork(foundToken);
					token=foundToken;
				}
			} catch (Exception e) {
				log.debug("Leaving getToken");
				log.error(e.getMessage());
				return null;
			}
			return foundToken;
		}

		// ITracesReader
		@Override
		public void release(TrackDBRWServicePortType twsp) {
			log.debug(String.format("Release token: serviceId='%s'; wsToken='%s';", token.getServiceId(), token.getWsToken()));
			blToken.releaseWork(token);
		}

		// ITracesReader
		@Override
		public void acknowledge(TrackDBRWServicePortType twsp) {
			log.debug(String.format("Acknowledge token: serviceId='%s'; wsToken='%s';", token.getServiceId(), token.getWsToken()));
			blToken.updateWork( token);
		}

		protected static void logTrace(Trace t, String prompt) {
			if (t.getChannel() != null)
			    log.debug(prompt + " CHANNEL = " + t.getChannel());
			if (t.getIdChannel() != null)
			    log.debug(prompt + " ID CHANNEL = " + t.getIdChannel());
			if (t.getTracedEntity() != null)
			    log.debug(prompt + " TRACED ENTITY = " + t.getTracedEntity());
			if (t.getIdTracedEntity() != null)
			    log.debug(prompt + " ID TRACED ENTITY = " + t.getIdTracedEntity());
			if (t.getLabelTracedEntity() != null)
			    log.debug(prompt + " LABEL TRACED ENTITY = " + t.getLabelTracedEntity());
			if (t.getServiceName() != null)
			    log.debug(prompt + " SERVICE NAME = " + t.getServiceName());
			if (t.getWhatHappened() != null)
			    log.debug(prompt + " WHAT HAPPENED = " + t.getWhatHappened());
			if (t.getWhenHappened() != null)
			    log.debug(prompt + " WHEN HAPPENED = " + t.getWhenHappened().toGregorianCalendar().getTime());
			if (t.getWhenRegistered() != null)
			    log.debug(prompt + " WHEN REGISTERED = " + t.getWhenRegistered().toGregorianCalendar().getTime());
			if (t.getWhereHappened() != null)
			    log.debug(prompt + " WHERE HAPPENED = " + t.getWhereHappened());
		    }

		 protected static void logGetTracesInput(TokenTilltoDateByFiltersList getTracesInput) {
			if (!log.isDebugEnabled())
			    return;
			log.debug("Token = " + getTracesInput.getToken());
			log.debug("Till to date = " + getTracesInput.getTilltoDate().toGregorianCalendar().getTime());
			if (getTracesInput.getTracesFilterByExampleEqual() != null) {
			    for (Trace t : getTracesInput.getTracesFilterByExampleEqual().getTraces()) {
				logTrace(t, "Filter Equal");
			    }
			}
			if (getTracesInput.getTracesFilterByExampleNotEqual() != null) {
			    for (Trace t : getTracesInput.getTracesFilterByExampleNotEqual().getTraces()) {
				logTrace(t, "Filter Not Equal");
			    }
			}
		    }


		 /**
		  * Operazioni generiche in chiusura elaborazione
		  */
		 @Override
		public void commitWork(){
			 return;
		 }
}
