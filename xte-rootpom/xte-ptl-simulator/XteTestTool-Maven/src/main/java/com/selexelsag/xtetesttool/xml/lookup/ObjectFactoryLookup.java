package com.selexelsag.xtetesttool.xml.lookup;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.selexelsag.xtetesttool.xml.lookup package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactoryLookup {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.selexelsag.xtetesttool.xml.lookup
     * 
     */
    public ObjectFactoryLookup() {
    }

    /**
     * Create an instance of {@link Lookup.UfficiDBServer.Ufficio }
     * 
     */
    public Lookup.UfficiDBServer.Ufficio createLookupUfficiDBServerUfficio() {
        return new Lookup.UfficiDBServer.Ufficio();
    }

    /**
     * Create an instance of {@link Lookup }
     * 
     */
    public Lookup createLookup() {
        return new Lookup();
    }

    /**
     * Create an instance of {@link Lookup.ProdottiMessi.Prodotto }
     * 
     */
    public Lookup.ProdottiMessi.Prodotto createLookupProdottiMessiProdotto() {
        return new Lookup.ProdottiMessi.Prodotto();
    }

    /**
     * Create an instance of {@link Lookup.Causali }
     * 
     */
    public Lookup.Causali createLookupCausali() {
        return new Lookup.Causali();
    }

    /**
     * Create an instance of {@link Lookup.Causali.Causale }
     * 
     */
    public Lookup.Causali.Causale createLookupCausaliCausale() {
        return new Lookup.Causali.Causale();
    }

    /**
     * Create an instance of {@link Lookup.CodiciProdotti.Prodotto }
     * 
     */
    public Lookup.CodiciProdotti.Prodotto createLookupCodiciProdottiProdotto() {
        return new Lookup.CodiciProdotti.Prodotto();
    }

    /**
     * Create an instance of {@link Lookup.CodiciProdotti }
     * 
     */
    public Lookup.CodiciProdotti createLookupCodiciProdotti() {
        return new Lookup.CodiciProdotti();
    }

    /**
     * Create an instance of {@link Lookup.UfficiDBServer }
     * 
     */
    public Lookup.UfficiDBServer createLookupUfficiDBServer() {
        return new Lookup.UfficiDBServer();
    }

    /**
     * Create an instance of {@link Lookup.ProdottiMessi }
     * 
     */
    public Lookup.ProdottiMessi createLookupProdottiMessi() {
        return new Lookup.ProdottiMessi();
    }

}
