package com.elsagdatamat.dbtrace.bridge.tosda.manager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.elsagdatamat.dbtrace.bridge.tosda.WhatHappened;
import com.elsagdatamat.dbtrace.bridge.tosda.dao.ITraceDAO;
import com.elsagdatamat.framework.search.Filter;
import com.elsagdatamat.framework.search.Search;
import com.elsagdatamat.tt.trackdbws.beans.Trace;

public class ToSDACache {
	
	private final static Logger log = Logger.getLogger(ToSDACache.class);

	public static final String FIXED_CHANNEL = "SDA";
	public static final String FIXED_TRACED_ENTITY = "PACCO";

	private final static String TRACED_ENTITY_FIELD = "tracedEntity";
	private final static String CHANNEL_FIELD = "channel";
	private final static String WHEN_REGISTERED_FIELD = "whenRegistered";
	private final static String WHAT_HAPPENED_FIELD = "whatHappened";
	private final static String TRACE_ID_FIELD = "traceId";
	
	private final ITraceDAO traceDAO;
	private final Integer chunkSize;
	private final Integer cacheSize;
	private final Integer queryMinutesInterval;
	private Date stopDate;
	private final ToToken token;
	
	public ToSDACache(
			ToToken token,
			Date stopDate,
			Integer chunkSize,
			Integer cacheSize,
			Integer queryMinutesInterval,
			ITraceDAO traceDAO) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		log.debug(String.format("Setting up cache; cacheSize:%d, chunkSize:%s, queryMinutesInterval:%d, targetDate:%s",
				cacheSize, chunkSize, queryMinutesInterval, sdf.format(stopDate)));
		
		this.traceDAO = traceDAO;
		this.chunkSize = chunkSize;
		this.cacheSize = cacheSize;
		
		this.queryMinutesInterval = queryMinutesInterval;
		this.stopDate = stopDate;
		this.token = token;
	}
	
	
	private List<Trace> cache = new ArrayList<Trace>();
	
	public List<Trace> getNextChunk() {
		if (cache.size() < chunkSize) {
			reloadCache();
		}

		int cs = Math.min(cache.size(), chunkSize);
		List<Trace> traceChunk = new ArrayList<Trace>(cs);
		for (int i = 0; i < cs; i++) {
			traceChunk.add(cache.remove(0));
		}
		
		return traceChunk;
	}
	
	
	private Date computeUpperDate(Date lowerDate) {
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		c.setTime(lowerDate);
		c.add(Calendar.MINUTE, queryMinutesInterval);
		Date guardDate = c.getTime();
		if (guardDate.before(stopDate)) {
			return guardDate;
		} else {
			return stopDate;
		}
	}

	private static class Bounds {
		Long id;
		Date date;
		public Bounds(Long id, Date date) {
			super();
			this.id = id;
			this.date = date;
		}
	}
	
	private Bounds computeLowerBounds() {
		Long id = null;
		Date lowerDate = null;
		if(cache.size()>0) {
			Trace lastTraceCached = cache.get(cache.size()-1);
			id = lastTraceCached.getTraceId();
			lowerDate = lastTraceCached.getWhenRegistered();
		} else { 
			String[] split = token.getWsToken().split(";");
			id = Long.valueOf(split[1]);
			Long epoch = Long.valueOf(split[0]);
			if (epoch < 145160640000L)
			epoch = 1451606400000L;
			lowerDate = new Date(epoch);
		}
		
		return new Bounds(id, lowerDate);
	}
	
	private void reloadCache() {
		
		Bounds lowerBounds = computeLowerBounds();

		while(true) {
			log.debug(String.format("Query lower bounds, date:%d, id:%d", lowerBounds.date.getTime(), lowerBounds.id));
			Date upperDate = computeUpperDate(lowerBounds.date);
	
			List<Trace> traceList = readFromDB(lowerBounds.date, lowerBounds.id, upperDate, cacheSize);
			cache.addAll(traceList);
			if (cache.size() >= chunkSize) {
				break;
			}
			if (upperDate.compareTo(stopDate) >= 0) {
				break;
			}
			
			if (traceList.size() > 0) {
				Trace lastTrace = traceList.get(traceList.size()-1);
				lowerBounds.date = lastTrace.getWhenRegistered();
				lowerBounds.id = lastTrace.getTraceId();
			} else {
				lowerBounds.date = upperDate;
//				lowerBounds.id = 0L;
			}
		}
	}
	
	private List<Trace> readFromDB(Date lowerDate, Long id, Date upperDate, int maxRows) {
		List<String> whatHappeneds = new ArrayList<String>();
		for (WhatHappened wh : WhatHappened.values()) {
			whatHappeneds.add(wh.name());
		}
		
		Search search = new Search(Trace.class);
		
		
		Filter filterChannelNotIn = Filter.notEqual(CHANNEL_FIELD, "SDA"/*,"APT","RTZ"*/);
		Filter filterTracedEntity = Filter.equal(TRACED_ENTITY_FIELD, "PACCO");
		Filter whatHappened = Filter.in(WHAT_HAPPENED_FIELD, whatHappeneds);
		
		java.sql.Date lastFetchedRecordSqlDate = new java.sql.Date(lowerDate.getTime());
		java.sql.Date sqlDateTillto = new java.sql.Date(upperDate.getTime());
		
		search.addFilterAnd(
				Filter.or(
					Filter.and(
							Filter.greaterThan(WHEN_REGISTERED_FIELD, lastFetchedRecordSqlDate),
							Filter.lessThan(WHEN_REGISTERED_FIELD, sqlDateTillto)),
					Filter.and(
							Filter.equal(WHEN_REGISTERED_FIELD, lastFetchedRecordSqlDate),
							Filter.greaterThan(TRACE_ID_FIELD, id))),
				Filter.and(
						filterChannelNotIn, filterTracedEntity, whatHappened));

		search.addSort(WHEN_REGISTERED_FIELD);
		search.addSort(TRACE_ID_FIELD);
	
		search.setMaxResults(maxRows);

		List<Trace> l = traceDAO.search(search);
		if (l == null) {
			l = new ArrayList<Trace>();
		}
		return l;
	}


	public Date getStopDate() {
		return stopDate;
	}


	public void setStopDate(Date stopDate) {
		this.stopDate = stopDate;
	}
}
