package com.elsagdatamat.dbtrace.bridge.tosda;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;

import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;

public class SDARitSda {
	@XmlElement(name="ID_RITIRO")
	public String idRitiro;
	
	@XmlElement(name="CODICE_SAP")
	public String codiceSap; 
	
	@XmlElement(name="PARTITA_IVA")
	public String partitaIva;
	
	@XmlElement(name="CODICE_FISCALE")
	public String codiceFiscale;
	
	@XmlElement(name="CAP")
	public String cap;
	
	@XmlElement(name="VIA")
	public String via;
	
	@XmlElement(name="LOCALITA")
	public String localita; 
	
	@XmlElement(name="PROVINCIA")
	public String provincia;
	
	@XmlElement(name="NUM_LDV_DA")
	public String numLdvDa;
	
	@XmlElement(name="NUM_LDV_A")
	public String numLdvA; 
	
	@XmlElement(name="TIPO_CLIENTE")
	public String tipoCliente;
	
	@XmlElement(name="IMPORTO_SPEDIZIONE")
	public String importoSpedizione;
	
	@XmlElement(name="IMPORTO_RITIRO")
	public String importoRitiro;
	
	@XmlElement(name="IMPORTO_TD")
	public String importoTd; 
	
	@XmlElement(name="FLAG_SERVIZIO_CA")
	public String flagServizioCa; 
	
	@XmlElement(name="FLAG_SERVIZIO_ASS")
	public String flagServizioAss; 
	
	@XmlElement(name="CODICE_CRE")
	public String codiceCre;
	
	@XmlElement(name="CODICE_FILIALE")
	public String codiceFiliale;
	
	@XmlElement(name="ACCOUNT_Q")
	public String accountQ; 
	
	@XmlElement(name="ACCOUNT_P")
	public String accountP; 
	
	@XmlElement(name="ACCOUNT_SERV_ACC_C_ASSEGNO_C_C")
	public String accountServAccCAssegnoCC; 
	
	@XmlElement(name="ACCOUNT_SERV_ACC_ASSICURAZIONE")
	public String accountServAccAssicurazione;
	
	@XmlElement(name="REFERENCE")
	public String reference;
	
	public SDARitSda(){}
	
	public SDARitSda(TraceDetail[] dlist)
	{
		Map<String, String> map = new HashMap<String, String>();
		for (TraceDetail d:dlist)
		{
			map.put(d.getParamClass(), d.getParamValue());
		}

		String key = "ID_RITIRO";
		this.idRitiro= map.get(key);
		
		key = "CODICE_SAP";
		this.codiceSap= map.get(key); 
		
		key = "PARTITA_IVA";
		this.partitaIva= map.get(key);
		
		key = "CODICE_FISCALE";
		this.codiceFiscale= map.get(key);
		
		key = "CAP";
		this.cap= map.get(key);
		
		key = "VIA";
		this.via= map.get(key);
		
		key = "LOCALITA";
		this.localita= map.get(key); 
		
		key = "PROVINCIA";
		this.provincia= map.get(key);
		
		key = "NUM_LDV_DA";
		this.numLdvDa= map.get(key);
		
		key = "NUM_LDV_A";
		this.numLdvA= map.get(key); 
		
		key = "TIPO_CLIENTE";
		this.tipoCliente= map.get(key);
		
		key = "IMPORTO_SPEDIZIONE";
		this.importoSpedizione= map.get(key);
		
		key = "IMPORTO_RITIRO";
		this.importoRitiro= map.get(key);
		
		key = "IMPORTO_TD";
		this.importoTd= map.get(key); 
		
		key = "FLAG_SERVIZIO_CA";
		this.flagServizioCa= map.get(key); 
		
		key = "FLAG_SERVIZIO_ASS";
		this.flagServizioAss= map.get(key); 
		
		key = "CODICE_CRE";
		this.codiceCre= map.get(key);
		
		key = "CODICE_FILIALE";
		this.codiceFiliale= map.get(key);
		
		key = "ACCOUNT_Q";
		this.accountQ= map.get(key); 
		
		key = "ACCOUNT_P";
		this.accountP= map.get(key); 
		
		key = "ACCOUNT_SERV_ACC_C_ASSEGNO_C_C";
		this.accountServAccCAssegnoCC= map.get(key); 
		
		key = "ACCOUNT_SERV_ACC_ASSICURAZIONE";
		this.accountServAccAssicurazione= map.get(key);
		
		key = "REFERENCE";
		this.reference= map.get(key);
	}
}
