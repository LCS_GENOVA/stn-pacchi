package com.selexelsag.xte.business.exceptions;

public class BusinessFlowMapperException extends BusinessException {

	public BusinessFlowMapperException(String message, Throwable cause) {
		super(message, cause);
		setExceptionId(BUSINESS_FLOWMAPPER_ID);
	}
}
