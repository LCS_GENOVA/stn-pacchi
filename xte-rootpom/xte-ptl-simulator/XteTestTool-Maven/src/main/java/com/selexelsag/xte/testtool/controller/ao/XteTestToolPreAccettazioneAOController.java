/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.ao;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.ao.XteTestToolPreAccettazioneAOView;
import com.selexelsag.xte.testtool.view.ao.XteTestToolWebServiceAOView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.dp.XteTestToolWebServiceDPView;
import com.selexelsag.xtetesttool.xml.accettazioneonline.AccettazioneOnline;
import com.selexelsag.xtetesttool.xml.accettazioneonline.AccettazioneOnline.PA;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolPreAccettazioneAOController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener {

    private XteTestToolPreAccettazioneAOView preAccettazioneAOView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolPreAccettazioneAOController.class);
    private List<JTextComponent> mailPieceFieldList = new ArrayList<JTextComponent>();
    
    private JFileChooser caricaXmlFileChooser = new JFileChooser();;
    
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    public XteTestToolPreAccettazioneAOController(JPanel preAccettazioneAOView) {
        try {
            logger.info("XteTestToolPreAccettazioneAOController start class creation");
            this.preAccettazioneAOView = (XteTestToolPreAccettazioneAOView) preAccettazioneAOView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.preAccettazioneAOView.initializeView();
            addViewObjectsListeners();
            logger.info("XteTestToolPreAccettazioneAOController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPreAccettazioneAOController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolPreAccettazioneAOController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Accettazione Online controller creation failed", ex);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = preAccettazioneAOView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        preAccettazioneAOView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.preAccettazioneAOView.getPreAccettazionePanelRef().addAncestorListener(this);        
        
        this.preAccettazioneAOView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.preAccettazioneAOView.getWsDataTextAreaRef().addFocusListener(this);    
        
        this.preAccettazioneAOView.getPulisciRichiestaButtonRef().addActionListener(this);
        this.preAccettazioneAOView.getPulisciRispostaButtonRef().addActionListener(this);
        this.preAccettazioneAOView.getChiamaWSButtonRef().addActionListener(this);
        this.preAccettazioneAOView.getCaricaFolderButtonRef().addActionListener(this);
        this.preAccettazioneAOView.getCaricaXmlButtonRef().addActionListener(this);
        
        this.preAccettazioneAOView.getBackButtonRef().addActionListener(this);
        this.preAccettazioneAOView.getHomeButtonRef().addActionListener(this);
        this.preAccettazioneAOView.getExitButtonRef().addActionListener(this);
        
        this.caricaXmlFileChooser.addActionListener(this);
                    
	this.salvaXmlFileChooser.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(preAccettazioneAOView.getPulisciRichiestaButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazioneAOController actionPerformed invoke pulisciRichiesta");
            pulisciRichiesta();
        }
        if(source.equals(preAccettazioneAOView.getPulisciRispostaButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazioneAOController actionPerformed invoke pulisciRisposta");
            pulisciRisposta();
        }
        if(source.equals(preAccettazioneAOView.getCaricaFolderButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazioneAOController actionPerformed invoke caricaFolder");
            caricaFolder();
        }
        if(source.equals(preAccettazioneAOView.getCaricaXmlButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazioneAOController actionPerformed invoke caricaXml");
            caricaXml();
        }

        if(source.equals(preAccettazioneAOView.getChiamaWSButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazioneAOController actionPerformed invoke chiamaWebService");
            chiamaWebService();
        }

        if(source.equals(preAccettazioneAOView.getBackButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazioneAOController actionPerformed invoke back");
            back();
        }

        if(source.equals(preAccettazioneAOView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazioneAOController actionPerformed invoke home");
            home();
        }

        if(source.equals(preAccettazioneAOView.getExitButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazioneAOController actionPerformed invoke exit");
            exit();
        }

		if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(preAccettazioneAOView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

        if(source.equals(caricaXmlFileChooser))
        {
            logger.debug("XteTestToolPreAccettazioneAOController actionPerformed invoke carica XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                if (selectedFile == null) {
                    return;
                }
                OutputStream out = null;
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    if (!readXmlFile(selectedFile, preAccettazioneAOView.getXmlDataTextAreaRef())) {
                        logger.error("XteTestToolPreAccettazioneAOController caricaXml failed load XML file");
                        throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Pre Accettazione Accettazione Online controller caricaXml failed load XML file");
                    }
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolPreAccettazioneAOView)
        {
            logger.debug("XteTestToolPreAccettazioneAOController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazioneAOController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Accettazione Online controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazioneAOController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Accettazione Online controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }    
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        preAccettazioneAOView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolPreAccettazioneAOController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.preAccettazioneAOView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(preAccettazioneAOView);
            updateXteTestToolDataModel(dataModel);

            clearMessageBar();

            preAccettazioneAOView.getXmlDataTextAreaRef().requestFocus();

            logger.info("XteTestToolPreAccettazioneAOController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPreAccettazioneAOController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazioneAOController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Accettazione Online controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolPreAccettazioneAOController start exit");
        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(preAccettazioneAOView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolPreAccettazioneAOController exit in progress");
            preAccettazioneAOView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolPreAccettazioneAOController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(preAccettazioneAOView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(preAccettazioneAOView.getPreAccettazionePanelRef().getParent(), preAccettazioneAOView.getPreAccettazionePanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolPreAccettazioneAOController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(preAccettazioneAOView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(preAccettazioneAOView.getPreAccettazionePanelRef().getParent(), preAccettazioneAOView.getPreAccettazionePanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }
    
    private void pulisciRichiesta()
    {
        logger.info("XteTestToolPreAccettazioneAOController start pulisciRichiesta");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            preAccettazioneAOView.getXmlDataTextAreaRef().setText("");
            clearMessageBar();

            logger.info("XteTestToolPreAccettazioneAOController pulisciRichiesta executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPreAccettazioneAOController pulisciRichiesta failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazioneAOController pulisciRichiesta failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Accettazione Online controller pulisciRichiesta failed", ex);
        }
    }
    
    private void pulisciRisposta()
    {
        logger.info("XteTestToolPreAccettazioneAOController start pulisciRisposta");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            preAccettazioneAOView.getWsDataTextAreaRef().setText("");
            clearMessageBar();

            logger.info("XteTestToolPreAccettazioneAOController pulisciRisposta executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPreAccettazioneAOController pulisciRisposta failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazioneAOController pulisciRisposta failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Accettazione Online controller pulisciRisposta failed", ex);
        }
    }

    private void caricaFolder()
    {
        try {
            logger.info("XteTestToolPreAccettazioneAOController start caricaFolder");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            logger.info("XteTestToolPreAccettazioneAOController caricaFolder executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolPreAccettazioneAOController caricaFolder Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazioneAOController caricaFolder failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Accettazione Online controller caricaFolder failed", ex);
        }
    }

    private void caricaXml()
    {
        try {
            logger.info("XteTestToolPreAccettazioneAOController start caricaXml");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            caricaXmlFileChooser.setCurrentDirectory(new File("."));
            caricaXmlFileChooser.setSize(new Dimension(350, 200));
            caricaXmlFileChooser.setFileFilter(new XmlFileFilter());
            caricaXmlFileChooser.showOpenDialog(preAccettazioneAOView.getParentFrame());
                                    
            logger.info("XteTestToolPreAccettazioneAOController caricaXml executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolPreAccettazioneAOController caricaXml Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazioneAOController caricaXml failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Accettazione Online controller caricaXml failed", ex);
        }
    }
    
    private boolean readXmlFile(File xmlFile, JTextComponent pane) {
        FileReader fr = null;
        try {
            fr = new FileReader(xmlFile);
            pane.read(fr, null);
            fr.close();
            return true;
        }
        catch (IOException e) {
            return false;
        }
        finally {
            try {
                fr.close();
            } catch (IOException ex) {
            }
        }                
    }

    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void chiamaWebService()
    {
        logger.info("XteTestToolPreAccettazioneAOController start chiamaWebService");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServiceAOView(preAccettazioneAOView.getParentFrame());
            XteTestToolWebServiceAOController controller = new XteTestToolWebServiceAOController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            AccettazioneOnline accettazioneOnlineConfig = XteTestToolConfiguration.getInstance().getAccettazioneOnlineConfigConfig();
            PA config = accettazioneOnlineConfig.getPA();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(preAccettazioneAOView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(preAccettazioneAOView.getPreAccettazionePanelRef().getParent(), preAccettazioneAOView.getPreAccettazionePanelRef(), webserviceview, new Rectangle(0,70,1000,690));


            logger.info("XteTestToolPreAccettazioneAOController chiamaWebService executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPreAccettazioneAOController chiamaWebService failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazioneAOController chiamaWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Accettazione Online controller chiamaWebService failed", ex);
        }
    }
}
