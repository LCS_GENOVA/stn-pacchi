/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.ssip.message;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.bind.JAXBException;
import javax.xml.validation.Schema;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.core.JmsTemplate;

import com.elsagdatamat.dbtrace.bridge.engine.MessageBuilderBase;
import com.elsagdatamat.dbtrace.bridge.engine.bl.IDiscardedTraceManager;
import com.elsagdatamat.framework.resources.ClassPathResource;
import com.selexelsag.dbtrace.bridge.to.ssip.manager.SSIPManagerInterface;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message;
import com.selexelsag.dbtrace.bridge.to.ssip.message.trans.Transformations;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TokenTilltoDateByFiltersList;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesListNextToken;

/**
 * @author arodriguez
 * 
 */
public class SSIPMessageBuilder extends MessageBuilderBase implements SSIPMessageBuilderInterface {

	private static final Log log = LogFactory.getLog(SSIPMessageBuilder.class);
	private IDiscardedTraceManager discardedTraceManager;
	private JmsTemplate jmsTemplate;
	private ClassPathResource xsdPath, queryParams;
	private SSIPManagerInterface ssipManager;
	private long callTimeout = 45;

	private long[] numberOfRuns = new long[]{0,0,0};
	private Transformations transformations;
	
	private Map<String, String> idStatusFilter, labelTracedEntityFilter, whatHappenedFilter, channelFilter;
	
	/**
	 * default constructor
	 */
	public SSIPMessageBuilder() {
		transformations = new Transformations();
		idStatusFilter = new HashMap<String, String>();
		labelTracedEntityFilter = new HashMap<String, String>();
		whatHappenedFilter = new HashMap<String, String>();
		channelFilter = new HashMap<String, String>();
	}

	/**
	 * set discarded traces manager
	 * @param discardedTraceManager the IDiscardedTraceManager to set
	 */
	public void setDiscardedTraceManager(IDiscardedTraceManager discardedTraceManager) {
		this.discardedTraceManager = discardedTraceManager;
	}

	/**
	 * set Spring JMS template for queuing activities
	 * @param jmsTemplate the JmsTemplate to set
	 */
	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	/**
	 * set Spring classpath matching path of XSD schema files
	 * @param xsdPath the ClassPathResource to set
	 */
	public void setXsdPath(ClassPathResource xsdPath) {
		this.xsdPath = xsdPath;
	}

	/**
	 * set Spring classpath matching path of query-params properties file
	 * @param queryParamsPath the ClassPathResource to set
	 */
	public void setQueryParams(ClassPathResource queryParamsPath) {
		this.queryParams = queryParamsPath;
	}

	/**
	 * set the SSIP token manager
	 * @param ssipManager the ssipManager to set
	 */
	public void setSsipManager(SSIPManagerInterface ssipManager) {
		this.ssipManager = ssipManager;
		transformations.setSsipManager(ssipManager);
	}

	
	/**
	 * @return the callTimeout
	 */
	public long getCallTimeout() {
		return callTimeout;
	}

	/**
	 * @param callTimeout the callTimeout to set
	 */
	public void setCallTimeout(long callTimeout) {
		this.callTimeout = callTimeout;
	}

	/**
	 * @return the numberOfRuns
	 */
	@Override
	public long[] getNumberOfRuns() {
		return numberOfRuns;
	}

	/**
	 * @param numberOfRuns the numberOfRuns to set
	 */
	@Override
	public void setNumberOfRuns(long[] numberOfRuns) {
		this.numberOfRuns = numberOfRuns;
	}

	private Map<String, String> getFilterValuesFor(String filterName, MessageBuilderFilterEnum filterType) {
		Map<String, String> result = new HashMap<String, String>();
		Properties paramsProperties = new Properties();
		BufferedInputStream bis;
		try {
			URL url = new URL(queryParams.getURL());
			bis = new BufferedInputStream(url.openStream());
			paramsProperties.load(bis);
			bis.close();
			if (paramsProperties.containsKey(filterName + filterType.value())) {
				String[] values = paramsProperties.getProperty(filterName + filterType.value(),"").split(",");	
				for (String value : values)
					result.put(value, value);
				log.info("Loaded " + values.length + " query params for key " + filterName + " (" + filterType + ")");
			}
		} catch(IOException ex) {
			log.error("Unable to load query parameters for: " + filterName + filterType.value(), ex);
		} finally {
			bis = null;
		}
		return result;
	}
	
	@Override
	public void loadQueryParams() {
		idStatusFilter.clear();
		labelTracedEntityFilter.clear();
		whatHappenedFilter.clear();
		channelFilter.clear();
		idStatusFilter.putAll(getFilterValuesFor("idstatus", MessageBuilderFilterEnum.ALLOWED));
		labelTracedEntityFilter.putAll(getFilterValuesFor("labeltracedentity", MessageBuilderFilterEnum.ALLOWED));
		whatHappenedFilter.putAll(getFilterValuesFor("whathappened", MessageBuilderFilterEnum.ALLOWED));
		channelFilter.putAll(getFilterValuesFor("channel", MessageBuilderFilterEnum.NOTALLOWED));
	}
	
	/**
	 * DBTraceEngine callback, get Traces list from WS
	 * @see com.elsagdatamat.dbtrace.bridge.engine.ITracesReader#getTraces(com.elsagdatamat.trackdbws.client.TrackDBRWServicePortType, java.lang.Object, java.util.Date)
	 */
	@Override
	public TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate) {
		
		
		// total counters increment
		numberOfRuns[MessageBuilderCounterEnum.TotalRuns.index()]++;
		// pass ssipManager reference
		transformations.setSsipManager(ssipManager);
		
		// Prepare working token
		TokenTilltoDateByFiltersList filterCallPrepare = new TokenTilltoDateByFiltersList();
		filterCallPrepare.setTilltoDate(transformations.toXMLGregorian(stopDate));
		filterCallPrepare.setToken(token.getWsToken());

		// Prepare top query params
		TracesList traceEqualList = new TracesList();
		for (String eqParamLabel : labelTracedEntityFilter.keySet()) {
			Trace traceQueryParamsEquals = new Trace();
			traceQueryParamsEquals.setTracedEntity("PACCO");
			traceQueryParamsEquals.setLabelTracedEntity(eqParamLabel);
			traceEqualList.getTraces().add(traceQueryParamsEquals);
		}
		
		TracesList traceNotEqualList = new TracesList();
		for (String notEqChannel : channelFilter.keySet()) {
			Trace traceQueryParamsNotEquals = new Trace();
			traceQueryParamsNotEquals.setChannel(notEqChannel);
			traceNotEqualList.getTraces().add(traceQueryParamsNotEquals);
		}
		
		log.info("Call DBtrace WS with " + (traceEqualList.getTraces().size() + traceNotEqualList.getTraces().size()) + " filter conditions...");
		filterCallPrepare.setTracesFilterByExampleEqual(traceEqualList);
		filterCallPrepare.setTracesFilterByExampleNotEqual(traceNotEqualList);

		// Let's use an ExecutorService to async-decouple the WS call with a global timeout  
		CallableFilteredTraces callableWS = new CallableFilteredTraces(filterCallPrepare, twsp);
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Future<TracesListNextToken> taskWS = executorService.submit(callableWS);
        log.info("Called web service getTracesByFiltersList(), with timeout: " + callTimeout + " sec.");
        
        TracesListNextToken tracesFromTokenCall = null;
        try {
            // ok, wait for 'callTimeout' seconds max
        	tracesFromTokenCall = taskWS.get(callTimeout, TimeUnit.SECONDS);
        } catch (ExecutionException e) {
			// ko counter increment
			numberOfRuns[MessageBuilderCounterEnum.FailedRuns.index()]++;
        	log.error("Execution failed during WS calling ... ", e);
        } catch (TimeoutException e) {
			// timeout counter increment
			numberOfRuns[MessageBuilderCounterEnum.TimeoutRuns.index()]++;
        	log.warn("WS call timeout expired; abort ", e);
        } catch (InterruptedException e) {
    		// cancel, so total counter decrement
    		numberOfRuns[MessageBuilderCounterEnum.TotalRuns.index()]--;
        	log.warn("WS call interrupted, abort ", e);
        }

		TracesList tracesList = filterTracesBySourceAndStatus(tracesFromTokenCall.getTraceList(), whatHappenedFilter, idStatusFilter);

		// Update working token WsToken content
		token.setWsToken(tracesFromTokenCall.getNextToken());
		
		if (tracesList == null || tracesList.getTraces() == null) {
			if (taskWS.isDone())
				log.info("It seems that there was nothing for us in DB ...");
			return null;
		}

		// ok counters increment
		numberOfRuns[MessageBuilderCounterEnum.SuccessRuns.index()]++;

		log.info("Retrieved " + tracesList.getTraces().size() + " traces from DBtrace");
		return tracesList;
	}

	/**
	 * filter results by source and idStatus
	 * @param traceList  TracesList to be filtered
	 * @param filter  Map of filter keys
	 * @return  filtered TracesList
	 */
	private TracesList filterTracesBySourceAndStatus(TracesList traceList, Map<String, String> filterSource, Map<String, String> filterStatus) {
		if (traceList == null || traceList.getTraces() == null) 
			return traceList; 
		
		TracesList result = new TracesList();
		log.debug("Filtering... ");
		for (Trace trace : traceList.getTraces()) {
			String whatHappened = trace.getWhatHappened();
			String idStatus = transformations.tryToFindIdStatusIfNull(trace, transformations.loadDetailValues(trace));
			log.debug(" >> WHAT_HAPPENED: " + whatHappened + ", ID_STATUS: " + idStatus);
			if (whatHappened.equalsIgnoreCase("ACC_NSP")) {
				result.getTraces().add(trace);
			} else if (filterSource.containsKey(whatHappened) && filterStatus.containsKey(idStatus)) {
				result.getTraces().add(trace);
			}
		}
		log.info("Filtered " + traceList.getTraces().size() + " traces from DBtrace, in " + result.getTraces().size() + " valid traces");
		return result;
	}

	/**
	 * DBTraceEngine callback, send messages to destination
	 * @see com.elsagdatamat.dbtrace.bridge.engine.IMessageSender#send(java.util.List)
	 */
	@Override
	public void send(List<String> messageBodies) throws IOException {
		log.info(messageBodies.size() + " messages ready to send.");
		for (final String str : messageBodies) {
			try {
				jmsTemplate.send(new TPAMessageCreator(str));
				log.info("Message queued.");
				log.debug("Content:\n" + str);
			} catch (Exception jmsEx) {
				log.error("Unable to queue message ", jmsEx);
				throw new IOException(jmsEx);
			}
		}
	}

	/**
	 * DBTraceEngine callback, create the message body list from traces
	 * @see com.elsagdatamat.dbtrace.bridge.engine.IMessageBuilder#create(com.elsagdatamat.trackdbws.client.TracesList, java.lang.Object)
	 * @param input  the TracesList input from WS
	 * @param token  the working token acquired
	 * @return List<String> of XML formatted messages
	 */
	@Override
	public List<String> create(TracesList input) {
		List<String> msgBodiesList = new ArrayList<String>();
		
		Schema schema = null;
		try {
			schema = transformations.loadSchemaFrom(xsdPath.getURL());
		} catch (Exception schemaEx) {
			log.warn("Unable to load XML validation Schema; format message without it");
		}
		
		// sort and group Traces by channel value
		SortedMap<String, TracesList> sortedTraces = groupTracesByChannel(input);
		
		String channelKey = null;
		try {
			log.info("Creating message bodies ...");
			Iterator<String> keys = sortedTraces.keySet().iterator();
			log.info("Found " + sortedTraces.size() + " destination match: " + sortedTraces.keySet());
			while (keys.hasNext()) {
				channelKey = keys.next();
				String xmlMessageBody = xmlMessageFromTraces(channelKey, sortedTraces.get(channelKey), schema);
				msgBodiesList.add(xmlMessageBody);
				log.debug("Message body created:\n " + xmlMessageBody);
				if (saveTraces)
					checkSaveTraces(xmlMessageBody, null);
			}
			log.info(sortedTraces.size() + " Message bodies successfully created.");
		} catch (JAXBException ex) {
			log.error("Errors creating XML message bodies ", ex);
			try {
				checkSaveTraces(xmlMessageFromTraces(channelKey, input, null), ex);
			} catch (JAXBException rex) {
				log.error("Errors trying to recovery XML message body from error (" + ex.getMessage() + ")", rex);
			}
		}
		return msgBodiesList;
	}

	protected SortedMap<String, TracesList> groupTracesByChannel(TracesList input) {
		SortedMap<String, TracesList> sortedResult = 
			Collections.synchronizedSortedMap(Collections.checkedSortedMap(new TreeMap<String, TracesList>(),	String.class, TracesList.class));
		List<Trace> ungroupedTraces = input.getTraces();
		for (Trace responseTrace : ungroupedTraces) {
			String key = responseTrace.getChannel();
			if (!sortedResult.containsKey(key)) {
				sortedResult.put(key, new TracesList());
			}
			sortedResult.get(key).getTraces().add(responseTrace);
		}
		return sortedResult;
	}

	private String xmlMessageFromTraces(String channelGroupKey, TracesList input, Schema schema) throws JAXBException {
		Message jaxbMessage = transformations.traceListToMessage(channelGroupKey, input.getTraces());
		return transformations.marshallMessageToXml(jaxbMessage, schema);
	}

	private void checkSaveTraces(String xmlMessage, Exception marshallEx) {
		discardedTraceManager.saveDiscardedTraces(token.getServiceId(), xmlMessage, marshallEx);
	}
	
	/**
	 * send test messages to destination queue
	 */
	@Override
	public void sendTestMessage() {
		try {
			jmsTemplate.send(new TPATestMessageCreator(TPATestMessageCreator.EMPTY_MESSAGE_BODY));
			log.info("Test message successfull queued");
		} catch (Exception jmsEx) {
			log.error("Unable to queue test message ", jmsEx);
		}
	}
	
	/**
	 * implements a Callable statement for async WS call
	 * @author arodriguez
	 */
	class CallableFilteredTraces implements Callable<TracesListNextToken> {

		private TokenTilltoDateByFiltersList filterList;
		private TrackDBRWServicePortType trackDBws;
	
		/**
		 * @param filterList
		 * @param trackDBws
		 */
		public CallableFilteredTraces(TokenTilltoDateByFiltersList filterList, TrackDBRWServicePortType trackDBws) {
			this.filterList = filterList;
			this.trackDBws = trackDBws;
		}

		/**
		 * call WS
		 */
		@Override
		public TracesListNextToken call() throws Exception {
			return trackDBws.getTracesByFiltersList(filterList);
		}
	}
}