package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.Token;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface ITokenDAO extends IGenericDAO<Token, String>{
}
