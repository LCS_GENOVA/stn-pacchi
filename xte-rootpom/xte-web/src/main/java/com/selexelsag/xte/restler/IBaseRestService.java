package com.selexelsag.xte.restler;



/**
 * Interfaccia comune a tutti i servizi rest
 * Al momento ha solo il contesto base per distinguere una richiesta destinata ad essere consumata ad un servizio rest.
 * In futuro, se ci sono diverse operazioni comuni a tutti i servizi rest, si avra' una classe astratta di base padre di tutti i servizi rest che implementa questa interfaccia. 
 * @author Frosi
 *
 */

public interface IBaseRestService {
	  public static final String REST_CONTEXT = "/rest";
	  
}
