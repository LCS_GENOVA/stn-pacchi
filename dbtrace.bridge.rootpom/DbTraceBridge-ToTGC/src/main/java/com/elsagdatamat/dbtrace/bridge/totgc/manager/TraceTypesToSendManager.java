package com.elsagdatamat.dbtrace.bridge.totgc.manager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elsagdatamat.dbtrace.bridge.totgc.dao.ITraceTypesToSendDAO;
import com.elsagdatamat.dbtrace.bridge.totgc.entities.TraceTypesToSend;


@Service
public class TraceTypesToSendManager implements ITraceTypesToSendManager {

    private ITraceTypesToSendDAO traceTypesToSendDAO;

    public void setTraceTypesToSendDAO(ITraceTypesToSendDAO traceTypesToSendDAO) {
		this.traceTypesToSendDAO = traceTypesToSendDAO;
	}

	@Override
    public List<TraceTypesToSend> findByDestinationTracedEntityChannel(String destination, String tracedEntity, String channel) 
    {
		TraceTypesToSend example=new TraceTypesToSend();
		example.setChannel(channel);
		example.setDestination(destination);
		example.setTracedEntity(tracedEntity);
		return traceTypesToSendDAO.findByExample(example);
    }
}
