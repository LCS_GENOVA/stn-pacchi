#created on: 2-mag-2012
package com.selexelsag.xte.business

#list any import classes here.
import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.model.track.respcreation.XPathQueryCallResult;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.business.utility.XmlUtils;
import com.selexelsag.xte.service.system.IAppConfig;
import java.util.HashMap;


#declare any global variables here

rule "COMMON_SETTINGS"
    when 
    $req: TrackRequest()
    $flowParam: FlowParams() 
    $basePathKey: HashMap()
    then
        #parametri di ricerca per il recupero dei dati del servizio remoto
        HashMap<String, String> conParam = new HashMap<String, String>();
            conParam.put(FlowParams.REQ_PROVIDER_SOURCE, $req.getService().toString());
            conParam.put(FlowParams.REQ_PROVIDER_KEY_1, $req.getService().toString());
            conParam.put(FlowParams.REQ_PROVIDER_KEY_2, $req.getChannel().toString());
            conParam.put(FlowParams.REQ_PROVIDER_KEY_3, $req.getEventName().toString());
            conParam.put(FlowParams.REQ_PROVIDER_KEY_4, $req.getVersion().toString());
            conParam.put(FlowParams.REQ_PROVIDER_KEY_5, $req.getDestinationOfficeId().toString());
        $flowParam.setServiceRequestProviderParameters(conParam);

        #xsd di validazione da percorso standard
        String syncValidationBasePathKey = (String)$basePathKey.get(IAppConfig.BPM_SYNC_VALIDATION_BASE_PATH_KEY);
        String pathFile = XmlUtils.composePathFile($req, syncValidationBasePathKey);
        $flowParam.setRequestValidationSchema(pathFile);
        
        #xslt di transformazione da percorso standard
        String syncTransformationBasePathKey = (String)$basePathKey.get(IAppConfig.BPM_SYNC_TRANSFORMATION_BASE_PATH_KEY);
        String pathFileXslt = XmlUtils.composePathFileXsltByService($req, syncTransformationBasePathKey);
        $flowParam.setRequestTrasformationModel(pathFileXslt);
        
        #processamento risposta
        $flowParam.setQueriesCallResult(new XPathQueryCallResult(
            XPathQueryCallResult.XPATH_ESITO, 
            XPathQueryCallResult.XPATH_DESC_ESITO, 
            XPathQueryCallResult.XPATH_CODICE_ESITO, 
            XPathQueryCallResult.XPATH_REITERABILITA));
end


rule "FID_PR_TP_AC"

    when 
    $req: TrackRequest(service.toString().equals("PR"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("AC"))
    $flowParam: FlowParams() 
    then
        #flowId richiesto per identificare univocamente il flusso
        $flowParam.setFlowId("FID_PR_TP_AC");
        $flowParam.setDoTransformation(true);
        $flowParam.setRequestValidationProperties(null);
end

rule "FID_PR_TP_AM"

    when
    $req: TrackRequest(service.toString().equals("PR"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("AM"))
    $flowParam: FlowParams() 
    then
        $flowParam.setFlowId("FID_PR_TP_AM");
        $flowParam.setDoTransformation(true);
        $flowParam.setRequestValidationProperties(null);
    end
rule "FID_PR_TP_LO"

    when
    $req: TrackRequest(service.toString().equals("PR"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("LO"))
    $flowParam: FlowParams()
    then
        $flowParam.setFlowId("FID_PR_TP_LO");      
        $flowParam.setDoTransformation(true);
        $flowParam.setRequestValidationProperties(null);
    end
rule "FID_PR_TP_LM"

    when
    $req: TrackRequest(service.toString().equals("PR"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("LM"))
    $flowParam: FlowParams() 
    then
        $flowParam.setFlowId("FID_PR_TP_LM");
        $flowParam.setDoTransformation(true);
        $flowParam.setRequestValidationProperties(null);
    end
rule "FID_PR_TP_CM"

    when
    $req: TrackRequest(service.toString().equals("PR"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("CM"))
    $flowParam: FlowParams() 
    then
        $flowParam.setFlowId("FID_PR_TP_CM");
        $flowParam.setDoTransformation(true);
        $flowParam.setRequestValidationProperties(null);
    end
rule "FID_PR_TP_NM"

    when
    $req: TrackRequest(service.toString().equals("PR"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("NM"))
    $flowParam: FlowParams() 
    then
        $flowParam.setFlowId("FID_PR_TP_NM");
        $flowParam.setDoTransformation(true);
        $flowParam.setRequestValidationProperties(null);
    end
rule "FID_PR_TP_SP"

    when
    $req: TrackRequest(service.toString().equals("PR"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("SP"))
    $flowParam: FlowParams() 
    then
        $flowParam.setFlowId("FID_PR_TP_SP");
        $flowParam.setDoTransformation(true);
        $flowParam.setRequestValidationProperties(null);
    end
rule "FID_PR_AO_AC"

    when
    $req: TrackRequest(service.toString().equals("PR"),
        channel.toString().equals("AO"), 
        eventName.toString().equals("AC"))
    $flowParam: FlowParams() 
    then
        $flowParam.setFlowId("FID_PR_AO_AC");
        $flowParam.setDoTransformation(true);
        $flowParam.setRequestValidationProperties(null);
    end
rule "FID_PR_AO_PA"

    when
    $req: TrackRequest(service.toString().equals("PR"),
        channel.toString().equals("AO"), 
        eventName.toString().equals("PA"))
    $flowParam: FlowParams() 
    $basePathKey: HashMap()
    then
        $flowParam.setFlowId("FID_PR_AO_PA");
        $flowParam.setDoTransformation(true);

        #Imposta officeId
        $flowParam.setOffRefID($req.getDestinationOfficeId().toString());
        
        #campi di controllo semantico da percorso standard
        String syncValidationBasePathKey = (String)$basePathKey.get(IAppConfig.BPM_SYNC_VALIDATION_BASE_PATH_KEY);
        String pathFile = XmlUtils.composePathFileSemanticProperties($req, syncValidationBasePathKey);
        $flowParam.setRequestValidationProperties(pathFile);
    end

rule "FID_PR_AO_AP"

    when
    $req: TrackRequest(service.toString().equals("PR"),
        channel.toString().equals("AO"), 
        eventName.toString().equals("AP"))
    $flowParam: FlowParams() 
    $basePathKey: HashMap()
    then
        $flowParam.setFlowId("FID_PR_AO_AP");
        $flowParam.setDoTransformation(false);

        #Imposta officeId
        $flowParam.setOffRefID($req.getDestinationOfficeId().toString());
        
        #campi di controllo semantico da percorso standard
        $flowParam.setRequestValidationProperties(null);
    end

rule "FID_DP_TP_AM"

    when
    $req: TrackRequest(service.toString().equals("DP"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("AM"))
    $flowParam: FlowParams() 
    then
        $flowParam.setFlowId("FID_DP_TP_AM");
        $flowParam.setDoTransformation(false);
        HashMap<String, String> paramTransf = new HashMap<String, String>();       
        paramTransf.put("channel",$req.getChannel().toString());
        paramTransf.put("eventName",$req.getEventName().toString());
        paramTransf.put("serviceName",$req.getService().toString());
        paramTransf.put("version",$req.getVersion().toString());
        paramTransf.put("channelID",$req.getDestinationOfficeId().toString());
        $flowParam.setParamTransf(paramTransf);
        
        $flowParam.setRequestValidationProperties(null);
end
rule "FID_DP_TP_PR"

    when
    $req: TrackRequest(service.toString().equals("DP"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("PR"))
    $flowParam: FlowParams() 
    then
        $flowParam.setFlowId("FID_DP_TP_PR");
        $flowParam.setDoTransformation(false);
        HashMap<String, String> paramTransf = new HashMap<String, String>();
        paramTransf.put("channel",$req.getChannel().toString());
        paramTransf.put("eventName",$req.getEventName().toString());
        paramTransf.put("serviceName",$req.getService().toString());
        paramTransf.put("version",$req.getVersion().toString());
        paramTransf.put("channelID",$req.getDestinationOfficeId().toString());

        $flowParam.setParamTransf(paramTransf);
        
        $flowParam.setRequestValidationProperties(null);
end
rule "FID_DP_TP_LM"

    when
    $req: TrackRequest(service.toString().equals("DP"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("LM"))
    $flowParam: FlowParams() 
    then
        $flowParam.setFlowId("FID_DP_TP_LM");
        $flowParam.setDoTransformation(false);
        HashMap<String, String> paramTransf = new HashMap<String, String>();
        paramTransf.put("channel",$req.getChannel().toString());
        paramTransf.put("eventName",$req.getEventName().toString());
        paramTransf.put("serviceName",$req.getService().toString());
        paramTransf.put("version",$req.getVersion().toString());
        paramTransf.put("channelID",$req.getDestinationOfficeId().toString());
        $flowParam.setParamTransf(paramTransf);
        
        $flowParam.setRequestValidationProperties(null);
end
rule "FID_DP_TP_NM"

    when
    $req: TrackRequest(service.toString().equals("DP"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("NM"))
    $flowParam: FlowParams() 
    then
        $flowParam.setFlowId("FID_DP_TP_NM");
        $flowParam.setDoTransformation(false);
        HashMap<String, String> paramTransf = new HashMap<String, String>();
        paramTransf.put("channel",$req.getChannel().toString());
        paramTransf.put("eventName",$req.getEventName().toString());
        paramTransf.put("serviceName",$req.getService().toString());
        paramTransf.put("version",$req.getVersion().toString());
        paramTransf.put("channelID",$req.getDestinationOfficeId().toString());
        $flowParam.setParamTransf(paramTransf);
        
        $flowParam.setRequestValidationProperties(null);
end
rule "FID_DP_TP_SM"

    when
    $req: TrackRequest(service.toString().equals("DP"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("SM"))
    $flowParam: FlowParams()
    then
        $flowParam.setFlowId("FID_DP_TP_SM");
        $flowParam.setDoTransformation(false);
        HashMap<String, String> paramTransf = new HashMap<String, String>();
        paramTransf.put("channel",$req.getChannel().toString());
        paramTransf.put("eventName",$req.getEventName().toString());
        paramTransf.put("serviceName",$req.getService().toString());
        paramTransf.put("version",$req.getVersion().toString());
        paramTransf.put("channelID",$req.getDestinationOfficeId().toString());
        $flowParam.setParamTransf(paramTransf);
        
        $flowParam.setRequestValidationProperties(null);
end
rule "FID_DP_TP_AO"

    when
    $req: TrackRequest(service.toString().equals("DP"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("AO"))
    $flowParam: FlowParams()
    then
        $flowParam.setFlowId("FID_DP_TP_AO");
        $flowParam.setDoTransformation(false);
        HashMap<String, String> paramTransf = new HashMap<String, String>();
        paramTransf.put("channel",$req.getChannel().toString());
        paramTransf.put("eventName",$req.getEventName().toString());
        paramTransf.put("serviceName",$req.getService().toString());
        paramTransf.put("version",$req.getVersion().toString());
        paramTransf.put("channelID",$req.getDestinationOfficeId().toString());
        $flowParam.setParamTransf(paramTransf);
        
        $flowParam.setRequestValidationProperties(null);
end
rule "FID_PR_TP_PI"

    when
    $req: TrackRequest(service.toString().equals("PR"),
        channel.toString().equals("TP"), 
        eventName.toString().equals("PI"))
    $flowParam: FlowParams() 
    then
        $flowParam.setFlowId("FID_PR_TP_PI");
        $flowParam.setDoTransformation(false);
        $flowParam.setRequestValidationProperties(null);
end




rule "FID_DP_EM_AO"

    when
    $req: TrackRequest(service.toString().equals("DP"),
        channel.toString().equals("EM"), 
        eventName.toString().equals("AO"))
    $flowParam: FlowParams()
    then
        $flowParam.setFlowId("FID_DP_EM_AO");
        $flowParam.setDoTransformation(false);
        HashMap<String, String> paramTransf = new HashMap<String, String>();
        paramTransf.put("channel",$req.getChannel().toString());
        paramTransf.put("eventName",$req.getEventName().toString());
        paramTransf.put("serviceName",$req.getService().toString());
        paramTransf.put("version",$req.getVersion().toString());
        paramTransf.put("channelID",$req.getDestinationOfficeId().toString());
        $flowParam.setParamTransf(paramTransf);
        
        $flowParam.setRequestValidationProperties(null);
end
rule "FID_DP_EM_AM"

    when
    $req: TrackRequest(service.toString().equals("DP"),
        channel.toString().equals("EM"), 
        eventName.toString().equals("AM"))
    $flowParam: FlowParams()
    then
        $flowParam.setFlowId("FID_DP_EM_AM");
        $flowParam.setDoTransformation(false);
        HashMap<String, String> paramTransf = new HashMap<String, String>();
        paramTransf.put("channel",$req.getChannel().toString());
        paramTransf.put("eventName",$req.getEventName().toString());
        paramTransf.put("serviceName",$req.getService().toString());
        paramTransf.put("version",$req.getVersion().toString());
        paramTransf.put("channelID",$req.getDestinationOfficeId().toString());
        $flowParam.setParamTransf(paramTransf);
        
        $flowParam.setRequestValidationProperties(null);
end
rule "FID_DP_EM_LM"

    when
    $req: TrackRequest(service.toString().equals("DP"),
        channel.toString().equals("EM"), 
        eventName.toString().equals("LM"))
    $flowParam: FlowParams()
    then
        $flowParam.setFlowId("FID_DP_EM_LM");
        $flowParam.setDoTransformation(false);
        HashMap<String, String> paramTransf = new HashMap<String, String>();
        paramTransf.put("channel",$req.getChannel().toString());
        paramTransf.put("eventName",$req.getEventName().toString());
        paramTransf.put("serviceName",$req.getService().toString());
        paramTransf.put("version",$req.getVersion().toString());
        paramTransf.put("channelID",$req.getDestinationOfficeId().toString());
        $flowParam.setParamTransf(paramTransf);
        
        $flowParam.setRequestValidationProperties(null);
end
rule "FID_DP_EM_SM"

    when
    $req: TrackRequest(service.toString().equals("DP"),
        channel.toString().equals("EM"), 
        eventName.toString().equals("SM"))
    $flowParam: FlowParams()
    then
        $flowParam.setFlowId("FID_DP_EM_SM");
        $flowParam.setDoTransformation(false);
        HashMap<String, String> paramTransf = new HashMap<String, String>();
        paramTransf.put("channel",$req.getChannel().toString());
        paramTransf.put("eventName",$req.getEventName().toString());
        paramTransf.put("serviceName",$req.getService().toString());
        paramTransf.put("version",$req.getVersion().toString());
        paramTransf.put("channelID",$req.getDestinationOfficeId().toString());
        $flowParam.setParamTransf(paramTransf);
        
        $flowParam.setRequestValidationProperties(null);
end
rule "FID_DP_EM_NM"

    when
    $req: TrackRequest(service.toString().equals("DP"),
        channel.toString().equals("EM"), 
        eventName.toString().equals("NM"))
    $flowParam: FlowParams()
    then
        $flowParam.setFlowId("FID_DP_EM_NM");
        $flowParam.setDoTransformation(false);
        HashMap<String, String> paramTransf = new HashMap<String, String>();
        paramTransf.put("channel",$req.getChannel().toString());
        paramTransf.put("eventName",$req.getEventName().toString());
        paramTransf.put("serviceName",$req.getService().toString());
        paramTransf.put("version",$req.getVersion().toString());
        paramTransf.put("channelID",$req.getDestinationOfficeId().toString());
        $flowParam.setParamTransf(paramTransf);
        
        $flowParam.setRequestValidationProperties(null);
end
rule "FID_DP_TP_RM"

   when
   $req: TrackRequest(service.toString().equals("DP"),
       channel.toString().equals("TP"), 
       eventName.toString().equals("RM"))
   $flowParam: FlowParams()
   then
       $flowParam.setFlowId("FID_DP_TP_RM");
       $flowParam.setDoTransformation(false);
       HashMap<String, String> paramTransf = new HashMap<String, String>();
       paramTransf.put("channel",$req.getChannel().toString());
       paramTransf.put("eventName",$req.getEventName().toString());
       paramTransf.put("serviceName",$req.getService().toString());
       paramTransf.put("version",$req.getVersion().toString());
       paramTransf.put("channelID",$req.getDestinationOfficeId().toString());
       $flowParam.setParamTransf(paramTransf);
       
       $flowParam.setRequestValidationProperties(null);
end
rule "FID_DP_EM_RM"

   when
   $req: TrackRequest(service.toString().equals("DP"),
       channel.toString().equals("EM"), 
       eventName.toString().equals("RM"))
   $flowParam: FlowParams()
   then
       $flowParam.setFlowId("FID_DP_EM_RM");
       $flowParam.setDoTransformation(false);
       HashMap<String, String> paramTransf = new HashMap<String, String>();
       paramTransf.put("channel",$req.getChannel().toString());
       paramTransf.put("eventName",$req.getEventName().toString());
       paramTransf.put("serviceName",$req.getService().toString());
       paramTransf.put("version",$req.getVersion().toString());
       paramTransf.put("channelID",$req.getDestinationOfficeId().toString());
       $flowParam.setParamTransf(paramTransf);
       
       $flowParam.setRequestValidationProperties(null);
end
