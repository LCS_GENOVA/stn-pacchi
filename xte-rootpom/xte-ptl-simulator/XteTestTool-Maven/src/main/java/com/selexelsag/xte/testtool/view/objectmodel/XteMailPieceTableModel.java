/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectmodel;

 
import com.selexelsag.xte.testtool.datamodel.XteMailPiece;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Da Procida
 */
public class XteMailPieceTableModel extends DefaultTableModel {

    private String[] columnNames = new String[]{
        ResourceBundle.getBundle("bundles/messages").getString("mailpiece.table.header.tipo"),
        ResourceBundle.getBundle("bundles/messages").getString("mailpiece.table.header.codice"),
        ResourceBundle.getBundle("bundles/messages").getString("mailpiece.table.header.causale"),
        ResourceBundle.getBundle("bundles/messages").getString("mailpiece.table.header.dataora"),
        ResourceBundle.getBundle("bundles/messages").getString("mailpiece.table.header.latitudine"),
        ResourceBundle.getBundle("bundles/messages").getString("mailpiece.table.header.longitudine"),
        ResourceBundle.getBundle("bundles/messages").getString("mailpiece.table.header.gpdcoords"),
        ResourceBundle.getBundle("bundles/messages").getString("mailpiece.table.header.ufficio"),
    };

    private Map<String, XteMailPiece> map = new HashMap<String, XteMailPiece>();
    
    public XteMailPieceTableModel() {

        super();
        this.setColumnCount(columnNames.length);
        this.setColumnIdentifiers(columnNames);

    }

    public void addMailPieceItem(XteMailPiece item) throws XteTestToolDuplicateKeyException{
        if(map.containsKey(item.getMailPieceCode()))
        {
            throw new XteTestToolDuplicateKeyException(Group.GENERIC, Severity.WARNING, "Mail Piece Table Model duplicate key exception");
        }
        else
        {
            addRow(new Object[]{item.getMailPieceType(), item.getMailPieceCode(), item.getMailPieceProduct(), item.getMailPieceDateTime(), item.getMailPieceLatitude(), item.getMailPieceLongitude(), item.getMailGPS(), item.getMailPieceOffice()});
            map.put(item.getMailPieceCode(), item);            
        }        
    }
    

    public void setModelData(Map<String, XteMailPiece> items) {
        getDataVector().clear();
        map.clear();                
        for (XteMailPiece item : items.values()) {
            addRow(new Object[]{item.getMailPieceType(), item.getMailPieceCode(), item.getMailPieceProduct(), item.getMailPieceDateTime(), item.getMailPieceLatitude(), item.getMailPieceLongitude(), item.getMailGPS(), item.getMailPieceOffice()});
            map.put(item.getMailPieceCode(), item);
        }
    }

    public XteMailPiece getSelectedItem(int rowIndex) {
        
        String mailPieceCode = (String) getValueAt(rowIndex, 0);
        if(map.containsKey(mailPieceCode))
        {
            return map.get(mailPieceCode);
        }
        else
        {
            return null;
        }
    }

    @Override
    public void removeRow(int row) {
        String mailPieceCode = (String) getValueAt(row, 1);
        if(map.containsKey(mailPieceCode))
        {
            map.remove(mailPieceCode);
        }        
        getDataVector().removeElementAt(row);
        fireTableDataChanged();
    }

    public void clearModel() {
        getDataVector().clear();
        map.clear();
    }

    public Map<String, XteMailPiece> getMap() {
        return map;
    }

    
    @Override
    public boolean isCellEditable(int row, int column) {
        return (false);
    }
    
}
