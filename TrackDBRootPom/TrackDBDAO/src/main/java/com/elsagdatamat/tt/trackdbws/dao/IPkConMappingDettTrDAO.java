package com.elsagdatamat.tt.trackdbws.dao;


import com.elsagdatamat.framework.dao.IGenericDAO;
import com.selexelsag.tt.cruscottopacchi.bean.PkConMappingDettTr;
import com.selexelsag.tt.cruscottopacchi.bean.PkConMappingDettTrPK;
/** 
* R.L. BeanGenerator - 27.05.2012 18:17:43
* R.L. - Inseriti per il nuovo metodo del trackDbWS - packDataRetrieve
*/ 
public interface IPkConMappingDettTrDAO extends IGenericDAO<PkConMappingDettTr,PkConMappingDettTrPK>{

}