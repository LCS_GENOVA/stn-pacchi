/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.pr;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XteMailPiece;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;
import com.selexelsag.xte.testtool.view.pr.XteTestToolAcquisizioneMazzettiPRView;
import com.selexelsag.xte.testtool.view.pr.XteTestToolWebServicePRView;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata.AM;
import com.selexelsag.xtetesttool.xml.postaregistrata.am.ObjectFactory;
import com.selexelsag.xtetesttool.xml.postaregistrata.am.Traccia;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolAcquisizioneMazzettiPRController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener {

    private XteTestToolAcquisizioneMazzettiPRView acquisizioneMazzettiPRView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolAcquisizioneMazzettiPRController.class);
    private XteMailPiece selectedMailPiece = null;
    private int currentSelectedRow = -1;
    private List<JTextComponent> mailPieceFieldList = new ArrayList<JTextComponent>();
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    
    private JAXBContext context;
    
    private static Traccia acquisizioneMazzettiTraccia;
    
    static {
        logger.debug("Creating Config Object Factory and Traccia");
        PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        AM config = postaRegistrataConfig.getAM();

        acquisizioneMazzettiTraccia = objectFactory.createTraccia();
        acquisizioneMazzettiTraccia.setEvento(config.getEventName());
    }
  

    public XteTestToolAcquisizioneMazzettiPRController(JPanel acquisizioneMazzettiPRView) {
        try {
            logger.info("XteTestToolAcquisizioneMazzettiPRController start class creation");
            this.acquisizioneMazzettiPRView = (XteTestToolAcquisizioneMazzettiPRView) acquisizioneMazzettiPRView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.acquisizioneMazzettiPRView.initializeView();

            updateView();

            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.postaregistrata.am");
            
            logger.info("XteTestToolAcquisizioneMazzettiPRController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneMazzettiPRController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolAcquisizioneMazzettiPRController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller creation failed", ex);
        }

    }
    
    private void updateDataModel() {
        String id = acquisizioneMazzettiPRView.getBundleIdTextRef().getText();
        String opid = acquisizioneMazzettiPRView.getBundleTPIdTextRef().getText();
        logger.debug("updateDataModel - " + id + "," + opid);
                acquisizioneMazzettiTraccia.setCodiceIdentificativo(new String(id));
        acquisizioneMazzettiTraccia.setIdOperatoreTP(new String(opid));
    }
    
    
    private void updateView() {  
        String id = acquisizioneMazzettiTraccia.getCodiceIdentificativo();
        String opid = acquisizioneMazzettiTraccia.getIdOperatoreTP();
        logger.debug("updateView - " + id + "," + opid);
                
        if (id != null) {
            acquisizioneMazzettiPRView.getBundleIdTextRef()
                    .setText(id);
        }

        if (opid != null) {
            acquisizioneMazzettiPRView.getBundleTPIdTextRef()
                    .setText(opid);
        }

    }
        
    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = acquisizioneMazzettiPRView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        acquisizioneMazzettiPRView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.acquisizioneMazzettiPRView.getAcquisizioneMazzettiPanelRef().addAncestorListener(this);        
        this.acquisizioneMazzettiPRView.getBundleIdTextRef().addFocusListener(this);
        this.acquisizioneMazzettiPRView.getBundleIdTextRef().getDocument().addDocumentListener(this);
        this.acquisizioneMazzettiPRView.getBundleTPIdTextRef().addFocusListener(this);
        this.acquisizioneMazzettiPRView.getBundleTPIdTextRef().getDocument().addDocumentListener(this);
        this.acquisizioneMazzettiPRView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.acquisizioneMazzettiPRView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.acquisizioneMazzettiPRView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.acquisizioneMazzettiPRView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.acquisizioneMazzettiPRView.getBackButtonRef().addActionListener(this);
        this.acquisizioneMazzettiPRView.getHomeButtonRef().addActionListener(this);
        this.acquisizioneMazzettiPRView.getExitButtonRef().addActionListener(this);   

        this.salvaXmlFileChooser.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(acquisizioneMazzettiPRView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke generaXmlData");

            updateDataModel();

            generaXmlData();
        }

        if(source.equals(acquisizioneMazzettiPRView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke impostaDatiWebService");

            updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(acquisizioneMazzettiPRView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke salvaXmlData");

            updateDataModel();

            salvaXmlData();
        }

        if(source.equals(acquisizioneMazzettiPRView.getBackButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke back");

            updateDataModel();

            back();
        }

        if(source.equals(acquisizioneMazzettiPRView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke home");

            updateDataModel();

            home();
        }

        if(source.equals(acquisizioneMazzettiPRView.getExitButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke exit");

            updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(acquisizioneMazzettiPRView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolAcquisizioneMazzettiPRView)
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneMazzettiPRController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneMazzettiPRController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        // updateDataModel();
        /*
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        logger.debug("id: " + acquisizioneMazzettiPRView.getBundleIdTextRef().getText());
        logger.debug("tpid: " + acquisizioneMazzettiPRView.getBundleTPIdTextRef().getText());
        logger.debug("changedUpdate stacktrace:\n");
        for (int i=0;i<stackTrace.length;i++) {
            logger.debug("\t" + stackTrace[i]);
        }
        * */
    }    
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        acquisizioneMazzettiPRView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolAcquisizioneMazzettiPRController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.acquisizioneMazzettiPRView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(acquisizioneMazzettiPRView);
            updateXteTestToolDataModel(dataModel);

            clearMessageBar();

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(acquisizioneMazzettiPRView.getImpostaDatiWSButtonRef());
            disabledComponents.add(acquisizioneMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            acquisizioneMazzettiPRView.getBundleIdTextRef().requestFocus();
            
            logger.info("XteTestToolAcquisizioneMazzettiPRController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneMazzettiPRController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneMazzettiPRController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller init failed", ex);
        }

    }
    
    private void exit()
    {
        logger.info("XteTestToolAcquisizioneMazzettiPRController start exit");
        
        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(acquisizioneMazzettiPRView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController exit in progress");
            acquisizioneMazzettiPRView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolAcquisizioneMazzettiPRController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(acquisizioneMazzettiPRView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(acquisizioneMazzettiPRView.getAcquisizioneMazzettiPanelRef().getParent(), acquisizioneMazzettiPRView.getAcquisizioneMazzettiPanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolAcquisizioneMazzettiPRController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(acquisizioneMazzettiPRView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(acquisizioneMazzettiPRView.getAcquisizioneMazzettiPanelRef().getParent(), acquisizioneMazzettiPRView.getAcquisizioneMazzettiPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }
    
    private void generaXmlData()
    {
        logger.info("XteTestToolAcquisizioneMazzettiPRController start generaXmlData");

        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();
        
        acquisizioneMazzettiTraccia.setCodiceIdentificativo(acquisizioneMazzettiPRView.getBundleIdTextRef().getText());
        acquisizioneMazzettiTraccia.setIdOperatoreTP(acquisizioneMazzettiPRView.getBundleTPIdTextRef().getText());
        
        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            java.util.logging.Logger.getLogger(XteTestToolAcquisizioneMazzettiPRController.class.getName()).log(Level.SEVERE, null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            java.util.logging.Logger.getLogger(XteTestToolAcquisizioneMazzettiPRController.class.getName()).log(Level.SEVERE, null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(acquisizioneMazzettiTraccia, out);
        } catch (JAXBException ex) {
            java.util.logging.Logger.getLogger(XteTestToolAcquisizioneMazzettiPRController.class.getName()).log(Level.SEVERE, null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
            
        try
        {            
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString());
            acquisizioneMazzettiPRView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("XteTestToolAcquisizioneMazzettiPRController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneMazzettiPRController generaXmlData failed", baseEx);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(baseEx.toString(), MessageType.ERROR);
            acquisizioneMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneMazzettiPRController generaXmlData failed", ex);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        acquisizioneMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(acquisizioneMazzettiPRView.getImpostaDatiWSButtonRef());
        enableComponent.add(acquisizioneMazzettiPRView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolAcquisizioneMazzettiPRController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServicePRView(acquisizioneMazzettiPRView.getParentFrame());
            XteTestToolWebServicePRController controller = new XteTestToolWebServicePRController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
            AM config = postaRegistrataConfig.getAM();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(acquisizioneMazzettiPRView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(acquisizioneMazzettiPRView.getAcquisizioneMazzettiPanelRef().getParent(), acquisizioneMazzettiPRView.getAcquisizioneMazzettiPanelRef(), webserviceview, new Rectangle(0,70,1000,690));

            logger.info("XteTestToolAcquisizioneMazzettiPRController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolAcquisizioneMazzettiPRController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneMazzettiPRController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller impostaDatiWebService failed", ex);
        }
    }
    
    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolAcquisizioneMazzettiPRController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolAcquisizioneMazzettiPRController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(acquisizioneMazzettiPRView.getParentFrame());

            logger.info("XteTestToolAcquisizioneMazzettiPRController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneMazzettiPRController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneMazzettiPRController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller salvaXmlData failed", ex);
        }
    }
}
