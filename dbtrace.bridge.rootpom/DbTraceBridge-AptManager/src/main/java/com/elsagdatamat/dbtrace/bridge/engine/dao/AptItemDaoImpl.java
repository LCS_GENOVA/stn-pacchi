package com.elsagdatamat.dbtrace.bridge.engine.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItem;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class AptItemDaoImpl extends SpringHibernateJpaDAOBase<AptItem, String> implements AptItemDao{
	@Override
	public void insertAptItemAndAptItemPosition(AptItem item, AptItemPosition itemPosition) {
		MyJpaBatchCallback<Integer> myJpaCallbak = new MyJpaBatchCallback(item, itemPosition);
		getJpaTemplate().execute(myJpaCallbak,true);
	}

	@Override
	public void insertAptItemAndUpdateAptItemPosition(AptItem item, AptItemPosition itemPosition) {
		MyJpaBatchCallbackInsertAndMerge<Integer> myJpaCallbak = new MyJpaBatchCallbackInsertAndMerge(item, itemPosition);
		getJpaTemplate().execute(myJpaCallbak,true);
	}

	@SuppressWarnings({ "hiding", "deprecation", "rawtypes" })
	class MyJpaBatchCallback<Integer> implements JpaCallback {
		AptItem item;
		AptItemPosition itemPosition;

		public MyJpaBatchCallback(AptItem _item, AptItemPosition _itemPosition){
			item = _item;
			itemPosition = _itemPosition;
		}

		@Override
		@Transactional(propagation=Propagation.SUPPORTS)
		public Object doInJpa(EntityManager em) throws PersistenceException {
			org.hibernate.Session session = ((HibernateEntityManager) em).getSession();
			Transaction tx = session.beginTransaction();
			try {
				session.save(item);
				session.save(itemPosition);

				tx.commit();
				session.close();
			} catch (Exception e) {
				tx.rollback();
				session.close();
			}

			return null;
		}
	}

	@SuppressWarnings({ "hiding", "deprecation", "rawtypes" })
	class MyJpaBatchCallbackInsertAndMerge<Integer> implements JpaCallback {
		AptItem item;
		AptItemPosition itemPosition;

		public MyJpaBatchCallbackInsertAndMerge(AptItem _item, AptItemPosition _itemPosition){
			item = _item;
			itemPosition = _itemPosition;
		}

		@Override
		@Transactional(propagation=Propagation.SUPPORTS)
		public Object doInJpa(EntityManager em) throws PersistenceException {
			org.hibernate.Session session = ((HibernateEntityManager) em).getSession();
			Transaction tx = session.beginTransaction();
			try {
				session.save(item);
				session.merge(itemPosition);

				tx.commit();
				session.close();
			} catch (Exception e) {
				tx.rollback();
				session.close();
			}

			return null;
		}
	}
}
