package com.elsagdatamat.dbtrace.validation.event;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.Marshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;

import com.elsagdatamat.dbtrace.bridge.tosda.SDATrace;

public class TraceValidator extends Marshaller.Listener implements
		ValidationEventHandler {

	private List<NonCompliantTrace> nonCompliantTraces;
	private SDATrace lastTrace;
	private int position;
	
	// ho aggiunto questo flag perch� il metodo handleEvent viene chiamato due volte su ogni traccia in errore.  
	// In questo modo evito di inserire due volte la stessa traccia nella lista delle tracce errate
	private boolean isTheSame; 
	
	public TraceValidator() {
		super();
		nonCompliantTraces = new ArrayList<NonCompliantTrace>();
		position = -1;
		isTheSame = false;
	}

	@Override
	public void beforeMarshal(Object source) {
		if (source instanceof SDATrace) {
			lastTrace = (SDATrace) source;
			position += 1;
			isTheSame = false;
		}
	}

	@Override
	public boolean handleEvent(ValidationEvent event) {
		if (!isTheSame) {
			OutputStream out = new ByteArrayOutputStream();
			PrintStream s = new PrintStream(out);
			event.getLinkedException().printStackTrace(s);
			nonCompliantTraces.add(new NonCompliantTrace(lastTrace, out.toString(), position));
			isTheSame = true;
		}
		return true;
	}

	public List<NonCompliantTrace> getNonCompliantTraces() {
		return nonCompliantTraces;
	}

	public void resetEventHanlder(){
		nonCompliantTraces = new ArrayList<NonCompliantTrace>();
		position = -1;
		isTheSame = false;
	}
}
