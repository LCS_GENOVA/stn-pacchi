package com.elsagdatamat.dbtrace.bridge.tosda.dao;

import com.elsagdatamat.dbtrace.bridge.tosda.entities.TraceTypesToSend;
import com.elsagdatamat.dbtrace.bridge.tosda.entities.TraceTypesToSendPK;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface ITraceTypesToSendDAO extends IGenericDAO<TraceTypesToSend, TraceTypesToSendPK> {

}