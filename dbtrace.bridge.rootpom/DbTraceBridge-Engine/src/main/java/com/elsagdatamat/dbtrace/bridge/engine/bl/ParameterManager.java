package com.elsagdatamat.dbtrace.bridge.engine.bl;

import com.elsagdatamat.dbtrace.bridge.engine.entities.Parameter;

public class ParameterManager extends BaseManager<Parameter, String> implements IParameterManager {
    
	private static final String TRUE = "T";

	@Override
	public Boolean isEnabled(String consumerName)
	{
	    Parameter ec=findByID(consumerName);
	    return ec==null ||  ec.getValue().equals(TRUE);
	}
}
