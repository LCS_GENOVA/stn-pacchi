/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller;

import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolGlassPaneView;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolGlassPaneController implements XteTestToolController, AncestorListener, ActionListener, MouseListener,
    MouseMotionListener, FocusListener  {
    private XteTestToolGlassPaneView glassPaneView;
    private ResourceBundle messages;
    private Toolkit toolkit;
    private boolean inDrag = false;
    private static Logger logger = Logger.getLogger(XteTestToolGlassPaneController.class);
    
    public XteTestToolGlassPaneController(XteTestToolGlassPaneView glassPaneView) {
        try {
            logger.info("XteTestToolGlassPaneController start class creation");
            this.glassPaneView = glassPaneView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.glassPaneView.initializeView();
            addViewObjectsListeners();
            logger.info("XteTestToolGlassPaneController class created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolGlassPaneController class creation exception", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolGlassPaneController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Glass Pane controller creation failed", ex);
        }
    }

    private XteTestToolDataModel getTotemDataModel() {
        XteTestToolDataModel totemDataModel = glassPaneView.getParentFrame().getApplicationFrameModel();
        return totemDataModel;
    }

    private void updateTotemDataModel(XteTestToolDataModel totemDataModel) {
        glassPaneView.getParentFrame().setApplicationFrameModel(totemDataModel);
    }

    @Override
    public void addViewObjectsListeners() {

        this.glassPaneView.addAncestorListener(this);
        this.glassPaneView.addMouseListener(this);
        this.glassPaneView.addMouseMotionListener(this);
        this.glassPaneView.addFocusListener(this);
        this.glassPaneView.getExitButtonRef().addActionListener(this);
    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof  XteTestToolGlassPaneView)
        {
            logger.debug("XteTestToolGlassPaneController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(glassPaneView.getExitButtonRef()))
        {
            logger.debug("XteTestToolGlassPaneController actionPerformed invoke backToTransactionsList");
            exit();
        }

    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        if (glassPaneView.isVisible()) {
            glassPaneView.requestFocus();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (glassPaneView.isNeedToRedispatch()) {
            redispatchMouseEvent(e);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (glassPaneView.isNeedToRedispatch()) {
            redispatchMouseEvent(e);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (glassPaneView.isNeedToRedispatch()) {
            redispatchMouseEvent(e);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (glassPaneView.isNeedToRedispatch()) {
            redispatchMouseEvent(e);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (glassPaneView.isNeedToRedispatch()) {
            redispatchMouseEvent(e);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (glassPaneView.isNeedToRedispatch()) {
            redispatchMouseEvent(e);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (glassPaneView.isNeedToRedispatch()) {
            redispatchMouseEvent(e);
        }
    }

    private void redispatchMouseEvent(MouseEvent e) {
        boolean inButton = false;
        boolean inMenuBar = false;
        boolean inTable = false;
        boolean inScrollBarButton = false;
        boolean inScrollBar = false;

        try{
            Point glassPanePoint = e.getPoint();
            Component component = null;
            Container container = glassPaneView.getParentFrame().getContentPaneView();
            Point containerPoint = SwingUtilities.convertPoint(glassPaneView,
                    glassPanePoint, glassPaneView.getParentFrame().getContentPaneView());
            int eventID = e.getID();

            if (containerPoint.y < 0) {
                inMenuBar = true;
                container = glassPaneView.getParentFrame().getJMenuBar();
                containerPoint = SwingUtilities.convertPoint(glassPaneView, glassPanePoint,
                        glassPaneView.getParentFrame().getJMenuBar());
                testForDrag(eventID);
            }

            component = SwingUtilities.getDeepestComponentAt(container,
                    containerPoint.x, containerPoint.y);

            if (component == null) {
                return;
            } else {
                //inButton = true;
                //testForDrag(eventID);
                //System.out.println(component.getClass().getName());
            }

            if(component instanceof JTable)
            {
                inTable = true;
            }

            if(component instanceof JScrollBar)
            {
                inScrollBar = true;
            }
            if(component instanceof JButton)
            {

                if(((JButton)component).getName()!=null)
                {
                    String buttonName = ((JButton)component).getName();
                    if(buttonName.equals(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR_INCR_BUTTON) || buttonName.equals(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR_DECR_BUTTON) || buttonName.equals(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR_INCR_BUTTON) || buttonName.equals(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR_DECR_BUTTON))
                        inScrollBarButton = true;
                }
            }

            if (inMenuBar || inScrollBarButton || inDrag || inScrollBar || inTable) {
                Point componentPoint = SwingUtilities.convertPoint(glassPaneView,
                        glassPanePoint, component);
                component.dispatchEvent(new MouseEvent(component, eventID, e.getWhen(), e.getModifiers(), componentPoint.x,
                        componentPoint.y, e.getClickCount(), e.isPopupTrigger()));
            }
        }catch (Exception ex)
        {
            logger.error("XteTestToolGlassPaneController redispatchMouseEvent failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Glass Pane controller redispatchMouseEvent failed", ex);
        }
    }

    private void testForDrag(int eventID) {
        if (eventID == MouseEvent.MOUSE_PRESSED) {
            inDrag = true;
        }
    }

    private void init()
    {
        try{
            logger.info("XteTestToolGlassPaneController start init");
            glassPaneView.customizeView();
            logger.info("XteTestToolGlassPaneController init completed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolGlassPaneController init failed", baseEx);
            throw baseEx;
        }
        catch(Exception ex)
        {
            logger.error("XteTestToolGlassPaneController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Glass Pane controller init failed", ex);
        }
    }

    private void exit()
    {
        logger.info("XteTestToolGlassPaneController start exit");
        
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(glassPaneView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolHomeController exit in progress");
            glassPaneView.setVisible(false);
            glassPaneView.setNeedToRedispatch(true);
            glassPaneView.getCurrentXteTestToolPanelView().exit();
        }        
    }

}
