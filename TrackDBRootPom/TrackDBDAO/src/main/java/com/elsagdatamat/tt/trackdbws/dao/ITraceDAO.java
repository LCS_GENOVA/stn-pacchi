package com.elsagdatamat.tt.trackdbws.dao;

import java.util.List;

import com.elsagdatamat.framework.dao.IGenericDAO;
import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.elsagdatamat.tt.trackdbws.beans.VO.EsiPackVO;

public interface ITraceDAO extends IGenericDAO<Trace, Long> {

	public List<Trace> returnTraceListExecuteHqlQuery(String hqlQuery);

	public List<EsiPackVO> getListOfEsiPackVO(String hqlQuery);

	public Trace merge(Trace entity);
}