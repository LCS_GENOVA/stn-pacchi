package com.elsagdatamat.dbtrace.wsclient.test;

import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import junit.framework.Assert;

import org.junit.Test;

import com.elsagdatamat.trackdbws.client.Trace;
import com.elsagdatamat.trackdbws.client.TraceDetail;
import com.elsagdatamat.trackdbws.client.TraceDetailsList;
import com.elsagdatamat.trackdbws.client.TracesList;
import com.elsagdatamat.trackdbws.client.TrackDBRWService;
import com.elsagdatamat.trackdbws.client.TrackDBRWServicePortType;

public class PacchiWsClientTest {

	@Test
	public void testSendTrace(){
		try {
			TracesList tracesList = new TracesList();
			
			Trace trace = new Trace();
			trace.setChannel("Channal");
			trace.setIdChannel("IdChannal");
			trace.setIdTracedEntity("IdTracedEntity");
			trace.setWhatHappened("Test");
			
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(new Date());
			XMLGregorianCalendar xc = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			trace.setWhenHappened(xc);
			trace.setWhereHappened("Qui");
			
			TraceDetail detail1 = new TraceDetail();
			detail1.setParamClass("Info1");
			detail1.setParamValue("Value1");
			
			TraceDetail detail2 = new TraceDetail();
			detail2.setParamClass("Info2");
			detail2.setParamValue("Value2");
			
			TraceDetailsList lstDetails = new  TraceDetailsList();
			
			lstDetails.getTraceDetails().add(detail1);
			
			lstDetails.getTraceDetails().add(detail2);
			
			trace.setTraceDetailsList(lstDetails);
			
			tracesList.getTraces().add(trace);
			
			URL url = new URL("http://localhost:8080/dbtrace_ws/dbtrace.ws");
			QName name = new QName("http://dataposta.tt.elsagdatamat.com/trackwriterws/trackwriterservice","TrackWriterServicePortType");
			//Service service = TrackWriterService.create(url, name);
			TrackDBRWService tws = new TrackDBRWService(url, name);
			//TrackWriterService tws = new TrackWriterService();
			TrackDBRWServicePortType  twsp = tws.getTrackDBRWServicePort();
			twsp.putTraces(tracesList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	
		
	}
}
