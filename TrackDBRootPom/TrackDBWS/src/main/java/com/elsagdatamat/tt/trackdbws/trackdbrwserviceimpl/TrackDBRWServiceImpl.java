package com.elsagdatamat.tt.trackdbws.trackdbrwserviceimpl;

import java.math.BigInteger;

import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.elsagdatamat.tt.trackdbws.trackdbrwservice.GetSealLastUseInput;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.GetSealLastUseResponse;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.PackCodeForRetrieve;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.PackDataRetrieveOutput;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TokenTillToDateForEsiPack;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TokenTilltoDate;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TokenTilltoDateByFiltersList;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TracesList;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TracesListNextToken;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TrackDBRWServicePortType;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.UseSealInput;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.UseSealResponse;


/**
 * @author AM, 10/2010
 * 
 * Aggiunta per ovviare problema della mancata transazionalita' 
 * delle operazioni che implementano il servizio
 *
 */
@WebService(serviceName = "TrackDBRWService", portName = "TrackDBRWServicePort", endpointInterface = "com.elsagdatamat.tt.trackdbws.trackdbrwservice.TrackDBRWServicePortType", targetNamespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice")
public class TrackDBRWServiceImpl extends SpringBeanAutowiringSupport implements TrackDBRWServicePortType {

	@Autowired
	TrackDBRWServiceTrans trackDBRWServiceTrans;
	
	public BigInteger putTraces(TracesList putTracesInput) {
		return trackDBRWServiceTrans.putTraces(putTracesInput);
	}

	public int getChunkSize(String getChunkSizeInput) {
		return trackDBRWServiceTrans.getChunkSize(getChunkSizeInput);
	}

	public String getToken(XMLGregorianCalendar getTokenInput) {
		return trackDBRWServiceTrans.getToken(getTokenInput);
	}

	public TracesListNextToken getTraces(TokenTilltoDate getTracesInput) {
		return trackDBRWServiceTrans.getTraces(getTracesInput);
	}

    public TracesListNextToken getTracesByFiltersList(TokenTilltoDateByFiltersList getTracesByFiltersListInput) {
    	return trackDBRWServiceTrans.getTracesByFiltersList(getTracesByFiltersListInput);
    }
	
    public TracesList getSackImageFromNSPByPackCode(String getSackImageFromNSPByPackCodeInput){
    	return trackDBRWServiceTrans.getSackImageFromNSPByPackCode(getSackImageFromNSPByPackCodeInput);
    }

    public TracesList getSackImageFromNSPBySackCode(String getSackImageFromNSPBySackCodeInput){
    	return trackDBRWServiceTrans.getSackImageFromNSPBySackCode(getSackImageFromNSPBySackCodeInput);
    }

	public TracesListNextToken getNextSackImageFromNSP(TokenTilltoDateByFiltersList getNextSackImageFromNSPInput) {
		return trackDBRWServiceTrans.getNextSackImageFromNSP(getNextSackImageFromNSPInput);
	}

	public TracesListNextToken getEsiPacks(TokenTillToDateForEsiPack getEsiPackInput) {
		return trackDBRWServiceTrans.getEsiPacks(getEsiPackInput);
	}
	
	public PackDataRetrieveOutput packDataRetrieve(PackCodeForRetrieve packCode)
	{
		return trackDBRWServiceTrans.packDataRetrieve( packCode);
	}
	public  GetSealLastUseResponse getSealLastUse(GetSealLastUseInput gslu)
	{
		return trackDBRWServiceTrans.getSealLastUse(gslu);
	}

	public UseSealResponse useSeal(UseSealInput useSealInput)
	{
		
		return trackDBRWServiceTrans.useSeal(useSealInput);
	}


}

