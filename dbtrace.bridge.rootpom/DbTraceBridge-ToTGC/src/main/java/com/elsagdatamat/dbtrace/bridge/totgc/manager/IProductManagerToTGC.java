package com.elsagdatamat.dbtrace.bridge.totgc.manager;

import java.util.List;

import com.elsagdatamat.dbtrace.bridge.totgc.manager.ProductManagerToTGC.ListOfSA;
import com.selexelsag.tracktrace.extension.managers.ProductManager;
import com.selexelsag.tracktrace.extension.objectmodel.Product;

public interface IProductManagerToTGC extends ProductManager {

	List<Product> getInterestingProducts();

	ListOfSA getInterestingAdditionalServices();

}
