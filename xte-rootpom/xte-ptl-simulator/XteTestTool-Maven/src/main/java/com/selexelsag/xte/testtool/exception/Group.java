/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.exception;

/**
 *
 * @author Da Procida
 */
public enum Group {

    CONFIGURATION(0),
    GENERIC(1);
    private final int code;

    private Group(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
