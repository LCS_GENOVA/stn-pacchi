package com.elsagdatamat.dbtrace.bridge.engine.bl;

import com.elsagdatamat.dbtrace.bridge.engine.entities.Parameter;

public interface IParameterManager extends IManager<Parameter, String>{

    Boolean isEnabled(String consumerName);
    
	
	
}
 