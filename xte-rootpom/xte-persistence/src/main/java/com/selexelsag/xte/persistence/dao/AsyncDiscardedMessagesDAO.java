package com.selexelsag.xte.persistence.dao;

import org.springframework.transaction.annotation.Transactional;

import com.selexelsag.xte.persistence.entities.AsyncDiscardedMessages;

@Transactional
public class AsyncDiscardedMessagesDAO extends BaseDAO<AsyncDiscardedMessages>
{
	//private static final Log log = LogFactory.getLog(AsyncDiscardedMessagesDAO.class);

	public AsyncDiscardedMessagesDAO()
	{
		super(AsyncDiscardedMessages.class);
	}
}
