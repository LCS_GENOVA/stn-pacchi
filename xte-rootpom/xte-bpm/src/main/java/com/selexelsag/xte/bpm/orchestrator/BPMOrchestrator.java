package com.selexelsag.xte.bpm.orchestrator;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.process.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.xte.bpm.exceptions.BpmException;
import com.selexelsag.xte.bpm.flows.IProcessManager;
import com.selexelsag.xte.bpm.model.ValidationResults;
import com.selexelsag.xte.business.exceptions.BusinessFlowMapperException;
import com.selexelsag.xte.business.tracking.IFlowMapper;
import com.selexelsag.xte.business.tracking.IInteropForward;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.model.flows.FlowResponse;
import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.model.track.TrackResponse;

public class BPMOrchestrator implements IBPMOrchestrator
{
	private Logger logger = LogManager.getLogger(BPMOrchestrator.class.getName());

	private Logger interopLogger = LogManager.getLogger("interopLogger");

	@Autowired
	private IProcessManager processManager;

	@Autowired
	private IFlowMapper flowMapper;

	@Override
	public IProcessManager getProcessManager() {return processManager;}

	@Autowired
	private IInteropForward interopForward;

	@Override
	public TrackResponse processRequest(TrackRequest trackRequest) throws BpmException
	{
		FlowParams flowParams = null;

		// ----------------------------------------------------------------------------------------
		// tramite il file flow-resolver.drl popolo la variabile di contesto
		// ----------------------------------------------------------------------------------------
		try
		{
			flowParams = flowMapper.getFlowParams(trackRequest);
		}
		catch (BusinessFlowMapperException bfme)
		{
			BpmException be = new BpmException("Errore sul processamento della richiesta", bfme);
			throw be;
		}

		// ----------------------------------------------------------------------------------------
		// tramite il file ProcessIdResolver.drl individuo il processo da attivare (sync piuttosto che async)
		// ----------------------------------------------------------------------------------------
		String processName = getProcessManager().getProcessId(flowParams);
		if(processName == null || processName.length()==0){
			BpmException be = new BpmException("Process id non trovato");
			throw be;
		}

		if (processName == null || processName.length()==0)
		{
			BpmException be = new BpmException("Process id non trovato");
		    throw be;
		}

		// params e' la variabile di contesto completa
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder dataResult = new StringBuilder();
		FlowResponse flowResponse = new FlowResponse();
		ValidationResults valRes = new ValidationResults();
		params.put("flowParams", flowParams);
		params.put("data", trackRequest.getTrackData());
		params.put("dataResult", dataResult);
		params.put("flowResponse", flowResponse);
		params.put("validationResults", valRes);
		// String process = "com.selexelsag.xte.bpmn.main_process";

		logger.debug("trackRequest : " + trackRequest);
		logger.debug("flowParams : " + flowParams);

		StatefulKnowledgeSession ksession = processManager.getKsession();

		//IMPORTANTE CHE IL VALORE STAMPATO SIA ZERO
		logger.debug("number of currently active process instances: " + ksession.getProcessInstances().size());
		logger.debug("ProcessId: " + processName);

		// ----------------------------------------------------------------------------------------
		// attivo il processo BPM
		// ----------------------------------------------------------------------------------------
		ProcessInstance instance = ksession.startProcess(processName, params);

		// ----------------------------------------------------------------------------------------
		// gestione eventuale errore sul processo BPM
		// ----------------------------------------------------------------------------------------
		if (instance.getState() != ProcessInstance.STATE_COMPLETED)
		{
			logger.error(instance.getProcessName());

			ksession.abortProcessInstance(instance.getId());

			if (instance.getState()==ProcessInstance.STATE_ABORTED)
			{
				logger.error("stato istanza STATE_ABORTED");
			}
			else if (instance.getState()==ProcessInstance.STATE_ACTIVE)
			{
				logger.error("stato istanza STATE_ACTIVE");
			}
			else if (instance.getState()==ProcessInstance.STATE_PENDING)
			{
				logger.error("stato istanza STATE_PENDING");
			}
			else if (instance.getState()==ProcessInstance.STATE_SUSPENDED)
			{
				logger.error("stato istanza STATE_SUSPENDED");
			}

			logger.error("stato istanza non STATE_COMPLETED, non dovrebbe mai succedere!");
			// TODO: cosa fare se lo stato dopo l'esecuzione non è COMPLETED???
			throw new BpmException("processo non completato correttamente", null);
		}

		if (flowResponse.getError() != null)
		{
			throw new BpmException("Errore nel processo", flowResponse.getError());
		}

		// ----------------------------------------------------------------------------------------
		// composizione della risposta
		// ----------------------------------------------------------------------------------------
		TrackResponse ans = makeTrackResponseFromFlow(trackRequest, (FlowResponse)params.get("flowResponse"), dataResult);

		logger.debug("ans: " + ans);

		// ----------------------------------------------------------------------------------------
		// eventuale inoltro a INTEROPERABILITA
		// ----------------------------------------------------------------------------------------
		String bRet = "";

		try
		{
			bRet = flowParams.getFowardToInt();

			logger.debug("interopForward flowParams: " + bRet);
			if (bRet != null) {
				if (bRet.equals("S"))
				{
					interopForward.manageInterop(trackRequest, ans);
				}
			}
		}
		catch (Exception e)
		{
			BpmException be = new BpmException("Errore sull'inoltro ad Interoperabilita'", e);
			String strErr = "\nErrore sull'inoltro ad Interoperabilita'\n\n";
			strErr += "TrackRequest : \n\n" + trackRequest.toString() + "\n\n";
			strErr += "TrackResponse : \n\n" + ans.toString() + "\n\n\n";
			interopLogger.error(strErr, e);
			//throw be;
		}

		return ans;
	}

	private TrackResponse makeTrackResponseFromFlow(TrackRequest request, FlowResponse flow, StringBuilder outputParam) throws BpmException
	{
		TrackResponse ans = null;
		try
		{
			ans = new TrackResponse();
			ans.setChannel(request.getChannel().toString());
			ans.setDestinationOfficeId(request.getDestinationOfficeId().toString());

			if (request.getSourceOfficeId()!=null)
			{
				ans.setSourceOfficeId(request.getSourceOfficeId().toString());
			}
			else
			{
				ans.setSourceOfficeId(null);
			}

			if (request.getObjectId()!=null)
			{
				ans.setObjectId(request.getObjectId().toString());
			}
			else
			{
				ans.setObjectId(null);
			}
			ans.setEventName(request.getEventName().toString());
			ans.setServiceName(request.getService().toString());

			ans.setOutputParam(StringEscapeUtils.unescapeXml(outputParam.toString()));

			ans.setErrorCode(flow.getFlowResult());
			ans.setErrorDesc(flow.getFlowDescResult());
			ans.setErrorId(flow.getFlowCodeResult());
			ans.setRetryFlag(flow.getFlowReiteration());
		}
		catch (Exception e)
		{
			BpmException be = new BpmException("errore in composizione risposta", e);
			throw be;
		}
		return ans;
	}
}
