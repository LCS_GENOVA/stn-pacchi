/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectmodel;

 
import com.selexelsag.xte.testtool.datamodel.XteGeneraMazzettiGenericObj;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 *
 * @author Da Procida
 */
public class XtePercorsoTableModel extends XteGeneraMazzettiTableModel {

    private String[] columnNames = new String[]{
        ResourceBundle.getBundle("bundles/messages").getString("percorso.table.header.prodotto"),
        ResourceBundle.getBundle("bundles/messages").getString("percorso.table.header.numero"),
    };
    
    private Map<String, XteGeneraMazzettiGenericObj> map = new HashMap<String, XteGeneraMazzettiGenericObj>();
    
    public XtePercorsoTableModel() {

        super();
        this.setColumnCount(columnNames.length);
        this.setColumnIdentifiers(columnNames);

    }

    public void addTipoMazzettoItem(XteGeneraMazzettiGenericObj item) throws XteTestToolDuplicateKeyException{
        if(map.containsKey(item.getTipo()))
        {
            throw new XteTestToolDuplicateKeyException(Group.GENERIC, Severity.WARNING, "Percorso Table Model duplicate key exception");
        }
        else
        {
            addRow(new Object[]{item.getTipo(), new Long(item.getNumero())});
            map.put(item.getTipo(), item);            
        }        
    }
    

    public void setModelData(Map<String, XteGeneraMazzettiGenericObj> items) {
        getDataVector().clear();
        map.clear();                
        for (XteGeneraMazzettiGenericObj item : items.values()) {
            addRow(new Object[]{item.getTipo(), new Long(item.getNumero())});
            map.put(item.getTipo(), item);            
        }
    }

    public XteGeneraMazzettiGenericObj getSelectedItem(int rowIndex) {
        
        String tipo = (String) getValueAt(rowIndex, 0);
        if(map.containsKey(tipo))
        {
            return map.get(tipo);
        }
        else
        {
            return null;
        }
    }

    public void clearModel() {
        getDataVector().clear();
        map.clear();
    }

    public Map<String, XteGeneraMazzettiGenericObj> getMap() {
        return map;
    }

    
    @Override
    public boolean isCellEditable(int row, int column) {
        return (false);
    }
    
}
