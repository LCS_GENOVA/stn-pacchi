CREATE OR REPLACE VIEW EDWH_TRACES_READY_TO_SEND_VW AS select t."ID",t."TRACED_ENTITY",t."ID_TRACED_ENTITY",t."WHEN_HAPPENED",t."WHEN_REGISTERED",t."WHERE_HAPPENED",t."CHANNEL",t."ID_CHANNEL",t."WHAT_HAPPENED",t."SERVICE_NAME",t."LABEL_TRACED_ENTITY",t."ID_STATUS",t."ID_CORRELAZIONE",t."ID_TRACED_EXTERNAL",t."ID_FORWARD_TO",t."SYS_FORWARD_TO" from Traces t inner join TRACE_TYPES_TO_SEND ttts on
t.what_happened=ttts.what_happened where not exists (select id from Sent_Trace st where st.id = t.id) 
and t.traced_entity = 'PACCO' order by t.id
/