package com.selexelsag.xte.business.tracking;

import com.selexelsag.xte.business.exceptions.BusinessFlowMapperException;
import com.selexelsag.xte.business.exceptions.BusinessValidationException;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.model.track.TrackRequest;

public interface IFlowMapper {

	public static final String FLOW_MAPPER_BEAN_NAME = "baseFlowMapper";
	
	/**
	 * Ritorna i parametri del flusso sulla base dell'oggetto TrackRequest.
	 * @param request l'oggetto contenente i parametri identificativi 
	 * del flusso richiesto.
	 * @return l'identificativo flusso.
	 * @throws BusinessValidationException in caso di errore nel processo 
	 * di riconoscimento dei flusso.
	 */
	FlowParams getFlowParams(TrackRequest trackRequest) throws BusinessFlowMapperException;
	
}
