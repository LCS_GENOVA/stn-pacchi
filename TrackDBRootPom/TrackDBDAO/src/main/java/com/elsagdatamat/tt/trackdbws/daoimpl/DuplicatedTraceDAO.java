package com.elsagdatamat.tt.trackdbws.daoimpl;

import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.tt.trackdbws.beans.DuplicatedTrace;
import com.elsagdatamat.tt.trackdbws.dao.IDuplicatedTraceDAO;

public class DuplicatedTraceDAO extends SpringHibernateJpaDAOBase<DuplicatedTrace, Long> implements IDuplicatedTraceDAO {

}
