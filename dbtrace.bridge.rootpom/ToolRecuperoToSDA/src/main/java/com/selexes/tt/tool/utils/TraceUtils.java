package com.selexes.tt.tool.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class TraceUtils {
	
	static public List<String> getProdottiValidi() {
		String[] prodottiArray= new String[]{
				"FJ1", "ERS", "HS4", "CBX", "HBX", "CB4", "CM2", "COE", "CJ3", "CJ1", 
				"CI1", "COR", "COI", "POI", "POE", "PCE", "HB4", "HB6", "PM2", "POR", 
				"PI1", "PJ1", "PJ3", "EMS", "EPG", "ARC", "ADR", "SADR", "SBAN", "SECO", 
				"SESP", "SFDX", "SGEN", "SINT", "SPHA", "SRAC" 
		};
		
		return Arrays.asList(prodottiArray);
	}
	
	
	static public List<String> getAllProductByCode(String idTracedEntity){
		List<String> prodotti = new ArrayList<String>();
		HashMap<String,String> hash = getAllRegexProduct();
		Pattern pattern;
		for (String regex : hash.keySet()) {
			pattern = Pattern.compile(regex);
			if(pattern.matcher(idTracedEntity).matches())
				prodotti.add(hash.get(regex));
			}
		return prodotti;
	}
	
	/**
	 * Recuperate con questa query: select 'ptipocodiceMap.put("'||pt.code_format_regex||'","'||p.ptype||'");'
	 *	from tt_application.p_tipocodice pt join tt_application.p_tipocodice_product ptp
	 *	on pt.id = ptp.tipocodice_id 
	 *	join tt_application.product p 
	 *	on ptp.product_id = p.id;
	 * 
	 * @return
	 */
	private static HashMap<String,String> getAllRegexProduct(){
		HashMap<String,String> ptipocodiceMap = new HashMap<String,String>(0); 
		ptipocodiceMap.put("^([A-U]|[W-Z])[A-Z]{3}B[0-9]{8}$","COI");
		ptipocodiceMap.put("^PR[0-9]{9}IT$","ARC");
		ptipocodiceMap.put("^B[A-Z]([0-9]){11}$","CI1");
		ptipocodiceMap.put("^1([0-9]|[A-Z]){4}(0[1-9][0-9]{5}|00[1-9][0-9]{4}|000[1-9][0-9]{3}|0000[1-9][0-9]{2}|00000[1-9][0-9]|000000[1-9]|[1-3][0-9]{6}|4000000)D$","HB4");
		ptipocodiceMap.put("^F2([0-9]|[A-Z]){4}(0[1-9][0-9]{5}|00[1-9][0-9]{4}|000[1-9][0-9]{3}|0000[1-9][0-9]{2}|00000[1-9][0-9]|000000[1-9]|[1-3][0-9]{6}|4000000)$","HS4");
		ptipocodiceMap.put("^ZW[0-9]{9}IT$","PCE");
		ptipocodiceMap.put("^SC[0-9]{9}IT$","ARC");
		ptipocodiceMap.put("^A[A-Z]([0-9]|[A-Z]){5}$","POI");
		ptipocodiceMap.put("^B[A-Z]([0-9]|[A-Z]){5}$","COI");
		ptipocodiceMap.put("^[0-8][0-9]{3}C[0-9]{8}$","POI");
		ptipocodiceMap.put("^[0-9]{4}E[0-9]{8}$","POR");
		ptipocodiceMap.put("^CA[0-9]{9}IT$","CI1");
		ptipocodiceMap.put("^Y[A-Z]([0-9]|[A-Z]){5}$","CJ3");
		ptipocodiceMap.put("^[A-Z]{4}Z[0-9]{8}$","PJ3");
		ptipocodiceMap.put("^D[A-Z]([0-9]){11}$","CI1");
		ptipocodiceMap.put("^F([A-E]|[H-Z])([0-9]){11}$","CJ1");
		ptipocodiceMap.put("^98ZZA[0-9]{8}$","POE");
		ptipocodiceMap.put("^99ZZA[0-9]{8}$","POE");
		ptipocodiceMap.put("^9[0-9]{3}D(0[1-9][0-9]{6}|00[1-9][0-9]{5}|000[1-9][0-9]{4}|0000[1-9][0-9]{3}|00000[1-9][0-9]{2}|000000[1-9][0-9]|0000000[1-9]|[1-9][0-9]{7})$","CB4");
		ptipocodiceMap.put("^J(F|G)([0-9]){11}$","PJ3");
		ptipocodiceMap.put("^K(F|G)([0-9]){11}$","CJ3");
		ptipocodiceMap.put("^XE[0-9]{9}IT$","FJ1");
		ptipocodiceMap.put("^61([0-9]|[A-Z]){5}(0[1-9][0-9]{4}|00[1-9][0-9]{3}|000[1-9][0-9]{2}|0000[1-9][0-9]|00000[1-9]|[1-3][0-9]{5}|400000)$","HBX");
		ptipocodiceMap.put("^62([0-9]|[A-Z]){5}(0[1-9][0-9]{4}|00[1-9][0-9]{3}|000[1-9][0-9]{2}|0000[1-9][0-9]|00000[1-9]|[1-3][0-9]{5}|400000)$","CBX");
		ptipocodiceMap.put("^EE[0-9]{9}IT$","EMS");
		ptipocodiceMap.put("^[0-8][0-9]{3}A[0-9]{8}$","POI");
		ptipocodiceMap.put("^[0-9]{4}H[0-9]{8}$","COR");
		ptipocodiceMap.put("^Z[A-Z]([0-9]|[A-Z]){5}$","PJ3");
		ptipocodiceMap.put("^ZC[0-9]{9}IT$","PCE");
		ptipocodiceMap.put("^CP[0-9]{9}IT$","POE");
		ptipocodiceMap.put("^H([A-E]|[H-Z])([0-9]){11}$","CJ1");
		ptipocodiceMap.put("^L([A-E]|[H-Z])([0-9]){11}$","PJ3");
		ptipocodiceMap.put("^9[0-9]{3}B(0[1-9][0-9]{6}|00[1-9][0-9]{5}|000[1-9][0-9]{4}|0000[1-9][0-9]{3}|00000[1-9][0-9]{2}|000000[1-9][0-9]|0000000[1-9]|[1-9][0-9]{7})$","CB4");
		ptipocodiceMap.put("^9[0-9]{3}C(0[1-9][0-9]{6}|00[1-9][0-9]{5}|000[1-9][0-9]{4}|0000[1-9][0-9]{3}|00000[1-9][0-9]{2}|000000[1-9][0-9]|0000000[1-9]|[1-9][0-9]{7})$","HB4");
		ptipocodiceMap.put("^LM[0-9]{9}IT$","CM2");
		ptipocodiceMap.put("^1([0-9]|[A-Z]){4}(0[1-9][0-9]{5}|00[1-9][0-9]{4}|000[1-9][0-9]{3}|0000[1-9][0-9]{2}|00000[1-9][0-9]|000000[1-9]|[1-3][0-9]{6}|4000000)V$","HBX");
		ptipocodiceMap.put("^61([0-9]|[A-Z]){5}(4[1-9][0-9]{4}|40[1-9][0-9]{3}|400[1-9][0-9]{2}|4000[1-9][0-9]|40000[1-9]|[5-9][0-9]{5})$","HBX");
		ptipocodiceMap.put("^62([0-9]|[A-Z]){5}(4[1-9][0-9]{4}|40[1-9][0-9]{3}|400[1-9][0-9]{2}|4000[1-9][0-9]|40000[1-9]|[5-9][0-9]{5})$","CBX");
		ptipocodiceMap.put("^L([0-9]|[A-Z]){6}$","POE");
		ptipocodiceMap.put("^[0-9]{4}F[0-9]{8}$","POR");
		ptipocodiceMap.put("^[A-Z]{4}F[0-9]{8}$","POR");
		ptipocodiceMap.put("^[A-Z]{4}G[0-9]{8}$","COR");
		ptipocodiceMap.put("^XD[0-9]{9}IT$","CJ1");
		ptipocodiceMap.put("^KY[0-9]{9}IT$","CJ3");
		ptipocodiceMap.put("^(J|X|Y|W)[A-Z]{3}E[0-9]{8}$","POR");
		ptipocodiceMap.put("^(J|X|Y|W)[A-Z]{3}H[0-9]{8}$","COR");
		ptipocodiceMap.put("^98ZZB[0-9]{8}$","COE");
		ptipocodiceMap.put("^99ZZB[0-9]{8}$","COE");
		ptipocodiceMap.put("^F1([0-9]|[A-Z]){5}(0[1-9][0-9]{4}|00[1-9][0-9]{3}|000[1-9][0-9]{2}|0000[1-9][0-9]|00000[1-9]|[1-3][0-9]{5}|400000)$","HS4");
		ptipocodiceMap.put("^9[0-9]{3}E(0[1-9][0-9]{6}|00[1-9][0-9]{5}|000[1-9][0-9]{4}|0000[1-9][0-9]{3}|00000[1-9][0-9]{2}|000000[1-9][0-9]|0000000[1-9]|[1-9][0-9]{7})$","HB4");
		ptipocodiceMap.put("^1([0-9]|[A-Z]){4}(4[1-9][0-9]{5}|40[1-9][0-9]{4}|400[1-9][0-9]{3}|4000[1-9][0-9]{2}|40000[1-9][0-9]|400000[1-9]|[5-9][0-9]{6})V$","HBX");
		ptipocodiceMap.put("^M([0-9]|[A-Z]){6}$","POE");
		ptipocodiceMap.put("^[0-8][0-9]{3}B[0-9]{8}$","COI");
		ptipocodiceMap.put("^[0-8][0-9]{3}D[0-9]{8}$","COI");
		ptipocodiceMap.put("^[0-9]{4}G[0-9]{8}$","COR");
		ptipocodiceMap.put("^([A-U]|[W-Z])[A-Z]{3}A[0-9]{8}$","POI");
		ptipocodiceMap.put("^[A-Z]{4}E[0-9]{8}$","POR");
		ptipocodiceMap.put("^PX[0-9]{9}IT$","PI1");
		ptipocodiceMap.put("^XC[0-9]{9}IT$","PJ1");
		ptipocodiceMap.put("^W[A-Z]([0-9]|[A-Z]){5}$","CJ3");
		ptipocodiceMap.put("^[A-Z]{4}Y[0-9]{8}$","CJ3");
		ptipocodiceMap.put("^[A-Z]{4}W[0-9]{8}$","CJ3");
		ptipocodiceMap.put("^E([A-E]|[H-Z])([0-9]){11}$","PJ1");
		ptipocodiceMap.put("^KW[0-9]{9}IT$","CJ3");
		ptipocodiceMap.put("^J([A-E]|[H-Z])([0-9]){11}$","PJ3");
		ptipocodiceMap.put("^M([A-E]|[H-Z])([0-9]){11}$","CJ3");
		ptipocodiceMap.put("^(J|X|Y|W)[A-Z]{3}G[0-9]{8}$","COR");
		ptipocodiceMap.put("^KK[0-9]{9}IT$","EMS");
		ptipocodiceMap.put("^LC[0-9]{9}IT$","CM2");
		ptipocodiceMap.put("^L(F|G)([0-9]){11}$","PJ3");
		ptipocodiceMap.put("^M(F|G)([0-9]){11}$","CJ3");
		ptipocodiceMap.put("^2([0-9]|[A-Z]){4}(0[1-9][0-9]{5}|00[1-9][0-9]{4}|000[1-9][0-9]{3}|0000[1-9][0-9]{2}|00000[1-9][0-9]|000000[1-9]|[1-3][0-9]{6}|4000000)V$","CBX");
		ptipocodiceMap.put("^PI[0-9]{9}IT$","PI1");
		ptipocodiceMap.put("^XA[0-9]{9}IT$","PJ1");
		ptipocodiceMap.put("^[A-Z]{4}X[0-9]{8}$","PJ3");
		ptipocodiceMap.put("^EP[0-9]{9}IT$","EPG");
		ptipocodiceMap.put("^ZA[0-9]{9}IT$","PCE");
		ptipocodiceMap.put("^K([A-E]|[H-Z])([0-9]){11}$","CJ3");
		ptipocodiceMap.put("^(J|X|Y|W)[A-Z]{3}F[0-9]{8}$","POR");
		ptipocodiceMap.put("^V[A-Z]{3}A(0[1-9][0-9]{6}|00[1-9][0-9]{5}|000[1-9][0-9]{4}|0000[1-9][0-9]{3}|00000[1-9][0-9]{2}|000000[1-9][0-9]|0000000[1-9]|[1-9][0-9]{7})$","HB4");
		ptipocodiceMap.put("^LX[0-9]{9}IT$","PM2");
		ptipocodiceMap.put("^2([0-9]|[A-Z]){4}(0[1-9][0-9]{5}|00[1-9][0-9]{4}|000[1-9][0-9]{3}|0000[1-9][0-9]{2}|00000[1-9][0-9]|000000[1-9]|[1-3][0-9]{6}|4000000)D$","CB4");
		ptipocodiceMap.put("^2([0-9]|[A-Z]){4}(4[1-9][0-9]{5}|40[1-9][0-9]{4}|400[1-9][0-9]{3}|4000[1-9][0-9]{2}|40000[1-9][0-9]|400000[1-9]|[5-9][0-9]{6})D$","CB4");
		ptipocodiceMap.put("^G(F|G)([0-9]){11}$","PJ1");
		ptipocodiceMap.put("^H(F|G)([0-9]){11}$","CJ1");
		ptipocodiceMap.put("^EW[0-9]{9}IT$","EMS");
		ptipocodiceMap.put("^([A-U]|[W-Z])[A-Z]{3}C[0-9]{8}$","POI");
		ptipocodiceMap.put("^([A-U]|[W-Z])[A-Z]{3}D[0-9]{8}$","COI");
		ptipocodiceMap.put("^[A-Z]{4}H[0-9]{8}$","COR");
		ptipocodiceMap.put("^XB[0-9]{9}IT$","CJ1");
		ptipocodiceMap.put("^CC[0-9]{9}IT$","POE");
		ptipocodiceMap.put("^C[A-Z]([0-9]){11}$","PI1");
		ptipocodiceMap.put("^G([A-E]|[H-Z])([0-9]){11}$","PJ1");
		ptipocodiceMap.put("^KZ[0-9]{9}IT$","PJ3");
		ptipocodiceMap.put("^9[0-9]{3}A(0[1-9][0-9]{6}|00[1-9][0-9]{5}|000[1-9][0-9]{4}|0000[1-9][0-9]{3}|00000[1-9][0-9]{2}|000000[1-9][0-9]|0000000[1-9]|[1-9][0-9]{7})$","HB4");
		ptipocodiceMap.put("^LP[0-9]{9}IT$","PM2");
		ptipocodiceMap.put("^F1([0-9]|[A-Z]){5}(4[1-9][0-9]{4}|40[1-9][0-9]{3}|400[1-9][0-9]{2}|4000[1-9][0-9]|40000[1-9]|[5-9][0-9]{5})$","HS4");
		ptipocodiceMap.put("^SI[0-9]{9}IT$","ARC");
		ptipocodiceMap.put("^F(F|G)([0-9]){11}$","CJ1");
		ptipocodiceMap.put("^2([0-9]|[A-Z]){4}(4[1-9][0-9]{5}|40[1-9][0-9]{4}|400[1-9][0-9]{3}|4000[1-9][0-9]{2}|40000[1-9][0-9]|400000[1-9]|[5-9][0-9]{6})V$","CBX");
		ptipocodiceMap.put("^E(F|G)([0-9]){11}$","PJ1");
		ptipocodiceMap.put("^C[A-Z]([0-9]|[A-Z]){5}$","POI");
		ptipocodiceMap.put("^D[A-Z]([0-9]|[A-Z]){5}$","COI");
		ptipocodiceMap.put("^CX[0-9]{9}IT$","CI1");
		ptipocodiceMap.put("^X[A-Z]([0-9]|[A-Z]){5}$","PJ3");
		ptipocodiceMap.put("^A[A-Z]([0-9]){11}$","PI1");
		ptipocodiceMap.put("^KX[0-9]{9}IT$","PJ3");
		ptipocodiceMap.put("^HH[0-9]{9}IT$","EPG");
		ptipocodiceMap.put("^V[A-Z]{3}B(0[1-9][0-9]{6}|00[1-9][0-9]{5}|000[1-9][0-9]{4}|0000[1-9][0-9]{3}|00000[1-9][0-9]{2}|000000[1-9][0-9]|0000000[1-9]|[1-9][0-9]{7})$","CB4");
		ptipocodiceMap.put("^V[A-Z]{3}C(0[1-9][0-9]{6}|00[1-9][0-9]{5}|000[1-9][0-9]{4}|0000[1-9][0-9]{3}|00000[1-9][0-9]{2}|000000[1-9][0-9]|0000000[1-9]|[1-9][0-9]{7})$","HB4");
		ptipocodiceMap.put("^V[A-Z]{3}D(0[1-9][0-9]{6}|00[1-9][0-9]{5}|000[1-9][0-9]{4}|0000[1-9][0-9]{3}|00000[1-9][0-9]{2}|000000[1-9][0-9]|0000000[1-9]|[1-9][0-9]{7})$","CB4");
		ptipocodiceMap.put("^1([0-9]|[A-Z]){4}(4[1-9][0-9]{5}|40[1-9][0-9]{4}|400[1-9][0-9]{3}|4000[1-9][0-9]{2}|40000[1-9][0-9]|400000[1-9]|[5-9][0-9]{6})D$","HB4");
		ptipocodiceMap.put("^F2([0-9]|[A-Z]){4}(4[1-9][0-9]{5}|40[1-9][0-9]{4}|400[1-9][0-9]{3}|4000[1-9][0-9]{2}|40000[1-9][0-9]|400000[1-9]|[5-9][0-9]{6})$","HS4");
		ptipocodiceMap.put("^CW[0-9]{9}IT$","EPG");
		ptipocodiceMap.put("^CU[0-9]{9}IT$","ERS");
		ptipocodiceMap.put("^XL[0-9]{9}IT$","FJ1");
		ptipocodiceMap.put("^XQ[0-9]{9}IT$","FJ1");
		return ptipocodiceMap;
	}
}
