package com.elsagdatamat.dbtrace.bridge.engine.bl;

import com.elsagdatamat.dbtrace.bridge.engine.entities.RemovedTrace;


public class RemovedTraceBL extends BaseManager<RemovedTrace, Long> implements IRemovedTraceBL {

	@Override
	public void insert(RemovedTrace trace) {
		dao.insert(trace);
	}


}