package com.elsagdatamat.dbtrace.bridge.toedwh;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ICodiciProdottoPICodiciProdottoSDAManager;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;


public class EDWHConversionUtility {
	protected final static Log log = LogFactory.getLog(EDWHConversionUtility.class);

	private static SimpleDateFormat data = new SimpleDateFormat("yyyyMMdd");
	private static SimpleDateFormat ora = new SimpleDateFormat("HH:mm");

//	private enum FlowType
//	{
//		PI, SDA
//	}
//	//SCOTT-PINO GRECO
//	private static Set<String> codiciStatoAmmessiSDA = null;
//	private static Set<String> codiciStatoAmmessiPI = null;
	
//	private static Set<String> getCodiciStatoAmmessiSDA() {
//		
//		if (codiciStatoAmmessiSDA == null)
//		{
//			codiciStatoAmmessiSDA = new HashSet<String>();
//					
//			///////////////////////////////
//			//SDA
//			///////////////////////////////
//			codiciStatoAmmessiSDA.add("ATP");
//			codiciStatoAmmessiSDA.add("RIN");
//			codiciStatoAmmessiSDA.add("001");
//			codiciStatoAmmessiSDA.add("002");
//			codiciStatoAmmessiSDA.add("003");
//			codiciStatoAmmessiSDA.add("006");
//			codiciStatoAmmessiSDA.add("007");
//			codiciStatoAmmessiSDA.add("008");
//			codiciStatoAmmessiSDA.add("009");
//			codiciStatoAmmessiSDA.add("010");
//			codiciStatoAmmessiSDA.add("011");
//			codiciStatoAmmessiSDA.add("013");
//			codiciStatoAmmessiSDA.add("015");
//			codiciStatoAmmessiSDA.add("017");
//			codiciStatoAmmessiSDA.add("021");
//			codiciStatoAmmessiSDA.add("027");
//			codiciStatoAmmessiSDA.add("034");
//			codiciStatoAmmessiSDA.add("041");
//			codiciStatoAmmessiSDA.add("042");
//			codiciStatoAmmessiSDA.add("084");
//			codiciStatoAmmessiSDA.add("092");
//			codiciStatoAmmessiSDA.add("093");
//			codiciStatoAmmessiSDA.add("096");
//			codiciStatoAmmessiSDA.add("098");
//			codiciStatoAmmessiSDA.add("F24");
//			codiciStatoAmmessiSDA.add("KKK");
//			//////////////////////////////
//			
//			/////////////////////////////
//			//PI & SDA
//			/////////////////////////////
//			codiciStatoAmmessiSDA.add("RIT");
//			codiciStatoAmmessiSDA.add("PPP");
//			codiciStatoAmmessiSDA.add("THE");
//			codiciStatoAmmessiSDA.add("000");
//			////////////////////////////
//			
//			/////////////////////////////
//			//PI-Aggiunti per sicurezza
//			/////////////////////////////
//			codiciStatoAmmessiSDA.add("AUP");
//			codiciStatoAmmessiSDA.add("HBS");
//			codiciStatoAmmessiSDA.add("HBN");
//			codiciStatoAmmessiSDA.add("GGG");
//			codiciStatoAmmessiSDA.add("POM");
//			codiciStatoAmmessiSDA.add("FDP");
//			codiciStatoAmmessiSDA.add("CNP");
//			codiciStatoAmmessiSDA.add("UFI");
//			codiciStatoAmmessiSDA.add("085");
//			codiciStatoAmmessiSDA.add("ARP");
//			codiciStatoAmmessiSDA.add("IPE");
//			codiciStatoAmmessiSDA.add("RES");
//			codiciStatoAmmessiSDA.add("RFP");
//			codiciStatoAmmessiSDA.add("GCP");
//			codiciStatoAmmessiSDA.add("RRR");
//			codiciStatoAmmessiSDA.add("DNP");
//			codiciStatoAmmessiSDA.add("ABP");
//			codiciStatoAmmessiSDA.add("SMP");
//			////////////////////////////
//			
//			log.debug("Inizializzato filtro su codiceStatus SDA");
//			
//		}
//		return codiciStatoAmmessiSDA;
//	}
//
//	private static Set<String> getCodiciStatoAmmessiPI() {		
//		if (codiciStatoAmmessiPI == null)
//		{
//			codiciStatoAmmessiPI = new HashSet<String>();
//		
//			/////////////////////////////
//			//PI
//			/////////////////////////////
//			codiciStatoAmmessiPI.add("AUP");
//			codiciStatoAmmessiPI.add("HBS");
//			codiciStatoAmmessiPI.add("HBN");
//			codiciStatoAmmessiPI.add("GGG");
//			codiciStatoAmmessiPI.add("POM");
//			codiciStatoAmmessiPI.add("FDP");
//			codiciStatoAmmessiPI.add("CNP");
//			codiciStatoAmmessiPI.add("UFI");
//			codiciStatoAmmessiPI.add("085");
//			codiciStatoAmmessiPI.add("ARP");
//			codiciStatoAmmessiPI.add("IPE");
//			codiciStatoAmmessiPI.add("RES");
//			codiciStatoAmmessiPI.add("RFP");
//			codiciStatoAmmessiPI.add("GCP");
//			codiciStatoAmmessiPI.add("RRR");
//			codiciStatoAmmessiPI.add("DNP");
//			codiciStatoAmmessiPI.add("ABP");
//			codiciStatoAmmessiPI.add("SMP");
//			////////////////////////////
//			
//			/////////////////////////////
//			//PI & SDA
//			/////////////////////////////
//			codiciStatoAmmessiPI.add("RIT");
//			codiciStatoAmmessiPI.add("PPP");
//			codiciStatoAmmessiPI.add("THE");
//			codiciStatoAmmessiPI.add("000");
//			////////////////////////////
//
//			///////////////////////////////
//			//SDA-Aggiunti per sicurezza
//			///////////////////////////////
//			codiciStatoAmmessiPI.add("ATP");
//			codiciStatoAmmessiPI.add("RIN");
//			codiciStatoAmmessiPI.add("001");
//			codiciStatoAmmessiPI.add("002");
//			codiciStatoAmmessiPI.add("003");
//			codiciStatoAmmessiPI.add("006");
//			codiciStatoAmmessiPI.add("007");
//			codiciStatoAmmessiPI.add("008");
//			codiciStatoAmmessiPI.add("009");
//			codiciStatoAmmessiPI.add("010");
//			codiciStatoAmmessiPI.add("011");
//			codiciStatoAmmessiPI.add("013");
//			codiciStatoAmmessiPI.add("015");
//			codiciStatoAmmessiPI.add("017");
//			codiciStatoAmmessiPI.add("021");
//			codiciStatoAmmessiPI.add("027");
//			codiciStatoAmmessiPI.add("034");
//			codiciStatoAmmessiPI.add("041");
//			codiciStatoAmmessiPI.add("042");
//			codiciStatoAmmessiPI.add("084");
//			codiciStatoAmmessiPI.add("092");
//			codiciStatoAmmessiPI.add("093");
//			codiciStatoAmmessiPI.add("096");
//			codiciStatoAmmessiPI.add("098");
//			codiciStatoAmmessiPI.add("F24");
//			codiciStatoAmmessiPI.add("KKK");
//			//////////////////////////////
//
//			log.debug("Inizializzato filtro su codiceStatus PI");
//			
//		}
//		return codiciStatoAmmessiPI;
//	}

//	private static boolean isAllowed(Map<String, String> map, FlowType flowType)
//	{
//		Set<String> codiciStatoAmmessi = null;
//		if (flowType==FlowType.SDA)
//			codiciStatoAmmessi=getCodiciStatoAmmessiSDA();
//		else 
//			codiciStatoAmmessi=getCodiciStatoAmmessiPI();
//			
//		if ( (!codiciStatoAmmessi.contains(map.get("DELREASON"))) &&
//			     (!codiciStatoAmmessi.contains(map.get("STATUS"))) )
//			{
//				//log.debug("Filtro su codiceStatus: STATUS=" + map.get("STATUS") + "; DELREASON=" + map.get("DELREASON"));
//				return false;
//			}
//		else
//			return true;
//		
//	}
	
	private static Map<String,String> mapTraceType2Status = null;

	private  static Map<String,String> getMapTraceType2Status() 
	{		
		if (mapTraceType2Status == null)
		{
			mapTraceType2Status = new HashMap<String,String>();
			mapTraceType2Status.put("1", "ACC");//ACCETTATI 
			mapTraceType2Status.put("2", "REC");//CONSEGNATI
			mapTraceType2Status.put("3", "INE");//INESITATI

			log.debug("Inizializzato mapTraceType2Status");
			
		}
		return mapTraceType2Status;
	}

//	private static List<EDWHEsiti> convertToEDWHEsitiList(List<Trace> traceList, FlowType flowType)
//	{
//		List<EDWHEsiti> esiti = new ArrayList<EDWHEsiti>();
//		for (Trace t : traceList)
//		{
//			Map<String, String> map = new HashMap<String, String>();
//			if ((t.getTraceDetailsList() != null) &&
//				(t.getTraceDetailsList().getTraceDetails() != null))
//			{
//				for (TraceDetail d:t.getTraceDetailsList().getTraceDetails())
//				{
//					map.put(d.getParamClass(), d.getParamValue());
//				}
//			}
//
//			//SCOTT-PINO GRECO
//			if ( !isAllowed(map,flowType) )
//			{
//				continue;
//			}
//			
//			EDWHEsiti e = new EDWHEsiti();
//
//			//SCOTT-AUP: anticipo la valorizzazione di e.codiceStatus
//			if (t.getWhatHappened().equals("RIT_SDA")) 
//			{
//				e.codiceStatus = map.get("STATUS"); //TODO: verificare
//			}
//			else if (t.getWhatHappened().equals("ESI_SDA"))
//			{
//				e.codiceStatus = map.get("DELREASON"); //TODO: verificare
//			}
//			else
//			{
//				e.codiceStatus = null;
//			}
//
//			//SCOTT-AUP: e.codiceStatus.equals("AUP") allora il frazionario trasmesso e' un ufficio postale
//			if (t.getWhereHappened().equals("UP") || t.getWhereHappened().equals("UNSP") || ( (e.codiceStatus!=null) && (e.codiceStatus.equals("AUP"))))
//			{
//				e.codiceUfficioPostale = t.getIdChannel();// map.get("WHERE_ID");
//				e.codiceFilialeSDA = "";
//				e.descrizioneFilialeSDA = "";
//			}
//			else 
//			{
//				e.codiceFilialeSDA = t.getIdChannel();// map.get("WHERE_ID");
//				e.codiceUfficioPostale = "";
//				e.descrizioneFilialeSDA = map.get("WHERE_DESCRIPTION");;
//			}
//			String subp = map.get("SUBP");
//			if (subp != null)
//			{
//				e.codiceProdotto = subp;
//			}
//			else
//			{
//				e.codiceProdotto = t.getLabelTracedEntity();//TODO: forse va rimappato
//			}
//
//			e.dataStatus = data.format(t.getWhenHappened().toGregorianCalendar().getTime());
//			e.oraStatus = ora.format(t.getWhenHappened().toGregorianCalendar().getTime());
//			e.letteraDiVettura = t.getIdTracedEntity();
//			
//			esiti.add(e);
//		}
//		
//		return esiti;
//	}

	private static List<EDWHEsiti> convertToEDWHEsitiList(List<Trace> traceList, ICodiciProdottoPICodiciProdottoSDAManager sdaManager)
	{
		List<EDWHEsiti> esiti = new ArrayList<EDWHEsiti>();
		for (Trace t : traceList)
		{
			Map<String, String> map = new HashMap<String, String>();
			if ((t.getTraceDetailsList() != null) &&
				(t.getTraceDetailsList().getTraceDetails() != null))
			{
				for (TraceDetail d:t.getTraceDetailsList().getTraceDetails())
				{
					map.put(d.getParamClass(), d.getParamValue());
				}
			}

			EDWHEsiti e = new EDWHEsiti();
			
			//PARTE RICAVABILE DA MASTER
			e.letteraDiVettura = t.getIdTracedEntity();
			e.dataStatus = data.format(t.getWhenHappened().toGregorianCalendar().getTime());
			e.oraStatus = ora.format(t.getWhenHappened().toGregorianCalendar().getTime());

			//PARTE RICAVABILE DA MASTER + map.get("WHERE_DESCRIPTION")
			if (t.getWhereHappened().equals("UP") || t.getWhereHappened().equals("UNSP") )
			{
				e.codiceUfficioPostale = t.getIdChannel();// map.get("WHERE_ID");
				e.codiceFilialeSDA = "";
				e.descrizioneFilialeSDA = "";
			}
			else 
			{
				e.codiceFilialeSDA = t.getIdChannel();
				e.codiceUfficioPostale = "";
				e.descrizioneFilialeSDA = map.get("WHERE_DESCRIPTION");;
			}
			
			if( t.getChannel().equals("SDA"))
				e.codiceProdotto = map.get("SUBP");
			else 
				e.codiceProdotto = sdaManager.getSdaCode(t.getIdTracedEntity());

			//CODICE STATUS
			//Creato un dettaglio fittizio TraceType
			//TODO tradotto
			e.codiceStatus = getMapTraceType2Status().get(map.get("TraceType")); 
						
			//DEBUG
			if((e.codiceProdotto==null) || (e.codiceProdotto.length()==0))
			{
				log.error(e.letteraDiVettura + ": codiceProdotto non valorizzato");
			}
			
			if((e.codiceStatus==null) || (e.codiceStatus.length()==0) )
			{
				log.error(e.letteraDiVettura + ": codiceStatus non valorizzato");
			}
			
			esiti.add(e);
		}
		
		return esiti;
	}

	private static List<EDWHFatturazioni> convertToEDWHFatturazioniList(List<Trace> traceList)
	{
		List<EDWHFatturazioni> fatturazioni = new ArrayList<EDWHFatturazioni>();
		for (Trace t : traceList)
		{
			Map<String, String> map = new HashMap<String, String>();
			if ((t.getTraceDetailsList() != null) &&
				(t.getTraceDetailsList().getTraceDetails() != null))
			{
				for (TraceDetail d:t.getTraceDetailsList().getTraceDetails())
				{
					map.put(d.getParamClass(), d.getParamValue());
				}
			}

			EDWHFatturazioni f = new EDWHFatturazioni();
			
			f.accountP = map.get("ACCOUNT_P");
			f.accountQ = map.get("ACCOUNT_Q");
			f.accountServAccAssicurazione = map.get("ACCOUNT_SERV_ACC_ASSICURAZIONE");
			f.accountServAccCAssegnoCC = map.get("ACCOUNT_SERV_ACC_C_ASSEGNO_C_C");
			f.cap = map.get("CAP");
			f.codiceCausaleDiFatturazione = map.get("CAUSALE_FATTURAZIONE");
			f.codiceCre = map.get("CODICE_CRE");
			f.codiceFiliale = map.get("CODICE_FILIALE");
			String subp = map.get("SUBP");
			if (subp != null)
			{
				f.codiceProdotto = subp;
			}
			else
			{
				f.codiceProdotto = t.getLabelTracedEntity();//TODO: forse va rimappato
			}
			f.codiceSap = map.get("CODICE_SAP");
			f.codiceUfficioPostale = map.get("CODICE_UP");
			f.dataFatturazione = data.format(t.getWhenHappened().toGregorianCalendar().getTime());
			f.dataRitiro = map.get("DATA_RITIRO"); //TODO: rivedere
			try {
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date d = null;
				d = df.parse(f.dataRitiro);
				if (d != null)
				{
					f.dataRitiro = data.format(d);
				}
			} catch (ParseException e) {
			}
			f.flagServizioAss = map.get("FLAG_SERVIZIO_ASS");
			f.flagServizioCa = map.get("FLAG_SERVIZIO_CA");
			f.idRitiro = map.get("ID_RITIRO");
			f.importoRitiro = map.get("IMPORTO_RITIRO"); //TODO
			f.importoSpedizione = map.get("IMPORTO_SPEDIZIONE"); //TODO
			f.importoTd = map.get("IMPORTO_TD");
			f.letteraDiVettura = t.getIdTracedEntity();
			f.localita = map.get("LOCALITA");
			f.numeroLetteraDiVetturaA = map.get("NUM_LDV_A");
			f.numeroLetteraDiVetturaDa = map.get("NUM_LDV_DA");
//			f.codiceFiscale = map.get("CODICE_FISCALE");
//			f.partitaIva = map.get("PARTITA_IVA");
			if (map.get("PARTITA_IVA") != null)
			{
				f.partitaIvaOCodiceFiscale = map.get("PARTITA_IVA");
			}
			else
			{
				f.partitaIvaOCodiceFiscale = map.get("CODICE_FISCALE");
			}
			f.provincia = map.get("PROVINCIA");
			f.tariffa = map.get("TARIFFA"); //TODO:
			f.tipoCliente = map.get("TIPO_CLIENTE");
			f.via = map.get("VIA");

			fatturazioni.add(f);
		}
		
		return fatturazioni;
	}
	
	static EDWHEsiPack convertToEsiPack(List<Trace> traceList, ICodiciProdottoPICodiciProdottoSDAManager sdaManager)
	{
		EDWHEsiPack message = new EDWHEsiPack();
		message.header = new EDWHHeader();
		message.footer = new EDWHFooter();
		
		message.header.descrizione = "data inizio";
		message.header.data = data.format(new Date());
		message.header.descrizioneFile = "esitipak";
		
		message.esiti = convertToEDWHEsitiList(traceList, sdaManager);
		
		message.footer.data = data.format(new Date());
		message.footer.descrizione = "data fine";
		
		return message;
	}
	
	static EDWHEsiPock convertToEsiPock(List<Trace> traceList, ICodiciProdottoPICodiciProdottoSDAManager sdaManager)
	{
		EDWHEsiPock message = new EDWHEsiPock();
		message.header = new EDWHHeader();
		message.footer = new EDWHFooter();
		
		message.header.descrizione = "data inizio";
		message.header.data = data.format(new Date());
		message.header.descrizioneFile = "esitipoc";
		
		message.esiti = convertToEDWHEsitiList(traceList, sdaManager);
		
		message.footer.data = data.format(new Date());
		message.footer.descrizione = "data fine";
		
		return message;
	}
	
	static EDWHFattPack convertToFattPack(List<Trace> traceList)
	{
		EDWHFattPack message = new EDWHFattPack();
		message.header = new EDWHHeader();
		message.footer = new EDWHFooter();
		
		message.header.descrizione = "data inizio";
		message.header.data = data.format(new Date());
		message.header.descrizioneFile = "fattpak";
		
		message.fatturazioni = convertToEDWHFatturazioniList(traceList);
		
		message.footer.data = data.format(new Date());
		message.footer.descrizione = "data fine";
		
		return message;
	}
	
	static EDWHFattPock convertToFattPock(List<Trace> traceList)
	{
		EDWHFattPock message = new EDWHFattPock();
		message.header = new EDWHHeader();
		message.footer = new EDWHFooter();
		
		message.header.descrizione = "data inizio";
		message.header.data = data.format(new Date());
		message.header.descrizioneFile = "fattpoc";
		
		message.fatturazioni = convertToEDWHFatturazioniList(traceList);
		
		message.footer.data = data.format(new Date());
		message.footer.descrizione = "data fine";
		
		return message;
	}
	
	static EDWHStati convertToStati(List<Trace> traceList)
	{
		return null;
	}
	
//	public static String marshal(CdGMessage o, Schema schema) throws JAXBException
//	{
//		JAXBContext context = JAXBContext.newInstance(o.getClass());
//		Marshaller marshaller = context.createMarshaller();
//		StringWriter sw = new StringWriter();
//		if (schema != null) marshaller.setSchema(schema);
//		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//		marshaller.marshal(o, sw);
//		return sw.toString();
//	}
//	
//	public static Schema getSchemaFromXSD(String xsdPath) throws SAXException
//	{
//		log.debug("XSD: "+xsdPath);
//		URL url = null;
//		try {
//			url = new URL(xsdPath);
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		SchemaFactory sf = SchemaFactory.newInstance( 
//		        javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI); 
//		//Schema schema = sf.newSchema(new File(xsdPath));
//		Schema schema = sf.newSchema(url);
//		return schema;
//	}
}
