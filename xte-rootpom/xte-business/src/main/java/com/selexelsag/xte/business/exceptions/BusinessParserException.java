package com.selexelsag.xte.business.exceptions;

public class BusinessParserException extends BusinessException {

	public BusinessParserException(String message, Throwable cause) {
		super(message, cause);
		setExceptionId(BUSINESS_ID);
	}
	
	public BusinessParserException(String msg) {
		super(msg);
	}
	
}
