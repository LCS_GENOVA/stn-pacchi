package com.elsagdatamat.dbtrace.bridge.totgc.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

// DB_TRACK_OWNER.TRACE_TYPES_TO_SEND

@Entity
@IdClass(TraceTypesToSendPK.class)
@Table(name = "TRACE_TYPES_TO_SEND")
public class TraceTypesToSend implements Serializable{

    private static final long serialVersionUID = -3031126251708419551L;
    
    @Column
    private String channel;
    @Id
    @Column(name="WHAT_HAPPENED")
    private String whatHappened;
    @Id
    @Column
    private String destination;
    @Column(name="TRACED_ENTITY")
    private String tracedEntity;
    
    public String getChannel() {
        return channel;
    }
    public void setChannel(String channel) {
        this.channel = channel;
    }
    public String getWhatHappened() {
        return whatHappened;
    }
    public void setWhatHappened(String whatHappened) {
        this.whatHappened = whatHappened;
    }
    public String getDestination() {
        return destination;
    }
    public void setDestination(String destination) {
        this.destination = destination;
    }
    public String getTracedEntity() {
        return tracedEntity;
    }
    public void setTracedEntity(String tracedEntity) {
        this.tracedEntity = tracedEntity;
    }
}
