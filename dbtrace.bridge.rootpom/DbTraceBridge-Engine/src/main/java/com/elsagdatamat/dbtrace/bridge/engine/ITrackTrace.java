package com.elsagdatamat.dbtrace.bridge.engine;

import java.util.List;


public interface ITrackTrace<T> {

	void insertTrace(List<T> lst);

}
