/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectmodel;

 
import com.selexelsag.xte.testtool.datamodel.XtePreAccettazione;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Tassara
 */
public class XtePreAccettazioneTableModel extends DefaultTableModel {

    private String[] columnNames = new String[]{
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.codice"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.causale"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.destinazione"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.destinatario"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.valoreassicurato"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.peso1"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.peso2"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.numerosigilli"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.numerovaglia"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.espresso"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.cap"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.indirizzo"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.importocontrassegno"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.codicear"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.sa"),
        ResourceBundle.getBundle("bundles/messages").getString("preaccettazione.table.header.param"),
    };

    private Map<String, XtePreAccettazione> map = new HashMap<String, XtePreAccettazione>();
    
    public XtePreAccettazioneTableModel() {

        super();
        this.setColumnCount(columnNames.length);
        this.setColumnIdentifiers(columnNames);

    }

    public void addPreAccettazioneItem(XtePreAccettazione item) throws XteTestToolDuplicateKeyException{
        if(map.containsKey(item.getPreAccettazioneCodInvio()))
        {
            throw new XteTestToolDuplicateKeyException(Group.GENERIC, Severity.WARNING, "Accettazione Pezzi Table Model duplicate key exception");
        }
        else
        {
            addRow(new Object[]{item.getPreAccettazioneCodInvio(), item.getPreAccettazioneCausale(), item.getPreAccettazioneDestinazione(), item.getPreAccettazioneDestinatario(), item.getPreAccettazioneValAss(), item.getPreAccettazionePeso1(), item.getPreAccettazionePeso2(), item.getPreAccettazioneNumSigilli(), item.getPreAccettazioneNumVaglia(), item.getPreAccettazioneEspresso(), item.getPreAccettazioneCAP(), item.getPreAccettazioneIndirizzo(), item.getPreAccettazioneImpContr(), item.getPreAccettazioneCodAR(), item.getPreAccettazioneSA(), item.getPreAccettazionePARAM()});
            map.put(item.getPreAccettazioneCodInvio(), item);            
        }        
    }
    

    public void setModelData(Map<String, XtePreAccettazione> items) {
        getDataVector().clear();
        map.clear();                
        for (XtePreAccettazione item : items.values()) {
            addRow(new Object[]{item.getPreAccettazioneCodInvio(), item.getPreAccettazioneCausale(), item.getPreAccettazioneDestinazione(), item.getPreAccettazioneDestinatario(), item.getPreAccettazioneValAss(), item.getPreAccettazionePeso1(), item.getPreAccettazionePeso2(), item.getPreAccettazioneNumSigilli(), item.getPreAccettazioneNumVaglia(), item.getPreAccettazioneEspresso(), item.getPreAccettazioneCAP(), item.getPreAccettazioneIndirizzo(), item.getPreAccettazioneImpContr(), item.getPreAccettazioneCodAR(), item.getPreAccettazioneSA(), item.getPreAccettazionePARAM()});
            map.put(item.getPreAccettazioneCodInvio(), item);
        }
    }

    public XtePreAccettazione getSelectedItem(int rowIndex) {
        
        String PreAccettazioneCode = (String) getValueAt(rowIndex, 0);
        if(map.containsKey(PreAccettazioneCode))
        {
            return map.get(PreAccettazioneCode);
        }
        else
        {
            return null;
        }
    }

    @Override
    public void removeRow(int row) {
        String PreAccettazioneCode = (String) getValueAt(row, 0);
        if(map.containsKey(PreAccettazioneCode))
        {
            map.remove(PreAccettazioneCode);
        }        
        getDataVector().removeElementAt(row);
        fireTableDataChanged();
    }

    public void clearModel() {
        getDataVector().clear();
        map.clear();
    }

    public Map<String, XtePreAccettazione> getMap() {
        return map;
    }

    
    @Override
    public boolean isCellEditable(int row, int column) {
        return (false);
    }
    
}
