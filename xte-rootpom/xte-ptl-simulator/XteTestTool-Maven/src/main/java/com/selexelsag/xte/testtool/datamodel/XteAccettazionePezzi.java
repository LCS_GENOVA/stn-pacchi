/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.datamodel;

import java.math.BigInteger;
import java.util.Date;


/**
 *
 * @author Tassara
 */
public class XteAccettazionePezzi {
    
    private String AccettazionePezziTipo;
    private String AccettazionePezziIdOp;
    private String AccettazionePezziOffFraz;
    private String AccettazionePezziNumSped;
    private String AccettazionePezziCodeCliente;
    private String AccettazionePezziCode;
    private String AccettazionePezziProduct;
    private Date AccettazionePezziDateTime;
    private String AccettazionePezziPeso;
    private int AccettazionePezziPrezzo;
    private int AccettazionePezziPercetto;
    private String AccettazionePezziDestinazione;
    private String AccettazionePezziDestinatario;
    private String AccettazionePezziIndirizzo;
    private String AccettazionePezziCAP;
    private String AccettazionePezziNomeMitt;
    private String AccettazionePezziDestMitt;
    private String AccettazionePezziIndMitt;
    private String AccettazionePezziCAPMitt;
    private String AccettazionePezziSA;
    private int AccettazionePezziValAss;
    private int AccettazionePezziImpContr;
    private String AccettazionePezziCodAR;
    private String AccettazionePezziIBAN;
    private String AccettazionePezziOraTimeDef;


    public String getAccettazionePezziTipo() {
        return AccettazionePezziTipo;
    }

    public void setAccettazionePezziTipo(String AccettazionePezziTipo) {
        this.AccettazionePezziTipo = AccettazionePezziTipo;
    } 

    public String getAccettazionePezziIdOp() {
        return AccettazionePezziIdOp;
    }

    public void setAccettazionePezziIdOp(String AccettazionePezziIdOp) {
        this.AccettazionePezziIdOp = AccettazionePezziIdOp;
    } 

    public String getAccettazionePezziOffFraz() {
        return AccettazionePezziOffFraz;
    }

    public void setAccettazionePezziOffFraz(String AccettazionePezziOffFraz) {
        this.AccettazionePezziOffFraz = AccettazionePezziOffFraz;
    } 

    public String getAccettazionePezziNumSped() {
        return AccettazionePezziNumSped;
    }

    public void setAccettazionePezziNumSped(String AccettazionePezziNumSped) {
        this.AccettazionePezziNumSped = AccettazionePezziNumSped;
    } 
    
    public String getAccettazionePezziCodeCliente() {
        return AccettazionePezziCodeCliente;
    }

    public void setAccettazionePezziCodeCliente(String AccettazionePezziCodeCliente) {
        this.AccettazionePezziCodeCliente = AccettazionePezziCodeCliente;
    } 

    public String getAccettazionePezziCode() {
        return AccettazionePezziCode;
    }

    public void setAccettazionePezziCode(String AccettazionePezziCode) {
        this.AccettazionePezziCode = AccettazionePezziCode;
    }
    
    public String getAccettazionePezziProduct() {
        return AccettazionePezziProduct;
    }

    public void setAccettazionePezziProduct(String AccettazionePezziProduct) {
        this.AccettazionePezziProduct = AccettazionePezziProduct;
    }  
    
    public Date getAccettazionePezziDateTime() {
        return AccettazionePezziDateTime;
    }

    public void setAccettazionePezziDateTime(Date AccettazionePezziDateTime) {
        this.AccettazionePezziDateTime = AccettazionePezziDateTime;
    }
    
    
    public String getAccettazionePezziPeso() {
        return AccettazionePezziPeso;
    }

    public void setAccettazionePezziPeso(String AccettazionePezziPeso) {
        this.AccettazionePezziPeso = AccettazionePezziPeso;
    }  
    
    public int getAccettazionePezziPrezzo() {
        return AccettazionePezziPrezzo;
    }

    public void setAccettazionePezziPrezzo(int AccettazionePezziPrezzo) {
        this.AccettazionePezziPrezzo = AccettazionePezziPrezzo;
    }  
    
    public int getAccettazionePezziPercetto() {
        return AccettazionePezziPercetto;
    }

    public void setAccettazionePezziPercetto(int AccettazionePezziPercetto) {
        this.AccettazionePezziPercetto = AccettazionePezziPercetto;
    }  
    
    public String getAccettazionePezziDestinazione() {
        return AccettazionePezziDestinazione;
    }

    public void setAccettazionePezziDestinazione(String AccettazionePezziDestinazione) {
        this.AccettazionePezziDestinazione = AccettazionePezziDestinazione;
    }  
    
    public String getAccettazionePezziDestinatario() {
        return AccettazionePezziDestinatario;
    }

    public void setAccettazionePezziDestinatario(String AccettazionePezziDestinatario) {
        this.AccettazionePezziDestinatario = AccettazionePezziDestinatario;
    }  
    
    public String getAccettazionePezziIndirizzo() {
        return AccettazionePezziIndirizzo;
    }

    public void setAccettazionePezziIndirizzo(String AccettazionePezziIndirizzo) {
        this.AccettazionePezziIndirizzo = AccettazionePezziIndirizzo;
    }  
    
    public String getAccettazionePezziCAP() {
        return AccettazionePezziCAP;
    }

    public void setAccettazionePezziCAP(String AccettazionePezziCAP) {
        this.AccettazionePezziCAP = AccettazionePezziCAP;
    }  
    
    public String getAccettazionePezziNomeMitt() {
        return AccettazionePezziNomeMitt;
    }

    public void setAccettazionePezziNomeMitt(String AccettazionePezziNomeMitt) {
        this.AccettazionePezziNomeMitt = AccettazionePezziNomeMitt;
    }  
    
    public String getAccettazionePezziDestMitt() {
        return AccettazionePezziDestMitt;
    }

    public void setAccettazionePezziDestMitt(String AccettazionePezziDestMitt) {
        this.AccettazionePezziDestMitt = AccettazionePezziDestMitt;
    }  
    
    public String getAccettazionePezziIndMitt() {
        return AccettazionePezziIndMitt;
    }

    public void setAccettazionePezziIndMitt(String AccettazionePezziIndMitt) {
        this.AccettazionePezziIndMitt = AccettazionePezziIndMitt;
    }  
    
    public String getAccettazionePezziCAPMitt() {
        return AccettazionePezziCAPMitt;
    }

    public void setAccettazionePezziCAPMitt(String AccettazionePezziCAPMitt) {
        this.AccettazionePezziCAPMitt = AccettazionePezziCAPMitt;
    }  
    
    public String getAccettazionePezziSA() {
        return AccettazionePezziSA;
    }

    public void setAccettazionePezziSA(String AccettazionePezziSA) {
        this.AccettazionePezziSA = AccettazionePezziSA;
    }  
    
    public int getAccettazionePezziValAss() {
        return AccettazionePezziValAss;
    }

    public void setAccettazionePezziValAss(int AccettazionePezziValAss) {
        this.AccettazionePezziValAss = AccettazionePezziValAss;
    }  
    public int getAccettazionePezziImpContr() {
        return AccettazionePezziImpContr;
    }

    public void setAccettazionePezziImpContr(int AccettazionePezziImpContr) {
        this.AccettazionePezziImpContr = AccettazionePezziImpContr;
    } 
    public String getAccettazionePezziCodAR() {
        return AccettazionePezziCodAR;
    }

    public void setAccettazionePezziCodAR(String AccettazionePezziCodAR) {
        this.AccettazionePezziCodAR = AccettazionePezziCodAR;
    } 
    public String getAccettazionePezziIBAN() {
        return AccettazionePezziIBAN;
    }

    public void setAccettazionePezziIBAN(String AccettazionePezziIBAN) {
        this.AccettazionePezziIBAN = AccettazionePezziIBAN;
    } 
    
    public String getAccettazionePezziOraTimeDef() {
        return AccettazionePezziOraTimeDef;
    }

    public void setAccettazionePezziOraTimeDef(String AccettazionePezziOraTimeDef) {
        this.AccettazionePezziOraTimeDef = AccettazionePezziOraTimeDef;
    } 


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.AccettazionePezziCode != null ? this.AccettazionePezziCode.hashCode() : 0);
        hash = 97 * hash + (this.AccettazionePezziDateTime != null ? this.AccettazionePezziDateTime.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final XteAccettazionePezzi other = (XteAccettazionePezzi) obj;
        if ((this.AccettazionePezziCode == null) ? (other.AccettazionePezziCode != null) : !this.AccettazionePezziCode.equals(other.AccettazionePezziCode)) {
            return false;
        }
        if ((this.AccettazionePezziDateTime == null) ? (other.AccettazionePezziDateTime != null) : !this.AccettazionePezziDateTime.equals(other.AccettazionePezziDateTime)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "XteAccettazionePezziPR{" + "AccettazionePezziCode=" + AccettazionePezziCode + ", AccettazionePezziDateTime=" + AccettazionePezziDateTime + '}';
    }
    

}
