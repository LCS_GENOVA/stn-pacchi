package com.elsagdatamat.trackdb.toedwh.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.hibernate.SQLQuery;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.framework.search.Search;
import com.elsagdatamat.trackdb.toedwh.entities.Trace;
import com.elsagdatamat.trackdb.toedwh.entities.TraceDetail;
import com.elsagdatamat.trackdb.toedwh.vo.TraceDetailVO;
import com.elsagdatamat.trackdb.toedwh.vo.TraceVO;
import com.elsagdatamat.tt.trackdbws.beans.VO.EsiPackVO;

public class TraceDAO extends SpringHibernateJpaDAOBase<Trace, Long> implements ITraceDAO {

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<com.elsagdatamat.tt.trackdbws.beans.Trace> returnTraceListExecuteHqlQuery(String hqlQuery) {
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<EsiPackVO> getListOfEsiPackVO(String hqlQuery) {
		return null;
	}

	@Override
	public Trace merge(final Trace entity) {
		return (Trace) executeEntityManagerCallBack(new JpaCallback() {
			@Override
			public Object doInJpa(EntityManager em) throws PersistenceException {
				return em.merge(entity);
			}
		}, true);
	}

	@Override
	public Long getId(String hqlQuery) {
		Object result = getEntityManager().createNativeQuery(hqlQuery).getSingleResult();
		return (Long) result;
	}

	@Override
	public List<TraceVO> executeQuery(String hqlQuery) {
		List<Object> result = executeHqlQuery(hqlQuery);
		Map<Long, TraceVO> traces = new HashMap<Long, TraceVO>(result.size());
		for (Object o1 : result) {
			Object[] o = (Object[]) o1;
			Trace t = (Trace) o[0];
			TraceVO traceVO = traces.get(t.getTraceId());
			if (traceVO == null) {
				traceVO = new TraceVO(t);
				traces.put(t.getTraceId(), traceVO);
			}
			if (o[1] != null)
				traceVO.getDetailsVO().add(new TraceDetailVO((TraceDetail) o[1]));
		}
		List<TraceVO> tracesList = new ArrayList<TraceVO>(traces.values());
		Collections.sort(tracesList);
		return tracesList;

	}

	@Override
	public List<TraceVO> executeTraceQuery(String hqlQuery) {
		List<Object> result = executeHqlQuery(hqlQuery);
		Map<Long, TraceVO> traces = new HashMap<Long, TraceVO>(result.size());
		for (Object o1 : result) {

			Trace t = (Trace) o1;
			traces.put(t.getTraceId(), new TraceVO(t));

		}
		List<TraceVO> tracesList = new ArrayList<TraceVO>(traces.values());
		return tracesList;

	}

	public void deleteBatch(List<TraceVO> traces) {
		this.deleteBatch(traces);
	}

	@Override
	public int bulkDeleteTraces(Integer timeHorizon, String traceType) {
		Search condition = new Search();
		condition.addFilterLessThan("whenHappened", getDateLimit(timeHorizon));
		condition.addFilterEqual("tracedEntity", traceType);
		return this.deleteBatch(condition);
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public int deleteTraces(Integer timeHorizon) {
		String sqlQuery = "delete from Traces t where t.when_Happened < (sysdate- " + timeHorizon
				+ ") and exists ( select st.id from Sent_Trace st where st.Id = t.Id ) ";

		MyJpaCallback<Integer> myJpaCallbak = new MyJpaCallback<Integer>(sqlQuery);
		Integer res = (Integer) getJpaTemplate().execute(myJpaCallbak, true);
		log.debug("TraceDAO::deleteTraces - Deleted traces :" + res);
		return res;
	}

	/**
	 * Subtracts 'timeHorizon' days from the actual date
	 * 
	 * @param timeHorizon
	 * @return
	 */
	private Date getDateLimit(Integer timeHorizon) {
		Calendar cDateTo = Calendar.getInstance();
		cDateTo.add(Calendar.DAY_OF_YEAR, -timeHorizon.intValue());
		return cDateTo.getTime();
	}

	@SuppressWarnings({ "hiding", "deprecation", "rawtypes" })
	class MyJpaCallback<Integer> implements JpaCallback {
		String hqlQuery;

		public MyJpaCallback(String hqlQuery) {
			this.hqlQuery = hqlQuery;
		}

		@Override
		@Transactional(propagation = Propagation.SUPPORTS)
		public Object doInJpa(EntityManager em) throws PersistenceException {
			org.hibernate.Session session = ((HibernateEntityManager) em).getSession();
			SQLQuery query = session.createSQLQuery(hqlQuery);

			return query.executeUpdate();
		}
	}

}
