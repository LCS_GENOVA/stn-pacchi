<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:p="http://www.springframework.org/schema/p"
	xmlns:aop="http://www.springframework.org/schema/aop" xmlns:tx="http://www.springframework.org/schema/tx"
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
       http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-3.0.xsd
       http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-3.0.xsd">

	<bean id="dbTraceBridge-jmsTransactionManager" class="org.springframework.jms.connection.JmsTransactionManager">
		<property name="connectionFactory">
			<ref bean="dbTraceBridge-jmsConnectionFactory" />
		</property>
	</bean>

	<bean id="jmsTemplate_SSIP" scope="singleton" class="org.springframework.jms.core.JmsTemplate">
		<property name="connectionFactory" ref="dbTraceBridge-jmsConnectionFactory" />
		<property name="defaultDestination">
			<ref bean="destination" />
		</property>
		<property name="sessionTransacted" value="true" />
		<property name="sessionAcknowledgeModeName" value="SESSION_TRANSACTED" />
	</bean>
	
	<bean id="ssipTokenManager" class="com.selexelsag.dbtrace.bridge.to.ssip.manager.SSIPManager">
		<property name="dao" ref="tokenDAO" />
		<property name="generalDao" ref="dbTraceBridgeGeneralDAO" />
		<property name="minutesToRecoveryMinValue" value="${tossip.processing.minutesToRecoveryMinValue}" />
		<property name="minutesToRestartMinValue" value="${tossip.processing.minutesToRestartMinValue}" />
	</bean>

	<!-- JBOSS JNDI server -->
	<bean id="jndiTemplate" class="org.springframework.jndi.JndiTemplate">
		<property name="environment">
			<props>
				<prop key="java.naming.factory.initial">org.jnp.interfaces.NamingContextFactory</prop>
				<prop key="java.naming.provider.url">${tossip.messaging.nodes.url}</prop>
				<prop key="java.naming.factory.url.pkgs">org.jnp.interfaces:org.jboss.naming</prop>
				<prop key="jnp.localAddress">${tossip.messaging.nodes.localaddress}</prop>
			</props>
		</property>
	</bean>

	<!-- JMS connection factory -->
	<bean id="dbTraceBridge-jmsConnectionFactory" class="org.springframework.jndi.JndiObjectFactoryBean">
		<property name="jndiTemplate" ref="jndiTemplate" />
		<property name="jndiName">
			<value>${jndi.jms.connectionfactoryname}</value>
		</property>
	</bean>

	<bean id="destination" class="org.springframework.jndi.JndiObjectFactoryBean">
		<property name="jndiTemplate">
			<ref bean="jndiTemplate" />
		</property>
		<property name="jndiName">
			<value>${tossip.messaging.queuename}</value>
		</property>
	</bean>
	
	<bean id="ssipMessageBuilder" class="com.selexelsag.dbtrace.bridge.to.ssip.message.SSIPMessageBuilder">
		<property name="jmsTemplate">
			<ref bean="jmsTemplate_SSIP" />
		</property>
		<property name="xsdPath" ref="xsdClassPathResourceBean" />
		<property name="ssipManager" ref="ssipTokenManager" />
		<property name="discardedTraceManager">
			<ref bean="discardedTraceManager" />
		</property>
		<property name="saveTraces" value="${tossip.processing.savetraces}" />
		<property name="queryParams" value="${tossip.config.params.properties}" />
		<property name="callTimeout" value="120" />
		<property name="blToken" ref="blToken" />
		<property name="token" ref="ssipToken"/>
	</bean>
	
	<bean id="ssipToken" class="com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken">
			<property name="serviceId" value="DBTraceToSSIPBridge" />
	</bean>

	<bean id="dbTracceToSSIPBridge" class="com.elsagdatamat.dbtrace.bridge.wstojms.WsToJmsBridge">
		<property name="parameterManager" ref="parameterManager" />
		<property name="messageBuilder">
			<ref bean="ssipMessageBuilder" />
		</property>
		<property name="messageSender">
			<ref bean="ssipMessageBuilder" />
		</property>
		<property name="tracesReader">
			<ref bean="ssipMessageBuilder" />
		</property>
		<property name="wsUrl">
			<value>${tossip.ws.url}</value>
		</property>
		<property name="wsNamespaceURI">
			<value>${tossip.ws.namespaceURI}</value>
		</property>
		<property name="wsLocalpart">
			<value>${tossip.ws.localpart}</value>
		</property>
	</bean>

	<bean id="toSSIPJobDetail"
		class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
		<property name="targetObject" ref="dbTracceToSSIPBridge" />
		<property name="targetMethod" value="unicoMetodoDaChiamareDaQuartz" />
		<property name="concurrent" value="false" />
	</bean>

	<bean id="toSSIPTrigger" class="org.springframework.scheduling.quartz.SimpleTriggerBean">
		<property name="jobDetail" ref="toSSIPJobDetail" />
		<property name="startDelay">
			<value>${tossip.trigger.startdelay}</value>
		</property>
		<property name="repeatInterval">
			<value>${tossip.trigger.repeatinterval}</value>
		</property>
	</bean>

	<bean class="org.springframework.scheduling.quartz.SchedulerFactoryBean">
		<property name="triggers">
			<list>
				<ref bean="toSSIPTrigger" />
			</list>
		</property>
	</bean>
	
	<bean id="DbtraceBridgeToSSIPPropertyConfigurer"
		class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
		<property name="systemPropertiesModeName" value="SYSTEM_PROPERTIES_MODE_OVERRIDE" />
		<property name="ignoreResourceNotFound" value="true" />
		<property name="ignoreUnresolvablePlaceholders" value="true" />
		<property name="locations">
			<list>
				<value>${jboss.server.config.url}dbtrace-bridge-toSSIP.properties</value>
            	<value>${jboss.server.config.url}selexes/common/selexes-common.properties</value>				
			</list>
		</property>
	</bean>

	<bean id="classPathXsdLocationBean" class="java.lang.String">
		<constructor-arg type="java.lang.String" value="/xsd/SsipTTPoste_20120529.xsd"/>
	</bean>

	<bean id="xsdClassPathResourceBean" class="com.elsagdatamat.framework.resources.ClassPathResource">
		<constructor-arg>
			<ref bean="classPathXsdLocationBean" />
		</constructor-arg>
	</bean>

	<import resource="classpath:/applicationContext-bl.xml" />
	<import resource="classpath:/applicationContext-bl-dao.xml" />
	<import resource="classpath:/dbTraceBridge-applicationContext-dataSource-default.xml" />
	
</beans>
  