package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "APT_ITEM", schema="TT_APPLICATION")
public class AptItem{

    @Id
    @Column(name = "ITEM_CODE")
    private String code;
    @Column(name = "PTYPE")
    private String ptype;
    @Column(name = "SUBPTYPE", columnDefinition = "CHAR(4 BYTE)")
    private String subPtype;
    @Column(name = "RECIPIENT_NAME")
    private String receiverName;
    @Column(name = "RECIPIENT_EMAIL")
    private String receiverEmail;
    @Column(name = "RECIPIENT_PHONE")
    private String receiverPhoneNumber;
    @Column(name = "SENDER_EMAIL")
    private String senderEmail;
    @Column(name = "SENDER_PHONE")
    private String senderPhoneNumber;
    @Column(name = "WEIGHT")
    private int weight;
    @Column(name = "WIDTH")
    private int width;
    @Column(name = "DEPTH")
    private int depth;
    @Column(name = "HEIGHT")
    private int height;
    @Column(name = "EXPIRY")
    private Date expiry;
    @Column(name = "MISSING")
    @Type(type = "true_false")
    private Boolean missing;
    @Column(name = "MISSING_DATE")
    private Date missingDate;
    @Column(name = "PRICE")
    private int price;
    @Column(name = "SERVICE_PRICE")
    private int servicePrice;
    @Column(name = "TOTAL_PRICE")
    private int totalPrice;
    @Column(name = "DESTINATION")
    private String destination;
    @Column(name = "ZIP")
    private String zipCode;
    @Column(name = "ADDRESSEE")
    private String addressee;
    @Column(name = "DEST_ADDR")
    private String destAddressee;
    @Column(name = "CHECK_VALUE")
    private Integer checkValue;
    @Column(name = "INSURED_VALUE")
    private Integer insuredValue;
    //2015/01/14: nuovo campo per CR APT
    @Column(name = "O2_APT_SENT" )
    @Type(type = "true_false")
    private Boolean o2AptSent;

    public AptItem() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPtype() {
        return ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public String getReceiverPhoneNumber() {
        return receiverPhoneNumber;
    }

    public void setReceiverPhoneNumber(String receiverPhoneNumber) {
        this.receiverPhoneNumber = receiverPhoneNumber;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getSenderPhoneNumber() {
        return senderPhoneNumber;
    }

    public void setSenderPhoneNumber(String senderPhoneNumber) {
        this.senderPhoneNumber = senderPhoneNumber;
    }

    public String getSubPtype() {
        return subPtype;
    }

    public void setSubPtype(String subPtype) {
        this.subPtype = subPtype;
    }

    public Date getExpiry() {
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    public Boolean getMissing() {
        return missing;
    }

    public void setMissing(Boolean missing) {
        this.missing = missing;
    }

    public Date getMissingDate() {
        return missingDate;
    }

    public void setMissingDate(Date missingDate) {
        this.missingDate = missingDate;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(int servicePrice) {
        this.servicePrice = servicePrice;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddressee() {
        return addressee;
    }

    public void setAddressee(String addressee) {
        this.addressee = addressee;
    }

    public String getDestAddressee() {
        return destAddressee;
    }

    public void setDestAddressee(String destAddressee) {
        this.destAddressee = destAddressee;
    }

    public Integer getCheckValue() {
        return checkValue;
    }

    public void setCheckValue(Integer checkValue) {
        this.checkValue = checkValue;
    }

    public Integer getInsuredValue() {
        return insuredValue;
    }

    public void setInsuredValue(Integer insuredValue) {
        this.insuredValue = insuredValue;
    }

	public Boolean getO2AptSent() {
		return o2AptSent;
	}

	public void setO2AptSent(Boolean o2AptSent) {
		this.o2AptSent = o2AptSent;
	}

}
