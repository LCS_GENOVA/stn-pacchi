package com.elsagdatamat.tt.trackdbws.beans;


import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "TRACES")

public class Trace implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID", nullable=false)
	@SequenceGenerator(name="TRA_SEQ", sequenceName="TRA_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRA_SEQ")
	private Long traceId;

	@Column(name="LABEL_TRACED_ENTITY", nullable=true)
	private String labelTracedEntity;

	@Column(name="TRACED_ENTITY", nullable=false)
	private String tracedEntity;

	@Column(name="ID_TRACED_ENTITY", nullable=false)
	private String idTracedEntity;

	@Column(name="WHEN_HAPPENED", nullable=false)
	private Date whenHappened;

	@Column(name="WHEN_REGISTERED", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date whenRegistered;

	@Column(name="WHERE_HAPPENED", nullable=false)
	private String whereHappened;

	@Column(name="CHANNEL", nullable=false)
	private String channel;

	@Column(name="ID_CHANNEL", nullable=false)
	private String idChannel;

	@Column(name="WHAT_HAPPENED", nullable=false)
	private String whatHappened;

	@Column(name="SERVICE_NAME", nullable=false)
	private String serviceName;

	@Transient
	private Map<String, String> detailList;

	@OneToMany(cascade=CascadeType.ALL, mappedBy="trace", fetch=FetchType.EAGER)
	private Set<TraceDetail> internalDetailList;


	// Aggiunto R.L.- 02-05-2012
	@Column(name="ID_STATUS", nullable=true)
	private String idStatus;
	@Column(name="ID_CORRELAZIONE", nullable=true)
	private String idCorrelazione;
	@Column(name="ID_TRACED_EXTERNAL", nullable=true)
	private String idTracedExternal;
	// Aggiunto R.L.- 03-05-2012
	@Column(name="ID_FORWARD_TO", nullable=true)
	private String idForwardTo;
	@Column(name="SYS_FORWARD_TO", nullable=true)
	private String sysForwardTo;

	public long getIdTrace() {
		return traceId;
	}
	public void setTracedEntity(String tracedEntity) {
		this.tracedEntity = tracedEntity;
	}
	public String getTracedEntity() {
		return tracedEntity;
	}
	public void setIdTracedEntity(String idTracedEntity) {
		this.idTracedEntity = idTracedEntity;
	}
	public String getIdTracedEntity() {
		return idTracedEntity;
	}
	public void setWhenHappened(Date whenHappened) {
		this.whenHappened = whenHappened;
	}
	public Date getWhenHappened() {
		return whenHappened;
	}
	public void setWhenRegistered(Date whenRegistered) {
		this.whenRegistered = whenRegistered;
	}
	public Date getWhenRegistered() {
		return whenRegistered;
	}
	public void setWhereHappened(String whereHappened) {
		this.whereHappened = whereHappened;
	}
	public String getWhereHappened() {
		return whereHappened;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getChannel() {
		return channel;
	}
	public void setIdChannel(String idChannel) {
		this.idChannel = idChannel;
	}
	public String getIdChannel() {
		return idChannel;
	}
	public void setWhatHappened(String whatHappened) {
		this.whatHappened = whatHappened;
	}
	public String getWhatHappened() {
		return whatHappened;
	}
	/*
	public void setDetailList(Map<String, String> detailList) {
		this.detailList = detailList;
	}
	*/
	@Transient
	public Map<String, String> getDetailList()
	{
		if (detailList == null)
		{
			detailList = new HashMap<String, String>();
			for (TraceDetail traceDetail : internalDetailList)
			{
				detailList.put(traceDetail.getParamClass(), traceDetail.getParamValue());
			}
		}
		return detailList;
	}

	public Set<TraceDetail> getInternalDetailList() {
		return internalDetailList;
	}
	public void setInternalDetailList(Set<TraceDetail> internalDetailList) {
		this.internalDetailList = internalDetailList;
	}
	public Long getTraceId() {
		return traceId;
	}
	public void setTraceId(Long traceId) {
		this.traceId = traceId;
	}
	public String getLabelTracedEntity() {
		return labelTracedEntity;
	}
	public void setLabelTracedEntity(String labelTracedEntity) {
		this.labelTracedEntity = labelTracedEntity;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	// Aggiunto R.L.- 02-05-2012
	public String getIdStatus() {
		return idStatus;
	}
	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}
	public String getIdTracedExternal() {
		return idTracedExternal;
	}
	public void setIdTracedExternal(String idTracedExternal) {
		this.idTracedExternal = idTracedExternal;
	}
	public String getIdCorrelazione() {
		return idCorrelazione;
	}
	public void setIdCorrelazione(String idCorrelazione) {
		this.idCorrelazione = idCorrelazione;
	}
	// Aggiunto R.L.- 02-05-2012
	public String getIdForwardTo() {
		return idForwardTo;
	}
	public void setIdForwardTo(String idForwardTo) {
		this.idForwardTo = idForwardTo;
	}
	public String getSysForwardTo() {
		return sysForwardTo;
	}
	public void setSysForwardTo(String sysForwardTo) {
		this.sysForwardTo = sysForwardTo;
	}

}
