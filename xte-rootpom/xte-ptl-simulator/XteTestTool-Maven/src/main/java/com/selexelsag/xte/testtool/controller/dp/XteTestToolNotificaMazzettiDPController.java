/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.dp;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.controller.common.XteTestToolFieldValidator;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XteMailPiece;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.dp.XteTestToolNotificaMazzettiDPView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;
import com.selexelsag.xte.testtool.view.dp.XteTestToolWebServiceDPView;
import com.selexelsag.xte.testtool.view.objectmodel.XteMailPieceTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteTestToolDuplicateKeyException;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta.NM;
import com.selexelsag.xtetesttool.xml.dataposta.nm.DatiInvio;
import com.selexelsag.xtetesttool.xml.dataposta.nm.ObjectFactory;
import com.selexelsag.xtetesttool.xml.dataposta.nm.Traccia;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import org.apache.commons.configuration.Configuration;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolNotificaMazzettiDPController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener, ListSelectionListener, TableModelListener {

    private XteTestToolNotificaMazzettiDPView notificaMazzettiDPView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolNotificaMazzettiDPController.class);
    private XteMailPiece selectedMailPiece = null;
    private int currentSelectedRow = -1;
    private List<JTextComponent> mailPieceFieldList = new ArrayList<JTextComponent>();
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    
    private JAXBContext context;
    
    private static Traccia notificaMazzettiTraccia;

    static {
        logger.debug("Creating Config Object Factory and Traccia");
        DataPosta dataPostaConfig = XteTestToolConfiguration.getInstance().getDataPostaConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        NM config = dataPostaConfig.getNM();

        notificaMazzettiTraccia = objectFactory.createTraccia();
        notificaMazzettiTraccia.setEvento(config.getEventName());
        notificaMazzettiTraccia.setHeaderMazzetto(objectFactory.createHeaderMazzetto());
        notificaMazzettiTraccia.setListaInvii(objectFactory.createListaInvii());
    }

    public XteTestToolNotificaMazzettiDPController(JPanel notificaMazzettiDPView) {
        try {
            logger.info("XteTestToolNotificaMazzettiDPController start class creation");
            this.notificaMazzettiDPView = (XteTestToolNotificaMazzettiDPView) notificaMazzettiDPView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.notificaMazzettiDPView.initializeView();
	
            this.notificaMazzettiDPView.getMailPieceTypeComboBoxRef().addItem("S");
            this.notificaMazzettiDPView.getMailPieceTypeComboBoxRef().addItem("N");

            updateView();

            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.dataposta.nm");

            logger.info("XteTestToolNotificaMazzettiDPController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolNotificaMazzettiDPController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolNotificaMazzettiDPController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Data Posta controller creation failed", ex);
        }
    }
    
    private void updateDataModel() {
        String id = notificaMazzettiDPView.getBundleIdTextRef().getText();
        String opid = notificaMazzettiDPView.getBundleTPIdTextRef().getText();
        logger.debug("updateDataModel - " + id + "," + opid);
        notificaMazzettiTraccia.getHeaderMazzetto().setCodiceIdentificativo(new String(id));
        notificaMazzettiTraccia.getHeaderMazzetto().setIdOperatoreTP(new String(opid));
        ObjectFactory objectFactory = new ObjectFactory();
        XteMailPiece[] entries = notificaMazzettiDPView.getMailPieceTableModelRef().getMap().values().toArray(new XteMailPiece[0]);
        logger.debug("entries count: " + entries.length);        
        notificaMazzettiTraccia.getListaInvii().getDatiInvio().clear();
        for (int i=0;i<entries.length;i++) {
        
            XteMailPiece item = entries[i];
            logger.debug("item " + i + ": " + item);
            if (item == null) 
                continue;
            DatiInvio obj = objectFactory.createDatiInvio();
            obj.setCodice(item.getMailPieceCode());
            obj.setCausale(item.getMailPieceProduct());
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(item.getMailPieceDateTime());
            try {
                obj.setDataOraNotifica(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            } catch (DatatypeConfigurationException ex) {
                logger.error("Error converting date " + c, ex);
                obj.setDataOraNotifica(null);
            }
            obj.setLatitudine(item.getMailPieceLatitude());
            obj.setLongitudine(item.getMailPieceLongitude());
            obj.setGPS(item.getMailGPS());
            // obj.setAltroUfficio(item.getMailPieceOffice());
            obj.setTipoCodice(item.getMailPieceType());
            notificaMazzettiTraccia.getListaInvii().getDatiInvio().add(obj);
        }
    }
    
    
    private void updateView() {  
        String id = notificaMazzettiTraccia.getHeaderMazzetto().getCodiceIdentificativo();
        String opid = notificaMazzettiTraccia.getHeaderMazzetto().getIdOperatoreTP();
        logger.debug("updateView - " + id + "," + opid);
                
        if (id != null) {
            notificaMazzettiDPView.getBundleIdTextRef()
                    .setText(id);
        }

        if (opid != null) {
            notificaMazzettiDPView.getBundleTPIdTextRef()
                    .setText(opid);
        }

        notificaMazzettiDPView.getMailPieceTableModelRef().clearModel();
            
        for (int i=0;i<notificaMazzettiTraccia.getListaInvii().getDatiInvio().size();i++) {
            DatiInvio item = notificaMazzettiTraccia.getListaInvii().getDatiInvio().get(i);
            XteMailPiece obj = new XteMailPiece();
            obj.setMailPieceCode(item.getCodice());
            obj.setMailPieceDateTime(item.getDataOraNotifica().toGregorianCalendar().getTime());
            obj.setMailPieceLatitude(item.getLatitudine());
            obj.setMailPieceLongitude(item.getLongitudine());
            obj.setMailPieceType(item.getTipoCodice());
            obj.setMailPieceProduct(item.getCausale());
            obj.setMailPieceGPS(item.getGPS());
            notificaMazzettiDPView.getMailPieceTableModelRef().addMailPieceItem(obj);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = notificaMazzettiDPView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        notificaMazzettiDPView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.notificaMazzettiDPView.getNotificaMazzettiPanelRef().addAncestorListener(this);        
        this.notificaMazzettiDPView.getBundleIdTextRef().addFocusListener(this);
        this.notificaMazzettiDPView.getBundleTPIdTextRef().addFocusListener(this);
        //this.notificaMazzettiDPView.getMailPieceTypeComboBoxRef().addFocusListener(this);
        this.notificaMazzettiDPView.getMailPieceCodeTextRef().addFocusListener(this);
        this.notificaMazzettiDPView.getMailPieceProductTextRef().addFocusListener(this);
        this.notificaMazzettiDPView.getMailPieceTypeComboBoxRef().addActionListener(this);
        this.notificaMazzettiDPView.getMailPieceDatePickerRef().getEditor().addFocusListener(this);
        JFormattedTextField startTimejFormattedTextField = ((JSpinner.DateEditor)notificaMazzettiDPView.getMailPieceTimeSpinnerRef().getEditor()).getTextField();
        startTimejFormattedTextField.addFocusListener(this);
        this.notificaMazzettiDPView.getMailPieceLatitudeTextRef().addFocusListener(this);
        this.notificaMazzettiDPView.getMailPieceLongitudeTextRef().addFocusListener(this);
        this.notificaMazzettiDPView.getMailPieceOfficeTextRef().addFocusListener(this);
        this.notificaMazzettiDPView.getMailPieceTableRef().getSelectionModel().addListSelectionListener(this);
        this.notificaMazzettiDPView.getMailPieceTableRef().getModel().addTableModelListener(this);
        this.notificaMazzettiDPView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.notificaMazzettiDPView.getAggiungiItemButtonRef().addActionListener(this);
        this.notificaMazzettiDPView.getEliminaItemButtonRef().addActionListener(this);
        this.notificaMazzettiDPView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.notificaMazzettiDPView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.notificaMazzettiDPView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.notificaMazzettiDPView.getBackButtonRef().addActionListener(this);
        this.notificaMazzettiDPView.getHomeButtonRef().addActionListener(this);
        this.notificaMazzettiDPView.getExitButtonRef().addActionListener(this);
                
        this.notificaMazzettiDPView.getMailPieceCodeTextRef().getDocument().addDocumentListener(this);
        this.notificaMazzettiDPView.getMailPieceProductTextRef().getDocument().addDocumentListener(this);
        this.notificaMazzettiDPView.getMailPieceDatePickerRef().getEditor().getDocument().addDocumentListener(this);
        ((JSpinner.DateEditor)notificaMazzettiDPView.getMailPieceTimeSpinnerRef().getEditor()).getTextField().getDocument().addDocumentListener(this);
        this.notificaMazzettiDPView.getMailPieceLatitudeTextRef().getDocument().addDocumentListener(this);
        this.notificaMazzettiDPView.getMailPieceLongitudeTextRef().getDocument().addDocumentListener(this);
        this.notificaMazzettiDPView.getMailPieceOfficeTextRef().getDocument().addDocumentListener(this);
        
        mailPieceFieldList.add(notificaMazzettiDPView.getMailPieceCodeTextRef());
        mailPieceFieldList.add(notificaMazzettiDPView.getMailPieceProductTextRef());
        mailPieceFieldList.add(notificaMazzettiDPView.getMailPieceDatePickerRef().getEditor());
        mailPieceFieldList.add(((JSpinner.DateEditor)notificaMazzettiDPView.getMailPieceTimeSpinnerRef().getEditor()).getTextField());
        mailPieceFieldList.add(notificaMazzettiDPView.getMailPieceLatitudeTextRef());
        mailPieceFieldList.add(notificaMazzettiDPView.getMailPieceLongitudeTextRef());
        //mailPieceFieldList.add(notificaMazzettiDPView.getMailPieceOfficeTextRef());
        
		this.salvaXmlFileChooser.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(notificaMazzettiDPView.getMailPieceTypeComboBoxRef()))
        {
            checkMailPieceFieldsFull();
        }
        if(source.equals(notificaMazzettiDPView.getAggiungiItemButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiDPController actionPerformed invoke addMailPieceItem");
			
            updateDataModel();

            addMailPieceItem();
        }
        
        if(source.equals(notificaMazzettiDPView.getEliminaItemButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiDPController actionPerformed invoke removeMailPieceItem");

            updateDataModel();

            removeMailPieceItem();
        }

        if(source.equals(notificaMazzettiDPView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiDPController actionPerformed invoke generaXmlData");

			updateDataModel();

            generaXmlData();
        }

        if(source.equals(notificaMazzettiDPView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiDPController actionPerformed invoke impostaDatiWebService");

			updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(notificaMazzettiDPView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiDPController actionPerformed invoke salvaXmlData");

			updateDataModel();

            salvaXmlData();
        }

        if(source.equals(notificaMazzettiDPView.getBackButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiDPController actionPerformed invoke back");

			updateDataModel();

            back();
        }

        if(source.equals(notificaMazzettiDPView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiDPController actionPerformed invoke home");

			updateDataModel();

            home();
        }

        if(source.equals(notificaMazzettiDPView.getExitButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiDPController actionPerformed invoke exit");

			updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(notificaMazzettiDPView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolNotificaMazzettiDPView)
        {
            logger.debug("XteTestToolNotificaMazzettiDPController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiDPController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Data Posta controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiDPController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Data Posta controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        checkMailPieceFieldsFull();  
    }    
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object source = e.getSource();
        if(source.equals(notificaMazzettiDPView.getMailPieceTableRef().getSelectionModel()))
        {               
            if (e.getValueIsAdjusting()) {
                    return;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
                        currentSelectedRow = notificaMazzettiDPView.getMailPieceTableRef().getSelectedRow();
                        logger.debug("XteTestToolNotificaMazzettiDPController valueChanged currentSelectedRow = " + currentSelectedRow);
                        logger.debug("XteTestToolNotificaMazzettiDPController valueChanged: " + ((XteMailPieceTableModel)notificaMazzettiDPView.getMailPieceTableRef().getModel()).getMap().size());
                        if(currentSelectedRow!=-1)
                        {
                            List<Component> enableComponents = new ArrayList<Component>();
                            enableComponents.add(notificaMazzettiDPView.getEliminaItemButtonRef());
                            componentStatusManager.enableComponent(enableComponents);
                            selectedMailPiece = ((XteMailPieceTableModel)notificaMazzettiDPView.getMailPieceTableRef().getModel()).getSelectedItem(currentSelectedRow);
                        }
                        else
                        {
                            List<Component> disableComponents = new ArrayList<Component>();
                            disableComponents.add(notificaMazzettiDPView.getEliminaItemButtonRef());
                            componentStatusManager.disableComponent(disableComponents);                                                        
                            selectedMailPiece = null;
                        }
                    }
                } );
        }
        
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int eventType = e.getType();
        DefaultTableModel model = (DefaultTableModel) e.getSource();
        if(model.equals(notificaMazzettiDPView.getMailPieceTableRef().getModel()))
        {               
            logger.debug("XteTestToolNotificaMazzettiDPController tableChanged: " + ((XteMailPieceTableModel)notificaMazzettiDPView.getMailPieceTableRef().getModel()).getMap().size());
        }
    }

    private void checkMailPieceFieldsFull() {

        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        for (JTextComponent field : mailPieceFieldList) {
            if (field.getText().trim().isEmpty())
            {
                List<Component> disableComponents = new ArrayList<Component>();
                disableComponents.add(notificaMazzettiDPView.getAggiungiItemButtonRef());
                componentStatusManager.disableComponent(disableComponents);                                                        
                return;
            }
        }
        
        if(notificaMazzettiDPView.getMailPieceTypeComboBoxRef().getSelectedIndex()==-1)
        {
            List<Component> disableComponents = new ArrayList<Component>();
            disableComponents.add(notificaMazzettiDPView.getAggiungiItemButtonRef());
            componentStatusManager.disableComponent(disableComponents);                                                        
            return;            
        }

        List<Component> enableComponents = new ArrayList<Component>();
        enableComponents.add(notificaMazzettiDPView.getAggiungiItemButtonRef());
        componentStatusManager.enableComponent(enableComponents);
    }

    private void clearMailPieceFields() {

        notificaMazzettiDPView.getMailPieceTypeComboBoxRef().setSelectedIndex(-1);

        for (JTextComponent field : mailPieceFieldList) {
            if (!field.getText().trim().isEmpty()) {
                field.setText("");
            }
        }
        notificaMazzettiDPView.getMailPieceGPSCoordsCheckBoxRef().setSelected(false);
        notificaMazzettiDPView.getMailPieceOfficeTextRef().setText("");
    }
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        notificaMazzettiDPView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolNotificaMazzettiDPController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.notificaMazzettiDPView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(notificaMazzettiDPView);
            updateXteTestToolDataModel(dataModel);

            clearMailPieceFields();
            clearMessageBar();
            
            Configuration configuration = XteTestToolConfiguration.getInstance().getConfiguration();
            List<String> mailPieceTypeList = configuration.getList("DataPosta.MailPieceType.List");
            DefaultComboBoxModel model = new DefaultComboBoxModel(mailPieceTypeList.toArray());                                    
            notificaMazzettiDPView.getMailPieceTypeComboBoxRef().setModel(model);
            notificaMazzettiDPView.getMailPieceTypeComboBoxRef().setSelectedIndex(-1);

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(notificaMazzettiDPView.getAggiungiItemButtonRef());
            disabledComponents.add(notificaMazzettiDPView.getEliminaItemButtonRef());
            disabledComponents.add(notificaMazzettiDPView.getImpostaDatiWSButtonRef());
            disabledComponents.add(notificaMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            Calendar date = Calendar.getInstance();
            this.notificaMazzettiDPView.getMailPieceDatePickerRef().setDate(date.getTime());
            this.notificaMazzettiDPView.getMailPieceTimeSpinnerRef().setValue(date.getTime());

            /*
            XteMailPiece item = new XteMailPiece();
            item.setMailPieceType("N");
            item.setMailPieceCode("1234567890121");
            item.setMailPieceProduct("Racc");
            
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss.SSS");
            //SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            
            Date startDate = this.notificaMazzettiDPView.getMailPieceDatePickerRef().getDate();

            SpinnerDateModel startTimeSpinnerDateModel = (SpinnerDateModel) this.notificaMazzettiDPView.getMailPieceTimeSpinnerRef().getModel();
            Date startTime = startTimeSpinnerDateModel.getDate();

            String startDateStr = dateFormatter.format(startDate);
            String startTimeStr = timeFormatter.format(startTime);
            String startDateTimeStr = startDateStr + "T" + startTimeStr;
            
            item.setMailPieceDateTime(startDate);
            item.setMailPieceLatitude(123.123f);
            item.setMailPieceLongitude(12.2f);
            item.setMailPieceOffice("Altro ufficio 1");

            XteMailPiece item2 = new XteMailPiece();
            item2.setMailPieceType("N");
            item2.setMailPieceCode("1234567890122");
            item2.setMailPieceProduct("Racc");                        
            item2.setMailPieceDateTime(startDate);
            item2.setMailPieceLatitude(123.123f);
            item2.setMailPieceLongitude(12.2f);
            item2.setMailPieceOffice("Altro ufficio 2");

            XteMailPiece item3 = new XteMailPiece();
            item3.setMailPieceType("P");
            item3.setMailPieceCode("1234567890123");
            item3.setMailPieceProduct("Racc");                        
            item3.setMailPieceDateTime(startDate);
            item3.setMailPieceLatitude(123.123f);
            item3.setMailPieceLongitude(12.2f);
            item3.setMailPieceOffice("Altro ufficio 3");
            
            this.notificaMazzettiDPView.getMailPieceTableModelRef().addMailPieceItem(item);
            this.notificaMazzettiDPView.getMailPieceTableModelRef().addMailPieceItem(item2);
            this.notificaMazzettiDPView.getMailPieceTableModelRef().addMailPieceItem(item3);
 
            */
            notificaMazzettiDPView.getBundleIdTextRef().requestFocus();

            logger.info("XteTestToolNotificaMazzettiDPController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolNotificaMazzettiDPController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiDPController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Data Posta controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolNotificaMazzettiDPController start exit");
        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(notificaMazzettiDPView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolNotificaMazzettiDPController exit in progress");
            notificaMazzettiDPView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolNotificaMazzettiDPController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(notificaMazzettiDPView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(notificaMazzettiDPView.getNotificaMazzettiPanelRef().getParent(), notificaMazzettiDPView.getNotificaMazzettiPanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolNotificaMazzettiDPController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(notificaMazzettiDPView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(notificaMazzettiDPView.getNotificaMazzettiPanelRef().getParent(), notificaMazzettiDPView.getNotificaMazzettiPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private void addMailPieceItem()
    {
        logger.info("XteTestToolNotificaMazzettiDPController start addMailPieceItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            
            clearMessageBar();
            
            XteTestToolMessageBar messageBar = new XteTestToolMessageBar();

            XteTestToolFieldValidator validator = new XteTestToolFieldValidator();            
            boolean isLatitude = validator.validateLatitude(notificaMazzettiDPView.getMailPieceLatitudeTextRef().getText());
            if(!isLatitude)
            {                
                StyledDocument document = messageBar.putContentOnMessageBar("Latitude incorrect format!", MessageType.WARNING);
                notificaMazzettiDPView.getMessageBarTextPaneRef().setDocument(document);
                notificaMazzettiDPView.getMailPieceLatitudeTextRef().requestFocus();
                return;
            }
            
            boolean isLongitude = validator.validateLongitude(notificaMazzettiDPView.getMailPieceLongitudeTextRef().getText());
            if(!isLongitude)
            {
                StyledDocument document = messageBar.putContentOnMessageBar("Longitude incorrect format!", MessageType.WARNING);
                notificaMazzettiDPView.getMessageBarTextPaneRef().setDocument(document);
                notificaMazzettiDPView.getMailPieceLongitudeTextRef().requestFocus();
                return;
            }
                                    
            XteMailPiece item = new XteMailPiece();
            
            String selectedItem = (String) notificaMazzettiDPView.getMailPieceTypeComboBoxRef().getModel().getSelectedItem();
            
            item.setMailPieceType(selectedItem);
            item.setMailPieceCode(notificaMazzettiDPView.getMailPieceCodeTextRef().getText());
            item.setMailPieceProduct(notificaMazzettiDPView.getMailPieceProductTextRef().getText());
            
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss.SSS");
            //SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");            
            Date startDate = this.notificaMazzettiDPView.getMailPieceDatePickerRef().getDate();
            SpinnerDateModel startTimeSpinnerDateModel = (SpinnerDateModel) this.notificaMazzettiDPView.getMailPieceTimeSpinnerRef().getModel();
            Date startTime = startTimeSpinnerDateModel.getDate();
            String startDateStr = dateFormatter.format(startDate);
            String startTimeStr = timeFormatter.format(startTime);
            String startDateTimeStr = startDateStr + "T" + startTimeStr;
            SimpleDateFormat lasttimeFormatter = new SimpleDateFormat("yyyy-MM-dd" + "'T'" + "HH:mm:ss");
            item.setMailPieceDateTime((Date) lasttimeFormatter.parse(startDateTimeStr));
            
            DecimalFormat numberFormat = new DecimalFormat();
            
             
            Float latitude = null;
            
            try {
                latitude = numberFormat.parse(notificaMazzettiDPView.getMailPieceLatitudeTextRef().getText()).floatValue();
            }catch (NumberFormatException nfe) {
                logger.error("invalid value for float " + notificaMazzettiDPView.getMailPieceLatitudeTextRef().getText(), nfe);                
            }
            item.setMailPieceLatitude(latitude);

            Float longitude = null;
            try {
                longitude = numberFormat.parse(notificaMazzettiDPView.getMailPieceLongitudeTextRef().getText()).floatValue();
            }catch (NumberFormatException nfe) {
                logger.error("invalid value for float " + notificaMazzettiDPView.getMailPieceLongitudeTextRef().getText(), nfe);                
            }
            item.setMailPieceLongitude(longitude);
            item.setMailPieceGPS(notificaMazzettiDPView.getMailPieceGPSCoordsCheckBoxRef().isSelected() ? "S" : "N");
            item.setMailPieceOffice(notificaMazzettiDPView.getMailPieceOfficeTextRef().getText());

            try 
            {
                this.notificaMazzettiDPView.getMailPieceTableModelRef().addMailPieceItem(item);
            }
            catch (XteTestToolDuplicateKeyException duplicateEx)
            {
                StyledDocument document = messageBar.putContentOnMessageBar("Attemp to insert duplicate key Item!", MessageType.WARNING);
                notificaMazzettiDPView.getMessageBarTextPaneRef().setDocument(document);
                this.notificaMazzettiDPView.getMailPieceCodeTextRef().requestFocus();
                return;
            }
            
            StyledDocument document = messageBar.putContentOnMessageBar("Item added successfully!", MessageType.CORRECT_PROCESSING);
            notificaMazzettiDPView.getMessageBarTextPaneRef().setDocument(document);
                        
            clearMailPieceFields();
            notificaMazzettiDPView.getMailPieceCodeTextRef().requestFocus();            
            
            logger.info("XteTestToolNotificaMazzettiDPController addMailPieceItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolNotificaMazzettiDPController addMailPieceItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiDPController addMailPieceItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Data Posta controller addMailPieceItem failed", ex);
        }
    }
    
    private void removeMailPieceItem()
    {
        logger.info("XteTestToolNotificaMazzettiDPController start removeMailPieceItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            if(currentSelectedRow!=-1)
            {                
                XteMailPieceTableModel model = ((XteMailPieceTableModel)notificaMazzettiDPView.getMailPieceTableRef().getModel());
                model.removeRow(currentSelectedRow);
                logger.debug("XteTestToolNotificaMazzettiDPController removeMailPieceItem: " + ((XteMailPieceTableModel)notificaMazzettiDPView.getMailPieceTableRef().getModel()).getMap().size());
            }

            logger.info("XteTestToolNotificaMazzettiDPController removeMailPieceItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolNotificaMazzettiDPController removeMailPieceItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiDPController removeMailPieceItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Data Posta controller removeMailPieceItem failed", ex);
        }
    }

	private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }

    private void generaXmlData()
    {
        logger.info("XteTestToolNotificaMazzettiDPController start generaXmlData");

        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();

        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            notificaMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(notificaMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(notificaMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            notificaMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(notificaMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(notificaMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(notificaMazzettiTraccia, out);
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            notificaMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(notificaMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(notificaMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
            
        try
        {            
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString());
            notificaMazzettiDPView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("XteTestToolNotificaMazzettiDPController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolNotificaMazzettiDPController generaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiDPController generaXmlData failed", ex);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            notificaMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(notificaMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(notificaMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Data Posta controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        notificaMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(notificaMazzettiDPView.getImpostaDatiWSButtonRef());
        enableComponent.add(notificaMazzettiDPView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolNotificaMazzettiDPController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServiceDPView(notificaMazzettiDPView.getParentFrame());
            XteTestToolWebServiceDPController controller = new XteTestToolWebServiceDPController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            DataPosta dataPostaConfig = XteTestToolConfiguration.getInstance().getDataPostaConfig();
            NM config = dataPostaConfig.getNM();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(notificaMazzettiDPView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(notificaMazzettiDPView.getNotificaMazzettiPanelRef().getParent(), notificaMazzettiDPView.getNotificaMazzettiPanelRef(), webserviceview, new Rectangle(0,70,1000,690));

            logger.info("XteTestToolNotificaMazzettiDPController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolNotificaMazzettiDPController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiDPController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Data Posta controller impostaDatiWebService failed", ex);
        }
    }
    
    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolNotificaMazzettiDPController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolNotificaMazzettiDPController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(notificaMazzettiDPView.getParentFrame());

            logger.info("XteTestToolNotificaMazzettiDPController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolNotificaMazzettiDPController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiDPController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Data Posta controller salvaXmlData failed", ex);
        }
    }
}
