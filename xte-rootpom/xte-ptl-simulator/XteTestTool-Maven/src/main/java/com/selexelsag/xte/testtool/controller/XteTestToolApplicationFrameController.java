/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.view.XteTestToolContentPaneView;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.Timer;
import org.apache.log4j.Logger;
/**
 *
 * @author Da Procida
 */
public class XteTestToolApplicationFrameController implements XteTestToolController, WindowListener{
    private Timer timestamp;

    private XteTestToolDataModel applicationFrameModel;
    private XteTestToolApplicationFrameView  applicationFrameView;
    private static Logger logger = Logger.getLogger(XteTestToolApplicationFrameController.class);

    public XteTestToolApplicationFrameController(XteTestToolDataModel applicationFrameModel, XteTestToolApplicationFrameView applicationFrameView) {
        logger.info("XteTestToolApplicationFrameController start class creation");
        this.applicationFrameModel = applicationFrameModel;
        this.applicationFrameView  = applicationFrameView;
        addViewObjectsListeners();
        logger.info("XteTestToolApplicationFrameController class created successfully");
    }
    
    @Override
    public void addViewObjectsListeners() {

        this.applicationFrameView.addWindowListener(this);
    }


    private class ImageClockListener implements ActionListener
    {
        @Override
    	public void actionPerformed(ActionEvent e)
      {
            Date dateTime = Calendar.getInstance().getTime();
            DateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
            String timestamp = timeFormatter.format(dateTime);
            DateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
            String date = dateFormatter.format(dateTime);
            applicationFrameView.getContentPaneView().getTotemGateTimeHour1LabelRef().setIcon(applicationFrameView.getContentPaneView().getImageTimeMap().get(timestamp.charAt(0)));
            applicationFrameView.getContentPaneView().getTotemGateTimeHour2LabelRef().setIcon(applicationFrameView.getContentPaneView().getImageTimeMap().get(timestamp.charAt(1)));
            applicationFrameView.getContentPaneView().getTotemGateTimeMinute1LabelRef().setIcon(applicationFrameView.getContentPaneView().getImageTimeMap().get(timestamp.charAt(3)));
            applicationFrameView.getContentPaneView().getTotemGateTimeMinute2LabelRef().setIcon(applicationFrameView.getContentPaneView().getImageTimeMap().get(timestamp.charAt(4)));
            applicationFrameView.getContentPaneView().getTotemGateTimeSecond1LabelRef().setIcon(applicationFrameView.getContentPaneView().getImageTimeMap().get(timestamp.charAt(6)));
            applicationFrameView.getContentPaneView().getTotemGateTimeSecond2LabelRef().setIcon(applicationFrameView.getContentPaneView().getImageTimeMap().get(timestamp.charAt(7)));
            applicationFrameView.getContentPaneView().getTotemGateTimeHMSeparatorLabelRef().setIcon(applicationFrameView.getContentPaneView().getImageTimeMap().get(':'));
            applicationFrameView.getContentPaneView().getTotemGateTimeMSSeparatorLabelRef().setIcon(applicationFrameView.getContentPaneView().getImageTimeMap().get(':'));
            applicationFrameView.getContentPaneView().getTotemGateDateLabelRef().setText(date);
    	}
    }

    private class DefaultClockListener implements ActionListener
    {
        @Override
    	public void actionPerformed(ActionEvent e)
      {
            Date dateTime = Calendar.getInstance().getTime();
            DateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
            String timestamp = timeFormatter.format(dateTime);
            DateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
            String date = dateFormatter.format(dateTime);
            applicationFrameView.getContentPaneView().getTotemGateTimeHour1LabelRef().setText(XteTestToolContentPaneView.STRING_TIME_MAP.get(timestamp.charAt(0)));
            applicationFrameView.getContentPaneView().getTotemGateTimeHour2LabelRef().setText(XteTestToolContentPaneView.STRING_TIME_MAP.get(timestamp.charAt(1)));
            applicationFrameView.getContentPaneView().getTotemGateTimeMinute1LabelRef().setText(XteTestToolContentPaneView.STRING_TIME_MAP.get(timestamp.charAt(3)));
            applicationFrameView.getContentPaneView().getTotemGateTimeMinute2LabelRef().setText(XteTestToolContentPaneView.STRING_TIME_MAP.get(timestamp.charAt(4)));
            applicationFrameView.getContentPaneView().getTotemGateTimeSecond1LabelRef().setText(XteTestToolContentPaneView.STRING_TIME_MAP.get(timestamp.charAt(6)));
            applicationFrameView.getContentPaneView().getTotemGateTimeSecond2LabelRef().setText(XteTestToolContentPaneView.STRING_TIME_MAP.get(timestamp.charAt(7)));
            applicationFrameView.getContentPaneView().getTotemGateTimeHMSeparatorLabelRef().setText(XteTestToolContentPaneView.STRING_TIME_MAP.get(':'));
            applicationFrameView.getContentPaneView().getTotemGateTimeMSSeparatorLabelRef().setText(XteTestToolContentPaneView.STRING_TIME_MAP.get(':'));
            applicationFrameView.getContentPaneView().getTotemGateDateLabelRef().setText(date);
    	}
    }

    @Override
    public void windowOpened(WindowEvent e) {
        try {
            logger.info("XteTestToolApplicationFrameController start windowOpened");
            Window window = (Window) e.getSource();
            if (window instanceof JFrame)
            {

                if(applicationFrameView.getContentPaneView().isUseImageTimeMap())
                    timestamp = new javax.swing.Timer(500, new ImageClockListener());
                else
                    timestamp = new javax.swing.Timer(500, new DefaultClockListener());

                timestamp.start();

                XteTestToolHomeView homeView = new XteTestToolHomeView(applicationFrameView);
                XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);

                logger.debug("XteTestToolApplicationFrameController windowOpened load Login view");
                PanelLoader panelLoader = PanelLoader.getInstance();
                panelLoader.changePanel(applicationFrameView.getContentPaneView(), null, homeView, new Rectangle(0, 70, 1000, 690));

                logger.info("XteTestToolApplicationFrameController windowOpened completed successfully");
            }

        } catch (Exception ex) {
            logger.error("XteTestToolApplicationFrameController windowOpened failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Unable to open application window", ex);
        }
    }

    @Override
    public void windowClosing(WindowEvent e) {
        try
        {
            logger.info("XteTestToolApplicationFrameController windowClosing");
            Window window = (Window) e.getSource();
            if(window instanceof JFrame)
            {
                timestamp.stop();
            }
        } catch (Exception ex) {
            logger.error("XteTestToolApplicationFrameController windowClosing failed", ex);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

}
