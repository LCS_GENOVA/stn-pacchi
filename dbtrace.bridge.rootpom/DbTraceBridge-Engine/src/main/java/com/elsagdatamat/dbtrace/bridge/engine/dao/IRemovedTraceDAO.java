package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.RemovedTrace;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface IRemovedTraceDAO  extends IGenericDAO<RemovedTrace, Long>{

}
