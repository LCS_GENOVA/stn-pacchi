package com.selexelsag.xte.model.message_processor;
import java.util.Map;
import java.util.Properties;

import com.selexelsag.xte.business.exceptions.BusinessMessageProcessorException;
import com.selexelsag.xte.business.tracking.IMessageDBManager;
import com.selexelsag.xte.persistence.XteMessagesDBAccess;
import com.selexelsag.xte.persistence.entities.AsyncMessages;


public interface MessageProcessor {

	/**
	 * Restituisce il flowId associato a questo processor
	 * @return il nome del flowId
	 */
	String getFlowId();
	
	/**
	 * Imposta l'oggetto properties contenente le impostazioni 
	 * per questo message processor
	 * @param properties le properties del message processor
	 */
	void setMessageProcessorProperties(Properties properties);
	
	/**
	 * Imposta il dbManager che verrà usato per gli accessi al db
	 * @param dbAccess una istanza funzionante del dbManager
	 */
	void setDBAccess(XteMessagesDBAccess dbAccess);
	
	/**
	 * Esegue le operazioni di invio del messaggio. 
	 * @param message il messaggio da inviare.
	 */
	void processMessage(AsyncMessages message) throws BusinessMessageProcessorException;
	
	/**
	 * Compone il messaggio di ack da restituire alla ricezione del messaggio asincrono.
	 * @param extractedData gli eventuali dati estratti dal messaggio ricevuto
	 * @return il corpo del messaggio di ack.
	 */
	public String buildAckResponse(Map<String,String> extractedData);
	
	void setMessageSender(IMessageDBManager msgSen);
}
