package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="DBTRACE_CONSUMER_PRODUCT")
@IdClass(ConsumerProductPK.class)
public class ConsumerProduct implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -385902249706841565L;

	public static final String SERVICE_NAME = "serviceName";
	public static final String PRODUCT_TYPE = "productType";

	private String serviceName;
	private String productType;

	@Id
	@Column(name="SERVICE_ID")
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@Id
	@Column(name="PTYPE")
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
}
