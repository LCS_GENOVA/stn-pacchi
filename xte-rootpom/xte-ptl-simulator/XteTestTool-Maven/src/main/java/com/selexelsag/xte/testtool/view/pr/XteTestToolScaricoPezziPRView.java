/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.pr;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.view.XteTestToolPanelView;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolCodeDocument;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolIntegerDocument;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolMailPieceCodeDocument;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolStringDocument;
import com.selexelsag.xte.testtool.view.objectmodel.XteListSelectionModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteScaricoPezziTableModel;
import com.selexelsag.xte.testtool.view.objectrenderer.XteMailPieceTableCellRenderer;
import com.selexelsag.xte.testtool.view.objectrenderer.XteTableHeaderRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.PlainDocument;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author Tassara
 */
public class XteTestToolScaricoPezziPRView extends XteTestToolPanelView{
    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolScaricoPezziPRView.class);
    private XteScaricoPezziTableModel xteScaricoPezziPRTableModel = new XteScaricoPezziTableModel();;
    
    public XteTestToolScaricoPezziPRView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try {
            logger.info("XteTestToolScaricoPezziPRView start class creation");
            this.messages = ResourceBundle.getBundle("bundles/messages");
            initComponents();
            logger.info("XteTestToolScaricoPezziPRView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolScaricoPezziPRView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata view creation failed", ex);
        }
    }

    public JPanel getScaricoPezziPanelRef(){ return this; }        
    public JLabel getScaricoPezziTitleLabelRef(){ return this.panelTitlejLabel; }
    
    public JPanel getBundleDataPaneRef(){ return this.bundleDatajPanel; }
    public JTextField getIDUnivocoTextRef(){ return this.IDUnivocojTextField; }
    public JTextField getBundleTPIdTextRef(){ return this.bundleTPIdjTextField; }
    public JTextField getFrazionariTextRef(){ return this.SPFrazionariJTextField; }
    
    public JPanel getMailPieceDataPaneRef(){ return this.mailPieceDatajPanel; }
    public JTextField getSPCodiceTextRef(){ return this.SPCodejTextField; }
    public JTextField getSPProductTextRef(){ return this.SPProductjTextField; }
    public JXDatePicker getMailPieceDatePickerRef(){ return this.SPDatejXDatePicker; }
    public JSpinner getMailPieceTimeSpinnerRef(){ return this.SPTimejSpinner; }
    public JTextField getSPPesoTextRef(){ return this.SPPesojTextField; }
    public JTextField getSPPrezzoTextRef(){ return this.SPPrezzojTextField; }
    public JTextField getSPPercettoTextRef(){ return this.SPPercettojTextField; }
    public JTextField getSPDestinazioneTextRef(){ return this.SPDestinazionejTextField; }
    public JTextField getSPDestinatarioTextRef(){ return this.SPDestinatariojTextField; }
    public JTextField getSPIndirizzoTextRef(){ return this.SPIndirizzojTextField; }
    public JTextField getSPCAPTextRef(){ return this.SPCAPjTextField; }
    public JTextField getSPNomeMittenteTextRef(){ return this.SPNomeMittjTextField; }
    public JTextField getSPDestMittenteTextRef(){ return this.SPDestMittjTextField; }
    public JTextField getSPIndMittenteTextRef(){ return this.SPIndMittjTextField; }
    public JTextField getSPCAPMittTextRef(){ return this.SPCAPMittjTextField; }
    public JComboBox getSPSAJComboBoxRef(){ return this.SPSAjComboBox; }
    public JTextField getSPValAssTextRef(){ return this.SPValAssjTextField; }
    public JTextField getSPImpContrTextRef(){ return this.SPImpContrjTextField; }
    public JTextField getSPCodARTextRef(){ return this.SPCodARjTextField; }
    public JTextField getSPIBANTextRef(){ return this.SPIBANjTextField; }
    public JTextField getSPOraTimeDefTextRef(){ return this.SPOraTimeDefjTextField; }
    public JCheckBox getSPSmarritoCheckRef(){ return this.SPSmarritojCheckBox; }
    
    public void setSPFields(){  
        Calendar date = Calendar.getInstance();
        this.SPFrazionariJTextField.setText("28428");
        this.SPCodejTextField.setText("KA00000000000"); 
        this.SPProductjTextField.setText("CI1");
        this.SPDatejXDatePicker.setDate(date.getTime());
        this.SPDatejXDatePicker.transferFocus();
        this.SPTimejSpinner.setValue(date.getTime());
        this.SPPesojTextField.setText("1100");  
        this.SPPrezzojTextField.setText("1200"); 
        this.SPPercettojTextField.setText("700"); 
        this.SPDestinazionejTextField.setText("Corleone"); 
        this.SPDestinatariojTextField.setText("Romolo Puccini"); 
        this.SPIndirizzojTextField.setText("Via dei Mille 999"); 
        this.SPCAPjTextField.setText("16124"); 
        this.SPNomeMittjTextField.setText("Mario Rossi"); 
        this.SPDestMittjTextField.setText("Bogliasco"); 
        this.SPIndMittjTextField.setText("Via Mazzini 4"); 
        this.SPCAPMittjTextField.setText("16034");  
        this.SPValAssjTextField.setText("10000"); 
        this.SPImpContrjTextField.setText("5000"); 
        this.SPOraTimeDefjTextField.setText("10"); 
        this.SPSmarritojCheckBox.setSelected(true);
        this.SPSAjComboBox.setSelectedIndex(0);
        this.SPCodARjTextField.setText("100000000012");
    }
    
    public JButton getTestButtonRef(){ return this.TestjButton;}
    
    public JTable getMailPieceTableRef(){ return this.mailPiecejTable; }
    public XteScaricoPezziTableModel getScaricoPezziTableModelRef(){ return (XteScaricoPezziTableModel) this.mailPiecejTable.getModel(); }
    public JPanel getMailPieceDataCommandPaneRef(){ return this.mailPieceDataCommandjPanel; }
    public JButton getAggiungiItemButtonRef(){ return this.addMailPiecejButton; }
    public JButton getEliminaItemButtonRef(){ return this.removeMailPiecejButton; }
    
    public JPanel getXmlDataPaneRef(){ return this.xmlDatajPanel; }
    public JScrollPane getXmlDataScrollPaneRef(){ return this.xmlDatajScrollPane; }
    public JTextArea getXmlDataTextAreaRef(){ return this.xmlDatajTextArea; }
    public JPanel getXmlDataCommandPaneRef(){ return this.xmlDataCommandjPanel; }    
    public JButton getGeneraXmlDataButtonRef(){ return this.generaXmlDatajButton; }
    public JButton getImpostaDatiWSButtonRef(){ return this.impostaDatiWSjButton; }
    public JButton getSalvaXmlDataButtonRef(){ return this.salvaXmlDatajButton; }
    
    public JButton getBackButtonRef(){ return this.backjButton; }
    public JButton getHomeButtonRef(){ return this.homejButton; }
    public JButton getExitButtonRef(){ return this.exitjButton; }
    public JTextPane getMessageBarTextPaneRef(){ return this.messageBarjTextPane; }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolScaricoPezziPRView start customizeView");
            //CUSTOMIZE TITLE
            this.panelTitlejLabel .setText(messages.getString("postaregistrata.scaricopezzi.view.title.label"));

            //CUSTOMIZE BUTTON
            this.addMailPiecejButton.setText(messages.getString("postaregistrata.scaricopezzi.view.additem.button"));
            this.removeMailPiecejButton.setText(messages.getString("postaregistrata.scaricopezzi.view.removeitem.button"));
            this.generaXmlDatajButton.setText(messages.getString("postaregistrata.scaricopezzi.view.generaxmldata.button"));
            this.impostaDatiWSjButton.setText(messages.getString("postaregistrata.scaricopezzi.view.impostadatiws.button"));
            this.salvaXmlDatajButton.setText(messages.getString("postaregistrata.scaricopezzi.view.salvaxmldata.button"));
            this.backjButton.setText(messages.getString("view.back.button"));
            this.homejButton.setText(messages.getString("view.home.button"));
            this.exitjButton.setText(messages.getString("view.exit.button"));

            logger.info("XteTestToolScaricoPezziPRView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolScaricoPezziPRView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata view customization failed", ex);
        }
    }

    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolScaricoPezziPRView start initializeView");

//            buildComponentSortedMap();
//
//            this.setFocusCycleRoot(true);
//            this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

            defineTabPolicyForTextArea(xmlDatajTextArea);
            
            JLabel cornelLabel = new JLabel("");
            cornelLabel.setOpaque(true);
            cornelLabel.setBackground(new Color(240, 240, 240));

            this.mailPieceDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.mailPieceDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.mailPieceDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.mailPieceDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.mailPieceDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            customizeTable(this.mailPiecejTable);
            this.mailPiecejTable.setModel(xteScaricoPezziPRTableModel);

            List<String> eventColumnWidthValues = new ArrayList<String>();
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");

            this.mailPiecejTable.getTableHeader().setDefaultRenderer(new XteTableHeaderRenderer(eventColumnWidthValues));
            this.mailPiecejTable.setDefaultRenderer(Object.class, new XteMailPieceTableCellRenderer(eventColumnWidthValues));
            
            this.xmlDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.xmlDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.xmlDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.xmlDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.xmlDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            JFormattedTextField mailPieceTimeJFormattedTextField = ((JSpinner.DateEditor)SPTimejSpinner.getEditor()).getTextField();
            DefaultFormatter mailPieceTimeFormatter = (DefaultFormatter) mailPieceTimeJFormattedTextField.getFormatter();
            mailPieceTimeFormatter.setCommitsOnValidEdit(true);
            
            this.SPCodejTextField.setDocument(new XteTestToolCodeDocument(30));            
            this.SPCodejTextField.getDocument().putProperty("owner", SPCodejTextField);
            this.SPProductjTextField.setDocument(new XteTestToolStringDocument(3));            
            this.SPProductjTextField.getDocument().putProperty("owner", SPProductjTextField); 
            this.SPPesojTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.SPPesojTextField.getDocument().putProperty("owner", SPPesojTextField);
            this.SPPrezzojTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.SPPrezzojTextField.getDocument().putProperty("owner", SPPrezzojTextField);
            this.SPPercettojTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.SPPercettojTextField.getDocument().putProperty("owner", SPPercettojTextField);
            this.SPDestinazionejTextField.setDocument(new XteTestToolStringDocument(20));            
            this.SPDestinazionejTextField.getDocument().putProperty("owner", SPDestinazionejTextField);
            this.SPDestinatariojTextField.setDocument(new XteTestToolStringDocument(20));            
            this.SPDestinatariojTextField.getDocument().putProperty("owner", SPDestinatariojTextField);
            this.SPIndirizzojTextField.setDocument(new XteTestToolStringDocument(100));            
            this.SPIndirizzojTextField.getDocument().putProperty("owner", SPIndirizzojTextField);
            this.SPCAPjTextField.setDocument(new XteTestToolStringDocument(9));            
            this.SPIndirizzojTextField.getDocument().putProperty("owner", SPIndirizzojTextField);
            this.SPNomeMittjTextField.setDocument(new XteTestToolStringDocument(20));            
            this.SPNomeMittjTextField.getDocument().putProperty("owner", SPNomeMittjTextField);
            this.SPDestMittjTextField.setDocument(new XteTestToolStringDocument(20));            
            this.SPDestMittjTextField.getDocument().putProperty("owner", SPDestMittjTextField);
            this.SPIndMittjTextField.setDocument(new XteTestToolStringDocument(100));            
            this.SPIndMittjTextField.getDocument().putProperty("owner", SPIndMittjTextField);
            this.SPCAPMittjTextField.setDocument(new XteTestToolStringDocument(9));            
            this.SPCAPMittjTextField.getDocument().putProperty("owner", SPCAPMittjTextField);
            //this.SPSAjTextField.setDocument(new PlainDocument());            
            //this.SPSAjTextField.getDocument().putProperty("owner", SPSAjTextField);
            this.SPCodARjTextField.setDocument(new XteTestToolStringDocument(30));            
            this.SPCodARjTextField.getDocument().putProperty("owner", SPCodARjTextField);
            this.SPValAssjTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.SPValAssjTextField.getDocument().putProperty("owner", SPValAssjTextField);
            this.SPIBANjTextField.setDocument(new XteTestToolStringDocument(27));            
            this.SPIBANjTextField.getDocument().putProperty("owner", SPIBANjTextField);
            this.SPImpContrjTextField.setDocument(new XteTestToolIntegerDocument(8));            
            this.SPImpContrjTextField.getDocument().putProperty("owner", SPImpContrjTextField);
            this.SPOraTimeDefjTextField.setDocument(new XteTestToolIntegerDocument(2));            
            this.SPOraTimeDefjTextField.getDocument().putProperty("owner", SPOraTimeDefjTextField);
            this.SPDatejXDatePicker.getEditor().setDocument(new PlainDocument());            
            this.SPDatejXDatePicker.getEditor().getDocument().putProperty("owner", SPDatejXDatePicker.getEditor());                        
            ((JSpinner.DateEditor)SPTimejSpinner.getEditor()).getTextField().setDocument(new PlainDocument());            
            ((JSpinner.DateEditor)SPTimejSpinner.getEditor()).getTextField().getDocument().putProperty("owner", ((JSpinner.DateEditor)SPTimejSpinner.getEditor()).getTextField());            


            
            logger.info("XteTestToolScaricoPezziPRView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolScaricoPezziPRView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata view initialization failed", ex);
        }
    }

    @Override
    public void restoreView() {
        logger.debug("XteTestToolScaricoPezziPRView restoreView");
        this.getExitButtonRef().doClick();
    }

    private void customizeTable(JTable table) {
        table.getTableHeader().setReorderingAllowed(false);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setRowHeight(20);
        table.setSelectionModel(new XteListSelectionModel());
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    private  static void defineTabPolicyForTextArea(final JTextArea textarea) {

        final Object tabActionKey = new Object();
        final Object shiftTabActionKey = new Object();
        KeyStroke tabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0, false);
        KeyStroke shiftTabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_DOWN_MASK, false);

        Action tabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusNextComponent((Component) source);
                }
            }
        };

        Action shiftTabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent((Component) source);
                }
            }
        };

        textarea.getActionMap().put(tabActionKey, tabAction);
        textarea.getInputMap().put(tabKeyStroke, tabActionKey);

        textarea.getActionMap().put(shiftTabActionKey, shiftTabAction);
        textarea.getInputMap().put(shiftTabKeyStroke, shiftTabActionKey);

    }
    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.IDUnivocojTextField);
        componentSortedMap.put(2, this.bundleTPIdjTextField);
        componentSortedMap.put(3, this.SPCAPjTextField);
        componentSortedMap.put(4, this.SPProductjTextField);
        componentSortedMap.put(5, this.SPDatejXDatePicker.getEditor());
        componentSortedMap.put(6, ((JSpinner.DateEditor)this.SPTimejSpinner.getEditor()).getTextField());
        componentSortedMap.put(7, this.mailPiecejTable);
        componentSortedMap.put(8, this.xmlDatajTextArea);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        commandjPanel = new javax.swing.JPanel();
        messageBarjScrollPane = new javax.swing.JScrollPane();
        messageBarjTextPane = new javax.swing.JTextPane();
        homejButton = new javax.swing.JButton();
        exitjButton = new javax.swing.JButton();
        backjButton = new javax.swing.JButton();
        workingjPanel = new javax.swing.JPanel();
        panelTitlejLabel = new javax.swing.JLabel();
        xmlDatajPanel = new javax.swing.JPanel();
        xmlDatajScrollPane = new javax.swing.JScrollPane();
        xmlDatajTextArea = new javax.swing.JTextArea();
        xmlDataCommandjPanel = new javax.swing.JPanel();
        generaXmlDatajButton = new javax.swing.JButton();
        impostaDatiWSjButton = new javax.swing.JButton();
        salvaXmlDatajButton = new javax.swing.JButton();
        bundleDatajPanel = new javax.swing.JPanel();
        bundleTPIdjLabel = new javax.swing.JLabel();
        bundleTPIdjTextField = new javax.swing.JTextField();
        IDUnivocojTextField = new javax.swing.JTextField();
        bundleIdjLabel = new javax.swing.JLabel();
        bundleTPIdjLabel1 = new javax.swing.JLabel();
        TestjButton = new javax.swing.JButton();
        SPFrazionariJTextField = new javax.swing.JTextField();
        mailPieceDatajPanel = new javax.swing.JPanel();
        mailPieceDatajScrollPane = new javax.swing.JScrollPane();
        mailPiecejTable = new javax.swing.JTable();
        mailPieceDataCommandjPanel = new javax.swing.JPanel();
        addMailPiecejButton = new javax.swing.JButton();
        removeMailPiecejButton = new javax.swing.JButton();
        mailPieceCodejLabel = new javax.swing.JLabel();
        SPCAPjTextField = new javax.swing.JTextField();
        mailPieceLatjLabel = new javax.swing.JLabel();
        mailPieceProductjLabel = new javax.swing.JLabel();
        SPProductjTextField = new javax.swing.JTextField();
        mailPieceLongjLabel = new javax.swing.JLabel();
        SPDatejXDatePicker = new org.jdesktop.swingx.JXDatePicker();
        mailPieceDateTimejLabel = new javax.swing.JLabel();
        SPTimejSpinner = new javax.swing.JSpinner();
        mailPieceLongjLabel1 = new javax.swing.JLabel();
        SPCodejTextField = new javax.swing.JTextField();
        SPNomeMittjTextField = new javax.swing.JTextField();
        SPDestMittjTextField = new javax.swing.JTextField();
        mailPieceCodejLabel1 = new javax.swing.JLabel();
        mailPieceProductjLabel1 = new javax.swing.JLabel();
        mailPieceDateTimejLabel1 = new javax.swing.JLabel();
        mailPieceLongjLabel2 = new javax.swing.JLabel();
        mailPieceLongjLabel3 = new javax.swing.JLabel();
        SPPercettojTextField = new javax.swing.JTextField();
        SPPrezzojTextField = new javax.swing.JTextField();
        SPPesojTextField = new javax.swing.JTextField();
        SPDestinatariojTextField = new javax.swing.JTextField();
        SPDestinazionejTextField = new javax.swing.JTextField();
        mailPieceCodejLabel2 = new javax.swing.JLabel();
        mailPieceProductjLabel2 = new javax.swing.JLabel();
        mailPieceDateTimejLabel2 = new javax.swing.JLabel();
        mailPieceLatjLabel2 = new javax.swing.JLabel();
        mailPieceLongjLabel4 = new javax.swing.JLabel();
        mailPieceLongjLabel5 = new javax.swing.JLabel();
        SPOraTimeDefjTextField = new javax.swing.JTextField();
        SPValAssjTextField = new javax.swing.JTextField();
        SPIndirizzojTextField = new javax.swing.JTextField();
        SPImpContrjTextField = new javax.swing.JTextField();
        SPIndMittjTextField = new javax.swing.JTextField();
        SPCAPMittjTextField = new javax.swing.JTextField();
        SPSmarritojCheckBox = new javax.swing.JCheckBox();
        SPSAjComboBox = new javax.swing.JComboBox();
        mailPieceLatjLabel3 = new javax.swing.JLabel();
        mailPieceDateTimejLabel3 = new javax.swing.JLabel();
        SPCodARjTextField = new javax.swing.JTextField();
        SPIBANjTextField = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setMaximumSize(new java.awt.Dimension(1000, 690));
        setMinimumSize(new java.awt.Dimension(1000, 690));
        setPreferredSize(new java.awt.Dimension(1000, 690));

        commandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        commandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        commandjPanel.setMaximumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setMinimumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setPreferredSize(new java.awt.Dimension(980, 80));

        messageBarjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageBarjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        messageBarjScrollPane.setMaximumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setMinimumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setPreferredSize(new java.awt.Dimension(350, 50));

        messageBarjTextPane.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        messageBarjTextPane.setFocusable(false);
        messageBarjScrollPane.setViewportView(messageBarjTextPane);

        homejButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        homejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Home3.png"))); // NOI18N
        homejButton.setText("<html><body>&nbsp;Home<body><html>");
        homejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        homejButton.setFocusPainted(false);
        homejButton.setFocusable(false);
        homejButton.setForeground(new java.awt.Color(51, 51, 51));
        homejButton.setIconTextGap(5);
        homejButton.setMaximumSize(new java.awt.Dimension(100, 50));
        homejButton.setMinimumSize(new java.awt.Dimension(100, 50));
        homejButton.setPreferredSize(new java.awt.Dimension(100, 50));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("<html><body>&nbsp;Exit<body><html>");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setFocusPainted(false);
        exitjButton.setFocusable(false);
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIconTextGap(5);
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        backjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        backjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/BackToMenu.png"))); // NOI18N
        backjButton.setText("<html><body>&nbsp;Back<body><html>");
        backjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        backjButton.setFocusPainted(false);
        backjButton.setFocusable(false);
        backjButton.setForeground(new java.awt.Color(51, 51, 51));
        backjButton.setIconTextGap(5);
        backjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        backjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        backjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout commandjPanelLayout = new javax.swing.GroupLayout(commandjPanel);
        commandjPanel.setLayout(commandjPanelLayout);
        commandjPanelLayout.setHorizontalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 268, Short.MAX_VALUE)
                .addComponent(backjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandjPanelLayout.setVerticalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandjPanelLayout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homejButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exitjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        workingjPanel.setBackground(new java.awt.Color(230, 250, 0));
        workingjPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20)));
        workingjPanel.setMaximumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setMinimumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setPreferredSize(new java.awt.Dimension(980, 580));

        panelTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        panelTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        panelTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelTitlejLabel.setText("Scarico Pezzi");
        panelTitlejLabel.setFocusable(false);
        panelTitlejLabel.setIconTextGap(30);
        panelTitlejLabel.setMaximumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setMinimumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setPreferredSize(new java.awt.Dimension(920, 20));

        xmlDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "XML Data", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        xmlDatajPanel.setFocusable(false);
        xmlDatajPanel.setMaximumSize(new java.awt.Dimension(860, 180));
        xmlDatajPanel.setMinimumSize(new java.awt.Dimension(860, 180));
        xmlDatajPanel.setOpaque(false);
        xmlDatajPanel.setPreferredSize(new java.awt.Dimension(860, 180));

        xmlDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        xmlDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        xmlDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 125));
        xmlDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 125));
        xmlDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 125));

        xmlDatajTextArea.setColumns(20);
        xmlDatajTextArea.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        xmlDatajTextArea.setForeground(new java.awt.Color(51, 51, 51));
        xmlDatajTextArea.setLineWrap(true);
        xmlDatajTextArea.setRows(5);
        xmlDatajTextArea.setWrapStyleWord(true);
        xmlDatajTextArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setViewportView(xmlDatajTextArea);

        xmlDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        xmlDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        xmlDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        generaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        generaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml6.png"))); // NOI18N
        generaXmlDatajButton.setText("<html><body>&nbsp;Genera<br>XML Data<body><html>");
        generaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        generaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        generaXmlDatajButton.setFocusPainted(false);
        generaXmlDatajButton.setFocusable(false);
        generaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        generaXmlDatajButton.setIconTextGap(2);
        generaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        xmlDataCommandjPanel.add(generaXmlDatajButton, gridBagConstraints);

        impostaDatiWSjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        impostaDatiWSjButton.setForeground(new java.awt.Color(51, 51, 51));
        impostaDatiWSjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/WebService5.png.png"))); // NOI18N
        impostaDatiWSjButton.setText("<html><body>&nbsp;Imposta<br>&nbsp;Dati WS<body><html>");
        impostaDatiWSjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        impostaDatiWSjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        impostaDatiWSjButton.setFocusPainted(false);
        impostaDatiWSjButton.setFocusable(false);
        impostaDatiWSjButton.setIconTextGap(1);
        impostaDatiWSjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        xmlDataCommandjPanel.add(impostaDatiWSjButton, gridBagConstraints);

        salvaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        salvaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml7.png"))); // NOI18N
        salvaXmlDatajButton.setText("<html><body>&nbsp;Salva<br>XML Data<body><html>");
        salvaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        salvaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        salvaXmlDatajButton.setFocusPainted(false);
        salvaXmlDatajButton.setFocusable(false);
        salvaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        salvaXmlDatajButton.setIconTextGap(2);
        salvaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        xmlDataCommandjPanel.add(salvaXmlDatajButton, gridBagConstraints);

        javax.swing.GroupLayout xmlDatajPanelLayout = new javax.swing.GroupLayout(xmlDatajPanel);
        xmlDatajPanel.setLayout(xmlDatajPanelLayout);
        xmlDatajPanelLayout.setHorizontalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        xmlDatajPanelLayout.setVerticalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(xmlDatajPanelLayout.createSequentialGroup()
                .addGroup(xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        bundleDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Informazioni Mazzetto", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        bundleDatajPanel.setMaximumSize(new java.awt.Dimension(860, 70));
        bundleDatajPanel.setMinimumSize(new java.awt.Dimension(860, 70));
        bundleDatajPanel.setOpaque(false);
        bundleDatajPanel.setPreferredSize(new java.awt.Dimension(860, 70));

        bundleTPIdjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleTPIdjLabel.setText("OfficeFrazionario:");
        bundleTPIdjLabel.setFocusable(false);
        bundleTPIdjLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        bundleTPIdjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjTextField.setText("Mario Rossi");
        bundleTPIdjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        bundleTPIdjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        bundleTPIdjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        bundleTPIdjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        IDUnivocojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        IDUnivocojTextField.setText("38999123456789012789");
        IDUnivocojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        IDUnivocojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        IDUnivocojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        IDUnivocojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        bundleIdjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleIdjLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleIdjLabel.setText("ID Univoco:");
        bundleIdjLabel.setFocusable(false);
        bundleIdjLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        bundleIdjLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        bundleIdjLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        bundleTPIdjLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjLabel1.setForeground(new java.awt.Color(51, 51, 51));
        bundleTPIdjLabel1.setText("ID Operatore TP:");
        bundleTPIdjLabel1.setFocusable(false);
        bundleTPIdjLabel1.setMaximumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel1.setMinimumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel1.setPreferredSize(new java.awt.Dimension(120, 20));

        TestjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TestjButton.setForeground(new java.awt.Color(51, 51, 51));
        TestjButton.setText("<html><body>Test<body><html>");
        TestjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        TestjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        TestjButton.setFocusPainted(false);
        TestjButton.setFocusable(false);
        TestjButton.setIconTextGap(1);
        TestjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        TestjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        TestjButton.setPreferredSize(new java.awt.Dimension(100, 35));

        SPFrazionariJTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPFrazionariJTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPFrazionariJTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPFrazionariJTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPFrazionariJTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        javax.swing.GroupLayout bundleDatajPanelLayout = new javax.swing.GroupLayout(bundleDatajPanel);
        bundleDatajPanel.setLayout(bundleDatajPanelLayout);
        bundleDatajPanelLayout.setHorizontalGroup(
            bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bundleIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(IDUnivocojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(bundleTPIdjLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleTPIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(bundleTPIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SPFrazionariJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addComponent(TestjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        bundleDatajPanelLayout.setVerticalGroup(
            bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(TestjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bundleDatajPanelLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bundleIdjLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(IDUnivocojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(bundleTPIdjLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(bundleTPIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(bundleTPIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(SPFrazionariJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );

        TestjButton.getAccessibleContext().setAccessibleParent(panelTitlejLabel);

        mailPieceDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Informazioni Invio", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        mailPieceDatajPanel.setMaximumSize(new java.awt.Dimension(860, 260));
        mailPieceDatajPanel.setMinimumSize(new java.awt.Dimension(860, 260));
        mailPieceDatajPanel.setOpaque(false);
        mailPieceDatajPanel.setPreferredSize(new java.awt.Dimension(860, 260));

        mailPieceDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        mailPieceDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        mailPieceDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        mailPieceDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 120));
        mailPieceDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 120));
        mailPieceDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 120));

        mailPiecejTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        mailPieceDatajScrollPane.setViewportView(mailPiecejTable);

        mailPieceDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        mailPieceDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        mailPieceDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 85));
        mailPieceDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 85));
        mailPieceDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 85));
        mailPieceDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        addMailPiecejButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        addMailPiecejButton.setForeground(new java.awt.Color(51, 51, 51));
        addMailPiecejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Add1.png"))); // NOI18N
        addMailPiecejButton.setText("<html><body>&nbsp;Aggiungi<br>&nbsp;&nbsp;&nbsp;Invio<body><html>");
        addMailPiecejButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        addMailPiecejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        addMailPiecejButton.setFocusPainted(false);
        addMailPiecejButton.setFocusable(false);
        addMailPiecejButton.setIconTextGap(1);
        addMailPiecejButton.setMaximumSize(new java.awt.Dimension(100, 35));
        addMailPiecejButton.setMinimumSize(new java.awt.Dimension(100, 35));
        addMailPiecejButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        mailPieceDataCommandjPanel.add(addMailPiecejButton, gridBagConstraints);

        removeMailPiecejButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        removeMailPiecejButton.setForeground(new java.awt.Color(51, 51, 51));
        removeMailPiecejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Remove1.png"))); // NOI18N
        removeMailPiecejButton.setText("<html><body>&nbsp;Elimina<br>&nbsp;&nbsp;&nbsp;Invio<body><html>");
        removeMailPiecejButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        removeMailPiecejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        removeMailPiecejButton.setFocusPainted(false);
        removeMailPiecejButton.setFocusable(false);
        removeMailPiecejButton.setIconTextGap(2);
        removeMailPiecejButton.setMaximumSize(new java.awt.Dimension(100, 35));
        removeMailPiecejButton.setMinimumSize(new java.awt.Dimension(100, 35));
        removeMailPiecejButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        mailPieceDataCommandjPanel.add(removeMailPiecejButton, gridBagConstraints);

        mailPieceCodejLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel.setText("Codice:");
        mailPieceCodejLabel.setFocusable(false);
        mailPieceCodejLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        SPCAPjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPCAPjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPCAPjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPCAPjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPCAPjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceLatjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLatjLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLatjLabel.setText("Peso:");
        mailPieceLatjLabel.setFocusable(false);
        mailPieceLatjLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        mailPieceProductjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceProductjLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceProductjLabel.setText("Prodotto:");
        mailPieceProductjLabel.setFocusable(false);
        mailPieceProductjLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        SPProductjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPProductjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPProductjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPProductjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPProductjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceLongjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel.setText("Percetto:");
        mailPieceLongjLabel.setFocusable(false);
        mailPieceLongjLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        SPDatejXDatePicker.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPDatejXDatePicker.setBackground(new java.awt.Color(255, 255, 255));
        SPDatejXDatePicker.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPDatejXDatePicker.setMaximumSize(new java.awt.Dimension(150, 20));
        SPDatejXDatePicker.setMinimumSize(new java.awt.Dimension(150, 20));
        SPDatejXDatePicker.setOpaque(true);
        SPDatejXDatePicker.setPreferredSize(new java.awt.Dimension(150, 20));

        mailPieceDateTimejLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel.setText("Data/Ora:");
        mailPieceDateTimejLabel.setFocusable(false);
        mailPieceDateTimejLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        SPTimejSpinner.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        SPTimejSpinner.setModel(new javax.swing.SpinnerDateModel());
        SPTimejSpinner.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPTimejSpinner.setEditor(new javax.swing.JSpinner.DateEditor(SPTimejSpinner, "HH:mm:ss"));
        SPTimejSpinner.setFocusable(false);
        SPTimejSpinner.setMaximumSize(new java.awt.Dimension(80, 20));
        SPTimejSpinner.setMinimumSize(new java.awt.Dimension(80, 20));
        SPTimejSpinner.setPreferredSize(new java.awt.Dimension(80, 20));

        mailPieceLongjLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel1.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel1.setText("Prezzo:");
        mailPieceLongjLabel1.setFocusable(false);
        mailPieceLongjLabel1.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel1.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel1.setPreferredSize(new java.awt.Dimension(120, 20));

        SPCodejTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPCodejTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPCodejTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPCodejTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPCodejTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPNomeMittjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPNomeMittjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPNomeMittjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPNomeMittjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPNomeMittjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPDestMittjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPDestMittjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPDestMittjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPDestMittjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPDestMittjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceCodejLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel1.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel1.setText("Destinazione:");
        mailPieceCodejLabel1.setFocusable(false);
        mailPieceCodejLabel1.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel1.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel1.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceProductjLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceProductjLabel1.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceProductjLabel1.setText("Destinatario:");
        mailPieceProductjLabel1.setFocusable(false);
        mailPieceProductjLabel1.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel1.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel1.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceDateTimejLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel1.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel1.setText("Indirizzo:");
        mailPieceDateTimejLabel1.setFocusable(false);
        mailPieceDateTimejLabel1.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel1.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel1.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceLongjLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel2.setText("Nome mittente:");
        mailPieceLongjLabel2.setFocusable(false);
        mailPieceLongjLabel2.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel2.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel2.setPreferredSize(new java.awt.Dimension(120, 20));

        mailPieceLongjLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel3.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel3.setText("Dest. mittente:");
        mailPieceLongjLabel3.setFocusable(false);
        mailPieceLongjLabel3.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel3.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel3.setPreferredSize(new java.awt.Dimension(120, 20));

        SPPercettojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPPercettojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPPercettojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPPercettojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPPercettojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPPrezzojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPPrezzojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPPrezzojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPPrezzojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPPrezzojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPPesojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPPesojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPPesojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPPesojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPPesojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPDestinatariojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPDestinatariojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPDestinatariojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPDestinatariojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPDestinatariojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPDestinazionejTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPDestinazionejTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPDestinazionejTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPDestinazionejTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPDestinazionejTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceCodejLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel2.setText("Ind. mittente:");
        mailPieceCodejLabel2.setFocusable(false);
        mailPieceCodejLabel2.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel2.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel2.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceProductjLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceProductjLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceProductjLabel2.setText("SA:");
        mailPieceProductjLabel2.setFocusable(false);
        mailPieceProductjLabel2.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel2.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel2.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceDateTimejLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel2.setText("Valore assicurato:");
        mailPieceDateTimejLabel2.setFocusable(false);
        mailPieceDateTimejLabel2.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel2.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel2.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceLatjLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLatjLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLatjLabel2.setText("Imp. contrassegno:");
        mailPieceLatjLabel2.setFocusable(false);
        mailPieceLatjLabel2.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel2.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel2.setPreferredSize(new java.awt.Dimension(120, 20));

        mailPieceLongjLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel4.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel4.setText("OraTimeDefinite:");
        mailPieceLongjLabel4.setFocusable(false);
        mailPieceLongjLabel4.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel4.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel4.setPreferredSize(new java.awt.Dimension(120, 20));

        mailPieceLongjLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel5.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel5.setText("Smarrito:");
        mailPieceLongjLabel5.setFocusable(false);
        mailPieceLongjLabel5.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel5.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel5.setPreferredSize(new java.awt.Dimension(120, 20));

        SPOraTimeDefjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPOraTimeDefjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPOraTimeDefjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPOraTimeDefjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPOraTimeDefjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPValAssjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPValAssjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPValAssjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPValAssjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPValAssjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPIndirizzojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPIndirizzojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPIndirizzojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPIndirizzojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPIndirizzojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPImpContrjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPImpContrjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPImpContrjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPImpContrjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPImpContrjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPIndMittjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPIndMittjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPIndMittjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPIndMittjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPIndMittjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPCAPMittjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPCAPMittjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPCAPMittjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPCAPMittjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPCAPMittjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPSAjComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        mailPieceLatjLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLatjLabel3.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLatjLabel3.setText("IBAN:");
        mailPieceLatjLabel3.setFocusable(false);
        mailPieceLatjLabel3.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel3.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel3.setPreferredSize(new java.awt.Dimension(120, 20));

        mailPieceDateTimejLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel3.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel3.setText("Codice AR:");
        mailPieceDateTimejLabel3.setFocusable(false);
        mailPieceDateTimejLabel3.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel3.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel3.setPreferredSize(new java.awt.Dimension(140, 20));

        SPCodARjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPCodARjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPCodARjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPCodARjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPCodARjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        SPIBANjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SPIBANjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        SPIBANjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        SPIBANjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        SPIBANjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        javax.swing.GroupLayout mailPieceDatajPanelLayout = new javax.swing.GroupLayout(mailPieceDatajPanel);
        mailPieceDatajPanel.setLayout(mailPieceDatajPanelLayout);
        mailPieceDatajPanelLayout.setHorizontalGroup(
            mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                        .addComponent(mailPieceDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(mailPieceDatajScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(mailPieceLatjLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                    .addComponent(mailPieceDateTimejLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(mailPieceLongjLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                    .addComponent(mailPieceLongjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addComponent(SPDatejXDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(SPTimejSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(SPPesojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                    .addComponent(SPPrezzojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                    .addComponent(SPPercettojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)))
                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(mailPieceProductjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(mailPieceCodejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(SPProductjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                    .addComponent(SPCodejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                .addComponent(mailPieceLongjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mailPieceDatajPanelLayout.createSequentialGroup()
                                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(mailPieceDateTimejLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                            .addComponent(mailPieceProductjLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                            .addComponent(mailPieceCodejLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(SPDestinazionejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                                .addComponent(SPIndirizzojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(SPCAPjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                                            .addComponent(SPDestinatariojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addComponent(mailPieceCodejLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(SPIndMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(SPCAPMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(SPNomeMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addComponent(mailPieceLongjLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(SPDestMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)))
                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                .addComponent(mailPieceDateTimejLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(SPCodARjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(mailPieceLongjLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(mailPieceLongjLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(mailPieceDateTimejLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                    .addComponent(mailPieceLatjLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(mailPieceProductjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(mailPieceLatjLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(SPIBANjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                    .addComponent(SPOraTimeDefjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(SPSmarritojCheckBox)
                                            .addComponent(SPImpContrjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(SPValAssjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(SPSAjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, Short.MAX_VALUE)))))))
                .addContainerGap())
        );
        mailPieceDatajPanelLayout.setVerticalGroup(
            mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mailPieceDatajPanelLayout.createSequentialGroup()
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mailPieceCodejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPCodejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceCodejLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPDestinazionejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceProductjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPSAjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mailPieceProductjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPProductjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceProductjLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPDestinatariojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDateTimejLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPCodARjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SPDatejXDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPTimejSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDateTimejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDateTimejLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPIndirizzojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPCAPjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDateTimejLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPValAssjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mailPieceLatjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPPesojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceLongjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPNomeMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceLatjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SPImpContrjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(SPDestMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceLatjLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(SPIBANjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(mailPieceLongjLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(SPCAPMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(SPIndMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(mailPieceCodejLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(SPPercettojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(mailPieceLongjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(SPOraTimeDefjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mailPieceLongjLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(SPSmarritojCheckBox))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mailPieceLongjLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(SPPrezzojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceLongjLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mailPieceDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout workingjPanelLayout = new javax.swing.GroupLayout(workingjPanel);
        workingjPanel.setLayout(workingjPanelLayout);
        workingjPanelLayout.setHorizontalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workingjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(workingjPanelLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bundleDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        workingjPanelLayout.setVerticalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mailPieceDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 147, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField IDUnivocojTextField;
    private javax.swing.JTextField SPCAPMittjTextField;
    private javax.swing.JTextField SPCAPjTextField;
    private javax.swing.JTextField SPCodARjTextField;
    private javax.swing.JTextField SPCodejTextField;
    private org.jdesktop.swingx.JXDatePicker SPDatejXDatePicker;
    private javax.swing.JTextField SPDestMittjTextField;
    private javax.swing.JTextField SPDestinatariojTextField;
    private javax.swing.JTextField SPDestinazionejTextField;
    private javax.swing.JTextField SPFrazionariJTextField;
    private javax.swing.JTextField SPIBANjTextField;
    private javax.swing.JTextField SPImpContrjTextField;
    private javax.swing.JTextField SPIndMittjTextField;
    private javax.swing.JTextField SPIndirizzojTextField;
    private javax.swing.JTextField SPNomeMittjTextField;
    private javax.swing.JTextField SPOraTimeDefjTextField;
    private javax.swing.JTextField SPPercettojTextField;
    private javax.swing.JTextField SPPesojTextField;
    private javax.swing.JTextField SPPrezzojTextField;
    private javax.swing.JTextField SPProductjTextField;
    private javax.swing.JComboBox SPSAjComboBox;
    private javax.swing.JCheckBox SPSmarritojCheckBox;
    private javax.swing.JSpinner SPTimejSpinner;
    private javax.swing.JTextField SPValAssjTextField;
    private javax.swing.JButton TestjButton;
    private javax.swing.JButton addMailPiecejButton;
    private javax.swing.JButton backjButton;
    private javax.swing.JPanel bundleDatajPanel;
    private javax.swing.JLabel bundleIdjLabel;
    private javax.swing.JLabel bundleTPIdjLabel;
    private javax.swing.JLabel bundleTPIdjLabel1;
    private javax.swing.JTextField bundleTPIdjTextField;
    private javax.swing.JPanel commandjPanel;
    private javax.swing.JButton exitjButton;
    private javax.swing.JButton generaXmlDatajButton;
    private javax.swing.JButton homejButton;
    private javax.swing.JButton impostaDatiWSjButton;
    private javax.swing.JLabel mailPieceCodejLabel;
    private javax.swing.JLabel mailPieceCodejLabel1;
    private javax.swing.JLabel mailPieceCodejLabel2;
    private javax.swing.JPanel mailPieceDataCommandjPanel;
    private javax.swing.JPanel mailPieceDatajPanel;
    private javax.swing.JScrollPane mailPieceDatajScrollPane;
    private javax.swing.JLabel mailPieceDateTimejLabel;
    private javax.swing.JLabel mailPieceDateTimejLabel1;
    private javax.swing.JLabel mailPieceDateTimejLabel2;
    private javax.swing.JLabel mailPieceDateTimejLabel3;
    private javax.swing.JLabel mailPieceLatjLabel;
    private javax.swing.JLabel mailPieceLatjLabel2;
    private javax.swing.JLabel mailPieceLatjLabel3;
    private javax.swing.JLabel mailPieceLongjLabel;
    private javax.swing.JLabel mailPieceLongjLabel1;
    private javax.swing.JLabel mailPieceLongjLabel2;
    private javax.swing.JLabel mailPieceLongjLabel3;
    private javax.swing.JLabel mailPieceLongjLabel4;
    private javax.swing.JLabel mailPieceLongjLabel5;
    private javax.swing.JLabel mailPieceProductjLabel;
    private javax.swing.JLabel mailPieceProductjLabel1;
    private javax.swing.JLabel mailPieceProductjLabel2;
    private javax.swing.JTable mailPiecejTable;
    private javax.swing.JScrollPane messageBarjScrollPane;
    private javax.swing.JTextPane messageBarjTextPane;
    private javax.swing.JLabel panelTitlejLabel;
    private javax.swing.JButton removeMailPiecejButton;
    private javax.swing.JButton salvaXmlDatajButton;
    private javax.swing.JPanel workingjPanel;
    private javax.swing.JPanel xmlDataCommandjPanel;
    private javax.swing.JPanel xmlDatajPanel;
    private javax.swing.JScrollPane xmlDatajScrollPane;
    private javax.swing.JTextArea xmlDatajTextArea;
    // End of variables declaration//GEN-END:variables


}
