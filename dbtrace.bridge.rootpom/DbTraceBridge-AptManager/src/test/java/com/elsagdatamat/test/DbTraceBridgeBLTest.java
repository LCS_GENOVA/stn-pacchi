package com.elsagdatamat.test;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.dbtrace.bridge.engine.bl.AptItemBL;
import com.elsagdatamat.dbtrace.bridge.engine.bl.AptItemPositionBL;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItem;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition.AptStatusEnum;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition.LockerSizeEnum;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = { "classpath:/applicationContext-trackdbws.xml","classpath:/applicationContext-BinSender-bl.xml","classpath:/applicationContext-BinSender-dao.xml","classpath:/applicationContext-BinSender.xml","classpath:/dbTraceBridge-applicationContext-dataSource-default.xml" })
@ContextConfiguration(locations = { "/tracktrace-applicationContext-dataSource.xml" })
public class DbTraceBridgeBLTest {

	@Resource
	AptItemBL aptItemBL;
	@Resource
	AptItemPositionBL aptItemPositionBL;

	@Test
	public void testaloPRESDA() throws Exception{

		try {
			/*
			aptItemBL.insertOrUpdate(getAptItem());
			aptItemPositionBL.insertOrUpdate(getItemPosition());
			*/
			AptItem item = getAptItem();
			AptItemPosition itemPosition = getItemPosition();
			AptItemPosition itemPositionF = aptItemPositionBL.find(item.getCode());
			if (itemPositionF == null) {
				insertAptItemAndAptItemPositionPreSDA(item, itemPosition);

			} else {
				aptItemBL.insertOrUpdate(item);
			}
		} catch (RuntimeException e) {
			System.out.print(e.getStackTrace());
		} catch (Exception e) {
			System.out.print(e.getStackTrace());
		}
	}

	@Test
	public void testaloUFISDA() throws Exception{

		try {
			/*
			aptItemBL.insertOrUpdate(getAptItem());
			aptItemPositionBL.insertOrUpdate(getItemPosition());
			*/
			AptItem item = getAptItemNoPreSdaFromTrace();
			AptItemPosition itemPosition = getItemPosition();
			if (itemPosition.getFrazionarioGeopost() != null) {
				insertAptItemAndAptItemPositionUfiSDA(item, itemPosition);
			}
		} catch (RuntimeException e) {
			System.out.print(e.getStackTrace());
		}
	}

	private AptItemPosition getItemPosition() {
		AptItemPosition itemPosition = new AptItemPosition();

		itemPosition.setCode("PI006660012IT");
		String fraz = "99999";
		itemPosition.setFrazionarioGeopost(fraz);
		//itemPosition.setNumber(Integer.valueOf("0"));
		//itemPosition.setSize(LockerSizeEnum.S);
		itemPosition.setStatus(AptStatusEnum.COURIER);
		itemPosition.setTimestamp(new Date());

		return itemPosition;
	}

	private AptItem getAptItem() {
		AptItem item = new AptItem();

		//2015/02/09 D.P. tronco i caratteri a 13
		String code = "PI006660012IT";
		if (code.length()==16){
			code = code.substring(3);
		}

		//item.setItemCode(trace.getIdTracedEntity());
		item.setCode(code);
		item.setSubPtype(aptItemBL.getSubPType(item.getCode()));
		item.setPtype("PI1");
		item.setDepth(new Integer(0));
		item.setWidth(new Integer(0));
		item.setHeight(new Integer(0));
		item.setWeight(new Integer(0));
		item.setReceiverName("Pippoo");
		item.setAddressee("Pioppo");
		item.setReceiverEmail("pippo@hotmail.com");
		item.setReceiverPhoneNumber("+390106583034");
		return item;
	}

	//@Transactional(value = "tt-transactionManager", propagation = Propagation.REQUIRED)
	public void insertAptItemAndAptItemPositionPreSDA(AptItem item, AptItemPosition itemPosition) {
		try{
			aptItemBL.insertAptItemAndAptItemPosition(item, itemPosition);


		} catch (RuntimeException exc){
			System.out.print(exc.getStackTrace());
		} catch (Exception e) {
			System.out.print(e.getStackTrace());
		}
	}

	@Transactional(value = "tt-transactionManager", propagation = Propagation.REQUIRED)
	public void insertAptItemAndUpdateAptItemPositionPreSDA(AptItem item, AptItemPosition itemPosition) {
		try{
			aptItemBL.insertAptItemAndUpdateAptItemPosition(item, itemPosition);

		} catch (RuntimeException exc){
			System.out.print(exc.getStackTrace());
		}
	}

	@Transactional(value = "tt-transactionManager", propagation = Propagation.REQUIRED)
	public void insertAptItemAndAptItemPositionUfiSDA(AptItem item, AptItemPosition itemPosition) {
		try{
			AptItem itemS = aptItemBL.find(item.getCode());

			if (itemS == null) {
				AptItemPosition itemPositionF = aptItemPositionBL.find(item.getCode());
				if (itemPositionF == null) {
					aptItemBL.insertAptItemAndAptItemPosition(item, itemPosition);
				} else {
					aptItemBL.insertAptItemAndUpdateAptItemPosition(item, itemPosition);
				}
			} else {
				AptItemPosition itemPositionF = aptItemPositionBL.find(item.getCode());
				if (itemPositionF == null) {
					aptItemPositionBL.insert(itemPosition);
				} else {
					itemPositionF.setFrazionarioGeopost(itemPosition.getFrazionarioGeopost());
					aptItemPositionBL.update(itemPositionF);
				}
			}

		} catch (RuntimeException exc){
			System.out.print(exc.getStackTrace());
		}
	}

	private AptItem getAptItemNoPreSdaFromTrace() {
		AptItem item = new AptItem();

		String code = "PI006660012IT";
		item.setCode(code);
		item.setSubPtype(aptItemBL.getSubPType(item.getCode()));
		item.setPtype("PI1");
/*
		item.setDepth(new Integer(0));
		item.setWidth(new Integer(0));
		item.setHeight(new Integer(0));
		item.setWeight(new Integer(0));
		item.setReceiverName("?");
		item.setAddressee("?");
		item.setReceiverEmail("?");
		item.setReceiverPhoneNumber("?");
*/
		return item;
	}
}
