package com.elsagdatamat.tt.trackdbws.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ESIPACK_SDA_FILTER")
public class EsiPackSDAFilter implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="TRACES_ID", nullable=false)
	private Long tracesId;

	@Column(name="TRACE_TYPE", nullable=true)
	private Long tracesType;
	
	public Long getTracesId() {
		return tracesId;
	}

	public void setTracesId(Long tracesId) {
		this.tracesId = tracesId;
	}
	
	public Long getTracesType() {
		return tracesType;
	}

	public void setTracesType(Long tracesType) {
		this.tracesType = tracesType;
	}
}