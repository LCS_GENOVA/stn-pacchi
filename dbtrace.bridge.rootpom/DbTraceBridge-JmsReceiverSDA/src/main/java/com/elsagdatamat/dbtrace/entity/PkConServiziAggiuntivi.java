package com.elsagdatamat.dbtrace.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;


@Entity
@Table(name = "PK_CON_SERVIZI")
@IdClass(PkConServiziAggiuntiviPk.class)
public class PkConServiziAggiuntivi implements Serializable {

	private static final long serialVersionUID = 6234831928024454303L;

	@Id
	@Column(name = "NOME_TAG", nullable = false)
	private String nomeTag;

	@Id
	@Column(name = "VALORE_TAG", nullable = false)
	private String valoreTag;

	@Id
	@Column(name = "SISTEMA_ESTERNO", nullable = false)
	private String sistemaEsterno;

	
	@Column(name = "VALORE_MAPPATO", nullable = false)
	private String valoreMappato;

	public String getValoreMappato() {
		return valoreMappato;
	}

	public void setValoreMappato(String valoreMappato) {
		this.valoreMappato = valoreMappato;
	}
	
	public String getNomeTag() {
		return nomeTag;
	}

	public void setNomeTag(String nomeTag) {
		this.nomeTag = nomeTag;
	}

	public String getValoreTag() {
		return valoreTag;
	}

	public void setValoreTag(String valoreTag) {
		this.valoreTag = valoreTag;
	}

	public String getSistemaEsterno() {
		return sistemaEsterno;
	}

	public void setSistemaEsterno(String sistemaEsterno) {
		this.sistemaEsterno = sistemaEsterno;
	}
	
	
	
	
}