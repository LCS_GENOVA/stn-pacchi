package com.selexelsag.xte.business.utility;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class M1MessageBluilder {
/*
<MSG>
	<HDR OFCID="25999" SWREL="A0000"/>
	<M1>
		<M1D>
			<OBJ>
				<OBJID>VAFFA11111111</OBJID>
				<PH>D</PH>
				<PSTF>0</PSTF>
				<OP>1</OP>
				<TDT>21/09/2012 09:38:56</TDT>
				<SUBC>HB4</SUBC>
				<SUBP>HN__</SUBP>
			</OBJ>
			<ACC>
				<AOFC>25999</AOFC>
				<ADT>21/09/2012 09:38:56</ADT>
				<ACURR>E</ACURR>
				<REGT>A</REGT>
			</ACC>
			<PARAM><![CDATA[<PARAMETRI><MNOM caption="Nominativo Mittente"></MNOM><MDES caption="Destinazione Mittente"></MDES><MIND caption="Indirizzo Mittente"></MIND><MCAP caption="Cap Mittente"></MCAP><IDPO caption="Importo Diritti Postali"></IDPO></PARAMETRI>]]></PARAM>
		</M1D>
	</M1>
</MSG>
 
 */
	private static Logger logger = Logger.getLogger(M1MessageBluilder.class);

	public final static String TIMESTAMP_FORMAT = "dd/MM/yyyy HH:mm:ss";
	
	public static final String TAG_M1 = "M1";
	public static final String TAG_ACCPTL = "ACCPTL";
	
	private String OFCID;
	private String SWREL;
	private String OBJID;
	private String PH;
	private String PSTF;
	private String OP;
	private String TDT;
	private String SUBC;
	private String AREADEST;
	
	private String DEST;
	private String ADDR;
	private String DADDR;
	private String ZIP;
	
	
	private String AOFC;
	private String ADT;
	
	private String REGM;
	private String W;
	private String FARE;
	private String PERC;
	
	private String ACURR;
	
	private String SA;
	private String CDGSA;
	private String ARID;
	
	private String OPER;
	private String MNOM;
	private String MDES;
	private String MIND;
	private String MCAP;
	private String IBAN;
	private String OTDE;
	
	private String RCURR;
	private String CCODE;
	private String INSVAL;
	private String CODVAL;
	
	private String LATIT;
	private String LONGIT;
	
	public static String formatTimestamp(Date timestamp) {
		if (timestamp==null) {
			logger.debug("tentativo di convertire un Date null");
			return null;
		}
		
		Format formatter;
		try {
			formatter = new SimpleDateFormat(TIMESTAMP_FORMAT);
		}catch (IllegalArgumentException iae) {
			logger.error("impossibile impostare il formato timestamp " + TIMESTAMP_FORMAT + ", formatto come default java", iae);
			return timestamp.toString();
		}
		
		String result;
		try {
			result = formatter.format(timestamp);
		}catch (IllegalArgumentException iae) {
			logger.error("impossibile convertire l'oggetto " + timestamp, iae);
			return timestamp.toString();
		}
		logger.debug("timestamp " + timestamp + " convertito in " + result);
		return result;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<HDR OFCID=\""+ OFCID + "\" SWREL=\"" + SWREL + "\"/>");
		sb.append("<OBJID>" + OBJID + "</OBJID>");
		sb.append("<PH>"+PH+"</PH>");
		sb.append("<PSTF>"+PSTF+"</PSTF>");
		sb.append("<OP>"+OP+"</OP>");
		sb.append("<TDT>"+TDT+"</TDT>");
		sb.append("<SUBC>"+SUBC+"</SUBC>");
		sb.append("<AOFC>"+AOFC+"</AOFC>");
		sb.append("<ADT>"+ADT+"</ADT>");
		sb.append("<ACURR>"+ACURR+"</ACURR>");
		sb.append("<PARAM><![CDATA[<PARAMETRI><MNOM caption=\""+MNOM+"\"></MNOM><MDES caption=\""+MDES+"\"></MDES><MIND caption=\""+MIND+"\"></MIND><MCAP caption=\""+MCAP+"\"></MCAP><IBAN caption=\"IBAN\">"+IBAN+"</IBAN>><OTDE caption=\"Ora Time Definite\">"+OTDE+"</OTDE></PARAMETRI>]]></PARAM>");
		
		return sb.toString();
	}
	
	public String generateM1Message() {
		return generateMessage(TAG_M1);
	}
	
	public String generateACCPTLMessage() {
		return generateMessage(TAG_ACCPTL);
	}
	
	private boolean checkLatit(String coord) {
		
		try {
			double c = Double.parseDouble(coord);
			
			if ((c >= -90) && (c <= 90))
				return true;
			
		} catch(NumberFormatException nfe){
			// TODO : log
		}
			
		return false;
	}
	
	private boolean checkLongit(String coord) {
		
		try {
			double c = Double.parseDouble(coord);
			
			if ((c >= -180) && (c <= 180))
				return true;
			
		} catch(NumberFormatException nfe){
			// TODO : log
		}
			
		return false;
	}
	private String generateMessage(String mainTag) {
		StringBuilder sb = new StringBuilder();
		if (OPER==null||
				AOFC==null||
				OBJID==null||
				SUBC==null||
				ADT==null) {
			logger.error("Required parameters not initialized!");
			return null;
		}
		sb.append("<MSG>\n");
		sb.append("<HDR OFCID=\""+ OFCID + "\" SWREL=\"" + SWREL + "\"/>\n");
		// sb.append("<ACCPTL>\n");
		sb.append("<"+mainTag+">\n");
		sb.append("	<M1D>\n");
		sb.append("		<OBJ>\n");
		sb.append("			<OBJID>" + OBJID + "</OBJID>\n");
		sb.append("			<PH>"+PH+"</PH>\n");
		sb.append("			<PSTF>"+PSTF+"</PSTF>\n");
		sb.append("			<OP>"+OP+"</OP>\n");
		sb.append("			<TDT>"+TDT+"</TDT>\n");
		sb.append("			<SUBC>"+SUBC+"</SUBC>\n");
		sb.append("			<AREADEST>"+AREADEST+"</AREADEST>\n");

		if ((LATIT != null) && (checkLatit(LATIT)))
			sb.append("			<LATIT>"+LATIT+"</LATIT>\n");
			
		if ((LONGIT != null) && (checkLongit(LONGIT)))
			sb.append("			<LONGIT>"+LONGIT+"</LONGIT>\n");
		
		sb.append("		</OBJ>\n");

		sb.append("		<ACC>\n");
		sb.append("			<AOFC>"+AOFC+"</AOFC>\n");
		sb.append("			<ADT>"+ADT+"</ADT>\n");

		// tutto facoltativo
		if (REGM !=null) {
			sb.append("			<REGM>"+REGM+"</REGM>\n");
		}
		if (W!=null) {
			sb.append("			<W>"+W+"</W>\n");
		}
		if (FARE!=null) {
			sb.append("			<FARE>"+FARE+"</FARE>\n");
		}
		if (PERC!=null) {
			sb.append("			<PERC>"+PERC+"</PERC>\n");
		}
		if (ACURR!=null) {
			sb.append("			<ACURR>"+ACURR+"</ACURR>\n");
		}
		if (mainTag.equals(TAG_M1)) {
			if (CDGSA!=null) {
				sb.append("			<SA>"+CDGSA+"</SA>\n");
			}
		}else if (mainTag.equals(TAG_ACCPTL)) {
			if (SA!=null) {
				sb.append("			<SA>"+SA+"</SA>\n");
			}
		} else {
			logger.error("Unmanaged tag " + mainTag + ", exiting");
			return null;
		}
		
		if (ARID!=null) {
			sb.append("			<ARID>"+ARID+"</ARID>\n");
		}
		if (ZIP!=null) {
			sb.append("			<ZIP>"+ZIP+"</ZIP>\n");
		}
		sb.append("		</ACC>\n");
		
		if (RCURR!=null || CCODE!=null || INSVAL!=null || CODVAL!=null ||
				DEST!=null || ADDR!=null || DADDR!=null) { 
			sb.append("		<REG>\n");
			if (RCURR!=null) {
				sb.append("			<RCURR>"+RCURR+"</RCURR>\n");
			}
			if (CCODE!=null) {
				sb.append("			<CCODE>"+CCODE+"</CCODE>\n");
			}
			if (INSVAL!=null) {
				sb.append("			<INSVAL>"+INSVAL+"</INSVAL>\n");
			}
			if (CODVAL!=null) {
				sb.append("			<CODVAL>"+CODVAL+"</CODVAL>\n");
			}
			if (DEST!=null) {
				sb.append("			<DEST>"+DEST+"</DEST>\n");
			}
			
			if (ADDR!=null) {
				sb.append("			<ADDR>"+ADDR+"</ADDR>\n");
			}
			
			if (DADDR!=null) {
				sb.append("			<DADDR>"+DADDR+"</DADDR>\n");
			}

			sb.append("		</REG>\n");
		}
		sb.append("		<PARAM><![CDATA[<PARAMETRI>");
		sb.append("<OPER caption=\"Id Operatore\">"+OPER+"</OPER>");
		// Facoltativi
		if (MNOM!=null) {
			sb.append("<MNOM caption=\"Nominativo Mittente\">"+MNOM+"</MNOM>");
		}
		if (MNOM!=null) {
			sb.append("<MDES caption=\"Destinazione Mittente\">"+MDES+"</MDES>");
		}
		if (MIND!=null) {
			sb.append("<MIND caption=\"Indirizzo Mittente\">"+MIND+"</MIND>");
		}
		if (MCAP!=null) {
			sb.append("<MCAP caption=\"Cap Mittente\">"+MCAP+"</MCAP>");
		}
		if (IBAN!=null) {
			sb.append("<IBAN caption=\"IBAN\">"+IBAN+"</IBAN>");
		}
		if (OTDE!=null) {
			sb.append("<OTDE caption=\"Ora Time Definite\">"+OTDE+"</OTDE>");
		}
		sb.append("</PARAMETRI>]]></PARAM>\n");
		sb.append("	</M1D>\n");
		// sb.append("</ACCPTL>\n");
		sb.append("</"+mainTag+">\n");
		sb.append("</MSG>\n");
		return sb.toString();
	}
	public String getOFCID() {
		return OFCID;
	}
	public void setOFCID(String oFCID) {
		OFCID = oFCID;
	}
	public String getSWREL() {
		return SWREL;
	}
	public void setSWREL(String sWREL) {
		SWREL = sWREL;
	}
	public String getOBJID() {
		return OBJID;
	}
	public void setOBJID(String oBJID) {
		OBJID = oBJID;
	}
	public String getPH() {
		return PH;
	}
	public void setPH(String pH) {
		PH = pH;
	}
	public String getPSTF() {
		return PSTF;
	}
	public void setPSTF(String pSTF) {
		PSTF = pSTF;
	}
	public String getOP() {
		return OP;
	}
	public void setOP(String oP) {
		OP = oP;
	}
	public String getTDT() {
		return TDT;
	}
	public void setTDT(String tDT) {
		TDT = tDT;
	}
	public String getSUBC() {
		return SUBC;
	}
	public void setSUBC(String sUBC) {
		SUBC = sUBC;
	}
	
	public String getAREADEST() {
		return AREADEST;
	}

	public void setAREADEST(String aREADEST) {
		AREADEST = aREADEST;
	}

	public String getDEST() {
		return DEST;
	}

	public void setDEST(String dEST) {
		DEST = dEST;
	}

	public String getADDR() {
		return ADDR;
	}

	public void setADDR(String aDDR) {
		ADDR = aDDR;
	}

	public String getDADDR() {
		return DADDR;
	}

	public void setDADDR(String dADDR) {
		DADDR = dADDR;
	}

	public String getZIP() {
		return ZIP;
	}

	public void setZIP(String zIP) {
		ZIP = zIP;
	}

	public String getAOFC() {
		return AOFC;
	}
	public void setAOFC(String aOFC) {
		AOFC = aOFC;
	}
	public String getADT() {
		return ADT;
	}
	public void setADT(String aDT) {
		ADT = aDT;
	}
	public String getW() {
		return W;
	}

	public void setW(String w) {
		W = w;
	}

	public String getFARE() {
		return FARE;
	}

	public void setFARE(String fARE) {
		FARE = fARE;
	}

	public String getPERC() {
		return PERC;
	}

	public void setPERC(String pERC) {
		PERC = pERC;
	}

	public String getACURR() {
		return ACURR;
	}
	public void setACURR(String aCURR) {
		ACURR = aCURR;
	}

	public String getSA() {
		return SA;
	}

	public void setSA(String sA) {
		SA = sA;
	}

	public String getCDGSA() {
		return CDGSA;
	}

	public void setCDGSA(String cDGSA) {
		CDGSA = cDGSA;
	}
	
	public String getARID() {
		return ARID;
	}

	public void setARID(String aRID) {
		ARID = aRID;
	}

	public String getOPER() {
		return OPER;
	}
	public void setOPER(String oPER) {
		OPER = oPER;
	}
	public String getMNOM() {
		return MNOM;
	}
	public void setMNOM(String mNOM) {
		MNOM = mNOM;
	}
	public String getMDES() {
		return MDES;
	}
	public void setMDES(String mDES) {
		MDES = mDES;
	}
	public String getMIND() {
		return MIND;
	}
	public void setMIND(String mIND) {
		MIND = mIND;
	}
	public String getMCAP() {
		return MCAP;
	}
	public void setMCAP(String mCAP) {
		MCAP = mCAP;
	}

	public String getIBAN() {
		return IBAN;
	}
	public void setIBAN(String iBAN) {
		IBAN = iBAN;
	}

	public String getOTDE() {
		return OTDE;
	}

	public void setOTDE(String oTDE) {
		OTDE = oTDE;
	}

	public String getRCURR() {
		return RCURR;
	}

	public void setRCURR(String rCURR) {
		RCURR = rCURR;
	}

	public String getCCODE() {
		return CCODE;
	}

	public void setCCODE(String cCODE) {
		CCODE = cCODE;
	}

	public String getINSVAL() {
		return INSVAL;
	}

	public void setINSVAL(String iNSVAL) {
		INSVAL = iNSVAL;
	}

	public String getCODVAL() {
		return CODVAL;
	}

	public void setCODVAL(String cODVAL) {
		CODVAL = cODVAL;
	}
	public String getREGM() {
		return REGM;
	}
	public void setREGM(String rEGM) {
		REGM = rEGM;
	}

	public String getLATIT() {
		return LATIT;
	}

	public void setLATIT(String lATIT) {
		LATIT = lATIT;
	}

	public String getLONGIT() {
		return LONGIT;
	}

	public void setLONGIT(String lONGIT) {
		LONGIT = lONGIT;
	}
}

