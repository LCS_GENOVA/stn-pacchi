<?xml version="1.0"?>
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="no" indent="yes"/>
	<xsl:template match="/">
		<MSG>
			<HDR SWREL="A0000" OFCID="00000"/>
			<M3>
					<xsl:apply-templates/>
			</M3>
		</MSG>
	</xsl:template>

	<xsl:template match="OBJ">

	<OBJ>
		<xsl:apply-templates/>
			<PH>A</PH>
			<PSTF>0</PSTF>
			<TDT>
				<xsl:value-of select="concat(substring(/MSG/HDR/DSPED,9,2),'/',substring(/MSG/HDR/DSPED,6,2),'/',substring(/MSG/HDR/DSPED,1,4), ' ',substring(/MSG/HDR/DSPED, 12,8) )"/>
			</TDT>
			<OP>1</OP>

	</OBJ>
	</xsl:template>

		<xsl:template match="OBJID|SUBC">
		<xsl:element name="{name()}">
			<xsl:value-of select="."/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="CODVAL|INSVAL|WB|WA|NSEALS|NCHECK|EXPR|HDR|DEST|ADDR|ZIP|DADDR|SA|PARAM|CODEAR"/>
</xsl:transform>


