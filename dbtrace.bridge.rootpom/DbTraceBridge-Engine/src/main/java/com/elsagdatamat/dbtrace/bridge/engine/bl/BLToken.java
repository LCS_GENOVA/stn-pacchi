package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.dao.CannotAcquireLockException;
import org.springframework.dao.DataIntegrityViolationException;

import com.elsagdatamat.dbtrace.bridge.engine.entities.Token;
import com.elsagdatamat.framework.search.Filter;
import com.elsagdatamat.framework.search.Search;

public class BLToken<T extends IToToken> extends BaseManager<Token, String> implements IBLToken<T> {

	private Integer minutesToRecoveryMinValue = 720;
	private Integer minutesToRestartMinValue = 15;

	public void setMinutesToRecoveryMinValue(Integer minutesToRecoveryMinValue) {
		this.minutesToRecoveryMinValue = minutesToRecoveryMinValue;
	}


	public void setMinutesToRestartMinValue(Integer minutesToRestartMinValue) {
		this.minutesToRestartMinValue = minutesToRestartMinValue;
	}
	@Override
	public Token getTokenByService(String service) {
		Search search = new Search();
		Filter filter = new Filter(Token.SERVICE_ID, service);
		search.addFilter(filter);
		Collection<Token> Tokens = super.find(search);
		if(Tokens.size()>1) {
			return null;
		}
		if(Tokens.size() == 1) {
			return Tokens.iterator().next();
		}
		return null;
	}
	@Override
	public T testAndLockWork(T toToken) {
		log.debug("Entering testAndLockWork");
		log.debug("dao: "+dao);
		log.debug(" token:"+toToken);
		Token token = dao.findById(toToken.getServiceId(), false);
		try {
			if (token != null)
				token = dao.findById(toToken.getServiceId(), true);
		} catch (CannotAcquireLockException ex) {
			log.debug("Cannot acquire lock on token: token locked by another process");
			log.debug("Leaving testAndLockWork");
			throw ex;
		}

		if (token != null) {
			log.debug("Record found on db");
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.MINUTE, -1 * minutesToRestartMinValue);

			if ((token.getReleaseDate() != null) && (token.getReleaseDate().after(c.getTime()))) {
				SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
				log.debug(String.format("Not enough time until last RELEASE: ReleaseDate=%s; checkDate=%s; ", sdf.format(token.getReleaseDate()), sdf.format(c.getTime()) ));
				log.debug("Leaving testAndLockWork");
				return null;
			}
			// C'e' un record che mi riguarda sul DB:
			// Definisco un'ora fa
			c.setTime(new Date());
			c.add(Calendar.MINUTE, -1 * minutesToRecoveryMinValue);
			// Se il record ha data nulla o piu' vecchia di un'ora fa,
			// acquisisco il token
			if ((token.getAccessDate() == null) || (token.getAccessDate().compareTo(c.getTime()) < 0)) {
				if (token.getAccessDate() == null) {
					log.debug("ACCESS TIME is null: get token");
				} else {
					log.debug("ACCESS TIME too old: recovery needed, get token");
				}
				token.setAccessDate(new Date());
				dao.update(token);
				log.debug("Leaving testAndLockWork");

				toToken.setServiceId(token.getServiceId());
				toToken.setWsToken(token.getWsToken());
			} else {
				// Altrimenti c'e' qualcun altro che lavora per me
				log.debug("ACCESS TIME is recent: token locked by another process");
				return null;
			}
			log.debug("Leaving testAndLockWork");
		} else {
			// Il record non c'e' (e' il primo accesso in assoluto)
			token = new Token();
			token.setServiceId(toToken.getServiceId());
			token.setAccessDate(new Date());

			try {
				dao.insert(token);
			} catch (DataIntegrityViolationException ex) {
				log.debug("Token inserted by another process: exit");
				log.debug("Leaving testAndLockWork");
				throw ex;
			}
			log.debug("Token not found: new token inserted");
			log.debug("Leaving testAndLockWork");

			toToken.setServiceId(token.getServiceId());
			toToken.setWsToken(token.getWsToken());
		}
		return toToken;
	}

	@Override
	public void updateWork(T toToken) {
		log.debug("Entering updateWork");
		Token token = dao.findById(toToken.getServiceId(), false);
		if (token == null) {
			token = new Token();
			token.setServiceId(toToken.getServiceId());
		}
		token.setWsToken(toToken.getWsToken());
		token.setAccessDate(new Date());
		dao.update(token);
		log.debug("Leaving updateWork");
	}

	@Override
	public void releaseWork(T toToken) {
		log.debug("Entering releaseWork");
		Token token = dao.findById(toToken.getServiceId(), false);
		if (token == null) {
			token = new Token();
			token.setServiceId(toToken.getServiceId());
		}
		token.setAccessDate(null);
		token.setReleaseDate(new Date());
		dao.update(token);
		log.debug("Leaving releaseWork");
	}

	@Override
	public void updateAccessDate(T toToken) {
		log.debug("updateAccessDate not implemented");
	}

}
