package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.RemovedTrace;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class RemovedTraceDAO  extends SpringHibernateJpaDAOBase <RemovedTrace, Long>
	implements IRemovedTraceDAO {
	
}
