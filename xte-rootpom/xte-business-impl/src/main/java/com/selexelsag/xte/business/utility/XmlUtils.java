package com.selexelsag.xte.business.utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.selexelsag.xte.business.exceptions.BusinessTrasformationException;
import com.selexelsag.xte.business.exceptions.BusinessValidationException;
import com.selexelsag.xte.model.track.TrackRequest;

/***
 * 
 * @author Lanciano
 *
 */
public class XmlUtils {
	private static Logger logger = Logger.getLogger(XmlUtils.class);


	public static boolean validateXml(StringBuilder request, InputStream schemaStream) throws BusinessValidationException{

		logger.debug("xml validation BEGINS");

		Schema schema = null;

		// 1. Lookup a factory for the W3C XML Schema language
		SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		//InputStream schemaStream = ClassLoader.getResourceAsStream ("some/pkg/resource.properties");
		StreamSource streamSource = new StreamSource(schemaStream);

		try {
			schema = factory.newSchema(streamSource);
		} catch (SAXException ex) {
			String msg = String.format("Error creating schema from factory Error=[%1$s]", ex
					.getMessage());
			throw new BusinessValidationException(msg, ex);
		}

		// 3. Get a validator from the schema.
		Validator validator = schema.newValidator();


		// 4. Parse the document you want to check.
		//		Source source = new StreamSource(request);
		Source source = new StreamSource(new StringReader(request.toString()));
		// 5. Check the document
		try {
			validator.validate(source);
		} catch (SAXException ex) {

			String msg = String.format("Error validating xml=[%1$s], Error=[%2$s]", request, ex.getMessage());
			throw new BusinessValidationException(msg, ex);


		} catch (IOException ex) {

			String msg = String.format("Error validating xml=[%1$s], Error=[%2$s]", request, ex.getMessage());
			throw new BusinessValidationException(msg, ex);

		}

		logger.debug("XML validation ENDS");
		return true;
	}

	
	
	public static StringBuffer xslTransform(String xmlSource, String xsltSource, 
			HashMap<String, String> parameters) throws BusinessTrasformationException {

		StreamSource streamSource = new StreamSource(new StringReader(xmlSource));
		StringWriter output = null;
		try{

			output = new StringWriter();
			StreamResult streamResult = new StreamResult(output);

			StreamSource xsltStreamSource = new StreamSource(new StringReader(xsltSource));
			xslTransform(streamSource,streamResult, xsltStreamSource,parameters);
			return output.getBuffer();

		}catch(Exception ex){

			throw new BusinessTrasformationException("problem into xlsTransormation process", ex);

		}finally{

			try { if( output != null) {output.close();output = null;}} catch (IOException e) {logger.error( e, e); } 

		}



	}
	
	
	public static void xslTransform(Source xmlSource, Result xmlResult, Source xsltSource,
			HashMap<String, String> parameters) throws BusinessTrasformationException {

		TransformerFactory factory = TransformerFactory.newInstance();		
		Transformer xsltTransformer = null;
		try {

			xsltTransformer = factory.newTransformer(xsltSource);
			
			if(parameters == null){
				
				xsltTransformer.transform(xmlSource, xmlResult);
				return;
			}
			
			for (Iterator<String> iterator = parameters.keySet().iterator(); iterator.hasNext();) {
				
				String attributeName = (String) iterator.next();
				String valueName = parameters.get(attributeName);
				xsltTransformer.setParameter(attributeName, valueName);
				
			}
			
			xsltTransformer.transform(xmlSource, xmlResult);
			
			
		}catch (TransformerConfigurationException e) {

			throw new BusinessTrasformationException("Unable to create xslt transformer", e);

		} catch (TransformerException e) {

			throw new BusinessTrasformationException("Unable to apply xsl transformation", e);

		}
	}


//	public static void xslTransform(Source xmlSource, Result xmlResult, Source xsltSource) throws BusinessTrasformationException {
//
//		TransformerFactory factory = TransformerFactory.newInstance();		
//		Transformer xsltTransformer = null;
//		try {
//
//			xsltTransformer = factory.newTransformer(xsltSource);
//			xsltTransformer.transform(xmlSource, xmlResult);
//
//		}catch (TransformerConfigurationException e) {
//
//			throw new BusinessTrasformationException("Unable to create xslt transformer", e);
//
//		} catch (TransformerException e) {
//
//			throw new BusinessTrasformationException("Unable to apply xsl transformation", e);
//
//		}
//	}

	public synchronized static String extractValue(String xQuery, String xmlData) throws BusinessTrasformationException{
		XPathFactory factory = XPathFactory.newInstance();
		try{
			XPathExpression xPathExpression = factory.newXPath().compile(xQuery);
			InputSource xmlInputSource = getXPathSource(xmlData);
			return xPathExpression.evaluate(xmlInputSource);
		}catch (Throwable t){
			throw new BusinessTrasformationException("Error extracting xml value for query " + xQuery + " XML=" + xmlData, t);
		}
	}

	private static InputSource getXPathSource(String xmlData) throws Throwable {
		StringReader xmlReader = new StringReader(xmlData);
		return new InputSource(xmlReader);
	}

	private static String composePathFile(TrackRequest req, String syncValidationBasePathKey, String extension) {
		
		String pathXsd = String.format("%1$s/%2$s/%3$s/%4$s.%5$s", 
				syncValidationBasePathKey, 
				req.getService().toString().toLowerCase(),
				req.getEventName().toString().toLowerCase(),
				req.getVersion().toString().toLowerCase(),
				extension
		);
		
		
		logger.debug("entered inside composPathFile path:"+pathXsd);
		
		return pathXsd;
	}
	
	public static String composePathFile(TrackRequest req, String syncValidationBasePathKey) {
		return composePathFile(req, syncValidationBasePathKey, "xsd");
	}

	public static String composePathFileSemanticProperties(TrackRequest req, String syncValidationBasePathKey) {
		return composePathFile(req, syncValidationBasePathKey, "properties");
	}
	
	public static String composePathFileXsltByService(TrackRequest req, String basePath){
		
		String pathXslt = String.format("%1$s/%2$s.xslt", 
				basePath, 
				req.getService().toString().toLowerCase()
		);
		
		logger.debug("entered inside composePathFileXsltByService. path:"+pathXslt);
		
		return pathXslt;
	}

}
