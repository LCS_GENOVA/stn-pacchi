package com.elsagdatamat.tt.trackdbws.trackdbrwserviceimpl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.elsagdatamat.tt.trackdbws.beans.Seal;
import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.elsagdatamat.tt.trackdbws.beans.TraceDetail;
import com.elsagdatamat.tt.trackdbws.beans.VO.EsiPackVO;
import com.elsagdatamat.tt.trackdbws.beans.VO.PackDataRetrieveVO;
import com.elsagdatamat.tt.trackdbws.bl.IDuplicatedTraceBL;
import com.elsagdatamat.tt.trackdbws.bl.IEnvironmentParameterBL;
import com.elsagdatamat.tt.trackdbws.bl.IEsiPackBL;
import com.elsagdatamat.tt.trackdbws.bl.IPkAnaDettPacchiBL;
import com.elsagdatamat.tt.trackdbws.bl.ISealBL;
import com.elsagdatamat.tt.trackdbws.bl.ITraceBL;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.GetSealLastUseInput;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.GetSealLastUseResponse;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.PackCodeForRetrieve;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.PackDataRetrieveOutput;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TokenTillToDateForEsiPack;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TokenTilltoDate;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TokenTilltoDateByFiltersList;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetailsList;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TracesList;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TracesListNextToken;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.TypeSelector;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.UseSealInput;
import com.elsagdatamat.tt.trackdbws.trackdbrwservice.UseSealResponse;
import com.elsagdatamat.tt.trackdbws.utilities.SortingSpecifications;

/*
 * @Author AM,  5/2010
 * @Author AM, 10/2010
 * @Author AM, 11/2010
 * @Author RL, 01/2011
 */
public class TrackDBRWServiceTrans extends SpringBeanAutowiringSupport {

    private static final String APT_CHANNEL = "APT";

    private static enum TracedEntity {
		DISPACCIO, PACCO, OTHER
	}

	private Log log = LogFactory.getLog(getClass());

	private int CHUNK_SIZE_DEFAULT = 1000;

	@Autowired
	ITraceBL traceBL;

	@Autowired
	ISealBL sealBL;

	@Autowired
	IDuplicatedTraceBL duplicatedTraceBL;

	@Autowired
	IEnvironmentParameterBL environmentParameterBL;

	@Autowired
	IEsiPackBL esiPackBL;

	@Autowired
	IPkAnaDettPacchiBL pkAnaDettPacchiBL;

	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public PackDataRetrieveOutput packDataRetrieve(PackCodeForRetrieve packCode) {

		Log log = LogFactory.getLog(getClass());
		log.info("packDataRetrieve. Inizio");

		if (packCode == null) {
			log.error("packDataRetrieve - Richiesta nulla");
			return null;
		}

		String code = packCode.getPackCode();
		if (code == null || code.equals("")) {
			log.error("packDataRetrieve - Codice pacco nullo");
			return null;
		}

		List<PackDataRetrieveVO> packDetList = pkAnaDettPacchiBL.findConfDetsByIdPacco(code);

		Set<com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail> setOfTraceDetails = new HashSet<com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail>();

		for (PackDataRetrieveVO dettaglio : packDetList) {
			com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail td = new com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail();
			td.setParamClass(dettaglio.getPkConMappingDettTr().getDettNuovo());
			td.setParamValue(dettaglio.getPkAnaDettPacchi().getValore());
			setOfTraceDetails.add(td);
		}

		log.info("packDataRetrieve. Fine");
		PackDataRetrieveOutput out = new PackDataRetrieveOutput();
		TraceDetailsList tdsl = new TraceDetailsList();
		tdsl.getTraceDetails().addAll(setOfTraceDetails);
		out.setPackDataRetrieveList(tdsl);
		return out;
	}

	// HZ: Refactoring del metodo
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public BigInteger putTraces(TracesList putTracesInput) {

		Log log = LogFactory.getLog(getClass());
		log.info("putTraces. Inizio");

		if (putTracesInput == null) {
			log.error("putTraces. Fine. Input nullo");
			return new BigInteger("-1");
		}

		List<Trace> listTraceToInsert = new ArrayList<Trace>();
		for (com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace inputTrace : putTracesInput.getTraces()) {
			listTraceToInsert.add(trace2trace(inputTrace));
		}

		long traces = traceBL.insertTrace(listTraceToInsert);

		log.info("putTraces. Fine");

		// ritorna la dimensione della lista inserita
		return new BigInteger(String.valueOf(traces));
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public int getChunkSize(String getChunkSizeInput) {
		Log log = LogFactory.getLog(getClass());
		log.info("getChunkSize. Inizio");

		String chunkSizeString = environmentParameterBL.getParamValue("ChunkSize");
		int chunkSize = CHUNK_SIZE_DEFAULT;

		if (chunkSizeString != null && chunkSizeString.length() != 0)
			chunkSize = Integer.parseInt(chunkSizeString);

		log.info("getChunkSize. Fine");
		return chunkSize;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public String getToken(XMLGregorianCalendar getTokenInput) {
		Log log = LogFactory.getLog(getClass());
		log.info("getToken. Inizio");

		if (getTokenInput == null) {
			log.error("getToken. Fine. Input nullo");
			return null;
		}

		Date dateFrom = xmlGregorianCalendar2Date(getTokenInput);

		log.info("getToken. Fine");
		return String.valueOf(dateFrom.getTime()) + ";" + String.valueOf(-1);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public TracesListNextToken getTraces(TokenTilltoDate getTracesInput) {
		Log log = LogFactory.getLog(getClass());
		log.info("getTraces. Inizio");

		if (getTracesInput == null) {
			log.error("getTraces. Fine. Input nullo");
			return null;
		}

		String token = getTracesInput.getToken();
		XMLGregorianCalendar dateTillto = getTracesInput.getTilltoDate();

		if (token == null) {
			log.error("getTraces. Fine. Token nullo");
			return null;
		}

		if (dateTillto == null) {
			log.error("getTraces. Fine. Data terminazione nulla");
			return null;
		}

		String[] splittedToken = token.split(";");
		Calendar calendar = wellFormed(splittedToken);
		if (calendar == null) {
			log.error("getTraces. Fine. Token mal formato");
			return null;
		}

		Date date = calendar.getTime();
		Long traceId = Long.decode(splittedToken[1]);

		com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace inputFilterByExampleEqual = getTracesInput.getTraceFilterByExampleEqual();
		com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace inputFilterByExampleNotEqual = getTracesInput
				.getTraceFilterByExampleNotEqual();

		Trace filterByExampleEqual = inputFilterByExampleEqual != null ? trace2trace(inputFilterByExampleEqual) : null;
		Trace filterByExampleNotEqual = inputFilterByExampleNotEqual != null ? trace2trace(inputFilterByExampleNotEqual) : null;

		ArrayList<SortingSpecifications> sortBy = new ArrayList<SortingSpecifications>(2);
		sortBy.add(0, new SortingSpecifications("whenRegistered", false));
		sortBy.add(1, new SortingSpecifications("traceId", false));

		List<Trace> listOfTraces = traceBL.getListOfNTraces(date, "whenRegistered", traceId, filterByExampleEqual, filterByExampleNotEqual,
				xmlGregorianCalendar2Date(dateTillto), getChunkSize(null), sortBy);

		int size = listOfTraces != null ? listOfTraces.size() : 0;

		TracesListNextToken tracesListNextToken = new TracesListNextToken();
		switch (size) {
		case 0:
			tracesListNextToken.setNextToken(token);
			tracesListNextToken.setTraceList(null);
			break;

		default:
			Trace lastTrace = listOfTraces.get(size - 1);

			token = String.valueOf(lastTrace.getWhenRegistered().getTime());
			token += ";";
			token += String.valueOf(lastTrace.getIdTrace());

			tracesListNextToken.setNextToken(token);
			TracesList traceList = new TracesList();

			com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace outputTrace;
			for (Trace trace : listOfTraces) {

				outputTrace = reverse_trace2trace(trace);

				if (outputTrace == null) {
					log.error("getTraces. Fine. Fallita conversione data (Date -> XMLCalendar)");
					return null;
				}

				traceList.getTraces().add(outputTrace);
			}
			tracesListNextToken.setTraceList(traceList);
		}

		log.info("getTraces. Fine.");
		return tracesListNextToken;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public TracesListNextToken getTracesByFiltersList(TokenTilltoDateByFiltersList getTracesByFiltersListInput) {
		Log log = LogFactory.getLog(getClass());
		log.info("getTracesByFiltersList. Inizio");

		if (getTracesByFiltersListInput == null) {
			log.error("getTracesByFiltersList. Fine. Input nullo");
			return null;
		}

		String token = getTracesByFiltersListInput.getToken();
		XMLGregorianCalendar dateTillto = getTracesByFiltersListInput.getTilltoDate();

		if (token == null) {
			log.error("getTracesByFiltersList. Fine. Token nullo");
			return null;
		}

		if (dateTillto == null) {
			log.error("getTracesByFiltersList. Fine. Data terminazione nulla");
			return null;
		}

		// L.C. F.G. 15/05/2014 - INIZIO
        // Risoluzione del troncamento della data when_registered. Tale comportamento � indotto dall'utilizzo
		// della annotazione del campo su Trace TemporalType.DATE (necessario per far usare l'indice a ORACLE impostato sul campo
		// when_registered) che per definizione tronca ora, minuti e secondi.

		Date dateTillToDate = xmlGregorianCalendar2Date(dateTillto);
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateTillToDate);
		cal.add(Calendar.DAY_OF_YEAR, 1);
		dateTillToDate = cal.getTime();

		// L.C. F.G. 15/05/2014 - FINE

		String[] splittedToken = token.split(";");
		Calendar calendar = wellFormed(splittedToken);
		if (calendar == null) {
			log.error("getTracesByFiltersList. Fine. Token mal formato");
			return null;
		}

		Date date = calendar.getTime();
		Long traceId = Long.decode(splittedToken[1]);

		TracesList filterListByExampleEqual = getTracesByFiltersListInput.getTracesFilterByExampleEqual();
		TracesList filterListByExampleNotEqual = getTracesByFiltersListInput.getTracesFilterByExampleNotEqual();

		List<com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace> listOfFiltersByExampleEqual = filterListByExampleEqual != null ? filterListByExampleEqual
				.getTraces() : new ArrayList<com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace>();
		List<com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace> listOfFiltersByExampleNotEqual = filterListByExampleNotEqual != null ? filterListByExampleNotEqual
				.getTraces() : new ArrayList<com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace>();

		List<Trace> filtersByExampleEqual = new ArrayList<Trace>();
		for (com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace inputFilter : listOfFiltersByExampleEqual) {
			Trace filter = trace2trace(inputFilter);
			filtersByExampleEqual.add(filter);
		}

		List<Trace> filtersByExampleNotEqual = new ArrayList<Trace>();
		for (com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace inputFilter : listOfFiltersByExampleNotEqual) {
			Trace filter = trace2trace(inputFilter);
			filtersByExampleNotEqual.add(filter);
		}

		ArrayList<SortingSpecifications> sortBy = new ArrayList<SortingSpecifications>(2);
		sortBy.add(0, new SortingSpecifications("whenRegistered", false));
		sortBy.add(1, new SortingSpecifications("traceId", false));

		List<Trace> listOfTraces = traceBL.getListOfNTraces(date, "whenRegistered", traceId, filtersByExampleEqual,
				filtersByExampleNotEqual, dateTillToDate, getChunkSize(null), sortBy);

		int size = listOfTraces != null ? listOfTraces.size() : 0;

		TracesListNextToken tracesListNextToken = new TracesListNextToken();
		switch (size) {
		case 0:
			tracesListNextToken.setNextToken(token);
			tracesListNextToken.setTraceList(null);
			break;

		default:
			Trace lastTrace = listOfTraces.get(size - 1);

			token = String.valueOf(lastTrace.getWhenRegistered().getTime());
			token += ";";
			token += String.valueOf(lastTrace.getIdTrace());

			tracesListNextToken.setNextToken(token);
			TracesList traceList = new TracesList();

			com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace outputTrace;
			for (Trace trace : listOfTraces) {

				outputTrace = reverse_trace2trace(trace);

				if (outputTrace == null) {
					log.error("getTracesByFiltersList. Fallita conversione data (Date -> XMLCalendar. Fine)");
					return null;
				}

				traceList.getTraces().add(outputTrace);
			}
			tracesListNextToken.setTraceList(traceList);
		}

		log.info("getTracesByFiltersList. Fine.");
		return tracesListNextToken;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public TracesList getSackImageFromNSPByPackCode(String getSackImageFromNSPByPackCodeInput) {
		Log log = LogFactory.getLog(getClass());
		log.info("getSackImageFromNSPByPackCode. Inizio");

		if (getSackImageFromNSPByPackCodeInput == null || getSackImageFromNSPByPackCodeInput.length() == 0) {
			log.error("getSackImageFromNSPByPackCode. Input nullo o vuoto. Fine");
			return null;
		}

		List<Trace> listOfTrace = traceBL.fastSackImageFromNSPByPackCode(getSackImageFromNSPByPackCodeInput);
		if (listOfTrace == null || listOfTrace.size() == 0) {
			log.error("getSackImageFromNSPByPackCode. Nessun dispaccio ne pacco trovato. Fine");
			return null;
		}

		TracesList tracesList = new TracesList();
		reverse_tracesList2tracesList(listOfTrace, tracesList.getTraces());

		log.info("getSackImageFromNSPByPackCode. Fine");
		return tracesList;
	}

	private TracesList getSackImageFromNSPBySackCode(List<Trace> nextSackTraceTillTo) {
		TracesList result = new TracesList();
		for (Trace trace : nextSackTraceTillTo)
			result.getTraces().addAll(getSackImageFromNSPBySackCode(trace.getIdTracedEntity()).getTraces());
		return result;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public TracesList getSackImageFromNSPBySackCode(String getSackImageFromNSPBySackCodeInput) {
		Log log = LogFactory.getLog(getClass());
		log.info("getSackImageFromNSPBySackCode. Inizio");

		if (getSackImageFromNSPBySackCodeInput == null || getSackImageFromNSPBySackCodeInput.length() == 0) {

			log.error("getSackImageFromNSPBySackCode. Input nullo o vuoto. Fine");
			return null;
		}

		List<Trace> listOfTrace = traceBL.fastSackImageFromNSPBySackCode(getSackImageFromNSPBySackCodeInput);

		if (listOfTrace == null || listOfTrace.size() == 0) {

			log.error("getSackImageFromNSPBySackCode. Nessun dispaccio ne pacco trovato. Fine");
			return null;
		}

		TracesList tracesList = new TracesList();
		reverse_tracesList2tracesList(listOfTrace, tracesList.getTraces());

		log.info("getSackImageFromNSPBySackCode. Fine");
		return tracesList;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public TracesListNextToken getNextSackImageFromNSP(TokenTilltoDateByFiltersList getNextSackImageFromNSPInput) {
		log.info("getNextSackImageFromNSP. Inizio");

		if (getNextSackImageFromNSPInput == null) {
			log.error("getNextSackImageFromNSP. Fine. Input nullo");
			return null;
		}

		String token = getNextSackImageFromNSPInput.getToken();
		XMLGregorianCalendar dateTillto = getNextSackImageFromNSPInput.getTilltoDate();

		if (token == null) {
			log.error("getNextSackImageFromNSP. Fine. Token nullo");
			return null;
		}

		if (dateTillto == null) {
			log.error("getNextSackImageFromNSP. Fine. Data terminazione nulla");
			return null;
		}

		String[] splittedToken = token.split(";");
		Calendar calendar = wellFormed(splittedToken);
		if (calendar == null) {
			log.error("getNextSackImageFromNSP. Fine. Token mal formato");
			return null;
		}

		Date date = calendar.getTime();
		Long traceId = Long.decode(splittedToken[1]);

        List<Trace> nextSackTraceTillTo = traceBL.nextSackTraceTillTo(traceId, date, xmlGregorianCalendar2Date(dateTillto), APT_CHANNEL);

		TracesListNextToken tracesListNextToken = new TracesListNextToken();
		if (nextSackTraceTillTo == null || nextSackTraceTillTo.isEmpty()) {
			tracesListNextToken.setNextToken(token);
			tracesListNextToken.setTraceList(null);
		} else {
			token = null;
			Trace trace0 = nextSackTraceTillTo.get(0);
			tracesListNextToken.setNextToken(trace0.getWhenRegistered().getTime() + ";" + trace0.getTraceId());
			tracesListNextToken.setTraceList(getSackImageFromNSPBySackCode(nextSackTraceTillTo));
		}

		log.info("getNextSackImageFromNSP. Fine");

		return tracesListNextToken;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private Date xmlGregorianCalendar2Date(XMLGregorianCalendar xmlGregorianCalendar) {
		Log log = LogFactory.getLog(getClass());
		log.info("xmlGregorianCalendar2Date. Inizio");

		Calendar calendar = xmlGregorianCalendar.toGregorianCalendar();

		log.info("xmlGregorianCalendar2Date. Fine");
		return calendar.getTime();
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private XMLGregorianCalendar date2XmlGregorianCalendar(Date date) {
		Log log = LogFactory.getLog(getClass());
		log.info("date2XmlGregorianCalendar. Inizio");

		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(date);

		DatatypeFactory datatypeFactory;

		try {
			datatypeFactory = DatatypeFactory.newInstance();
		} catch (Exception e) {

			log.error("date2XmlGregorianCalendar. Fallita conversione data (Date -> XMLCalendar. Fine");
			return null;
		}

		log.info("date2XmlGregorianCalendar. Fine");
		return datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private Collection<com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail> reverse_details2details(
			Set<TraceDetail> setOfTraceDetails) {
		Log log = LogFactory.getLog(getClass());
		log.info("reverse_details2details. Inizio");

		Collection<com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail> listOfOutputTraceDetails = new ArrayList<com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail>();

		com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail outputTraceDetail;
		for (TraceDetail traceDetail : setOfTraceDetails) {
			outputTraceDetail = new com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail();

			outputTraceDetail.setParamClass(traceDetail.getParamClass());
			outputTraceDetail.setParamValue(traceDetail.getParamValue());

			listOfOutputTraceDetails.add(outputTraceDetail);
		}

		log.info("reverse_details2details. Fine");
		return listOfOutputTraceDetails;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace reverse_trace2trace(Trace trace) {
		Log log = LogFactory.getLog(getClass());
		log.info("reverse_trace2trace. Inizio");

		com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace outputTrace = new com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace();

		XMLGregorianCalendar whenHappened = date2XmlGregorianCalendar(trace.getWhenHappened());
		XMLGregorianCalendar whenRegistered = date2XmlGregorianCalendar(trace.getWhenRegistered());

		if (whenHappened == null || whenRegistered == null) {
			log.error("reverse_trace2trace. Campo data vuoto. Fine");
			return null;
		}

		outputTrace.setChannel(trace.getChannel());
		outputTrace.setIdChannel(trace.getIdChannel());
		outputTrace.setIdCorrelazione(trace.getIdCorrelazione());
		outputTrace.setLabelTracedEntity(trace.getLabelTracedEntity());
		outputTrace.setIdTracedEntity(trace.getIdTracedEntity());
		outputTrace.setTracedEntity(trace.getTracedEntity());
		outputTrace.setWhatHappened(trace.getWhatHappened());
		outputTrace.setWhenHappened(whenHappened);
		outputTrace.setWhenRegistered(whenRegistered);
		outputTrace.setWhereHappened(trace.getWhereHappened());
		outputTrace.setServiceName(trace.getServiceName());

		Set<TraceDetail> setOfTraceDetails = trace.getInternalDetailList();

		Collection<com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail> listOfTraceDetails = setOfTraceDetails != null ? reverse_details2details(setOfTraceDetails)
				: null;

		TraceDetailsList traceDetailsList = null;
		if (listOfTraceDetails != null) {
			traceDetailsList = new TraceDetailsList();
			traceDetailsList.getTraceDetails().addAll(listOfTraceDetails);
		}

		outputTrace.setTraceDetailsList(traceDetailsList);

		log.info("reverse_trace2trace. Fine");
		return outputTrace;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private void reverse_tracesList2tracesList(List<Trace> traceList,
			List<com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace> outputTraceList) {
		Log log = LogFactory.getLog(getClass());
		log.info("reverse_tracesList2tracesList. Inizio");

		for (Trace trace : traceList)
			outputTraceList.add(reverse_trace2trace(trace));

		log.info("reverse_tracesList2tracesList. Fine");
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private HashSet<TraceDetail> details2details(Trace trace,
			List<com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail> listOfTraceDetails) {
		Log log = LogFactory.getLog(getClass());
		log.info("details2details. Inizio");

		HashSet<TraceDetail> traceDetailSet = new HashSet<TraceDetail>();

		TraceDetail traceDetail;
		for (com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail inputTraceDetail : listOfTraceDetails) {

			traceDetail = new TraceDetail();

			traceDetail.setTrace(trace);
			traceDetail.setParamClass(inputTraceDetail.getParamClass());
			traceDetail.setParamValue(inputTraceDetail.getParamValue());

			traceDetailSet.add(traceDetail);
		}

		log.info("details2details. Fine");
		return traceDetailSet;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private Trace trace2trace(com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace inputTrace) {
		Log log = LogFactory.getLog(getClass());
		log.info("trace2trace. Inizio");

		Trace trace = new Trace();

		XMLGregorianCalendar whenHappened = inputTrace.getWhenHappened();
		XMLGregorianCalendar whenRegistered = inputTrace.getWhenRegistered();

		trace.setChannel(inputTrace.getChannel());
		trace.setIdChannel(inputTrace.getIdChannel());
		trace.setLabelTracedEntity(inputTrace.getLabelTracedEntity());
		trace.setIdTracedEntity(inputTrace.getIdTracedEntity());
		trace.setTracedEntity(inputTrace.getTracedEntity());
		trace.setWhatHappened(inputTrace.getWhatHappened());
		trace.setWhenHappened(whenHappened != null ? xmlGregorianCalendar2Date(whenHappened) : null);
		trace.setWhenRegistered(whenRegistered != null ? xmlGregorianCalendar2Date(whenRegistered) : null);
		trace.setWhereHappened(inputTrace.getWhereHappened());
		trace.setServiceName(inputTrace.getServiceName());

		TraceDetailsList traceDetailsList = inputTrace.getTraceDetailsList();
		HashSet<TraceDetail> traceDetailSet = traceDetailsList != null ? details2details(trace, traceDetailsList.getTraceDetails()) : null;

		trace.setInternalDetailList(traceDetailSet);

		log.info("trace2trace. Fine");
		return trace;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private Calendar wellFormed(String[] splittedToken) {
		Log log = LogFactory.getLog(getClass());
		log.info("wellFormed. Inizio");

		if (splittedToken.length != 2) {
			log.error("wellFormed. Lunghezza token diversa da 2. Fine");
			return null;
		}

		Calendar calendar = Calendar.getInstance();
		try {
			calendar.setTimeInMillis(Long.parseLong(splittedToken[0]));
		} catch (Exception e) {
			log.error("wellFormed. Campo millisecondi non convertibile in data. Fine");
			return null;
		}

		log.info("wellFormed. Fine");
		return calendar;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private List<Trace> listOfPackTracesByDidDetail(String did) {
		Log log = LogFactory.getLog(getClass());
		log.info("listOfPackTracesByDidDetail. Inizio");

		Trace filter = new Trace();
		filter.setWhatHappened("AVV_NSP");
		filter.setTracedEntity("PACCO");

		Set<TraceDetail> traceDetails = new HashSet<TraceDetail>();
		TraceDetail traceDetail = new TraceDetail();

		traceDetail.setParamClass("DID");
		traceDetail.setParamValue(did);
		traceDetails.add(traceDetail);

		filter.setInternalDetailList(traceDetails);

		ArrayList<SortingSpecifications> sortBy = new ArrayList<SortingSpecifications>(1);
		sortBy.add(0, new SortingSpecifications("idTracedEntity", false));

		Calendar startDate = Calendar.getInstance();
		startDate.add(Calendar.DAY_OF_MONTH, -16);
		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);

		log.info("listOfPackTracesByDidDetail. Fine");
		// Sostituito new Date(0) con startDate (ossia Now-15 giorni) - Lello
		return traceBL.getListOfNTraces(startDate.getTime(), "whenHappened", Long.valueOf("-1"), filter, new Trace(), Calendar
				.getInstance().getTime(), -1, sortBy);
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private boolean containsOnlyADispaccio(List<Trace> listOfTrace) {
		Log log = LogFactory.getLog(getClass());
		log.info("containsOnlyADispaccio. Inizio");

		boolean containsOnlyADispaccio = true;

		String idDispaccio = null;
		for (Trace trace : listOfTrace) {

			switch (tracedEntity(trace.getTracedEntity())) {
			case DISPACCIO:
				if (idDispaccio == null)
					idDispaccio = trace.getIdTracedEntity();
				else
					containsOnlyADispaccio = false;
				break;

			case PACCO:
				Map<String, String> detailList = trace.getDetailList();
				if (idDispaccio != null && detailList.containsKey("DID") && detailList.get("DID").equalsIgnoreCase(idDispaccio))
					break; // TODO: SOLO SE ENTRA NELL'IF !!!!
			default:// TODO: ATTENZIONE CHE IL CASE PRECEDENTE NON HA BREACK!!!!!!
				containsOnlyADispaccio = false;

			}

			if (!containsOnlyADispaccio)
				break;
		}

		log.info("containsOnlyADispaccio. Fine");
		return containsOnlyADispaccio;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private List<Trace> slowSackImageFromNSPBySackCode(String parameterValue) {
		Log log = LogFactory.getLog(getClass());
		log.info("slowSackImageFromNSPBySackCode. Inizio");

		Trace filter = new Trace();
		filter.setTracedEntity("DISPACCIO");
		filter.setWhatHappened("AVV_NSP");

		Set<TraceDetail> traceDetails = new HashSet<TraceDetail>();
		TraceDetail traceDetail = new TraceDetail();

		traceDetail.setParamClass("DCOD");
		traceDetail.setParamValue(parameterValue);
		traceDetails.add(traceDetail);

		filter.setInternalDetailList(traceDetails);

		ArrayList<SortingSpecifications> sortBy = new ArrayList<SortingSpecifications>(2);
		sortBy.add(0, new SortingSpecifications("whenHappened", true));
		sortBy.add(1, new SortingSpecifications("whenRegistered", false));

		Calendar startDate = Calendar.getInstance();
		startDate.add(Calendar.DAY_OF_MONTH, -16);
		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);

		// Sostituito new Date(0) con startDate (ossia Now-15 giorni) - Lello
		List<Trace> listOfTrace = traceBL.getListOfNTraces(startDate.getTime(), "whenHappened", Long.valueOf("-1"), filter, new Trace(),
				Calendar.getInstance().getTime(), 1, sortBy);

		if (listOfTrace == null || listOfTrace.size() == 0) {

			log.error("slowSackImageFromNSPBySackCode. Dispaccio atteso non trovato. Fine");
			return null;
		}

		String did = listOfTrace.get(0).getIdTracedEntity();
		List<Trace> listOfPackTraceByDidDetail = listOfPackTracesByDidDetail(did);

		if (listOfPackTraceByDidDetail == null || listOfPackTraceByDidDetail.size() == 0) {

			log.error("slowSackImageFromNSPBySackCode. Pacchi non trovati per il dispaccio " + did + ". Fine");
			return null;
		}

		listOfTrace.addAll(listOfPackTraceByDidDetail);

		log.info("slowSackImageFromNSPBySackCode. Fine");
		return listOfTrace;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private TracedEntity tracedEntity(String tracedEntity) {
		Log log = LogFactory.getLog(getClass());
		log.info("tracedEntity");

		return tracedEntity == null || tracedEntity.length() == 0 ? TracedEntity.OTHER
				: tracedEntity.equalsIgnoreCase("DISPACCIO") ? TracedEntity.DISPACCIO
						: tracedEntity.equalsIgnoreCase("PACCO") ? TracedEntity.PACCO : TracedEntity.OTHER;

	}

	@Transactional(propagation = Propagation.SUPPORTS)
	public TracesListNextToken getEsiPacks(TokenTillToDateForEsiPack getEsiPackInput) {
		Log log = LogFactory.getLog(getClass());
		log.info("getEsiPacks. Inizio");

		if (getEsiPackInput == null) {
			log.error("getEsiPacks. Fine. Input nullo");
			return null;
		}

		TypeSelector typeSelector = getEsiPackInput.getTypeSelector();
		XMLGregorianCalendar dateTillto = getEsiPackInput.getTillToDate();
		String token = getEsiPackInput.getToken();

		if (token == null) {
			log.error("getEsiPacks. Fine. Token nullo");
			return null;
		}

		if (dateTillto == null) {
			log.error("getEsiPacks. Fine. Data nulla");
			return null;
		}

		if (typeSelector == null) {
			log.error("getEsiPacks. Fine. TypeSelector nulla");
			return null;
		}

		String[] splittedToken = token.split(";");
		Calendar calendar = wellFormed(splittedToken);
		if (calendar == null) {
			log.error("getEsiPacks. Fine. Token mal formato");
			return null;
		}

		Date date = calendar.getTime();
		Long traceId = Long.decode(splittedToken[1]);

		List<EsiPackVO> listOfTraces = null;

		switch (typeSelector) {
		case POSTE_ITALIANE:
			listOfTraces = esiPackBL.getListOfNEsiPacks_PI(date, traceId, xmlGregorianCalendar2Date(dateTillto),
					getChunkSize(null));
			break;
		case SDA_ONLY:
			listOfTraces = esiPackBL.getListOfNEsiPacks_SDA(date, traceId, xmlGregorianCalendar2Date(dateTillto),
					getChunkSize(null));
			break;
		}

		int size = listOfTraces != null ? listOfTraces.size() : 0;

		TracesListNextToken tracesListNextToken = new TracesListNextToken();
		switch (size) {
		case 0:
			tracesListNextToken.setNextToken(token);
			tracesListNextToken.setTraceList(null);
			break;

		default:
			EsiPackVO lastTrace = listOfTraces.get(size - 1);

			token = String.valueOf(lastTrace.getTrace().getWhenRegistered().getTime());
			token += ";";
			token += String.valueOf(lastTrace.getTrace().getIdTrace());

			tracesListNextToken.setNextToken(token);
			TracesList traceList = new TracesList();

			com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace outputTrace;
			for (EsiPackVO esiPackVO : listOfTraces) {

				outputTrace = reverse_esiPackVO2trace(esiPackVO);

				if (outputTrace == null) {
					log.error("getEsiPacks. Fine. Fallita conversione data (Date -> XMLCalendar)");
					return null;
				}

				traceList.getTraces().add(outputTrace);
			}
			tracesListNextToken.setTraceList(traceList);
		}

		log.info("getEsiPacks. Fine.");
		return tracesListNextToken;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace reverse_esiPackVO2trace(EsiPackVO esiPackVO) {
		Log log = LogFactory.getLog(getClass());
		log.info("reverse_trace2trace. Inizio");

		com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace outputTrace = new com.elsagdatamat.tt.trackdbws.trackdbrwservice.Trace();

		XMLGregorianCalendar whenHappened = date2XmlGregorianCalendar(esiPackVO.getTrace().getWhenHappened());
		XMLGregorianCalendar whenRegistered = date2XmlGregorianCalendar(esiPackVO.getTrace().getWhenRegistered());

		if (whenHappened == null || whenRegistered == null) {
			log.error("reverse_trace2trace. Campo data vuoto. Fine");
			return null;
		}

		outputTrace.setChannel(esiPackVO.getTrace().getChannel());
		outputTrace.setIdChannel(esiPackVO.getTrace().getIdChannel());
		outputTrace.setLabelTracedEntity(esiPackVO.getTrace().getLabelTracedEntity());
		outputTrace.setIdTracedEntity(esiPackVO.getTrace().getIdTracedEntity());
		outputTrace.setTracedEntity(esiPackVO.getTrace().getTracedEntity());
		outputTrace.setWhatHappened(esiPackVO.getTrace().getWhatHappened());
		outputTrace.setWhenHappened(whenHappened);
		outputTrace.setWhenRegistered(whenRegistered);
		outputTrace.setWhereHappened(esiPackVO.getTrace().getWhereHappened());
		outputTrace.setServiceName(esiPackVO.getTrace().getServiceName());

		Set<TraceDetail> setOfTraceDetails = esiPackVO.getTrace().getInternalDetailList();

		Collection<com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail> listOfTraceDetails = setOfTraceDetails != null ? reverse_details2details(setOfTraceDetails)
				: null;

		TraceDetailsList traceDetailsList = null;
		if (listOfTraceDetails != null) {
			traceDetailsList = new TraceDetailsList();
			traceDetailsList.getTraceDetails().addAll(listOfTraceDetails);
		}

		com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail traceTypeDetail = new com.elsagdatamat.tt.trackdbws.trackdbrwservice.TraceDetail();
		traceTypeDetail.setParamClass("TraceType");
		traceTypeDetail.setParamValue(esiPackVO.getTraceType());

		traceDetailsList.getTraceDetails().add(traceTypeDetail);

		outputTrace.setTraceDetailsList(traceDetailsList);

		log.info("reverse_trace2trace. Fine");
		return outputTrace;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	public GetSealLastUseResponse getSealLastUse(GetSealLastUseInput gslu) {
		Seal sealByNumberDate = sealBL.getSealByNumberDate(gslu.getNumber(), gslu.getDate());
		GetSealLastUseResponse gslur = new GetSealLastUseResponse();

		if (sealByNumberDate != null) {
			GregorianCalendar gc = new GregorianCalendar();
			Date d = sealByNumberDate.getLastUse();
			gc.setTime(d);
			XMLGregorianCalendar xgc = null;
			try {
				xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
			gslur.setGetSealLastUseOutput(xgc);
		}

		return gslur;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public UseSealResponse useSeal(UseSealInput useSealInput) {
		sealBL.saveSeal(useSealInput.getNumber());
		return new UseSealResponse();
	}

}
