package com.elsagdatamat.dbtrace.bridge.tosda;

import javax.xml.bind.annotation.XmlElement;

public class SDATraceWhatDetail {
	@XmlElement(name = "ACC_NSP")
	public SDAAccNsp accNsp;

	@XmlElement(name = "ACC_CMP")
	public SDAAccCmp accCmp;

	@XmlElement(name = "AVV_NSP")
	public SDAAvvNsp avvNsp;

	@XmlElement(name = "PAR_CMP")
	public SDAParCmp parCmp;
	
	@XmlElement(name = "ARR_CMP")
	public SDAArrCmp arrCmp;

	@XmlElement(name = "RES_NSP")
	public SDAResNsp resNsp;

	@XmlElement(name = "SMI_CMP")
	public SDASmiCmp smiCmp;

	@XmlElement(name = "SDA_UFI")
	public SDASdaUfi sdaUfi;

	@XmlElement(name = "FROM_SSIP")
	public SDAFromSsip fromSsip;
	
	@XmlElement(name = "CON_SPE")
	public SDAConSpe conSpe;

}
