/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.logging;

import org.apache.log4j.MDC;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Da Procida
 */
public class LoggerConfig {

    private static final String LOGGER_CONFIG_FILE = "config/log4j.properties";

    public static void configureLogger(String id) {
        MDC.put("applicationId", id);
        PropertyConfigurator.configureAndWatch(LOGGER_CONFIG_FILE, 1000);
    } 
}
