package com.elsagdatamat.dbtrace.bridge.tosda;

import java.math.BigInteger;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;

public class SDASmiCmp {
	@XmlElement(name = "DID")
	public String did;

	@XmlElement(name = "DPH")
	public String dph;

	@XmlElement(name = "PH")
	public String ph;

	@XmlElement(name = "PSTF")
	public String pstf;

	@XmlElement(name = "AREADEST")
	public String areadest;

	@XmlElement(name = "STATUS")
	protected String status;
	@XmlElement(name = "NUM_MANCATA_CONSEGNA")
	protected String nummancataconsegna;
	@XmlElement(name = "FILIALE_SDA")
	protected String filialesda;
	@XmlElement(name = "DESTR")
	protected String destr;
	@XmlElement(name = "ADDRS")
	protected String addrs;
	@XmlElement(name = "ZIP_DIST")
	protected String zipdist;
	@XmlElement(name = "DESTZ")
	protected String destz;
	@XmlElement(name = "PROV")
	protected String prov;
	@XmlElement(name = "CONTO_CORRENTE_DEST")
	protected String contocorrentedest;
	@XmlElement(name = "NUM_GIORNI_GIACENZA")
	protected String numgiornigiacenza;
	@XmlElement(name = "CNTRS")
	protected BigInteger cntrs;
	@XmlElement(name = "COD_ISTR", required = true)
	protected String codistr;
	@XmlElement(name = "FLAG_RRR", required = true)
	protected String flagrrr;
	@XmlElement(name = "FLAG_ANN", required = true)
	protected String flagann;
	@XmlElement(name = "FORWARD_TO_TRAC_PAC", required = true)
	protected String forwardtotracpac;
	@XmlElement(name = "TIPO_TRAC_PAC", required = true)
	protected String tipoTracPac;
	@XmlElement(name = "DESTINATION_ID", required = true)
	protected String destinationid;

	public SDASmiCmp() {
	}

	public SDASmiCmp(Map<String, String> map) {
		String key = "AREADEST";
		this.areadest = map.get(key);
		key = "DID";
		this.did = map.get(key);
		key = "DPH";
		this.dph = map.get(key);
		key = "PH";
		this.ph = map.get(key);
		key = "PSTF";
		this.pstf = map.get(key);
		key = "STATUS";
		this.status = map.get(key);
		key = "NUM_MANCATA_CONSEGNA";
		this.nummancataconsegna = map.get(key);
		key = "OFNAME"; //CONVERSIONE NOME TAG
		this.filialesda = map.get(key);
		key = "DESTR";
		this.destr = map.get(key);
		key = "ADDRS";
		this.addrs = map.get(key);
		key = "ZIP_DIST";
		String zipDist = map.get(key);
		if (zipDist != null && zipDist.length() >=1 && zipDist.length() <=9) {
			this.zipdist = zipDist;
		}
		key = "DESTZ";
		this.destz = map.get(key);
		key = "PROV";
		this.prov = map.get(key);
		key = "CONTO_CORRENTE_DEST";
		this.contocorrentedest = map.get(key);
		key = "NUM_GIORNI_GIACENZA";
		this.numgiornigiacenza = map.get(key);
		key = "CNTRS";
		this.cntrs = (map.get(key)==null)?null:new BigInteger(map.get(key));
		key = "COD_ISTR";
		this.codistr = map.get(key);
		key = "FLAG_RRR";
		this.flagrrr = map.get(key);
		key = "FLAG_ANN";
		this.flagann = map.get(key);
		key = "FORWARD_TO_TRAC_PAC";
		this.forwardtotracpac = map.get(key);
		key = "TIPO_TRAC_PAC";
		this.tipoTracPac = map.get(key);
		key = "OFCOTH"; //CONVERSIONE NOME TAG
		this.destinationid = map.get(key);
		
	}
}
