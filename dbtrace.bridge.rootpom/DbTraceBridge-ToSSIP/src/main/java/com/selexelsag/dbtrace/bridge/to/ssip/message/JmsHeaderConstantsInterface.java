/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.ssip.message;

/**
 * @author arodriguez
 * 
 */
public interface JmsHeaderConstantsInterface {

	/**
	 * Data invio messaggio JMS Il formato della data deve essere il seguente:
	 * dd/MM/yyyy HH:mm:ss
	 */
	public final static String SENT_DATE_KEY = "MessageSentDateKey";

	/**
	 * Formato date
	 */
	public final static String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";

	/**
	 * Frazionario Sorgente
	 * 
	 * Es: 28999
	 */
	public final static String SOURCE_OFFICE_CODE_KEY = "SourceOfficeCodeKey";

	/**
	 * Frazionario Destinazione
	 * 
	 * Es: 68999
	 * 
	 * Nel caso di Centro di Gestione (CdG) inserire come valore CDG
	 */
	public final static String DESTINATION_OFFICE_CODE_KEY = "DestinationOfficeCodeKey";

	/**
	 * Costante che definisce il CDG come destinazione del messaggio.
	 */
	public final static String DESTINATION_CDG = "CDG";

	/**
	 * Costante che definisce il DBTRACE come destinazione del messaggio.
	 */
	public final static String DESTINATION_DBTRACE = "DBTRACE";

	/**
	 * Tipologia Messaggio
	 * 
	 * Es: P1, P2, P3, D1 etc.
	 */
	public final static String MESSAGE_TYPE_KEY = "MessageTypeKey";

	/**
	 * Proprieta booleana che indica se il messaggio e' da considerarsi come
	 * messaggio di test.
	 * 
	 * Utile in Produzione quando si vogliono fare delle prove che tutta
	 * l'infrastruttura funzioni senza dover inviare per forza messaggi reali.
	 */
	public final static String TEST_MESSAGE_KEY = "TestMessageKey";

	/**
	 * Codice aggregato, se applicabile.
	 * 
	 * Es: 810000000012 nel caso di un Dispaccio Raccomandate
	 */
	public final static String CONTAINER_CODE_KEY = "ContainerCodeKey";

	/**
	 * Proprieta booleana che indica se il messaggio e' da considerarsi come
	 * compresso.
	 * 
	 * Nel caso il messaggio sia compresso utilizzare la compressione GZip
	 * presente nella JDK.
	 */
	public final static String COMPRESSED_KEY = "CompressedKey";
	
}
