package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.dbtrace.bridge.engine.entities.DiscardedTrace;

public interface IDiscardedTraceManager extends IManager<DiscardedTrace, Long> {

	@Transactional(value = "dbTraceBridge-transactionManager", propagation = Propagation.REQUIRES_NEW)
	public void saveDiscardedTraces(String discardingProcess, String traceBody, Exception marshallException);
	
	@Transactional(value="dbTraceBridge-transactionManager", propagation = Propagation.REQUIRES_NEW)	
	public long saveDiscardedTraces(String discardingProcess, String traceBody, Exception marshallException, boolean returnId);

	List<Object> find(String process, Date from, Date to, String reason, Boolean single);

}
