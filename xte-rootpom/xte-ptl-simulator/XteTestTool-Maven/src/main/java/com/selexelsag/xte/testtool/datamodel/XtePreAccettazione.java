/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.datamodel;

import java.math.BigInteger;
import java.util.Date;


/**
 *
 * @author Tassara
 */
public class XtePreAccettazione {
    
    private String PreAccettazioneCodInvio;
    private String PreAccettazioneCausale;
    private String PreAccettazioneDestinazione;
    private String PreAccettazioneDestinatario;
    private int PreAccettazioneValAss;
    private int PreAccettazionePeso1;
    private int PreAccettazionePeso2;
    private int PreAccettazioneNumSigilli;
    private int PreAccettazioneNumVaglia;
    private String PreAccettazioneEspresso;
    private String PreAccettazioneCAP;
    private String PreAccettazioneIndirizzo;
    private int PreAccettazioneImpContr;
    private String PreAccettazioneCodAR;
    private String PreAccettazioneSA;
    private String PreAccettazionePARAM;


    public String getPreAccettazioneCodInvio() {
        return PreAccettazioneCodInvio;
    }

    public void setPreAccettazioneCodInvio(String PreAccettazioneCodInvio) {
        this.PreAccettazioneCodInvio = PreAccettazioneCodInvio;
    } 

    public String getPreAccettazioneCausale() {
        return PreAccettazioneCausale;
    }

    public void setPreAccettazioneCausale(String PreAccettazioneCausale) {
        this.PreAccettazioneCausale = PreAccettazioneCausale;
    } 
    
        public String getPreAccettazioneDestinazione() {
        return PreAccettazioneDestinazione;
    }

    public void setPreAccettazioneDestinazione(String PreAccettazioneDestinazione) {
        this.PreAccettazioneDestinazione = PreAccettazioneDestinazione;
    }  
    
    public String getPreAccettazioneDestinatario() {
        return PreAccettazioneDestinatario;
    }

    public void setPreAccettazioneDestinatario(String PreAccettazioneDestinatario) {
        this.PreAccettazioneDestinatario = PreAccettazioneDestinatario;
    }  

    public int getPreAccettazioneValAss() {
        return PreAccettazioneValAss;
    }

    public void setPreAccettazioneValAss(int PreAccettazioneValAss) {
        this.PreAccettazioneValAss = PreAccettazioneValAss;
    }  
    
    public int getPreAccettazionePeso1() {
        return PreAccettazionePeso1;
    }

    public void setPreAccettazionePeso1(int PreAccettazionePeso1) {
        this.PreAccettazionePeso1 = PreAccettazionePeso1;
    } 

    public int getPreAccettazionePeso2() {
        return PreAccettazionePeso2;
    }

    public void setPreAccettazionePeso2(int PreAccettazionePeso2) {
        this.PreAccettazionePeso2 = PreAccettazionePeso2;
    } 
    
    public int getPreAccettazioneNumSigilli() {
        return PreAccettazioneNumSigilli;
    }

    public void setPreAccettazioneNumSigilli(int PreAccettazioneNumSigilli) {
        this.PreAccettazioneNumSigilli = PreAccettazioneNumSigilli;
    } 

    public int getPreAccettazioneNumVaglia() {
        return PreAccettazioneNumVaglia;
    }

    public void setPreAccettazioneNumVaglia(int PreAccettazioneNumVaglia) {
        this.PreAccettazioneNumVaglia = PreAccettazioneNumVaglia;
    }
    
    public String getPreAccettazioneEspresso() {
        return PreAccettazioneEspresso;
    }

    public void setPreAccettazioneEspresso(String PreAccettazioneEspresso) {
        this.PreAccettazioneEspresso = PreAccettazioneEspresso;
    }  
    
    public String getPreAccettazioneCAP() {
        return PreAccettazioneCAP;
    }

    public void setPreAccettazioneCAP(String PreAccettazioneCAP) {
        this.PreAccettazioneCAP = PreAccettazioneCAP;
    }  
    
    public String getPreAccettazioneIndirizzo() {
        return PreAccettazioneIndirizzo;
    }

    public void setPreAccettazioneIndirizzo(String PreAccettazioneIndirizzo) {
        this.PreAccettazioneIndirizzo = PreAccettazioneIndirizzo;
    }  
    
    
    public int getPreAccettazioneImpContr() {
        return PreAccettazioneImpContr;
    }

    public void setPreAccettazioneImpContr(int PreAccettazioneImpContr) {
        this.PreAccettazioneImpContr = PreAccettazioneImpContr;
    } 
    public String getPreAccettazioneCodAR() {
        return PreAccettazioneCodAR;
    }

    public void setPreAccettazioneCodAR(String PreAccettazioneCodAR) {
        this.PreAccettazioneCodAR = PreAccettazioneCodAR;
    } 
    public String getPreAccettazioneSA() {
        return PreAccettazioneSA;
    }

    public void setPreAccettazioneSA(String PreAccettazioneCodSA) {
        this.PreAccettazioneSA = PreAccettazioneCodSA;
    } 
    public String getPreAccettazionePARAM() {
        return PreAccettazionePARAM;
    }

    public void setPreAccettazionePARAM(String PreAccettazionePARAM) {
        this.PreAccettazionePARAM = PreAccettazionePARAM;
    } 


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.PreAccettazioneCodInvio != null ? this.PreAccettazioneCodInvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final XtePreAccettazione other = (XtePreAccettazione) obj;
        if ((this.PreAccettazioneCodInvio == null) ? (other.PreAccettazioneCodInvio != null) : !this.PreAccettazioneCodInvio.equals(other.PreAccettazioneCodInvio)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "XtePreAccettazionePR{" + "PreAccettazioneCode=" + PreAccettazioneCodInvio + ", PreAccettazioneDateTime=" + '}';
    }
    

}
