/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.am;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.view.XteTestToolPanelView;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import com.selexelsag.xte.testtool.view.dp.*;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolIntegerDocument;
import com.selexelsag.xte.testtool.view.objectmodel.XteListSelectionModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteMessiNotificatoriTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteOfficeTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XtePercorsoTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XtePortaLettereTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteTipoMazzettiTableModel;
import com.selexelsag.xte.testtool.view.objectrenderer.XteGeneraMazzettiObjTableCellRenderer;
import com.selexelsag.xte.testtool.view.objectrenderer.XteOfficeTableCellRenderer;
import com.selexelsag.xte.testtool.view.objectrenderer.XteTableHeaderButtonRenderer;
import com.selexelsag.xte.testtool.view.objectrenderer.XteTableHeaderRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolGeneraMazzettiAMView extends XteTestToolPanelView{
    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolItemOracleDPView.class);
    private XteOfficeTableModel xteOfficeTableModel = new XteOfficeTableModel();;
    private XteTipoMazzettiTableModel xteTipoMazzettiTableModel = new XteTipoMazzettiTableModel();
    private XtePortaLettereTableModel xtePortaLettereTableModel = new XtePortaLettereTableModel();
    private XteMessiNotificatoriTableModel xteMessiNotificatoriTableModel = new XteMessiNotificatoriTableModel();
    private XtePercorsoTableModel xtePercorsoTableModel = new XtePercorsoTableModel();
    
    public XteTestToolGeneraMazzettiAMView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try {
            logger.info("XteTestToolGeneraMazzettiAMView start class creation");
            this.messages = ResourceBundle.getBundle("bundles/messages");
            initComponents();
            logger.info("XteTestToolGeneraMazzettiAMView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolGeneraMazzettiAMView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Genera Mazzetti Acquisizione Mazzetti view creation failed", ex);
        }
    }

    public JPanel getGeneraMazzettiPanelRef(){ return this; }        
    public JLabel getGeneraMazzettiTitleLabelRef(){ return this.panelTitlejLabel; }
    
    public JPanel getOfficeDataPaneRef(){ return this.officeDatajPanel; }    
    public JTable getOfficeTableRef(){ return this.officejTable; }
    public XteOfficeTableModel getOfficeTableModelRef(){ return (XteOfficeTableModel) this.officejTable.getModel(); }

    public JPanel getProductDataPaneRef(){ return this.productDatajPanel; }
    
    public JTable getTipoMazzettiTableRef(){ return this.bundlejTable; }
    public XteTipoMazzettiTableModel getTipoMazzettiTableModelRef(){ return (XteTipoMazzettiTableModel) this.bundlejTable.getModel(); }

    public JTable getPortaLettereTableRef(){ return this.portaLetterejTable; }
    public XtePortaLettereTableModel getPortaLettereTableModelRef(){ return (XtePortaLettereTableModel) this.portaLetterejTable.getModel(); }

    public JTable getPercorsoTableRef(){ return this.percorsojTable; }
    public XtePercorsoTableModel getPercorsoTableModelRef(){ return (XtePercorsoTableModel) this.percorsojTable.getModel(); }

    public JTable getMessiNotificatoriTableRef(){ return this.messiNotificatorijTable; }
    public XteMessiNotificatoriTableModel getMessiNotificatoriTableModelRef(){ return (XteMessiNotificatoriTableModel) this.messiNotificatorijTable.getModel(); }

    public JTextField getThreadNumberTextRef(){ return this.threadNumberjTextField; }

    public JPanel getProductDataCommandPaneRef(){ return this.productDataCommandjPanel; }    
    public JButton getGeneraMazzettiButtonRef(){ return this.generaMazzettijButton; }
    public JButton getTestWSButtonRef(){ return this.testWSjButton; }

    public JProgressBar getThreadProgressBarRef(){ return this.threadjProgressBar; }
    
    public JButton getBackButtonRef(){ return this.backjButton; }
    public JButton getHomeButtonRef(){ return this.homejButton; }
    public JButton getExitButtonRef(){ return this.exitjButton; }
    public JTextPane getMessageBarTextPaneRef(){ return this.messageBarjTextPane; }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolGeneraMazzettiAMView start customizeView");
            //CUSTOMIZE TITLE
            this.panelTitlejLabel .setText(messages.getString("acquisizionemazzetti.generamazzetti.view.title.label"));

            //CUSTOMIZE BUTTON
            this.generaMazzettijButton.setText(messages.getString("acquisizionemazzetti.generamazzetti.view.generamazzetti.button"));
            this.testWSjButton.setText(messages.getString("acquisizionemazzetti.generamazzetti.view.testws.button"));
            
            this.backjButton.setText(messages.getString("view.back.button"));
            this.homejButton.setText(messages.getString("view.home.button"));
            this.exitjButton.setText(messages.getString("view.exit.button"));

            logger.info("XteTestToolGeneraMazzettiAMView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolGeneraMazzettiAMView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Genera Mazzetti Acquisizione Mazzetti view customization failed", ex);
        }
    }

    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolGeneraMazzettiAMView start initializeView");

//            buildComponentSortedMap();
//
//            this.setFocusCycleRoot(true);
//            this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

            JLabel cornelLabel = new JLabel("");
            cornelLabel.setOpaque(true);
            cornelLabel.setBackground(new Color(240, 240, 240));
            
            //OFFICE
            this.officeDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.officeDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.officeDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.officeDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.officeDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));
            
            customizeTable(this.officejTable);
            this.officejTable.setModel(xteOfficeTableModel);

            List<String> officeColumnWidthValues = new ArrayList<String>();
            officeColumnWidthValues.add("30");
            officeColumnWidthValues.add("170");
            officeColumnWidthValues.add("150");
            officeColumnWidthValues.add("150");
            officeColumnWidthValues.add("120");
            officeColumnWidthValues.add("180");

            this.officejTable.getTableHeader().setDefaultRenderer(new XteTableHeaderRenderer(officeColumnWidthValues));
            this.officejTable.setDefaultRenderer(Object.class, new XteOfficeTableCellRenderer(officeColumnWidthValues));            
            
            //TIPO MAZZETTI
            this.bundlejScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.bundlejScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.bundlejScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.bundlejScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.bundlejScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            customizeTable(this.bundlejTable);
            this.bundlejTable.setModel(xteTipoMazzettiTableModel);

            List<String> bundleColumnWidthValues = new ArrayList<String>();
            bundleColumnWidthValues.add("130");
            bundleColumnWidthValues.add("142");

            this.bundlejTable.getTableHeader().setDefaultRenderer(new XteTableHeaderButtonRenderer(bundleColumnWidthValues));
            XteGeneraMazzettiObjTableCellRenderer rendererTipoMazzetti = new XteGeneraMazzettiObjTableCellRenderer();
            bundlejTable.getColumnModel().getColumn(0).setCellRenderer(rendererTipoMazzetti);
            bundlejTable.getColumnModel().getColumn(0).setPreferredWidth(130);
            bundlejTable.getColumnModel().getColumn(1).setCellRenderer(rendererTipoMazzetti);
            bundlejTable.getColumnModel().getColumn(1).setPreferredWidth(150);
            
            //PORTALETTERE
            this.portaLetterejScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.portaLetterejScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.portaLetterejScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.portaLetterejScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.portaLetterejScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            customizeTable(this.portaLetterejTable);
            this.portaLetterejTable.setModel(xtePortaLettereTableModel);

            List<String> portaLettereColumnWidthValues = new ArrayList<String>();
            portaLettereColumnWidthValues.add("142");
            portaLettereColumnWidthValues.add("130");

            this.portaLetterejTable.getTableHeader().setDefaultRenderer(new XteTableHeaderButtonRenderer(portaLettereColumnWidthValues));
            XteGeneraMazzettiObjTableCellRenderer rendererPortaLettere = new XteGeneraMazzettiObjTableCellRenderer();
            portaLetterejTable.getColumnModel().getColumn(0).setCellRenderer(rendererPortaLettere);
            portaLetterejTable.getColumnModel().getColumn(0).setPreferredWidth(130);
            portaLetterejTable.getColumnModel().getColumn(1).setCellRenderer(rendererPortaLettere);
            portaLetterejTable.getColumnModel().getColumn(1).setPreferredWidth(150);
            
            //PERCORSO
            this.percorsojScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.percorsojScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.percorsojScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.percorsojScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.percorsojScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            customizeTable(this.percorsojTable);
            this.percorsojTable.setModel(xtePercorsoTableModel);

            List<String> percorsoColumnWidthValues = new ArrayList<String>();
            percorsoColumnWidthValues.add("142");
            percorsoColumnWidthValues.add("130");

            this.percorsojTable.getTableHeader().setDefaultRenderer(new XteTableHeaderButtonRenderer(percorsoColumnWidthValues));
            XteGeneraMazzettiObjTableCellRenderer rendererPercorso = new XteGeneraMazzettiObjTableCellRenderer();
            percorsojTable.getColumnModel().getColumn(0).setCellRenderer(rendererPercorso);
            percorsojTable.getColumnModel().getColumn(0).setPreferredWidth(130);
            percorsojTable.getColumnModel().getColumn(1).setCellRenderer(rendererPercorso);
            percorsojTable.getColumnModel().getColumn(1).setPreferredWidth(150);

            //MESSI NOTIFICATORI
            this.messiNotificatorijScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.messiNotificatorijScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.messiNotificatorijScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.messiNotificatorijScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.messiNotificatorijScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            customizeTable(this.messiNotificatorijTable);
            this.messiNotificatorijTable.setModel(xteMessiNotificatoriTableModel);

            List<String> messiNotificatoriColumnWidthValues = new ArrayList<String>();
            messiNotificatoriColumnWidthValues.add("130");
            messiNotificatoriColumnWidthValues.add("142");

            this.messiNotificatorijTable.getTableHeader().setDefaultRenderer(new XteTableHeaderButtonRenderer(messiNotificatoriColumnWidthValues));
            XteGeneraMazzettiObjTableCellRenderer rendererMessiNotificatori = new XteGeneraMazzettiObjTableCellRenderer();
            messiNotificatorijTable.getColumnModel().getColumn(0).setCellRenderer(rendererMessiNotificatori);
            messiNotificatorijTable.getColumnModel().getColumn(0).setPreferredWidth(130);
            messiNotificatorijTable.getColumnModel().getColumn(1).setCellRenderer(rendererMessiNotificatori);
            messiNotificatorijTable.getColumnModel().getColumn(1).setPreferredWidth(150);
            
            this.threadNumberjTextField.setDocument(new XteTestToolIntegerDocument(6));            
            this.threadNumberjTextField.getDocument().putProperty("owner", threadNumberjTextField);
            
            logger.info("XteTestToolGeneraMazzettiAMView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolGeneraMazzettiAMView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Genera Mazzetti Acquisizione Mazzetti view initialization failed", ex);
        }
    }

    @Override
    public void restoreView() {
        logger.debug("XteTestToolGeneraMazzettiAMView restoreView");
        this.getExitButtonRef().doClick();
    }

    private void customizeTable(JTable table) {
        table.getTableHeader().setReorderingAllowed(false);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setRowHeight(20);
        table.setSelectionModel(new XteListSelectionModel());
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.officejTable);
        componentSortedMap.put(2, this.bundlejTable);
        componentSortedMap.put(3, this.portaLetterejTable);
        componentSortedMap.put(4, this.percorsojTable);
        componentSortedMap.put(5, this.messiNotificatorijTable);
        componentSortedMap.put(6, this.threadNumberjTextField);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        tipologiaButtonGroup = new javax.swing.ButtonGroup();
        commandjPanel = new javax.swing.JPanel();
        messageBarjScrollPane = new javax.swing.JScrollPane();
        messageBarjTextPane = new javax.swing.JTextPane();
        homejButton = new javax.swing.JButton();
        exitjButton = new javax.swing.JButton();
        backjButton = new javax.swing.JButton();
        workingjPanel = new javax.swing.JPanel();
        panelTitlejLabel = new javax.swing.JLabel();
        officeDatajPanel = new javax.swing.JPanel();
        officeDatajScrollPane = new javax.swing.JScrollPane();
        officejTable = new javax.swing.JTable();
        productDatajPanel = new javax.swing.JPanel();
        portaLetterejScrollPane = new javax.swing.JScrollPane();
        portaLetterejTable = new javax.swing.JTable();
        messiNotificatorijScrollPane = new javax.swing.JScrollPane();
        messiNotificatorijTable = new javax.swing.JTable();
        threadNumberjLabel = new javax.swing.JLabel();
        threadNumberjTextField = new javax.swing.JTextField();
        productDataCommandjPanel = new javax.swing.JPanel();
        generaMazzettijButton = new javax.swing.JButton();
        testWSjButton = new javax.swing.JButton();
        bundlejScrollPane = new javax.swing.JScrollPane();
        bundlejTable = new javax.swing.JTable();
        percorsojScrollPane = new javax.swing.JScrollPane();
        percorsojTable = new javax.swing.JTable();
        threadjProgressBar = new javax.swing.JProgressBar();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setMaximumSize(new java.awt.Dimension(1000, 690));
        setMinimumSize(new java.awt.Dimension(1000, 690));
        setPreferredSize(new java.awt.Dimension(1000, 690));

        commandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        commandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        commandjPanel.setMaximumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setMinimumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setPreferredSize(new java.awt.Dimension(980, 80));

        messageBarjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageBarjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        messageBarjScrollPane.setMaximumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setMinimumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setPreferredSize(new java.awt.Dimension(350, 50));

        messageBarjTextPane.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        messageBarjTextPane.setFocusable(false);
        messageBarjScrollPane.setViewportView(messageBarjTextPane);

        homejButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        homejButton.setForeground(new java.awt.Color(51, 51, 51));
        homejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Home3.png"))); // NOI18N
        homejButton.setText("<html><body>&nbsp;Home<body><html>");
        homejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        homejButton.setFocusPainted(false);
        homejButton.setFocusable(false);
        homejButton.setIconTextGap(5);
        homejButton.setMaximumSize(new java.awt.Dimension(100, 50));
        homejButton.setMinimumSize(new java.awt.Dimension(100, 50));
        homejButton.setPreferredSize(new java.awt.Dimension(100, 50));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("<html><body>&nbsp;Exit<body><html>");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setFocusPainted(false);
        exitjButton.setFocusable(false);
        exitjButton.setIconTextGap(5);
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        backjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        backjButton.setForeground(new java.awt.Color(51, 51, 51));
        backjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/BackToMenu.png"))); // NOI18N
        backjButton.setText("<html><body>&nbsp;Back<body><html>");
        backjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        backjButton.setFocusPainted(false);
        backjButton.setFocusable(false);
        backjButton.setIconTextGap(5);
        backjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        backjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        backjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout commandjPanelLayout = new javax.swing.GroupLayout(commandjPanel);
        commandjPanel.setLayout(commandjPanelLayout);
        commandjPanelLayout.setHorizontalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 272, Short.MAX_VALUE)
                .addComponent(backjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandjPanelLayout.setVerticalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandjPanelLayout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homejButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exitjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        workingjPanel.setBackground(new java.awt.Color(230, 250, 0));
        workingjPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20)));
        workingjPanel.setMaximumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setMinimumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setPreferredSize(new java.awt.Dimension(980, 580));

        panelTitlejLabel.setBackground(new java.awt.Color(240, 240, 240));
        panelTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        panelTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        panelTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelTitlejLabel.setText("Genera Mazzetti");
        panelTitlejLabel.setFocusable(false);
        panelTitlejLabel.setIconTextGap(30);
        panelTitlejLabel.setMaximumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setMinimumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setPreferredSize(new java.awt.Dimension(920, 20));

        officeDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Ufficio", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        officeDatajPanel.setMaximumSize(new java.awt.Dimension(860, 160));
        officeDatajPanel.setMinimumSize(new java.awt.Dimension(860, 160));
        officeDatajPanel.setPreferredSize(new java.awt.Dimension(860, 160));
        officeDatajPanel.setOpaque(false);

        officeDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        officeDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        officeDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        officeDatajScrollPane.setMaximumSize(new java.awt.Dimension(830, 120));
        officeDatajScrollPane.setMinimumSize(new java.awt.Dimension(830, 120));
        officeDatajScrollPane.setPreferredSize(new java.awt.Dimension(830, 120));

        officejTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        officeDatajScrollPane.setViewportView(officejTable);

        javax.swing.GroupLayout officeDatajPanelLayout = new javax.swing.GroupLayout(officeDatajPanel);
        officeDatajPanel.setLayout(officeDatajPanelLayout);
        officeDatajPanelLayout.setHorizontalGroup(
            officeDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(officeDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(officeDatajScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        officeDatajPanelLayout.setVerticalGroup(
            officeDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(officeDatajPanelLayout.createSequentialGroup()
                .addComponent(officeDatajScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 11, Short.MAX_VALUE))
        );

        productDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Nessun Ufficio Selezionato", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        productDatajPanel.setMaximumSize(new java.awt.Dimension(860, 350));
        productDatajPanel.setMinimumSize(new java.awt.Dimension(860, 350));
        productDatajPanel.setPreferredSize(new java.awt.Dimension(860, 350));
        productDatajPanel.setOpaque(false);

        portaLetterejScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        portaLetterejScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        portaLetterejScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        portaLetterejScrollPane.setMaximumSize(new java.awt.Dimension(300, 130));
        portaLetterejScrollPane.setMinimumSize(new java.awt.Dimension(300, 130));
        portaLetterejScrollPane.setPreferredSize(new java.awt.Dimension(300, 130));

        portaLetterejTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        portaLetterejScrollPane.setViewportView(portaLetterejTable);

        messiNotificatorijScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        messiNotificatorijScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        messiNotificatorijScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        messiNotificatorijScrollPane.setMaximumSize(new java.awt.Dimension(300, 130));
        messiNotificatorijScrollPane.setMinimumSize(new java.awt.Dimension(300, 130));
        messiNotificatorijScrollPane.setPreferredSize(new java.awt.Dimension(300, 130));

        messiNotificatorijTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        messiNotificatorijScrollPane.setViewportView(messiNotificatorijTable);

        threadNumberjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        threadNumberjLabel.setText("<html><body>Num. Thread<br>Paralleli:<body><html>");
        threadNumberjLabel.setFocusable(false);
        threadNumberjLabel.setForeground(new java.awt.Color(51, 51, 51));
        threadNumberjLabel.setMaximumSize(new java.awt.Dimension(110, 35));
        threadNumberjLabel.setMinimumSize(new java.awt.Dimension(110, 35));
        threadNumberjLabel.setPreferredSize(new java.awt.Dimension(110, 35));

        threadNumberjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        threadNumberjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        threadNumberjTextField.setMaximumSize(new java.awt.Dimension(110, 20));
        threadNumberjTextField.setMinimumSize(new java.awt.Dimension(110, 20));
        threadNumberjTextField.setPreferredSize(new java.awt.Dimension(110, 20));

        productDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        productDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        productDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 85));
        productDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 85));
        productDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 85));
        productDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        generaMazzettijButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        generaMazzettijButton.setForeground(new java.awt.Color(51, 51, 51));
        generaMazzettijButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exec1.png"))); // NOI18N
        generaMazzettijButton.setText("<html><body>&nbsp;Genera<br>&nbsp;Mazzetti<body><html>");
        generaMazzettijButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        generaMazzettijButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        generaMazzettijButton.setFocusPainted(false);
        generaMazzettijButton.setFocusable(false);
        generaMazzettijButton.setIconTextGap(2);
        generaMazzettijButton.setMaximumSize(new java.awt.Dimension(100, 35));
        generaMazzettijButton.setMinimumSize(new java.awt.Dimension(100, 35));
        generaMazzettijButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        productDataCommandjPanel.add(generaMazzettijButton, gridBagConstraints);

        testWSjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        testWSjButton.setForeground(new java.awt.Color(51, 51, 51));
        testWSjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/WebService5.png.png"))); // NOI18N
        testWSjButton.setText("<html><body>Test WS<body><html>");
        testWSjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        testWSjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        testWSjButton.setFocusPainted(false);
        testWSjButton.setFocusable(false);
        testWSjButton.setIconTextGap(1);
        testWSjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        testWSjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        testWSjButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        productDataCommandjPanel.add(testWSjButton, gridBagConstraints);

        bundlejScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        bundlejScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        bundlejScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        bundlejScrollPane.setMaximumSize(new java.awt.Dimension(300, 130));
        bundlejScrollPane.setMinimumSize(new java.awt.Dimension(300, 130));
        bundlejScrollPane.setPreferredSize(new java.awt.Dimension(300, 130));

        bundlejTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        bundlejScrollPane.setViewportView(bundlejTable);

        percorsojScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        percorsojScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        percorsojScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        percorsojScrollPane.setMaximumSize(new java.awt.Dimension(300, 130));
        percorsojScrollPane.setMinimumSize(new java.awt.Dimension(300, 130));
        percorsojScrollPane.setPreferredSize(new java.awt.Dimension(300, 130));

        percorsojTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        percorsojScrollPane.setViewportView(percorsojTable);

        javax.swing.GroupLayout productDatajPanelLayout = new javax.swing.GroupLayout(productDatajPanel);
        productDatajPanel.setLayout(productDatajPanelLayout);
        productDatajPanelLayout.setHorizontalGroup(
            productDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(productDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(productDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(threadjProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(productDatajPanelLayout.createSequentialGroup()
                        .addGroup(productDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(productDataCommandjPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(threadNumberjTextField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(threadNumberjLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(productDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(productDatajPanelLayout.createSequentialGroup()
                                .addComponent(bundlejScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 100, Short.MAX_VALUE)
                                .addComponent(portaLetterejScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(productDatajPanelLayout.createSequentialGroup()
                                .addComponent(percorsojScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(messiNotificatorijScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        productDatajPanelLayout.setVerticalGroup(
            productDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, productDatajPanelLayout.createSequentialGroup()
                .addGroup(productDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(portaLetterejScrollPane, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bundlejScrollPane, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(productDatajPanelLayout.createSequentialGroup()
                        .addComponent(threadNumberjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(threadNumberjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(productDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(messiNotificatorijScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(productDataCommandjPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(percorsojScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(threadjProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout workingjPanelLayout = new javax.swing.GroupLayout(workingjPanel);
        workingjPanel.setLayout(workingjPanelLayout);
        workingjPanelLayout.setHorizontalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workingjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(workingjPanelLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(officeDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(productDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        workingjPanelLayout.setVerticalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(officeDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(productDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(181, 181, 181))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backjButton;
    private javax.swing.JScrollPane bundlejScrollPane;
    private javax.swing.JTable bundlejTable;
    private javax.swing.JPanel commandjPanel;
    private javax.swing.JButton exitjButton;
    private javax.swing.JButton generaMazzettijButton;
    private javax.swing.JButton homejButton;
    private javax.swing.JScrollPane messageBarjScrollPane;
    private javax.swing.JTextPane messageBarjTextPane;
    private javax.swing.JScrollPane messiNotificatorijScrollPane;
    private javax.swing.JTable messiNotificatorijTable;
    private javax.swing.JPanel officeDatajPanel;
    private javax.swing.JScrollPane officeDatajScrollPane;
    private javax.swing.JTable officejTable;
    private javax.swing.JLabel panelTitlejLabel;
    private javax.swing.JScrollPane percorsojScrollPane;
    private javax.swing.JTable percorsojTable;
    private javax.swing.JScrollPane portaLetterejScrollPane;
    private javax.swing.JTable portaLetterejTable;
    private javax.swing.JPanel productDataCommandjPanel;
    private javax.swing.JPanel productDatajPanel;
    private javax.swing.JButton testWSjButton;
    private javax.swing.JLabel threadNumberjLabel;
    private javax.swing.JTextField threadNumberjTextField;
    private javax.swing.JProgressBar threadjProgressBar;
    private javax.swing.ButtonGroup tipologiaButtonGroup;
    private javax.swing.JPanel workingjPanel;
    // End of variables declaration//GEN-END:variables


}
