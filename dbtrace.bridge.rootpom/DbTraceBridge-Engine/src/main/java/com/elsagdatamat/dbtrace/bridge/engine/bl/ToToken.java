package com.elsagdatamat.dbtrace.bridge.engine.bl;

public class ToToken implements IToToken {

	private String wsToken;
	private String serviceId;

	@Override
	public String getWsToken() {
		return wsToken;
	}

	@Override
	public void setWsToken(String wsToken) {
		this.wsToken = wsToken;
	}

	@Override
	public String getServiceId() {
		return serviceId;
	}

	@Override
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	

}
