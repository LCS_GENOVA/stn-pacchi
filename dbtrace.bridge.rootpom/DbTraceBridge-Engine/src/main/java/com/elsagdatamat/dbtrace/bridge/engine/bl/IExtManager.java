package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.elsagdatamat.framework.search.Search;
import com.elsagdatamat.framework.utils.Pair;


public interface IExtManager<Z, K extends Serializable>{

	
//	public Class<Z> getManagedClass();
	
	public List<Z> findByProperties(Collection<Pair<String, Object>> properties);
	
	public Collection<Z> find(Search search);
	
	public Z findByID(K objectID);
	
	public void delete(Z entity);
	
//	public int deleteBatch(Search condition);
	
	public void update(Z entity);
	
	public int updateBatch(Collection<Pair<String, Object>> setProperties, Collection<Pair<String, Object>> conditionProperties);
	
//	public int updateBatch(Collection<Pair<String, Object>> setProperties, Search condition);
//	
//	public int updateBatch(Collection<Pair<String, Object>> newProperties,
//			Search condition, Integer maxObjectAffected);
	
	public void insert(Z entity);
	
//	public Object findMin(Class<Z> clazz, String minPropertyName, Collection<Pair<String, Object>> properties);
//	
//	public Object findMin(Class<Z> clazz, String minPropertyName, Collection<Pair<String, Object>> properties, boolean lock);
//	
//	public Object findMax(Class<Z> clazz, String maxPropertyName, Collection<Pair<String, Object>> properties);
//	
//	public Object findMax(Class<Z> clazz, String maxPropertyName, Collection<Pair<String, Object>> properties, boolean lock) ;
//	
//	public int getCount(Search search);
//	
//	public void refresh(Z object);
}
