package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.elsagdatamat.dbtrace.bridge.engine.entities.CodiciProdottoPICodiciProdottoSDA;
import com.elsagdatamat.dbtrace.bridge.engine.entities.CodiciProdottoPICodiciProdottoSDAPK;

public class CodiciProdottoPICodiciProdottoSDAManager extends
		BaseManager<CodiciProdottoPICodiciProdottoSDA, CodiciProdottoPICodiciProdottoSDAPK> implements
		ICodiciProdottoPICodiciProdottoSDAManager {
	private Map<String, String> conversionMap = null;

	private List<PatternAndCode> sdaEntryList;

	@Override
	public synchronized Map<String, String> getConversionMap() {
		if (conversionMap == null) {
			List<CodiciProdottoPICodiciProdottoSDA> list = this.generalDao.findAll(CodiciProdottoPICodiciProdottoSDA.class);

			conversionMap = new HashMap<String, String>();
			for (CodiciProdottoPICodiciProdottoSDA r : list) {
				conversionMap.put(r.getPiCode(), r.getSdaCode());
			}
		}
		return conversionMap;
	}

	@Override
	public synchronized String getSdaCode(String objectCode) {
		loadEntryList();
		PatternAndCode patternAndCode = retrievePatternByCode(objectCode);
		if (patternAndCode != null)
			return patternAndCode.sdaCode;
		log.debug(String.format("SDA code not found for objectCode '%s'", objectCode));
		return null;
	}

	@Override
	public synchronized String getPiCode(String objectCode) {
		loadEntryList();
		PatternAndCode patternAndCode = retrievePatternByCode(objectCode);
		if (patternAndCode != null)
			return patternAndCode.piCode;
		log.debug(String.format("PI code not found for objectCode '%s'", objectCode));
		return null;
	}

	private PatternAndCode retrievePatternByCode(String objectCode) {
		for (PatternAndCode patternAndCode : sdaEntryList) {
			if (patternAndCode.pattern.matcher(objectCode).matches()) {
				log.debug(String.format("SDA code for objectCode '%s' is %s", objectCode, patternAndCode.sdaCode));
				return patternAndCode;
			}
		}
		return null;
	}

	private void loadEntryList() {
		if (sdaEntryList == null) {
			sdaEntryList = new LinkedList<PatternAndCode>();
			List<CodiciProdottoPICodiciProdottoSDA> list = this.generalDao.findAll(CodiciProdottoPICodiciProdottoSDA.class);
			for (CodiciProdottoPICodiciProdottoSDA entity : list) {
				sdaEntryList.add(new PatternAndCode(Pattern.compile(entity.getCodeFormatRegex()), entity.getSdaCode(), entity.getPiCode()));
				log.debug(String.format("Regex for SDA code '%s' is '%s'", entity.getSdaCode(), entity.getCodeFormatRegex()));
			}
			log.debug("SdaEntryList loaded!");
		}
	}

	private class PatternAndCode {
		public Pattern pattern;
		public String sdaCode;
		public String piCode;

		public PatternAndCode(Pattern pattern, String sdaCode, String piCode) {
			super();
			this.pattern = pattern;
			this.sdaCode = sdaCode;
			this.piCode = piCode;
		}

	}

}
