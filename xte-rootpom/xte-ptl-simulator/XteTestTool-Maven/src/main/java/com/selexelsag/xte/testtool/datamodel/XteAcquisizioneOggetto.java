/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.datamodel;

import java.util.Date;


/**
 *
 * @author Tassara
 */
public class XteAcquisizioneOggetto {

    private String AcquisizioneOggettoType;
    private String AcquisizioneOggettoCode;
    private Date AcquisizioneOggettoDateTime;
    private String AcquisizioneOggettoOffice;

    public String getAcquisizioneOggettoType() {
        return AcquisizioneOggettoType;
    }

    public void setAcquisizioneOggettoType(String AcquisizioneOggettoType) {
        this.AcquisizioneOggettoType = AcquisizioneOggettoType;
    }

    public String getAcquisizioneOggettoCode() {
        return AcquisizioneOggettoCode;
    }

    public void setAcquisizioneOggettoCode(String AcquisizioneOggettoCode) {
        this.AcquisizioneOggettoCode = AcquisizioneOggettoCode;
    }
    
    public Date getAcquisizioneOggettoDateTime() {
        return AcquisizioneOggettoDateTime;
    }

    public void setAcquisizioneOggettoDateTime(Date AcquisizioneOggettoDateTime) {
        this.AcquisizioneOggettoDateTime = AcquisizioneOggettoDateTime;
    }  

    public String getAcquisizioneOggettoOffice() {
        return AcquisizioneOggettoOffice;
    }

    public void setAcquisizioneOggettoOffice(String AcquisizioneOggettoOffice) {
        this.AcquisizioneOggettoOffice = AcquisizioneOggettoOffice;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.AcquisizioneOggettoCode != null ? this.AcquisizioneOggettoCode.hashCode() : 0);
        hash = 97 * hash + (this.AcquisizioneOggettoDateTime != null ? this.AcquisizioneOggettoDateTime.hashCode() : 0);
        hash = 97 * hash + (this.AcquisizioneOggettoOffice != null ? this.AcquisizioneOggettoOffice.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final XteAcquisizioneOggetto other = (XteAcquisizioneOggetto) obj;
        if ((this.AcquisizioneOggettoCode == null) ? (other.AcquisizioneOggettoCode != null) : !this.AcquisizioneOggettoCode.equals(other.AcquisizioneOggettoCode)) {
            return false;
        }
        if ((this.AcquisizioneOggettoDateTime == null) ? (other.AcquisizioneOggettoDateTime != null) : !this.AcquisizioneOggettoDateTime.equals(other.AcquisizioneOggettoDateTime)) {
            return false;
        }
        if ((this.AcquisizioneOggettoOffice == null) ? (other.AcquisizioneOggettoOffice != null) : !this.AcquisizioneOggettoOffice.equals(other.AcquisizioneOggettoOffice)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "XteAcquisizioneOggettoPR{" + "AcquisizioneOggettoCode=" + AcquisizioneOggettoCode + ", AcquisizioneOggettoDateTime=" + AcquisizioneOggettoDateTime + ", AcquisizioneOggettoOffice=" + AcquisizioneOggettoOffice + '}';
    }
    

}
