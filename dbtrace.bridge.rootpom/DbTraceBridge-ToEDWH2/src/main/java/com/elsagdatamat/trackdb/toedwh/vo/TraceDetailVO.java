package com.elsagdatamat.trackdb.toedwh.vo;

import java.io.Serializable;

import org.springframework.beans.BeanUtils;

import com.elsagdatamat.trackdb.toedwh.entities.TraceDetail;

public class TraceDetailVO implements Serializable {	
	
	private static final long serialVersionUID = 1L;
	
	private String paramClass;
	
	private String paramValue;

	public TraceDetailVO(){
	}
	public TraceDetailVO(TraceDetail td){
		super();
		paramClass=td.getParamClass();
		paramValue=td.getParamValue();
	}
	
	public void setParamClass(String paramClass) {
		this.paramClass = paramClass;
	}
	public String getParamClass() {
		return paramClass;
	}
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	public String getParamValue() {
		return paramValue;
	}
}
