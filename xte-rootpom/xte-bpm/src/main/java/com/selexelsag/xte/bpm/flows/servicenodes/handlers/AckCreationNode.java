package com.selexelsag.xte.bpm.flows.servicenodes.handlers;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.drools.process.instance.WorkItemHandler;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemManager;

import com.selexelsag.xte.bpm.model.ValidationResults;
import com.selexelsag.xte.business.tracking.ITrasformation;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.model.flows.FlowResponse;
import com.selexelsag.xte.service.context.SpringApplicationContext;

public class AckCreationNode implements WorkItemHandler {

	private Logger logger = LogManager.getLogger(AckCreationNode.class);
	
	@Override
	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
		
	}

	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
	
		logger.debug("executeWorkItem START");
		
		Map<String, Object> results = null;

		FlowParams params = (FlowParams)workItem.getParameter("flowParams");
		if (params==null) {
			logger.debug("flowParams parameter is null!");
		}
		
		StringBuilder dataResult = (StringBuilder)workItem.getParameter("dataResult");
		if (dataResult==null) {
			logger.debug("dataResult parameter is null!");
		}
		
		FlowResponse flowResponse = (FlowResponse)workItem.getParameter("flowResponse");
		if (flowResponse==null) {
			logger.debug("flowResponse parameter is null!");
		}
		
		ValidationResults valres = (ValidationResults)workItem.getParameter("validationResults");
		if (valres==null) {
			logger.debug("validationResults parameter is null!");
		}
		
		try{
 			results = new HashMap<String, Object>();
 			
 			ITrasformation trasformation = (ITrasformation)SpringApplicationContext.
					getBean(ITrasformation.TRASFORMATION_BEAN_NAME);

 			trasformation.buildAckResponse(params, valres.getDataExtract(), dataResult, flowResponse);
 			
 			results.put("dataResult", dataResult);
 			results.put("flowResponse", flowResponse);
 		}catch (Throwable e){	
			logger.error(e, e);
			results = new HashMap<String, Object>();
			results.put("error", e);
			results.put("dataResult", dataResult);
			results.put("flowResponse", flowResponse);

		}finally{
			
			// logger.debug("executeWorkItem "+ getClass().getName() +" END, result is " + results.get("semanticValidationSuccess"));	
			manager.completeWorkItem(workItem.getId(), results);
			logger.debug("executeWorkItem END");
		}
	}

}
