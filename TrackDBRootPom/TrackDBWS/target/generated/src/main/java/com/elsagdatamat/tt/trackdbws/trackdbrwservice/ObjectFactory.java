
package com.elsagdatamat.tt.trackdbws.trackdbrwservice;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.elsagdatamat.tt.trackdbws.trackdbrwservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UseSealOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "UseSealOutput");
    private final static QName _PackDataRetrieveOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "packDataRetrieveOutput");
    private final static QName _GetNextSackImageFromNSPInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetNextSackImageFromNSPInput");
    private final static QName _GetSackImageFromNSPBySackCodeInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetSackImageFromNSPBySackCodeInput");
    private final static QName _GetTracesByFiltersListOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetTracesByFiltersListOutput");
    private final static QName _GetEsiPacksInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetEsiPacksInput");
    private final static QName _GetSealNumberLastUseOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetSealNumberLastUseOutput");
    private final static QName _PutTracesOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "PutTracesOutput");
    private final static QName _GetTokenOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetTokenOutput");
    private final static QName _GetNextSackImageFromNSPOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetNextSackImageFromNSPOutput");
    private final static QName _GetChunkSizeInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetChunkSizeInput");
    private final static QName _GetSackImageFromNSPByPackCodeInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetSackImageFromNSPByPackCodeInput");
    private final static QName _GetSealLastUseInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "getSealLastUseInput");
    private final static QName _UseSealInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "useSealInput");
    private final static QName _GetTokenInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetTokenInput");
    private final static QName _GetSackImageFromNSPByPackCodeOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetSackImageFromNSPByPackCodeOutput");
    private final static QName _PutTracesInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "PutTracesInput");
    private final static QName _GetSackImageFromNSPBySackCodeOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetSackImageFromNSPBySackCodeOutput");
    private final static QName _GetTracesByFiltersListInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetTracesByFiltersListInput");
    private final static QName _GetTracesInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetTracesInput");
    private final static QName _GetChunkSizeOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetChunkSizeOutput");
    private final static QName _GetTracesOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetTracesOutput");
    private final static QName _PackDataRetrieveInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "packDataRetrieveInput");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.elsagdatamat.tt.trackdbws.trackdbrwservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TokenTillToDateForEsiPack }
     * 
     */
    public TokenTillToDateForEsiPack createTokenTillToDateForEsiPack() {
        return new TokenTillToDateForEsiPack();
    }

    /**
     * Create an instance of {@link GetSealLastUseResponse }
     * 
     */
    public GetSealLastUseResponse createGetSealLastUseResponse() {
        return new GetSealLastUseResponse();
    }

    /**
     * Create an instance of {@link UseSealResponse }
     * 
     */
    public UseSealResponse createUseSealResponse() {
        return new UseSealResponse();
    }

    /**
     * Create an instance of {@link PackDataRetrieveOutput }
     * 
     */
    public PackDataRetrieveOutput createPackDataRetrieveOutput() {
        return new PackDataRetrieveOutput();
    }

    /**
     * Create an instance of {@link TokenTilltoDateByFiltersList }
     * 
     */
    public TokenTilltoDateByFiltersList createTokenTilltoDateByFiltersList() {
        return new TokenTilltoDateByFiltersList();
    }

    /**
     * Create an instance of {@link TracesListNextToken }
     * 
     */
    public TracesListNextToken createTracesListNextToken() {
        return new TracesListNextToken();
    }

    /**
     * Create an instance of {@link GetSealLastUseInput }
     * 
     */
    public GetSealLastUseInput createGetSealLastUseInput() {
        return new GetSealLastUseInput();
    }

    /**
     * Create an instance of {@link UseSealInput }
     * 
     */
    public UseSealInput createUseSealInput() {
        return new UseSealInput();
    }

    /**
     * Create an instance of {@link TracesList }
     * 
     */
    public TracesList createTracesList() {
        return new TracesList();
    }

    /**
     * Create an instance of {@link TokenTilltoDate }
     * 
     */
    public TokenTilltoDate createTokenTilltoDate() {
        return new TokenTilltoDate();
    }

    /**
     * Create an instance of {@link PackCodeForRetrieve }
     * 
     */
    public PackCodeForRetrieve createPackCodeForRetrieve() {
        return new PackCodeForRetrieve();
    }

    /**
     * Create an instance of {@link TraceDetailsList }
     * 
     */
    public TraceDetailsList createTraceDetailsList() {
        return new TraceDetailsList();
    }

    /**
     * Create an instance of {@link Trace }
     * 
     */
    public Trace createTrace() {
        return new Trace();
    }

    /**
     * Create an instance of {@link TraceDetail }
     * 
     */
    public TraceDetail createTraceDetail() {
        return new TraceDetail();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UseSealResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "UseSealOutput")
    public JAXBElement<UseSealResponse> createUseSealOutput(UseSealResponse value) {
        return new JAXBElement<UseSealResponse>(_UseSealOutput_QNAME, UseSealResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PackDataRetrieveOutput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "packDataRetrieveOutput")
    public JAXBElement<PackDataRetrieveOutput> createPackDataRetrieveOutput(PackDataRetrieveOutput value) {
        return new JAXBElement<PackDataRetrieveOutput>(_PackDataRetrieveOutput_QNAME, PackDataRetrieveOutput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenTilltoDateByFiltersList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetNextSackImageFromNSPInput")
    public JAXBElement<TokenTilltoDateByFiltersList> createGetNextSackImageFromNSPInput(TokenTilltoDateByFiltersList value) {
        return new JAXBElement<TokenTilltoDateByFiltersList>(_GetNextSackImageFromNSPInput_QNAME, TokenTilltoDateByFiltersList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetSackImageFromNSPBySackCodeInput")
    public JAXBElement<String> createGetSackImageFromNSPBySackCodeInput(String value) {
        return new JAXBElement<String>(_GetSackImageFromNSPBySackCodeInput_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TracesListNextToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetTracesByFiltersListOutput")
    public JAXBElement<TracesListNextToken> createGetTracesByFiltersListOutput(TracesListNextToken value) {
        return new JAXBElement<TracesListNextToken>(_GetTracesByFiltersListOutput_QNAME, TracesListNextToken.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenTillToDateForEsiPack }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetEsiPacksInput")
    public JAXBElement<TokenTillToDateForEsiPack> createGetEsiPacksInput(TokenTillToDateForEsiPack value) {
        return new JAXBElement<TokenTillToDateForEsiPack>(_GetEsiPacksInput_QNAME, TokenTillToDateForEsiPack.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSealLastUseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetSealNumberLastUseOutput")
    public JAXBElement<GetSealLastUseResponse> createGetSealNumberLastUseOutput(GetSealLastUseResponse value) {
        return new JAXBElement<GetSealLastUseResponse>(_GetSealNumberLastUseOutput_QNAME, GetSealLastUseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "PutTracesOutput")
    public JAXBElement<BigInteger> createPutTracesOutput(BigInteger value) {
        return new JAXBElement<BigInteger>(_PutTracesOutput_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetTokenOutput")
    public JAXBElement<String> createGetTokenOutput(String value) {
        return new JAXBElement<String>(_GetTokenOutput_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TracesListNextToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetNextSackImageFromNSPOutput")
    public JAXBElement<TracesListNextToken> createGetNextSackImageFromNSPOutput(TracesListNextToken value) {
        return new JAXBElement<TracesListNextToken>(_GetNextSackImageFromNSPOutput_QNAME, TracesListNextToken.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetChunkSizeInput")
    public JAXBElement<String> createGetChunkSizeInput(String value) {
        return new JAXBElement<String>(_GetChunkSizeInput_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetSackImageFromNSPByPackCodeInput")
    public JAXBElement<String> createGetSackImageFromNSPByPackCodeInput(String value) {
        return new JAXBElement<String>(_GetSackImageFromNSPByPackCodeInput_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSealLastUseInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "getSealLastUseInput")
    public JAXBElement<GetSealLastUseInput> createGetSealLastUseInput(GetSealLastUseInput value) {
        return new JAXBElement<GetSealLastUseInput>(_GetSealLastUseInput_QNAME, GetSealLastUseInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UseSealInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "useSealInput")
    public JAXBElement<UseSealInput> createUseSealInput(UseSealInput value) {
        return new JAXBElement<UseSealInput>(_UseSealInput_QNAME, UseSealInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetTokenInput")
    public JAXBElement<XMLGregorianCalendar> createGetTokenInput(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetTokenInput_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TracesList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetSackImageFromNSPByPackCodeOutput")
    public JAXBElement<TracesList> createGetSackImageFromNSPByPackCodeOutput(TracesList value) {
        return new JAXBElement<TracesList>(_GetSackImageFromNSPByPackCodeOutput_QNAME, TracesList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TracesList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "PutTracesInput")
    public JAXBElement<TracesList> createPutTracesInput(TracesList value) {
        return new JAXBElement<TracesList>(_PutTracesInput_QNAME, TracesList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TracesList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetSackImageFromNSPBySackCodeOutput")
    public JAXBElement<TracesList> createGetSackImageFromNSPBySackCodeOutput(TracesList value) {
        return new JAXBElement<TracesList>(_GetSackImageFromNSPBySackCodeOutput_QNAME, TracesList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenTilltoDateByFiltersList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetTracesByFiltersListInput")
    public JAXBElement<TokenTilltoDateByFiltersList> createGetTracesByFiltersListInput(TokenTilltoDateByFiltersList value) {
        return new JAXBElement<TokenTilltoDateByFiltersList>(_GetTracesByFiltersListInput_QNAME, TokenTilltoDateByFiltersList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenTilltoDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetTracesInput")
    public JAXBElement<TokenTilltoDate> createGetTracesInput(TokenTilltoDate value) {
        return new JAXBElement<TokenTilltoDate>(_GetTracesInput_QNAME, TokenTilltoDate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetChunkSizeOutput")
    public JAXBElement<Integer> createGetChunkSizeOutput(Integer value) {
        return new JAXBElement<Integer>(_GetChunkSizeOutput_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TracesListNextToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetTracesOutput")
    public JAXBElement<TracesListNextToken> createGetTracesOutput(TracesListNextToken value) {
        return new JAXBElement<TracesListNextToken>(_GetTracesOutput_QNAME, TracesListNextToken.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PackCodeForRetrieve }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "packDataRetrieveInput")
    public JAXBElement<PackCodeForRetrieve> createPackDataRetrieveInput(PackCodeForRetrieve value) {
        return new JAXBElement<PackCodeForRetrieve>(_PackDataRetrieveInput_QNAME, PackCodeForRetrieve.class, null, value);
    }

}
