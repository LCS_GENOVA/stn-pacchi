package com.selexelsag.xte.business.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.xte.business.exceptions.BusinessPersistentAccessException;
import com.selexelsag.xte.business.tracking.IPersistentAccess;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.persistence.XteDBAccess;

import com.selexelsag.xte.persistence.XteMessagesDBAccess;

import com.selexelsag.xte.persistence.exceptions.PersistenceException;
import com.selexelsag.xte.persistence.utils.PersistenceUtils;
import com.selexelsag.xte.service.system.IAppConfig;

public class BasePersistentAccess implements IPersistentAccess {

	@Autowired
	private XteDBAccess dbAccess;
	
	private Logger logger = LogManager.getLogger(BasePersistentAccess.class.getName());
		
	private static final String QUERY_LABEL_OFFICE = "OFFICE";
	private static final String QUERY_LABEL_SERVICES = "SERVICES";
	
	private static final String URLMAP_PROP_NAME_PREFIX = "com.selexelsag.xte.business.connection.params.table.";
	
	@Autowired
	private IAppConfig appConfig;
	
	
	@Autowired
	private XteMessagesDBAccess messagesDBAccess;
	
	
	public IAppConfig getAppConfig() {
		return appConfig;
	}
	
	public void setAppConfig(IAppConfig appConfig) {
		this.appConfig = appConfig;
	}
	
	@Override
	public void save(FlowParams flowParams, HashMap<String, String> headerData, StringBuilder data) throws BusinessPersistentAccessException {
		logger.debug("TODO: save(" + flowParams + ", " + data);
		
		try {
			messagesDBAccess.saveMessage(PersistenceUtils.fillAsyncMessages(flowParams.getFlowId(), null, flowParams.getOffRefID(),null, data));
		} catch (PersistenceException e) {
			BusinessPersistentAccessException bpae = new BusinessPersistentAccessException("Errore nel salvataggio dei dati", e);
			throw bpae;
		}
		
	}
	
	private void fillConnectionParametersOFFICE(FlowParams flowParams)
			throws BusinessPersistentAccessException {
	
		String hostName = null;
		String ip = null;
		String officeId = null;
		String aspRelativeUrl = null;
		
		String service = flowParams.getServiceRequestProviderParameters().get(FlowParams.REQ_PROVIDER_KEY_1);
		String channel = flowParams.getServiceRequestProviderParameters().get(FlowParams.REQ_PROVIDER_KEY_2);
		String event = flowParams.getServiceRequestProviderParameters().get(FlowParams.REQ_PROVIDER_KEY_3);
		String version = flowParams.getServiceRequestProviderParameters().get(FlowParams.REQ_PROVIDER_KEY_4);
		String channelId = flowParams.getServiceRequestProviderParameters().get(FlowParams.REQ_PROVIDER_KEY_5);
		
		try {
			Map<String, String> map = dbAccess.getFromOFFICES(channelId);
			hostName = map.get(XteDBAccess.HOST_NAME);
			ip = map.get(XteDBAccess.IP);
			officeId = map.get(XteDBAccess.OFFICE_ID);
		} catch (PersistenceException e) {
			BusinessPersistentAccessException bpae = new BusinessPersistentAccessException("Errore nel caricamento da db ", e);
			throw bpae;
		}
		Map<String, String> ret = getRelativeUrlAndFoward(service, channel, event, version);
		
		aspRelativeUrl = ret.get(XteDBAccess.URL);
		
		flowParams.setFowardToInt(ret.get(XteDBAccess.FORWARD_TO_INT));
		
		flowParams.setOffRefID(officeId);
		
		URL url = composeUrl(hostName, ip, aspRelativeUrl);
		
		flowParams.setHttpServiceUrl(url);
	}
	
	private void fillConnectionParametersSERVICES(FlowParams flowParams)
			throws BusinessPersistentAccessException {
		String hostName = null;
		String ip = null;
		String officeId = null;
		String aspRelativeUrl = null;
		
		String service = flowParams.getServiceRequestProviderParameters().get(FlowParams.REQ_PROVIDER_KEY_1);
		String channel = flowParams.getServiceRequestProviderParameters().get(FlowParams.REQ_PROVIDER_KEY_2);
		String event = flowParams.getServiceRequestProviderParameters().get(FlowParams.REQ_PROVIDER_KEY_3);
		String version = flowParams.getServiceRequestProviderParameters().get(FlowParams.REQ_PROVIDER_KEY_4);

		try {
			Map<String, String> map = dbAccess.getFromSERVICES(service);
			hostName = map.get(XteDBAccess.HOST_NAME);
			ip = map.get(XteDBAccess.IP);
			officeId = map.get(XteDBAccess.OFFICE_ID);
		} catch (PersistenceException e) {
			BusinessPersistentAccessException bpae = new BusinessPersistentAccessException("Errore nel caricamento da db ", e);
			throw bpae;
		}
		Map<String, String> ret = getRelativeUrlAndFoward(service, channel, event, version);
		
		aspRelativeUrl = ret.get(XteDBAccess.URL);
		
		flowParams.setFowardToInt(ret.get(XteDBAccess.FORWARD_TO_INT));
		
		flowParams.setOffRefID(officeId);
		
		URL url = composeUrl(hostName, ip, aspRelativeUrl);
		
		flowParams.setHttpServiceUrl(url);
	}
	
	private URL composeUrl(String hostName, String ip, String relativeUrl) 
		throws BusinessPersistentAccessException {
		StringBuilder finalUrl = new StringBuilder();
		finalUrl.append("http://");
		if (ip!=null && !ip.trim().isEmpty()) {
			finalUrl.append(String.format("%1$s%2$s", ip, relativeUrl));
		}else {
			finalUrl.append(String.format("%1$s%2$s", hostName, relativeUrl));
		}		
		
		URL url = null;
		try {
			url = new URL(finalUrl.toString());
		} catch (MalformedURLException e) {
			BusinessPersistentAccessException bpae = new BusinessPersistentAccessException("Exception forming the url", e);
			throw bpae;
		}
		return url;
	}
	
	private Map<String, String> getRelativeUrlAndFoward(String service, String channel, String event, String version) 
			throws BusinessPersistentAccessException {
		/* TODO: bisogna implementare le chiamate per le seguenti query:
		final String SELECT_ASP_URL_STATEMENT 	= "SELECT URL FROM EVENTS WHERE SERVICE = ? AND CHANNEL = ? AND EVENT_ID = ? AND VERSION = ?";
		*/
		Map<String, String> map;
		try {
			map = dbAccess.getFromEVENTS(service, channel, event, version);
		} catch (PersistenceException e) {
			BusinessPersistentAccessException bpae = new BusinessPersistentAccessException("Errore nel caricamento da db ", e);
			throw bpae;
		}
		return map;
	}
	
	@Override
	public void fillConnectionParameters(FlowParams flowParams)
			throws BusinessPersistentAccessException {
		
		String source = flowParams.getServiceRequestProviderParameters().get(FlowParams.REQ_PROVIDER_SOURCE);
		String propName = URLMAP_PROP_NAME_PREFIX + source.toLowerCase();
		String operationName = appConfig.getUrlMapParam(propName);
		
		if (operationName==null) {
			BusinessPersistentAccessException bpae = new BusinessPersistentAccessException("nessuna query associata alla sorgente " + source, null);
			throw bpae;
		}
		
		if (operationName.equals(QUERY_LABEL_OFFICE)) {
			fillConnectionParametersOFFICE(flowParams);
		}else if (operationName.equals(QUERY_LABEL_SERVICES)) {
			fillConnectionParametersSERVICES(flowParams);
		}else {
			BusinessPersistentAccessException bpae = new BusinessPersistentAccessException("query non riconosciuta per la sorgente " + source, null);
			throw bpae;
		}
		
	}
}
