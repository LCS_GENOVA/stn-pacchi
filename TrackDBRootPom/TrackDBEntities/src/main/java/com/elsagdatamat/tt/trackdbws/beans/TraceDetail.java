package com.elsagdatamat.tt.trackdbws.beans;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "TRACEDETAILS")
public class TraceDetail implements Serializable {	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID", nullable=false)
	@SequenceGenerator(name="TRD_SEQ", sequenceName="TRD_SEQ", allocationSize=50)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRD_SEQ")
	private Long traceDetailId;
	
	@Column(name="PARAM_CLASS", nullable=false)
	private String paramClass;
	
	@Column(name="PARAM_VALUE", nullable=false)
	private String paramValue;
	
	@ManyToOne
	@JoinColumn(name="TRA_ID", nullable=false)
	private Trace trace;
	
	public void setParamClass(String paramClass) {
		this.paramClass = paramClass;
	}
	public String getParamClass() {
		return paramClass;
	}
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	public String getParamValue() {
		return paramValue;
	}
	public Trace getTrace() {
		return trace;
	}
	public void setTrace(Trace trace) {
		this.trace = trace;
	}
	public Long getTraceDetailId() {
		return traceDetailId;
	}
	public void setTraceDetailId(Long traceDetailId) {
		this.traceDetailId = traceDetailId;
	}
}
