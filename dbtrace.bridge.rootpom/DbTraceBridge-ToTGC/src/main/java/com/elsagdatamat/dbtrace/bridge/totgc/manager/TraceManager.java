package com.elsagdatamat.dbtrace.bridge.totgc.manager;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.elsagdatamat.dbtrace.bridge.totgc.dao.ITraceDAO;
import com.elsagdatamat.dbtrace.bridge.totgc.entities.TraceTypesToSend;
import com.elsagdatamat.dbtrace.bridge.totgc.vo.AcceptanceInfoVO;
import com.elsagdatamat.framework.search.Filter;
import com.elsagdatamat.framework.search.Search;
import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.selexelsag.tracktrace.extension.objectmodel.Product;

@Service
public class TraceManager implements ITraceManager {
	
	private static final Log log = LogFactory.getLog(TraceManager.class);

//	private static final String DETAIL_SA_KEY = "";

	public static final String FIXED_CHANNEL = "SDA";
	public static final String FIXED_TRACED_ENTITY = "PACCO";

	private final static String ID_STATUS_FIELD = "idStatus";
	private final static String LABEL_TRACED_ENTITY_FIELD = "labelTracedEntity";
	private final static String TRACED_ENTITY_FIELD = "tracedEntity";
	private final static String CHANNEL_FIELD = "channel";
	private final static String WHEN_REGISTERED_FIELD = "whenRegistered";
	private final static String WHAT_HAPPENED_FIELD = "whatHappened";
	private final static String TRACE_ID_FIELD = "traceId";

	static ProductManagerToTGC productManagerToTGC;
	static ITraceTypesToSendManager traceTypesToSendManager;
	
	static List<String> interestingAdditionalServicesPerif = null;
	static List<String> interestingAdditionalServicesCdg = null;
	static List<String> interestingAdditionalServicesSdp = null;
	static List<String> interestingProducts = null;

	private ITraceDAO traceDAO;
	public void setTraceDAO(ITraceDAO traceDAO) {
		this.traceDAO = traceDAO;
	}
	
	public void setProductManagerToTGC(ProductManagerToTGC productManagerToTGC) {
		TraceManager.productManagerToTGC = productManagerToTGC;
		if (interestingProducts == null) {
			initInterestingProducts();
		}
		if (interestingAdditionalServicesPerif == null) {
			initInterestingAdditionalServices();
		}
	}
	private void initInterestingProducts() {
		interestingProducts = new ArrayList<String>();
		List<Product> pList = productManagerToTGC.getInterestingProducts();
		for (Product p : pList) {
			interestingProducts.add(p.getPtype());
		}
	}
	private void initInterestingAdditionalServices() {
		interestingAdditionalServicesPerif = productManagerToTGC.getInterestingAdditionalServices().perif;
		interestingAdditionalServicesCdg = productManagerToTGC.getInterestingAdditionalServices().cdg;
		interestingAdditionalServicesSdp = productManagerToTGC.getInterestingAdditionalServices().sdp;
	}
	
	static List<String> configuredWhatHappened = null;
	public void setTraceTypesToSendManager(ITraceTypesToSendManager traceTypesToSendManager) {
		TraceManager.traceTypesToSendManager = traceTypesToSendManager;
		if (configuredWhatHappened == null) {
			initConfiguredWhatHappened();
		}
	}
	private void initConfiguredWhatHappened() {
		configuredWhatHappened = new ArrayList<String>();
		List<TraceTypesToSend> findByDestinationTracedEntityChannel =
				traceTypesToSendManager.findByDestinationTracedEntityChannel(
							"TGC", FIXED_TRACED_ENTITY, FIXED_CHANNEL);
		for (TraceTypesToSend t : findByDestinationTracedEntityChannel) {
			configuredWhatHappened.add(t.getWhatHappened());
		}
	}


	@Override
	public List<com.selexelsag.tracktrace.trackdbws.xml.Trace> loadNextTraces(
			Date lastFetchedRecordDate,
			Long lastFetchedRecordId,
			Date dateTillto,
			int maxRows) throws DatatypeConfigurationException {
		
		return filterByAdditionalServices(newLoadNextTraces(lastFetchedRecordDate, lastFetchedRecordId, dateTillto, maxRows));
	}
	
	private com.selexelsag.tracktrace.trackdbws.xml.Trace convert(Trace trace) throws DatatypeConfigurationException {
		com.selexelsag.tracktrace.trackdbws.xml.Trace txml = new com.selexelsag.tracktrace.trackdbws.xml.Trace();
		txml.setChannel(trace.getChannel());
		txml.setIdChannel(trace.getIdChannel());
		txml.setIdCorrelazione(trace.getIdCorrelazione());
		txml.setIdForwardTo(trace.getIdForwardTo());
		txml.setIdStatus(trace.getIdStatus());
		txml.setIdTracedEntity(trace.getIdTracedEntity());
		txml.setIdTracedExternal(trace.getIdTracedExternal());
		txml.setLabelTracedEntity(trace.getLabelTracedEntity());
		txml.setServiceName(trace.getServiceName());
		txml.setSysForwardTo(trace.getSysForwardTo());
		txml.setTracedEntity(trace.getTracedEntity());
//		txml.setTraceDetailsList(trace.getInternalDetailList());
		txml.setWhatHappened(trace.getWhatHappened());
		txml.setWhenHappened(dateToCalendar(trace.getWhenHappened()));
		txml.setWhenRegistered(dateToCalendar(trace.getWhenRegistered()));
		txml.setWhereHappened(trace.getWhereHappened());
		
		return txml;
	}
	
	private XMLGregorianCalendar dateToCalendar(Date d) throws DatatypeConfigurationException {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(d);
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);	
	}

	@Override
	public List<Trace> newLoadNextTraces(Date lastFetchedRecordDate, Long lastFetchedRecordId, Date dateTillto,
			int maxRows) throws DatatypeConfigurationException {
		
		log.info(String.format("Entering newLoadNextTraces() for lastFetchedRecordDate '%s', lastFetchedRecordId '%s', dateTillto='%s', maxRows '%s'", 
				lastFetchedRecordDate,
				lastFetchedRecordId, 
				dateTillto,
				maxRows));
		
		Search search = new Search(Trace.class);
		search.addFilterAnd(
				Filter.or(
					Filter.and(
							Filter.greaterThan(WHEN_REGISTERED_FIELD, lastFetchedRecordDate),
							Filter.lessThan(WHEN_REGISTERED_FIELD, dateTillto)),
					Filter.and(
							Filter.equal(WHEN_REGISTERED_FIELD, lastFetchedRecordDate),
							Filter.greaterThan(TRACE_ID_FIELD, lastFetchedRecordId))),
				Filter.and(
					Filter.in(WHAT_HAPPENED_FIELD, configuredWhatHappened),
					Filter.in(LABEL_TRACED_ENTITY_FIELD, interestingProducts),
					Filter.equal(ID_STATUS_FIELD, "000"),
					Filter.equal(TRACED_ENTITY_FIELD, TraceManager.FIXED_TRACED_ENTITY),
					Filter.equal(CHANNEL_FIELD, TraceManager.FIXED_CHANNEL)));

		search.addSort(WHEN_REGISTERED_FIELD);
		search.addSort(TRACE_ID_FIELD);
	
		search.setMaxResults(maxRows);

		List<Trace> list = traceDAO.search(search);
		
		return list;
	}
	
	@Override
	public List<com.selexelsag.tracktrace.trackdbws.xml.Trace> filterByAdditionalServices(
			List<com.elsagdatamat.tt.trackdbws.beans.Trace> l) throws DatatypeConfigurationException {
		
		List<com.selexelsag.tracktrace.trackdbws.xml.Trace> list = 
				new ArrayList<com.selexelsag.tracktrace.trackdbws.xml.Trace>();
		
		for (Trace t : l) {
			
			boolean found = false;
			List<AcceptanceInfoVO> acceptanceTraces = traceDAO.getAcceptanceInfo(t.getIdTracedEntity());
			for (AcceptanceInfoVO at : acceptanceTraces) {
				
				String detailsCsv = at.getAdditionalService();
				String[] details = detailsCsv.split(";");
				for (String detail : details) {
					if (at.getWhatHappened().equals("ACC_CMP")) {
						if (interestingAdditionalServicesPerif.contains(detail)) {
							found = true;
//							list.add(t);
//							break;
						}
					} else if (at.getWhatHappened().equals("ACC_NSP")) {
						if (interestingAdditionalServicesSdp.contains(detail)) {
							found = true;
//							list.add(t);
//							break;
						}
					}
					if (found) {
						break;
					}
				}
				if (found) {
					break;
				}
			}
			if (found) {
				list.add(convert(t));
			}
		}
		return list;
	}
}
