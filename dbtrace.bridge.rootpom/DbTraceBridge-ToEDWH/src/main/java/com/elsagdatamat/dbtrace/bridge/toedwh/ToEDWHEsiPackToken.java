package com.elsagdatamat.dbtrace.bridge.toedwh;

public class ToEDWHEsiPackToken extends ToEDWHToken {
	public static final String TOKEN_ID="DBTraceToEDWHEsiPackBridge";
	
	@Override
	public String getServiceId() {
		return TOKEN_ID;
	}
}
