package com.selexelsag.xte.persistence.dao;

import java.util.List;

import com.selexelsag.messagetobedelivered.entity.MessageToBeDelivered;

public interface MessageToBeDeliveredDao 
{

    void insert(MessageToBeDelivered entity);

    List<MessageToBeDelivered> listAll();

	void flushAndClear();
}
