package com.selexelsag.dbtrace.bridge.to.omp.manager;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.elsagdatamat.dbtrace.bridge.engine.dao.IModPagContrPiSdaDAO;
import com.elsagdatamat.dbtrace.bridge.engine.dao.ModPagContrPiSdaDAO;
import com.elsagdatamat.dbtrace.bridge.engine.dao.StatusPiSdaDAO;
import com.elsagdatamat.dbtrace.bridge.engine.entities.ModPagContrPiSda;
import com.elsagdatamat.dbtrace.bridge.engine.entities.OfficeMapper;
import com.elsagdatamat.dbtrace.bridge.engine.entities.StatusPiSda;
import com.elsagdatamat.dbtrace.bridge.engine.entities.StatusPiSdaPK;
import com.elsagdatamat.framework.utils.PairLinkedList;

/**
 * 
 * @author arodriguez
 * 
 */
public class OMPManager extends OMPAbstractManager implements OMPManagerInterface {
	
	private static final Log log = LogFactory.getLog(OMPManager.class);

	private Integer minutesToRecoveryMinValue = 60;
	private Integer minutesToRestartMinValue = 15;
	private IModPagContrPiSdaDAO mpDao;

	private boolean releaseDateNotExpired(Date releaseDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -1 * minutesToRestartMinValue);
		if (releaseDate != null && releaseDate.compareTo(calendar.getTime()) > 0) {
			return true;
		}
		return false;
	}

	private boolean accessDateExpired(Date accessDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -1 * minutesToRecoveryMinValue);
		if (accessDate == null || accessDate.compareTo(calendar.getTime()) < 0) {
			return true;
		}
		return false;
	}

	@Override
	public void setMinutesToRecoveryMinValue(Integer minutesToRecoveryMinValue) {
		this.minutesToRecoveryMinValue = minutesToRecoveryMinValue;
	}

	@Override
	public void setMinutesToRestartMinValue(Integer minutesToRestartMinValue) {
		this.minutesToRestartMinValue = minutesToRestartMinValue;
	}

	@Override
	public Integer getMinutesToRecoveryMinValue() {
		return minutesToRecoveryMinValue;
	}

	@Override
	public Integer getMinutesToRestartMinValue() {
		return minutesToRestartMinValue;
	}

	@Override
	public Long getNextSequenceCode() {
		Long result = Long.valueOf(-1);
		try {
			Object qResult = generalDao.executeNativeSqlSingleValue("select DID_SEQ.NEXTVAL from dual");
			result = ((BigDecimal)qResult).longValue();
		} catch(Exception ex) {
			log.warn("Unable to get DID code (check SQL sequence 'DID_SEQ'): " + ex.getMessage());
		}
		return result;
	}

	@Override
	public Map<String, String> loadAllPostOfficeCodes() {
		Map<String, String> result = new HashMap<String, String>();
		
		try {
						
			PairLinkedList<String, Object> properties = new PairLinkedList<String, Object>();
			properties.add(OfficeMapper.FAMILY, "OMP");
			List<OfficeMapper> allPostOffices = generalDao.findByProperties(OfficeMapper.class,properties);
												
			for (OfficeMapper po : allPostOffices) 
			{
				if(log.isTraceEnabled())
					log.trace("ufficio caricato: " + po.getFrazionarioInterno());
				
				result.put(po.getFrazionarioInterno(), po.getFrazionarioInterno());
			}
		} catch(Exception ex) {
			
			log.warn("Unable to load P.O. codes from DB: " + ex.getMessage());
		}
		return result;
	}

	@Override
	public String getBillModeCode(String keyCode) {
		try {
			ModPagContrPiSda mpQuery = new ModPagContrPiSda() {{
				setReverseMapping(1);
				setPiChannel("OMP");
			}};
			mpQuery.setSdaCode(keyCode);
			List<ModPagContrPiSda> allModPag = mpDao.findByExample(mpQuery, new String[]{});
			if (!allModPag.isEmpty())
				return allModPag.get(0).getPiCode();
		} catch(Exception ex) {
			log.warn("Unable to load mod. pag. codes from DB: " + ex.getMessage());
		}
		log.warn("Unable to find translation for code: " + keyCode + "; used as is");
		return null;
	}
	
	/**
	 * try to get transcoded ID_STATUS field from DB
	 * @param keyCode  String the code to seek
	 * @param keyChannel  String the channel to seek
	 * @return String  transcodified ID_STATUS field if found, null otherwise
	 */
	@Override
	public String getIdStatusTranscoded(String keyCode, String keyChannel) {
		try {
			StatusPiSdaDAO idStatusDao = new StatusPiSdaDAO();
			StatusPiSdaPK seekKey = new StatusPiSdaPK();
			seekKey.setPiChannel(keyChannel);
			seekKey.setStatusPI(keyCode);
			StatusPiSda statusSda = idStatusDao.findById(seekKey, false);
			if (statusSda != null)
				return statusSda.getStatusSDA();
		} catch(Exception ex) {
			log.warn("Unable to load status codes from DB: " + ex.getMessage());
		}
		log.warn("Unable to find translation for code: " + keyCode + "; used as is");
		return keyCode;
	}

	/**
	 * Verifica se l'ufficio cercato � censito come OMP
	 * @param code	frazionario dell'ufficio
	 */
	@Override
	public OfficeMapper findOfficeMapperByCode(String code) {
		
		try{
			PairLinkedList<String, Object> properties = new PairLinkedList<String, Object>();
			properties.add(OfficeMapper.FRAZIONARIO_INTERNO, code);
			properties.add(OfficeMapper.FAMILY, "OMP");
			List<OfficeMapper> officeMapperList = generalDao.findByProperties(OfficeMapper.class, properties);
			
			if (officeMapperList.size() == 0)
				return null;
			
			if (officeMapperList.size() > 1)
				log.warn("Ufficio con frazionario: " + code + " trovato piu' volte su DB");
			
			return officeMapperList.get(0);
			
		}catch(Exception ex) {
			
			log.warn("Unable to load office from DB: " + ex.getMessage());
		}
		
		return null;
	}

	public IModPagContrPiSdaDAO getMpDao() {
		return mpDao;
	}

	public void setMpDao(IModPagContrPiSdaDAO mpDao) {
		this.mpDao = mpDao;
	}
	
	
}
