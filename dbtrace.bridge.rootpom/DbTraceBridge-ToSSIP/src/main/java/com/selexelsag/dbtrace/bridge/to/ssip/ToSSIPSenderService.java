/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.ssip;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.system.ServiceMBeanSupport;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.selexelsag.dbtrace.bridge.to.ssip.manager.SSIPManager;
import com.selexelsag.dbtrace.bridge.to.ssip.manager.SSIPManagerInterface;
import com.selexelsag.dbtrace.bridge.to.ssip.message.SSIPMessageBuilder;
import com.selexelsag.dbtrace.bridge.to.ssip.message.SSIPMessageBuilderInterface;

/**
 * @author arodriguez
 * 
 */
public class ToSSIPSenderService extends ServiceMBeanSupport implements ToSSIPSenderServiceMBean {

	private final static Log log = LogFactory.getLog(ToSSIPSenderService.class);

	private boolean created;
	private boolean started;
	private String configPath;
	private String springConfigPath;
	private long errorCount = 0;
	private Date lastStartDate = new Date();

	private ClassPathXmlApplicationContext springContext;
	private SSIPManagerInterface ssipManager;
	private SSIPMessageBuilderInterface ssipMBuilder;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss");

	
	/**
	 * init operations at server startup
	 */
	private void serviceStartInits() {
		lastStartDate = new Date();
		ssipManager = bindSSIPManager();
		ssipMBuilder = bindSSIPMessageBuilder();
		if (ssipMBuilder != null) {
			ssipMBuilder.setNumberOfRuns(new long[]{0,0,0});
			ssipMBuilder.loadQueryParams();
		}
	}

	/**
	 * start SpringContext
	 * @throws Exception
	 */
	private void createSpringContext() throws Exception {
		log.info("Starting Spring context. . .");
		springContext = new ClassPathXmlApplicationContext(springConfigPath);
		springContext.registerShutdownHook();
	}

	/*
	 * (non-Javadoc)
	 * @see org.jboss.system.ServiceMBeanSupport#create()
	 */
	@Override
	public void create() throws Exception {
		log.info("Create service invoked. . .");
		if (!created) {
			try {
				// TODO: put some inits here or remove try/catch
				created = true;
			} catch (Exception ex) {
				errorCount++;
				created = false;
				log.error("Unable to start Spring Context", ex);
			}
		}
		log.info("Service created.");
	}

	/*
	 * (non-Javadoc)
	 * @see org.jboss.system.ServiceMBeanSupport#destroy()
	 */
	@Override
	public void destroy() {
		log.info("Destroy service invoked; stopping Spring context. . .");
		if (created) {
			try {
				stop();
				if (springContext.isActive()) {
					springContext.close();
					springContext.destroy();
					springContext = null;
				}
				created = false;
			} catch (Throwable ex) {
				log.error(" Failed to destroy service. ", ex);
			}
		}
		log.info("Service destroyed.");
	}

	/*
	 * (non-Javadoc)
	 * @see org.jboss.system.ServiceMBeanSupport#start()
	 */
	@Override
	public void start() throws Exception {
		if (!started) {
			if (created) // added because of service dependencies
				createSpringContext();
			log.info("Starting Spring ...");
			try {
				if (!springContext.isActive()) {
					springContext.refresh();
					springContext.start();
				}
				started = true;
				serviceStartInits();
				log.info("Spring context started. OK");
			} catch (Throwable ex) {
				errorCount++;
				started = false;
				log.error("Failed to start Spring context ", ex);
			}
			return;
		}
		log.info("Spring already started");
	}

	/*
	 * (non-Javadoc)
	 * @see org.jboss.system.ServiceMBeanSupport#stop()
	 */
	@Override
	public void stop() {
		if (started) {
			log.info("Stopping Spring ...");
			try {
				springContext.close();
				started = false;
				log.info("Spring context stopped. OK");
			} catch (Throwable ex) {
				errorCount++;
				started = false;
				log.error("Failed to stop Spring context ", ex);
			}
			return;
		}
		log.info("Spring already stopped");
	}

	private SSIPManagerInterface bindSSIPManager() {
		if (springContext.containsBean(SSIPManagerInterface.DEFAULT_NAME)) {
			return (SSIPManagerInterface)springContext.getBean(SSIPManagerInterface.DEFAULT_NAME);
		} 
		return springContext.getBean(SSIPManager.class); // not found, try with concrete class
	}

	private SSIPMessageBuilderInterface bindSSIPMessageBuilder() {
		if (springContext.containsBean(SSIPMessageBuilderInterface.DEFAULT_NAME)) {
			return (SSIPMessageBuilderInterface)springContext.getBean(SSIPMessageBuilderInterface.DEFAULT_NAME);
		} 
		return springContext.getBean(SSIPMessageBuilder.class); // not found, try with concrete class
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.ToSSIPSenderServiceMBean#
	 * reloadConfiguration()
	 */
	public void reloadConfiguration() {
		if (ssipMBuilder != null) 
			ssipMBuilder.loadQueryParams();
	}

	/* (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.ToSSIPSenderServiceMBean#setConfigPath(java.lang.String)
	 */
	@Override
	public void setConfigPath(String configPath) {
		this.configPath = configPath;
	}

	/**
	 * @return the configPath
	 */
	public String getConfigPath() {
		return configPath;
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.ToSSIPSenderServiceMBean#
	 * setMinutesToRecovery(java.lang.Integer)
	 */
	@Override
	public void setMinutesToRecovery(Integer minValue) {
		if (ssipManager != null) 
			ssipManager.setMinutesToRecoveryMinValue(minValue);
	}

	@Override
	public Integer getMinutesToRecovery() {
		if (ssipManager != null) 
			return ssipManager.getMinutesToRecoveryMinValue();
		return -1;
	}
	
	/* (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.ToSSIPSenderServiceMBean#getNumberOfRuns()
	 */
	@Override
	public String getNumberOfRuns() {
		String result = "<counters not accessible>";
		if (ssipMBuilder != null) {
			result = "";
			for (long nr : ssipMBuilder.getNumberOfRuns())
				result += nr + " "; 
		}
		return result; 
	}

	/* (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.ToSSIPSenderServiceMBean#sendTestMessage()
	 */
	@Override
	public void sendTestMessage() {
		if (ssipMBuilder != null) 
			ssipMBuilder.sendTestMessage();
	}
	
	/* (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.ToSSIPSenderServiceMBean#getLastStartDate()
	 */
	@Override
	public String getLastStartDate() {
		return sdf.format(lastStartDate);
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.ToSSIPSenderServiceMBean#
	 * setMinutesToRestart(java.lang.Integer)
	 */
	@Override
	public void setMinutesToRestart(Integer minValue) {
		if (ssipManager != null) 
			ssipManager.setMinutesToRestartMinValue(minValue);
	}

	@Override
	public Integer getMinutesToRestart() {
		if (ssipManager != null) 
			return ssipManager.getMinutesToRestartMinValue();
		return -1;
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.ToSSIPSenderServiceMBean#setSpringConfigPath(java.lang.String)
	 */
	@Override
	public void setSpringConfigPath(String path) {
		springConfigPath = path;
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.ToSSIPSenderServiceMBean#getSpringConfigPath()
	 */
	@Override
	public String getSpringConfigPath() {
		return springConfigPath;
	}


	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.ToSSIPSenderServiceMBean#isSpringLoaded()
	 */
	@Override
	public boolean isSpringLoaded() {
		return (springContext != null) ? springContext.isActive() : false;
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.ToSSIPSenderServiceMBean#isStarted()
	 */
	@Override
	public boolean isStarted() {
		return started;
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.ToSSIPSenderServiceMBean#getErrorCount()
	 */
	@Override
	public long getErrorCount() {
		return errorCount;
	}

}
