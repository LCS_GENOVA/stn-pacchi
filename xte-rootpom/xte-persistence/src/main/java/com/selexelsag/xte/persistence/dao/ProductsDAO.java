package com.selexelsag.xte.persistence.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.selexelsag.xte.persistence.entities.Products;
import com.selexelsag.xte.persistence.exceptions.PersistenceException;

/**
 * Home object for domain model class Events.
 * @see com.selexelsag.xte.persistence.entities.Events
 * @author Hibernate Tools
 */
public class ProductsDAO extends BaseDAO<Products>
{
	private static final Log log = LogFactory.getLog(ProductsDAO.class);

	public ProductsDAO() 
	{
		super(Products.class);
	}
	
	public Boolean findByPType(String pType) throws PersistenceException 
	{
		log.debug("getting instance with ptype code: " + pType);
		try 
		{
			Criteria criteria = sessionFactory.getCurrentSession()
					.createCriteria(Products.class.getName());
			criteria.add(Restrictions.eq("ptype", pType));
			List<Products> instanceList;
				instanceList = (List<Products>)findByCriteria(criteria);
			if (instanceList.size()==0) 
			{
				log.error("No value find for perifCode " + pType + ", update the data table!");
				return null;
			}
			else if (instanceList.size()>1) 
			{
				log.error("multiples value for perifCode " + pType + ", corrupted data table!");
			}
			
			return instanceList.get(0).getIsAPack().contains("S");
		} 
		catch (RuntimeException re) 
		{
			log.error("get failed", re);
			throw re;
		}
	}
}
