package com.selexelsag.xte.service.exceptions;

/**
 * Eccezione di base del progetto
 *
 */
public class BaseException extends Exception 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9214089572924527415L;

	public static final String BASE_ID = "BASE_ID";
	public static final String BUSINESS_ID = "BUSINESS_ID";
	public static final String BUSINESS_VALIDATION_ID = "BUSINESS_VALIDATION_ID";
	public static final String BUSINESS_FLOWMAPPER_ID = "BUSINESS_FLOWMAPPER_ID";
	public static final String BUSINESS_PERSISTENCE_ID = "BUSINESS_PERSISTENCE_ID";
	public static final String BUSINESS_SERVICEREQ_ID = "BUSINESS_SERVICEREQ_ID";
	public static final String BUSINESS_TRANSFORM_ID = "BUSINESS_TRANSFORM_ID";
	public static final String BUSINESS_MESSAGE_PROCESSOR_ID = "BUSINESS_MESSAGE_PROCESSOR_ID";
	public static final String SERVICE_ID = "SERVICE_ID";
	public static final String PERSISTENCE_ID = "PERSISTENCE_ID";
	public static final String BPM_ID = "BPM_ID";
	public static final String WEB_ID = "WEB_ID";

	public BaseException(String message, Throwable cause) 
	{
		super(message, cause);
		exceptionId = BASE_ID;
	}
	
	private String exceptionId;
	public String getExceptionId() {return exceptionId;}
	protected void setExceptionId(String exceptionId) {this.exceptionId = exceptionId;}
	

	public BaseException(Exception exception,String message, final boolean copyStack)
	{
		super(message + (copyStack ? ":\n" + stackToString(exception) : "" ));
	}

	public static String stackToString(Exception e)
	{
	      java.io.StringWriter sw= new java.io.StringWriter(1024); 
	      java.io.PrintWriter pw= new java.io.PrintWriter(sw); 
	      e.printStackTrace(pw);
	      pw.close();
	      return sw.toString();
    }
	
	public BaseException(String msg) 
	{
		super(msg);
	}
}
