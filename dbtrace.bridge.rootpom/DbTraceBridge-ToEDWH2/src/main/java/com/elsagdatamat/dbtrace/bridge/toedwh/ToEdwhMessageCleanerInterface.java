package com.elsagdatamat.dbtrace.bridge.toedwh;

import java.util.List;

import com.elsagdatamat.trackdb.toedwh.vo.EdwhTracesList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public interface ToEdwhMessageCleanerInterface {

	public abstract List<String> create(TracesList input);

	public abstract List<String> create(EdwhTracesList input);

	public abstract Integer deleteTraces() throws Exception;

	public abstract void fillSentTraces(Long l);


}