package com.elsagdatamat.tt.trackdbws.bl;

import java.util.Date;

import javax.xml.datatype.XMLGregorianCalendar;

import com.elsagdatamat.tt.trackdbws.beans.Seal;

public interface ISealBL {

	Seal getSealByNumberDate(String sealNumber, XMLGregorianCalendar xmlGregorianCalendar);

	void saveSeal(String sealNumber);

}