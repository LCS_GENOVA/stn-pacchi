package com.elsagdatamat.tt.trackdbws.daoimpl;

import java.util.Set;

import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.elsagdatamat.tt.trackdbws.beans.TraceDetail;
import com.elsagdatamat.tt.trackdbws.dao.ITraceDetailDAO;

public class TraceDetailDAO extends SpringHibernateJpaDAOBase<TraceDetail, Long> implements ITraceDetailDAO {
	@Override
	public void mergeTraceDetails(Trace source, Trace target, Boolean sourcePrevails)
	{
		log.debug("Entering mergeTraceDetails");
		Set<TraceDetail> sourceDetails=source.getInternalDetailList();
		Set<TraceDetail> targetDetails=target.getInternalDetailList();
		TraceDetail[] targetDetailsArray=new TraceDetail[targetDetails.size()];
		targetDetails.toArray(targetDetailsArray);
		for(TraceDetail td: sourceDetails)
		{
			log.debug("Merge di "+ td.getParamClass());
			String sourceClass=td.getParamClass();
			Boolean found=false;
			for(TraceDetail td1: targetDetailsArray)
			{
				if(td1.getParamClass().equals(sourceClass))
				{
					found=true;
					if(sourcePrevails)
					{
						td1.setParamValue(td.getParamValue());
						log.debug("Update "+ td.getParamClass()+" to "+td.getParamValue());
						update(td1);
					}
				}
			}
			if(!found)
			{
				TraceDetail newTd=new TraceDetail();
				newTd.setTrace(target);
				newTd.setParamClass(td.getParamClass());
				newTd.setParamValue(td.getParamValue());
				targetDetails.add(newTd);
				log.debug("Add "+ td.getParamClass()+" a "+td.getParamValue());
				insert(newTd);			
			}
		}
		log.debug("Leaving mergeTraceDetails");
	}
}
