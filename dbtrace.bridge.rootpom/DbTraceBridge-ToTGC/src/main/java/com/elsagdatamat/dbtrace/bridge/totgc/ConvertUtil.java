package com.elsagdatamat.dbtrace.bridge.totgc;

import java.util.List;

import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public class ConvertUtil {
	public static TracesList tracesToTraceList(List<Trace> traces) {
		TracesList ret = new TracesList();
		
		for (Trace trace:traces) {
//			com.selexelsag.tracktrace.trackdbws.xml.Trace t = new com.selexelsag.tracktrace.trackdbws.xml.Trace();
//			BeanUtils.copyProperties(trace, t);
//			
			ret.getTraces().add(trace);
		}

		return ret;
	}
}
