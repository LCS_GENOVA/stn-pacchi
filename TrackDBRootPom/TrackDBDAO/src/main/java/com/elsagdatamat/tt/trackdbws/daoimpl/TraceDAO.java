package com.elsagdatamat.tt.trackdbws.daoimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.framework.dao.hibernate.HibernatePrimitiveDAO;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.elsagdatamat.tt.trackdbws.beans.VO.EsiPackVO;
import com.elsagdatamat.tt.trackdbws.dao.ITraceDAO;

public class TraceDAO extends SpringHibernateJpaDAOBase<Trace, Long> implements ITraceDAO {

	class ParametricTraceDAO<T> {
		@SuppressWarnings("hiding")
		Log log = LogFactory.getLog(getClass());	
		
		@SuppressWarnings("unchecked")
		@Transactional(propagation=Propagation.SUPPORTS)
		public List<T> executeHqlQuery(String hqlQuery) { 
			
			class MyJpaCallback<T1> implements JpaCallback {
				@SuppressWarnings("hiding")
				String hqlQuery; 
				
				public MyJpaCallback(String hqlQuery){
					this.hqlQuery = hqlQuery;
				}
				
				@Override
				@Transactional(propagation=Propagation.SUPPORTS)
				public List<T1> doInJpa(EntityManager em) throws PersistenceException {
					return (List<T1>)HibernatePrimitiveDAO._executeHqlQuery(
																((HibernateEntityManager) em).getSession(),
																hqlQuery
																);
				}
			}
			
			MyJpaCallback<T> myJpaCallbak = new MyJpaCallback<T>(hqlQuery);
			List<T> listOfTObject = (List<T>) getJpaTemplate().execute(myJpaCallbak,true);
			
			return listOfTObject;
		}

	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Trace> returnTraceListExecuteHqlQuery(String hqlQuery) { 
		log.info("returnTraceListExecuteHqlQuery. Inizio");
		
		ParametricTraceDAO<Trace> traceTraceDAO = new ParametricTraceDAO<Trace>();
	
		log.info("returnTraceListExecuteHqlQuery. Fine");
		return traceTraceDAO.executeHqlQuery(hqlQuery);
	}


	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<EsiPackVO> getListOfEsiPackVO(String hqlQuery) { 
		log.info("getListOfEsiPackVO. Inizio");
		
		ParametricTraceDAO<EsiPackVO> traceTraceDAO = new ParametricTraceDAO<EsiPackVO>();
	
		log.info("getListOfEsiPackVO. Fine");
		return traceTraceDAO.executeHqlQuery(hqlQuery);
	}
	
	@Override
	public Trace merge(final Trace entity) {
		return (Trace) executeEntityManagerCallBack(new JpaCallback() {
			@Override
			public Object doInJpa(EntityManager em) throws PersistenceException {
				return em.merge(entity);
			}
		}, true);
	}

}
