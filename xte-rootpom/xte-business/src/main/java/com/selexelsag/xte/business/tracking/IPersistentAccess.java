package com.selexelsag.xte.business.tracking;

import java.util.HashMap;

import com.selexelsag.xte.business.exceptions.BusinessPersistentAccessException;
import com.selexelsag.xte.model.flows.FlowParams;

/**
 * @author Motta
 * classe per l'accesso al db su cui salvare le richieste asincrone
 * TODO: gestione delle eccezioni
 */
public interface IPersistentAccess {

	public static final String PERSISTENT_ACCESS_BEAN_NAME = "basePersistentAccess";
	
	/**
	 * @param flowParams parametri specifici al flusso
	 * @param headerData i parametri estratti dallo header.
	 * @param data i dati da salvare.
	 * @return l'oggetto contentente i dati in risposta dalla chiamata
	 */
	// TODO: implementa chiamata JMS
	void save(FlowParams flowParams, HashMap<String, String> headerData, StringBuilder data) throws BusinessPersistentAccessException;

	/** Recupera le informazioni di connessione dal db e riempe
	 * i relativi attributi di flowParams.
	 * Le query da utilizzare per il recupero dei parametri
	 * sono identificate da un nome e l'associazione tra il service
	 * e tale nome è gestita da un relativo file di properties.
	 * @param request la richiesta dalla quale estrarre  
	 * @param flowParams il container di parametri da aggiornare
	 * @throws BusinessPersistentAccessException nel caso in cui 
	 * un errore intercorra nel recupero delle informazioni o se 
	 * non esiste alcuna query associata al servizio.
	 */
	public void fillConnectionParameters(FlowParams flowParams) 
			throws BusinessPersistentAccessException;
	
}
