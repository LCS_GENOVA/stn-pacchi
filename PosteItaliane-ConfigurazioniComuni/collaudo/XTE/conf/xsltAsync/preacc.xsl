<?xml version="1.0"?>
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="no" indent="yes"/>
	<xsl:strip-space  elements="*"/>
	<xsl:template match="/">
		<MSG>
			<HDR SWREL="A0000" OFCID="00000">
			
			<xsl:choose>
				<xsl:when test="/MSG/HDR/CANALE">			
					<xsl:attribute name="CANALE">
						<xsl:value-of select="/MSG/HDR/CANALE"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			
			</HDR>
			<M3>
					<xsl:apply-templates/>
			</M3>
		</MSG>
	</xsl:template>

	<xsl:template match="OBJ">

	<OBJ>
		<xsl:apply-templates/>
			<PH>A</PH>
			<PSTF>0</PSTF>
			<TDT>
				<xsl:value-of select="concat(substring(/MSG/HDR/DSPED,9,2),'/',substring(/MSG/HDR/DSPED,6,2),'/',substring(/MSG/HDR/DSPED,1,4), ' ',substring(/MSG/HDR/DSPED, 12,8) )"/>
			</TDT>
			<OP>1</OP>
			<xsl:choose>
				<xsl:when test="CHIAVE_CLIENTE">
					<CHIAVE_CLIENTE>
					<xsl:value-of select="CHIAVE_CLIENTE"/>
					</CHIAVE_CLIENTE>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			
			<xsl:choose>
				<xsl:when test="TIPOLOGIA_CHIAVE">
					<TIPOLOGIA_CHIAVE>
					<xsl:value-of select="TIPOLOGIA_CHIAVE"/>
					</TIPOLOGIA_CHIAVE>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
			
			<xsl:choose>
				<xsl:when test="GUID">
					<GUID>
					<xsl:value-of select="GUID"/>
					</GUID>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>

			<xsl:choose>
				<xsl:when test="SERVIZIOGUID">
					<SERVIZIOGUID>
					<xsl:value-of select="SERVIZIOGUID"/>
					</SERVIZIOGUID>
				</xsl:when>
			<xsl:otherwise/>
			</xsl:choose>
			
			<xsl:choose>
				<xsl:when test="ADDR">
					<ADDR>
					<xsl:value-of select="ADDR"/>
					</ADDR>
				</xsl:when>
			<xsl:otherwise/>
			</xsl:choose>
			
			<xsl:choose>
				<xsl:when test="DADDR">
					<DADDR>
					<xsl:value-of select="DADDR"/>
					</DADDR>
				</xsl:when>
			<xsl:otherwise/>
			</xsl:choose>

			<xsl:choose>
				<xsl:when test="ZIP">
					<ZIP>
					<xsl:value-of select="ZIP"/>
					</ZIP>
				</xsl:when>
			<xsl:otherwise/>
			</xsl:choose>

			<xsl:choose>
				<xsl:when test="DEST">
					<DEST>
					<xsl:value-of select="DEST"/>
					</DEST>
				</xsl:when>
			<xsl:otherwise/>
			</xsl:choose>
		</OBJ>

		</xsl:template>

		<xsl:template match="OBJID|SUBC">
		<xsl:element name="{name()}">
			<xsl:value-of select="."/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="WA|NSEALS|NCHECK|EXPR|HDR|DEST|ADDR|ZIP|DADDR|SA|PARAM|
	CANALE|CHIAVE_CLIENTE|TIPOLOGIA_CHIAVE|GUID|PROVDEST|TELDEST|
	EMAILDEST|MDEST|MADDR|MDADDR|MITTCAP|PROVMITT|INSVAL|WB|CODVAL|CODEAR|SERVIZIOGUID|GUID"/>
</xsl:transform>


