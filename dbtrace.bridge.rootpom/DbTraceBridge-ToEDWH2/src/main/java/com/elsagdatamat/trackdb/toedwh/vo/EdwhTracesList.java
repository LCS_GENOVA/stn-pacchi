package com.elsagdatamat.trackdb.toedwh.vo;

import java.util.ArrayList;
import java.util.List;

import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public class EdwhTracesList extends TracesList {

	private List<TraceVO> traceList;

	public List<TraceVO> getTraceList() {
		return traceList;
	}

	public void setTraceList(List<TraceVO> traceList) {
		this.traceList = traceList;
	}

	public EdwhTracesList(List<TraceVO> traces) {
		super();
		this.traceList = traces;
		// devo mettere qualcosa in traces perch� WsToJmsBridge controlla...
		if(traces!=null && traces.size()>0){
			List<Trace> list = new ArrayList<Trace>();
			Trace dummyTrace = new Trace();
			list.add(dummyTrace);
			this.traces = list;
		}
	}


}
