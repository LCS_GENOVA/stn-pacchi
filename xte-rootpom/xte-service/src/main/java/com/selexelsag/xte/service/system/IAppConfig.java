package com.selexelsag.xte.service.system;

import java.util.Properties;

import com.selexelsag.xte.service.exceptions.ServiceException;

/**
 * Interfaccia configurazine generale dell'applicazione
 * @author Frosi
 *
 */
public interface IAppConfig {

	/**
	 * Inizializza la configurazione caricando tutte le properties dell'applicazione
	 */
	public void init() throws ServiceException;
	
	
	/**
	 * restituisce un parametro di configurazione generale, se non esiste il parametro o la configurazione e' vuota restituisce null
	 * @param key
	 * @return
	 */
	public String getGeneralParam(String key);

	/**
	 * resituisce un parametro di configurazione riguardante jbpm, se non esiste il parametro o la configurazione e' vuota restituisce null
	 * @param key
	 * @return
	 */
	public String getBpmParam(String key);
	
	/**
	 * resituisce un parametro di configurazione riguardante il mapping url, se non esiste il parametro o la configurazione e' vuota restituisce null
	 * @param key
	 * @return
	 */
	public String getUrlMapParam(String key);
	
	/**
	 * resituisce un parametro di configurazione riguardante il message processor, se non esiste il parametro o la configurazione e' vuota restituisce null
	 * @param key
	 * @return
	 */
	public String getMessageProcessorParam(String key);
	
	/**
	 * resituisce l'oggetto properties riguardante il message processor, se la configurazione e' vuota restituisce null
	 * @param key
	 * @return
	 */
	public Properties getMessageProcessorProperties();
	
	/**
	 * resituisce un parametro di configurazione riguardante il message processor, se non esiste il parametro o la configurazione e' vuota restituisce null
	 * @param key
	 * @return
	 */
	public String getProbingParam(String key);
	
	/**
	 * resituisce l'oggetto properties riguardante il message processor, se la configurazione e' vuota restituisce null
	 * @param key
	 * @return
	 */
	public Properties getProbingProperties();
	
	/**
	 * Identificativo dell'applicazione
	 */
	public static final String APP_ID = "XTE";
	
	/**
	 * Chiave della url completa a cui risponde il servizio soap nella configurazione
	 */
	public static final String GENERAL_ENDPOINT_URL_KEY = "com.selexelsag.xte.soap.endpoint";
	
	/**
	 * Chiave della propieta' del path completo assoluto in cui si trova il file di configurazione per log4j per il webservice
	 */
	public static final String GENERAL_LOG_CONF_FILE_WS_KEY = "com.selexelsag.xte.service.log.ws.configuration";
	
	/**
	 * Chiave della propieta' del path completo assoluto in cui si trova il file di configurazione per log4j per il webservice
	 */
	public static final String GENERAL_LOG_CONF_FILE_SCHED_KEY = "com.selexelsag.xte.service.log.scheduler.configuration";
	
	
	/**
	 * Nome del file di configurazione generale dell'applicazione
	 */
	public static final String GENERAL_CONFIGURATION_FILENAME="xte-general-config.properties";
	
	/**
	 * Nome del file di configurazione dei message processors dell'applicazione
	 */
	public static final String MESSAGE_PROCESSOR_FILENAME="xte-message-processor.properties";
	
	/**
	 * Nome del file di configurazione del probing
	 */
	public static final String PROBING_FILENAME="xte-probing-config.properties";

	
	/**
	 * Nome del file di configurazione per jbpm dell'applicazione
	 */
	public static final String BPM_CONFIGURATION_FILENAME="xte-bpm-config.properties";

	/**
	 * Nome del file di configurazione per il mapping url dell'applicazione
	 */
	public static final String URLMAP_CONFIGURATION_FILENAME="xte-urlmap-config.properties";

	/**
	 * Nome di default del file di configurazione generale dell'applicazione
	 */
	public static final String DEFAULT_GENERAL_CONFIGURATION_FILENAME="xte-general-config-default.properties";
	
	/**
	 * Nome di default del file di configurazione per jbpm dell'applicazione
	 */
	public static final String DEFAULT_BPM_CONFIGURATION_FILENAME="xte-bpm-config-default.properties";

	/**
	 * Nome di default del file di configurazione per il mapping url dell'applicazione
	 */
	public static final String DEFAULT_URLMAP_CONFIGURATION_FILENAME="xte-urlmap-config-default.properties";

	/**
	 * Nome di default del file di configurazione dei message processors dell'applicazione
	 */
	public static final String DEFAULT_MESSAGE_PROCESSOR_FILENAME="xte-message-processor-default.properties";

	/**
	 * Nome di default del file di configurazione del probing
	 */
	public static final String DEFAULT_PROBING_FILENAME="xte-probing-config-default.properties";

	/**
	 * Chiave del path assoluto da cui partono le risorse per flusso sincrono
	 */
	public static final String BPM_SYNC_VALIDATION_BASE_PATH_KEY = "com.selexelsag.xte.bpm.TP.validation.basepath";
	public static final String BPM_SYNC_TRANSFORMATION_BASE_PATH_KEY = "com.selexelsag.xte.bpm.TP.transformation.basepath";

	/**
	 * Nome del bean che contiene la configurazione
	 */
	public static final String APP_CONFIG_BEAN_NAME = "appConfig";
	
	/**
	 * Chiave del path assoluto dove sono memorizzate le risorse bpmn
	 */
	public static final String PATH_JBPM_RESOURCES = "resourcesJBPM";
	
	/**
	 * Chiave del nome del file drl per la configurazione dei flussi
	 */
	public static final String GENERAL_FLOW_RESOLVER = "com.selexelsag.xte.business.flow_resolver";
	
	/**
	 * Chiave della lista dei flowId gestiti dal message processor
	 */
	public static final String MESSAGE_PROCESSOR_FLOW_RESOLVER = "com.selexelsag.xte.business.message_processor.managedFlows";

	/**
	 * Chiave del nome del file drl per il send a interoperabilita'
	 */
	public static final String INTEROP_RESOLVER = "com.selexelsag.xte.business.check_for_interop";

	/**
	 * Chiave della propieta' del path completo assoluto in cui si trova il file di configurazione per log4j per la servlet di interoperabilita'
	 */
	public static final String INTEROP_LOG_CONF_FILE_SCHED_KEY = "com.selexelsag.xte.service.log.ptl.servlet.configuration";
	
}
