/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.exception;

/**
 *
 * @author Da Procida
 */
public class BaseException extends RuntimeException {

    private final Group group;
    private final Severity severity;

    public BaseException(Group group, Severity severity, Throwable cause) {
        super(cause);
        this.group = group;
        this.severity = severity;
    }

    public BaseException(Group group, Severity severity, String message, Throwable cause) {
        super(message, cause);
        this.group = group;
        this.severity = severity;
    }

    public BaseException(Group group, Severity severity, String message) {
        super(message);
        this.group = group;
        this.severity = severity;
    }

    public BaseException(Group group, Severity severity) {
        this.group = group;
        this.severity = severity;
    }

    public Group getGroup() {
        return group;
    }

    public Severity getSeverity() {
        return severity;
    }
}
