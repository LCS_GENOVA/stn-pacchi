package com.selexelsag.xte.bpm.flows.testflow;

import static org.junit.Assert.*;

import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.junit.Test;

import com.selexelsag.xte.bpm.flows.ProcessManager;

public class TestBpmn {

	@Test
	public void test() {
		try {
			KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();

			kbuilder.add(ResourceFactory.newClassPathResource("main_process.bpmn"), ResourceType.BPMN2);
			kbuilder.add(ResourceFactory.newClassPathResource("sync.default.bpmn"), ResourceType.BPMN2);
			kbuilder.add(ResourceFactory.newClassPathResource("async.default.bpmn"), ResourceType.BPMN2);
			kbuilder.add(ResourceFactory.newClassPathResource("composizione_risposta.bpmn"), ResourceType.BPMN2);
			kbuilder.add(ResourceFactory.newClassPathResource("inoltro.bpmn"), ResourceType.BPMN2);
			kbuilder.add(ResourceFactory.newClassPathResource("salvataggio.bpmn"), ResourceType.BPMN2);
			kbuilder.add(ResourceFactory.newClassPathResource("trasformazione.bpmn"), ResourceType.BPMN2);
			kbuilder.add(ResourceFactory.newClassPathResource("validazione.bpmn"), ResourceType.BPMN2);
			kbuilder.add(ResourceFactory.newClassPathResource("norm_cap.bpmn"), ResourceType.BPMN2);
			kbuilder.add(ResourceFactory.newClassPathResource("ProcessIdResolver.drl"), ResourceType.DRL);
			
			kbuilder.newKnowledgeBase();
		}catch (Throwable e) {
			fail("Error loading resource files: " + e);
		}
	}

}
