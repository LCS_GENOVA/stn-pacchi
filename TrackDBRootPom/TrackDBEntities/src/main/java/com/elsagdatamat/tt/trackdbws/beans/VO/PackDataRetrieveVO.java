package com.elsagdatamat.tt.trackdbws.beans.VO;

import com.selexelsag.tt.cruscottopacchi.bean.PkAnaDettPacchi;
import com.selexelsag.tt.cruscottopacchi.bean.PkConMappingDettTr;

public class PackDataRetrieveVO {

	private PkConMappingDettTr pkConMappingDettTr;
	private PkAnaDettPacchi pkAnaDettPacchi;
	
	public PackDataRetrieveVO( PkAnaDettPacchi pkAnaDettPacchi, PkConMappingDettTr pkConMappingDettTr) {
		super();
		this.pkConMappingDettTr = pkConMappingDettTr;
		this.pkAnaDettPacchi = pkAnaDettPacchi;
	}
	
	public PkConMappingDettTr getPkConMappingDettTr() {
		return pkConMappingDettTr;
	}
	
	public void setPkConMappingDettTr(PkConMappingDettTr pkConMappingDettTr) {
		this.pkConMappingDettTr = pkConMappingDettTr;
	}
	
	public PkAnaDettPacchi getPkAnaDettPacchi() {
		return pkAnaDettPacchi;
	}
	
	public void setPkAnaDettPacchi(PkAnaDettPacchi pkAnaDettPacchi) {
		this.pkAnaDettPacchi = pkAnaDettPacchi;
	}

}
