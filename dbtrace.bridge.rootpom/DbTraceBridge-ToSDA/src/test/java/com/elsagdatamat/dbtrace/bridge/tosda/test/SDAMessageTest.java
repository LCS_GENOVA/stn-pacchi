package com.elsagdatamat.dbtrace.bridge.tosda.test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.xml.namespace.QName;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elsagdatamat.dbtrace.bridge.engine.IMessageBuilder;
import com.elsagdatamat.dbtrace.bridge.engine.ITracesReader;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWService;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

@RunWith(SpringJUnit4ClassRunner.class)	
@ContextConfiguration(locations = {"classpath:/applicationContext-test.xml"})
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
//@Transactional(propagation=Propagation.NEVER)
public class SDAMessageTest {

	@Resource
	protected IMessageBuilder messageBuilder;
	
	@Resource
	protected ITracesReader tracesReader;

	@Resource
	protected ITracesReader tracesReaderApt;
	
	String wsUrl ="http://172.31.30.201/TrackDBWS/TrackDBRWService?wsdl";
	String wsNamespaceURI = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice";
	String wsLocalpart = "TrackDBRWService";
	
	@Test
	public void invokeToSDAEngine(){
		
		URL url = null;
		try {
			url = new URL(wsUrl); // "http://localhost:8080/pacchi-ws/pacchi.ws"
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}		
		
		QName name = new QName(wsNamespaceURI, wsLocalpart);
		TrackDBRWService tws = new TrackDBRWService(url, name);
		TrackDBRWServicePortType twsp = tws.getTrackDBRWServicePort();
		Object token;
		while (true) {
			Boolean someTracePresent = true;

			TracesList tracesList = null;
			try {
				long startDate = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
				token = tracesReader.getToken(twsp);
				tracesList = tracesReader.getTraces(twsp, new Date());

				if (tracesList != null && tracesList.getTraces() != null && tracesList.getTraces().size() != 0) {
					List<String> sList = null;

					startDate = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
					sList = messageBuilder.create(tracesList);
					startDate = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
				} else {
					someTracePresent = false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}

			// next token memorizzato in twsp
			if (someTracePresent == false)
				break;
		}
	}
	
	@Test
	public void invokeToSDA_APT(){
		
		URL url = null;
		try {
			url = new URL(wsUrl); // "http://localhost:8080/pacchi-ws/pacchi.ws"
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}		
		
		QName name = new QName(wsNamespaceURI, wsLocalpart);
		TrackDBRWService tws = new TrackDBRWService(url, name);
		TrackDBRWServicePortType twsp = tws.getTrackDBRWServicePort();
		Object token;
		while (true) {
			Boolean someTracePresent = true;

			TracesList tracesList = null;
			try {
				long startDate = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
				token = tracesReaderApt.getToken(twsp);
				tracesList = tracesReaderApt.getTraces(twsp, new Date());

				if (tracesList != null && tracesList.getTraces() != null && tracesList.getTraces().size() != 0) {
					List<String> sList = null;

					startDate = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
					sList = messageBuilder.create(tracesList);
					startDate = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
				} else {
					someTracePresent = false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}

			// next token memorizzato in twsp
			if (someTracePresent == false)
				break;
		}
	}
	
//	private XMLGregorianCalendar convertDate2XmlGregorianCalendar (Date myDate){
//		try{
//			GregorianCalendar c = new GregorianCalendar();
//			c.setTime(myDate);
//			XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
//			return date2;
//		} catch (DatatypeConfigurationException e) {
//			e.printStackTrace();
//		}
//		return null;
//		
//	}

	public IMessageBuilder getMessageBuilder() {
		return messageBuilder;
	}

	public void setMessageBuilder(IMessageBuilder messageBuilder) {
		this.messageBuilder = messageBuilder;
	}

	public ITracesReader getTracesReader() {
		return tracesReader;
	}

	public void setTracesReader(ITracesReader tracesReader) {
		this.tracesReader = tracesReader;
	}
	
	@Test
	public void testRun() {
		
	}
}
