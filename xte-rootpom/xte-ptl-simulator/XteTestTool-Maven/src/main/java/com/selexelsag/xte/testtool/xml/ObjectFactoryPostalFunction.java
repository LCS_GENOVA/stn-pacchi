package com.selexelsag.xte.testtool.xml;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactoryPostalFunction {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.selexelsag.xtetesttool.xml
     * 
     */
    public ObjectFactoryPostalFunction() {
    }

    /**
     * Create an instance of {@link Menu }
     * 
     */
    public Menu createMenu() {
        return new Menu();
    }

    /**
     * Create an instance of {@link Menu.Function }
     * 
     */
    public Function createMenuFunction() {
        return new Function();
    }

}
