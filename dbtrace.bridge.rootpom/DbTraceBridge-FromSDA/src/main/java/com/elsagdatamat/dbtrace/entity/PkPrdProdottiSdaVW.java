package com.elsagdatamat.dbtrace.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PK_PRD_PRODOTTI_SDA_VW")
public class PkPrdProdottiSdaVW implements Serializable {

	private static final long serialVersionUID = 6234831928024454303L;

	@Id
	@Column(name = "COD_PRODOTTO", nullable = false)
	private String codProdotto;

	@Column(name = "DESC_PRODOTTO", nullable = false)
	private String descProdotto;

	public String getCodProdotto() {
		return codProdotto;
	}

	public void setCodProdotto(String codProdotto) {
		this.codProdotto = codProdotto;
	}

	public String getDescProdotto() {
		return descProdotto;
	}

	public void setDescProdotto(String descProdotto) {
		this.descProdotto = descProdotto;
	}

	
}
