/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.manager;

import org.springframework.dao.CannotAcquireLockException;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ExtBaseManager;
import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.elsagdatamat.dbtrace.bridge.engine.entities.Token;
import com.selexelsag.dbtrace.bridge.to.omp.manager.token.OMPTokenDecorator;
import com.selexelsag.dbtrace.bridge.to.omp.manager.token.TokenDecorator;

/**
 * @author arodriguez
 *
 */
public abstract class OMPAbstractManager extends ExtBaseManager<ToToken, Token, String> {

	private final boolean BLOCKING = true;
	private final boolean NONBLOCKING = false;
	
	/**
	 * You know, "convertTtoZ" is an unhappy name for a Base-services class; isn't that ? 
	 * @param t the Token to convert from
	 * @return a decorator of ToToken, initialized with Token IDs
	 */
	@Override
	@Deprecated
	protected final ToToken convertTtoZ(Token t) {
		return new OMPTokenDecorator(new ToToken(), t);
	}

	/**
	 * You know, "convertZtoT" is an unhappy name for a Base-services class; isn't that ? 
	 * @param z the ToToken to convert from
	 * @return a decorator of Token, initialized with ToToken IDs
	 */
	@Override
	@Deprecated
	protected final Token convertZtoT(ToToken z) {
		return new TokenDecorator(new Token(), z);
	}

	/**
	 * a more understandable Token to ToToken converter
	 * @param fromToken the Token to convert from
	 * @return a decorator of ToToken, initialized with Token IDs
	 */
	protected final ToToken toToToken(Token fromToken) {
		return convertTtoZ(fromToken);		
	}
	
	/**
	 * a more understandable ToToken to Token converter
	 * @param fromToToken the ToToken to convert from
	 * @return a decorator of Token, initialized with ToToken IDs
	 */
	protected final Token toToken(ToToken fromToToken) {
		return convertZtoT(fromToToken);
	}
	
	/**
	 * @param serviceId
	 * @return
	 */
	protected Token tryTokenByServiceId(String serviceId) throws CannotAcquireLockException {
		Token token = dao.findById(serviceId, NONBLOCKING); 
		try {
			if (token != null)
				token = dao.findById(serviceId, BLOCKING);
		} catch (CannotAcquireLockException cannot) {
			log.error("Cannot acquire lock on token: '" + serviceId + "' token locked by another process");
			throw cannot;
		}
		return token;
	}

	/**
	 * 
	 * @param ssipToken
	 * @return
	 */
	protected Token retrieveCreateToken(ToToken ssipToken) {
		Token token = dao.findById(ssipToken.getServiceId(), NONBLOCKING);
		if (token == null) {
			token = new Token();
			token.setServiceId(ssipToken.getServiceId());
		}
		return token;
	}
	
}
 
	