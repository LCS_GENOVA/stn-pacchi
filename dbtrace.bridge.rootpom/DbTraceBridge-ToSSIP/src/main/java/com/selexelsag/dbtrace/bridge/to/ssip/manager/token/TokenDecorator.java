/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.ssip.manager.token;

import java.io.Serializable;
import java.util.Date;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.elsagdatamat.dbtrace.bridge.engine.entities.Token;

/**
 * @author arodriguez
 *
 */
public class TokenDecorator extends Token implements Serializable {

	private Token decoratedToken = null;
	
	/**
	 * hided default constructor
	 */
	private TokenDecorator() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param decoratedToken
	 */
	public TokenDecorator(Token decorator, ToToken ToToken) {
		this.decoratedToken = decorator;
		this.decoratedToken.setServiceId(ToToken.getServiceId());
		this.decoratedToken.setWsToken(ToToken.getWsToken());
	}

	/**
	 * @return
	 * @see com.elsagdatamat.dbtrace.bridge.engine.entities.Token#getServiceId()
	 */
	public String getServiceId() {
		return decoratedToken.getServiceId();
	}

	/**
	 * @param serviceId
	 * @see com.elsagdatamat.dbtrace.bridge.engine.entities.Token#setServiceId(java.lang.String)
	 */
	public void setServiceId(String serviceId) {
		decoratedToken.setServiceId(serviceId);
	}

	/**
	 * @return
	 * @see com.elsagdatamat.dbtrace.bridge.engine.entities.Token#getWsToken()
	 */
	public String getWsToken() {
		return decoratedToken.getWsToken();
	}

	/**
	 * @param wsToken
	 * @see com.elsagdatamat.dbtrace.bridge.engine.entities.Token#setWsToken(java.lang.String)
	 */
	public void setWsToken(String wsToken) {
		decoratedToken.setWsToken(wsToken);
	}

	/**
	 * @return
	 * @see com.elsagdatamat.dbtrace.bridge.engine.entities.Token#getAccessDate()
	 */
	public Date getAccessDate() {
		return decoratedToken.getAccessDate();
	}

	/**
	 * @param accessDate
	 * @see com.elsagdatamat.dbtrace.bridge.engine.entities.Token#setAccessDate(java.util.Date)
	 */
	public void setAccessDate(Date accessDate) {
		decoratedToken.setAccessDate(accessDate);
	}

	/**
	 * @return
	 * @see com.elsagdatamat.dbtrace.bridge.engine.entities.Token#getReleaseDate()
	 */
	public Date getReleaseDate() {
		return decoratedToken.getReleaseDate();
	}

	/**
	 * @param releaseDate
	 * @see com.elsagdatamat.dbtrace.bridge.engine.entities.Token#setReleaseDate(java.util.Date)
	 */
	public void setReleaseDate(Date releaseDate) {
		decoratedToken.setReleaseDate(releaseDate);
	}

}
