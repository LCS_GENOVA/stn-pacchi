package com.elsagdatamat.dbtrace.bridge.engine.bl;

public interface IToToken {

	String getWsToken();

	void setWsToken(String wsToken);

	String getServiceId();

	void setServiceId(String serviceId);

}