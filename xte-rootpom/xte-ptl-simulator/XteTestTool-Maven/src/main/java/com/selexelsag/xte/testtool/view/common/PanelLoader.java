/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.common;

import java.awt.Container;
import java.awt.Rectangle;
import javax.swing.JPanel;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class PanelLoader {

    private static PanelLoader panelLoader = null;
    private static Logger logger = Logger.getLogger(PanelLoader.class);

    private PanelLoader() {
    }

    public static PanelLoader getInstance() {
        logger.debug("PanelLoader invoked");
        return (panelLoader == null) ? panelLoader = new PanelLoader() : panelLoader;
    }

    public void changePanel(Container container, JPanel currentPanel, JPanel nextPanel, Rectangle rectangle)
    {
        logger.debug("PanelLoader start changePanel");

        if(currentPanel != null)
        {
            container.remove(currentPanel);
            container.validate();
            container.repaint();
        }
        currentPanel = nextPanel;
        container.add(nextPanel);     
        nextPanel.setBounds(rectangle);
        nextPanel.setVisible(true);
        container.validate();
        container.repaint();

        logger.debug("PanelLoader changePanel executed successfully");
    }

}
