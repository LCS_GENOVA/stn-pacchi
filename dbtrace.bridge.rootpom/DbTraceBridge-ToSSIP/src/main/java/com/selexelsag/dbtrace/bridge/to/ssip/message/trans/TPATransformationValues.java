/**
 *
 */
package com.selexelsag.dbtrace.bridge.to.ssip.message.trans;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Map;

/**
 * @author arodriguez
 *
 */
public enum TPATransformationValues implements Serializable {

	VARTR("VARPTR"), SPRD("SPRD"), NAZ("NAZ"),
	P("P"),	SA("SA"), CNTRS("CNTRS"), ASSIC("ASSIC"),
	IMPP("IMPP"), TAS("TAS"), PREPG_ID("PREPG_ID"),
	TRB("TRB"),	TRSA("TRSA"), DIRD("DIRD"),
	DESTZ("DESTZ"),	DESTR("DESTR"),	ADDRS("ADDRS"),
	ZIP("ZIP"),	DIST("DIST"), PROV("PROV"),
	LOCAL_ID("LOCAL_ID"), CONT_ID("CONT_ID"), FSC("FSC"),
	FRM("FRM"),	MRES_ID("MRES_ID"),	IRES("IRES"),
	CODICE_SAP("CODICE_SAP"), PARTITA_IVA("PARTITA_IVA"),
	CODICE_FISCALE("CODICE_FISCALE"), DESC_STATUS("DESC_STATUS"),
	DESC_CAUSALE("DESC_CAUSALE"), ID_CAUSALE("ID_CAUSALE"),
	NOTE("NOTE"), WHERE_ID("WHERE_ID"),	WHERE_DESCRIPTION("WHERE_DESCRIPTION");

	private String key = null;
	private String val = null;

	/**
	 * private enum constructor
	 * @param key  String key to match value
	 */
	private TPATransformationValues(String key) {
		this.key = key;
	}

	/**
	 * return enum instance filling-in values map
	 * @param sourceMap the Map values to set
	 * @return this TPATransformationValues object
	 */
	public TPATransformationValues map(Map<String, String> sourceMap) {
		if (sourceMap.containsKey(key))
			val = sourceMap.get(key);
		else  // reimposto altrimenti mantiene il valore vecchio
			val = null;
		return this;
	}

	/**
	 * value of specified enum key
	 * @return String value
	 */
	public String value() {
		return val;
	}

	/**
	 * int converted value of specified enum key
	 * @return BigInteger value if numeric, null otherwise
	 */
	public BigInteger intValue() {
		try {
			return new BigInteger(val);
		} catch(Exception nfe) {
		}
		return null;
	}
}
