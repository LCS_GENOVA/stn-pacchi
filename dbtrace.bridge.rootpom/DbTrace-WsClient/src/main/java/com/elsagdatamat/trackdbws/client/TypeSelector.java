
package com.elsagdatamat.trackdbws.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeSelector.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeSelector">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PosteItaliane"/>
 *     &lt;enumeration value="SDAOnly"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypeSelector")
@XmlEnum
public enum TypeSelector {

    @XmlEnumValue("PosteItaliane")
    POSTE_ITALIANE("PosteItaliane"),
    @XmlEnumValue("SDAOnly")
    SDA_ONLY("SDAOnly");
    private final String value;

    TypeSelector(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeSelector fromValue(String v) {
        for (TypeSelector c: TypeSelector.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
