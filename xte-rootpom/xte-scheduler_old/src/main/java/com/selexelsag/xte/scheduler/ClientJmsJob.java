package com.selexelsag.xte.scheduler;
 
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;
 
public class ClientJmsJob extends QuartzJobBean implements StatefulJob {
	private static Logger logger = Logger.getLogger(ClientJmsJob.class);
	
	private JmsTask jmsTask;
	

	public void setJmsTask(JmsTask jmsTask) {
		this.jmsTask = jmsTask;
	}


	
	protected void executeInternal(JobExecutionContext context)
	throws JobExecutionException {
		try {
			jmsTask.processAll();
		} catch (Exception e) {
			logger.error("Error processAll(): " + e.getMessage(), e);
		}
	}
}