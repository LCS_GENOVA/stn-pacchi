package com.selexes.tt.tool.recupero.messaggi;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.springframework.beans.factory.support.ChildBeanDefinition;

import com.selexes.tt.tool.utils.TraceUtils;

public class JDomParser {

	private SAXBuilder builder;
	private Document document;
	
	private Log log;
	
	public JDomParser(String schemaFile) {
		builder = new SAXBuilder();
		log = LogFactory.getLog(JDomParser.class);
	}

	private List<Element> getElements(StringReader fileXml, String elemChild) {
		List<Element> tagTraces = null;
		List<Element> xmlElements = null;
		try {
			document = (Document) builder.build(fileXml);
			Element rootNode = document.getRootElement();
			tagTraces = rootNode.getChildren("traces");
			xmlElements = tagTraces.get(0).getChildren(elemChild);
		} catch (IOException io) {
			log.error(io.getMessage(),io);
		} catch (JDOMException jdomex) {
			log.error(jdomex.getMessage(),jdomex);
		}
		return xmlElements;
	}
	
	
	public void parseNStore(StringReader fileXml, String elemChild, String nomefile, String nomefileScarti) throws IOException {
		List<Element> elements = getElements(fileXml, elemChild);
		for (int i=0; i<elements.size();i++){
			if(!validate(elements.get(i))){
				saveTrace(elements.get(i), nomefileScarti);
				elements.remove(i);
			}
		}
		XMLOutputter xmlOutput = new XMLOutputter();
		xmlOutput.setFormat(Format.getPrettyFormat());
		xmlOutput.output(document, new FileWriter(nomefile));
	}
	
	private void saveTrace(Element elem, String nomefileScarti) throws IOException{
		XMLOutputter xmlOutput = new XMLOutputter();
		// display nice nice
		xmlOutput.setFormat(Format.getPrettyFormat());
		xmlOutput.output(elem, new FileWriter(nomefileScarti,true));
	}
	
	private boolean validate(Element element){
		
		String prodotto = element.getChildText("LABEL_TRACED_ENTITY");
		Element whatHappenedDetailParent = element.getChild("WHAT_HAPPENED_DETAIL");
		
		if(!controlloProdotto(prodotto))
			if(!changeProdotto(element))
				return false;
		
		if(whatHappenedDetailParent.getChild("SMI_CMP")!=null && whatHappenedDetailParent.getChild("SMI_CMP").getChildText("ZIP_DIST")!=null) // solo su SMI_CMP - se il cap esiste e non � nullo controllo che sia di 5 numeri o 9 numeri
			return controlloZipDist(whatHappenedDetailParent.getChild("SMI_CMP").getChildText("ZIP_DIST"),element);
		
		if(whatHappenedDetailParent.getChild("ACC_CMP")!=null)
			return controlloSubP(whatHappenedDetailParent.getChild("ACC_CMP").getChildText("SUBP"),element);

		return true;
	}
	
	private boolean controlloProdotto(String prodotto){
		if(prodotto == null)
			return false;
		
		if(!TraceUtils.getProdottiValidi().contains(prodotto))
			return false;
		
		return true;
	}
	
	private boolean changeProdotto(Element element){
		String prodottoOriginale = element.getChildText("LABEL_TRACED_ENTITY");
		String idTracedEntity = element.getChildText("ID_TRACED_ENTITY");
		List<String> prodottiDaCodice = TraceUtils.getAllProductByCode(idTracedEntity);
		for (String prodotto : prodottiDaCodice) {
			try{
				element.getChild("LABEL_TRACED_ENTITY").setText(prodotto);	
			} catch (Exception e){
				Element childLabelTracedEntity = new Element("LABEL_TRACED_ENTITY");
				childLabelTracedEntity.setText(prodotto);
				element.addContent(5,childLabelTracedEntity);
//				return false;
			}
			if(controlloProdotto(element.getChildText("LABEL_TRACED_ENTITY"))){
				log.info("Sostituito il prodotto ["+prodottoOriginale+"] con ["+prodotto+"] per l'oggetto ["+idTracedEntity.toString()+"]");
				return true;
			}
		}
		
		log.error("Non � stato trovato un prodotto per l'oggetto ["+idTracedEntity+"]. Traccia inserita tra gli scartati.");
		return false;
	}
	
	private boolean controlloZipDist(String zipDist, Element element){
		String idTracedEntity = element.getChildText("ID_TRACED_ENTITY");
		
		String regex5 = "[0-9]{5}";
		String regex9 = "[0-9]{9}";
		Pattern pattern5 = Pattern.compile(regex5);
		Pattern pattern9 = Pattern.compile(regex9);
		if(!pattern5.matcher(zipDist).matches() && !pattern9.matcher(zipDist).matches()){
			log.error("ZIP_DIST errato ["+zipDist+"] per il pacco ["+idTracedEntity+"]. Traccia inserita tra gli scartati.");
			return false;
		}
		return true;
	}
	
	private boolean controlloSubP(String subP, Element element){
		String idTracedEntity = element.getChildText("ID_TRACED_ENTITY");
		String whatHappened = element.getChildText("WHAT_HAPPENED");
		
		if(subP==null){
			log.error("SUBP  non valorizzato per il pacco ["+idTracedEntity+"] e what happened ["+whatHappened+"]. Traccia inserita tra gli scartati.");
			return false;
		}
		return true;
	}
}
