package com.elsagdatamat.dbtrace.entity;

import java.io.Serializable;

import javax.persistence.Column;


public class PkConServiziAggiuntiviPk implements Serializable {

	private static final long serialVersionUID = 6234831928024454303L;

	@Column(name = "NOME_TAG", nullable = false)
	private String nomeTag;

	@Column(name = "VALORE_TAG", nullable = false)
	private String valoreTag;
	

	@Column(name = "SISTEMA_ESTERNO", nullable = false)
	private String sistemaEsterno;


	public String getNomeTag() {
		return nomeTag;
	}

	public void setNomeTag(String nomeTag) {
		this.nomeTag = nomeTag;
	}

	public String getValoreTag() {
		return valoreTag;
	}

	public void setValoreTag(String valoreTag) {
		this.valoreTag = valoreTag;
	}

	public String getSistemaEsterno() {
		return sistemaEsterno;
	}

	public void setSistemaEsterno(String sistemaEsterno) {
		this.sistemaEsterno = sistemaEsterno;
	}
	
	@Override
	public int hashCode() {
		return nomeTag.hashCode() + valoreTag.hashCode() + sistemaEsterno.hashCode();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		PkConServiziAggiuntiviPk other = (PkConServiziAggiuntiviPk) obj;
		
		if (nomeTag == null) {
			if (other.nomeTag != null)
				return false;
		} else if (!nomeTag.equals(other.nomeTag))
			return false;
		
		if (valoreTag == null) {
			if (other.valoreTag != null)
				return false;
		} else if (!valoreTag.equals(other.valoreTag))
			return false;
		
		if (sistemaEsterno == null) {
			if (other.sistemaEsterno != null)
				return false;
		} else if (!sistemaEsterno.equals(other.sistemaEsterno))
			return false;
		
		return true;
	}
	
	
	
}