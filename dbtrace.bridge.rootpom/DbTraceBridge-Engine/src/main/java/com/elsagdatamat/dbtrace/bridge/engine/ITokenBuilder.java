package com.elsagdatamat.dbtrace.bridge.engine;

import java.util.Date;

import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public interface ITokenBuilder<K, T> {

	Date getStopDate(Integer mode);

	TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate, T toToken);

	void acknowledge(TrackDBRWServicePortType twsp, K token, T toToken);

	void release(TrackDBRWServicePortType twsp, K token, T toToken);

	void initializeToken(TrackDBRWServicePortType twsp, K token, T toToken);

}
