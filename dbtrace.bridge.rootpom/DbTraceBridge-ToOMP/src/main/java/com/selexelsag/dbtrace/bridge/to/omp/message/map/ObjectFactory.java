//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.07.25 at 06:07:24 PM CEST 
//


package com.selexelsag.dbtrace.bridge.to.omp.message.map;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MSGD1MOBJREGCODEAR_QNAME = new QName("", "CODEAR");
    private final static QName _MSGD1MOBJREGSANEW_QNAME = new QName("", "SA_NEW");
    private final static QName _MSGD1MOBJREGINSVAL_QNAME = new QName("", "INSVAL");
    private final static QName _MSGD1MOBJREGDADDR_QNAME = new QName("", "DADDR");
    private final static QName _MSGD1MOBJREGREGT_QNAME = new QName("", "REGT");
    private final static QName _MSGD1MOBJREGCUSTCODE_QNAME = new QName("", "CUSTCODE");
    private final static QName _MSGD1MOBJREGNCHECK_QNAME = new QName("", "NCHECK");
    private final static QName _MSGD1MOBJREGCODVAL_QNAME = new QName("", "CODVAL");
    private final static QName _MSGD1MOBJREGWB_QNAME = new QName("", "WB");
    private final static QName _MSGD1MOBJREGWA_QNAME = new QName("", "WA");
    private final static QName _MSGD1MOBJREGNSEALS_QNAME = new QName("", "NSEALS");
    private final static QName _MSGD1MOBJREGADEST_QNAME = new QName("", "ADEST");
    private final static QName _MSGD1MOBJREGRCURR_QNAME = new QName("", "RCURR");
    private final static QName _MSGD1MOBJREGADDR_QNAME = new QName("", "ADDR");
    private final static QName _MSGD1MOBJREGSA_QNAME = new QName("", "SA");
    private final static QName _MSGD1MOBJOBJBTSF_QNAME = new QName("", "BTSF");
    private final static QName _MSGD1MOBJOBJSCANTIME_QNAME = new QName("", "SCANTIME");
    private final static QName _MSGD1MOBJOBJFAWAIT_QNAME = new QName("", "FAWAIT");
    private final static QName _MSGD1MOBJPACCONTOCORRENTEDEST_QNAME = new QName("", "CONTO_CORRENTE_DEST");
    private final static QName _MSGD1MOBJPACCODISTR_QNAME = new QName("", "COD_ISTR");
    private final static QName _MSGD1MOBJPACNUMMANCATACONSEGNA_QNAME = new QName("", "NUM_MANCATA_CONSEGNA");
    private final static QName _MSGD1MOBJPACIMPONERIDOGANALI_QNAME = new QName("", "IMP_ONERI_DOGANALI");
    private final static QName _MSGD1MOBJPACFLAGRRR_QNAME = new QName("", "FLAG_RRR");
    private final static QName _MSGD1MOBJPACNUMGIORNIGIACENZA_QNAME = new QName("", "NUM_GIORNI_GIACENZA");
    private final static QName _MSGD1MOBJPACIMPDIRITTIPOSTALI_QNAME = new QName("", "IMP_DIRITTI_POSTALI");
    private final static QName _MSGD1MOBJPACFLAGANN_QNAME = new QName("", "FLAG_ANN");
    private final static QName _MSGD1MOBJPACSTATUS_QNAME = new QName("", "STATUS");
    private final static QName _MSGD1MOBJPACFILIALESDA_QNAME = new QName("", "FILIALE_SDA");
    private final static QName _MSGD1MOBJACCACURR_QNAME = new QName("", "ACURR");
    private final static QName _MSGD1MOBJACCZIP_QNAME = new QName("", "ZIP");
    private final static QName _MSGD1MOBJACCFARE_QNAME = new QName("", "FARE");
    private final static QName _MSGD1MOBJACCPERC_QNAME = new QName("", "PERC");
    private final static QName _MSGD1DHDRCOURIER_QNAME = new QName("", "COURIER");
    private final static QName _MSGD1DHDRMC_QNAME = new QName("", "MC");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MSG.D1 .MOBJ.REG }
     * 
     */
    public MSG.D1 .MOBJ.REG createMSGD1MOBJREG() {
        return new MSG.D1 .MOBJ.REG();
    }

    /**
     * Create an instance of {@link MSG.D1 .MOBJ.OBJ }
     * 
     */
    public MSG.D1 .MOBJ.OBJ createMSGD1MOBJOBJ() {
        return new MSG.D1 .MOBJ.OBJ();
    }

    /**
     * Create an instance of {@link MSG }
     * 
     */
    public MSG createMSG() {
        return new MSG();
    }

    /**
     * Create an instance of {@link MSG.D1 .MOBJ.PAC }
     * 
     */
    public MSG.D1 .MOBJ.PAC createMSGD1MOBJPAC() {
        return new MSG.D1 .MOBJ.PAC();
    }

    /**
     * Create an instance of {@link MSG.D1 .MOBJ }
     * 
     */
    public MSG.D1 .MOBJ createMSGD1MOBJ() {
        return new MSG.D1 .MOBJ();
    }

    /**
     * Create an instance of {@link MSG.D1 }
     * 
     */
    public MSG.D1 createMSGD1() {
        return new MSG.D1();
    }

    /**
     * Create an instance of {@link MSG.HDR }
     * 
     */
    public MSG.HDR createMSGHDR() {
        return new MSG.HDR();
    }

    /**
     * Create an instance of {@link MSG.D1 .MOBJ.ACC }
     * 
     */
    public MSG.D1 .MOBJ.ACC createMSGD1MOBJACC() {
        return new MSG.D1 .MOBJ.ACC();
    }

    /**
     * Create an instance of {@link MSG.D1 .DHDR }
     * 
     */
    public MSG.D1 .DHDR createMSGD1DHDR() {
        return new MSG.D1 .DHDR();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CODEAR", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<String> createMSGD1MOBJREGCODEAR(String value) {
        return new JAXBElement<String>(_MSGD1MOBJREGCODEAR_QNAME, String.class, MSG.D1 .MOBJ.REG.class, value);
    }
    
    
    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SA_NEW", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<String> createMSGD1MOBJREGSANEW(String value) {
    	// L.C. 26/05/2015 R15.2 - Inizio
    	//value = "<![CDATA[<SERVIZI><"+value+"/></SERVIZI>]]>";
    	value = "<SERVIZI><"+value+"/></SERVIZI>";
    	// L.C. 26/05/2015 R15.2 - Fine
        return new JAXBElement<String>(_MSGD1MOBJREGSANEW_QNAME, String.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "INSVAL", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<BigInteger> createMSGD1MOBJREGINSVAL(BigInteger value) {
        return new JAXBElement<BigInteger>(_MSGD1MOBJREGINSVAL_QNAME, BigInteger.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DADDR", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<String> createMSGD1MOBJREGDADDR(String value) {
        return new JAXBElement<String>(_MSGD1MOBJREGDADDR_QNAME, String.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "REGT", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<String> createMSGD1MOBJREGREGT(String value) {
        return new JAXBElement<String>(_MSGD1MOBJREGREGT_QNAME, String.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CUSTCODE", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<String> createMSGD1MOBJREGCUSTCODE(String value) {
        return new JAXBElement<String>(_MSGD1MOBJREGCUSTCODE_QNAME, String.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NCHECK", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<BigInteger> createMSGD1MOBJREGNCHECK(BigInteger value) {
        return new JAXBElement<BigInteger>(_MSGD1MOBJREGNCHECK_QNAME, BigInteger.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CODVAL", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<BigInteger> createMSGD1MOBJREGCODVAL(BigInteger value) {
        return new JAXBElement<BigInteger>(_MSGD1MOBJREGCODVAL_QNAME, BigInteger.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "WB", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<BigInteger> createMSGD1MOBJREGWB(BigInteger value) {
        return new JAXBElement<BigInteger>(_MSGD1MOBJREGWB_QNAME, BigInteger.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "WA", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<BigInteger> createMSGD1MOBJREGWA(BigInteger value) {
        return new JAXBElement<BigInteger>(_MSGD1MOBJREGWA_QNAME, BigInteger.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NSEALS", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<BigInteger> createMSGD1MOBJREGNSEALS(BigInteger value) {
        return new JAXBElement<BigInteger>(_MSGD1MOBJREGNSEALS_QNAME, BigInteger.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ADEST", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<String> createMSGD1MOBJREGADEST(String value) {
        return new JAXBElement<String>(_MSGD1MOBJREGADEST_QNAME, String.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RCURR", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<String> createMSGD1MOBJREGRCURR(String value) {
        return new JAXBElement<String>(_MSGD1MOBJREGRCURR_QNAME, String.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ADDR", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<String> createMSGD1MOBJREGADDR(String value) {
        return new JAXBElement<String>(_MSGD1MOBJREGADDR_QNAME, String.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SA", scope = MSG.D1 .MOBJ.REG.class)
    public JAXBElement<String> createMSGD1MOBJREGSA(String value) {
        return new JAXBElement<String>(_MSGD1MOBJREGSA_QNAME, String.class, MSG.D1 .MOBJ.REG.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BTSF", scope = MSG.D1 .MOBJ.OBJ.class)
    public JAXBElement<String> createMSGD1MOBJOBJBTSF(String value) {
        return new JAXBElement<String>(_MSGD1MOBJOBJBTSF_QNAME, String.class, MSG.D1 .MOBJ.OBJ.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SCANTIME", scope = MSG.D1 .MOBJ.OBJ.class)
    public JAXBElement<String> createMSGD1MOBJOBJSCANTIME(String value) {
        return new JAXBElement<String>(_MSGD1MOBJOBJSCANTIME_QNAME, String.class, MSG.D1 .MOBJ.OBJ.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FAWAIT", scope = MSG.D1 .MOBJ.OBJ.class)
    public JAXBElement<String> createMSGD1MOBJOBJFAWAIT(String value) {
        return new JAXBElement<String>(_MSGD1MOBJOBJFAWAIT_QNAME, String.class, MSG.D1 .MOBJ.OBJ.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CONTO_CORRENTE_DEST", scope = MSG.D1 .MOBJ.PAC.class)
    public JAXBElement<String> createMSGD1MOBJPACCONTOCORRENTEDEST(String value) {
        return new JAXBElement<String>(_MSGD1MOBJPACCONTOCORRENTEDEST_QNAME, String.class, MSG.D1 .MOBJ.PAC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "COD_ISTR", scope = MSG.D1 .MOBJ.PAC.class)
    public JAXBElement<String> createMSGD1MOBJPACCODISTR(String value) {
        return new JAXBElement<String>(_MSGD1MOBJPACCODISTR_QNAME, String.class, MSG.D1 .MOBJ.PAC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NUM_MANCATA_CONSEGNA", scope = MSG.D1 .MOBJ.PAC.class)
    public JAXBElement<String> createMSGD1MOBJPACNUMMANCATACONSEGNA(String value) {
        return new JAXBElement<String>(_MSGD1MOBJPACNUMMANCATACONSEGNA_QNAME, String.class, MSG.D1 .MOBJ.PAC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IMP_ONERI_DOGANALI", scope = MSG.D1 .MOBJ.PAC.class)
    public JAXBElement<BigInteger> createMSGD1MOBJPACIMPONERIDOGANALI(BigInteger value) {
        return new JAXBElement<BigInteger>(_MSGD1MOBJPACIMPONERIDOGANALI_QNAME, BigInteger.class, MSG.D1 .MOBJ.PAC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FLAG_RRR", scope = MSG.D1 .MOBJ.PAC.class)
    public JAXBElement<String> createMSGD1MOBJPACFLAGRRR(String value) {
        return new JAXBElement<String>(_MSGD1MOBJPACFLAGRRR_QNAME, String.class, MSG.D1 .MOBJ.PAC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NUM_GIORNI_GIACENZA", scope = MSG.D1 .MOBJ.PAC.class)
    public JAXBElement<String> createMSGD1MOBJPACNUMGIORNIGIACENZA(String value) {
        return new JAXBElement<String>(_MSGD1MOBJPACNUMGIORNIGIACENZA_QNAME, String.class, MSG.D1 .MOBJ.PAC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IMP_DIRITTI_POSTALI", scope = MSG.D1 .MOBJ.PAC.class)
    public JAXBElement<BigInteger> createMSGD1MOBJPACIMPDIRITTIPOSTALI(BigInteger value) {
        return new JAXBElement<BigInteger>(_MSGD1MOBJPACIMPDIRITTIPOSTALI_QNAME, BigInteger.class, MSG.D1 .MOBJ.PAC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FLAG_ANN", scope = MSG.D1 .MOBJ.PAC.class)
    public JAXBElement<String> createMSGD1MOBJPACFLAGANN(String value) {
        return new JAXBElement<String>(_MSGD1MOBJPACFLAGANN_QNAME, String.class, MSG.D1 .MOBJ.PAC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "STATUS", scope = MSG.D1 .MOBJ.PAC.class)
    public JAXBElement<String> createMSGD1MOBJPACSTATUS(String value) {
        return new JAXBElement<String>(_MSGD1MOBJPACSTATUS_QNAME, String.class, MSG.D1 .MOBJ.PAC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FILIALE_SDA", scope = MSG.D1 .MOBJ.PAC.class)
    public JAXBElement<String> createMSGD1MOBJPACFILIALESDA(String value) {
        return new JAXBElement<String>(_MSGD1MOBJPACFILIALESDA_QNAME, String.class, MSG.D1 .MOBJ.PAC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ACURR", scope = MSG.D1 .MOBJ.ACC.class)
    public JAXBElement<String> createMSGD1MOBJACCACURR(String value) {
        return new JAXBElement<String>(_MSGD1MOBJACCACURR_QNAME, String.class, MSG.D1 .MOBJ.ACC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ZIP", scope = MSG.D1 .MOBJ.ACC.class)
    public JAXBElement<String> createMSGD1MOBJACCZIP(String value) {
        return new JAXBElement<String>(_MSGD1MOBJACCZIP_QNAME, String.class, MSG.D1 .MOBJ.ACC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FARE", scope = MSG.D1 .MOBJ.ACC.class)
    public JAXBElement<BigInteger> createMSGD1MOBJACCFARE(BigInteger value) {
        return new JAXBElement<BigInteger>(_MSGD1MOBJACCFARE_QNAME, BigInteger.class, MSG.D1 .MOBJ.ACC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PERC", scope = MSG.D1 .MOBJ.ACC.class)
    public JAXBElement<BigInteger> createMSGD1MOBJACCPERC(BigInteger value) {
        return new JAXBElement<BigInteger>(_MSGD1MOBJACCPERC_QNAME, BigInteger.class, MSG.D1 .MOBJ.ACC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "COURIER", scope = MSG.D1 .DHDR.class)
    public JAXBElement<String> createMSGD1DHDRCOURIER(String value) {
        return new JAXBElement<String>(_MSGD1DHDRCOURIER_QNAME, String.class, MSG.D1 .DHDR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MC", scope = MSG.D1 .DHDR.class)
    public JAXBElement<String> createMSGD1DHDRMC(String value) {
        return new JAXBElement<String>(_MSGD1DHDRMC_QNAME, String.class, MSG.D1 .DHDR.class, value);
    }

}
