package com.elsagdatamat.dbtrace.bridge.engine.bl;

public enum WhatHappenedOnDBTOnTT {
	//Tracce che vanno inserite su TT_APPLICATION.APT_ITEM e su DBT
	UFI_SDA;

	public static boolean contains(String value) {
	    for (WhatHappenedOnDBTOnTT c : WhatHappenedOnDBTOnTT.values()) {
	        if (c.name().equals(value)) {
	            return true;
	        }
	    }
	    return false;
	}
}
