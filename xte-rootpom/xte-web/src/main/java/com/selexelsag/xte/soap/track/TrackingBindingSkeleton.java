/**
 * TrackingBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.selexelsag.xte.soap.track;

import com.selexelsag.xte.model.track.soap.TrackResponse;

public class TrackingBindingSkeleton implements com.selexelsag.xte.soap.track.Tracking, org.apache.axis.wsdl.Skeleton {
    private com.selexelsag.xte.soap.track.Tracking impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "serviceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "channel"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "channelID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "eventName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "version"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "trackData"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("trackMessage", _params, new javax.xml.namespace.QName("", "returnTrackMessage"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://webservices.tt.elsagdatamat.com/", "trackResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://webservices.tt.elsagdatamat.com/", "trackMessage"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("trackMessage") == null) {
            _myOperations.put("trackMessage", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("trackMessage")).add(_oper);
    }

    public TrackingBindingSkeleton() {
        this.impl = new com.selexelsag.xte.soap.track.TrackingBindingImpl();
    }

    public TrackingBindingSkeleton(com.selexelsag.xte.soap.track.Tracking impl) {
        this.impl = impl;
    }
    public TrackResponse trackMessage(java.lang.String serviceName, java.lang.String channel, java.lang.String channelID, java.lang.String eventName, java.lang.String version, java.lang.String trackData) throws java.rmi.RemoteException
    {
        TrackResponse ret = impl.trackMessage(serviceName, channel, channelID, eventName, version, trackData);
        return ret;
    }

}
