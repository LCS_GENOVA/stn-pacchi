/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.dp;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.view.XteTestToolPanelView;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.PlainDocument;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author Da Procida
 */
public class XteTestToolAcquisizioneMazzettiDPView extends XteTestToolPanelView{
    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolAcquisizioneMazzettiDPView.class);
    
    public XteTestToolAcquisizioneMazzettiDPView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try {
            logger.info("XteTestToolAcquisizioneMazzettiDPView start class creation");
            this.messages = ResourceBundle.getBundle("bundles/messages");
            initComponents();
            logger.info("XteTestToolAcquisizioneMazzettiDPView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolAcquisizioneMazzettiDPView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Data Posta view creation failed", ex);
        }
    }

    public JPanel getAcquisizioneMazzettiPanelRef(){ return this; }        
    public JLabel getAcquisizioneMazzettiTitleLabelRef(){ return this.panelTitlejLabel; }
    
    public JPanel getBundleDataPaneRef(){ return this.bundleDatajPanel; }
    public JTextField getBundleIdTextRef(){ return this.bundleIdjTextField; }
    public JTextField getBundleTPIdTextRef(){ return this.bundleTPIdjTextField; }    
    public JXDatePicker getBundleDatePickerRef(){ return this.bundleDatejXDatePicker; }
    public JSpinner getBundleTimeSpinnerRef(){ return this.bundleTimejSpinner; }
    
    public JPanel getXmlDataPaneRef(){ return this.xmlDatajPanel; }
    public JScrollPane getXmlDataScrollPaneRef(){ return this.xmlDatajScrollPane; }
    public JTextArea getXmlDataTextAreaRef(){ return this.xmlDatajTextArea; }
    public JPanel getXmlDataCommandPaneRef(){ return this.xmlDataCommandjPanel; }    
    public JButton getGeneraXmlDataButtonRef(){ return this.generaXmlDatajButton; }
    public JButton getImpostaDatiWSButtonRef(){ return this.impostaDatiWSjButton; }
    public JButton getSalvaXmlDataButtonRef(){ return this.salvaXmlDatajButton; }
    
    public JButton getBackButtonRef(){ return this.backjButton; }
    public JButton getHomeButtonRef(){ return this.homejButton; }
    public JButton getExitButtonRef(){ return this.exitjButton; }
    public JTextPane getMessageBarTextPaneRef(){ return this.messageBarjTextPane; }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolAcquisizioneMazzettiDPView start customizeView");
            //CUSTOMIZE TITLE
            this.panelTitlejLabel .setText(messages.getString("dataposta.acquisizionemazzetti.view.title.label"));

            //CUSTOMIZE BUTTON
            this.generaXmlDatajButton.setText(messages.getString("dataposta.acquisizionemazzetti.view.generaxmldata.button"));
            this.impostaDatiWSjButton.setText(messages.getString("dataposta.acquisizionemazzetti.view.impostadatiws.button"));
            this.salvaXmlDatajButton.setText(messages.getString("dataposta.acquisizionemazzetti.view.salvaxmldata.button"));
            this.backjButton.setText(messages.getString("view.back.button"));
            this.homejButton.setText(messages.getString("view.home.button"));
            this.exitjButton.setText(messages.getString("view.exit.button"));

            logger.info("XteTestToolAcquisizioneMazzettiDPView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolAcquisizioneMazzettiDPView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Data Posta view customization failed", ex);
        }
    }

    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolAcquisizioneMazzettiDPView start initializeView");

//            buildComponentSortedMap();
//
//            this.setFocusCycleRoot(true);
//            this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

            defineTabPolicyForTextArea(xmlDatajTextArea);
            
            JLabel cornelLabel = new JLabel("");
            cornelLabel.setOpaque(true);
            cornelLabel.setBackground(new Color(240, 240, 240));

            this.xmlDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.xmlDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.xmlDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.xmlDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.xmlDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            JFormattedTextField mailPieceTimeJFormattedTextField = ((JSpinner.DateEditor)bundleTimejSpinner.getEditor()).getTextField();
            DefaultFormatter mailPieceTimeFormatter = (DefaultFormatter) mailPieceTimeJFormattedTextField.getFormatter();
            mailPieceTimeFormatter.setCommitsOnValidEdit(true);
            
            this.bundleDatejXDatePicker.getEditor().setDocument(new PlainDocument());            
            this.bundleDatejXDatePicker.getEditor().getDocument().putProperty("owner", bundleDatejXDatePicker.getEditor());                        
            ((JSpinner.DateEditor)bundleTimejSpinner.getEditor()).getTextField().setDocument(new PlainDocument());            
            ((JSpinner.DateEditor)bundleTimejSpinner.getEditor()).getTextField().getDocument().putProperty("owner", ((JSpinner.DateEditor)bundleTimejSpinner.getEditor()).getTextField());            
            
            logger.info("XteTestToolAcquisizioneMazzettiDPView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolAcquisizioneMazzettiDPView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Data Posta view initialization failed", ex);
        }
    }

    @Override
    public void restoreView() {
        logger.debug("XteTestToolAcquisizioneMazzettiDPView restoreView");
        this.getExitButtonRef().doClick();
    }

    private  static void defineTabPolicyForTextArea(final JTextArea textarea) {

        final Object tabActionKey = new Object();
        final Object shiftTabActionKey = new Object();
        KeyStroke tabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0, false);
        KeyStroke shiftTabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_DOWN_MASK, false);

        Action tabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusNextComponent((Component) source);
                }
            }
        };

        Action shiftTabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent((Component) source);
                }
            }
        };

        textarea.getActionMap().put(tabActionKey, tabAction);
        textarea.getInputMap().put(tabKeyStroke, tabActionKey);

        textarea.getActionMap().put(shiftTabActionKey, shiftTabAction);
        textarea.getInputMap().put(shiftTabKeyStroke, shiftTabActionKey);

    }
    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.bundleIdjTextField);
        componentSortedMap.put(2, this.bundleTPIdjTextField);
        componentSortedMap.put(3, this.bundleDatejXDatePicker.getEditor());
        componentSortedMap.put(4, ((JSpinner.DateEditor)this.bundleTimejSpinner.getEditor()).getTextField());
        componentSortedMap.put(5, this.xmlDatajTextArea);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        commandjPanel = new javax.swing.JPanel();
        messageBarjScrollPane = new javax.swing.JScrollPane();
        messageBarjTextPane = new javax.swing.JTextPane();
        homejButton = new javax.swing.JButton();
        exitjButton = new javax.swing.JButton();
        backjButton = new javax.swing.JButton();
        workingjPanel = new javax.swing.JPanel();
        panelTitlejLabel = new javax.swing.JLabel();
        xmlDatajPanel = new javax.swing.JPanel();
        xmlDatajScrollPane = new javax.swing.JScrollPane();
        xmlDatajTextArea = new javax.swing.JTextArea();
        xmlDataCommandjPanel = new javax.swing.JPanel();
        generaXmlDatajButton = new javax.swing.JButton();
        impostaDatiWSjButton = new javax.swing.JButton();
        salvaXmlDatajButton = new javax.swing.JButton();
        bundleDatajPanel = new javax.swing.JPanel();
        bundleTPIdjLabel = new javax.swing.JLabel();
        bundleTPIdjTextField = new javax.swing.JTextField();
        bundleIdjTextField = new javax.swing.JTextField();
        bundleIdjLabel = new javax.swing.JLabel();
        bundleDateTimejLabel = new javax.swing.JLabel();
        bundleDatejXDatePicker = new org.jdesktop.swingx.JXDatePicker();
        bundleTimejSpinner = new javax.swing.JSpinner();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setMaximumSize(new java.awt.Dimension(1000, 690));
        setMinimumSize(new java.awt.Dimension(1000, 690));
        setPreferredSize(new java.awt.Dimension(1000, 690));

        commandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        commandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        commandjPanel.setMaximumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setMinimumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setPreferredSize(new java.awt.Dimension(980, 80));

        messageBarjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageBarjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        messageBarjScrollPane.setMaximumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setMinimumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setPreferredSize(new java.awt.Dimension(350, 50));

        messageBarjTextPane.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        messageBarjTextPane.setFocusable(false);
        messageBarjScrollPane.setViewportView(messageBarjTextPane);

        homejButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        homejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Home3.png"))); // NOI18N
        homejButton.setText("<html><body>&nbsp;Home<body><html>");
        homejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        homejButton.setFocusPainted(false);
        homejButton.setFocusable(false);
        homejButton.setForeground(new java.awt.Color(51, 51, 51));
        homejButton.setIconTextGap(5);
        homejButton.setMaximumSize(new java.awt.Dimension(100, 50));
        homejButton.setMinimumSize(new java.awt.Dimension(100, 50));
        homejButton.setPreferredSize(new java.awt.Dimension(100, 50));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("<html><body>&nbsp;Exit<body><html>");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setFocusPainted(false);
        exitjButton.setFocusable(false);
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIconTextGap(5);
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        backjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        backjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/BackToMenu.png"))); // NOI18N
        backjButton.setText("<html><body>&nbsp;Back<body><html>");
        backjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        backjButton.setFocusPainted(false);
        backjButton.setFocusable(false);
        backjButton.setForeground(new java.awt.Color(51, 51, 51));
        backjButton.setIconTextGap(5);
        backjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        backjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        backjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout commandjPanelLayout = new javax.swing.GroupLayout(commandjPanel);
        commandjPanel.setLayout(commandjPanelLayout);
        commandjPanelLayout.setHorizontalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 272, Short.MAX_VALUE)
                .addComponent(backjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandjPanelLayout.setVerticalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandjPanelLayout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homejButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exitjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        workingjPanel.setBackground(new java.awt.Color(230, 250, 0));
        workingjPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20)));
        workingjPanel.setMaximumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setMinimumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setPreferredSize(new java.awt.Dimension(980, 580));

        panelTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        panelTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelTitlejLabel.setText("Acquisizione Mazzetti");
        panelTitlejLabel.setBackground(new java.awt.Color(240, 240, 240));
        panelTitlejLabel.setFocusable(false);
        panelTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        panelTitlejLabel.setIconTextGap(30);
        panelTitlejLabel.setMaximumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setMinimumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setPreferredSize(new java.awt.Dimension(920, 20));

        xmlDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "XML Data", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        xmlDatajPanel.setFocusable(false);
        xmlDatajPanel.setMaximumSize(new java.awt.Dimension(860, 380));
        xmlDatajPanel.setMinimumSize(new java.awt.Dimension(860, 380));
        xmlDatajPanel.setPreferredSize(new java.awt.Dimension(860, 380));
        xmlDatajPanel.setOpaque(false);

        xmlDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        xmlDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        xmlDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 325));
        xmlDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 325));
        xmlDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 325));

        xmlDatajTextArea.setColumns(20);
        xmlDatajTextArea.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        xmlDatajTextArea.setForeground(new java.awt.Color(51, 51, 51));
        xmlDatajTextArea.setLineWrap(true);
        xmlDatajTextArea.setRows(5);
        xmlDatajTextArea.setWrapStyleWord(true);
        xmlDatajTextArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setViewportView(xmlDatajTextArea);

        xmlDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        xmlDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        xmlDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        generaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        generaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml6.png"))); // NOI18N
        generaXmlDatajButton.setText("<html><body>&nbsp;Genera<br>XML Data<body><html>");
        generaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        generaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        generaXmlDatajButton.setFocusPainted(false);
        generaXmlDatajButton.setFocusable(false);
        generaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        generaXmlDatajButton.setIconTextGap(2);
        generaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        xmlDataCommandjPanel.add(generaXmlDatajButton, gridBagConstraints);

        impostaDatiWSjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        impostaDatiWSjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/WebService5.png.png"))); // NOI18N
        impostaDatiWSjButton.setText("<html><body>&nbsp;Imposta<br>&nbsp;Dati WS<body><html>");
        impostaDatiWSjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        impostaDatiWSjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        impostaDatiWSjButton.setFocusPainted(false);
        impostaDatiWSjButton.setFocusable(false);
        impostaDatiWSjButton.setForeground(new java.awt.Color(51, 51, 51));
        impostaDatiWSjButton.setIconTextGap(1);
        impostaDatiWSjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        xmlDataCommandjPanel.add(impostaDatiWSjButton, gridBagConstraints);

        salvaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        salvaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml7.png"))); // NOI18N
        salvaXmlDatajButton.setText("<html><body>&nbsp;Salva<br>XML Data<body><html>");
        salvaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        salvaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        salvaXmlDatajButton.setFocusPainted(false);
        salvaXmlDatajButton.setFocusable(false);
        salvaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        salvaXmlDatajButton.setIconTextGap(2);
        salvaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        xmlDataCommandjPanel.add(salvaXmlDatajButton, gridBagConstraints);

        javax.swing.GroupLayout xmlDatajPanelLayout = new javax.swing.GroupLayout(xmlDatajPanel);
        xmlDatajPanel.setLayout(xmlDatajPanelLayout);
        xmlDatajPanelLayout.setHorizontalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        xmlDatajPanelLayout.setVerticalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        bundleDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Informazioni Mazzetto", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        bundleDatajPanel.setMaximumSize(new java.awt.Dimension(860, 110));
        bundleDatajPanel.setMinimumSize(new java.awt.Dimension(860, 110));
        bundleDatajPanel.setPreferredSize(new java.awt.Dimension(860, 110));
        bundleDatajPanel.setOpaque(false);

        bundleTPIdjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleTPIdjLabel.setText("ID Operatore TP:");
        bundleTPIdjLabel.setFocusable(false);
        bundleTPIdjLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        bundleTPIdjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjTextField.setText("Mario Rossi");
        bundleTPIdjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        bundleTPIdjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        bundleTPIdjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        bundleTPIdjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        bundleIdjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleIdjTextField.setText("38999123456789012789");
        bundleIdjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        bundleIdjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        bundleIdjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        bundleIdjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        bundleIdjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleIdjLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleIdjLabel.setText("Codice Identificativo:");
        bundleIdjLabel.setFocusable(false);
        bundleIdjLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        bundleIdjLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        bundleIdjLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        bundleDateTimejLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleDateTimejLabel.setText("Data/Ora:");
        bundleDateTimejLabel.setFocusable(false);
        bundleDateTimejLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleDateTimejLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        bundleDateTimejLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        bundleDateTimejLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        bundleDatejXDatePicker.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleDatejXDatePicker.setBackground(new java.awt.Color(255, 255, 255));
        bundleDatejXDatePicker.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        bundleDatejXDatePicker.setMaximumSize(new java.awt.Dimension(150, 20));
        bundleDatejXDatePicker.setMinimumSize(new java.awt.Dimension(150, 20));
        bundleDatejXDatePicker.setOpaque(true);
        bundleDatejXDatePicker.setPreferredSize(new java.awt.Dimension(150, 20));

        bundleTimejSpinner.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        bundleTimejSpinner.setModel(new javax.swing.SpinnerDateModel());
        bundleTimejSpinner.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        bundleTimejSpinner.setEditor(new javax.swing.JSpinner.DateEditor(bundleTimejSpinner, "HH:mm:ss"));
        bundleTimejSpinner.setFocusable(false);
        bundleTimejSpinner.setMaximumSize(new java.awt.Dimension(80, 20));
        bundleTimejSpinner.setMinimumSize(new java.awt.Dimension(80, 20));
        bundleTimejSpinner.setPreferredSize(new java.awt.Dimension(80, 20));

        javax.swing.GroupLayout bundleDatajPanelLayout = new javax.swing.GroupLayout(bundleDatajPanel);
        bundleDatajPanel.setLayout(bundleDatajPanelLayout);
        bundleDatajPanelLayout.setHorizontalGroup(
            bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                        .addComponent(bundleIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bundleIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                        .addComponent(bundleTPIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bundleTPIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                        .addComponent(bundleDateTimejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bundleDatejXDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(bundleTimejSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        bundleDatajPanelLayout.setVerticalGroup(
            bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bundleTPIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bundleTPIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bundleIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bundleIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bundleDatejXDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bundleDateTimejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bundleTimejSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout workingjPanelLayout = new javax.swing.GroupLayout(workingjPanel);
        workingjPanel.setLayout(workingjPanelLayout);
        workingjPanelLayout.setHorizontalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workingjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(workingjPanelLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bundleDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        workingjPanelLayout.setVerticalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backjButton;
    private javax.swing.JPanel bundleDatajPanel;
    private javax.swing.JLabel bundleDateTimejLabel;
    private org.jdesktop.swingx.JXDatePicker bundleDatejXDatePicker;
    private javax.swing.JLabel bundleIdjLabel;
    private javax.swing.JTextField bundleIdjTextField;
    private javax.swing.JLabel bundleTPIdjLabel;
    private javax.swing.JTextField bundleTPIdjTextField;
    private javax.swing.JSpinner bundleTimejSpinner;
    private javax.swing.JPanel commandjPanel;
    private javax.swing.JButton exitjButton;
    private javax.swing.JButton generaXmlDatajButton;
    private javax.swing.JButton homejButton;
    private javax.swing.JButton impostaDatiWSjButton;
    private javax.swing.JScrollPane messageBarjScrollPane;
    private javax.swing.JTextPane messageBarjTextPane;
    private javax.swing.JLabel panelTitlejLabel;
    private javax.swing.JButton salvaXmlDatajButton;
    private javax.swing.JPanel workingjPanel;
    private javax.swing.JPanel xmlDataCommandjPanel;
    private javax.swing.JPanel xmlDatajPanel;
    private javax.swing.JScrollPane xmlDatajScrollPane;
    private javax.swing.JTextArea xmlDatajTextArea;
    // End of variables declaration//GEN-END:variables


}
