package com.elsagdatamat.dbtrace.bridge.tocdg.config;

public class SDACodes {
	private String webDesc;
	private String intDesc;
	private String cdgCode;
	public String getWebDesc() {
		return webDesc;
	}
	public void setWebDesc(String webDesc) {
		this.webDesc = webDesc;
	}
	public String getIntDesc() {
		return intDesc;
	}
	public void setIntDesc(String intDesc) {
		this.intDesc = intDesc;
	}
	public String getCdgCode() {
		return cdgCode;
	}
	public void setCdgCode(String cdgCode) {
		this.cdgCode = cdgCode;
	}
}
