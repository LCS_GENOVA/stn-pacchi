/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.dp;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XteMailPiece;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.dp.XteTestToolLasciaMazzettiDPView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;

import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;


import com.selexelsag.xte.testtool.view.dp.XteTestToolWebServiceDPView;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta.LM;
import com.selexelsag.xtetesttool.xml.dataposta.lm.ObjectFactory;
import com.selexelsag.xtetesttool.xml.dataposta.lm.Traccia;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolLasciaMazzettiDPController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener {

    private XteTestToolLasciaMazzettiDPView lasciaMazzettiDPView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolLasciaMazzettiDPController.class);
    private XteMailPiece selectedMailPiece = null;
    private int currentSelectedRow = -1;
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    private JAXBContext context;
    
    private static Traccia lasciaMazzettiTraccia;
    
    static {
        logger.debug("Creating Config Object Factory and Traccia");
        DataPosta dataPostaConfig = XteTestToolConfiguration.getInstance().getDataPostaConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        LM config = dataPostaConfig.getLM();

        lasciaMazzettiTraccia = objectFactory.createTraccia();
        lasciaMazzettiTraccia.setEvento(config.getEventName());
        lasciaMazzettiTraccia.setHeaderMazzetto(objectFactory.createHeaderMazzetto());
    }

    public XteTestToolLasciaMazzettiDPController(JPanel lasciaMazzettiDPView) {
        try {
            logger.info("XteTestToolLasciaMazzettiDPController start class creation");
            this.lasciaMazzettiDPView = (XteTestToolLasciaMazzettiDPView) lasciaMazzettiDPView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.lasciaMazzettiDPView.initializeView();

            updateView();

            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.dataposta.lm");

            logger.info("XteTestToolLasciaMazzettiDPController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolLasciaMazzettiDPController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolLasciaMazzettiDPController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Data Posta controller creation failed", ex);
        }
    }
    
    private void updateDataModel() {
        String id = lasciaMazzettiDPView.getBundleIdTextRef().getText();
        String opid = lasciaMazzettiDPView.getBundleTPIdTextRef().getText();
        String causale = lasciaMazzettiDPView.getBundleProductTextRef().getText();
        Date date = lasciaMazzettiDPView.getBundleDatePickerRef().getDate();
        Date time = (Date)lasciaMazzettiDPView.getBundleTimeSpinnerRef().getValue();
        
        logger.debug("updateDataModel - " + id + "," + opid + "," + causale + "," + date + "," + time);
        lasciaMazzettiTraccia.getHeaderMazzetto().setCodiceIdentificativo(id);
        lasciaMazzettiTraccia.getHeaderMazzetto().setIdOperatoreTP(opid);
        lasciaMazzettiTraccia.getHeaderMazzetto().setCausale(causale);
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);
        
        GregorianCalendar timeCalendar = new GregorianCalendar();
        timeCalendar.setTime(time);
        c.set(Calendar.HOUR_OF_DAY, timeCalendar.get(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));
        c.set(Calendar.SECOND, timeCalendar.get(Calendar.SECOND));
        c.set(Calendar.MILLISECOND, timeCalendar.get(Calendar.MILLISECOND));
        
        try {
            lasciaMazzettiTraccia.getHeaderMazzetto().setDataOraScansione(
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException ex) {
            logger.error("Error converting date " + c, ex);
            lasciaMazzettiTraccia.getHeaderMazzetto().setDataOraScansione(null);
        }
    }
    
    
    private void updateView() {  
        String id = lasciaMazzettiTraccia.getHeaderMazzetto().getCodiceIdentificativo();
        String opid = lasciaMazzettiTraccia.getHeaderMazzetto().getIdOperatoreTP();
        String causale = lasciaMazzettiTraccia.getHeaderMazzetto().getCausale();
        XMLGregorianCalendar date = lasciaMazzettiTraccia.getHeaderMazzetto().getDataOraScansione();
        logger.debug("updateView - " + id + "," + opid + "," + date);
                
        if (id != null) {
            lasciaMazzettiDPView.getBundleIdTextRef()
                    .setText(id);
        }

        if (opid != null) {
            lasciaMazzettiDPView.getBundleTPIdTextRef()
                    .setText(opid);
        }
    
        if (causale != null) {
            lasciaMazzettiDPView.getBundleProductTextRef()
                    .setText(causale);
        }
        
        if (date != null) {
            lasciaMazzettiDPView.getBundleDatePickerRef().setDate(
                date.toGregorianCalendar().getTime());
            lasciaMazzettiDPView.getBundleTimeSpinnerRef().setValue(
                date.toGregorianCalendar().getTime());
        }
    }
        
    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = lasciaMazzettiDPView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        lasciaMazzettiDPView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.lasciaMazzettiDPView.getLasciaMazzettiPanelRef().addAncestorListener(this);        
        this.lasciaMazzettiDPView.getBundleIdTextRef().addFocusListener(this);
        this.lasciaMazzettiDPView.getBundleTPIdTextRef().addFocusListener(this);
        this.lasciaMazzettiDPView.getBundleDatePickerRef().getEditor().addFocusListener(this);
        JFormattedTextField startTimejFormattedTextField = ((JSpinner.DateEditor)lasciaMazzettiDPView.getBundleTimeSpinnerRef().getEditor()).getTextField();
        startTimejFormattedTextField.addFocusListener(this);
        this.lasciaMazzettiDPView.getBundleProductTextRef().addFocusListener(this);
        this.lasciaMazzettiDPView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.lasciaMazzettiDPView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.lasciaMazzettiDPView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.lasciaMazzettiDPView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.lasciaMazzettiDPView.getBackButtonRef().addActionListener(this);
        this.lasciaMazzettiDPView.getHomeButtonRef().addActionListener(this);
        this.lasciaMazzettiDPView.getExitButtonRef().addActionListener(this);
                
        this.lasciaMazzettiDPView.getBundleDatePickerRef().getEditor().getDocument().addDocumentListener(this);
        ((JSpinner.DateEditor)lasciaMazzettiDPView.getBundleTimeSpinnerRef().getEditor()).getTextField().getDocument().addDocumentListener(this);

		this.salvaXmlFileChooser.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(lasciaMazzettiDPView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolLasciaMazzettiDPController actionPerformed invoke generaXmlData");

            updateDataModel();

            generaXmlData();
        }

        if(source.equals(lasciaMazzettiDPView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolLasciaMazzettiDPController actionPerformed invoke impostaDatiWebService");

            updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(lasciaMazzettiDPView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolLasciaMazzettiDPController actionPerformed invoke salvaXmlData");

            updateDataModel();

            salvaXmlData();
        }

        if(source.equals(lasciaMazzettiDPView.getBackButtonRef()))
        {
            logger.debug("XteTestToolLasciaMazzettiDPController actionPerformed invoke back");

            updateDataModel();

            back();
        }

        if(source.equals(lasciaMazzettiDPView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolLasciaMazzettiDPController actionPerformed invoke home");

            updateDataModel();

            home();
        }

        if(source.equals(lasciaMazzettiDPView.getExitButtonRef()))
        {
            logger.debug("XteTestToolLasciaMazzettiDPController actionPerformed invoke exit");

            updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(lasciaMazzettiDPView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolLasciaMazzettiDPView)
        {
            logger.debug("XteTestToolLasciaMazzettiDPController ancestorAdded invoke init");
            init();
            
            updateView();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiDPController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Data Posta controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiDPController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Data Posta controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }    
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        lasciaMazzettiDPView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolLasciaMazzettiDPController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.lasciaMazzettiDPView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(lasciaMazzettiDPView);
            updateXteTestToolDataModel(dataModel);

            clearMessageBar();

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(lasciaMazzettiDPView.getImpostaDatiWSButtonRef());
            disabledComponents.add(lasciaMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            Calendar date = Calendar.getInstance();
            this.lasciaMazzettiDPView.getBundleDatePickerRef().setDate(date.getTime());
            this.lasciaMazzettiDPView.getBundleTimeSpinnerRef().setValue(date.getTime());

            lasciaMazzettiDPView.getBundleIdTextRef().requestFocus();

            logger.info("XteTestToolLasciaMazzettiDPController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolLasciaMazzettiDPController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiDPController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Data Posta controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolLasciaMazzettiDPController start exit");
        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(lasciaMazzettiDPView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolLasciaMazzettiDPController exit in progress");
            lasciaMazzettiDPView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolLasciaMazzettiDPController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(lasciaMazzettiDPView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(lasciaMazzettiDPView.getLasciaMazzettiPanelRef().getParent(), lasciaMazzettiDPView.getLasciaMazzettiPanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolLasciaMazzettiDPController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(lasciaMazzettiDPView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(lasciaMazzettiDPView.getLasciaMazzettiPanelRef().getParent(), lasciaMazzettiDPView.getLasciaMazzettiPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }
    
    private void generaXmlData()
    {
        logger.info("XteTestToolLasciaMazzettiDPController start generaXmlData");

        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();
        
        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            lasciaMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(lasciaMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(lasciaMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            lasciaMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(lasciaMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(lasciaMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(lasciaMazzettiTraccia, out);
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            lasciaMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(lasciaMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(lasciaMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
        try
        {            
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString());
            lasciaMazzettiDPView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("XteTestToolLasciaMazzettiDPController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolLasciaMazzettiDPController generaXmlData failed", baseEx);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(baseEx.toString(), MessageType.ERROR);
            lasciaMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(lasciaMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(lasciaMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiDPController generaXmlData failed", ex);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            lasciaMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(lasciaMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(lasciaMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Data Posta controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        lasciaMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(lasciaMazzettiDPView.getImpostaDatiWSButtonRef());
        enableComponent.add(lasciaMazzettiDPView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolLasciaMazzettiDPController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServiceDPView(lasciaMazzettiDPView.getParentFrame());
            XteTestToolWebServiceDPController controller = new XteTestToolWebServiceDPController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            DataPosta dataPostaConfig = XteTestToolConfiguration.getInstance().getDataPostaConfig();
            LM config = dataPostaConfig.getLM();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(lasciaMazzettiDPView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(lasciaMazzettiDPView.getLasciaMazzettiPanelRef().getParent(), lasciaMazzettiDPView.getLasciaMazzettiPanelRef(), webserviceview, new Rectangle(0,70,1000,690));

            logger.info("XteTestToolLasciaMazzettiDPController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolLasciaMazzettiDPController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiDPController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Data Posta controller impostaDatiWebService failed", ex);
        }
    }

    
    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolLasciaMazzettiDPController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolLasciaMazzettiDPController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(lasciaMazzettiDPView.getParentFrame());

            logger.info("XteTestToolLasciaMazzettiDPController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolLasciaMazzettiDPController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiDPController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Data Posta controller salvaXmlData failed", ex);
        }
    }
}
