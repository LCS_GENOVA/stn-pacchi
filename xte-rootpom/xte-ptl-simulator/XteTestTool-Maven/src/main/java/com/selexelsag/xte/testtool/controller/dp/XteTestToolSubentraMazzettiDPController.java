/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.dp;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XteMailPiece;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;

import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;


import com.selexelsag.xte.testtool.view.dp.XteTestToolWebServiceDPView;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta.SM;
import com.selexelsag.xtetesttool.xml.dataposta.sm.ObjectFactory;
import com.selexelsag.xtetesttool.xml.dataposta.sm.Traccia;

import com.selexelsag.xte.testtool.view.dp.XteTestToolSubentraMazzettoDPView;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;

import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolSubentraMazzettiDPController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener {

    private XteTestToolSubentraMazzettoDPView subentraMazzettoDPView;
    private ResourceBundle messages;

    private static Logger logger = Logger.getLogger(XteTestToolSubentraMazzettiDPController.class);

    private XteMailPiece selectedMailPiece = null;
    private int currentSelectedRow = -1;
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    private JAXBContext context;
    
    private static Traccia subentraMazzettiTraccia;
    
    static {
        logger.debug("Creating Config Object Factory and Traccia");
        DataPosta dataPostaConfig = XteTestToolConfiguration.getInstance().getDataPostaConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        SM config = dataPostaConfig.getSM();

        subentraMazzettiTraccia = objectFactory.createTraccia();
        subentraMazzettiTraccia.setEvento(config.getEventName());
        subentraMazzettiTraccia.setHeaderMazzetto(objectFactory.createHeaderMazzetto());
    }

    public XteTestToolSubentraMazzettiDPController(JPanel subentraMazzettoDPView) {
        try {
            logger.info("XteTestToolSubentraMazzettoDPController start class creation");
            this.subentraMazzettoDPView = (XteTestToolSubentraMazzettoDPView) subentraMazzettoDPView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.subentraMazzettoDPView.initializeView();

            updateView();

            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.dataposta.sm");

            logger.info("XteTestToolSubentraMazzettoDPController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolSubentraMazzettoDPController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolSubentraMazzettoDPController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Subentra Mazzetto Data Posta controller creation failed", ex);
        }
    }
    
    private void updateDataModel() {        

        String id = subentraMazzettoDPView.getBundleIdTextRef().getText();
        String opid = subentraMazzettoDPView.getBundleTPIdTextRef().getText();
        String causale = subentraMazzettoDPView.getBundleProductTextRef().getText();
        Date date = subentraMazzettoDPView.getBundleDatePickerRef().getDate();
        Date time = (Date)subentraMazzettoDPView.getBundleTimeSpinnerRef().getValue();
        
        logger.debug("updateDataModel - " + id + "," + opid + "," + causale + "," + date + "," + time);
        subentraMazzettiTraccia.getHeaderMazzetto().setCodiceIdentificativo(id);
        subentraMazzettiTraccia.getHeaderMazzetto().setIdOperatoreTP(opid);
        subentraMazzettiTraccia.getHeaderMazzetto().setCausale(causale);
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);
        
        GregorianCalendar timeCalendar = new GregorianCalendar();
        timeCalendar.setTime(time);
        c.set(Calendar.HOUR_OF_DAY, timeCalendar.get(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));
        c.set(Calendar.SECOND, timeCalendar.get(Calendar.SECOND));
        c.set(Calendar.MILLISECOND, timeCalendar.get(Calendar.MILLISECOND));
        
        try {
            subentraMazzettiTraccia.getHeaderMazzetto().setDataOraScansione(
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException ex) {
            logger.error("Error converting date " + c, ex);
            subentraMazzettiTraccia.getHeaderMazzetto().setDataOraScansione(null);
        }
    }
    
    
    private void updateView() {  
        String id = subentraMazzettiTraccia.getHeaderMazzetto().getCodiceIdentificativo();
        String opid = subentraMazzettiTraccia.getHeaderMazzetto().getIdOperatoreTP();
        String causale = subentraMazzettiTraccia.getHeaderMazzetto().getCausale();
        XMLGregorianCalendar date = subentraMazzettiTraccia.getHeaderMazzetto().getDataOraScansione();
        logger.debug("updateView - " + id + "," + opid + "," + date);
                
        if (id != null) {
            subentraMazzettoDPView.getBundleIdTextRef()
                    .setText(id);
        }

        if (opid != null) {
            subentraMazzettoDPView.getBundleTPIdTextRef()
                    .setText(opid);
        }
    
        if (causale != null) {
            subentraMazzettoDPView.getBundleProductTextRef()
                    .setText(causale);
        }
        
        if (date != null) {
            subentraMazzettoDPView.getBundleDatePickerRef().setDate(
                date.toGregorianCalendar().getTime());
            subentraMazzettoDPView.getBundleTimeSpinnerRef().setValue(
                date.toGregorianCalendar().getTime());
        }
    }
        
    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = subentraMazzettoDPView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        subentraMazzettoDPView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.subentraMazzettoDPView.getSubentraMazzettiPanelRef().addAncestorListener(this);
        
        this.subentraMazzettoDPView.getBundleIdTextRef().addFocusListener(this);
        this.subentraMazzettoDPView.getBundleTPIdTextRef().addFocusListener(this);
        this.subentraMazzettoDPView.getBundleDatePickerRef().getEditor().addFocusListener(this);
        JFormattedTextField startTimejFormattedTextField = ((JSpinner.DateEditor)subentraMazzettoDPView.getBundleTimeSpinnerRef().getEditor()).getTextField();
        startTimejFormattedTextField.addFocusListener(this);
        this.subentraMazzettoDPView.getBundleProductTextRef().addFocusListener(this);

        this.subentraMazzettoDPView.getXmlDataTextAreaRef().addFocusListener(this);
        this.subentraMazzettoDPView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.subentraMazzettoDPView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.subentraMazzettoDPView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.subentraMazzettoDPView.getBackButtonRef().addActionListener(this);
        this.subentraMazzettoDPView.getHomeButtonRef().addActionListener(this);
        this.subentraMazzettoDPView.getExitButtonRef().addActionListener(this);

        this.subentraMazzettoDPView.getBundleDatePickerRef().getEditor().getDocument().addDocumentListener(this);
        ((JSpinner.DateEditor)subentraMazzettoDPView.getBundleTimeSpinnerRef().getEditor()).getTextField().getDocument().addDocumentListener(this);

		this.salvaXmlFileChooser.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(subentraMazzettoDPView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolSubentraMazzettoDPController actionPerformed invoke generaXmlData");

            updateDataModel();

            generaXmlData();
        }

        if(source.equals(subentraMazzettoDPView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolSubentraMazzettoDPController actionPerformed invoke impostaDatiWebService");

            updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(subentraMazzettoDPView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolSubentraMazzettoDPController actionPerformed invoke salvaXmlData");

            updateDataModel();

            salvaXmlData();
        }

        if(source.equals(subentraMazzettoDPView.getBackButtonRef()))
        {
            logger.debug("XteTestToolSubentraMazzettoDPController actionPerformed invoke back");

            updateDataModel();

            back();
        }

        if(source.equals(subentraMazzettoDPView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolSubentraMazzettoDPController actionPerformed invoke home");

            updateDataModel();

            home();
        }

        if(source.equals(subentraMazzettoDPView.getExitButtonRef()))
        {
            logger.debug("XteTestToolSubentraMazzettoDPController actionPerformed invoke exit");

            updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(subentraMazzettoDPView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolSubentraMazzettoDPView)
        {
            logger.debug("XteTestToolSubentraMazzettoDPController ancestorAdded invoke init");
            init();
            
            updateView();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiDPController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Data Posta controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiDPController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Data Posta controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }    
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        subentraMazzettoDPView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolSubentraMazzettoDPController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.subentraMazzettoDPView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(subentraMazzettoDPView);
            updateXteTestToolDataModel(dataModel);

            clearMessageBar();

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(subentraMazzettoDPView.getImpostaDatiWSButtonRef());
            disabledComponents.add(subentraMazzettoDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            Calendar date = Calendar.getInstance();
            this.subentraMazzettoDPView.getBundleDatePickerRef().setDate(date.getTime());
            this.subentraMazzettoDPView.getBundleTimeSpinnerRef().setValue(date.getTime());

            subentraMazzettoDPView.getBundleIdTextRef().requestFocus();

            logger.info("XteTestToolLasciaMazzettiDPController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolSubentraMazzettoDPController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolSubentraMazzettoDPController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Subentra Mazzetto Data Posta controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolSubentraMazzettoDPController start exit");

        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(subentraMazzettoDPView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolSubentraMazzettoDPController exit in progress");
            subentraMazzettoDPView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolAcquisizioneMazzettiPRController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(subentraMazzettoDPView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(subentraMazzettoDPView.getSubentraMazzettiPanelRef().getParent(), subentraMazzettoDPView.getSubentraMazzettiPanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolSubentraMazzettoDPController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();

        XteTestToolHomeView homeView = new XteTestToolHomeView(subentraMazzettoDPView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(subentraMazzettoDPView.getSubentraMazzettiPanelRef().getParent(), subentraMazzettoDPView.getSubentraMazzettiPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }
    
    private void generaXmlData()
    {
        logger.info("XteTestToolSubentraMazzettoDPController start generaXmlData");

        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();
        
        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            subentraMazzettoDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(subentraMazzettoDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(subentraMazzettoDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            subentraMazzettoDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(subentraMazzettoDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(subentraMazzettoDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(subentraMazzettiTraccia, out);
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            subentraMazzettoDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(subentraMazzettoDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(subentraMazzettoDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
        try
        {            
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString());
            subentraMazzettoDPView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("XteTestToolSubentraMazzettoDPController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolSubentraMazzettoDPController generaXmlData failed", baseEx);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(baseEx.toString(), MessageType.ERROR);
            subentraMazzettoDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(subentraMazzettoDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(subentraMazzettoDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolSubentraMazzettoDPController generaXmlData failed", ex);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            subentraMazzettoDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(subentraMazzettoDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(subentraMazzettoDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Subentra Mazzetto Data Posta controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        subentraMazzettoDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(subentraMazzettoDPView.getImpostaDatiWSButtonRef());
        enableComponent.add(subentraMazzettoDPView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolSubentraMazzettoDPController start impostaDatiWebService");

            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServiceDPView(subentraMazzettoDPView.getParentFrame());
            XteTestToolWebServiceDPController controller = new XteTestToolWebServiceDPController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            DataPosta dataPostaConfig = XteTestToolConfiguration.getInstance().getDataPostaConfig();
            SM config = dataPostaConfig.getSM();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(subentraMazzettoDPView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(subentraMazzettoDPView.getSubentraMazzettiPanelRef().getParent(), subentraMazzettoDPView.getSubentraMazzettiPanelRef(), webserviceview, new Rectangle(0,70,1000,690));

            logger.info("XteTestToolSubentraMazzettoDPController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolSubentraMazzettoDPController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolSubentraMazzettoDPController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Subentra Mazzetto Data Posta controller impostaDatiWebService failed", ex);
        }
    }

    
    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolSubentraMazzettoDPController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolSubentraMazzettiDPController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(subentraMazzettoDPView.getParentFrame());

            logger.info("XteTestToolSubentraMazzettoDPController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolSubentraMazzettoDPController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolSubentraMazzettoDPController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Subentra Mazzetto Data Posta controller salvaXmlData failed", ex);
        }
    }

}
