package com.selexelsag.xte.model.track.soap;

import java.io.Serializable;

import javax.jws.WebService;


/**
 * Classe di ritorno della chiamata soap ricevuta.
 * Per compatibilita' con sistema soap precedente e' identico a quello di prima, aggiunte solo notazioni per serializzazione xml.
 * 
 * 
 * <?xml version="1.0" encoding="UTF-8"?><br />
 * <trackResponse serviceName="" channel="" channelId="" eventName="" retryFlag="S|N">
 * 		<error errorCode="Positivo|Negativo" errorDesc="Descrizione esito" errorId="Codice esito" />
 * 		<outputParam></outputParam>
 * </trackResponse>
 *
 * @author Motta
 *
 */
@WebService
public class TrackResponse implements Serializable {

	private static final long serialVersionUID = -1582310230937878873L;

	private String serviceName;
	
	private String channel;
	
	private String channelId;

	private String eventName;
	
	private String errorCode;	// ESITO (Positivo/Negativo)
	private String errorDesc;	// DESCRIZIONE ESITO ("bla bla bla")
	private String errorId;		// CODICE ESITO (NM999)
	private String retryFlag;	// REITERABILITA (S/N)
	private String outputParam;

	public TrackResponse() {

	}
	
	public TrackResponse(com.selexelsag.xte.model.track.TrackResponse restResponse) {
		this(
				restResponse.getServiceName(),
				restResponse.getChannel(),
				restResponse.getDestinationOfficeId(),
				restResponse.getEventName()
				);
		this.errorCode = restResponse.getErrorCode();
		this.errorDesc = restResponse.getErrorDesc();
		this.errorId = restResponse.getErrorId();
		this.retryFlag = restResponse.getRetryFlag();
		this.outputParam = restResponse.getOutputParam();
	}
	
	
	public TrackResponse(String serviceName, String channel, String channelId, String eventName) {
		this.serviceName = serviceName;
		this.channel = channel;
		this.channelId = channelId;
		this.eventName = eventName;
	}

	public String getErrorId() {
		return errorId;
	}

	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}

	public String getRetryFlag() {
		return retryFlag;
	}

	public void setRetryFlag(String retryFlag) {
		this.retryFlag = retryFlag;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public String getOutputParam() {
		return outputParam;
	}

	public void setOutputParam(String outputParam) {
		this.outputParam = outputParam;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return String
				.format("TrackResponse [service=%s, channel=%s, channelId=%s, event=%s, errorCode=%s, errorDesc=%s, errorId=%s, retryFlag=%s]",
						serviceName, channel, channelId, eventName, errorCode,
						errorDesc, errorId, retryFlag);
	}
	
	

}
