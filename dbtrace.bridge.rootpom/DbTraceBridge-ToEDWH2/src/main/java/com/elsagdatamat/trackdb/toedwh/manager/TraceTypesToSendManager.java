package com.elsagdatamat.trackdb.toedwh.manager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elsagdatamat.trackdb.toedwh.dao.ITraceTypesToSendDAO;
import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSend;


/*
 * @Author AM,  5/2010
 * @Author AM, 11/2010
 * @Author RL, 01/2011
 */
@Service
public class TraceTypesToSendManager implements ITraceTypesToSendManager {

    @Autowired
    ITraceTypesToSendDAO dao;

    @Override
    public List<TraceTypesToSend> findByDestinationTracedEntityChannel(String destination, String tracedEntity, String channel) {
	TraceTypesToSend example=new TraceTypesToSend();
	example.setChannel(channel);
	example.setDestination(destination);
	example.setTracedEntity(tracedEntity);
	return dao.findByExample(example);

    }


}
