
package com.elsagdatamat.tt.trackdbws.trackdbrwservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TracesList_NextToken complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TracesList_NextToken">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="traceList" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}TracesList"/>
 *         &lt;element name="nextToken" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TracesList_NextToken", propOrder = {
    "traceList",
    "nextToken"
})
public class TracesListNextToken {

    @XmlElement(required = true, nillable = true)
    protected TracesList traceList;
    @XmlElement(required = true)
    protected String nextToken;

    /**
     * Gets the value of the traceList property.
     * 
     * @return
     *     possible object is
     *     {@link TracesList }
     *     
     */
    public TracesList getTraceList() {
        return traceList;
    }

    /**
     * Sets the value of the traceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link TracesList }
     *     
     */
    public void setTraceList(TracesList value) {
        this.traceList = value;
    }

    /**
     * Gets the value of the nextToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextToken() {
        return nextToken;
    }

    /**
     * Sets the value of the nextToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextToken(String value) {
        this.nextToken = value;
    }

}
