/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.objectdocument;

/**
 *
 * @author Da Procida
 */
import com.selexelsag.xte.testtool.controller.common.XteTestToolFieldValidator;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;


public class XteTestToolNumericDocument extends PlainDocument {
     private int maxLenght;

    public XteTestToolNumericDocument(int maxLenght) {
        super();
        this.maxLenght = maxLenght;
    }

    @Override
    public void insertString(int offs, String str, AttributeSet a)
            throws BadLocationException {

        if (str == null) {
            return;
        }

        str = str.replace('.', XteTestToolFieldValidator.getDecimalFormatSymbolsLocal().getDecimalSeparator());
        str = str.replace(',', XteTestToolFieldValidator.getDecimalFormatSymbolsLocal().getDecimalSeparator());
        
        char[] buffer = str.toCharArray();
        char[] digit = new char[buffer.length];
        int j = 0;

        for (int i = 0; i < buffer.length; i++) {
                        
            if (Character.isDigit(buffer[i]) || buffer[i]=='-' || buffer[i]=='+' || buffer[i]==XteTestToolFieldValidator.getDecimalFormatSymbolsLocal().getDecimalSeparator()) {                
                digit[j++] = buffer[i];
            }            
        }

        String added = new String(digit, 0, j);

        if ((getLength() + added.length()) <= maxLenght) {
            super.insertString(offs, added, a);
        }

    }
    
}
