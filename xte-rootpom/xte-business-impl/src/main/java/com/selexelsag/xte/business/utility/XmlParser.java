package com.selexelsag.xte.business.utility;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.selexelsag.xte.business.exceptions.BusinessParserException;

public class XmlParser {

	private static Logger logger = Logger.getLogger(XmlParser.class);
	
	private Document doc;

	public XmlParser() {}
	
	public synchronized void init(String xml) throws BusinessParserException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true); // never forget this!
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			BusinessParserException bge = new BusinessParserException("Error creating document builder ", e1);
			throw bge;
		}
		
		try {
			 // xmlInput = new StringBufferInputStream(xml);
			 InputSource xmlSource = new InputSource(new StringReader(xml));
			
			doc = builder.parse(xmlSource);
		} catch (SAXException e1) {
			BusinessParserException bge = new BusinessParserException("Error parsing document " + xml, e1);
			throw bge;
		} catch (IOException e1) {
			BusinessParserException bge = new BusinessParserException("Error accessing document " + xml, e1);
			throw bge;
		}

	}
	
	public synchronized NodeList getNodeListQuery(String query) throws BusinessParserException {
		
		XPathFactory xPathFactory = XPathFactory.newInstance();

		XPathExpression xPathExpression;
		
		try {
			xPathExpression = xPathFactory.newXPath().compile(query);
		} catch (XPathExpressionException e) {
			BusinessParserException bge = new BusinessParserException("Error compiling xpath query " + query, e);
			throw bge;
		}
		
		Object parsingResult;
		
		try {
			parsingResult = xPathExpression.evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			BusinessParserException bge = new BusinessParserException("Error evaluating xpath query " + query, e);
			throw bge;
		}
		
		NodeList docNodes = (NodeList) parsingResult;
		
		logger.debug("Query "  + query + " gives " + docNodes.getLength() + " entry results");
		return docNodes;
	}
}
