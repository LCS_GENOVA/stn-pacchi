set lines 300
set serveroutput on
set verify off
spool EDWH_Main.a.tt_dbtrace_consumer.lst
col nome new_value  nomefile
select 'Schema_Connesso_'||user nome from dual;
WHENEVER OSERROR EXIT ROLLBACK; 
WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK;

set echo off
prompt ===>>>>EDWH_create_traces_ready_to_send_vw
@EDWH_create_traces_ready_to_send_vw;
commit;

spool off
