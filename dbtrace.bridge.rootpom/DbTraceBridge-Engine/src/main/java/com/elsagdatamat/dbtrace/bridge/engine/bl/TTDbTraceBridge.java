package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.elsagdatamat.dbtrace.bridge.engine.IDbTraceBridge;
import com.elsagdatamat.dbtrace.bridge.engine.IToMessageBuilder;
import com.elsagdatamat.dbtrace.bridge.engine.ITrackTrace;
import com.elsagdatamat.dbtrace.bridge.engine.exceptions.InsertTracesException;
import com.elsagdatamat.dbtrace.bridge.engine.exceptions.TrackDBRWException;
import com.elsagdatamat.dbtrace.bridge.engine.ws.CustomTraceWs;
import com.elsagdatamat.framework.entities.EntityCreationException;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public class TTDbTraceBridge implements IDbTraceBridge<TracesList> {

	private Log log = LogFactory.getLog(this.getClass());

	private IToMessageBuilder traceBuilder;

	public void setTraceBuilder(IToMessageBuilder traceBuilder) {
		this.traceBuilder = traceBuilder;
	}

	private ITrackTrace<CustomTraceWs> trackTrace;

	public void setTrackTraceBL(ITrackTrace<CustomTraceWs> trackTraceBL) {
		this.trackTrace = trackTraceBL;
	}

	public void init() {
		log.info("Initializing WsDbTraceBridge...");

		if (traceBuilder == null)
			throw new NullPointerException("traceBuilder not set");
		if (trackTrace == null)
			throw new NullPointerException("trackTrace not set");

		log.info("WsDbTraceBridge initialized");
	}

	@Override
	public void insertTraces(TracesList tracesList) throws InsertTracesException, TrackDBRWException {

		log.debug("Found traces n. " + tracesList.getTraces().size());

		// // TODO: Decidere l'oggetto che vogliamo usare per disaccoppiare il ws dal tt
		List<CustomTraceWs> lst = null;
		// // //////////////////////////////////////////////////////////////////////////
		try {
			lst = traceBuilder.create(tracesList);
			log.info("Traces created");
		} catch (EntityCreationException e) {
			throw new TrackDBRWException("Impossibile creare le tracce", e);
		}

		if (lst == null || lst.isEmpty()) {
			throw new InsertTracesException("Nessuna traccia estraibile dall'input");
		}

		try {
			trackTrace.insertTrace(lst);
			log.info("Traces processed");
		} catch (Exception e) {
			throw new InsertTracesException("Errore durante la chiamata al ITrackTrace", e);
		}

	}

}
