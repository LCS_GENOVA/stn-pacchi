package com.selexelsag.xte.persistence.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ASYNC_PROCESSED")
public class AsyncProcessed implements java.io.Serializable
{
	/**
	 *
	 */
	private static final long serialVersionUID = 16544645646L;

	private long id;
	private String flowId;
	private String sourceOfficeId;
	private String destinationOfficeId;
	private String objectId;
	private String originalMessage;
	private String processedMessage;
	private Date receivedTimestamp;
	private Date sendTimestamp;
	private String lockId;
	private Date lockTimestamp;
	private String errDescr;

	public AsyncProcessed() {}

	public AsyncProcessed(long id, String flowId, String destinationOfficeId, String originalMessage, Date receivedTimestamp)
	{
		this.id = id;
		this.flowId = flowId;
		this.destinationOfficeId = destinationOfficeId;
		this.originalMessage = originalMessage;
		this.receivedTimestamp = receivedTimestamp;
	}

	public AsyncProcessed(long id, String flowId, String sourceOfficeId,
						String destinationOfficeId, String objectId,
						String originalMessage, String processedMessage,
						Date receivedTimestamp, Date sendTimestamp, String lockId,
						Date lockTimestamp, String errDescr)
	{
		this.id = id;
		this.flowId = flowId;
		this.sourceOfficeId = sourceOfficeId;
		this.destinationOfficeId = destinationOfficeId;
		this.objectId = objectId;
		this.originalMessage = originalMessage;
		this.processedMessage = processedMessage;
		this.receivedTimestamp = receivedTimestamp;
		this.sendTimestamp = sendTimestamp;
		this.lockId = lockId;
		this.lockTimestamp = lockTimestamp;
		this.errDescr = errDescr;
	}

	@Id
	@Column(name = "ID", unique = true, nullable = false, precision = 10, scale = 0)
	public long getId()
	{
		return this.id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@Column(name = "FLOW_ID", nullable = false, length = 16)
	public String getFlowId()
	{
		return this.flowId;
	}

	public void setFlowId(String flowId)
	{
		this.flowId = flowId;
	}

	@Column(name = "SOURCE_OFFICE_ID", length = 5)
	public String getSourceOfficeId()
	{
		return this.sourceOfficeId;
	}

	public void setSourceOfficeId(String sourceOfficeId)
	{
		this.sourceOfficeId = sourceOfficeId;
	}

	@Column(name = "DESTINATION_OFFICE_ID", nullable = false, length = 5)
	public String getDestinationOfficeId()
	{
		return this.destinationOfficeId;
	}

	public void setDestinationOfficeId(String destinationOfficeId)
	{
		this.destinationOfficeId = destinationOfficeId;
	}

	@Column(name = "OBJECT_ID", length = 32)
	public String getObjectId()
	{
		return this.objectId;
	}

	public void setObjectId(String objectId)
	{
		this.objectId = objectId;
	}

	@Column(name = "ORIGINAL_MESSAGE", nullable = false)
	public String getOriginalMessage()
	{
		return this.originalMessage;
	}

	public void setOriginalMessage(String originalMessage)
	{
		this.originalMessage = originalMessage;
	}

	@Column(name = "PROCESSED_MESSAGE")
	public String getProcessedMessage()
	{
		return this.processedMessage;
	}

	public void setProcessedMessage(String processedMessage)
	{
		this.processedMessage = processedMessage;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "RECEIVED_TIMESTAMP", nullable = false, length = 7)
	public Date getReceivedTimestamp()
	{
		return this.receivedTimestamp;
	}

	public void setReceivedTimestamp(Date receivedTimestamp)
	{
		this.receivedTimestamp = receivedTimestamp;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SEND_TIMESTAMP", length = 7)
	public Date getSendTimestamp()
	{
		return this.sendTimestamp;
	}

	public void setSendTimestamp(Date sendTimestamp)
	{
		this.sendTimestamp = sendTimestamp;
	}

	@Column(name = "LOCK_ID", length = 100)
	public String getLockId()
	{
		return this.lockId;
	}

	public void setLockId(String lockId)
	{
		this.lockId = lockId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LOCK_TIMESTAMP", length = 7)
	public Date getLockTimestamp()
	{
		return this.lockTimestamp;
	}

	public void setLockTimestamp(Date lockTimestamp)
	{
		this.lockTimestamp = lockTimestamp;
	}

	@Column(name = "ERR_DESCR", length = 4000)
	public String getErrDescr()
	{
		return this.errDescr;
	}

	public void setErrDescr(String errDescr)
	{
		this.errDescr = errDescr;
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder("ASyncMessages ");
		sb.append("id=" + id + " ");
		sb.append("Original Message=" + originalMessage + " ");;
		sb.append("Processed Message=" + processedMessage + " ");;
		sb.append("received timeStamp=" + receivedTimestamp + " ");
		sb.append("locker=" + lockId + " ");
		sb.append("lock timestamp=" + lockTimestamp + " ");
		sb.append("send timestamp=" + sendTimestamp + " ");
		return sb.toString();
	}
}
