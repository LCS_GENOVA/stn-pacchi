package com.selexelsag.xte.persistence.exceptions;

import com.selexelsag.xte.service.exceptions.BaseException;

public class PersistenceException extends BaseException 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 142142145215L;

	public PersistenceException(String message, Throwable cause) 
	{
		super(message, cause);
		setExceptionId(PERSISTENCE_ID);
	}
	
	public PersistenceException(String msg) 
	{
		super(msg);
	}
}
