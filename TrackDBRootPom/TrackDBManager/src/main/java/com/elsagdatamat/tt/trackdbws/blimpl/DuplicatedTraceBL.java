package com.elsagdatamat.tt.trackdbws.blimpl;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.tt.trackdbws.beans.DuplicatedTrace;
import com.elsagdatamat.tt.trackdbws.beans.DuplicatedTraceDetail;
import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.elsagdatamat.tt.trackdbws.beans.TraceDetail;
import com.elsagdatamat.tt.trackdbws.bl.IDuplicatedTraceBL;
import com.elsagdatamat.tt.trackdbws.dao.IDuplicatedTraceDAO;

/*
 * @Author AM, 10/2010
 */

public class DuplicatedTraceBL implements IDuplicatedTraceBL{
	
	@Autowired
	IDuplicatedTraceDAO duplicatedTraceDAO;

	Log log = LogFactory.getLog(getClass());	
	
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void singleInsertDuplicatedTrace(DuplicatedTrace duplicatedTrace) {
		log.trace("singleInsertDuplicatedTrace. Inizio");

		duplicatedTraceDAO.insert(duplicatedTrace);
		duplicatedTraceDAO.flush();
	
		log.trace("singleInsertDuplicatedTrace. Fine");
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void singleInsertDuplicatedTrace(Trace trace) {
		log.trace("singleInsertDuplicatedTrace. Inizio");
		
		duplicatedTraceDAO.insert(createDuplicatedTraces(trace));
		duplicatedTraceDAO.flush();
		
		log.trace("singleInsertDuplicatedTrace. Fine");
	}
	
	private DuplicatedTrace createDuplicatedTraces(Trace trace) {
		log.trace("createDuplicatedTraces. Inizio");
		DuplicatedTrace duplicatedTrace = new DuplicatedTrace();

		duplicatedTrace.setChannel(trace.getChannel());
		duplicatedTrace.setIdChannel(trace.getIdChannel());
		duplicatedTrace.setIdTracedEntity(trace.getIdTracedEntity());
		duplicatedTrace.setLabelTracedEntity(trace.getLabelTracedEntity());
		duplicatedTrace.setServiceName(trace.getServiceName());
		duplicatedTrace.setTracedEntity(trace.getTracedEntity());
		duplicatedTrace.setWhatHappened(trace.getWhatHappened());
		duplicatedTrace.setWhenHappened(trace.getWhenHappened());
		duplicatedTrace.setWhenRegistered(TraceBLSupport.local2utc());
		duplicatedTrace.setWhereHappened(trace.getWhereHappened());

		Set<TraceDetail> setOfTraceDetails = trace.getInternalDetailList();

		DuplicatedTraceDetail duplicatedTraceDetail;
		Set<DuplicatedTraceDetail> setOfDuplicatedTraceDetails = new HashSet<DuplicatedTraceDetail>();
		for (TraceDetail traceDetail : setOfTraceDetails) {
			duplicatedTraceDetail = new DuplicatedTraceDetail();

			duplicatedTraceDetail.setParamClass(traceDetail.getParamClass());
			duplicatedTraceDetail.setParamValue(traceDetail.getParamValue());
			duplicatedTraceDetail.setDuplicatedTrace(duplicatedTrace);

			setOfDuplicatedTraceDetails.add(duplicatedTraceDetail);
		}
		duplicatedTrace.setInternalDetailList(setOfDuplicatedTraceDetails);
		log.trace("createDuplicatedTraces. Fine");
		return duplicatedTrace;
	}
}
