package com.elsagdatamat.tt.trackdbws.tokenbuilder;

import java.util.Date;

public class TokenBuilder {

	/**
	 * @author AM, 11/2010
	 * @param args
	 */
	public static void main(String[] args) {
		
		if (args==null||args.length!=1) {
			System.out.print("Utilizzo: TokenBuilder \"25/11/10 13.00\"");
			return;
		}
		
		Date date = DateParser.parseADate(args[0]);
		if (date!=null) System.out.print(args[0] + ": "+String.valueOf(String.valueOf(date.getTime())));
	}


	
}
