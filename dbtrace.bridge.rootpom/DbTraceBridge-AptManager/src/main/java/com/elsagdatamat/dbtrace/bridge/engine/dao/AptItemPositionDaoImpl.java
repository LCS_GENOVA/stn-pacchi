package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class AptItemPositionDaoImpl extends SpringHibernateJpaDAOBase<AptItemPosition, String> implements AptItemPositionDao{

}
