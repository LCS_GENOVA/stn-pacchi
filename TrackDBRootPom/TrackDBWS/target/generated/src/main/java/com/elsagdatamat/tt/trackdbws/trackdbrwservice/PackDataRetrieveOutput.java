
package com.elsagdatamat.tt.trackdbws.trackdbrwservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for packDataRetrieveOutput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="packDataRetrieveOutput">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="packDataRetrieveList" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}TraceDetailsList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "packDataRetrieveOutput", propOrder = {
    "packDataRetrieveList"
})
public class PackDataRetrieveOutput {

    @XmlElement(required = true)
    protected TraceDetailsList packDataRetrieveList;

    /**
     * Gets the value of the packDataRetrieveList property.
     * 
     * @return
     *     possible object is
     *     {@link TraceDetailsList }
     *     
     */
    public TraceDetailsList getPackDataRetrieveList() {
        return packDataRetrieveList;
    }

    /**
     * Sets the value of the packDataRetrieveList property.
     * 
     * @param value
     *     allowed object is
     *     {@link TraceDetailsList }
     *     
     */
    public void setPackDataRetrieveList(TraceDetailsList value) {
        this.packDataRetrieveList = value;
    }

}
