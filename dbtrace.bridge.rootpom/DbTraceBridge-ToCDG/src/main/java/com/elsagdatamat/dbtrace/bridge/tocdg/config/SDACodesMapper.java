package com.elsagdatamat.dbtrace.bridge.tocdg.config;

import java.util.Map;

public class SDACodesMapper {
	private Map<String, SDACodes> prodottiPostali;
	private Map<String, SDACodes> prodottiSDA;
	public Map<String, SDACodes> getProdottiPostali() {
		return prodottiPostali;
	}
	public void setProdottiPostali(Map<String, SDACodes> prodottiPostali) {
		this.prodottiPostali = prodottiPostali;
	}
	public Map<String, SDACodes> getProdottiSDA() {
		return prodottiSDA;
	}
	public void setProdottiSDA(Map<String, SDACodes> prodottiSDA) {
		this.prodottiSDA = prodottiSDA;
	}
}
