package com.selexelsag.tt.dbtrace.bridge.sender;

import java.io.Serializable;

import com.selexelsag.tt.dbtrace.bridge.sender.update.core.ProductKey;



public interface IMessageDestination  {
	
	public void init();
	public void send(final Serializable trace, final String externalSystem);
	public boolean isFlagSender();
	public boolean getProductConfig(ProductKey key);
	
}
