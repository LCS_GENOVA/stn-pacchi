package com.elsagdatamat.dbtrace.bridge.engine.bl;

import com.elsagdatamat.dbtrace.bridge.engine.entities.FromTo;

public interface IBLFromTo extends IManager<FromTo, String> {

	String getCmpDestinationFromNspSource(String nspSource);

}
