package com.elsagdatamat.dbtrace.bridge.engine;

import java.util.List;

import com.elsagdatamat.dbtrace.bridge.engine.ws.CustomTraceWs;
import com.elsagdatamat.framework.entities.IEntityBuilder;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public interface IToMessageBuilder extends IEntityBuilder<TracesList, List<CustomTraceWs>> {


}
