package com.selexelsag.xte.bpm.flows.servicenodes.handlers;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.drools.process.instance.WorkItemHandler;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemManager;

import com.selexelsag.xte.business.tracking.ITrasformation;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.model.flows.FlowResponse;
import com.selexelsag.xte.service.context.SpringApplicationContext;

public class ResponseCreationNode implements WorkItemHandler {
	
	private Logger logger = LogManager.getLogger(ResponseCreationNode.class.getName());

	@Override
	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
		
	}

	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		StringBuilder data = (StringBuilder)workItem.getParameter("data");
		if (data==null) {
			logger.debug("data parameter is null!");
		}
		
		FlowParams params = (FlowParams)workItem.getParameter("flowParams");
		if (params==null) {
			logger.debug("flowParams parameter is null!");
		}
		
		FlowResponse flowResponse = (FlowResponse)workItem.getParameter("flowResponse");
		if (flowResponse==null) {
			logger.debug("flowResponse parameter is null!");
		}
		
		Map<String, Object> results = null;
		
		try{
			
			// StringBuilder dataResult = (StringBuilder)workItem.getParameter("dataResult");
			
			ITrasformation transformation = (ITrasformation)SpringApplicationContext.
					getBean(ITrasformation.TRASFORMATION_BEAN_NAME);

			transformation.buildResponse(params, data, flowResponse);
		
			results = new HashMap<String, Object>();
				
			results.put("flowResponse", flowResponse);
			
		}catch (Throwable t){
			// logger.error(t , t);
			
			results = new HashMap<String, Object>();
			results.put("error", t);
			results.put("flowResponse", flowResponse);
		}finally {
			manager.completeWorkItem(workItem.getId(), results);
		}

	}

}
