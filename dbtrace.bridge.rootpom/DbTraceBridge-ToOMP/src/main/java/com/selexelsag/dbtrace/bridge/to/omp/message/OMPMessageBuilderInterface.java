package com.selexelsag.dbtrace.bridge.to.omp.message;


public interface OMPMessageBuilderInterface {

	public static final String DEFAULT_NAME = "ompMessageBuilder";

	public long[] getNumberOfRuns();
	
	public void setNumberOfRuns(long[] numberOfRuns);
	
	public void sendTestMessage();
	
	public void loadQueryParams();
	
}