package com.elsagdatamat.tt.trackdbws.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.elsagdatamat.tt.trackdbws.utilities.SortingSpecifications;

public interface ITraceBL {

	public List<Trace> getListOfNTraces(Date date, String fieldToFilterDate, Long traceId, Trace getTraceFilterByExampleEqual,
			Trace getTraceFilterByExampleNotEqual, Date dateTillto, int size, ArrayList<SortingSpecifications> sortBy);

	public List<Trace> getListOfNTraces(Date date, String fieldToFilterDate, Long traceId, List<Trace> filtersByExampleEqual,
			List<Trace> filtersByExampleNotEqual, Date dateTillto, int size, ArrayList<SortingSpecifications> sortBy);

	public List<Trace> fastSackImageFromNSPBySackCode(String parameterValue);

	public List<Trace> nextSackTraceTillTo(Long traceId, Date date, Date dateTillto);

	public long insertTrace(List<Trace> listOfTraces);
	
	public void mergeAvvNspTraceWithAccNspTrace(Trace t);

	List<Trace> fastSackImageFromNSPByPackCode(String parameterValue);
	
	// R.L. - Aggiunto 05.2012 per il cruscotto pacchi
	public boolean insertTrace(Trace trace);

    List<Trace> nextSackTraceTillTo(Long traceId, Date date, Date dateTillto, String channel);
}
