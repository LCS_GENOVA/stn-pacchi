/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.message;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.bind.JAXBException;
import javax.xml.validation.Schema;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.core.JmsTemplate;

import com.elsagdatamat.dbtrace.bridge.engine.MessageBuilderBase;
import com.elsagdatamat.dbtrace.bridge.engine.entities.OfficeMapper;
import com.elsagdatamat.framework.cache.SimpleCache;
import com.elsagdatamat.framework.cache.SimpleCacheCallback;
import com.elsagdatamat.framework.resources.ClassPathResource;
import com.selexelsag.dbtrace.bridge.to.omp.manager.OMPManagerInterface;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1.DHDR;
import com.selexelsag.dbtrace.bridge.to.omp.message.trans.Transformations;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TokenTilltoDateByFiltersList;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesListNextToken;

/**
 * @author arodriguez
 * 
 */
public class OMPMessageBuilder extends MessageBuilderBase implements OMPMessageBuilderInterface {

	private static final Log log = LogFactory.getLog(OMPMessageBuilder.class);
	private JmsTemplate jmsTemplate;
	private ClassPathResource xsdPath, queryParams;
	private OMPManagerInterface ompManager;
	private long callTimeout = 45;

	private long[] numberOfRuns = new long[]{0,0,0};
	private Transformations transformations;
	
	private Map<String, String> didFilter, channelFilter;

	/**
	 * default constructor
	 */
	public OMPMessageBuilder() {
		transformations = new Transformations();
		didFilter = new HashMap<String, String>();
		channelFilter = new HashMap<String, String>();
	}

	/**
	 * set Spring JMS template for queuing activities
	 * @param jmsTemplate the JmsTemplate to set
	 */
	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	/**
	 * set Spring classpath matching path of XSD schema files
	 * @param xsdPath the ClassPathResource to set
	 */
	public void setXsdPath(ClassPathResource xsdPath) {
		this.xsdPath = xsdPath;
	}

	/**
	 * set Spring classpath matching path of query-params properties file
	 * @param queryParamsPath the ClassPathResource to set
	 */
	public void setQueryParams(ClassPathResource queryParamsPath) {
		this.queryParams = queryParamsPath;
	}

	/**
	 * set the OMP token manager
	 * @param ompManager the ompManager to set
	 */
	public void setOmpManager(OMPManagerInterface ompManager) {
		this.ompManager = ompManager;
		transformations.setOmpManager(ompManager);
	}

	
	/**
	 * @return the callTimeout
	 */
	public long getCallTimeout() {
		return callTimeout;
	}

	/**
	 * @param callTimeout the callTimeout to set
	 */
	public void setCallTimeout(long callTimeout) {
		this.callTimeout = callTimeout;
	}

	/**
	 * @return the numberOfRuns
	 */
	@Override
	public long[] getNumberOfRuns() {
		return numberOfRuns;
	}

	/**
	 * @param numberOfRuns the numberOfRuns to set
	 */
	@Override
	public void setNumberOfRuns(long[] numberOfRuns) {
		this.numberOfRuns = numberOfRuns;
	}

	private Map<String, String> getFilterValuesFor(String filterName, MessageBuilderFilterEnum filterType) {
		Map<String, String> result = new HashMap<String, String>();
		Properties paramsProperties = new Properties();
		BufferedInputStream bis;
		try {
			URL url = new URL(queryParams.getURL());
			bis = new BufferedInputStream(url.openStream());
			paramsProperties.load(bis);
			bis.close();
			if (paramsProperties.containsKey(filterName + filterType.value())) {
				String[] values = paramsProperties.getProperty(filterName + filterType.value(),"").split(",");	
				for (String value : values)
					result.put(value, value);
				log.info("Loaded " + values.length + " query params for key " + filterName + " (" + filterType + ")");
			}
		} catch(IOException ex) {
			log.error("Unable to load query parameters for: " + filterName + filterType.value(), ex);
		} finally {
			bis = null;
		}
		return result;
	}
	
	@Override
	public void loadQueryParams() {
		didFilter.clear();
		channelFilter.clear();
		didFilter.putAll(getFilterValuesFor("did", MessageBuilderFilterEnum.ALLOWED));
		channelFilter.putAll(getFilterValuesFor("channel", MessageBuilderFilterEnum.NOTALLOWED));
	}
	
	/**
	 * DBTraceEngine callback, get Traces list from WS
	 * @see com.elsagdatamat.dbtrace.bridge.engine.ITracesReader#getTraces(com.elsagdatamat.trackdbws.client.TrackDBRWServicePortType, java.lang.Object, java.util.Date)
	 */
	@Override
	public TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate) {
		
		// total counters increment
		numberOfRuns[MessageBuilderCounterEnum.TotalRuns.index()]++;
		// pass ompManager reference
		transformations.setOmpManager(ompManager);

		// Prepare working token
		TokenTilltoDateByFiltersList filterCallPrepare = new TokenTilltoDateByFiltersList();
		filterCallPrepare.setTilltoDate(transformations.toXMLGregorian(stopDate));
		filterCallPrepare.setToken(token.getWsToken());

		
		// Prepare top query params
		TracesList traceEqualList = new TracesList();
		
		Trace traceQueryParamsEquals = new Trace();
		traceQueryParamsEquals.setTracedEntity("PACCO");
		traceQueryParamsEquals.setWhatHappened("UFI_SDA");
		traceEqualList.getTraces().add(traceQueryParamsEquals);
		

		TracesList traceNotEqualList = new TracesList();
		for (String notEqChannel : channelFilter.keySet()) {
			Trace traceQueryParamsNotEquals = new Trace();
			traceQueryParamsNotEquals.setChannel(notEqChannel);
			traceNotEqualList.getTraces().add(traceQueryParamsNotEquals);
		}
		
		log.info("Call DBtrace WS with " + (traceEqualList.getTraces().size() + traceNotEqualList.getTraces().size()) + " filter conditions...");
		filterCallPrepare.setTracesFilterByExampleEqual(traceEqualList);
		filterCallPrepare.setTracesFilterByExampleNotEqual(traceNotEqualList);

		// Let's use an ExecutorService to async-decouple the WS call with a global timeout  
		CallableFilteredTraces callableWS = new CallableFilteredTraces(filterCallPrepare, twsp);
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Future<TracesListNextToken> taskWS = executorService.submit(callableWS);
        log.info("Called web service getTracesByFiltersList(), with timeout: " + callTimeout + " sec.");
        
        TracesListNextToken tracesFromTokenCall = null;
        try {
            // ok, wait for 'callTimeout' seconds max
        	tracesFromTokenCall = taskWS.get(callTimeout, TimeUnit.SECONDS);
        } catch (ExecutionException e) {
			// ko counter increment
			numberOfRuns[MessageBuilderCounterEnum.FailedRuns.index()]++;
        	log.error("Execution failed during WS calling ... ", e);
        } catch (TimeoutException e) {
			// timeout counter increment
			numberOfRuns[MessageBuilderCounterEnum.TimeoutRuns.index()]++;
        	log.warn("WS call timeout expired; abort ", e);
        } catch (InterruptedException e) {
    		// cancel, so total counter decrement
    		numberOfRuns[MessageBuilderCounterEnum.TotalRuns.index()]--;
        	log.warn("WS call interrupted, abort ", e);
        }

		TracesList tracesList = tracesFromTokenCall.getTraceList();
		
		TracesList filteredTracesListToOMP = filterTracesList(tracesList);
		

		// Update working token WsToken content
		token.setWsToken(tracesFromTokenCall.getNextToken());
		
		if (filteredTracesListToOMP == null || filteredTracesListToOMP.getTraces() == null) {
			if (taskWS.isDone())
				log.info("It seems that there was nothing for us in DB ...");
			return null;
		}

		// ok counters increment
		numberOfRuns[MessageBuilderCounterEnum.SuccessRuns.index()]++;

		log.info("Retrieved " + filteredTracesListToOMP.getTraces().size() + " traces from DBtrace");
		return filteredTracesListToOMP;
	}

	private TracesList filterTracesList(TracesList tracesList) {
		
		SimpleCache<String, OfficeMapper> ompOfficesCache = new SimpleCache<String, OfficeMapper>(new SimpleCacheCallback<String, OfficeMapper>() {

			@Override
			public OfficeMapper get(String key) {
				
				OfficeMapper officeMapper = ompManager.findOfficeMapperByCode(key);
				return officeMapper;
			}

			
		});
		
		
		TracesList filteredTracesListToOMP = new TracesList();
		
		for (Trace trace:tracesList.getTraces())
		{
			OfficeMapper office = ompOfficesCache.get(trace.getIdChannel());
			
			if (office != null)
				filteredTracesListToOMP.getTraces().add(trace);
		}
		
		
		return filteredTracesListToOMP;

	}

	/**
	 * extract DHDR message header from xml artifact
	 * @param body  the String xml content
	 * @return  DHDR message header
	 */
	private DHDR messageBodyToDHDR(String body) {
		try {
			MSG tempMessage = transformations.unmarshallXmlToMessage(body, null);
			D1 tempD1container = (D1)tempMessage.getHDROrD1().get(1);
			return tempD1container.getDHDR().get(0);
		} catch(Exception ex) {
			return new DHDR();
		}
	}
	
	/**
	 * DBTraceEngine callback, send messages to destination
	 * @see com.elsagdatamat.dbtrace.bridge.engine.IMessageSender#send(java.util.List)
	 */
	@Override
	public void send(List<String> messageBodies) throws IOException {
		log.info(messageBodies.size() + " messages ready to send.");
		for (final String str : messageBodies) {
			try {
				DHDR tempDhdr = messageBodyToDHDR(str);  // TODO: a bit redundant solution, but arch template are too coupled...
				jmsTemplate.send(new D1PCompressedMessageCreator(str, tempDhdr.getOFCOTH(), tempDhdr.getDID()));
				log.info("Message queued.");
				log.debug("Content:\n" + str);
			} catch (Exception jmsEx) {
				log.error("Unable to queue message ", jmsEx);
				throw new IOException(jmsEx);
			}
		}
	}

	/**
	 * DBTraceEngine callback, create the message body list from traces
	 * @see com.elsagdatamat.dbtrace.bridge.engine.IMessageBuilder#create(com.elsagdatamat.trackdbws.client.TracesList, java.lang.Object)
	 * @param input  the TracesList input from WS
	 * @param token  the working token acquired
	 * @return List<String> of XML formatted messages
	 */
	@Override
	public List<String> create(TracesList input) {
		List<String> msgBodiesList = new ArrayList<String>();
		
		Schema schema = null;
		try {
			schema = transformations.loadSchemaFrom(xsdPath.getURL());
		} catch (Exception schemaEx) {
			log.warn("Unable to load XML validation Schema; format message without it");
		}
		
		// sort and group Traces by channel value
		SortedMap<String, TracesList> sortedTraces = groupTracesByChannel(input);
		
		String channelKey = null;
		try {
			log.info("Creating message bodies ...");
			Iterator<String> keys = sortedTraces.keySet().iterator();
			log.info("Found " + sortedTraces.size() + " destination match: " + sortedTraces.keySet());
			while (keys.hasNext()) {
				channelKey = keys.next();
				String xmlMessageBody = xmlMessageFromTraces(channelKey, sortedTraces.get(channelKey), schema);
				msgBodiesList.add(xmlMessageBody);
				log.debug("Message body created:\n " + xmlMessageBody);
				if (saveTraces)
					checkSaveTraces(xmlMessageBody,  null);
			}
			log.info(sortedTraces.size() + " Message bodies successfully created.");
		} catch (JAXBException ex) {
			log.error("Errors creating XML message bodies ", ex);
			try {
				checkSaveTraces(xmlMessageFromTraces(channelKey, input, null),  ex);
			} catch (JAXBException rex) {
				log.error("Errors trying to recovery XML message body from error (" + ex.getMessage() + ")", rex);
			}
		}
		return msgBodiesList;
	}

	protected SortedMap<String, TracesList> groupTracesByChannel(TracesList input) {
		SortedMap<String, TracesList> sortedResult = 
			Collections.synchronizedSortedMap(Collections.checkedSortedMap(new TreeMap<String, TracesList>(),	String.class, TracesList.class));
		List<Trace> ungroupedTraces = input.getTraces();
		for (Trace responseTrace : ungroupedTraces) {
			String key = responseTrace.getIdChannel(); /** previous candidates was getChannel(), getIdForwardTo(): @see: com.selexelsag.dbtrace.bridge.to.omp.message.trans.Transformations.traceListToMessage(String, List<Trace>) */			
			if (!sortedResult.containsKey(key)) {
				sortedResult.put(key, new TracesList());
			}
			sortedResult.get(key).getTraces().add(responseTrace);
		}
		return sortedResult;
	}

	private String xmlMessageFromTraces(String channelGroupKey, TracesList input, Schema schema) throws JAXBException {
		MSG jaxbMessage = transformations.traceListToMessage(channelGroupKey, input.getTraces());
		return transformations.marshallMessageToXml(jaxbMessage, schema);
	}

	private void checkSaveTraces(String xmlMessage,  Exception marshallEx) {
		discardedTraceManager.saveDiscardedTraces(token.getServiceId(), xmlMessage, marshallEx);
	}
	
	/**
	 * send test messages to destination queue
	 */
	@Override
	public void sendTestMessage() {
		try {
			jmsTemplate.send(new D1PTestMessageCreator(D1PTestMessageCreator.EMPTY_MESSAGE_BODY));
			log.info("Test message successfull queued");
		} catch (Exception jmsEx) {
			log.error("Unable to queue test message ", jmsEx);
		}
	}
	
	/**
	 * implements a Callable statement for async WS call
	 * @author arodriguez
	 */
	class CallableFilteredTraces implements Callable<TracesListNextToken> {

		private TokenTilltoDateByFiltersList filterList;
		private TrackDBRWServicePortType trackDBws;
	
		/**
		 * @param filterList
		 * @param trackDBws
		 */
		public CallableFilteredTraces(TokenTilltoDateByFiltersList filterList, TrackDBRWServicePortType trackDBws) {
			this.filterList = filterList;
			this.trackDBws = trackDBws;
		}

		/**
		 * call WS
		 */
		@Override
		public TracesListNextToken call() throws Exception {
			return trackDBws.getTracesByFiltersList(filterList);
		}
		
	}
}
