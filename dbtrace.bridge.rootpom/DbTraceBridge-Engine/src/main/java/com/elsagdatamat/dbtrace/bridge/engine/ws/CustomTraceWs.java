package com.elsagdatamat.dbtrace.bridge.engine.ws;

import java.lang.reflect.Method;
import java.util.List;
import java.util.UUID;

import com.elsagdatamat.framework.entities.Entity;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetailsList;

public class CustomTraceWs extends Trace implements Entity {

	private String strUuid;

	public CustomTraceWs() {
		setTraceDetailsList(new TraceDetailsList());
		String tmpUuid = UUID.randomUUID().toString();
		tmpUuid = tmpUuid.replace("-", "");
		this.strUuid = tmpUuid.substring(0, 30);
	}

	public void addDetails(String tag, String value) {
		TraceDetail td = new TraceDetail();
		td.setParamClass(tag);
		td.setParamValue(value);
		getTraceDetailsList().getTraceDetails().add(td);
	}
	
	public void addAndReplaceDetails(String tagToReplace, String value){

		List<TraceDetail> traceDetails = getTraceDetailsList().getTraceDetails();
		for (TraceDetail traceDetail : traceDetails) 
		{
			if(traceDetail.getParamClass().equals(tagToReplace)){
				traceDetails.remove(traceDetail);
				addDetails(tagToReplace, value);
				return;
			}
		}
		addDetails(tagToReplace, value);
	}

	public void removeDetails(String tagToRemove){

		List<TraceDetail> traceDetails = getTraceDetailsList().getTraceDetails();
		for (TraceDetail traceDetail : traceDetails) 
		{
			if(traceDetail.getParamClass().equals(tagToRemove)){
				traceDetails.remove(traceDetail);
				return;
			}
		}
	}

	public void generateUUID(String tag, String methodToInvoke) {
		Method[] methods = this.getClass().getMethods();
		for (Method method : methods) {
			if (method.getName().contentEquals(methodToInvoke)) {
				try {
					method.invoke(this, strUuid);
					return;
				} catch (Exception e) {
					throw new RuntimeException("Invalid method to invoke: " + methodToInvoke);
				}
			}
		}

		TraceDetail td = new TraceDetail();
		td.setParamClass(tag);
		td.setParamValue(strUuid);
		getTraceDetailsList().getTraceDetails().add(td);
	}

	@Override
	public CustomTraceWs clone() {
		CustomTraceWs trace = new CustomTraceWs();

		trace.setChannel(getChannel());
		trace.setIdChannel(getIdChannel());
		trace.setIdTracedEntity(getIdTracedEntity());
		trace.setTracedEntity(getTracedEntity());
		trace.setWhatHappened(getWhatHappened());
		trace.setWhenHappened(getWhenHappened());
		trace.setWhereHappened(getWhereHappened());
		trace.setLabelTracedEntity(getLabelTracedEntity());
		trace.setServiceName(getServiceName());
		trace.setStrUuid(getStrUuid());
		// Aggiunti R.L. - 04.05.2012
		trace.setIdCorrelazione(getIdCorrelazione());
		trace.setIdForwardTo(getIdForwardTo());
		trace.setSysForwardTo(getSysForwardTo());
		trace.setIdStatus(getIdStatus());
		trace.setIdTracedExternal(getIdTracedExternal());
		
		trace.getTraceDetailsList().getTraceDetails().addAll(getTraceDetailsList().getTraceDetails());
		return trace;
	}

	public void setStrUuid(String strUuid) {
		this.strUuid = strUuid;
	}

	public String getStrUuid() {
		return strUuid;
	}

}
