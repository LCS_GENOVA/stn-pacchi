/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.message.trans;

import java.math.BigInteger;

import javax.xml.bind.JAXBElement;

import com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory;

/**
 * @author arodriguez
 *
 */
public class NullableObjectFactoryDecorator extends ObjectFactory {

	private ObjectFactory innerFactory;
	
	/**
	 * 
	 */
	private NullableObjectFactoryDecorator() {
		super();
	}

	/**
	 * @param innerFactory
	 */
	public NullableObjectFactoryDecorator(ObjectFactory innerFactory) {
		this.innerFactory = innerFactory;
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1DHDRCOURIER(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1DHDRCOURIER(String value) {
		return (value == null ? null : innerFactory.createMSGD1DHDRCOURIER(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1DHDRMC(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1DHDRMC(String value) {
		return (value == null ? null : innerFactory.createMSGD1DHDRMC(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGCODEAR(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJREGCODEAR(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGCODEAR(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGINSVAL(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMSGD1MOBJREGINSVAL(BigInteger value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGINSVAL(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGDADDR(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJREGDADDR(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGDADDR(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGREGT(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJREGREGT(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGREGT(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGCUSTCODE(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJREGCUSTCODE(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGCUSTCODE(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGNCHECK(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMSGD1MOBJREGNCHECK(BigInteger value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGNCHECK(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGCODVAL(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMSGD1MOBJREGCODVAL(BigInteger value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGCODVAL(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGWB(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMSGD1MOBJREGWB(BigInteger value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGWB(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGWA(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMSGD1MOBJREGWA(BigInteger value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGWA(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGNSEALS(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMSGD1MOBJREGNSEALS(BigInteger value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGNSEALS(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGADEST(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJREGADEST(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGADEST(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGRCURR(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJREGRCURR(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGRCURR(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGADDR(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJREGADDR(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGADDR(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJREGSA(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJREGSA(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJREGSA(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJPACCONTOCORRENTEDEST(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJPACCONTOCORRENTEDEST(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJPACCONTOCORRENTEDEST(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJPACCODISTR(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJPACCODISTR(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJPACCODISTR(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJPACNUMMANCATACONSEGNA(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJPACNUMMANCATACONSEGNA(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJPACNUMMANCATACONSEGNA(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJPACIMPONERIDOGANALI(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMSGD1MOBJPACIMPONERIDOGANALI(BigInteger value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJPACIMPONERIDOGANALI(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJPACFLAGRRR(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJPACFLAGRRR(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJPACFLAGRRR(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJPACNUMGIORNIGIACENZA(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJPACNUMGIORNIGIACENZA(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJPACNUMGIORNIGIACENZA(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJPACIMPDIRITTIPOSTALI(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMSGD1MOBJPACIMPDIRITTIPOSTALI(BigInteger value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJPACIMPDIRITTIPOSTALI(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJPACFLAGANN(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJPACFLAGANN(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJPACFLAGANN(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJPACSTATUS(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJPACSTATUS(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJPACSTATUS(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJPACFILIALESDA(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJPACFILIALESDA(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJPACFILIALESDA(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJACCACURR(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJACCACURR(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJACCACURR(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJACCZIP(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJACCZIP(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJACCZIP(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJACCFARE(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMSGD1MOBJACCFARE(BigInteger value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJACCFARE(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJACCPERC(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMSGD1MOBJACCPERC(BigInteger value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJACCPERC(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJOBJBTSF(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJOBJBTSF(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJOBJBTSF(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJOBJSCANTIME(javax.xml.datatype.XMLGregorianCalendar)
	 */
	public JAXBElement<String> createMSGD1MOBJOBJSCANTIME(String value) {
		return(value == null ? null : innerFactory.createMSGD1MOBJOBJSCANTIME(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory#createMSGD1MOBJOBJFAWAIT(java.lang.String)
	 */
	public JAXBElement<String> createMSGD1MOBJOBJFAWAIT(String value) {
		return (value == null ? null : innerFactory.createMSGD1MOBJOBJFAWAIT(value));
	}

	
}
