package com.elsagdatamat.tt.trackdbws.dao;

import com.elsagdatamat.framework.dao.IGenericDAO;
import com.elsagdatamat.tt.trackdbws.beans.DuplicatedTrace;


public interface IDuplicatedTraceDAO extends IGenericDAO<DuplicatedTrace, Long> { 

	
}