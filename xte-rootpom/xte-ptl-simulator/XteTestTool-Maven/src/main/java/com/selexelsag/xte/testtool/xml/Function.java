package com.selexelsag.xte.testtool.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",    
    "name",
    "description",
    "context",
    "viewclass",
    "controllerclass"
})
public class Function {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected String description;
    @XmlElement(required = true)
    protected String context;
    @XmlElement(required = true)
    protected String viewclass;
    @XmlElement(required = true)
    protected String controllerclass;

    /**
     * Gets the value of the id property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the context property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getContext() {
        return context;
    }

    /**
     * Sets the value of the context property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setContext(String value) {
        this.context = value;
    }

    /**
     * Gets the value of the viewclass property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getViewclass() {
        return viewclass;
    }

    /**
     * Sets the value of the viewclass property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setViewclass(String value) {
        this.viewclass = value;
    }

    /**
     * Gets the value of the controllerclass property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getControllerclass() {
        return controllerclass;
    }

    /**
     * Sets the value of the controllerclass property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setControllerclass(String value) {
        this.controllerclass = value;
    }
}
