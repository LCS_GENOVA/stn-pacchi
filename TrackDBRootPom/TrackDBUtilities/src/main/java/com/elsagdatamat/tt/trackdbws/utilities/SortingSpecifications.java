package com.elsagdatamat.tt.trackdbws.utilities;

public class SortingSpecifications {

	private String field;
	private boolean desc;
	
	public SortingSpecifications(){}
	public SortingSpecifications(String field, boolean desc){
		this.field=field; 
		this.desc=desc;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public boolean isDesc() {
		return desc;
	}
	public void setDesc(boolean desc) {
		this.desc = desc;
	}

}

