/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.dp;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.view.XteTestToolPanelView;
import com.selexelsag.xte.testtool.view.common.XteTestToolFocusTraversalPolicy;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolSubentraMazzettiDPView extends XteTestToolPanelView{
    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolSubentraMazzettiDPView.class);
    
    public XteTestToolSubentraMazzettiDPView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try {
            logger.info("XteTestToolSubentraMazzettiDPView start class creation");
            this.messages = ResourceBundle.getBundle("bundles/messages");
            initComponents();
            logger.info("XteTestToolSubentraMazzettiDPView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolSubentraMazzettiDPView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Subentra Mazzetti Data Posta view creation failed", ex);
        }
    }

    public JPanel getSubentraMazzettoPanelRef(){ return this; }        
    public JLabel getSubentraMazzettoTitleLabelRef(){ return this.panelTitlejLabel; }
    public JPanel getXmlDataPaneRef(){ return this.xmlDatajPanel; }
    public JScrollPane getXmlDataScrollPaneRef(){ return this.xmlDatajScrollPane; }
    public JTextArea getXmlDataTextAreaRef(){ return this.xmlDatajTextArea; }
    public JPanel getDataCommandPaneRef(){ return this.dataCommandjPanel; }    
    public JButton getGeneraXmlDataButtonRef(){ return this.generaXmlDatajButton; }
    public JButton getImpostaDatiWSButtonRef(){ return this.impostaDatiWSjButton; }
    public JButton getSalvaXmlDataButtonRef(){ return this.salvaXmlDatajButton; }
    public JButton getBackButtonRef(){ return this.backjButton; }
    public JButton getHomeButtonRef(){ return this.homejButton; }
    public JButton getExitButtonRef(){ return this.exitjButton; }
    public JTextPane getMessageTextPaneRef(){ return this.messageBarjTextPane; }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolSubentraMazzettiDPView start customizeView");
            //CUSTOMIZE TITLE
            this.panelTitlejLabel .setText(messages.getString("dataposta.subentramazzetto.view.title.label"));

            //CUSTOMIZE BUTTON
            this.generaXmlDatajButton.setText(messages.getString("dataposta.subentramazzetto.view.generaxmldata.button"));
            this.impostaDatiWSjButton.setText(messages.getString("dataposta.subentramazzetto.view.impostadatiws.button"));
            this.salvaXmlDatajButton.setText(messages.getString("dataposta.subentramazzetto.view.salvaxmldata.button"));
            this.backjButton.setText(messages.getString("view.back.button"));
            this.homejButton.setText(messages.getString("view.home.button"));
            this.exitjButton.setText(messages.getString("view.exit.button"));

            logger.info("XteTestToolSubentraMazzettiDPView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolSubentraMazzettiDPView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Subentra Mazzetti Data Posta view customization failed", ex);
        }
    }

    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolSubentraMazzettiDPView start initializeView");

            //buildComponentSortedMap();

            //this.setFocusCycleRoot(true);
            //this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

            JLabel cornelLabel = new JLabel("");
            cornelLabel.setOpaque(true);
            cornelLabel.setBackground(new Color(240, 240, 240));
            
            this.xmlDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.xmlDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.xmlDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.xmlDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.xmlDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            logger.info("XteTestToolSubentraMazzettiDPView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolSubentraMazzettiDPView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Subentra Mazzetti Data Posta view initialization failed", ex);
        }
    }

    @Override
    public void restoreView() {
        logger.debug("XteTestToolSubentraMazzettiDPView restoreView");
        this.getExitButtonRef().doClick();
    }

    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.xmlDatajTextArea);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        commandjPanel = new javax.swing.JPanel();
        messageBarjScrollPane = new javax.swing.JScrollPane();
        messageBarjTextPane = new javax.swing.JTextPane();
        homejButton = new javax.swing.JButton();
        exitjButton = new javax.swing.JButton();
        backjButton = new javax.swing.JButton();
        workingjPanel = new javax.swing.JPanel();
        panelTitlejLabel = new javax.swing.JLabel();
        xmlDatajPanel = new javax.swing.JPanel();
        xmlDatajScrollPane = new javax.swing.JScrollPane();
        xmlDatajTextArea = new javax.swing.JTextArea();
        dataCommandjPanel = new javax.swing.JPanel();
        generaXmlDatajButton = new javax.swing.JButton();
        impostaDatiWSjButton = new javax.swing.JButton();
        salvaXmlDatajButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setMaximumSize(new java.awt.Dimension(1000, 690));
        setMinimumSize(new java.awt.Dimension(1000, 690));
        setPreferredSize(new java.awt.Dimension(1000, 690));

        commandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        commandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        commandjPanel.setMaximumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setMinimumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setPreferredSize(new java.awt.Dimension(980, 80));

        messageBarjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        messageBarjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageBarjScrollPane.setMaximumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setMinimumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setPreferredSize(new java.awt.Dimension(350, 50));

        messageBarjTextPane.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        messageBarjTextPane.setFocusable(false);
        messageBarjScrollPane.setViewportView(messageBarjTextPane);

        homejButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        homejButton.setForeground(new java.awt.Color(51, 51, 51));
        homejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Home3.png"))); // NOI18N
        homejButton.setText("<html><body>&nbsp;Home<body><html>");
        homejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        homejButton.setFocusPainted(false);
        homejButton.setFocusable(false);
        homejButton.setIconTextGap(5);
        homejButton.setMaximumSize(new java.awt.Dimension(100, 50));
        homejButton.setMinimumSize(new java.awt.Dimension(100, 50));
        homejButton.setPreferredSize(new java.awt.Dimension(100, 50));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("<html><body>&nbsp;Exit<body><html>");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setFocusPainted(false);
        exitjButton.setFocusable(false);
        exitjButton.setIconTextGap(5);
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        backjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        backjButton.setForeground(new java.awt.Color(51, 51, 51));
        backjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/BackToMenu.png"))); // NOI18N
        backjButton.setText("<html><body>&nbsp;Back<body><html>");
        backjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        backjButton.setFocusPainted(false);
        backjButton.setFocusable(false);
        backjButton.setIconTextGap(5);
        backjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        backjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        backjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout commandjPanelLayout = new javax.swing.GroupLayout(commandjPanel);
        commandjPanel.setLayout(commandjPanelLayout);
        commandjPanelLayout.setHorizontalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 272, Short.MAX_VALUE)
                .addComponent(backjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandjPanelLayout.setVerticalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandjPanelLayout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(backjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        workingjPanel.setBackground(new java.awt.Color(230, 250, 0));
        workingjPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20)));
        workingjPanel.setMaximumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setMinimumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setPreferredSize(new java.awt.Dimension(980, 580));

        panelTitlejLabel.setBackground(new java.awt.Color(240, 240, 240));
        panelTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        panelTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        panelTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelTitlejLabel.setText("Subentra Mazzetti");
        panelTitlejLabel.setFocusable(false);
        panelTitlejLabel.setIconTextGap(30);
        panelTitlejLabel.setMaximumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setMinimumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setPreferredSize(new java.awt.Dimension(920, 20));

        xmlDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "XML Data", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        xmlDatajPanel.setMaximumSize(new java.awt.Dimension(860, 460));
        xmlDatajPanel.setMinimumSize(new java.awt.Dimension(860, 460));
        xmlDatajPanel.setOpaque(false);
        xmlDatajPanel.setPreferredSize(new java.awt.Dimension(860, 460));

        xmlDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        xmlDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        xmlDatajScrollPane.setMaximumSize(new java.awt.Dimension(680, 410));
        xmlDatajScrollPane.setMinimumSize(new java.awt.Dimension(680, 410));
        xmlDatajScrollPane.setPreferredSize(new java.awt.Dimension(680, 410));

        xmlDatajTextArea.setColumns(20);
        xmlDatajTextArea.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        xmlDatajTextArea.setForeground(new java.awt.Color(51, 51, 51));
        xmlDatajTextArea.setLineWrap(true);
        xmlDatajTextArea.setRows(5);
        xmlDatajTextArea.setWrapStyleWord(true);
        xmlDatajTextArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setViewportView(xmlDatajTextArea);

        dataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        dataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        dataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 125));
        dataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 125));
        dataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 125));
        dataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        generaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        generaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        generaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml6.png"))); // NOI18N
        generaXmlDatajButton.setText("<html><body>&nbsp;Genera<br>XML Data<body><html>");
        generaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        generaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        generaXmlDatajButton.setFocusPainted(false);
        generaXmlDatajButton.setFocusable(false);
        generaXmlDatajButton.setIconTextGap(2);
        generaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        dataCommandjPanel.add(generaXmlDatajButton, gridBagConstraints);

        impostaDatiWSjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        impostaDatiWSjButton.setForeground(new java.awt.Color(51, 51, 51));
        impostaDatiWSjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/WebService5.png.png"))); // NOI18N
        impostaDatiWSjButton.setText("<html><body>&nbsp;Imposta<br>&nbsp;Dati WS<body><html>");
        impostaDatiWSjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        impostaDatiWSjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        impostaDatiWSjButton.setFocusPainted(false);
        impostaDatiWSjButton.setFocusable(false);
        impostaDatiWSjButton.setIconTextGap(1);
        impostaDatiWSjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        dataCommandjPanel.add(impostaDatiWSjButton, gridBagConstraints);

        salvaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        salvaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        salvaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml7.png"))); // NOI18N
        salvaXmlDatajButton.setText("<html><body>&nbsp;Salva<br>XML Data<body><html>");
        salvaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        salvaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        salvaXmlDatajButton.setFocusPainted(false);
        salvaXmlDatajButton.setFocusable(false);
        salvaXmlDatajButton.setIconTextGap(2);
        salvaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        dataCommandjPanel.add(salvaXmlDatajButton, gridBagConstraints);

        javax.swing.GroupLayout xmlDatajPanelLayout = new javax.swing.GroupLayout(xmlDatajPanel);
        xmlDatajPanel.setLayout(xmlDatajPanelLayout);
        xmlDatajPanelLayout.setHorizontalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        xmlDatajPanelLayout.setVerticalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout workingjPanelLayout = new javax.swing.GroupLayout(workingjPanel);
        workingjPanel.setLayout(workingjPanelLayout);
        workingjPanelLayout.setHorizontalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workingjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37))
        );
        workingjPanelLayout.setVerticalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(40, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backjButton;
    private javax.swing.JPanel commandjPanel;
    private javax.swing.JPanel dataCommandjPanel;
    private javax.swing.JButton exitjButton;
    private javax.swing.JButton generaXmlDatajButton;
    private javax.swing.JButton homejButton;
    private javax.swing.JButton impostaDatiWSjButton;
    private javax.swing.JScrollPane messageBarjScrollPane;
    private javax.swing.JTextPane messageBarjTextPane;
    private javax.swing.JLabel panelTitlejLabel;
    private javax.swing.JButton salvaXmlDatajButton;
    private javax.swing.JPanel workingjPanel;
    private javax.swing.JPanel xmlDatajPanel;
    private javax.swing.JScrollPane xmlDatajScrollPane;
    private javax.swing.JTextArea xmlDatajTextArea;
    // End of variables declaration//GEN-END:variables


}
