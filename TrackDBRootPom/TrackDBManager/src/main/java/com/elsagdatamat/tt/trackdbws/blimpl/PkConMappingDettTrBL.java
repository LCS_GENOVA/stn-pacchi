package com.elsagdatamat.tt.trackdbws.blimpl;


import org.springframework.beans.factory.annotation.Autowired;

import com.elsagdatamat.tt.trackdbws.bl.IPkConMappingDettTrBL;
import com.elsagdatamat.tt.trackdbws.dao.IPkConMappingDettTrDAO;
import com.selexelsag.tt.cruscottopacchi.bean.PkConMappingDettTr;
/** 
* R.L. BeanGenerator - 27.05.2012 18:17:43
*/ 
public class PkConMappingDettTrBL implements IPkConMappingDettTrBL {

	@Autowired
	private IPkConMappingDettTrDAO pkConMappingDettTrDAO;

	@Override
	public void insert(PkConMappingDettTr dettaglio) {
		pkConMappingDettTrDAO.insert(dettaglio);
	}

}