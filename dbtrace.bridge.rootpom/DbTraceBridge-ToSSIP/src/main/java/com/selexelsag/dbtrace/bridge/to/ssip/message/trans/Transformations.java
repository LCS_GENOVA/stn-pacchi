package com.selexelsag.dbtrace.bridge.to.ssip.message.trans;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.server.UID;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;

import com.selexelsag.dbtrace.bridge.to.ssip.manager.SSIPManagerInterface;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Header;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Traces;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Traces.Trace.WHATHAPPENEDDETAIL;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Traces.Trace.WHATHAPPENEDDETAIL.ACCNSP;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Traces.Trace.WHATHAPPENEDDETAIL.TTTOSSIP;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Traces.Trace.WHEREHAPPENEDDETAIL;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Traces.Trace.WHEREHAPPENEDDETAIL.OFFICE;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;

public class Transformations {
	
	private static final Log log = LogFactory.getLog(Transformations.class);

	private SSIPManagerInterface ssipManager;

	/**
	 * default constructor 
	 */
	public Transformations() {
		super();
	}

	/**
	 * construct Transformations object binding ssipManager 
	 * @param ssipManager  the SSIPManagerInterface object to bind to
	 */
	public Transformations(SSIPManagerInterface ssipManager) {
		this.ssipManager = ssipManager;
	}
	
	/**
	 * set ssipManager reference
	 * @param ssipManager  the OMPManagerInterface object to set
	 */
	public void setSsipManager(SSIPManagerInterface ssipManager) {
		this.ssipManager = ssipManager;
	}


	/**
	 * create and fill SSIP Jaxb Message from WS List of Trace() objects
	 * @param channel  the String value of channel id
	 * @param tracesList the WS List<Trace> response object list
	 * @return a Jaxb SSIP Message object, filled with response values
	 */
	public Message traceListToMessage(String channel, List<Trace> tracesList) {

		log.info("Creating message for " + channel + " ...");
    	NullableObjectFactoryDecorator objFactory = new NullableObjectFactoryDecorator(new ObjectFactory());
		Message ssipMessage = objFactory.createMessage();
 
		Header msgHeader = objFactory.createMessageHeader();
		msgHeader.setSERVICE("PACCHI");		
		msgHeader.setCHANNEL(TPAHeaderTransformationValues.valueOf(channel).getChannel());  
		msgHeader.setMSGID(generateSSIPUniqueID());
		msgHeader.setMSGTYPE("TPA");
		
		GregorianCalendar now = (GregorianCalendar)GregorianCalendar.getInstance();
		XMLGregorianCalendar msgDate = toXMLGregorian(now);
		
		msgHeader.setMSGDATE(msgDate);
		msgHeader.setVERSION("1.0");
		
		Traces msgTraces = objFactory.createMessageTraces();

		log.debug("Headers created; add " + tracesList.size() + " traces");

		for (Trace responseTrace : tracesList) {
			Map<String,String> traceDetailsMap = loadDetailValues(responseTrace);
			
			com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Traces.Trace msgTrace = objFactory.createMessageTracesTrace();
			
			//msgTrace.setIDDISPACCIO(objFactory.createMessageTracesTraceIDDISPACCIO(responseTrace.getIdTracedEntity()));
			msgTrace.setIDDISPACCIO(null);  // because of lacks of informations respect to expected xsd value 
			msgTrace.setTRACEDENTITY(responseTrace.getTracedEntity());
			msgTrace.setLABELTRACEDENTITY(responseTrace.getLabelTracedEntity());
			msgTrace.setWHATHAPPENED(responseTrace.getWhatHappened());
			msgTrace.setWHEREHAPPENED(TPAHeaderTransformationValues.valueOf(channel).getWhereHappened());
			msgTrace.setWHENHAPPENED(responseTrace.getWhenHappened());
			msgTrace.setIDTRACEDEXTERNAL(responseTrace.getIdTracedExternal());
			
			String idTracedEntity = (responseTrace.getIdTracedEntity() == null ? "ID_TRACED_EXTERNAL" : responseTrace.getIdTracedEntity());
			msgTrace.setIDTRACEDENTITY(idTracedEntity);

			// create WHATHAPPENEDDETAIL trace detail
			WHATHAPPENEDDETAIL whatDetail = objFactory.createMessageTracesTraceWHATHAPPENEDDETAIL();

			// TODO: eliminate if below, change to polymorph impl.
			String whatAppened = responseTrace.getWhatHappened().trim();
			if (whatAppened.equalsIgnoreCase("ACC_NSP")) {
				msgTrace.setIDSTATUS("AUP");
				
				// create ACCNSP type WHATHAPPENEDDETAIL
				ACCNSP accNspDetail = createACCNSPdetail(objFactory, traceDetailsMap); 
				
				whatDetail.setACCNSP(accNspDetail);
			} else {
				msgTrace.setIDSTATUS(tryToFindIdStatusIfNull(responseTrace, traceDetailsMap));
				msgTrace.setWHATHAPPENED("TT_TO_SSIP");

				// create TTTOSSIP type WHATHAPPENEDDETAIL
				TTTOSSIP tttossipDetail = createTTTOSSIPdetail(responseTrace, objFactory, traceDetailsMap);
				
				whatDetail.setTTTOSSIP(tttossipDetail);
			}
			// add WHATHAPPENEDDETAIL details
			msgTrace.setWHATHAPPENEDDETAIL(null);
			if (whatDetail.getACCNSP() != null || whatDetail.getTTTOSSIP() != null)
				msgTrace.setWHATHAPPENEDDETAIL(whatDetail);
			
			// create OFFICE type WHEREHAPPENEDDETAIL
			WHEREHAPPENEDDETAIL whereDetail = objFactory.createMessageTracesTraceWHEREHAPPENEDDETAIL();
			OFFICE office = objFactory.createMessageTracesTraceWHEREHAPPENEDDETAILOFFICE();
			//office.setWHEREID(TPATransformationValues.WHERE_ID.map(traceDetailsMap).value());
			office.setWHEREID(responseTrace.getIdChannel()); 			
			office.setWHEREDESCRIPTION(objFactory.createMessageTracesTraceWHEREHAPPENEDDETAILOFFICEWHEREDESCRIPTION(TPATransformationValues.WHERE_DESCRIPTION.map(traceDetailsMap).value())); 
			whereDetail.setOFFICE(office);

			// add WHEREHAPPENEDDETAIL detail
			msgTrace.setWHEREHAPPENEDDETAIL(whereDetail);
			// add detail to Message Traces
			msgTraces.getTrace().add(msgTrace);
		}

		log.debug("All traces added");

		// add header and traces to Message
		ssipMessage.setHeader(msgHeader);
		ssipMessage.setTraces(msgTraces);
 
		log.info("TPA message successfully created");
        return ssipMessage;
    }

	
	/**
	 * create TTTOSSIP sub-section Jaxb record
	 * @param objFactory  Jaxb objects creation factory
	 * @param details  Map of Trace details
	 * @return TTTOSSIP filled with details values
	 */
	private TTTOSSIP createTTTOSSIPdetail(Trace responseTrace, ObjectFactory objFactory, Map<String, String> details) {
		TTTOSSIP tttossipDetail = objFactory.createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIP();
		
		tttossipDetail.setIDSTATUS(tryToFindIdStatusIfNull(responseTrace, details));
		tttossipDetail.setDESCSTATUS(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPDESCSTATUS(TPATransformationValues.DESC_STATUS.map(details).value())); 
		tttossipDetail.setDESCCAUSALE(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPDESCCAUSALE(TPATransformationValues.DESC_CAUSALE.map(details).value())); 
		tttossipDetail.setIDCAUSALE(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPIDCAUSALE(TPATransformationValues.ID_CAUSALE.map(details).value())); 
		tttossipDetail.setNOTE(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPNOTE(TPATransformationValues.NOTE.map(details).value()));
		log.debug("... TTTOSSIP section created");
		
		return tttossipDetail;
	}

	/**
	 * create ACCNSP sub-section Jaxb record
	 * @param objFactory  Jaxb objects creation factory
	 * @param details  Map of Trace details
	 * @return ACCNSP filled with details values
	 */
	private ACCNSP createACCNSPdetail(ObjectFactory objFactory, Map<String, String> details) {
		ACCNSP accNspDetail = objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSP();
		
		accNspDetail.setVARTR(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPVARTR(TPATransformationValues.VARTR.map(details).intValue()));
		accNspDetail.setSPRD(TPATransformationValues.SPRD.map(details).value());
		accNspDetail.setNAZ(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPNAZ(TPATransformationValues.NAZ.map(details).value()));
		accNspDetail.setP(TPATransformationValues.P.map(details).intValue());
		accNspDetail.setSA(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPSA(TPATransformationValues.SA.map(details).value()));
		accNspDetail.setCNTRS(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCNTRS(TPATransformationValues.CNTRS.map(details).intValue()));
		accNspDetail.setASSIC(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPASSIC(TPATransformationValues.ASSIC.map(details).intValue()));

		accNspDetail.setIMPP(TPATransformationValues.IMPP.map(details).intValue());
		accNspDetail.setTAS(TPATransformationValues.TAS.map(details).intValue());
		accNspDetail.setPREPGID(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPPREPGID(TPATransformationValues.PREPG_ID.map(details).intValue()));
		accNspDetail.setTRB(TPATransformationValues.TRB.map(details).intValue());
		accNspDetail.setTRSA(TPATransformationValues.TRSA.map(details).intValue());
		accNspDetail.setDIRD(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDIRD(TPATransformationValues.DIRD.map(details).intValue()));
		accNspDetail.setDESTZ(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDESTZ(TPATransformationValues.DESTZ.map(details).value()));
		
		accNspDetail.setDESTR(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDESTR(TPATransformationValues.DESTR.map(details).value()));
		accNspDetail.setADDRS(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPADDRS(TPATransformationValues.ADDRS.map(details).value()));
		accNspDetail.setZIP(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPZIP(TPATransformationValues.ZIP.map(details).intValue()));
		accNspDetail.setDIST(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDIST(TPATransformationValues.DIST.map(details).value()));
		accNspDetail.setPROV(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPPROV(TPATransformationValues.PROV.map(details).value()));
		accNspDetail.setLOCALID(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPLOCALID(TPATransformationValues.LOCAL_ID.map(details).intValue())); 
		accNspDetail.setCONTID(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCONTID(TPATransformationValues.CONT_ID.map(details).intValue()));
		accNspDetail.setFSC(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPFSC(TPATransformationValues.FSC.map(details).intValue()));
		accNspDetail.setFRM(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPFRM(TPATransformationValues.FRM.map(details).intValue()));
		accNspDetail.setMRESID(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPMRESID(TPATransformationValues.MRES_ID.map(details).intValue())); 
		accNspDetail.setIRES(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPIRES(TPATransformationValues.IRES.map(details).value()));
		accNspDetail.setCODICESAP(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCODICESAP(TPATransformationValues.CODICE_SAP.map(details).value())); 
		accNspDetail.setPARTITAIVA(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPPARTITAIVA(TPATransformationValues.PARTITA_IVA.map(details).value())); 
		accNspDetail.setCODICEFISCALE(objFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCODICEFISCALE(TPATransformationValues.CODICE_FISCALE.map(details).value()));
		log.debug("... ACCNSP section created");
				
		return accNspDetail;
	}
	
	/**
	 * unmarshal a formatted XML String and parse in an SSIP Jaxb Message, applying Schema validation if present
	 * @param xmlDocument the String XML formatted document source
	 * @param schema the javax.xml.validation.Schema if present, null otherwise
	 * @return Jaxb SSIP Message object
	 * @throws JAXBException
	 */
    public Message unmarshallXmlToMessage(String xmlDocument, Schema schema) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Message.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        StringReader reader = new StringReader(xmlDocument);
        if (schema != null) 
        	unmarshaller.setSchema(schema);
        Message unmarshalledMessage = (Message) unmarshaller.unmarshal(reader);
        return unmarshalledMessage;
    } 

    /**
     * marshal an SSIP Jaxb Message object to formatted XML String, applying Schema validation if present
     * @param ssipMessage the SSIP Jaxb Message source
     * @param schema the javax.xml.validation.Schema if present, null otherwise
     * @return XML formatted String
     * @throws JAXBException
     */
    public String marshallMessageToXml(Message ssipMessage, Schema schema) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Message.class);
        Marshaller marshaller = context.createMarshaller();
        StringWriter writer = new StringWriter();
        if (schema != null) 
        	marshaller.setSchema(schema);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(ssipMessage, writer);
        return writer.toString();
    }

    /**
     * load XML Schema validation file .XSD
     * @param xsdSchemaPath full path (url) to xsd file
     * @return javax.xml.validation.Schema if a valid xsd file was loaded, null otherwise
     * @throws MalformedURLException
     * @throws SAXException
     */
	public Schema loadSchemaFrom(String xsdSchemaPath) throws MalformedURLException, SAXException {
		URL pathUrl = new URL(xsdSchemaPath);
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		return schemaFactory.newSchema(pathUrl);
	}

	/**
	 * date conversion from GregorianCalendar to XML notation
	 * @param gregorianCal  the GregorianCalendar formatted date to convert
	 * @return the XMLGregorianCalendar date formatted
	 */
	public XMLGregorianCalendar toXMLGregorian(GregorianCalendar gregorianCal) {
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCal);
		} catch (DatatypeConfigurationException e) {
			log.debug("Date conversion failed ", e);
		}	
		return null;
	}

	/**
	 * date conversion from Date to XML notation
	 * @param date  the Date to convert
	 * @return the XMLGregorianCalendar date formatted
	 */
	public XMLGregorianCalendar toXMLGregorian(Date date) {
		GregorianCalendar calendar = (GregorianCalendar)GregorianCalendar.getInstance();
		calendar.setTime(date);
		return toXMLGregorian(calendar);
	}

	/**
	 * fill a mapped details list from Trace object
	 * @param trace  Trace object top of details list
	 * @return the Map details list 
	 */
	public Map<String, String> loadDetailValues(Trace trace) {
		Map<String, String> paramsMap = new HashMap<String, String>();

		if (trace.getTraceDetailsList() != null && trace.getTraceDetailsList().getTraceDetails() != null) 
			for (TraceDetail detail : trace.getTraceDetailsList().getTraceDetails())
				paramsMap.put(detail.getParamClass(), detail.getParamValue());

		return paramsMap;
	}

	/**
	 * try to seek ID_STATUS field from trace details if absent
	 * @param seekTrace  the Trace to search in
	 * @param detailMap  Map of trace details
	 * @return  String ID_STATUS field if found, null otherwise
	 */
	public String tryToFindIdStatusIfNull(Trace seekTrace, Map<String, String> detailMap) {
		if (seekTrace.getIdStatus() == null) {
			if (detailMap.containsKey("STATUS"))
				return ssipManager.getIdStatusTranscoded(detailMap.get("STATUS"), seekTrace.getChannel());			
			if (detailMap.containsKey("ID_STATUS"))
				return ssipManager.getIdStatusTranscoded(detailMap.get("ID_STATUS"), seekTrace.getChannel());			
			if (detailMap.containsKey("DELREASON")) 
				return ssipManager.getIdStatusTranscoded(detailMap.get("DELREASON"), seekTrace.getChannel());			
			if (detailMap.containsKey("PSTF")) {
				String dfValue = (detailMap.containsKey("DF") ? detailMap.get("DF") : "");
				return ssipManager.getIdStatusTranscoded("DF_" + dfValue + detailMap.get("PSTF"), seekTrace.getChannel());
			}
		}
		return seekTrace.getIdStatus();
	}

	/**
	 * generate a unique UID, SSIP prefixed
	 * @return a String unique ID
	 */
	private String generateSSIPUniqueID() {
		String unique = (new UID()).toString();
		if (unique.length() > 45) unique = unique.substring(0,45);
		return "SSIP:" + unique;
	}
}
