package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.FromNspToCmp;
import com.elsagdatamat.dbtrace.bridge.engine.entities.FromNspToCmpPK;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface IFromNspToCmpDAO  extends IGenericDAO<FromNspToCmp, FromNspToCmpPK>{

}
