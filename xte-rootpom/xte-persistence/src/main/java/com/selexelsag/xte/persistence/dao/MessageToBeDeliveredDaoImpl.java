package com.selexelsag.xte.persistence.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.selexelsag.messagetobedelivered.entity.MessageToBeDelivered;

@Transactional
public class MessageToBeDeliveredDaoImpl implements MessageToBeDeliveredDao 
{
	@Autowired
	@Qualifier("messageToBeDeliveredSessionFactory")
    protected SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) 
    {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public void insert(MessageToBeDelivered entity) 
    {
        this.sessionFactory.getCurrentSession().save(entity);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<MessageToBeDelivered> listAll() 
    {
        Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(MessageToBeDelivered.class);
        return criteria.list();
    }
    
    @Override
    public void flushAndClear() 
    {
        this.sessionFactory.getCurrentSession().flush();
        this.sessionFactory.getCurrentSession().clear();
    }
}
