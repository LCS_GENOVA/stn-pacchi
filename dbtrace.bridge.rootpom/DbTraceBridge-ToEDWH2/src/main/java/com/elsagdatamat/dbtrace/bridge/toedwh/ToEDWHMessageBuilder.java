package com.elsagdatamat.dbtrace.bridge.toedwh;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.elsagdatamat.dbtrace.bridge.engine.MessageBuilderBase;
import com.elsagdatamat.dbtrace.bridge.engine.bl.IParameterManager;
import com.elsagdatamat.trackdb.toedwh.entities.SentTrace;
import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSend;
import com.elsagdatamat.trackdb.toedwh.manager.ISentTraceManager;
import com.elsagdatamat.trackdb.toedwh.manager.ITraceManager;
import com.elsagdatamat.trackdb.toedwh.manager.ITraceTypesToSendManager;
import com.elsagdatamat.trackdb.toedwh.vo.EdwhTracesList;
import com.elsagdatamat.trackdb.toedwh.vo.TraceVO;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public class ToEDWHMessageBuilder extends MessageBuilderBase implements ToEdwhMessageBuilderInterface {

	private static final String TOKEN_SPLITTER = ";";

	private static final String EDWH = "EDWH";

	private static final String PACCO = "PACCO";

	private static final String FILE_NAME_PARAM = "DBTraceToEDWHBridgeFileName";
	private static final String RECORD_FILEPATH_EDWH = "DBTraceToEDWHBridgeFilePath";
	private static final String FILE_SIZE_PARAM = "DBTraceToEDWHBridgeFileSize";
	private static final String RECORD_NUMBER_PARAM = "DBTraceToEDWHBridgeRecordNumber";
	private static final String RECORD_ENABLE_EDWH = "DBTraceToEDWHBridge";
	private static final String RECORD_MASSIVE_MODE = "DBTraceToEDWHBridgeMassiveMode";
	
	private static final int NUM_ID_NOT_COMMITTED = 1000;

	protected static final Log log = LogFactory.getLog(ToEDWHMessageBuilder.class);

	private IParameterManager parameterManager;

	private static Long maxFileSize;
	private static Long maxRecordNumber;
	private static String isActive;
	private static String filePath;
	private static String[] fileName;
	private static String isMassiveMode;

	private Long currentTrace = 0L;
	private BufferedWriter currentFile = null;
	private ITraceManager traceManager;

	private ITraceTypesToSendManager traceTypesToSendManager;
	private ISentTraceManager sentTraceManager;

	public ITraceManager getTraceManager() {
		return traceManager;
	}

	public void setTraceManager(ITraceManager traceManager) {
		this.traceManager = traceManager;
	}

	public ITraceTypesToSendManager getTraceTypesToSendManager() {
		return traceTypesToSendManager;
	}

	public void setTraceTypesToSendManager(ITraceTypesToSendManager traceTypesToSendManager) {
		this.traceTypesToSendManager = traceTypesToSendManager;
	}

	public ISentTraceManager getSentTraceManager() {
		return sentTraceManager;
	}

	public void setSentTraceManager(ISentTraceManager sentTraceManager) {
		this.sentTraceManager = sentTraceManager;
	}

	public IParameterManager getParameterManager() {
		return parameterManager;
	}

	public void setParameterManager(IParameterManager parameterManager) {
		this.parameterManager = parameterManager;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.elsagdatamat.dbtrace.bridge.toedwh.ToEdwhMessageBuilderInterface#
	 * create(com.selexelsag.tracktrace.trackdbws.xml.TracesList)
	 */

	@Override
	public List<String> create(TracesList input) {
		if (input instanceof EdwhTracesList) {
			return create((EdwhTracesList) input);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.elsagdatamat.dbtrace.bridge.toedwh.ToEdwhMessageBuilderInterface#
	 * create(com.elsagdatamat.dbtrace.bridge.toedwh.EdwhTracesList)
	 */
	@Override
	public List<String> create(EdwhTracesList input) {
		return create(input.getTraceList());
	}

	private List<String> create(List<TraceVO> traceList) {
		List<String> toWrite = new ArrayList<String>();
		for (TraceVO t : traceList)
			toWrite.add(CsvUtils.toCsv(t).toString());

		// logica appartenente alla send() spostata qui
		try {
			scrivifiles(toWrite);
			if (currentFile != null)
				currentFile.flush();
		} catch (Exception ex) {
			log.error(ex);
			return null; // non aggiorno le sent-traces
		}

		aggiornaSentTraces(traceList);
		return toWrite;
	}

	// IMessageSender
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.elsagdatamat.dbtrace.bridge.toedwh.ToEdwhMessageBuilderInterface#
	 * send(java.util.List)
	 */

	@Override
	/**
	 * Builds the file. It will be saved in a folder and later sent by Axway
	 */
	public void send(List<String> messageList) throws IOException {
		log.debug("Entering send");

		// logica spostata nella create perch� per aggiornare la tabella
		// sent_traces servono gli id delle tracce
		// loadParams(parameterManager);
		//
		// scrivifiles(messageList);
		//
		// aggiornaSentTraces();

		log.debug("Leaving send");
	}

	private void scrivifiles(List<String> messageList) throws Exception {

		try {
			// currentTrace = 0L;

			if (currentFile == null)
				currentFile = getBufferedWriter();
			for (String c : messageList) {
				if (currentTrace.longValue() % maxFileSize.longValue() == 0 && currentTrace > 0) {
					currentFile.close();
					currentFile = getBufferedWriter();
				}
				currentFile.write(c);
				currentFile.newLine();
				currentTrace++;
			}

		} catch (Exception e) {
			log.error("Errore nello scrivere il file", e);
			currentFile.close();
			currentFile = null;
			currentTrace = 0L;
			log.debug("File closed OK");
			throw e;
		}
	}

	private void aggiornaSentTraces(List<TraceVO> traceList) {
		// insert record in sent_trace (not always)
		if (!"T".equals(isMassiveMode)) {
			log.debug("Insert sent traces");
			insertSentTraces(traceList);
		}
	}

	@Override
	public void loadParams() {
		fileName = parameterManager.findByID(FILE_NAME_PARAM).getValue().split("[.]");
		filePath = parameterManager.findByID(RECORD_FILEPATH_EDWH).getValue();
		maxFileSize = new Long(parameterManager.findByID(FILE_SIZE_PARAM).getValue());
		maxRecordNumber = new Long(parameterManager.findByID(RECORD_NUMBER_PARAM).getValue());
		isActive = parameterManager.findByID(RECORD_ENABLE_EDWH).getValue();
		isMassiveMode = parameterManager.findByID(RECORD_MASSIVE_MODE).getValue();
	}

	private static void loadParamsMock() {
		fileName = new String[] { "toedwh", "txt" };
		filePath = ".";
		maxFileSize = 10000L;
		maxRecordNumber = 4032233730L;
		isActive = "T";
		isMassiveMode = "T";
	}

	private static String generateTempFileName(int progressivo) throws Exception {
		Calendar calendar = GregorianCalendar.getInstance();
		String giulianoDate = String.valueOf(calendar.getTimeInMillis() / 1000);
		String strProgressivo = "";
		if (progressivo != 0)
			strProgressivo = "_" + progressivo;
		String uuidFileName = filePath + fileName[0] + giulianoDate + strProgressivo + "." + fileName[1];
		return uuidFileName;
	}

	@Override
	public TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate) {
		loadParams();
		if (!"T".equals(isActive)) {
			return null;
		}

		log.debug(String.format("Entering getTraces for service '%s' (wsToken='%s'): stopDate=%s;", token.getServiceId(),
				token.getWsToken(), stopDate));
		String[] split = token.getWsToken().split(TOKEN_SPLITTER);
		Long id = new Long(split[1]);
		id = id-NUM_ID_NOT_COMMITTED;
		List<TraceVO> traces = findTraces(id, maxRecordNumber, "T".equals(isMassiveMode));
		TracesList tl = new EdwhTracesList(traces);

		// update token???
		if (traces != null && traces.size() > 0) {
			String newToken = split[0] + TOKEN_SPLITTER + getMaxId(traces);
			log.debug("Next token: " + newToken);
			token.setWsToken(newToken);
		} else {
			currentTrace = 0L;
			try {
				if (currentFile != null) {
					currentFile.flush();
					currentFile.close();
					currentFile = null;
				}

			} catch (IOException e) {
				log.error(e);
			}
		}
		return tl;
	}

	private String getMaxId(List<TraceVO> traces) {
		long max = -1;
		for (TraceVO traceVO : traces) {
			if (traceVO.getIdTrace() > max) {
				max = traceVO.getIdTrace();
			}
		}
		return "" + max;
	}

	@Override
	public List<TraceVO> findTraces(Long id, Long maxRows, Boolean allTypes) {
		List<TraceVO> traces;
		loadParams();
		if (!"T".equals(isActive)) {
			log.debug("Servizio non attivo");
			return null;
		}

		if (!allTypes) {
			List<TraceTypesToSend> findByDestinationTracedEntityChannel = traceTypesToSendManager.findByDestinationTracedEntityChannel(
					EDWH, PACCO, null);
			if (findByDestinationTracedEntityChannel != null && findByDestinationTracedEntityChannel.size() > 0) {
				traces = traceManager.findByListOfType(id, findByDestinationTracedEntityChannel, maxRows);
			} else {
				traces = null;
				log.error(String.format("No record found on trace_type_to_send for destination '%s', tracedEntity '%s'", EDWH, PACCO));
			}
		} else
			traces = traceManager.find(id, maxRows, PACCO);
		return traces;
	}

	private static BufferedWriter getBufferedWriter() throws Exception {
		int progressivo = 0;
		String generateTempFileName = ToEDWHMessageBuilder.generateTempFileName(progressivo);
		File file = new File(generateTempFileName);
		while(file.exists()){
			progressivo++;
			generateTempFileName = ToEDWHMessageBuilder.generateTempFileName(progressivo);
			file = new File(generateTempFileName);
		}
		file.createNewFile();
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		return bw;
	}

	public static void main(String[] args) throws Exception {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		ToEdwhMessageBuilderInterface toEDWHMessageBuilder = context.getBean("toEDWHBuilder", ToEdwhMessageBuilderInterface.class);
		loadParamsMock();
		maxRecordNumber = 4032233730L;
		System.currentTimeMillis();
		List<TraceVO> findTraces = toEDWHMessageBuilder.findTraces(0L, maxRecordNumber, "T".equals(isMassiveMode));

		long currentTimeMillis3 = System.currentTimeMillis();
		toEDWHMessageBuilder.insertSentTraces(findTraces);
		long currentTimeMillis4 = System.currentTimeMillis() - currentTimeMillis3;
		System.out.print("\nTime to insert sent traces :" + currentTimeMillis4 + "\n");

		System.exit(0);
	}

	@Override
	public void insertSentTraces(List<TraceVO> traces) {
		log.debug("insertSentTraces");
		List<SentTrace> listSentTraces = new ArrayList<SentTrace>();
		SentTrace st = null;
		Date now = new Date();
		for (TraceVO trace : traces) {
			st = new SentTrace();
			st.setTraceId(trace.getIdTrace());
			st.setTime(now);
			listSentTraces.add(st);
		}
		sentTraceManager.insertSentTracesBatch(listSentTraces);
		log.debug("insertSentTraces END");
	}

/**
 * termina il lavoro nel caso in cui il task venga interrotto prima di aver finito le tracce o completato il file
 * chiude il file resetta le informazioni sul file
 */
	@Override
	public void commitWork(){
		try {
			if (currentFile != null) {
				currentFile.flush();
				currentFile.close();
				currentTrace = 0L;
				currentFile = null;
			}
		} catch (IOException e) {
			log.error(e);
		}
	 }

}
