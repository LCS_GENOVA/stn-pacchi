
package com.elsagdatamat.trackdbws.client;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.elsagdatamat.tt.trackdbws.trackdbrwservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetTokenOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetTokenOutput");
    private final static QName _GetChunkSizeOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetChunkSizeOutput");
    private final static QName _GetTracesOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetTracesOutput");
    private final static QName _GetTracesByFiltersListInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetTracesByFiltersListInput");
    private final static QName _GetTokenInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetTokenInput");
    private final static QName _GetTracesInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetTracesInput");
    private final static QName _PutTracesInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "PutTracesInput");
    private final static QName _GetChunkSizeInput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetChunkSizeInput");
    private final static QName _PutTracesOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "PutTracesOutput");
    private final static QName _GetTracesByFiltersListOutput_QNAME = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "GetTracesByFiltersListOutput");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.elsagdatamat.tt.trackdbws.trackdbrwservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TracesListNextToken }
     * 
     */
    public TracesListNextToken createTracesListNextToken() {
        return new TracesListNextToken();
    }

    /**
     * Create an instance of {@link TraceDetail }
     * 
     */
    public TraceDetail createTraceDetail() {
        return new TraceDetail();
    }

    /**
     * Create an instance of {@link TracesList }
     * 
     */
    public TracesList createTracesList() {
        return new TracesList();
    }

    /**
     * Create an instance of {@link TokenTilltoDate }
     * 
     */
    public TokenTilltoDate createTokenTilltoDate() {
        return new TokenTilltoDate();
    }

    /**
     * Create an instance of {@link TokenTilltoDateByFiltersList }
     * 
     */
    public TokenTilltoDateByFiltersList createTokenTilltoDateByFiltersList() {
        return new TokenTilltoDateByFiltersList();
    }

    /**
     * Create an instance of {@link Trace }
     * 
     */
    public Trace createTrace() {
        return new Trace();
    }

    /**
     * Create an instance of {@link TraceDetailsList }
     * 
     */
    public TraceDetailsList createTraceDetailsList() {
        return new TraceDetailsList();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetTokenOutput")
    public JAXBElement<String> createGetTokenOutput(String value) {
        return new JAXBElement<String>(_GetTokenOutput_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetChunkSizeOutput")
    public JAXBElement<Integer> createGetChunkSizeOutput(Integer value) {
        return new JAXBElement<Integer>(_GetChunkSizeOutput_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TracesListNextToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetTracesOutput")
    public JAXBElement<TracesListNextToken> createGetTracesOutput(TracesListNextToken value) {
        return new JAXBElement<TracesListNextToken>(_GetTracesOutput_QNAME, TracesListNextToken.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenTilltoDateByFiltersList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetTracesByFiltersListInput")
    public JAXBElement<TokenTilltoDateByFiltersList> createGetTracesByFiltersListInput(TokenTilltoDateByFiltersList value) {
        return new JAXBElement<TokenTilltoDateByFiltersList>(_GetTracesByFiltersListInput_QNAME, TokenTilltoDateByFiltersList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetTokenInput")
    public JAXBElement<XMLGregorianCalendar> createGetTokenInput(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetTokenInput_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenTilltoDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetTracesInput")
    public JAXBElement<TokenTilltoDate> createGetTracesInput(TokenTilltoDate value) {
        return new JAXBElement<TokenTilltoDate>(_GetTracesInput_QNAME, TokenTilltoDate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TracesList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "PutTracesInput")
    public JAXBElement<TracesList> createPutTracesInput(TracesList value) {
        return new JAXBElement<TracesList>(_PutTracesInput_QNAME, TracesList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetChunkSizeInput")
    public JAXBElement<String> createGetChunkSizeInput(String value) {
        return new JAXBElement<String>(_GetChunkSizeInput_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "PutTracesOutput")
    public JAXBElement<BigInteger> createPutTracesOutput(BigInteger value) {
        return new JAXBElement<BigInteger>(_PutTracesOutput_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TracesListNextToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", name = "GetTracesByFiltersListOutput")
    public JAXBElement<TracesListNextToken> createGetTracesByFiltersListOutput(TracesListNextToken value) {
        return new JAXBElement<TracesListNextToken>(_GetTracesByFiltersListOutput_QNAME, TracesListNextToken.class, null, value);
    }

}
