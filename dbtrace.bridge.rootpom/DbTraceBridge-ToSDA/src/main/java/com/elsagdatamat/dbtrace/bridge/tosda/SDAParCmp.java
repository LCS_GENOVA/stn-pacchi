package com.elsagdatamat.dbtrace.bridge.tosda;

import java.util.Map;

import javax.xml.bind.annotation.XmlElement;

public class SDAParCmp {
	@XmlElement(name = "VETTORE")
	public String vettore;

	@XmlElement(name = "DESTINATION_TYPE")
	public String destinationType;

	@XmlElement(name = "DESTINATION_ID")
	public String destinationId;

	@XmlElement(name = "DESTINATION_DESCR")
	public String destinationDescr;

	public SDAParCmp(){}
	
	public SDAParCmp(Map<String, String> map)
	{
		String key = "COURIER";
		this.vettore = map.get(key);
		key = "DESTINATION_TYPE";
		this.destinationType = map.get(key);
		key = "OFCOTH";
		this.destinationId = map.get(key);
		key = "DESTDESCR";
		this.destinationDescr = map.get(key);
	}
}
