package com.elsagdatamat.dbtrace.bridge.jms;

import javax.ejb.EJBException;
import javax.interceptor.Interceptors;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.ejb3.annotation.Pool;
import org.jboss.ejb3.annotation.defaults.PoolDefaults;
import org.springframework.beans.factory.annotation.Autowired;

import com.elsagdatamat.dbtrace.bridge.jms.interceptor.CustomInterceptor;

@Pool(value=PoolDefaults.POOL_IMPLEMENTATION_STRICTMAX, maxSize=10)
@Interceptors(CustomInterceptor.class)
public class JmsToTraceBridgeMDB implements MessageListener {

	protected static Log log = LogFactory.getLog(JmsToTraceBridgeMDB.class);

	@Autowired
	private MessageListener messageListener;

	public void init() {
		log.info("Initializing MessageListener...");

		if (messageListener == null)
			throw new NullPointerException("MessageListener not set");

		log.info("MessageListener initialized");
	}

	@Override
	public void onMessage(Message message) {
		try {
			messageListener.onMessage(message);
		} catch (Exception e) {
			log.error("Error in onMessage() - throw new EJBException - MDB TRANSACTION IS ROLLED BACK", e);
			throw new EJBException(e);
		}
	}
}
