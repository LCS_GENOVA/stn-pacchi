package com.selexelsag.xte.model.track;

import java.io.Serializable;

import javax.jws.WebService;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Classe di ritorno della chiamata ricevuta.
 * Per compatibilita' con sistema soap precedente e' identico a quello di prima, aggiunte solo notazioni per serializzazione xml.
 * 
 * 
 * <?xml version="1.0" encoding="UTF-8"?><br />
 * <trackResponse serviceName="" channel="" channelId="" eventName="" retryFlag="S|N">
 * 		<error errorCode="Positivo|Negativo" errorDesc="Descrizione esito" errorId="Codice esito" />
 * 		<outputParam></outputParam>
 * </trackResponse>
 *
 * @author Frosi
 *
 */
@WebService
@XmlRootElement(name="trackResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class TrackResponse implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3134140925237779606L;
	@XmlAttribute(name="serviceName" )
	private String serviceName;
	@XmlAttribute(name="channel" )
	private String channel;
	@XmlAttribute(name="channelId" )
	private String destinationOfficeId;
	
	private String sourceOfficeId;
	
	private String objectId;
	@XmlAttribute(name="eventName" )
	private String eventName;
	@XmlAttribute(name="errorCode" )
	private String errorCode;	// ESITO (Positivo/Negativo)
	private String errorDesc;	// DESCRIZIONE ESITO ("bla bla bla")
	private String errorId;		// CODICE ESITO (NM999)
	private String retryFlag;	// REITERABILITA (S/N)
	private String outputParam;

	public TrackResponse() {

	}
	
	public TrackResponse(String serviceName, String channel, String channelId, String eventName) {
		this(serviceName, channel, null, channelId, null, eventName);
	}

	public TrackResponse(String serviceName, String channel, String sourceOfficeId, String destinationOfficeId, String objectId, String eventName) {
		this.serviceName = serviceName;
		this.channel = channel;
		this.sourceOfficeId = sourceOfficeId;
		this.destinationOfficeId = destinationOfficeId;
		this.objectId = objectId;
		this.eventName = eventName;
	}

	public String getErrorId() {
		return errorId;
	}

	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}

	public String getRetryFlag() {
		return retryFlag;
	}

	public void setRetryFlag(String retryFlag) {
		this.retryFlag = retryFlag;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getDestinationOfficeId() {
		return destinationOfficeId;
	}

	public void setDestinationOfficeId(String destinationOfficeId) {
		this.destinationOfficeId = destinationOfficeId;
	}

	public String getSourceOfficeId() {
		return sourceOfficeId;
	}

	public void setSourceOfficeId(String sourceOfficeId) {
		this.sourceOfficeId = sourceOfficeId;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	public String getOutputParam() {
		return outputParam;
	}

	public void setOutputParam(String outputParam) {
		this.outputParam = outputParam;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return String
				.format("TrackResponse [service=%s, channel=%s, channelId=%s, event=%s, errorCode=%s, errorDesc=%s, errorId=%s, retryFlag=%s]",
						serviceName, channel, destinationOfficeId, eventName, errorCode,
						errorDesc, errorId, retryFlag);
	}
	
	

}
