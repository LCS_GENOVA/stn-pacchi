/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.pr;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.controller.common.XteTestToolFieldValidator;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XteMailPiece;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;
import com.selexelsag.xte.testtool.view.objectmodel.XteMailPieceTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteTestToolDuplicateKeyException;
import com.selexelsag.xte.testtool.view.pr.XteTestToolNotificaMazzettiPRView;
import com.selexelsag.xte.testtool.view.pr.XteTestToolWebServicePRView;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata.NM;
import com.selexelsag.xtetesttool.xml.postaregistrata.nm.DatiInvio;
import com.selexelsag.xtetesttool.xml.postaregistrata.nm.ObjectFactory;
import com.selexelsag.xtetesttool.xml.postaregistrata.nm.Traccia;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolNotificaMazzettiPRController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener, ListSelectionListener, TableModelListener {

    private XteTestToolNotificaMazzettiPRView notificaMazzettiPRView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolNotificaMazzettiPRController.class);
    private XteMailPiece selectedMailPiece = null;
    private int currentSelectedRow = -1;
    private List<JTextComponent> mailPieceFieldList = new ArrayList<JTextComponent>();
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    
    private JAXBContext context;
    
    private static Traccia notificaMazzettiTraccia;

    static {
        logger.debug("Creating Config Object Factory and Traccia");
        PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        NM config = postaRegistrataConfig.getNM();

        notificaMazzettiTraccia = objectFactory.createTraccia();
        notificaMazzettiTraccia.setEvento(config.getEventName());
        notificaMazzettiTraccia.setHeaderMazzetto(objectFactory.createHeaderMazzetto());
        notificaMazzettiTraccia.setListaInvii(objectFactory.createListaInvii());
    }
    
    
    public XteTestToolNotificaMazzettiPRController(JPanel notificaMazzettiPRView) {
        try {
            logger.info("XteTestToolNotificaMazzettiPRController start class creation");
            this.notificaMazzettiPRView = (XteTestToolNotificaMazzettiPRView) notificaMazzettiPRView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.notificaMazzettiPRView.initializeView();
            
            updateView();
            
            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.postaregistrata.nm");
            
            logger.info("XteTestToolNotificaMazzettiPRController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolNotificaMazzettiPRController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolNotificaMazzettiPRController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Posta Registrata controller creation failed", ex);
        }

    }
    
    private void updateDataModel() {
        String id = notificaMazzettiPRView.getBundleIdTextRef().getText();
        String opid = notificaMazzettiPRView.getBundleTPIdTextRef().getText();
        logger.debug("updateDataModel - " + id + "," + opid);
        notificaMazzettiTraccia.getHeaderMazzetto().setCodiceIdentificativo(new String(id));
        notificaMazzettiTraccia.getHeaderMazzetto().setIdOperatoreTP(new String(opid));
        ObjectFactory objectFactory = new ObjectFactory();
        XteMailPiece[] entries = notificaMazzettiPRView.getMailPieceTableModelRef().getMap().values().toArray(new XteMailPiece[0]);
        logger.debug("entries count: " + entries.length);        
        notificaMazzettiTraccia.getListaInvii().getDatiInvio().clear();
        for (int i=0;i<entries.length;i++) {
        
            XteMailPiece item = entries[i];
            logger.debug("item " + i + ": " + item);
            if (item == null) 
                continue;
            DatiInvio obj = objectFactory.createDatiInvio();
            obj.setCodice(item.getMailPieceCode());
            obj.setIdCausale(item.getMailPieceProduct());
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(item.getMailPieceDateTime());
            try {
                obj.setDataOraNotifica(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            } catch (DatatypeConfigurationException ex) {
                logger.error("Error converting date " + c, ex);
                obj.setDataOraNotifica(null);
            }
            obj.setLatitudine(item.getMailPieceLatitude());
            obj.setLongitudine(item.getMailPieceLongitude());
            obj.setAltroUfficio(item.getMailPieceOffice());
            notificaMazzettiTraccia.getListaInvii().getDatiInvio().add(obj);
        }
    }
    
    
    private void updateView() {  
        String id = notificaMazzettiTraccia.getHeaderMazzetto().getCodiceIdentificativo();
        String opid = notificaMazzettiTraccia.getHeaderMazzetto().getIdOperatoreTP();
        logger.debug("updateView - " + id + "," + opid);
                
        if (id != null) {
            notificaMazzettiPRView.getBundleIdTextRef()
                    .setText(id);
        }

        if (opid != null) {
            notificaMazzettiPRView.getBundleTPIdTextRef()
                    .setText(opid);
        }

        notificaMazzettiPRView.getMailPieceTableModelRef().clearModel();
            
        for (int i=0;i<notificaMazzettiTraccia.getListaInvii().getDatiInvio().size();i++) {
            DatiInvio item = notificaMazzettiTraccia.getListaInvii().getDatiInvio().get(i);
            XteMailPiece obj = new XteMailPiece();
            obj.setMailPieceCode(item.getCodice());
            obj.setMailPieceDateTime(item.getDataOraNotifica().toGregorianCalendar().getTime());
            obj.setMailPieceLatitude(item.getLatitudine());
            obj.setMailPieceLongitude(item.getLongitudine());
            obj.setMailPieceOffice(item.getAltroUfficio());
            obj.setMailPieceProduct(item.getIdCausale());
            notificaMazzettiPRView.getMailPieceTableModelRef().addMailPieceItem(obj);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = notificaMazzettiPRView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        notificaMazzettiPRView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.notificaMazzettiPRView.getNotificaMazzettiPanelRef().addAncestorListener(this);        
        this.notificaMazzettiPRView.getBundleIdTextRef().addFocusListener(this);
        this.notificaMazzettiPRView.getBundleTPIdTextRef().addFocusListener(this);
        this.notificaMazzettiPRView.getMailPieceCodeTextRef().addFocusListener(this);
        this.notificaMazzettiPRView.getMailPieceProductTextRef().addFocusListener(this);
        this.notificaMazzettiPRView.getMailPieceDatePickerRef().getEditor().addFocusListener(this);
        JFormattedTextField startTimejFormattedTextField = ((JSpinner.DateEditor)notificaMazzettiPRView.getMailPieceTimeSpinnerRef().getEditor()).getTextField();
        startTimejFormattedTextField.addFocusListener(this);
        this.notificaMazzettiPRView.getMailPieceLatitudeTextRef().addFocusListener(this);
        this.notificaMazzettiPRView.getMailPieceLongitudeTextRef().addFocusListener(this);
        this.notificaMazzettiPRView.getMailPieceOfficeTextRef().addFocusListener(this);
        this.notificaMazzettiPRView.getMailPieceTableRef().getSelectionModel().addListSelectionListener(this);
        this.notificaMazzettiPRView.getMailPieceTableRef().getModel().addTableModelListener(this);
        this.notificaMazzettiPRView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.notificaMazzettiPRView.getAggiungiItemButtonRef().addActionListener(this);
        this.notificaMazzettiPRView.getEliminaItemButtonRef().addActionListener(this);
        this.notificaMazzettiPRView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.notificaMazzettiPRView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.notificaMazzettiPRView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.notificaMazzettiPRView.getBackButtonRef().addActionListener(this);
        this.notificaMazzettiPRView.getHomeButtonRef().addActionListener(this);
        this.notificaMazzettiPRView.getExitButtonRef().addActionListener(this);
        
        
        this.notificaMazzettiPRView.getMailPieceCodeTextRef().getDocument().addDocumentListener(this);
        this.notificaMazzettiPRView.getMailPieceProductTextRef().getDocument().addDocumentListener(this);
        this.notificaMazzettiPRView.getMailPieceDatePickerRef().getEditor().getDocument().addDocumentListener(this);
        ((JSpinner.DateEditor)notificaMazzettiPRView.getMailPieceTimeSpinnerRef().getEditor()).getTextField().getDocument().addDocumentListener(this);
        this.notificaMazzettiPRView.getMailPieceLatitudeTextRef().getDocument().addDocumentListener(this);
        this.notificaMazzettiPRView.getMailPieceLongitudeTextRef().getDocument().addDocumentListener(this);
        this.notificaMazzettiPRView.getMailPieceOfficeTextRef().getDocument().addDocumentListener(this);
        
        mailPieceFieldList.add(notificaMazzettiPRView.getMailPieceCodeTextRef());
        mailPieceFieldList.add(notificaMazzettiPRView.getMailPieceProductTextRef());
        mailPieceFieldList.add(notificaMazzettiPRView.getMailPieceDatePickerRef().getEditor());
        mailPieceFieldList.add(((JSpinner.DateEditor)notificaMazzettiPRView.getMailPieceTimeSpinnerRef().getEditor()).getTextField());
        mailPieceFieldList.add(notificaMazzettiPRView.getMailPieceLatitudeTextRef());
        mailPieceFieldList.add(notificaMazzettiPRView.getMailPieceLongitudeTextRef());
        mailPieceFieldList.add(notificaMazzettiPRView.getMailPieceOfficeTextRef());
        
		this.salvaXmlFileChooser.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(notificaMazzettiPRView.getAggiungiItemButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiPRController actionPerformed invoke addMailPieceItem");
			
			updateDataModel();

            addMailPieceItem();
        }
        
        if(source.equals(notificaMazzettiPRView.getEliminaItemButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiPRController actionPerformed invoke removeMailPieceItem");

			updateDataModel();

            removeMailPieceItem();
        }

        if(source.equals(notificaMazzettiPRView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiPRController actionPerformed invoke generaXmlData");

			updateDataModel();

            generaXmlData();
        }

        if(source.equals(notificaMazzettiPRView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiPRController actionPerformed invoke impostaDatiWebService");

			updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(notificaMazzettiPRView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiPRController actionPerformed invoke salvaXmlData");

			updateDataModel();

            salvaXmlData();
        }

        if(source.equals(notificaMazzettiPRView.getBackButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiPRController actionPerformed invoke back");

			updateDataModel();

            back();
        }

        if(source.equals(notificaMazzettiPRView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiPRController actionPerformed invoke home");

			updateDataModel();

            home();
        }

        if(source.equals(notificaMazzettiPRView.getExitButtonRef()))
        {
            logger.debug("XteTestToolNotificaMazzettiPRController actionPerformed invoke exit");
			
			updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(notificaMazzettiPRView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolNotificaMazzettiPRView)
        {
            logger.debug("XteTestToolNotificaMazzettiPRController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiPRController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Posta Registrata controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiPRController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Posta Registrata controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        checkMailPieceFieldsFull();  
    }    
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object source = e.getSource();
        if(source.equals(notificaMazzettiPRView.getMailPieceTableRef().getSelectionModel()))
        {               
            if (e.getValueIsAdjusting()) {
                    return;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
                        currentSelectedRow = notificaMazzettiPRView.getMailPieceTableRef().getSelectedRow();
                        logger.debug("XteTestToolNotificaMazzettiPRController valueChanged currentSelectedRow = " + currentSelectedRow);
                        logger.debug("XteTestToolNotificaMazzettiPRController valueChanged: " + ((XteMailPieceTableModel)notificaMazzettiPRView.getMailPieceTableRef().getModel()).getMap().size());
                        if(currentSelectedRow!=-1)
                        {
                            List<Component> enableComponents = new ArrayList<Component>();
                            enableComponents.add(notificaMazzettiPRView.getEliminaItemButtonRef());
                            componentStatusManager.enableComponent(enableComponents);
                            selectedMailPiece = ((XteMailPieceTableModel)notificaMazzettiPRView.getMailPieceTableRef().getModel()).getSelectedItem(currentSelectedRow);
                        }
                        else
                        {
                            List<Component> disableComponents = new ArrayList<Component>();
                            disableComponents.add(notificaMazzettiPRView.getEliminaItemButtonRef());
                            componentStatusManager.disableComponent(disableComponents);                                                        
                            selectedMailPiece = null;
                        }
                    }
                } );
        }
        
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int eventType = e.getType();
        DefaultTableModel model = (DefaultTableModel) e.getSource();
        if(model.equals(notificaMazzettiPRView.getMailPieceTableRef().getModel()))
        {               
            logger.debug("XteTestToolNotificaMazzettiPRController tableChanged: " + ((XteMailPieceTableModel)notificaMazzettiPRView.getMailPieceTableRef().getModel()).getMap().size());
        }
    }

    private void checkMailPieceFieldsFull() {

        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

        for (JTextComponent field : mailPieceFieldList) {
            if (field.getText().trim().isEmpty()) {
                List<Component> disableComponents = new ArrayList<Component>();
                disableComponents.add(notificaMazzettiPRView.getAggiungiItemButtonRef());
                componentStatusManager.disableComponent(disableComponents);                                                        
                return;
            }
        }
        List<Component> enableComponents = new ArrayList<Component>();
        enableComponents.add(notificaMazzettiPRView.getAggiungiItemButtonRef());
        componentStatusManager.enableComponent(enableComponents);
    }

    private void clearMailPieceFields() {

        for (JTextComponent field : mailPieceFieldList) {
            if (!field.getText().trim().isEmpty()) {
                field.setText("");
            }
        }
    }
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        notificaMazzettiPRView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolNotificaMazzettiPRController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.notificaMazzettiPRView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(notificaMazzettiPRView);
            updateXteTestToolDataModel(dataModel);

            clearMailPieceFields();
            clearMessageBar();

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(notificaMazzettiPRView.getAggiungiItemButtonRef());
            disabledComponents.add(notificaMazzettiPRView.getEliminaItemButtonRef());
            disabledComponents.add(notificaMazzettiPRView.getImpostaDatiWSButtonRef());
            disabledComponents.add(notificaMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            Calendar date = Calendar.getInstance();
            this.notificaMazzettiPRView.getMailPieceDatePickerRef().setDate(date.getTime());
            this.notificaMazzettiPRView.getMailPieceTimeSpinnerRef().setValue(date.getTime());

            /*
            XteMailPiece item = new XteMailPiece();
            item.setMailPieceCode("1234567890121");
            item.setMailPieceProduct("Racc");
            
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss.SSS");
            //SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            
            Date startDate = this.notificaMazzettiPRView.getMailPieceDatePickerRef().getDate();

            SpinnerDateModel startTimeSpinnerDateModel = (SpinnerDateModel) this.notificaMazzettiPRView.getMailPieceTimeSpinnerRef().getModel();
            Date startTime = startTimeSpinnerDateModel.getDate();

            String startDateStr = dateFormatter.format(startDate);
            String startTimeStr = timeFormatter.format(startTime);
            String startDateTimeStr = startDateStr + "T" + startTimeStr;
            GregorianCalendar c = new GregorianCalendar();
            item.setMailPieceDateTime(startDate);
            item.setMailPieceLatitude(123.12f);
            item.setMailPieceLongitude(12.2f);
            item.setMailPieceOffice("Altro ufficio 1");

            XteMailPiece item2 = new XteMailPiece();
            item2.setMailPieceCode("1234567890122");
            item2.setMailPieceProduct("Racc");                        
            item2.setMailPieceDateTime(startDate);
            item2.setMailPieceLatitude(123.12f);
            item2.setMailPieceLongitude(12.2f);
            item2.setMailPieceOffice("Altro ufficio 2");

            XteMailPiece item3 = new XteMailPiece();
            item3.setMailPieceCode("1234567890123");
            item3.setMailPieceProduct("Racc");                        
            item3.setMailPieceDateTime(startDate);
            item3.setMailPieceLatitude(123.123f);
            item3.setMailPieceLongitude(12.2f);
            item3.setMailPieceOffice("Altro ufficio 3");
            
            this.notificaMazzettiPRView.getMailPieceTableModelRef().addMailPieceItem(item);
            this.notificaMazzettiPRView.getMailPieceTableModelRef().addMailPieceItem(item2);
            this.notificaMazzettiPRView.getMailPieceTableModelRef().addMailPieceItem(item3);
            
            */
            notificaMazzettiPRView.getBundleIdTextRef().requestFocus();

            logger.info("XteTestToolNotificaMazzettiPRController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolNotificaMazzettiPRController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiPRController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Posta Registrata controller init failed", ex);
        }

    }
    
    private void exit()
    {
        logger.info("XteTestToolNotificaMazzettiPRController start exit");

        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(notificaMazzettiPRView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolNotificaMazzettiPRController exit in progress");
            notificaMazzettiPRView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolNotificaMazzettiPRController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(notificaMazzettiPRView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(notificaMazzettiPRView.getNotificaMazzettiPanelRef().getParent(), notificaMazzettiPRView.getNotificaMazzettiPanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolNotificaMazzettiPRController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(notificaMazzettiPRView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(notificaMazzettiPRView.getNotificaMazzettiPanelRef().getParent(), notificaMazzettiPRView.getNotificaMazzettiPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private void addMailPieceItem()
    {
        logger.info("XteTestToolNotificaMazzettiPRController start addMailPieceItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            
            clearMessageBar();
            
            XteTestToolMessageBar messageBar = new XteTestToolMessageBar();

            XteTestToolFieldValidator validator = new XteTestToolFieldValidator();            
            boolean isLatitude = validator.validateLatitude(notificaMazzettiPRView.getMailPieceLatitudeTextRef().getText());
            if(!isLatitude)
            {                
                StyledDocument document = messageBar.putContentOnMessageBar("Latitude incorrect format!", MessageType.WARNING);
                notificaMazzettiPRView.getMessageBarTextPaneRef().setDocument(document);
                notificaMazzettiPRView.getMailPieceLatitudeTextRef().requestFocus();
                return;
            }
            
            boolean isLongitude = validator.validateLongitude(notificaMazzettiPRView.getMailPieceLongitudeTextRef().getText());
            if(!isLongitude)
            {
                StyledDocument document = messageBar.putContentOnMessageBar("Longitude incorrect format!", MessageType.WARNING);
                notificaMazzettiPRView.getMessageBarTextPaneRef().setDocument(document);
                notificaMazzettiPRView.getMailPieceLongitudeTextRef().requestFocus();
                return;
            }
                                    
            XteMailPiece item = new XteMailPiece();
            item.setMailPieceCode(notificaMazzettiPRView.getMailPieceCodeTextRef().getText());
            item.setMailPieceProduct(notificaMazzettiPRView.getMailPieceProductTextRef().getText());
            
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss.SSS");
            //SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");            
            Date startDate = this.notificaMazzettiPRView.getMailPieceDatePickerRef().getDate();
            SpinnerDateModel startTimeSpinnerDateModel = (SpinnerDateModel) this.notificaMazzettiPRView.getMailPieceTimeSpinnerRef().getModel();
            Date startTime = startTimeSpinnerDateModel.getDate();
            String startDateStr = dateFormatter.format(startDate);
            String startTimeStr = timeFormatter.format(startTime);
            String startDateTimeStr = startDateStr + "T" + startTimeStr;   
            SimpleDateFormat lasttimeFormatter = new SimpleDateFormat("yyyy-MM-dd" + "'T'" + "HH:mm:ss");
            item.setMailPieceDateTime((Date) lasttimeFormatter.parse(startDateTimeStr));
            
            DecimalFormat numberFormat = new DecimalFormat();
            
            
            Float latitude = null;
            
            try {
                latitude = numberFormat.parse(notificaMazzettiPRView.getMailPieceLatitudeTextRef().getText()).floatValue();
            }catch (NumberFormatException nfe) {
                logger.error("invalid value for float " + notificaMazzettiPRView.getMailPieceLatitudeTextRef().getText(), nfe);                
            }
            item.setMailPieceLatitude(latitude);

            Float longitude = null;
            try {
                longitude = numberFormat.parse(notificaMazzettiPRView.getMailPieceLongitudeTextRef().getText()).floatValue();
            }catch (NumberFormatException nfe) {
                logger.error("invalid value for float " + notificaMazzettiPRView.getMailPieceLongitudeTextRef().getText(), nfe);                
            }
            item.setMailPieceLongitude(longitude);

            item.setMailPieceOffice(notificaMazzettiPRView.getMailPieceOfficeTextRef().getText());

            try 
            {
                this.notificaMazzettiPRView.getMailPieceTableModelRef().addMailPieceItem(item);
            }
            catch (XteTestToolDuplicateKeyException duplicateEx)
            {
                StyledDocument document = messageBar.putContentOnMessageBar("Attemp to insert duplicate key Item!", MessageType.WARNING);
                notificaMazzettiPRView.getMessageBarTextPaneRef().setDocument(document);
                this.notificaMazzettiPRView.getMailPieceCodeTextRef().requestFocus();
                return;
            }
            
            StyledDocument document = messageBar.putContentOnMessageBar("Item added successfully!", MessageType.CORRECT_PROCESSING);
            notificaMazzettiPRView.getMessageBarTextPaneRef().setDocument(document);
                        
            clearMailPieceFields();
            notificaMazzettiPRView.getMailPieceCodeTextRef().requestFocus();            
            
            logger.info("XteTestToolNotificaMazzettiPRController addMailPieceItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolNotificaMazzettiPRController addMailPieceItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiPRController addMailPieceItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Posta Registrata controller addMailPieceItem failed", ex);
        }
    }
    
    private void removeMailPieceItem()
    {
        logger.info("XteTestToolNotificaMazzettiPRController start removeMailPieceItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            if(currentSelectedRow!=-1)
            {                
                XteMailPieceTableModel model = ((XteMailPieceTableModel)notificaMazzettiPRView.getMailPieceTableRef().getModel());
                model.removeRow(currentSelectedRow);
                logger.debug("XteTestToolNotificaMazzettiPRController removeMailPieceItem: " + ((XteMailPieceTableModel)notificaMazzettiPRView.getMailPieceTableRef().getModel()).getMap().size());
            }

            logger.info("XteTestToolNotificaMazzettiPRController removeMailPieceItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolNotificaMazzettiPRController removeMailPieceItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiPRController removeMailPieceItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Posta Registrata controller removeMailPieceItem failed", ex);
        }
    }

	private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }

    private void generaXmlData()
    {
        logger.info("XteTestToolNotificaMazzettiPRController start generaXmlData");

        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();

        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            notificaMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(notificaMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(notificaMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            notificaMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(notificaMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(notificaMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(notificaMazzettiTraccia, out);
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            notificaMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(notificaMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(notificaMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
            
        try
        {            
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString());
            notificaMazzettiPRView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("XteTestToolNotificaMazzettiPRController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolNotificaMazzettiPRController generaXmlData failed", baseEx);
			StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(baseEx.toString(), MessageType.ERROR);
            notificaMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(notificaMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(notificaMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiPRController generaXmlData failed", ex);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            notificaMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(notificaMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(notificaMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        notificaMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(notificaMazzettiPRView.getImpostaDatiWSButtonRef());
        enableComponent.add(notificaMazzettiPRView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolNotificaMazzettiPRController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServicePRView(notificaMazzettiPRView.getParentFrame());
            XteTestToolWebServicePRController controller = new XteTestToolWebServicePRController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
            NM config = postaRegistrataConfig.getNM();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(notificaMazzettiPRView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(notificaMazzettiPRView.getNotificaMazzettiPanelRef().getParent(), notificaMazzettiPRView.getNotificaMazzettiPanelRef(), webserviceview, new Rectangle(0,70,1000,690));

            logger.info("XteTestToolNotificaMazzettiPRController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolNotificaMazzettiPRController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiPRController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Posta Registrata controller impostaDatiWebService failed", ex);
        }
    }
    
    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolNotificaMazzettiPRController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolNotificaMazzettiPRController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(notificaMazzettiPRView.getParentFrame());

            logger.info("XteTestToolNotificaMazzettiPRController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolNotificaMazzettiPRController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolNotificaMazzettiPRController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Notifica Mazzetti Posta Registrata controller salvaXmlData failed", ex);
        }
    }
}
