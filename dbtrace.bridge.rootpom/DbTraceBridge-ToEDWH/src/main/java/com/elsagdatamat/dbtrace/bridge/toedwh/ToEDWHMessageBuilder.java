package com.elsagdatamat.dbtrace.bridge.toedwh;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTP;

import com.elsagdatamat.dbtrace.bridge.engine.MessageBuilderBase;
import com.elsagdatamat.dbtrace.bridge.engine.bl.ICodiciProdottoPICodiciProdottoSDAManager;
import com.elsagdatamat.dbtrace.bridge.engine.bl.ITagToConvertSubPManager;
import com.elsagdatamat.dbtrace.bridge.engine.bl.IConsumerProductManager;
import com.elsagdatamat.dbtrace.bridge.engine.entities.ConsumerProduct;
import com.elsagdatamat.framework.io.StringInputStream;
import com.elsagdatamat.framework.utils.SimpleFtpClient;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TokenTillToDateForEsiPack;
import com.selexelsag.tracktrace.trackdbws.xml.TokenTilltoDateByFiltersList;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesListNextToken;
import com.selexelsag.tracktrace.trackdbws.xml.TypeSelector;

public class ToEDWHMessageBuilder extends MessageBuilderBase{
	protected static final Log log = LogFactory.getLog(ToEDWHMessageBuilder.class);

	private SimpleFtpClient ftpClient;

	public void setFtpClient(SimpleFtpClient ftpClient) {
		this.ftpClient = ftpClient;
	}

	private String ftpWorkingDirectory = "/";

	public void setFtpWorkingDirectory(String ftpWorkingDirectory) {
		this.ftpWorkingDirectory = ftpWorkingDirectory;
	}

	private String ftpFileName;

	public void setFtpFileName(String ftpFileName) {
		this.ftpFileName = ftpFileName;
	}

	/* SCOTT Watermark */
	private Long watermark;

	public void setWatermark(Long watermark) {
		this.watermark = watermark;
	}

	private String messageType;

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	private IConsumerProductManager consumerProductManager;

	public void setConsumerProductManager(IConsumerProductManager consumerProductManager) {
		this.consumerProductManager = consumerProductManager;
	}

	private ICodiciProdottoPICodiciProdottoSDAManager codiciProdottoPICodiciProdottoSDAManager;


	public void setCodiciProdottoPICodiciProdottoSDAManager(
			ICodiciProdottoPICodiciProdottoSDAManager codiciProdottoPICodiciProdottoSDAManager) {
		this.codiciProdottoPICodiciProdottoSDAManager = codiciProdottoPICodiciProdottoSDAManager;
	}

	// R.L. - 2012.06.15 - Gestione tabella configurazione per l'associazione tipologia traccia, tag da caricare 
	private ITagToConvertSubPManager codPIInSDAMAnager;
	
	public ITagToConvertSubPManager getCodPIInSDAMAnager() {
		return codPIInSDAMAnager;
	}

	public void setCodPIInSDAMAnager(ITagToConvertSubPManager codPIInSDAMAnager) {
		this.codPIInSDAMAnager = codPIInSDAMAnager;
	}


	@Override
	public List<String> create(TracesList input) {
		log.debug("Entering create");
		String outStr = null;
		List<String> sList = new ArrayList<String>();
		
		try {
			if (messageType.equals("EsiPack")) {
				log.debug("Calling convertToEsiPack");
				//long startDate = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
				//OLD EDWHEsiPack edwhEsiPack = EDWHConversionUtility.convertToEsiPack(input.getTraces());
				
				
				EDWHEsiPack edwhEsiPack = EDWHConversionUtility.convertToEsiPack(input.getTraces(), codiciProdottoPICodiciProdottoSDAManager); 
				//log.debug("convertToEsiPack returned. Seconds elapsed: " + (Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis() - startDate)/1000);
				//log.debug("Calling edwhEsiPack.toCsv");
				//startDate = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
				outStr = edwhEsiPack.toCsv();
				//log.debug("edwhEsiPack.toCsv. Seconds elapsed: " + (Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis() - startDate)/1000);
			} else if (messageType.equals("EsiPock")) {
				log.debug("Calling convertToEsiPock");
				EDWHEsiPock edwhEsiPock = EDWHConversionUtility.convertToEsiPock(input.getTraces(), codiciProdottoPICodiciProdottoSDAManager); 
				outStr = edwhEsiPock.toCsv();
			} else if (messageType.equals("FattPack")) {
				log.debug("Calling convertToFattPack");
				EDWHFattPack edwhFattPack = EDWHConversionUtility.convertToFattPack(input.getTraces());
				outStr = edwhFattPack.toCsv();
			} else if (messageType.equals("FattPock")) {
				log.debug("Calling convertToFattPock");
				EDWHFattPock edwhFattPock = EDWHConversionUtility.convertToFattPock(input.getTraces());
				outStr = edwhFattPock.toCsv();
			} else if (messageType.equals("Stati")) {
				log.debug("Calling convertToStati");
				EDWHStati edwhStati = EDWHConversionUtility.convertToStati(input.getTraces());
				outStr = edwhStati.toCsv();
			} else {
				log.error("Errore: messaggio non previsto: " + messageType);
			}

			if (isSaveTraces()) {
				discardedTraceManager.saveDiscardedTraces(token.getServiceId(), outStr, null);
			}
			sList.add(outStr);

		} catch (Exception e) {
			discardedTraceManager.saveDiscardedTraces( token.getServiceId(), outStr, e);
			return sList;
		}
		return sList;
	}

	// IMessageSender
	@Override
	public void send(final List<String> strList) throws IOException {
		log.debug("Entering send");
		InputStream inputStream = new StringInputStream(strList.get(0));

		Calendar calendar = GregorianCalendar.getInstance();
		String giulianoDate = String.valueOf(calendar.getTimeInMillis() / 1000);

		String tmpUuidFileName = ftpFileName.substring(0, ftpFileName.lastIndexOf(".")) + giulianoDate;
		String uuidFileName = tmpUuidFileName + ftpFileName.substring(ftpFileName.lastIndexOf("."));

		ftpClient.createNewDefaultConnectionFromParameters();
		if (!ftpClient.isConnected())
			throw new RuntimeException("Connessione fallita. ClientFtp non connesso.");

		ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
		ftpClient.upload(inputStream, uuidFileName, ftpWorkingDirectory);
		ftpClient.destroyConnection();

		log.debug("Leaving send");
	}


	private TracesList getTracesOld(TrackDBRWServicePortType twsp,  Date stopDate) {
		log.debug(String.format("Entering getTracesOld for service '%s' (wsToken='%s'): stopDate=%s;", token.getServiceId(), token.getWsToken(), stopDate));
		// Costruisco il parametro per la richiesta delle tracce
		// Voglio tutte le tracce a partire dal token precedentemente ottenuto
		// fino a stopDate (passato dal motore che lo inizializza una volta per
		// tutte)....
		
		TracesList traceEqualList = new TracesList();

		
		List<ConsumerProduct> involvedProductList = consumerProductManager.listInvolvedProducts(token.getServiceId());
		if (messageType.equals("EsiPack")) {
			for (ConsumerProduct product : involvedProductList) {
				Trace traceEqual = new Trace();
				traceEqual.setTracedEntity("PACCO");
				traceEqual.setChannel("SDA");
				traceEqual.setWhatHappened("ESI_SDA");
				traceEqual.setLabelTracedEntity(product.getProductType());
				traceEqualList.getTraces().add(traceEqual);
				traceEqual = new Trace();
				traceEqual.setTracedEntity("PACCO");
				traceEqual.setChannel("SDA");
				traceEqual.setWhatHappened("RIT_SDA");
				traceEqual.setLabelTracedEntity(product.getProductType());
				traceEqualList.getTraces().add(traceEqual);
			}
		} else if (messageType.equals("EsiPock")) {
			for (ConsumerProduct product : involvedProductList) {
				Trace traceEqual = new Trace();
				traceEqual.setTracedEntity("PACCO");
				traceEqual.setChannel("SDA");
				traceEqual.setWhatHappened("ESI_SDA");
				traceEqual.setLabelTracedEntity(product.getProductType());
				traceEqualList.getTraces().add(traceEqual);
				traceEqual = new Trace();
				traceEqual.setTracedEntity("PACCO");
				traceEqual.setChannel("SDA");
				traceEqual.setWhatHappened("RIT_SDA");
				traceEqual.setLabelTracedEntity(product.getProductType());
				traceEqualList.getTraces().add(traceEqual);
			}
		} else if (messageType.equals("FattPack")) {
			for (ConsumerProduct product : involvedProductList) {
				Trace traceEqual = new Trace();
				traceEqual.setTracedEntity("PACCO");
				traceEqual.setChannel("SDA");
				traceEqual.setWhatHappened("FAT_SDA");
				traceEqual.setLabelTracedEntity(product.getProductType());
				traceEqualList.getTraces().add(traceEqual);
			}
		} else if (messageType.equals("FattPock")) {
			for (ConsumerProduct product : involvedProductList) {
				Trace traceEqual = new Trace();
				traceEqual.setTracedEntity("PACCO");
				traceEqual.setChannel("SDA");
				traceEqual.setWhatHappened("FAT_SDA");
				traceEqual.setLabelTracedEntity(product.getProductType());
				traceEqualList.getTraces().add(traceEqual);
			}
		} else if (messageType.equals("Stati")) {
			return null;
		} else {
			log.error(String.format("Errore: messaggio non previsto: serviceId='%s'; wsToken='%s'; messageType='%s';", token.getServiceId(), token.getWsToken(), messageType));
			return null;
		}

		TokenTilltoDateByFiltersList getTracesInput = new TokenTilltoDateByFiltersList();
		getTracesInput.setTilltoDate(getGCalendar(stopDate));

		getTracesInput.setTracesFilterByExampleEqual(traceEqualList);
		TracesList tracesList = null;

		while (true/* someTracePresent */) {
			getTracesInput.setToken(token.getWsToken());
			logGetTracesInput(getTracesInput);

			log.debug("Calling web service getTraces");
			TracesListNextToken tlnt = twsp.getTracesByFiltersList(getTracesInput);
			token.setWsToken(tlnt.getNextToken());
			log.debug("Called web service getTraces");

			blToken.updateAccessDate(token);
			log.debug("AccessDate updated");

			if ((tlnt != null) && (tlnt.getTraceList() != null) && (tlnt.getTraceList().getTraces() != null)
					&& (tlnt.getTraceList().getTraces().size() != 0)) {
				log.debug("Found traces n. " + tlnt.getTraceList().getTraces().size());
				if (tracesList == null) {
					tracesList = tlnt.getTraceList();
				} else {
					// append delle tracce
					tracesList.getTraces().addAll(tlnt.getTraceList().getTraces());
				}
				if (tracesList.getTraces().size() >= this.watermark) {
					log.debug("watermark [" + this.watermark + "] reached!");
					break;
				}
			} else {
				log.debug("No traces found");
				break;
			}
		}

		// Prese le tracce, aggiorno il wsToken dentro lo SDAToken

		return tracesList;
	}

	@Override
	public TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate) {
		// Costruisco il parametro per la richiesta delle tracce
		// Voglio tutte le tracce a partire dal token precedentemente ottenuto
		// fino a stopDate (passato dal motore che lo inizializza una volta per
		// tutte)....
		
		log.debug(String.format("Entering getTraces for service '%s' (wsToken='%s'): stopDate=%s;", token.getServiceId(), token.getWsToken(), stopDate));
		
		TokenTillToDateForEsiPack getEsiPacksInput = new TokenTillToDateForEsiPack();
		
		if (messageType.equals("EsiPack")) 
		{
			getEsiPacksInput.setTypeSelector(TypeSelector.SDA_ONLY);
		}
		else if (messageType.equals("EsiPock"))
		{
			getEsiPacksInput.setTypeSelector(TypeSelector.POSTE_ITALIANE);
		}
		else if (messageType.equals("FattPack")) 
		{
			//TODO
			return getTracesOld(twsp,stopDate);
		} 
		else if (messageType.equals("FattPock")) 
		{
			//TODO
			return getTracesOld(twsp,stopDate);
		} 
		else if (messageType.equals("Stati")) 
		{
			//TODO
			return null;
		} 
		else {
			log.error("Errore: messaggio non previsto: " + messageType);
			return null;
		}

		getEsiPacksInput.setTillToDate(getGCalendar(stopDate));
		
		TracesList tracesList = null;

		while (true/* someTracePresent */) {
			getEsiPacksInput.setToken( token.getWsToken());

			log.debug("Calling web service getTraces");
			TracesListNextToken tlnt = twsp.getEsiPacks(getEsiPacksInput);
			token.setWsToken(tlnt.getNextToken());
			log.debug("Called web service getTraces");

			blToken.updateAccessDate( token);
			log.debug("AccessDate updated");

			if ((tlnt != null) && (tlnt.getTraceList() != null) && (tlnt.getTraceList().getTraces() != null)
					&& (tlnt.getTraceList().getTraces().size() != 0)) {
				log.debug("Found traces n. " + tlnt.getTraceList().getTraces().size());
				if (tracesList == null) {
					tracesList = tlnt.getTraceList();
				} else {
					// append delle tracce
					tracesList.getTraces().addAll(tlnt.getTraceList().getTraces());
				}
				if (tracesList.getTraces().size() >= this.watermark) {
					log.debug("watermark [" + this.watermark + "] reached!");
					break;
				}
			} else {
				log.debug("No traces found");
				break;
			}
		}

		// Prese le tracce, aggiorno il wsToken dentro lo SDAToken
		return tracesList;
	}


	
}
