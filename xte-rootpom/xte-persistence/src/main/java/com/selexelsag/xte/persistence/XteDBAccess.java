package com.selexelsag.xte.persistence;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.xte.persistence.dao.EventsDAO;
import com.selexelsag.xte.persistence.dao.OfficesDAO;
import com.selexelsag.xte.persistence.dao.ServicesDAO;
import com.selexelsag.xte.persistence.entities.Events;
import com.selexelsag.xte.persistence.entities.Offices;
import com.selexelsag.xte.persistence.entities.OfficesId;
import com.selexelsag.xte.persistence.entities.Services;
import com.selexelsag.xte.persistence.exceptions.PersistenceException;

public class XteDBAccess
{
	public static final String HOST_NAME = "HOST_NAME";
	public static final String IP = "IP";
	public static final String OFFICE_ID = "OFFICE_ID";
	public static final String URL = "URL";
	public static final String FORWARD_TO_INT = "FORWARD_TO_INT";

	private static final Log logger = LogFactory.getLog(XteDBAccess.class);

	@Autowired
	private EventsDAO eventsDAO;
	public void setEventsDAO(EventsDAO eventsDAO) {this.eventsDAO = eventsDAO;}
	public EventsDAO getEventsDAO() {return eventsDAO;}

	@Autowired
	private ServicesDAO servicesDAO;
	public ServicesDAO getServicesDAO() {return servicesDAO;}
	public void setServicesDAO(ServicesDAO servicesDAO) {this.servicesDAO = servicesDAO;}

	@Autowired
	private OfficesDAO officesDAO;
	public OfficesDAO getOfficesDAO() {return officesDAO;}
	public void setOfficesDAO(OfficesDAO officesDAO) {this.officesDAO = officesDAO;}

	public Map<String, String> getFromOFFICES(String officeId) throws PersistenceException
	{
		// SELECT HOSTNAME, IP_HOST, OFF_REF_ID FROM OFFICES WHERE OFFICE_ID = ? AND FLAG_VALID='V'
		Offices example = new Offices();
		example.setId(new OfficesId());
		example.getId().setOfficeId(officeId);
		example.setFlagValid("V");
		List<Offices> list = officesDAO.findByExample(example);
		if (list.size()> 1)
		{
			PersistenceException pe = new PersistenceException("instanze multiple su OFFICE con id " + officeId);
			throw pe;
		}
		else if (list.size()== 0)
		{
			PersistenceException pe = new PersistenceException("nessuna instanza su OFFICE con id " + officeId);
			throw pe;
		}
		Offices entity = list.get(0);

		Map<String, String> result = new HashMap<String, String>();
		result.put(HOST_NAME, entity.getHostname());
		result.put(IP, entity.getIpHost());
		result.put(OFFICE_ID, entity.getOffRefId());

		return result;
	}

	public Map<String, String> getFromSERVICES(String serviceId) throws PersistenceException
	{
		// SELECT HOSTNAME, IP_HOST FROM SERVICES WHERE SERVICE_ID = ?
		Services example = new Services();
		example.setServiceId(serviceId);
		List<Services> list = servicesDAO.findByExample(example);
		if (list.size()> 1)
		{
			PersistenceException pe = new PersistenceException("instanze multiple su SERVICES con id " + serviceId);
			throw pe;
		}
		else if (list.size()== 0)
		{
			PersistenceException pe = new PersistenceException("nessuna instanza su SERVICES con id " + serviceId);
			throw pe;
		}
		Services entity = list.get(0);

		Map<String, String> result = new HashMap<String, String>();
		result.put(HOST_NAME, entity.getHostname());
		result.put(IP, entity.getIpHost());
		result.put(OFFICE_ID, null);

		return result;
	}

	public Map<String, String> getFromEVENTS(String serviceId, String channelId, String eventId, String version) throws PersistenceException
	{
		// SELECT URL FROM EVENTS WHERE SERVICE = ? AND CHANNEL = ? AND EVENT_ID = ? AND VERSION = ?
		Events example = new Events();
		example.setService(serviceId);
		example.setChannel(channelId);
		example.setEventId(eventId);
		example.setVersion(version);
		List<Events> list = eventsDAO.findByExample(example);
		if (list.size()> 1)
		{
			PersistenceException pe = new PersistenceException("instanze multiple su EVENTS con id " + serviceId);
			throw pe;
		}
		else if (list.size()== 0)
		{
			PersistenceException pe = new PersistenceException("nessuna instanza su EVENTS con id " + serviceId);
			throw pe;
		}
		Events entity = list.get(0);

		Map<String, String> result = new HashMap<String, String>();
		result.put(URL, entity.getUrl());
		result.put(FORWARD_TO_INT, entity.getForwardToInt());

		return result;
	}

	public void readAll() throws PersistenceException
	{
		List<Events> list = eventsDAO.findAll();
		logger.debug(list);
		Iterator<Events> it = list.listIterator();
		while (it.hasNext())
		{
			logger.debug(it.next());
		}
	}
}
