/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.pr;

import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.XteLasciaPrendi;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.objectmodel.XteLasciaPrendiTableModel;
import com.selexelsag.xte.testtool.view.pr.XteTestToolRichiesteMassivePRView;
import java.awt.Color;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ResourceBundle;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.StyledDocument;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolRichiesteMassivePRController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener, ChangeListener, ListSelectionListener, TableModelListener {

    private XteTestToolRichiesteMassivePRView richiesteMassivePRView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolRichiesteMassivePRController.class);
    private XteLasciaPrendi selectedLasciaPrendiItem = null;
    private int currentSelectedRow = -1;
    
    public XteTestToolRichiesteMassivePRController(JPanel richiesteMassivePRView) {
        try {
            logger.info("XteTestToolRichiesteMassivePRController start class creation");
            this.richiesteMassivePRView = (XteTestToolRichiesteMassivePRView) richiesteMassivePRView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.richiesteMassivePRView.initializeView();
            addViewObjectsListeners();
            logger.info("XteTestToolRichiesteMassivePRController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolRichiesteMassivePRController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolRichiesteMassivePRController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Richieste Massive Posta Registrata controller creation failed", ex);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = richiesteMassivePRView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        richiesteMassivePRView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.richiesteMassivePRView.getRichiesteMassivePanelRef().addAncestorListener(this);        
        this.richiesteMassivePRView.getBundleIdTextRef().addFocusListener(this);
        this.richiesteMassivePRView.getBundleTPIdTextRef().addFocusListener(this);        
        this.richiesteMassivePRView.getXmlDataTextAreaRef().addFocusListener(this);                
        this.richiesteMassivePRView.getRichiesteMassiveDataTabbedPaneRef().addChangeListener(this);
        this.richiesteMassivePRView.getNotificaDataTextAreaRef().addFocusListener(this);        
        this.richiesteMassivePRView.getLasciaPrendiTableRef().getSelectionModel().addListSelectionListener(this);
        this.richiesteMassivePRView.getLasciaPrendiTableRef().getModel().addTableModelListener(this);        
        this.richiesteMassivePRView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.richiesteMassivePRView.getNotificaGeneraXmlDataButtonRef().addActionListener(this);
        this.richiesteMassivePRView.getStartLasciaPrendiAutoButtonRef().addActionListener(this);
        this.richiesteMassivePRView.getLasciaInviiMenoUnoButtonRef().addActionListener(this);        
        this.richiesteMassivePRView.getBackButtonRef().addActionListener(this);
        this.richiesteMassivePRView.getHomeButtonRef().addActionListener(this);
        this.richiesteMassivePRView.getExitButtonRef().addActionListener(this);                        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(richiesteMassivePRView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolRichiesteMassivePRController actionPerformed invoke generaXmlData");
            generaXmlData();
        }

        if(source.equals(richiesteMassivePRView.getNotificaGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolRichiesteMassivePRController actionPerformed invoke notificaGeneraXmlData");
            notificaGeneraXmlData();
        }

        if(source.equals(richiesteMassivePRView.getStartLasciaPrendiAutoButtonRef()))
        {
            logger.debug("XteTestToolRichiesteMassivePRController actionPerformed invoke startLasciaPrendiAuto");
            startLasciaPrendiAuto();
        }

        if(source.equals(richiesteMassivePRView.getLasciaInviiMenoUnoButtonRef()))
        {
            logger.debug("XteTestToolRichiesteMassivePRController actionPerformed invoke lasciaInviiMenoUno");
            lasciaInviiMenoUno();
        }

        if(source.equals(richiesteMassivePRView.getBackButtonRef()))
        {
            logger.debug("XteTestToolRichiesteMassivePRController actionPerformed invoke back");
            back();
        }

        if(source.equals(richiesteMassivePRView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolRichiesteMassivePRController actionPerformed invoke home");
            home();
        }

        if(source.equals(richiesteMassivePRView.getExitButtonRef()))
        {
            logger.debug("XteTestToolRichiesteMassivePRController actionPerformed invoke exit");
            exit();
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolRichiesteMassivePRView)
        {
            logger.debug("XteTestToolRichiesteMassivePRController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolRichiesteMassivePRController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Richieste Massive Posta Registrata controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolRichiesteMassivePRController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Richieste Massive Posta Registrata controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }    

    @Override
    public void stateChanged(ChangeEvent e) {
        Object source = (Object) e.getSource();
        if(source.equals(richiesteMassivePRView.getRichiesteMassiveDataTabbedPaneRef()))
        {
            JTabbedPane sourceTabbedPane;
            sourceTabbedPane = (JTabbedPane) e.getSource();
            
            if(sourceTabbedPane.getSelectedIndex() == 0)
            {
            }
            
            if(sourceTabbedPane.getSelectedIndex() == 1)
            {
            }
        }
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object source = e.getSource();
        if(source.equals(richiesteMassivePRView.getLasciaPrendiTableRef().getSelectionModel()))
        {               
            if (e.getValueIsAdjusting()) {
                    return;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
                        currentSelectedRow = richiesteMassivePRView.getLasciaPrendiTableRef().getSelectedRow();
                        logger.debug("XteTestToolRichiesteMassivePRController valueChanged currentSelectedRow = " + currentSelectedRow);
                        logger.debug("XteTestToolRichiesteMassivePRController valueChanged: " + ((XteLasciaPrendiTableModel)richiesteMassivePRView.getLasciaPrendiTableRef().getModel()).getMap().size());
                        if(currentSelectedRow!=-1)
                        {
//                            List<Component> enableComponents = new ArrayList<Component>();
//                            enableComponents.add(richiesteMassivePRView.getLasciaInviiMenoUnoButtonRef());
//                            componentStatusManager.enableComponent(enableComponents);
                            selectedLasciaPrendiItem = ((XteLasciaPrendiTableModel)richiesteMassivePRView.getLasciaPrendiTableRef().getModel()).getSelectedItem(currentSelectedRow);
                        }
                        else
                        {
//                            List<Component> disableComponents = new ArrayList<Component>();
//                            disableComponents.add(richiesteMassivePRView.getLasciaInviiMenoUnoButtonRef());
//                            componentStatusManager.disableComponent(disableComponents);                                                        
                            selectedLasciaPrendiItem = null;
                        }
                    }
                } );
        }
        
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int eventType = e.getType();
        DefaultTableModel model = (DefaultTableModel) e.getSource();
        if(model.equals(richiesteMassivePRView.getLasciaPrendiTableRef().getModel()))
        {               
            logger.debug("XteTestToolRichiesteMassivePRController tableChanged: " + ((XteLasciaPrendiTableModel)richiesteMassivePRView.getLasciaPrendiTableRef().getModel()).getMap().size());
        }
    }
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        richiesteMassivePRView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolRichiesteMassivePRController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.richiesteMassivePRView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(richiesteMassivePRView);
            updateXteTestToolDataModel(dataModel);

            clearMessageBar();

            richiesteMassivePRView.getBundleIdTextRef().requestFocus();

            logger.info("XteTestToolRichiesteMassivePRController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolRichiesteMassivePRController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolRichiesteMassivePRController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Richieste Massive Posta Registrata controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolRichiesteMassivePRController start exit");
        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(richiesteMassivePRView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolRichiesteMassivePRController exit in progress");
            richiesteMassivePRView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolRichiesteMassivePRController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(richiesteMassivePRView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(richiesteMassivePRView.getRichiesteMassivePanelRef().getParent(), richiesteMassivePRView.getRichiesteMassivePanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolRichiesteMassivePRController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(richiesteMassivePRView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(richiesteMassivePRView.getRichiesteMassivePanelRef().getParent(), richiesteMassivePRView.getRichiesteMassivePanelRef(), homeView, new Rectangle(0,70,1000,690));
    }


    private void generaXmlData()
    {
        logger.info("XteTestToolRichiesteMassivePRController start generaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            logger.info("XteTestToolRichiesteMassivePRController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolRichiesteMassivePRController generaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolRichiesteMassivePRController generaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Richieste Massive Posta Registrata controller generaXmlData failed", ex);
        }
    }

    private void notificaGeneraXmlData()
    {
        logger.info("XteTestToolRichiesteMassivePRController start notificaGeneraXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            logger.info("XteTestToolRichiesteMassivePRController notificaGeneraXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolRichiesteMassivePRController notificaGeneraXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolRichiesteMassivePRController notificaGeneraXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Richieste Massive Posta Registrata controller notificaGeneraXmlData failed", ex);
        }
    }
    
    private void startLasciaPrendiAuto()
    {
        try {
            logger.info("XteTestToolRichiesteMassivePRController start startLasciaPrendiAuto");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            logger.info("XteTestToolRichiesteMassivePRController startLasciaPrendiAuto executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolRichiesteMassivePRController startLasciaPrendiAuto failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolRichiesteMassivePRController startLasciaPrendiAuto failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Richieste Massive Posta Registrata controller startLasciaPrendiAuto failed", ex);
        }
    }

    private void lasciaInviiMenoUno()
    {
        logger.info("XteTestToolRichiesteMassivePRController start lasciaInviiMenoUno");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            logger.info("XteTestToolRichiesteMassivePRController lasciaInviiMenoUno executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolRichiesteMassivePRController lasciaInviiMenoUno failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolRichiesteMassivePRController lasciaInviiMenoUno failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Richieste Massive Posta Registrata controller lasciaInviiMenoUno failed", ex);
        }
    }
}
