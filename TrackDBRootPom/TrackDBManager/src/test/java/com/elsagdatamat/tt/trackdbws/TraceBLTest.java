package com.elsagdatamat.tt.trackdbws;


import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.elsagdatamat.tt.trackdbws.bl.ITraceBL;
import com.elsagdatamat.tt.trackdbws.bl.ITraceBLSupport;
import com.elsagdatamat.tt.trackdbws.dao.ITraceDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
@TransactionConfiguration(defaultRollback = false)
public class TraceBLTest {

	@Resource
	private ITraceBL traceBL;
	@Resource
	private ITraceDAO traceDAO;

	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void mergeTrace() {
		Trace t=traceDAO.findById(648479356L, false);
		traceBL.mergeAvvNspTraceWithAccNspTrace(t);
	}

}
