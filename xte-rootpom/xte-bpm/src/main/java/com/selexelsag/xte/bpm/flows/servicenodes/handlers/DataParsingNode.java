package com.selexelsag.xte.bpm.flows.servicenodes.handlers;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.drools.process.instance.WorkItemHandler;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemManager;

import com.selexelsag.xte.model.flows.FlowParams;

public class DataParsingNode implements WorkItemHandler {

	private Logger logger = LogManager.getLogger(DataParsingNode.class);
	
	@Override
	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
		
	}

	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		logger.debug("executeWorkItem START");
		
		Map<String, Object> results = null;
		
		try{
			
			StringBuilder data = (StringBuilder)workItem.getParameter("data");
			if (data==null) {
				logger.debug("data parameter is null!");
			}
			
			FlowParams flowParams = (FlowParams)workItem.getParameter("flowParams");
			if (flowParams==null) {
				logger.debug("flowParams parameter is null!");
			}
			
			results = new HashMap<String, Object>();
			results.put("data", data);
			results.put("flowParams", flowParams);
		
		}catch(Throwable ex){

			// logger.error(ex, ex);
			results = new HashMap<String, Object>();
			results.put("error", ex);
			results.put("data", (StringBuilder)workItem.getParameter("data"));
			results.put("flowParams", (FlowParams)workItem.getParameter("flowParams"));
		}finally {
			manager.completeWorkItem(workItem.getId(), results);
			logger.debug("executeWorkItem END");
		}
		
		
		/* FOR TEST
		FlowParams params = (FlowParams)workItem.getParameter("flowParams");
		System.out.println("trasformazione");
		System.out.println(params.getFlowId());
		
		Map<String, Object> results = new HashMap<String, Object>();
		results.put("data", new StringBuilder());
		results.put("flowParams", null);
		manager.completeWorkItem(workItem.getId(), results);
		*/
		

	}

}
