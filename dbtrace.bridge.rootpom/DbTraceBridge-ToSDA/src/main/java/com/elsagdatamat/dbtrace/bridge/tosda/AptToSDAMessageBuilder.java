package com.elsagdatamat.dbtrace.bridge.tosda;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.elsagdatamat.dbtrace.bridge.engine.MessageBuilderBase;
import com.elsagdatamat.dbtrace.bridge.engine.bl.ICodiciProdottoPICodiciProdottoSDAManager;
import com.elsagdatamat.dbtrace.bridge.engine.bl.IRemovedTraceBL;
import com.elsagdatamat.dbtrace.bridge.engine.dao.IModPagContrPiSdaDAO;
import com.elsagdatamat.dbtrace.bridge.engine.dao.IOfficeMapperDAO;
import com.elsagdatamat.dbtrace.bridge.engine.dao.IStatusPiSdaDAO;
import com.elsagdatamat.dbtrace.bridge.engine.entities.RemovedTrace;
import com.elsagdatamat.dbtrace.validation.event.NonCompliantTrace;
import com.elsagdatamat.dbtrace.validation.event.TraceValidator;
import com.elsagdatamat.framework.resources.ClassPathResource;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TokenTilltoDateByFiltersList;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesListNextToken;


public class AptToSDAMessageBuilder extends MessageBuilderBase {
	protected final Log log = LogFactory.getLog(getClass());

	private IModPagContrPiSdaDAO modPagDao;
	private IStatusPiSdaDAO idStatusDao;
	private ICodiciProdottoPICodiciProdottoSDAManager subpManager;
	private IOfficeMapperDAO officeMapperDAO;
	private IRemovedTraceBL removedTraceBL;

	public IRemovedTraceBL getRemovedTraceBL() {
		return removedTraceBL;
	}
	public void setRemovedTraceBL(IRemovedTraceBL removedTraceBL) {
		this.removedTraceBL = removedTraceBL;
	}
	public IOfficeMapperDAO getOfficeMapperDAO() {
		return officeMapperDAO;
	}
	public void setOfficeMapperDAO(IOfficeMapperDAO officeMapperDAO) {
		this.officeMapperDAO = officeMapperDAO;
	}

	private JmsTemplate jmsTemplate;

	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	private ClassPathResource xsdPath;

	public void setXsdPath(ClassPathResource xsdPath) {
		this.xsdPath = xsdPath;
	}
	/**
	 * @return the modPagDao
	 */
	public IModPagContrPiSdaDAO getModPagDao() {
		return modPagDao;
	}

	/**
	 * @param modPagDao
	 *            the modPagDao to set
	 */
	public void setModPagDao(IModPagContrPiSdaDAO modPagDao) {
		this.modPagDao = modPagDao;
	}

	/**
	 * @return the idStatusDao
	 */
	public IStatusPiSdaDAO getIdStatusDao() {
		return idStatusDao;
	}

	/**
	 * @param idStatusDao
	 *            the idStatusDao to set
	 */
	public void setIdStatusDao(IStatusPiSdaDAO idStatusDao) {
		this.idStatusDao = idStatusDao;
	}

	/**
	 *
	 * @return subpManager
	 */
	public ICodiciProdottoPICodiciProdottoSDAManager getSubpManager() {
		return subpManager;
	}

	/**
	 *
	 * @param subpManager the subpManager to set
	 */
	public void setSubpManager(ICodiciProdottoPICodiciProdottoSDAManager subpManager) {
		this.subpManager = subpManager;
	}

	// IMessageBuilder
	@Override
	public List<String> create(TracesList input) {
		log.debug("Entering create");

		long idMessage = -1;
		Exception exception = null;
		List<String> sList = new ArrayList<String>();
		String sdaStr = null;
		SDAMessage sdaMsg = null;
		try {
			Schema schema = null;
			if (xsdPath != null) {
				log.debug("Calling getSchema");
				schema = ConversionUtility.getInstance().getSchemaFromXSD(xsdPath.getURL());
			}
			log.debug("Calling convertToSDA");
			sdaMsg = ConversionUtility.getInstance(modPagDao, idStatusDao).convertAptTracesToSDATraces(input.getTraces(), modPagDao,subpManager,officeMapperDAO);
			log.debug("Calling marshal");
			TraceValidator traceValidator = new TraceValidator();
			sdaStr = ConversionUtility.getInstance().marshal(sdaMsg, schema , traceValidator);
			sList.add(sdaStr);
		} catch (Exception e) {
			exception = e;
			log.error(e.getMessage(),e);
			return sList;
		} finally {
			try {
				if (exception != null) {
					String str = ConversionUtility.getInstance().marshal(sdaMsg, null);
					idMessage = discardedTraceManager.saveDiscardedTraces(token.getServiceId(), str, exception, true);
					insertRemovedTraces(ConversionUtility.getInstance().getNonCompliantTraces(),idMessage, token.getServiceId());
				} else if (isSaveTraces()) {
					idMessage = discardedTraceManager.saveDiscardedTraces(token.getServiceId(), sdaStr, null, true);
					insertRemovedTraces(ConversionUtility.getInstance().getNonCompliantTraces(),idMessage, token.getServiceId());
				}
			} catch (Exception e) {
				log.error(e.getMessage(),e);
			}
		}
		log.debug("Leaving create");
		return sList;
	}


	private void insertRemovedTraces(List<NonCompliantTrace> traces, long idDiscardedTrace, String serviceId) throws JAXBException, IOException{
		JAXBContext context = JAXBContext.newInstance(SDARemovedTrace.class);
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);


		for (NonCompliantTrace nonCompliantTrace : traces) {
			RemovedTrace removedTrace = new RemovedTrace();
			removedTrace.setDiscardingProcess(serviceId);
			SDARemovedTrace trace = new SDARemovedTrace(nonCompliantTrace.getSdaTrace());
			StringWriter sw = new StringWriter();
			marshaller.marshal(trace, sw);

			removedTrace.setTraced(sw.toString());
			removedTrace.setReason(nonCompliantTrace.getReason());
			removedTrace.setIdDiscardedTrace(idDiscardedTrace);
			removedTraceBL.insert(removedTrace);
		}
	}

	// IMessageSender
	@Override
	public void send(final List<String> strList) throws IOException {
		log.debug("Entering send");
		for (final String str : strList) {
			jmsTemplate.send(new MessageCreator() {
				@Override
				public Message createMessage(Session session) throws JMSException {
					TextMessage tm = session.createTextMessage();
					tm.setText(str);
					return tm;
				}
			});
		}
		log.debug("Leaving send");
	}

	// ITracesReader
	@Override
	public TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate) {

		// Costruisco il parametro per la richiesta delle tracce
		// Voglio tutte le tracce a partire dal token precedentemente ottenuto
		// fino a stopDate (passato dal motore che lo inizializza una volta per
		// tutte)....
		log.debug(String.format("Entering getTraces for service '%s' (wsToken='%s'): stopDate=%s;", token.getServiceId(), token.getWsToken(), stopDate));

		TokenTilltoDateByFiltersList filterCallPrepare = new TokenTilltoDateByFiltersList();
		filterCallPrepare.setTilltoDate(getGCalendar(stopDate));
		filterCallPrepare.setToken(token.getWsToken());

		TracesList traceEqualList = new TracesList();
		for(WhatHappened eqWhatHapp:WhatHappened.values()) {
			Trace whatHappQueryParamsEquals = new Trace();
			whatHappQueryParamsEquals.setWhatHappened(eqWhatHapp.toString());
			whatHappQueryParamsEquals.setTracedEntity("PACCO");
			// R.L. - 30052014 - Chioschi: solo canale chioschi APT
			whatHappQueryParamsEquals.setChannel("APT");
			traceEqualList.getTraces().add(whatHappQueryParamsEquals);
		}

		filterCallPrepare.setTracesFilterByExampleEqual(traceEqualList);

		TracesListNextToken tlnt = twsp.getTracesByFiltersList(filterCallPrepare);

		String nextTokenFromWS = tlnt.getNextToken();

		TracesList tracesList = tlnt.getTraceList();
		if ((tracesList != null) && (tracesList.getTraces() != null)){
			log.info(String.format("Called web service getTraces: obtained %d traces (service='%s'; wsToken='%s')",  tracesList.getTraces().size(), token.getServiceId(), token.getWsToken()));
		} else {
			log.debug(String.format("Called web service getTraces: obtained 0 traces (service='%s'; wsToken='%s')", token.getServiceId(), token.getWsToken()));
			String nToken = tlnt.getNextToken();
			String[] splitteToken = nToken.split(";");

			Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			c.add(Calendar.MINUTE, -60);
			nextTokenFromWS = String.valueOf(c.getTimeInMillis()) + ";" + splitteToken[1];
		}
		// Prese le tracce, aggiorno il wsToken dentro lo SDAToken
		token.setWsToken(nextTokenFromWS);

		return tracesList;
	}

}
