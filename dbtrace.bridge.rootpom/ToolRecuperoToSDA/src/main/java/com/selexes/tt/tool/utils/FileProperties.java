package com.selexes.tt.tool.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FileProperties {

	private String fileProperties = "messagingTool.properties";
	private Log log= LogFactory.getLog(this.getClass());
	private HashMap<String,String> map = null;
	
	public FileProperties (String filename){
		this.fileProperties = filename;
	}
	
	/**
	 * @param args
	 */
	public String getProp(String key) {
		try {
			if(map==null)
				getProperties();
			String value=map.get(key);
			log.info("richiesta proprieta' ["+key+"] - ottenuto valore ["+value+"]");
			return value;
		} catch (IOException e) {
			log.error("File di properties non trovato");
			e.printStackTrace();
		}
		return null;
	}
	
	public  HashMap<String,String> getProperties() throws IOException { 
		map = new HashMap<String,String>();  
		String result = ""; 
		Object obj = null; 
		Properties props = new Properties();
		props.load(new FileInputStream(fileProperties)); 
		Enumeration<?> e = props.keys(); 
		while (e.hasMoreElements()) { 
			obj = e.nextElement();
			result = props.getProperty(obj.toString()); 
			map.put(String.valueOf(obj), result);
		} 
		return map;
		} 
	}

