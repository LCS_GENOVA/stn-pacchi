/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.datamodel;


/**
 *
 * @author Da Procida
 */
public class XteLasciaPrendi {

    private String codiceInvio;
    private String lasciaPrendi;
    private String errCode;
    private String errDescription;

    public String getCodiceInvio() {
        return codiceInvio;
    }

    public void setCodiceInvio(String codiceInvio) {
        this.codiceInvio = codiceInvio;
    }

    public String getLasciaPrendi() {
        return lasciaPrendi;
    }

    public void setLasciaPrendi(String lasciaPrendi) {
        this.lasciaPrendi = lasciaPrendi;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrDescription() {
        return errDescription;
    }

    public void setErrDescription(String errDescription) {
        this.errDescription = errDescription;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (this.codiceInvio != null ? this.codiceInvio.hashCode() : 0);
        hash = 41 * hash + (this.lasciaPrendi != null ? this.lasciaPrendi.hashCode() : 0);
        hash = 41 * hash + (this.errCode != null ? this.errCode.hashCode() : 0);
        hash = 41 * hash + (this.errDescription != null ? this.errDescription.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final XteLasciaPrendi other = (XteLasciaPrendi) obj;
        if ((this.codiceInvio == null) ? (other.codiceInvio != null) : !this.codiceInvio.equals(other.codiceInvio)) {
            return false;
        }
        if ((this.lasciaPrendi == null) ? (other.lasciaPrendi != null) : !this.lasciaPrendi.equals(other.lasciaPrendi)) {
            return false;
        }
        if ((this.errCode == null) ? (other.errCode != null) : !this.errCode.equals(other.errCode)) {
            return false;
        }
        if ((this.errDescription == null) ? (other.errDescription != null) : !this.errDescription.equals(other.errDescription)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "XteLasciaPrendiPR{" + "codiceInvio=" + codiceInvio + ", lasciaPrendi=" + lasciaPrendi + ", errCode=" + errCode + ", errDescription=" + errDescription + '}';
    }
    
}
