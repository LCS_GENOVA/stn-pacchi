package com.selexelsag.xte.bpm.flows.servicenodes.handlers;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.drools.process.instance.WorkItemHandler;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemManager;

import com.selexelsag.xte.business.exceptions.BusinessTrasformationException;
import com.selexelsag.xte.business.tracking.ITrasformation;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.service.context.SpringApplicationContext;

public class CapNormalizationNode  implements WorkItemHandler{

	private Logger logger = LogManager.getLogger(CapNormalizationNode.class.getName());
	
	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		Map<String, Object> results = null;
		
		try{
			
			StringBuilder data = (StringBuilder)workItem.getParameter("data");

			ITrasformation trasformation = (ITrasformation)SpringApplicationContext.
					getBean(ITrasformation.TRASFORMATION_BEAN_NAME);
			
			StringBuilder trasformationAfterValidation = trasformation.trasformationAfterValidation(data);
			
			results = new HashMap<String, Object>();
			results.put("data", trasformationAfterValidation);
			results.put("flowParams", (FlowParams)workItem.getParameter("flowParams"));
			
		// }catch(BusinessTrasformationException ex){
		}catch (Throwable ex) {
			// logger.error(ex, ex);
			results = new HashMap<String, Object>();
			results.put("error", ex);
			results.put("data", (StringBuilder)workItem.getParameter("data"));
			results.put("flowParams", (FlowParams)workItem.getParameter("flowParams"));
			
		}finally {
			manager.completeWorkItem(workItem.getId(), results);
		}
	}

	@Override
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager){
		
		
	}

}
