package com.selexelsag.xte.business.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.xte.business.exceptions.BusinessMessageProcessorException;
import com.selexelsag.xte.business.tracking.IMessageProcessorFactory;
import com.selexelsag.xte.model.message_processor.MessageProcessor;
import com.selexelsag.xte.persistence.XteMessagesDBAccess;
import com.selexelsag.xte.service.context.SpringApplicationContext;
import com.selexelsag.xte.service.resource.IMessageManager;
import com.selexelsag.xte.service.system.IAppConfig;

public class MessageProcessorFactory implements IMessageProcessorFactory {

	private static Logger logger = Logger.getLogger(MessageProcessorFactory.class);
	
	@Autowired
	private IAppConfig appConfig;
	
	@Autowired
	private XteMessagesDBAccess managerDb;
	
	private String[] flowId;
	
	private Map<String, Integer> rowCount;
	
	private Map<String, String> processors;
	
	public MessageProcessorFactory() {
		
		rowCount = new HashMap<String, Integer>();
		
		processors = new HashMap<String, String>();
	}
	
	@PostConstruct
	public void init() {
		String flowIdListCSV = appConfig.getMessageProcessorParam(IAppConfig.MESSAGE_PROCESSOR_FLOW_RESOLVER);
		flowId = flowIdListCSV.split(",");
		
		for(int i=0;i<flowId.length;i++) {
			rowCount.put(flowId[i], new Integer(appConfig.getMessageProcessorParam(flowId[i] + ".numRows")));
			processors.put(flowId[i], appConfig.getMessageProcessorParam(flowId[i] + ".processorClass"));
		}
	}
	
	@Override
	public String[] getManagedFlows() {
		return flowId;
	}
	@Override
	public int getRowCount(String flowId) {
		return rowCount.get(flowId).intValue();
	}
	
	@Override
	public MessageProcessor getMessageProcessor(String flowId) throws BusinessMessageProcessorException {
		MessageProcessor processor;
		
		try {
			String processorClassName = processors.get(flowId);
			if (processorClassName==null) {
				BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("No entry for flowid " + flowId, null);
				throw bmpe;
			}
			processor = (MessageProcessor)Class.forName(processorClassName).getConstructor().newInstance();
		} catch (ClassNotFoundException e) {
			BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("Error in processor load " + flowId, e);
			throw bmpe;
		} catch (SecurityException e) {
			BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("Error in processor load " + flowId, e);
			throw bmpe;
		} catch (NoSuchMethodException e) {
			BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("Error in processor load " + flowId, e);
			throw bmpe;
		} catch (IllegalArgumentException e) {
			BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("Error in processor load " + flowId, e);
			throw bmpe;
		} catch (InstantiationException e) {
			BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("Error in processor load " + flowId, e);
			throw bmpe;
		} catch (IllegalAccessException e) {
			BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("Error in processor load " + flowId, e);
			throw bmpe;
		} catch (InvocationTargetException e) {
			BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("Error in processor load " + flowId, e);
			throw bmpe;
		}
		processor.setMessageProcessorProperties(appConfig.getMessageProcessorProperties());
		processor.setDBAccess(managerDb);
		return processor;
	}
}
