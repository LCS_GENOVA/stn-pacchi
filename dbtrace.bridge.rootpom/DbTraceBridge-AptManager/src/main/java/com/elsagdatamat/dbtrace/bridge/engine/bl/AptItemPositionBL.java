package com.elsagdatamat.dbtrace.bridge.engine.bl;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition;

public interface AptItemPositionBL {
	@Transactional(value = "tt-transactionManager", propagation = Propagation.REQUIRED)
	public void insertOrUpdate(AptItemPosition item);

	@Transactional(value = "tt-transactionManager", propagation = Propagation.REQUIRED)
	public void insert(AptItemPosition item);

	@Transactional(value = "tt-transactionManager", propagation = Propagation.REQUIRED)
	public void update(AptItemPosition itemP);

	public AptItemPosition find(String code);

}
