package com.selexelsag.xte.persistence.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Restrictions;

import com.selexelsag.xte.persistence.entities.Offices;

/**
 * Home object for domain model class Offices.
 * @see com.selexelsag.xte.persistence.entities.Offices
 * @author Hibernate Tools
 */
public class OfficesDAO extends BaseDAO<Offices> 
{
	private static final Log log = LogFactory.getLog(OfficesDAO.class);

	public OfficesDAO() 
	{
		super(Offices.class);
	}
	
	public Offices findById(com.selexelsag.xte.persistence.entities.OfficesId id) 
	{
		log.debug("getting Offices instance with id: " + id);
		try 
		{
			Offices instance = (Offices) sessionFactory.getCurrentSession()
					.get("com.selexelsag.xte.persistence.entities.Offices", id);
			if (instance == null) 
			{
				log.debug("get successful, no instance found");
			}
			else 
			{
				log.debug("get successful, instance found");
			}
			return instance;
		} 
		catch (RuntimeException re) 
		{
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Offices instance) 
	{
		log.debug("finding Offices instance by example");
		try
		{
			if (instance.getId()!=null && instance.getId().getOfficeId()!=null) 
			{
				List results = sessionFactory
						.getCurrentSession()
						.createCriteria(
								Offices.class).createAlias("id", "officesId")
								.add(Restrictions.eq("id.officeId", instance.getId().getOfficeId()))
								.add(Example.create(instance)).list();
				log.debug("find by example successful, result size: "
						+ results.size());
				return results;
			}
			else 
			{
				List results = sessionFactory
						.getCurrentSession()
						.createCriteria(
								Offices.class).add(Example.create(instance)).list();
				log.debug("find by example successful, result size: "
						+ results.size());
				return results;
			}
		} 
		catch (RuntimeException re) 
		{
			log.error("find by example failed", re);
			throw re;
		}
	}
}
