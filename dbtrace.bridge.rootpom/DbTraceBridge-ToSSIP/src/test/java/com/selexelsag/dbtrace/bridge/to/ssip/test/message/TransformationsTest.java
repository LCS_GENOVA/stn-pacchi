/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.ssip.test.message;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import junit.framework.Assert;

import org.junit.Test;

import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message;
import com.selexelsag.dbtrace.bridge.to.ssip.message.trans.TPATransformationValues;
import com.selexelsag.dbtrace.bridge.to.ssip.message.trans.Transformations;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetailsList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

/**
 * @author arodriguez
 *
 */
public class TransformationsTest {

	@Test
	public void transformationsTracesListToMessageBodyCreateValidMessage() {
		
		Transformations transformation = new Transformations();
		
		TracesList tracesList = new TracesList();
		List<Trace> traces = new ArrayList<Trace>();
		
		for (int i=0; i < 5; i++) {
			Trace trace = new Trace();
			trace.setChannel("APT");
			trace.setIdChannel("ID" + i);
			trace.setIdForwardTo("22222" + i);
			trace.setIdTracedEntity("00200");
			trace.setIdStatus("002");
			trace.setTracedEntity("PACCO");
			trace.setServiceName("PACCHI");
			trace.setLabelTracedEntity("LABEL" + i);
			trace.setIdCorrelazione("" + i);
			trace.setWhatHappened("ACC_NSP");
			trace.setWhereHappened("00001");
			
			TraceDetailsList details = new TraceDetailsList();

			int j = 0;
			for (TPATransformationValues key : TPATransformationValues.values()) {
			    TraceDetail detail = new TraceDetail();
			    
				detail.setParamClass(key.toString());
				detail.setParamValue("test" + j);
				details.getTraceDetails().add(detail);
				j++;
			}
			trace.setTraceDetailsList(details);
			tracesList.getTraces().add(trace);
		}

		for (int i=0; i < 5; i++) {
			Trace trace = new Trace();
			trace.setChannel("NSP");
			trace.setIdChannel("ID" + i);
			trace.setIdForwardTo("33333" + i);
			trace.setIdTracedEntity("00300");
			trace.setIdStatus("003");
			trace.setTracedEntity("PACCO");
			trace.setServiceName("PACCHI");
			trace.setLabelTracedEntity("LABEL" + i);
			trace.setIdCorrelazione("" + i);
			trace.setWhatHappened("TT_TO_SSIP");
			trace.setWhereHappened("00002");
			
			TraceDetailsList details = new TraceDetailsList();

			int j = 0;
			for (TPATransformationValues key : TPATransformationValues.values()) {
			    TraceDetail detail = new TraceDetail();
			    
				detail.setParamClass(key.toString());
				detail.setParamValue("test" + j);
				details.getTraceDetails().add(detail);
				j++;
			}
			trace.setTraceDetailsList(details);
			tracesList.getTraces().add(trace);
		}

		Message ssipMessage = transformation.traceListToMessage("APT", tracesList.getTraces());
		Assert.assertNotNull(ssipMessage);
		
		try {
			String xmlMessage = transformation.marshallMessageToXml(ssipMessage, null);
			Assert.assertNotNull(xmlMessage);
			
			System.out.println(xmlMessage);
			
			Message ssipUnmarshalled = transformation.unmarshallXmlToMessage(xmlMessage, null);
			Assert.assertNotNull(ssipUnmarshalled);
			String xmlUnmarshalled = transformation.marshallMessageToXml(ssipUnmarshalled, null);
			Assert.assertEquals(xmlMessage, xmlUnmarshalled);
		} catch (JAXBException e) {
			Assert.fail(e.getMessage());
		}
	}
	
}
