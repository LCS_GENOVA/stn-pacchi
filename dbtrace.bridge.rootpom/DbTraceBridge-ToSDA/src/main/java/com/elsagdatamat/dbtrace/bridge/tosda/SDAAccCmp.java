package com.elsagdatamat.dbtrace.bridge.tosda;

import java.util.Map;

import javax.xml.bind.annotation.XmlElement;

public class SDAAccCmp {
	@XmlElement(name = "PH")
	public String ph;

	@XmlElement(name = "PSTF")
	public String pstf;

	@XmlElement(name = "SUBP")
	public String subp;

	@XmlElement(name = "REGT")
	public String regt;

	public SDAAccCmp(Map<String, String> map)
	{
		String key = "PH";
		this.ph = map.get(key);
		key = "SUBP";
		this.subp = map.get(key);
		key = "REGT";
		this.regt = map.get(key);
		key = "PSTF";
		this.pstf = map.get(key);
	}
	
	public SDAAccCmp()
	{
	}
}
