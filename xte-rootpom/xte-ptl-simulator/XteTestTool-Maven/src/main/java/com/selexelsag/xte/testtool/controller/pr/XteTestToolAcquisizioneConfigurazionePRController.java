/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.pr;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;
import com.selexelsag.xte.testtool.view.pr.XteTestToolAcquisizioneConfigurazionePRView;
import com.selexelsag.xte.testtool.view.pr.XteTestToolWebServicePRView;

import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata.AC;
import com.selexelsag.xtetesttool.xml.postaregistrata.ac.ObjectFactory;
import com.selexelsag.xtetesttool.xml.postaregistrata.ac.Traccia;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolAcquisizioneConfigurazionePRController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener {

    private XteTestToolAcquisizioneConfigurazionePRView acquisizioneConfigurazionePRView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolAcquisizioneConfigurazionePRController.class);

	private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    
    private JAXBContext context;
    
    private static Traccia acquisizioneConfigurazioneTraccia;
    
    static {
        logger.debug("Creating Config Object Factory and Traccia");
        PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        AC config = postaRegistrataConfig.getAC();

        acquisizioneConfigurazioneTraccia = objectFactory.createTraccia();
        acquisizioneConfigurazioneTraccia.setEvento(config.getEventName());
    }
  

    public XteTestToolAcquisizioneConfigurazionePRController(JPanel acquisizioneConfigurazionePRView) {
        try {
            logger.info("XteTestToolAcquisizioneConfigurazionePRController start class creation");
            this.acquisizioneConfigurazionePRView = (XteTestToolAcquisizioneConfigurazionePRView) acquisizioneConfigurazionePRView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.acquisizioneConfigurazionePRView.initializeView();
			updateView();
            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.postaregistrata.ac");

            logger.info("XteTestToolAcquisizioneConfigurazionePRController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneConfigurazionePRController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolAcquisizioneConfigurazionePRController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Configurazione Posta Registrata controller creation failed", ex);
        }
    }
    
    private void updateDataModel() {
        logger.debug("updateDataModel");
    }
    
    
    private void updateView() {  
        logger.debug("updateView");
     }
    
    
    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = acquisizioneConfigurazionePRView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        acquisizioneConfigurazionePRView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.acquisizioneConfigurazionePRView.getAcquisizioneConfigurazionePanelRef().addAncestorListener(this);
        this.acquisizioneConfigurazionePRView.getXmlDataTextAreaRef().addFocusListener(this);
        this.acquisizioneConfigurazionePRView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.acquisizioneConfigurazionePRView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.acquisizioneConfigurazionePRView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.acquisizioneConfigurazionePRView.getBackButtonRef().addActionListener(this);
        this.acquisizioneConfigurazionePRView.getHomeButtonRef().addActionListener(this);
        this.acquisizioneConfigurazionePRView.getExitButtonRef().addActionListener(this);

		this.salvaXmlFileChooser.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(acquisizioneConfigurazionePRView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneConfigurazionePRController actionPerformed invoke generaXmlData");

            updateDataModel();

            generaXmlData();
        }

        if(source.equals(acquisizioneConfigurazionePRView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneConfigurazionePRController actionPerformed invoke impostaDatiWebService");

            updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(acquisizioneConfigurazionePRView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneConfigurazionePRController actionPerformed invoke salvaXmlData");

            updateDataModel();

            salvaXmlData();
        }

        if(source.equals(acquisizioneConfigurazionePRView.getBackButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneConfigurazionePRController actionPerformed invoke back");

            updateDataModel();

            back();
        }

        if(source.equals(acquisizioneConfigurazionePRView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneConfigurazionePRController actionPerformed invoke home");

            updateDataModel();

            home();
        }

        if(source.equals(acquisizioneConfigurazionePRView.getExitButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneConfigurazionePRController actionPerformed invoke exit");

            updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(acquisizioneConfigurazionePRView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolAcquisizioneConfigurazionePRView)
        {
            logger.debug("XteTestToolAcquisizioneConfigurazionePRController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        if(comp instanceof JTextField)
        {
            ((JTextField)comp).setBackground(new Color(255,204,102));
        }
        if(comp instanceof JPasswordField)
        {
            ((JPasswordField)comp).setBackground(new Color(255,204,102));
        }
        if(comp instanceof JTextArea)
        {
            ((JTextArea)comp).setBackground(new Color(255,204,102));
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        if(comp instanceof JTextField)
        {
            if(((JTextField)comp).isEditable()) {
                ((JTextField)comp).setBackground(Color.WHITE);
            }
            else {
                ((JTextField)comp).setBackground(new Color(220, 220, 220));
            }
        }
        if(comp instanceof JPasswordField)
        {
            if(((JPasswordField)comp).isEditable()) {
                ((JPasswordField)comp).setBackground(Color.WHITE);
            }
            else {
                ((JPasswordField)comp).setBackground(new Color(220, 220, 220));
            }
        }
        if(comp instanceof JTextArea)
        {
            if(((JTextArea)comp).isEditable()) {
                ((JTextArea)comp).setBackground(Color.WHITE);
            }
            else {
                ((JTextArea)comp).setBackground(new Color(220, 220, 220));
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
            
    }

	private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        acquisizioneConfigurazionePRView.getMessageTextPaneRef().setDocument(document);    
    }

    private void init()
    {
        try{
            logger.info("XteTestToolAcquisizioneConfigurazionePRController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.acquisizioneConfigurazionePRView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(acquisizioneConfigurazionePRView);
            updateXteTestToolDataModel(dataModel);

            clearMessageBar();

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(acquisizioneConfigurazionePRView.getImpostaDatiWSButtonRef());
            disabledComponents.add(acquisizioneConfigurazionePRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            acquisizioneConfigurazionePRView.getXmlDataTextAreaRef().requestFocus();

            logger.info("XteTestToolAcquisizioneConfigurazionePRController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneConfigurazionePRController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneConfigurazionePRController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Configurazione Posta Registrata controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolAcquisizioneConfigurazionePRController start exit");

		clearMessageBar();

        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(acquisizioneConfigurazionePRView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolAcquisizioneConfigurazionePRController exit in progress");
            acquisizioneConfigurazionePRView.exit();
        }
    }

    private void back()
    {
        logger.info("start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(acquisizioneConfigurazionePRView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(acquisizioneConfigurazionePRView.getAcquisizioneConfigurazionePanelRef().getParent(), acquisizioneConfigurazionePRView.getAcquisizioneConfigurazionePanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolAcquisizioneConfigurazionePRController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(acquisizioneConfigurazionePRView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(acquisizioneConfigurazionePRView.getAcquisizioneConfigurazionePanelRef().getParent(), acquisizioneConfigurazionePRView.getAcquisizioneConfigurazionePanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }
    
    private void generaXmlData()
    {
        logger.info("start generaXmlData");
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();
        
        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            logger.error("Error creating the marshaller", ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneConfigurazionePRView.getMessageTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneConfigurazionePRView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneConfigurazionePRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione configurazione Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            logger.debug(messages);
            logger.error("error setting property in marshaller", ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneConfigurazionePRView.getMessageTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneConfigurazionePRView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneConfigurazionePRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Configurazione Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(acquisizioneConfigurazioneTraccia, out);
        } catch (JAXBException ex) {
            logger.error("Error marshalling", ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneConfigurazionePRView.getMessageTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneConfigurazionePRView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneConfigurazionePRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Configurazione Posta Registrata controller generaXmlData failed", ex);
        }
        
            
        try
        {            
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString());
            acquisizioneConfigurazionePRView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("generaXmlData failed", baseEx);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(baseEx.toString(), MessageType.ERROR);
            acquisizioneConfigurazionePRView.getMessageTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneConfigurazionePRView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneConfigurazionePRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("generaXmlData failed", ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneConfigurazionePRView.getMessageTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneConfigurazionePRView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneConfigurazionePRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Configurazione Posta Registrata controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        acquisizioneConfigurazionePRView.getMessageTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(acquisizioneConfigurazionePRView.getImpostaDatiWSButtonRef());
        enableComponent.add(acquisizioneConfigurazionePRView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolAcquisizioneConfigurazionePRController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServicePRView(acquisizioneConfigurazionePRView.getParentFrame());
            XteTestToolWebServicePRController controller = new XteTestToolWebServicePRController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
            AC config = postaRegistrataConfig.getAC();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(acquisizioneConfigurazionePRView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(acquisizioneConfigurazionePRView.getAcquisizioneConfigurazionePanelRef().getParent(), acquisizioneConfigurazionePRView.getAcquisizioneConfigurazionePanelRef(), webserviceview, new Rectangle(0,70,1000,690));
            
            logger.info("XteTestToolAcquisizioneConfigurazionePRController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolAcquisizioneConfigurazionePRController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneConfigurazionePRController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Configurazione Posta Registrata controller impostaDatiWebService failed", ex);
        }
    }
	class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }


    private void salvaXmlData()
    {
        logger.info("start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolAcquisizioneConfigurazionePRController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(acquisizioneConfigurazionePRView.getParentFrame());

            logger.info("XteTestToolAcquisizioneConfigurazionePRController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneConfigurazionePRController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneConfigurazionePRController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Configurazione Posta Registrata controller salvaXmlData failed", ex);
        }
    }

}
