package com.selexelsag.tt.dbtrace.bridge.serial.object;


import java.io.Serializable;
import java.util.Date;
import java.util.Set;


public class Trace implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long traceId;

	private String labelTracedEntity;
	
	private String tracedEntity;
	
	private String idTracedEntity;
	
	private Date whenHappened;
	
	private Date whenRegistered;
	
	private String whereHappened;
	
	private String channel;
	
	private String idChannel;
	
	private String whatHappened;
	
	private String serviceName;
	
	private String idStatus;
	
	private String idCorrelazione;
	
	private String idTracedExternal;
	
	private String idForwardTo;
	
	private String sysForwardTo;
	
	private Set<TraceDetail> internalDetailList;
	
	public Trace(){
	}
	
	public long getIdTrace() {
		return traceId;
	}
	public void setTracedEntity(String tracedEntity) {
		this.tracedEntity = tracedEntity;
	}
	public String getTracedEntity() {
		return tracedEntity;
	}	
	public void setIdTracedEntity(String idTracedEntity) {
		this.idTracedEntity = idTracedEntity;
	}
	public String getIdTracedEntity() {
		return idTracedEntity;
	}
	public void setWhenHappened(Date whenHappened) {
		this.whenHappened = whenHappened;
	}
	public Date getWhenHappened() {
		return whenHappened;
	}
	public void setWhenRegistered(Date whenRegistered) {
		this.whenRegistered = whenRegistered;
	}
	public Date getWhenRegistered() {
		return whenRegistered;
	}
	public void setWhereHappened(String whereHappened) {
		this.whereHappened = whereHappened;
	}
	public String getWhereHappened() {
		return whereHappened;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getChannel() {
		return channel;
	}
	public void setIdChannel(String idChannel) {
		this.idChannel = idChannel;
	}
	public String getIdChannel() {
		return idChannel;
	}
	public void setWhatHappened(String whatHappened) {
		this.whatHappened = whatHappened;
	}
	public String getWhatHappened() {
		return whatHappened;
	}
	public Set<TraceDetail> getInternalDetailList() {
		return internalDetailList;
	}
	public void setInternalDetailList(Set<TraceDetail> internalDetailList) {
		this.internalDetailList = internalDetailList;
	}
	public Long getTraceId() {
		return traceId;
	}
	public void setTraceId(Long traceId) {
		this.traceId = traceId;
	}
	public String getLabelTracedEntity() {
		return labelTracedEntity;
	}
	public void setLabelTracedEntity(String labelTracedEntity) {
		this.labelTracedEntity = labelTracedEntity;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getIdStatus() {
		return idStatus;
	}
	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}
	public String getIdTracedExternal() {
		return idTracedExternal;
	}
	public void setIdTracedExternal(String idTracedExternal) {
		this.idTracedExternal = idTracedExternal;
	}
	public String getIdCorrelazione() {
		return idCorrelazione;
	}
	public void setIdCorrelazione(String idCorrelazione) {
		this.idCorrelazione = idCorrelazione;
	}
	public String getIdForwardTo() {
		return idForwardTo;
	}
	public void setIdForwardTo(String idForwardTo) {
		this.idForwardTo = idForwardTo;
	}
	public String getSysForwardTo() {
		return sysForwardTo;
	}
	public void setSysForwardTo(String sysForwardTo) {
		this.sysForwardTo = sysForwardTo;
	}

}
