package com.selexelsag.xte.business.tracking;

import java.util.HashMap;
import java.util.Map;

import com.selexelsag.xte.business.exceptions.BusinessTrasformationException;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.model.flows.FlowResponse;

/**
 * 
 * @author Motta
 * Effettua le trasformazioni xml da e verso i servizi esterni da chiamare.
 * TODO: gestione delle eccezioni
 */
public interface ITrasformation {

	public static final String TRASFORMATION_BEAN_NAME = "baseTrasformation";
	
	/**
	 * 
	 * @param flowParams parametri specifici al flusso 
	 * @param data i dati da trasformare
	 * @return i dati processati
	 */
	StringBuffer requestTrasformation (FlowParams flowParams, StringBuilder data, HashMap<String, String> parameter) throws BusinessTrasformationException;
	
	/**
	 * Questo metodo effettua una specifica trasformazione che viene avviata dopo la validazione 
	 * @param data i dati da trasformare
	 * @return dati processati 
	 * @throws BusinessTrasformationException
	 */
	StringBuilder trasformationAfterValidation (StringBuilder data) throws BusinessTrasformationException;
	
	/**
	 * 
	 * @param flowParams parametri specifici al flusso 
	 * @param data i dati da trasformare
	 * @return i dati processati
	 */
	StringBuffer responseTrasformation(FlowParams flowParams, StringBuffer data);
	
	/**
	 * 
	 * Costruisce l'oggetto di risposta da dare al client, prendendo in dati dall'xml dato in input 
	 * @param data
	 * @param queries gli xpath dove andare a prendere i dati da inserire nell'oggetto TrackResponse
	 * @param flowResponse
	 * @return L'oggetto TrackResponse rappresentante l'esito della chiamata ai serivzi di tracciatura
	 * @throws BusinessTrasformationException
	 */
	void buildResponse(FlowParams params, StringBuilder dataResult, FlowResponse flowResponse) throws BusinessTrasformationException;
	
	/**
	 * 
	 * @param params parametri specifici al flusso 
	 * @param dataResult i dati processati
	 * @param flowResponse l'oggetto da riempire con i dati risposta
	 */
	void buildAckResponse(FlowParams params, Map<String, String> headerData, StringBuilder dataResult, FlowResponse flowResponse) throws BusinessTrasformationException;

}
