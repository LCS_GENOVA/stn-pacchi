package com.elsagdatamat.dbtrace.bridge.engine.dao;


import com.elsagdatamat.dbtrace.bridge.engine.entities.StatusPiSda;
import com.elsagdatamat.dbtrace.bridge.engine.entities.StatusPiSdaPK;
import com.elsagdatamat.framework.dao.IGenericDAO;
/** 
* R.L. BeanGenerator - 22.05.2012 03:10:16
*/ 
public interface IStatusPiSdaDAO extends IGenericDAO<StatusPiSda,StatusPiSdaPK>{

}