package com.selexelsag.xte.persistence.dao;

import static org.hibernate.criterion.Example.create;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.selexelsag.xte.persistence.exceptions.PersistenceException;

@Transactional
public class BaseDAO<T> 
{
	private static final Log log = LogFactory.getLog(BaseDAO.class);

	private Class<T> entityClass;
	
	@Autowired
	@Qualifier("xteDbSessionFactory")
	SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) 
	{
		this.sessionFactory = sessionFactory;
	}
	
	public BaseDAO(Class<T> entityClass) 
	{
		this.entityClass = entityClass;
	}

	public void persist(T transientInstance) throws PersistenceException 
	{
		log.debug("persisting " + entityClass.getSimpleName() + " instance");
		try 
		{
			sessionFactory.getCurrentSession().persist(transientInstance);
		}
		catch(RuntimeException re) 
		{
			PersistenceException pe = new PersistenceException("DB Error", re);
			throw pe;
		}
		log.debug("persist successful");
	}

	public void attachDirty(T instance) throws PersistenceException 
	{
		log.debug("attaching dirty " + entityClass.getSimpleName() + " instance");

		try 
		{
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
		}
		catch(RuntimeException re) 
		{
			PersistenceException pe = new PersistenceException("DB Error", re);
			throw pe;
		}
		log.debug("attach successful");
	}

	public void attachClean(T instance) throws PersistenceException 
	{
		log.debug("attaching clean " + entityClass.getName() + " nstance");
		try 
		{
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
		}
		catch(RuntimeException re) 
		{
			PersistenceException pe = new PersistenceException("DB Error", re);
			throw pe;
		}
		log.debug("attach successful");
	}

	public void delete(T persistentInstance) throws PersistenceException 
	{
		log.debug("deleting " + entityClass.getSimpleName() + " instance");
		try 
		{
			sessionFactory.getCurrentSession().delete(persistentInstance);
		}
		catch(RuntimeException re) 
		{
			PersistenceException pe = new PersistenceException("DB Error", re);
			throw pe;
		}
		log.debug("delete successful");
	}

	@SuppressWarnings("unchecked")
	public T merge(T detachedInstance) throws PersistenceException 
	{
		log.debug("merging " + entityClass.getSimpleName() + " instance");
		if (sessionFactory.getCurrentSession().contains(detachedInstance)) 
		{
			log.debug("instance currently on session, lockmode is " + sessionFactory.getCurrentSession().getCurrentLockMode(detachedInstance));
		}
		T result = null;
		try 
		{
			result = (T) sessionFactory.getCurrentSession()
				.merge(detachedInstance);
		}
		catch(RuntimeException re) 
		{
			PersistenceException pe = new PersistenceException("DB Error", re);
			throw pe;
		}
		log.debug("merge successful");
		return result;
	}

	public T findById(long id) throws PersistenceException 
	{
		return findById(id, LockMode.NONE);
	}
	
	@SuppressWarnings("unchecked")
	public T findById(long id, LockMode lockMode) throws PersistenceException 
	{
		log.debug("getting " + entityClass.getSimpleName() + " instance with id: " + id);
		T instance;
		try 
		{
			instance = (T) sessionFactory.getCurrentSession()
					.get(entityClass.getName(),	id, lockMode);
		}
		catch(RuntimeException re) 
		{
			PersistenceException pe = new PersistenceException("DB Error", re);
			throw pe;
		}
		if (instance == null) 
		{
			log.debug("get successful, no instance found");
		}
		else 
		{
			log.debug("get successful, instance found");
		}
		return instance;

	}
	
	@SuppressWarnings("unchecked")
	public List<T> findAll() throws PersistenceException 
	{
		log.debug("findAll " + entityClass.getSimpleName() + " ");

		List<T> list;
		try 
		{
			
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(entityClass.getName());
			list = (List<T>)criteria.list();
		}
		catch(RuntimeException re) 
		{
			PersistenceException pe = new PersistenceException("DB Error", re);
			throw pe;
		}
		if (list == null) 
		{
			log.debug("findAll successful, no instance found");
		}
		else 
		{
			log.debug("findAll successful, instance found");
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<T> findByExample(T instance) throws PersistenceException 
	{
		log.debug("finding " + entityClass.getSimpleName() + " by example");

		List<T> results;
		
		try 
		{
			results = (List<T>) sessionFactory
				.getCurrentSession()
				.createCriteria(
					entityClass.getName())
					.add(create(instance)).list();	
		}
		catch(RuntimeException re) 
		{
			PersistenceException pe = new PersistenceException("DB Error", re);
			throw pe;
		}
		log.debug("find by example successful, result size: "
				+ results.size());
		return results;
	}
	
	public List<T> findByCriterion(Criterion criterion, int maxResults) throws PersistenceException 
	{
		return findByCriterion(criterion, false, maxResults);
	}
	
	public List<T> findByCriterion(Criterion criterion, boolean lockResults, int maxResults) throws PersistenceException 
	{
		Criteria criteria = sessionFactory.getCurrentSession()
				.createCriteria(entityClass.getName());
		criteria = criteria.add(criterion);
		criteria = criteria.setMaxResults(maxResults);
		if (lockResults) 
		{
			criteria = criteria.setLockMode(LockMode.UPGRADE);
		}
		return (List<T>)findByCriteria(criteria);
	}
	
	@SuppressWarnings("unchecked")
	public List findByCriteria(Criteria criteria) throws PersistenceException 
	{
		log.debug("finding " + entityClass.getSimpleName() + " instance by criteria");

		List<T> results;
		
		try 
		{
			results = (List<T>)criteria.list();
		}
		catch(RuntimeException re) 
		{
			PersistenceException pe = new PersistenceException("DB Error", re);
			throw pe;
		}
		log.debug("find by example successful, result size: "
				+ results.size());
		return results;
	}
	
	public void flush() throws PersistenceException 
	{
		try 
		{
			sessionFactory.getCurrentSession().flush();
		}
		catch (RuntimeException re) 
		{
			PersistenceException pe = new PersistenceException("DB Error", re);
			throw pe;
		}
	}
}
