package com.selexelsag.xtetesttool.xml.postaregistrata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AM">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AC">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="NM">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LM">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LO">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PO">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CM">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "am",
    "ac",
    "sp",
    "ap",
    "pa",
    "nm",
    "lm",
    "lo",
    "po",
    "cm"
})
@XmlRootElement(name = "PostaRegistrata")
public class PostaRegistrata {

    @XmlElement(name = "AM", required = true)
    protected PostaRegistrata.AM am;
    @XmlElement(name = "AC", required = true)
    protected PostaRegistrata.AC ac;
    @XmlElement(name = "SP", required = true)
    protected PostaRegistrata.SP sp;
    @XmlElement(name = "AP", required = true)
    protected PostaRegistrata.AP ap;
    @XmlElement(name = "PA", required = true)
    protected PostaRegistrata.PA pa;
    @XmlElement(name = "NM", required = true)
    protected PostaRegistrata.NM nm;
    @XmlElement(name = "LM", required = true)
    protected PostaRegistrata.LM lm;
    @XmlElement(name = "LO", required = true)
    protected PostaRegistrata.LO lo;
    @XmlElement(name = "PO", required = true)
    protected PostaRegistrata.PO po;
    @XmlElement(name = "CM", required = true)
    protected PostaRegistrata.CM cm;

    /**
     * Gets the value of the am property.
     * 
     * @return
     *     possible object is
     *     {@link PostaRegistrata.AM }
     *     
     */
    public PostaRegistrata.AM getAM() {
        return am;
    }

    /**
     * Sets the value of the am property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostaRegistrata.AM }
     *     
     */
    public void setAM(PostaRegistrata.AM value) {
        this.am = value;
    }

    /**
     * Gets the value of the sp property.
     * 
     * @return
     *     possible object is
     *     {@link PostaRegistrata.SP }
     *     
     */
    public PostaRegistrata.SP getSP() {
        return sp;
    }

    /**
     * Sets the value of the sp property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostaRegistrata.SP }
     *     
     */
    public void setSP(PostaRegistrata.SP value) {
        this.sp = value;
    }
    
    /**
     * Gets the value of the ap property.
     * 
     * @return
     *     possible object is
     *     {@link PostaRegistrata.AP }
     *     
     */
    public PostaRegistrata.AP getAP() {
        return ap;
    }

    /**
     * Sets the value of the ap property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostaRegistrata.AP }
     *     
     */
    public void setAP(PostaRegistrata.AP value) {
        this.ap = value;
    }
    
    /**
     * Gets the value of the pa property.
     * 
     * @return
     *     possible object is
     *     {@link PostaRegistrata.PA }
     *     
     */
    public PostaRegistrata.PA getPA() {
        return pa;
    }

    /**
     * Sets the value of the pa property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostaRegistrata.PA }
     *     
     */
    public void setPA(PostaRegistrata.PA value) {
        this.pa = value;
    }
    
    /**
     * Gets the value of the ac property.
     * 
     * @return
     *     possible object is
     *     {@link PostaRegistrata.AC }
     *     
     */
    public PostaRegistrata.AC getAC() {
        return ac;
    }

    /**
     * Sets the value of the ac property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostaRegistrata.AC }
     *     
     */
    public void setAC(PostaRegistrata.AC value) {
        this.ac = value;
    }

    /**
     * Gets the value of the nm property.
     * 
     * @return
     *     possible object is
     *     {@link PostaRegistrata.NM }
     *     
     */
    public PostaRegistrata.NM getNM() {
        return nm;
    }

    /**
     * Sets the value of the nm property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostaRegistrata.NM }
     *     
     */
    public void setNM(PostaRegistrata.NM value) {
        this.nm = value;
    }

    /**
     * Gets the value of the lm property.
     * 
     * @return
     *     possible object is
     *     {@link PostaRegistrata.LM }
     *     
     */
    public PostaRegistrata.LM getLM() {
        return lm;
    }

    /**
     * Sets the value of the lm property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostaRegistrata.LM }
     *     
     */
    public void setLM(PostaRegistrata.LM value) {
        this.lm = value;
    }

    /**
     * Gets the value of the lo property.
     * 
     * @return
     *     possible object is
     *     {@link PostaRegistrata.LO }
     *     
     */
    public PostaRegistrata.LO getLO() {
        return lo;
    }

    /**
     * Sets the value of the lo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostaRegistrata.LO }
     *     
     */
    public void setLO(PostaRegistrata.LO value) {
        this.lo = value;
    }

    /**
     * Gets the value of the po property.
     * 
     * @return
     *     possible object is
     *     {@link PostaRegistrata.PO }
     *     
     */
    public PostaRegistrata.PO getPO() {
        return po;
    }

    /**
     * Sets the value of the po property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostaRegistrata.PO }
     *     
     */
    public void setPO(PostaRegistrata.PO value) {
        this.po = value;
    }

    /**
     * Gets the value of the cm property.
     * 
     * @return
     *     possible object is
     *     {@link PostaRegistrata.CM }
     *     
     */
    public PostaRegistrata.CM getCM() {
        return cm;
    }

    /**
     * Sets the value of the cm property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostaRegistrata.CM }
     *     
     */
    public void setCM(PostaRegistrata.CM value) {
        this.cm = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class AC {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class AM {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }
    
    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class SP {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }
    
    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class AP {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class PA {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class CM {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class LM {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class LO {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class NM {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class PO {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }

}
