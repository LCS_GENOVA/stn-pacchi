package com.elsagdatamat.dbtrace.bridge.tocdg;

public class ToCDGM2Token extends ToCDGToken {
	public static final String TOKEN_ID="DBTraceToCDGM2Bridge";
	
	@Override
	public String getServiceId() {
		return TOKEN_ID;
	}
}
