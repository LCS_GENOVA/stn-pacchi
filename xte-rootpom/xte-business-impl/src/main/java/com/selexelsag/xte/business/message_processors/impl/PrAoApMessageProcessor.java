package com.selexelsag.xte.business.message_processors.impl;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.DatatypeConverter;

import com.leonardo.core.log.Bpm;
import com.selexelsag.tracktrace.extension.objectmodel.message.xte.*;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.NodeList;

import com.selexelsag.messagetobedelivered.entity.MessageToBeDelivered;
import com.selexelsag.messagetobedelivered.vo.AnagBodyFormat;
import com.selexelsag.messagetobedelivered.vo.AnagHeaderConstants;
import com.selexelsag.messagetobedelivered.vo.AnagMessageType;
import com.selexelsag.messagetobedelivered.vo.AnagSistema;
import com.selexelsag.messagetobedelivered.vo.MessageDestinationType;

import com.selexelsag.tracktrace.extension.objectmodel.AreaDom;
import com.selexelsag.tracktrace.extension.objectmodel.ChiaveDom;
import com.selexelsag.tracktrace.extension.objectmodel.PhaseEnum;
import com.selexelsag.tracktrace.extension.objectmodel.RegModeDom;
import com.selexelsag.tracktrace.extension.objectmodel.StatusDom;
import com.selexelsag.tracktrace.extension.objectmodel.SubCatDom;
import com.selexelsag.tracktrace.extension.objectmodel.message.AccMessage;
import com.selexelsag.tracktrace.extension.objectmodel.message.HdrMessage;
import com.selexelsag.tracktrace.extension.objectmodel.message.ObjMessage;
import com.selexelsag.tracktrace.extension.objectmodel.message.RegMessage;
import com.selexelsag.tracktrace.extension.objectmodel.message.m.m1.M1C;
import com.selexelsag.tracktrace.extension.objectmodel.message.m.m1.M1CHelper;
import com.selexelsag.tracktrace.extension.objectmodel.message.m.m1.M1DBTracce;
import com.selexelsag.tracktrace.extension.objectmodel.message.m.m1.M1DBTracceHelper;
import com.selexelsag.tracktrace.extension.objectmodel.message.m.m1.M1DC;
import com.selexelsag.xte.business.exceptions.BusinessMessageProcessorException;
import com.selexelsag.xte.business.exceptions.BusinessParserException;
import com.selexelsag.xte.business.tracking.IMessageDBManager;
import com.selexelsag.xte.business.utility.XmlParser;
import com.selexelsag.xte.model.message_processor.MessageProcessor;
import com.selexelsag.xte.persistence.XteMessagesDBAccess;
import com.selexelsag.xte.persistence.entities.AdditionalServices;
import com.selexelsag.xte.persistence.entities.AsyncMessages;
import com.selexelsag.xte.persistence.exceptions.PersistenceException;
import com.selexelsag.xte.service.context.SpringApplicationContext;
import com.selexelsag.xte.service.resource.IMessageManager;

public class PrAoApMessageProcessor implements MessageProcessor {

	private static Logger logger = Logger.getLogger(PrAoApMessageProcessor.class);

	private static final String flowId = "FID_PR_AO_AP";

	private Properties properties;

	private XteMessagesDBAccess dbAccess;

	private IMessageDBManager messageManager;

	private Map<Character, Character> additionalServicesMap = null;
	
	private void bpmLogOk(AccettazionePezzi accettazionePezzi, Date dataTracce) {
		
		bpmLogError(accettazionePezzi, dataTracce, Bpm.Esito.OK);
		
	}
	
	private void bpmLogError(AccettazionePezzi accettazionePezzi, Date dataTracce, String errorMessage) {
		if (accettazionePezzi == null) return;
		List<AccettazioneObj> objs = accettazionePezzi.getObjs();
		for (AccettazioneObj obj : objs) {
			Bpm.logInvioInfo(Bpm.Evento.ACCETTAZIONE_XTE, obj.getFrazionario(), dataTracce, obj.getCodice(), errorMessage);
		}
	}
	
	@Override
	public void setMessageSender(IMessageDBManager messageManagerBase) {
		this.messageManager = messageManagerBase;
	}

	@Override
	public String getFlowId() {
		return flowId;
	}

	private String mapToCdc(String codiceSA) {
		StringBuilder cdgCode = new StringBuilder();
		if (codiceSA==null) {
			return null;
		}

		for (int i=0;i<codiceSA.length();i++) {
			try {
				char saCode = codiceSA.charAt(i);
				if (additionalServicesMap.get(saCode) != null)
					cdgCode.append(additionalServicesMap.get(saCode));
				else
					logger.error("Servizio Aggiuntivo sconosciuto : " + saCode + " (servizio non inoltrato al CdG)");
			} catch (Exception e) {
				logger.error("Errore nella lettura codice Cdg da db", e);
				return null;
			}
		}
		return cdgCode.toString();
	}

	@Override
	public void processMessage(AsyncMessages message)  throws BusinessMessageProcessorException{
		logger.debug("processMessage() - processing id: " + message.getId() );

		AccettazionePezziSaxParser saxParser = new AccettazionePezziSaxParser();
		AccettazionePezzi accettazionePezzi = saxParser.parse(message.getOriginalMessage());

		Date dataTracce = new Date();

		List<AdditionalServices> aServices = null;
		try {
			aServices = dbAccess.getAdditionalServices();

			additionalServicesMap = new HashMap<Character, Character>();
			for (AdditionalServices additionalServices : aServices) {
				additionalServicesMap.put(additionalServices.getPerif(), additionalServices.getCdg());
			}
		} catch (PersistenceException e1) {
			logger.error("Error getting Additional Services");
		}

		XmlParser parser = new XmlParser();
		try {
			parser.init(message.getOriginalMessage());
		} catch (BusinessParserException e) {
			BusinessMessageProcessorException mpe = new BusinessMessageProcessorException("Errore di inizializzazione parser", e);
			throw mpe;
		}

		String datiInvioQuery = properties.getProperty(flowId + ".datiInvioQuery");
		NodeList datiInvioList;
		try {
			datiInvioList = parser.getNodeListQuery(datiInvioQuery);
		} catch (BusinessParserException e) {
			BusinessMessageProcessorException mpe = new BusinessMessageProcessorException("Errore nell'analisi del messaggio", e);
			throw mpe;
		}

		StringBuilder error = new StringBuilder();

		boolean resend = false;

		List<MessageToBeDelivered> mtbdList = new ArrayList<MessageToBeDelivered>();

		logger.debug("Processing datiInvio list start");

		for (int i = 0; i < datiInvioList.getLength(); i++) {
			NodeList datiInvioFieldsList = datiInvioList.item(i).getChildNodes();
			HashMap<String, String> datiInvioFields = new HashMap<String, String>();

			for (int j = 0; j < datiInvioFieldsList.getLength(); j++) {
				String fieldName = datiInvioFieldsList.item(j).getNodeName();
				String fieldValue = StringEscapeUtils.escapeXml(datiInvioFieldsList.item(j).getTextContent());
				logger.debug("DatiInvio " + i + ": " + fieldName + "=" + fieldValue);
				datiInvioFields.put(fieldName, fieldValue);
			}

			String codiceMessaggio = datiInvioFields.get("Codice");
//			23/04/2015 commentato perche' la validazione fatta sul web service sincrono controlla gia' questo campo
//			if (codiceMessaggio==null) {
//				logger.error("Campo obbligatorio \"Codice\" assente! il messaggio non sarà inviato");
//				error.append("Msg pos " + i + ": no Codice;\n");
//				continue;
//			}

			String tipoMessaggio = datiInvioFields.get("TipoMessaggio");
//			23/04/2015 commentato perche' la validazione fatta sul web service sincrono controlla gia' questo campo
//			if (tipoMessaggio==null) {
//				logger.error("Campo obbligatorio \"TipoMessaggio\" assente! il messaggio non sarà inviato");
//				error.append(codiceMessaggio + ": no TipoMessaggio;\n");
//				continue;
//			}
			if (tipoMessaggio.contains("P")) {
				logger.error("TipoMessaggio non può valere \"P\", il messaggio non sarà inviato");
				error.append(codiceMessaggio + ": TipoMessaggio=P;\n");
				continue;
			}

			// ******************** UTILIZZO LIBRERIA COMUNE DI GENERAZIONE MESSAGGI			
			String messageToCdG = foo(EM1.M1_PER_IL_CDG, datiInvioFields, message.getReceivedTimestamp(), message.getDestinationOfficeId());
			String messageToPacchi = foo(EM1.M1_PER_IL_DBTRACCE, datiInvioFields, message.getReceivedTimestamp(), message.getDestinationOfficeId());
			String messageToAnagrafica = foo(EM1.M1_PER_ANAGRAFICA, datiInvioFields, message.getReceivedTimestamp(), message.getDestinationOfficeId());
			HashMap<String, Serializable> additionalToCdG = bar(EM1.M1_PER_IL_CDG, datiInvioFields, message.getReceivedTimestamp(), message.getDestinationOfficeId());
			HashMap<String, Serializable> additionalToPacchi = bar(EM1.M1_PER_IL_DBTRACCE, datiInvioFields, message.getReceivedTimestamp(), message.getDestinationOfficeId());
			HashMap<String, Serializable> additionalToAnagrafica = bar(EM1.M1_PER_ANAGRAFICA, datiInvioFields, message.getReceivedTimestamp(), message.getDestinationOfficeId());
			
			if (messageToCdG == null) {
				logger.error("Non sono stati valorizzati tutti i campi obbligatori del messaggio");
				error.append(codiceMessaggio + ": campi incompleti\n");
				continue;
			}

			Boolean isAPack = false;
			try {
				isAPack = dbAccess.getIsAPack(datiInvioFields.get("Prodotto"));
				if (isAPack == null) {
					logger.error("Prodotto non riconosciuto");
					error.append(codiceMessaggio + ": \"Prodotto\" sconosciuto\n");
					continue;
				}
			} catch (PersistenceException e) {
				logger.error("Tabella \"PRODUCTS\" non raggiungibile");
				error.append(codiceMessaggio + ": DBERR lettura \"Prodotto\"\n");
				continue;
			}
			
			boolean anagraficaEnabled = properties.getProperty(flowId + ".anagrafica.enable").equals("1");

			boolean cdgCompressed = properties.getProperty(flowId + ".CdG.compression").equals("1");
			boolean centralePacchiCompressed = properties.getProperty(flowId + ".centralePacchi.compression").equals("1");
			boolean anagraficaCompressed = properties.getProperty(flowId + ".anagrafica.compression").equals("1");

			String cdgQueueName = properties.getProperty(flowId + ".CdG.queueName");
			String centralePacchiQueueName = properties.getProperty(flowId + ".centralePacchi.queueName");
			String anagraficaQueueName = properties.getProperty(flowId + ".anagrafica.queueName");
			String packMessageType = properties.getProperty(flowId + ".packMessageType");
			String anagraficaMessageType = properties.getProperty(flowId + ".anagraficaMessageType");
			String messageType = properties.getProperty(flowId + ".trackingMessageType");

			if (isAPack) {
				try {
					logger.trace("Adding to messageList cdg: \n" + messageToCdG + "\n Adding to messageList cdg" + messageToPacchi);
					mtbdList.add(messageManager.buildMessage(MessageDestinationType.CENTRALE, messageToCdG, messageType, cdgQueueName, cdgCompressed, additionalToCdG));
					mtbdList.add(messageManager.buildMessage(MessageDestinationType.TRACEDB, messageToPacchi, packMessageType, centralePacchiQueueName, centralePacchiCompressed, additionalToPacchi));
					if (anagraficaEnabled)
						mtbdList.add(messageManager.buildMessage(MessageDestinationType.ANAGRAFICA, messageToAnagrafica, anagraficaMessageType, anagraficaQueueName, anagraficaCompressed, additionalToAnagrafica));
				} catch(Exception ex) {
					logger.error("Error processing message id: " + message.getId(),ex);
					error.append(codiceMessaggio + ": errore DB\n");
					resend = true;
				}
			} else { 
				try {
					logger.trace("Adding to messageList: \n" + messageToCdG);
					mtbdList.add(messageManager.buildMessage(MessageDestinationType.CENTRALE, messageToCdG, messageType, cdgQueueName, cdgCompressed, additionalToCdG));
					if (anagraficaEnabled)
						mtbdList.add(messageManager.buildMessage(MessageDestinationType.ANAGRAFICA, messageToAnagrafica, anagraficaMessageType, anagraficaQueueName, anagraficaCompressed, additionalToAnagrafica));
				} catch(Exception ex) {
					logger.error("Error processing message id: " + message.getId(),ex);
					error.append(codiceMessaggio + ": errore DB\n");
					resend = true;
				}
			}
		}
		logger.debug("Processing datiInvio list end");

		if(error.length() > 0){

			message.setErrDescr("[" + new Date() + "] " + error.toString());

			try {
				//invio le tracce buone e sposto il record nei messaggi in errore
				//per evitare di riprocessare gli stessi invii in quanto alcune tracce vanno in errore
				messageManager.sendMultipleMessageAndMoveToDiscardedMessages(message, mtbdList, error.toString());
				
			} catch (Exception e) {
				logger.error("Error sending multiple message, async_message id: " + message.getId(),e);
			}
			logger.error(error);
			
			bpmLogError(accettazionePezzi, dataTracce, error.toString());
			return;
		}else {
			try {
				messageManager.sendMultipleMessage(message, mtbdList, true);
			} catch (Exception e) {
				String errorMessage = "Error sending multiple message, async_message id: " + message.getId();
				message.setErrDescr("[" + new Date() + "]" + e.getMessage());
				logger.error(errorMessage,e);

				//AGGIORNO il campo "receivedTimestamp" per fare andare in coda il messaggio se in errore e permettere l'elaborazione degli altri
				message.setReceivedTimestamp(dataTracce);
				message.setSendTimestamp(null);
				
				bpmLogError(accettazionePezzi, dataTracce, errorMessage);
				try {
					dbAccess.updateMessages(message);
				} catch (PersistenceException ee) {
					BusinessMessageProcessorException bmpe = new BusinessMessageProcessorException("Error saving to db", ee);
					throw bmpe;
				}
			}
		}
		bpmLogOk(accettazionePezzi, new Date());
		logger.debug("processMessage() - end");
	}

	private enum EM1 {
		M1_PER_ANAGRAFICA,
		M1_PER_IL_CDG,
		M1_PER_IL_DBTRACCE;
	}
	
	private String foo(EM1 m1Type, HashMap<String, String> datiInvioFields, Date receivedTimestamp, String destinationOfficeId)
	{
		StringBuilder error = new StringBuilder();

		String sa = datiInvioFields.get("SA");
		String codiciAS = null;
		if (sa!=null) {
			codiciAS = mapToCdc(sa);
			if (codiciAS==null) {
				logger.error("SA contiene codici non riconosciuti, il messaggio non sarà inviato");
				error.append(datiInvioFields.get("Codice") + ": AS sconosciuto;\n");
				return null;
			}
		}

		if ((datiInvioFields.get("PIVA") != null) && (datiInvioFields.get("CodiceFiscale") != null)) {
			logger.error("PIVA e CodiceFiscale presenti entrambi, il messaggio non sarà inviato");
			return null;
		}
		ChiaveDom tipologiaChiave = null;
		String chiaveCliente = null;
		if (datiInvioFields.get("CodiceFiscale") != null) {
			tipologiaChiave = ChiaveDom.CODICE_FISCALE;
			chiaveCliente = datiInvioFields.get("CodiceFiscale");
		}
		if (datiInvioFields.get("PIVA") != null) {
			tipologiaChiave = ChiaveDom.PARTITA_IVA;
			chiaveCliente = datiInvioFields.get("PIVA");
		}
	
		String prodotto = datiInvioFields.get("Prodotto");
		SubCatDom scd = null;
		if (prodotto != null)
			scd = SubCatDom.valueOf(prodotto);
		
		String latitudine = datiInvioFields.get("Latitudine");
		Double lat = null;
		if (latitudine != null)
			lat = Double.parseDouble(latitudine);
			
		String longitudine = datiInvioFields.get("Longitudine");
		Double longit = null;
		if (longitudine != null)
			longit = Double.parseDouble(longitudine);
		
		ObjMessage.Builder builder = new ObjMessage.Builder(
			datiInvioFields.get("Codice"), 
			PhaseEnum.ACCETTAZIONE,
			StatusDom.S0,
			1L,
			receivedTimestamp,
			scd);
		if (longitudine != null)
			builder.longitude(Double.parseDouble(longitudine));
		
		if (latitudine != null)
			builder.latitude(Double.parseDouble(latitudine));
		
		if (datiInvioFields.get("CodiceCliente") != null)
			builder.customCode(datiInvioFields.get("CodiceCliente"));
		
		if (tipologiaChiave != null)
			builder.tipologiaChiave(tipologiaChiave);
		
		if (chiaveCliente != null)
			builder.chiaveCliente(chiaveCliente);
		
		ObjMessage objMsg = builder.areaDest(AreaDom.TERMINALE_PORTALETTERE).build();
		
		List<M1DC> objects = new ArrayList<M1DC>();
		
		String dataOraRitiroStr = datiInvioFields.get("DataOraRitiro");
		Calendar dataOraRitiroCalendar = null;
		Date dataOraRitiroDate = null;
		if (dataOraRitiroStr != null) {
			try {
				dataOraRitiroCalendar = DatatypeConverter.parseDateTime(dataOraRitiroStr);
				dataOraRitiroDate = dataOraRitiroCalendar.getTime();
			}catch (IllegalArgumentException iae) {
				dataOraRitiroDate = null;
			}
		}
		
		String prezzo = datiInvioFields.get("Prezzo");
		Long prz = null;
		if (prezzo != null)
			prz = Long.parseLong(prezzo);
		
		String percetto = datiInvioFields.get("Percetto");
		Long perc = null;
		if (percetto != null)
			perc = Long.parseLong(percetto);
		
		String peso = datiInvioFields.get("Peso");
		Integer ps = null;
		if (peso != null)
			ps = Integer.parseInt(peso);
		
		
		AccMessage accMessage = null;
		AccMessage.Builder accBuilder = new AccMessage.Builder();
		
		if (datiInvioFields.get("OfficeFrazionario") != null)
			accBuilder.aofc(datiInvioFields.get("OfficeFrazionario"));

		if (dataOraRitiroDate != null)
			accBuilder.adt(dataOraRitiroDate);
		
		if (datiInvioFields.get("CodiceAR") != null)
			accBuilder.arid(datiInvioFields.get("CodiceAR"));

		if (datiInvioFields.get("Cap") != null)
			accBuilder.zip(datiInvioFields.get("Cap"));

		if (prz != null)
			accBuilder.fare(prz);
				
		if (perc != null)
			accBuilder.perc(perc);
		
		if (ps != null)
			accBuilder.w(ps);
				
		accBuilder.regMode(RegModeDom.DA_UFFICIO_NON_AUTOMATIZZATO);

		switch (m1Type) {
			case M1_PER_ANAGRAFICA:
			case M1_PER_IL_CDG:
				accBuilder.sa(codiciAS);
				break;
			case M1_PER_IL_DBTRACCE:
				accBuilder.sa(sa);
				break;
		}
		
		accMessage = accBuilder.build();
		
		String valoreAssicurato = datiInvioFields.get("ValoreAssicurato");
		Integer vas = null;
		if (valoreAssicurato != null)
			vas = Integer.parseInt(valoreAssicurato);
		
		String importoContrassegno = datiInvioFields.get("ImportoContrassegno");
		Integer ic = null;
		if (importoContrassegno != null)
			ic = Integer.parseInt(importoContrassegno);
		
		RegMessage.Builder regBuilder = new RegMessage.Builder();

		if (vas != null)
			regBuilder.insVal(vas);
		
		if (ic != null)
			regBuilder.codVal(ic);
		
		if (datiInvioFields.get("Destinazione") != null)
			regBuilder.dest(datiInvioFields.get("Destinazione"));

		if (datiInvioFields.get("Destinatario") != null)
			regBuilder.addr(datiInvioFields.get("Destinatario"));
		
		if (datiInvioFields.get("Indirizzo") != null)
			regBuilder.daddr(datiInvioFields.get("Indirizzo"));
		
		RegMessage reg = regBuilder.build();
		

		StringBuilder sb = new StringBuilder();
		sb.append("<PARAM><![CDATA[<PARAMETRI>");
		sb.append("<OPER caption=\"Id Operatore\">" + datiInvioFields.get("IdOperatore") + "</OPER>");
		// Facoltativi
		if (datiInvioFields.get("NominativoMittente") != null) 
			sb.append("<MNOM caption=\"Nominativo Mittente\">" + datiInvioFields.get("NominativoMittente") + "</MNOM>");
		if (datiInvioFields.get("DestinazioneMittente") != null) 
			sb.append("<MDES caption=\"Destinazione Mittente\">" + datiInvioFields.get("DestinazioneMittente") + "</MDES>");
		if (datiInvioFields.get("IndirizzoMittente") != null) 
			sb.append("<MIND caption=\"Indirizzo Mittente\">" + datiInvioFields.get("IndirizzoMittente") + "</MIND>");
		if (datiInvioFields.get("CapMittente") != null) 
			sb.append("<MCAP caption=\"Cap Mittente\">" + datiInvioFields.get("CapMittente") + "</MCAP>");
		if (datiInvioFields.get("IBAN") != null) 
			sb.append("<IBAN caption=\"IBAN\">" + datiInvioFields.get("IBAN") + "</IBAN>");
		if (datiInvioFields.get("OraTimeDefinite") != null) 
			sb.append("<OTDE caption=\"Ora Time Definite\">" + datiInvioFields.get("OraTimeDefinite") + "</OTDE>");
		sb.append("</PARAMETRI>]]></PARAM>\n");
		String param = sb.toString();
		
		M1DC m1dObj = new M1DC.Builder(objMsg, param, accMessage, reg).build();

		objects.add(m1dObj);

		HdrMessage hdr = new HdrMessage.Builder(destinationOfficeId, "A0000").build();

		switch (m1Type) {
			case M1_PER_ANAGRAFICA:
				M1C m1Anagrafica = new M1C.Builder(hdr, objects).build();
				String messageToAnagrafica = M1CHelper.createM1CObjectAsString(m1Anagrafica);
				return messageToAnagrafica;
			case M1_PER_IL_CDG:
				M1C m1c = new M1C.Builder(hdr, objects).build();
				String messageToCdG = M1CHelper.createM1CObjectAsString(m1c);
				return messageToCdG;	
			case M1_PER_IL_DBTRACCE:
				M1DBTracce m1DBTracce = new M1DBTracce.Builder(hdr, objects).build();
				String messageToPacchi = M1DBTracceHelper.createM1DbTracceObjectAsString(m1DBTracce);
				return messageToPacchi;
		}
		
		return null;
	}
	
	private HashMap<String, Serializable> bar(EM1 m1Type, HashMap<String, String> datiInvioFields, Date receivedTimestamp, String destinationOfficeId) {
		HashMap<String, Serializable> additionalData = new HashMap<String, Serializable>();
		if (m1Type.equals(EM1.M1_PER_ANAGRAFICA)) {
			additionalData.put(AnagHeaderConstants.ANAG_BODY_FORMAT, AnagBodyFormat.XML.value());
			additionalData.put(AnagHeaderConstants.ANAG_MESSAGE_TYPE, AnagMessageType.M1.value());
			additionalData.put(AnagHeaderConstants.ANAG_SISTEMA, AnagSistema.XTE.value());
			return additionalData;
		}
		return additionalData;
	}

	@Override
	public void setDBAccess(XteMessagesDBAccess dbAccess) {
		this.dbAccess = dbAccess;
	}

	@Override
	public void setMessageProcessorProperties(Properties properties) {
		this.properties = properties;
	}

	@Override
	public String buildAckResponse(Map<String, String> extractedData) {
		IMessageManager messageManager = (IMessageManager)SpringApplicationContext.
				getBean(IMessageManager.TRACK_MSG_MANAGER_BEAN);

		logger.debug("buildAckResponse(" + extractedData + ")");

		StringBuilder resultString = new StringBuilder();

		String result = messageManager.getMessageText(IMessageManager.RESULT_POSITIVO_KEY);
		String descResult = messageManager.getMessageText(IMessageManager.RESULT_OK_KEY);
		String flowReiteration = messageManager.getMessageText(IMessageManager.RESULT_REITERATION_N_KEY);

		resultString.append("<Risultato Esito=\"" + result + "\" ");
		resultString.append("DescrizioneEsito=\"" + descResult + "\" ");
		resultString.append("CodiceEsito=\"PA000\" ");
		resultString.append("Reiterabilita=\"" + flowReiteration + "\">");

		resultString.append("</Risultato>");

		return resultString.toString();
	}
}
