
package com.elsagdatamat.trackdbws.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Token_TilltoDateByFiltersList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Token_TilltoDateByFiltersList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}Token"/>
 *         &lt;element name="tilltoDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="tracesFilterByExampleEqual" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}TracesList"/>
 *         &lt;element name="tracesFilterByExampleNotEqual" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}TracesList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Token_TilltoDateByFiltersList", propOrder = {
    "token",
    "tilltoDate",
    "tracesFilterByExampleEqual",
    "tracesFilterByExampleNotEqual"
})
public class TokenTilltoDateByFiltersList {

    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar tilltoDate;
    @XmlElement(required = true, nillable = true)
    protected TracesList tracesFilterByExampleEqual;
    @XmlElement(required = true, nillable = true)
    protected TracesList tracesFilterByExampleNotEqual;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the tilltoDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTilltoDate() {
        return tilltoDate;
    }

    /**
     * Sets the value of the tilltoDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTilltoDate(XMLGregorianCalendar value) {
        this.tilltoDate = value;
    }

    /**
     * Gets the value of the tracesFilterByExampleEqual property.
     * 
     * @return
     *     possible object is
     *     {@link TracesList }
     *     
     */
    public TracesList getTracesFilterByExampleEqual() {
        return tracesFilterByExampleEqual;
    }

    /**
     * Sets the value of the tracesFilterByExampleEqual property.
     * 
     * @param value
     *     allowed object is
     *     {@link TracesList }
     *     
     */
    public void setTracesFilterByExampleEqual(TracesList value) {
        this.tracesFilterByExampleEqual = value;
    }

    /**
     * Gets the value of the tracesFilterByExampleNotEqual property.
     * 
     * @return
     *     possible object is
     *     {@link TracesList }
     *     
     */
    public TracesList getTracesFilterByExampleNotEqual() {
        return tracesFilterByExampleNotEqual;
    }

    /**
     * Sets the value of the tracesFilterByExampleNotEqual property.
     * 
     * @param value
     *     allowed object is
     *     {@link TracesList }
     *     
     */
    public void setTracesFilterByExampleNotEqual(TracesList value) {
        this.tracesFilterByExampleNotEqual = value;
    }

}
