package com.elsagdatamat.trackdbws.client.impl;

import java.math.BigInteger;

import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.elsagdatamat.trackdbws.client.TokenTilltoDate;
import com.elsagdatamat.trackdbws.client.TokenTilltoDateByFiltersList;
import com.elsagdatamat.trackdbws.client.TracesList;
import com.elsagdatamat.trackdbws.client.TracesListNextToken;
import com.elsagdatamat.trackdbws.client.TrackDBRWServicePortType;

@WebService(
		endpointInterface="com.elsagdatamat.dbtrace.ws.TrackWriterServicePortType", serviceName="TrackWriterServicePortType", 
		targetNamespace="http://dataposta.tt.elsagdatamat.com/trackwriterws/trackwriterservice")
public class TrackDBRWServiceImpl implements TrackDBRWServicePortType{

	Log log = LogFactory.getLog(this.getClass());
	
	@Override
	public BigInteger putTraces(TracesList putTracesInput) {

		log.info("Nuove tracce ricevute: " + putTracesInput.getTraces().size());
		
		return BigInteger.valueOf(0);
	}

	@Override
	public int getChunkSize(String getChunkSizeInput) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getToken(XMLGregorianCalendar getTokenInput) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TracesListNextToken getTraces(TokenTilltoDate getTracesInput) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TracesListNextToken getTracesByFiltersList(
			TokenTilltoDateByFiltersList getTracesByFiltersListInput) {
		// TODO Auto-generated method stub
		return null;
	}

}
