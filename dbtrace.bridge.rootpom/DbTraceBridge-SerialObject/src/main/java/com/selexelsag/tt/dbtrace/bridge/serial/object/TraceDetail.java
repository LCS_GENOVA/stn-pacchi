package com.selexelsag.tt.dbtrace.bridge.serial.object;

import java.io.Serializable;

public class TraceDetail implements Serializable {	
	
	private static final long serialVersionUID = 1L;
	
	private Long traceDetailId;
	
	private String paramClass;
	
	private String paramValue;
	
	
	public void setParamClass(String paramClass) {
		this.paramClass = paramClass;
	}
	public String getParamClass() {
		return paramClass;
	}
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	public String getParamValue() {
		return paramValue;
	}
	public Long getTraceDetailId() {
		return traceDetailId;
	}
	public void setTraceDetailId(Long traceDetailId) {
		this.traceDetailId = traceDetailId;
	}
}
