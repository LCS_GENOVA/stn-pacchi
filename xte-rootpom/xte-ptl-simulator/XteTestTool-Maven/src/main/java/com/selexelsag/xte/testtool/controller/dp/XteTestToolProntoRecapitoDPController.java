/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.dp;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.controller.common.XteTestToolFieldValidator;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XteMailPiece;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.dp.XteTestToolProntoRecapitoDPView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;
import com.selexelsag.xte.testtool.view.dp.XteTestToolWebServiceDPView;
import com.selexelsag.xte.testtool.view.objectmodel.XteMailPieceTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteTestToolDuplicateKeyException;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta.PR;
import com.selexelsag.xtetesttool.xml.dataposta.pr.DatiInvio;
import com.selexelsag.xtetesttool.xml.dataposta.pr.ObjectFactory;
import com.selexelsag.xtetesttool.xml.dataposta.pr.Traccia;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import org.apache.commons.configuration.Configuration;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolProntoRecapitoDPController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener, ListSelectionListener, TableModelListener {

    private XteTestToolProntoRecapitoDPView prontoRecapitoDPView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolProntoRecapitoDPController.class);
    private XteMailPiece selectedMailPiece = null;
    private int currentSelectedRow = -1;
    private List<JTextComponent> mailPieceFieldList = new ArrayList<JTextComponent>();
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    private JAXBContext context;
    
    private static Traccia prontoRecapitoTraccia;

    static {
        logger.debug("Creating Config Object Factory and Traccia");
        DataPosta dataPostaConfig = XteTestToolConfiguration.getInstance().getDataPostaConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        PR config = dataPostaConfig.getPR();

        prontoRecapitoTraccia = objectFactory.createTraccia();
        prontoRecapitoTraccia.setEvento(config.getEventName());
        prontoRecapitoTraccia.setHeaderMazzetto(objectFactory.createHeaderMazzetto());
        prontoRecapitoTraccia.setListaInvii(objectFactory.createListaInvii());
    }

    public XteTestToolProntoRecapitoDPController(JPanel prontoRecapitoDPView) {
        try {
            logger.info("XteTestToolProntoRecapitoDPController start class creation");
            this.prontoRecapitoDPView = (XteTestToolProntoRecapitoDPView) prontoRecapitoDPView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.prontoRecapitoDPView.initializeView();

			updateView();

            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.dataposta.pr");

            logger.info("XteTestToolProntoRecapitoDPController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolProntoRecapitoDPController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolProntoRecapitoDPController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pronto Recapito Data Posta controller creation failed", ex);
        }
    }
    
    private void updateDataModel() {
        String id = prontoRecapitoDPView.getBundleIdTextRef().getText();
        String opid = prontoRecapitoDPView.getBundleTPIdTextRef().getText();
        logger.debug("updateDataModel - " + id + "," + opid);
        prontoRecapitoTraccia.getHeaderMazzetto().setCodiceIdentificativo(new String(id));
        prontoRecapitoTraccia.getHeaderMazzetto().setIdOperatoreTP(new String(opid));
        ObjectFactory objectFactory = new ObjectFactory();
        XteMailPiece[] entries = prontoRecapitoDPView.getMailPieceTableModelRef().getMap().values().toArray(new XteMailPiece[0]);
        logger.debug("entries count: " + entries.length);        
        prontoRecapitoTraccia.getListaInvii().getDatiInvio().clear();
        for (int i=0;i<entries.length;i++) {
        
            XteMailPiece item = entries[i];
            logger.debug("item " + i + ": " + item);
            if (item == null) 
                continue;
            DatiInvio obj = objectFactory.createDatiInvio();
            obj.setCodice(item.getMailPieceCode());
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(item.getMailPieceDateTime());
            try {
                obj.setDataOraScansione(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            } catch (DatatypeConfigurationException ex) {
                logger.error("Error converting date " + c, ex);
                obj.setDataOraScansione(null);
            }
            obj.setLatitudine(item.getMailPieceLatitude());
            obj.setLongitudine(item.getMailPieceLongitude());
            // obj.setAltroUfficio(item.getMailPieceOffice());
            // obj.setTipoCodice(item.getMailPieceType());
            prontoRecapitoTraccia.getListaInvii().getDatiInvio().add(obj);
        }
    }
    
    
    private void updateView() {  
        String id = prontoRecapitoTraccia.getHeaderMazzetto().getCodiceIdentificativo();
        String opid = prontoRecapitoTraccia.getHeaderMazzetto().getIdOperatoreTP();
        logger.debug("updateView - " + id + "," + opid);
                
        if (id != null) {
            prontoRecapitoDPView.getBundleIdTextRef()
                    .setText(id);
        }

        if (opid != null) {
            prontoRecapitoDPView.getBundleTPIdTextRef()
                    .setText(opid);
        }

        prontoRecapitoDPView.getMailPieceTableModelRef().clearModel();
            
        for (int i=0;i<prontoRecapitoTraccia.getListaInvii().getDatiInvio().size();i++) {
            DatiInvio item = prontoRecapitoTraccia.getListaInvii().getDatiInvio().get(i);
            XteMailPiece obj = new XteMailPiece();
            obj.setMailPieceCode(item.getCodice());
            obj.setMailPieceDateTime(item.getDataOraScansione().toGregorianCalendar().getTime());
            obj.setMailPieceLatitude(item.getLatitudine());
            obj.setMailPieceLongitude(item.getLongitudine());
            prontoRecapitoDPView.getMailPieceTableModelRef().addMailPieceItem(obj);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = prontoRecapitoDPView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        prontoRecapitoDPView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.prontoRecapitoDPView.getProntoRacapitoPanelRef().addAncestorListener(this);        
        this.prontoRecapitoDPView.getBundleIdTextRef().addFocusListener(this);
        this.prontoRecapitoDPView.getBundleTPIdTextRef().addFocusListener(this);
        this.prontoRecapitoDPView.getMailPieceCodeTextRef().addFocusListener(this);
        this.prontoRecapitoDPView.getMailPieceDatePickerRef().getEditor().addFocusListener(this);
        JFormattedTextField startTimejFormattedTextField = ((JSpinner.DateEditor)prontoRecapitoDPView.getMailPieceTimeSpinnerRef().getEditor()).getTextField();
        startTimejFormattedTextField.addFocusListener(this);
        this.prontoRecapitoDPView.getMailPieceLatitudeTextRef().addFocusListener(this);
        this.prontoRecapitoDPView.getMailPieceLongitudeTextRef().addFocusListener(this);
        this.prontoRecapitoDPView.getMailPieceTableRef().getSelectionModel().addListSelectionListener(this);
        this.prontoRecapitoDPView.getMailPieceTableRef().getModel().addTableModelListener(this);
        this.prontoRecapitoDPView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.prontoRecapitoDPView.getAggiungiItemButtonRef().addActionListener(this);
        this.prontoRecapitoDPView.getEliminaItemButtonRef().addActionListener(this);
        this.prontoRecapitoDPView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.prontoRecapitoDPView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.prontoRecapitoDPView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.prontoRecapitoDPView.getBackButtonRef().addActionListener(this);
        this.prontoRecapitoDPView.getHomeButtonRef().addActionListener(this);
        this.prontoRecapitoDPView.getExitButtonRef().addActionListener(this);
                
        this.prontoRecapitoDPView.getMailPieceCodeTextRef().getDocument().addDocumentListener(this);
        this.prontoRecapitoDPView.getMailPieceDatePickerRef().getEditor().getDocument().addDocumentListener(this);
        ((JSpinner.DateEditor)prontoRecapitoDPView.getMailPieceTimeSpinnerRef().getEditor()).getTextField().getDocument().addDocumentListener(this);
        this.prontoRecapitoDPView.getMailPieceLatitudeTextRef().getDocument().addDocumentListener(this);
        this.prontoRecapitoDPView.getMailPieceLongitudeTextRef().getDocument().addDocumentListener(this);
        
        mailPieceFieldList.add(prontoRecapitoDPView.getMailPieceCodeTextRef());
        mailPieceFieldList.add(prontoRecapitoDPView.getMailPieceDatePickerRef().getEditor());
        mailPieceFieldList.add(((JSpinner.DateEditor)prontoRecapitoDPView.getMailPieceTimeSpinnerRef().getEditor()).getTextField());
        mailPieceFieldList.add(prontoRecapitoDPView.getMailPieceLatitudeTextRef());
        mailPieceFieldList.add(prontoRecapitoDPView.getMailPieceLongitudeTextRef());
        
		this.salvaXmlFileChooser.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(prontoRecapitoDPView.getAggiungiItemButtonRef()))
        {
            logger.debug("XteTestToolProntoRecapitoDPController actionPerformed invoke addMailPieceItem");

			updateDataModel();

            addMailPieceItem();
        }
        
        if(source.equals(prontoRecapitoDPView.getEliminaItemButtonRef()))
        {
            logger.debug("XteTestToolProntoRecapitoDPController actionPerformed invoke removeMailPieceItem");

            updateDataModel();

            removeMailPieceItem();
        }

        if(source.equals(prontoRecapitoDPView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolProntoRecapitoDPController actionPerformed invoke generaXmlData");

			updateDataModel();

            generaXmlData();
        }

        if(source.equals(prontoRecapitoDPView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolProntoRecapitoDPController actionPerformed invoke impostaDatiWebService");

			updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(prontoRecapitoDPView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolProntoRecapitoDPController actionPerformed invoke salvaXmlData");

			updateDataModel();

            salvaXmlData();
        }

        if(source.equals(prontoRecapitoDPView.getBackButtonRef()))
        {
            logger.debug("XteTestToolProntoRecapitoDPController actionPerformed invoke back");

			updateDataModel();

            back();
        }

        if(source.equals(prontoRecapitoDPView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolProntoRecapitoDPController actionPerformed invoke home");

			updateDataModel();

            home();
        }

        if(source.equals(prontoRecapitoDPView.getExitButtonRef()))
        {
            logger.debug("XteTestToolProntoRecapitoDPController actionPerformed invoke exit");

			updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(prontoRecapitoDPView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolProntoRecapitoDPView)
        {
            logger.debug("XteTestToolProntoRecapitoDPController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolProntoRecapitoDPController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pronto Recapito Data Posta controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolProntoRecapitoDPController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pronto Recapito Data Posta controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        checkMailPieceFieldsFull();  
    }    
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object source = e.getSource();
        if(source.equals(prontoRecapitoDPView.getMailPieceTableRef().getSelectionModel()))
        {               
            if (e.getValueIsAdjusting()) {
                    return;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
                        currentSelectedRow = prontoRecapitoDPView.getMailPieceTableRef().getSelectedRow();
                        logger.debug("XteTestToolProntoRecapitoDPController valueChanged currentSelectedRow = " + currentSelectedRow);
                        logger.debug("XteTestToolProntoRecapitoDPController valueChanged: " + ((XteMailPieceTableModel)prontoRecapitoDPView.getMailPieceTableRef().getModel()).getMap().size());
                        if(currentSelectedRow!=-1)
                        {
                            List<Component> enableComponents = new ArrayList<Component>();
                            enableComponents.add(prontoRecapitoDPView.getEliminaItemButtonRef());
                            componentStatusManager.enableComponent(enableComponents);
                            selectedMailPiece = ((XteMailPieceTableModel)prontoRecapitoDPView.getMailPieceTableRef().getModel()).getSelectedItem(currentSelectedRow);
                        }
                        else
                        {
                            List<Component> disableComponents = new ArrayList<Component>();
                            disableComponents.add(prontoRecapitoDPView.getEliminaItemButtonRef());
                            componentStatusManager.disableComponent(disableComponents);                                                        
                            selectedMailPiece = null;
                        }
                    }
                } );
        }
        
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int eventType = e.getType();
        DefaultTableModel model = (DefaultTableModel) e.getSource();
        if(model.equals(prontoRecapitoDPView.getMailPieceTableRef().getModel()))
        {               
            logger.debug("XteTestToolProntoRecapitoDPController tableChanged: " + ((XteMailPieceTableModel)prontoRecapitoDPView.getMailPieceTableRef().getModel()).getMap().size());
        }
    }

    private void checkMailPieceFieldsFull() {

        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

        for (JTextComponent field : mailPieceFieldList) {
            if (field.getText().trim().isEmpty()) {
                List<Component> disableComponents = new ArrayList<Component>();
                disableComponents.add(prontoRecapitoDPView.getAggiungiItemButtonRef());
                componentStatusManager.disableComponent(disableComponents);                                                        
                return;
            }
        }
        
        List<Component> enableComponents = new ArrayList<Component>();
        enableComponents.add(prontoRecapitoDPView.getAggiungiItemButtonRef());
        componentStatusManager.enableComponent(enableComponents);
    }

    private void clearMailPieceFields() {

        for (JTextComponent field : mailPieceFieldList) {
            if (!field.getText().trim().isEmpty()) {
                field.setText("");
            }
        }
    }
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        prontoRecapitoDPView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolProntoRecapitoDPController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.prontoRecapitoDPView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(prontoRecapitoDPView);
            updateXteTestToolDataModel(dataModel);

            clearMailPieceFields();
            clearMessageBar();

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(prontoRecapitoDPView.getAggiungiItemButtonRef());
            disabledComponents.add(prontoRecapitoDPView.getEliminaItemButtonRef());
            disabledComponents.add(prontoRecapitoDPView.getImpostaDatiWSButtonRef());
            disabledComponents.add(prontoRecapitoDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            Calendar date = Calendar.getInstance();
            this.prontoRecapitoDPView.getMailPieceDatePickerRef().setDate(date.getTime());
            this.prontoRecapitoDPView.getMailPieceTimeSpinnerRef().setValue(date.getTime());

            /*
            XteMailPiece item = new XteMailPiece();
            item.setMailPieceCode("1234567890121");
            
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss.SSS");
            //SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            
            Date startDate = this.prontoRecapitoDPView.getMailPieceDatePickerRef().getDate();

            SpinnerDateModel startTimeSpinnerDateModel = (SpinnerDateModel) this.prontoRecapitoDPView.getMailPieceTimeSpinnerRef().getModel();
            Date startTime = startTimeSpinnerDateModel.getDate();

            String startDateStr = dateFormatter.format(startDate);
            String startTimeStr = timeFormatter.format(startTime);
            String startDateTimeStr = startDateStr + "T" + startTimeStr;
            
            item.setMailPieceDateTime(startDate);
            item.setMailPieceLatitude(123.123f);
            item.setMailPieceLongitude(12.2f);

            XteMailPiece item2 = new XteMailPiece();
            item2.setMailPieceCode("1234567890122");
            item2.setMailPieceDateTime(startDate);
            item2.setMailPieceLatitude(123.123f);
            item2.setMailPieceLongitude(12.2f);

            XteMailPiece item3 = new XteMailPiece();
            item3.setMailPieceCode("1234567890123");
            item3.setMailPieceDateTime(startDate);
            item3.setMailPieceLatitude(123.123f);
            item3.setMailPieceLongitude(12.2f);
            
            this.prontoRecapitoDPView.getMailPieceTableModelRef().addMailPieceItem(item);
            this.prontoRecapitoDPView.getMailPieceTableModelRef().addMailPieceItem(item2);
            this.prontoRecapitoDPView.getMailPieceTableModelRef().addMailPieceItem(item3);
            
            prontoRecapitoDPView.getBundleIdTextRef().requestFocus();

			*/
            logger.info("XteTestToolProntoRecapitoDPController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolProntoRecapitoDPController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolProntoRecapitoDPController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pronto Recapito Data Posta controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolProntoRecapitoDPController start exit");
        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(prontoRecapitoDPView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolProntoRecapitoDPController exit in progress");
            prontoRecapitoDPView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolProntoRecapitoDPController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(prontoRecapitoDPView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(prontoRecapitoDPView.getProntoRacapitoPanelRef().getParent(), prontoRecapitoDPView.getProntoRacapitoPanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolProntoRecapitoDPController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(prontoRecapitoDPView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(prontoRecapitoDPView.getProntoRacapitoPanelRef().getParent(), prontoRecapitoDPView.getProntoRacapitoPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private void addMailPieceItem()
    {
        logger.info("XteTestToolProntoRecapitoDPController start addMailPieceItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            
            clearMessageBar();
            
            XteTestToolMessageBar messageBar = new XteTestToolMessageBar();

            XteTestToolFieldValidator validator = new XteTestToolFieldValidator();            
            boolean isLatitude = validator.validateLatitude(prontoRecapitoDPView.getMailPieceLatitudeTextRef().getText());
            if(!isLatitude)
            {                
                StyledDocument document = messageBar.putContentOnMessageBar("Latitude incorrect format!", MessageType.WARNING);
                prontoRecapitoDPView.getMessageBarTextPaneRef().setDocument(document);
                prontoRecapitoDPView.getMailPieceLatitudeTextRef().requestFocus();
                return;
            }
            
            boolean isLongitude = validator.validateLongitude(prontoRecapitoDPView.getMailPieceLongitudeTextRef().getText());
            if(!isLongitude)
            {
                StyledDocument document = messageBar.putContentOnMessageBar("Longitude incorrect format!", MessageType.WARNING);
                prontoRecapitoDPView.getMessageBarTextPaneRef().setDocument(document);
                prontoRecapitoDPView.getMailPieceLongitudeTextRef().requestFocus();
                return;
            }
                                    
            XteMailPiece item = new XteMailPiece();
            
            item.setMailPieceCode(prontoRecapitoDPView.getMailPieceCodeTextRef().getText());
            
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss.SSS");
            //SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");            
            Date startDate = this.prontoRecapitoDPView.getMailPieceDatePickerRef().getDate();
            SpinnerDateModel startTimeSpinnerDateModel = (SpinnerDateModel) this.prontoRecapitoDPView.getMailPieceTimeSpinnerRef().getModel();
            Date startTime = startTimeSpinnerDateModel.getDate();
            String startDateStr = dateFormatter.format(startDate);
            String startTimeStr = timeFormatter.format(startTime);
            String startDateTimeStr = startDateStr + "T" + startTimeStr;            
            item.setMailPieceDateTime(startTime);

			DecimalFormat numberFormat = new DecimalFormat();
            

            Float latitude = null;
            try {
                latitude = numberFormat.parse(prontoRecapitoDPView.getMailPieceLatitudeTextRef().getText()).floatValue();
            }catch (NumberFormatException nfe) {
                logger.error("invalid value for float " + prontoRecapitoDPView.getMailPieceLatitudeTextRef().getText(), nfe);                
            }
            item.setMailPieceLatitude(latitude);

            Float longitude = null;
            try {
                longitude = numberFormat.parse(prontoRecapitoDPView.getMailPieceLongitudeTextRef().getText()).floatValue();
            }catch (NumberFormatException nfe) {
                logger.error("invalid value for float " + prontoRecapitoDPView.getMailPieceLongitudeTextRef().getText(), nfe);                
            }
            item.setMailPieceLongitude(longitude);
            
            try 
            {
                this.prontoRecapitoDPView.getMailPieceTableModelRef().addMailPieceItem(item);
            }
            catch (XteTestToolDuplicateKeyException duplicateEx)
            {
                StyledDocument document = messageBar.putContentOnMessageBar("Attemp to insert duplicate key Item!", MessageType.WARNING);
                prontoRecapitoDPView.getMessageBarTextPaneRef().setDocument(document);
                this.prontoRecapitoDPView.getMailPieceCodeTextRef().requestFocus();
                return;
            }
            
            StyledDocument document = messageBar.putContentOnMessageBar("Item added successfully!", MessageType.CORRECT_PROCESSING);
            prontoRecapitoDPView.getMessageBarTextPaneRef().setDocument(document);
                        
            clearMailPieceFields();
            prontoRecapitoDPView.getMailPieceCodeTextRef().requestFocus();            
            
            logger.info("XteTestToolProntoRecapitoDPController addMailPieceItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolProntoRecapitoDPController addMailPieceItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolProntoRecapitoDPController addMailPieceItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pronto Recapito Data Posta controller addMailPieceItem failed", ex);
        }
    }
    
    private void removeMailPieceItem()
    {
        logger.info("XteTestToolProntoRecapitoDPController start removeMailPieceItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            if(currentSelectedRow!=-1)
            {                
                XteMailPieceTableModel model = ((XteMailPieceTableModel)prontoRecapitoDPView.getMailPieceTableRef().getModel());
                model.removeRow(currentSelectedRow);
                logger.debug("XteTestToolProntoRecapitoDPController removeMailPieceItem: " + ((XteMailPieceTableModel)prontoRecapitoDPView.getMailPieceTableRef().getModel()).getMap().size());
            }

            logger.info("XteTestToolProntoRecapitoDPController removeMailPieceItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolProntoRecapitoDPController removeMailPieceItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolProntoRecapitoDPController removeMailPieceItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pronto Recapito Data Posta controller removeMailPieceItem failed", ex);
        }
    }

	private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }

    private void generaXmlData()
    {
        logger.info("XteTestToolProntoRecapitoDPController start generaXmlData");

        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();

        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            prontoRecapitoDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(prontoRecapitoDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(prontoRecapitoDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            prontoRecapitoDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(prontoRecapitoDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(prontoRecapitoDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(prontoRecapitoTraccia, out);
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            prontoRecapitoDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(prontoRecapitoDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(prontoRecapitoDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
            
        try
        {            
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString());
            prontoRecapitoDPView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("XteTestToolProntoRecapitoDPController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolProntoRecapitoDPController generaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolProntoRecapitoDPController generaXmlData failed", ex);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            prontoRecapitoDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(prontoRecapitoDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(prontoRecapitoDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pronto Recapito Data Posta controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        prontoRecapitoDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(prontoRecapitoDPView.getImpostaDatiWSButtonRef());
        enableComponent.add(prontoRecapitoDPView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolProntoRecapitoDPController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServiceDPView(prontoRecapitoDPView.getParentFrame());
            XteTestToolWebServiceDPController controller = new XteTestToolWebServiceDPController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            DataPosta dataPostaConfig = XteTestToolConfiguration.getInstance().getDataPostaConfig();
            PR config = dataPostaConfig.getPR();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(prontoRecapitoDPView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(prontoRecapitoDPView.getProntoRacapitoPanelRef().getParent(), prontoRecapitoDPView.getProntoRacapitoPanelRef(), webserviceview, new Rectangle(0,70,1000,690));

            logger.info("XteTestToolProntoRecapitoDPController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolProntoRecapitoDPController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolProntoRecapitoDPController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pronto Recapito Data Posta controller impostaDatiWebService failed", ex);
        }
    }
    
    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolProntoRecapitoDPController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolProntoRecapitoDPController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(prontoRecapitoDPView.getParentFrame());

            logger.info("XteTestToolProntoRecapitoDPController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolProntoRecapitoDPController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolProntoRecapitoDPController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pronto Recapito Data Posta controller salvaXmlData failed", ex);
        }
    }
}
