/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.common;

import com.selexelsag.xte.testtool.datamodel.MessageType;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/**
 *
 * @author Da Procida
 */
public class XteTestToolMessageBar {

    private StyleContext context = null;
    private StyledDocument document = null;
    private Style infoStyleMessage = null;
    private Style warningStyleMessage = null;
    private Style errorStyleMessage = null;
    private ImageIcon infoIcon = null;
    private ImageIcon warningIcon = null;
    private ImageIcon errorIcon = null;

    public XteTestToolMessageBar() {
        this.context = new StyleContext();
        this.document = new DefaultStyledDocument(context);
        defineInfoStyleMessage();
        defineWarningStyleMessage();
        defineErrorStyleMessage();
    }

    private void defineInfoStyleMessage() {
        this.infoStyleMessage = context.addStyle("XteTestToolInfoStyleMessage", null);
        this.infoStyleMessage.addAttribute(StyleConstants.FontFamily, "tahoma");
        this.infoStyleMessage.addAttribute(StyleConstants.Foreground, new Color(0,0,204));
        this.infoStyleMessage.addAttribute(StyleConstants.FontSize, new Integer(11));
        this.infoStyleMessage.addAttribute(StyleConstants.Bold, true);
        this.infoStyleMessage.addAttribute(StyleConstants.Italic, false);
        this.infoStyleMessage.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        this.infoIcon = new ImageIcon(getClass().getResource("/images/InfoMsg.png"));
    }

    private void defineWarningStyleMessage() {
        this.warningStyleMessage = context.addStyle("XteTestToolWarningStyleMessage", null);
        this.warningStyleMessage.addAttribute(StyleConstants.FontFamily, "tahoma");
        this.warningStyleMessage.addAttribute(StyleConstants.Foreground, new Color(204,102,0));
        this.warningStyleMessage.addAttribute(StyleConstants.FontSize, new Integer(11));
        this.warningStyleMessage.addAttribute(StyleConstants.Bold, true);
        this.warningStyleMessage.addAttribute(StyleConstants.Italic, false);
        this.warningStyleMessage.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        this.warningIcon = new ImageIcon(getClass().getResource("/images/WarningMsg.png"));
    }

    private void defineErrorStyleMessage() {
        this.errorStyleMessage = context.addStyle("XteTestToolErrorStyleMessage", null);
        this.errorStyleMessage.addAttribute(StyleConstants.FontFamily, "tahoma");
        this.errorStyleMessage.addAttribute(StyleConstants.Foreground, new Color(204,0,0));
        this.errorStyleMessage.addAttribute(StyleConstants.FontSize, new Integer(11));
        this.errorStyleMessage.addAttribute(StyleConstants.Bold, true);
        this.errorStyleMessage.addAttribute(StyleConstants.Italic, false);
        this.errorStyleMessage.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        this.errorIcon = new ImageIcon(getClass().getResource("/images/ErrorMsg.png"));
    }

    private Style getStyleFromMessageType(MessageType messageType) {

        switch (messageType){
            case CORRECT_PROCESSING:
                return infoStyleMessage;
            case WARNING:
                return warningStyleMessage;
            case ERROR:
                return errorStyleMessage;
            case FATAL:
                return errorStyleMessage;
            default:
                return null;
        }
    }

    private ImageIcon getImageIconFromMessageType(MessageType messageType) {

        switch (messageType){
            case CORRECT_PROCESSING:
                return infoIcon;
            case WARNING:
                return warningIcon;
            case ERROR:
                return errorIcon;
            case FATAL:
                return errorIcon;
            default:
                return null;
        }
    }

    public StyledDocument putContentOnMessageBar(String message, MessageType messageType) {
        try {
            Style messageStyle = getStyleFromMessageType(messageType);
            ImageIcon messageIcon = getImageIconFromMessageType(messageType);
            document.remove(0, document.getLength());

            document.insertString(document.getLength(), message.trim(), messageStyle);

            JLabel label = new JLabel(messageIcon);
            StyleConstants.setComponent(messageStyle, label);
            document.insertString(document.getLength(), "Ignored", messageStyle);

            return document;

        } catch (BadLocationException badLocationException) {
            return null;
        }

    }

    public StyledDocument removeContentFromMessageBar() {
        try {
            document.remove(0, document.getLength());
            return document;
        } catch (BadLocationException badLocationException) {
            return null;
        }

    }

}
