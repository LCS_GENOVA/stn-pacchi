package com.elsagdatamat.trackdb.toedwh.manager;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elsagdatamat.trackdb.toedwh.dao.ISentTraceDAO;
import com.elsagdatamat.trackdb.toedwh.entities.SentTrace;
import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSend;

/*
 * @Author AM,  5/2010
 * @Author AM, 11/2010
 * @Author RL, 01/2011
 */
@Service
public class SentTraceManager implements ISentTraceManager {

	private static final Log log = LogFactory.getLog(SentTraceManager.class);

	@Autowired
	ISentTraceDAO sentTraceDAO;

	@Override
	public void insertSentTraces(List<SentTrace> listSentTraces) {
		sentTraceDAO.insertSentTraces(listSentTraces);
	}
	@Override
	public void insertSentTracesBatch(List<SentTrace> listSentTraces) {
		sentTraceDAO.insertSentTracesBatch(listSentTraces);
	}

	@Override
	public void fillSentTraces(Long id, List<TraceTypesToSend> typesToSend) {
		sentTraceDAO.fillSentTraces(id, typesToSend);
	}

}
