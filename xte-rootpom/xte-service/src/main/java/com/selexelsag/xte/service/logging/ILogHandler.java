package com.selexelsag.xte.service.logging;

import com.selexelsag.xte.service.exceptions.ServiceException;


/**
 * Interfaccia di gestione generale dei log applicativi
 *
 */
public interface ILogHandler 
{
	/**
	 * inizializzazione di log4j
	 */
	public void init() throws ServiceException;
	
	/**
	 * Nome del parametro inserito sulla mappa del thread per il request id, utile a riconoscere il percorso di una chiamiata
	 */
	public static String REQUESTID_TOKEN_NAME = "requestid";
}
