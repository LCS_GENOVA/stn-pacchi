package com.elsagdatamat.dbtrace.bridge.tosda.manager;

import java.util.Date;
import java.util.List;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.elsagdatamat.tt.trackdbws.beans.Trace;





public interface ITraceManager {

	List<Trace> loadNextTraces(ToToken token, Date stopDate, Integer maxNumOfTracesToSDA, Integer maxNumOfTracesFromDB, Integer queryMinutesInterval);
}
