package com.elsagdatamat.dbtrace.bridge.wstojms;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.elsagdatamat.dbtrace.bridge.engine.IMessageBuilder;
import com.elsagdatamat.dbtrace.bridge.engine.IMessageSender;
import com.elsagdatamat.dbtrace.bridge.engine.ITracesReader;
import com.elsagdatamat.dbtrace.bridge.engine.bl.IParameterManager;
import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWService;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public class WsToJmsBridge {
	protected final Log log = LogFactory.getLog(getClass());

	protected IMessageSender messageSender;
	protected IParameterManager parameterManager;

	public IParameterManager getParameterManager() {
		return parameterManager;
	}

	public void setParameterManager(IParameterManager parameterManager) {
		this.parameterManager = parameterManager;
	}

	public void setMessageSender(IMessageSender messageSender) {
		this.messageSender = messageSender;
	}

	protected ITracesReader tracesReader;

	public void setTracesReader(ITracesReader tracesReader) {
		this.tracesReader = tracesReader;
	}

	protected IMessageBuilder messageBuilder;

	public void setMessageBuilder(IMessageBuilder messageBuilder) {
		this.messageBuilder = messageBuilder;
	}

	protected String wsUrl;

	public void setWsUrl(String wsUrl) {
		this.wsUrl = wsUrl;
	}

	protected String wsNamespaceURI;

	public void setWsNamespaceURI(String wsNamespaceURI) {
		this.wsNamespaceURI = wsNamespaceURI;
	}

	protected String wsLocalpart;

	public void setWsLocalpart(String wsLocalpart) {
		this.wsLocalpart = wsLocalpart;
	}

	protected Integer mode; // 0 o null: NOW; 1 - Mezzanotte; 2 - Settimanale; 3
							// -Ieri + finestra

	// (mezzanotte fra domenica e lunedi'

	public void setMode(Integer mode) {
		this.mode = mode;
	}

	protected Integer windowSize = 5;

	public void setWindowSize(Integer windowSize) {
		this.windowSize = windowSize;
	}

	protected Integer windowFrom = 0;

	public void setWindowFrom(Integer windowFrom) {
		this.windowFrom = windowFrom;
	}

	String taskId;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		unicoMetodoDaChiamareDaQuartz();
	}

	protected Integer getIntParameter(String param) {
		String res = parameterManager.findByID(param).getValue();
		if (res != null) {
			return Integer.parseInt(res);
		}
		return null;
	}

	public void unicoMetodoDaChiamareDaQuartz() {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		String start = sdf.format(new Date());
		log.info(String.format("Executing Task with ID=[%1$s] DateTime=[%2$s]", taskId, start));
		long startTaskTime = System.currentTimeMillis();

		// log.debug("Start work");
		// Connessione alla coda JMS
		// Connessione al Web Service
		// log.debug("WS URL: [" + wsUrl + "]");
		// log.debug("WS NSURI: [" + wsNamespaceURI + "]");
		// log.debug("WS Localpart: [" + wsLocalpart + "]");
		log.debug(String.format("[%s] WsToJmsBridge Started: WS URL=%s; WS NS_URI=%s; WS Localpart=%s;mode=%d", taskId, wsUrl, wsNamespaceURI,
				wsLocalpart, mode));

		if (parameterManager != null && !parameterManager.isEnabled(tracesReader.getserviceId())) {
			log.info(String.format("[%s] Service %s is disabled", taskId, tracesReader.getserviceId()));
			return;
		}
		if (mode != null && mode == 3) {
			Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			Integer windowTo = (windowFrom + windowSize) % 24;
			boolean canWork = false;

			log.debug(String.format("[%s] UTC NOW=%d; WindowFrom=%d; WindowTo=%d", taskId, c.get(Calendar.HOUR_OF_DAY), windowFrom, windowTo));
			// log.debug("UTC NOW: " + c.get(Calendar.HOUR_OF_DAY));
			// log.debug("WindowFrom=" + windowFrom );
			// log.debug("WindowTo=" + windowTo );

			if (windowTo > windowFrom) {
				canWork = c.get(Calendar.HOUR_OF_DAY) >= windowFrom && c.get(Calendar.HOUR_OF_DAY) < windowTo;

			} else {
				canWork = c.get(Calendar.HOUR_OF_DAY) >= windowFrom || c.get(Calendar.HOUR_OF_DAY) < windowTo;

			}

			if (canWork == false) {
				return;
			}
		}

		URL url = null;
		try {
			url = new URL(wsUrl); // "http://localhost:8080/pacchi-ws/pacchi.ws"
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			log.fatal(String.format("[%s] Malformed URL", taskId), e);
		}
		QName name = new QName(wsNamespaceURI, wsLocalpart);
		// Service service = TrackWriterService.create(url, name);
		log.debug(String.format("[%s] Instantiate ws", taskId));
		TrackDBRWService tws = new TrackDBRWService(url, name);
		// TrackWriterService tws = new TrackWriterService();
		log.debug(String.format("[%s] Getting port", taskId));
		TrackDBRWServicePortType twsp = tws.getTrackDBRWServicePort();

		// Lettura chunk di tracce da Ws
		// twsp.putTraces(putTracesInput);
		ToToken token = null;
		log.debug(String.format("[%s] Getting token", taskId));
		token = (ToToken)tracesReader.getToken(twsp);
		if (token == null) {
			log.debug(String.format("[%s] error during getToken", taskId));
			return;
		}
		String[] splittenToken = token.getWsToken().split(";");
		String timestampToken = splittenToken[0];
		long idStartToken = Long.parseLong(splittenToken[1]);

		/*
		 * TODO ZAGO 2 casi di stop date (che e' sempre un NOW, ossia OGGI): al
		 * minuto (timestamp) o alla mezzanotte (date) proprieta' che imposto da
		 * spring.
		 */
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		//c.add(Calendar.MINUTE, -10);
		//parto da -20 per i run lunghi potrei perdere delle tracce inserite successivamente
		c.add(Calendar.MINUTE, -20);

		if (mode != null) {
			if (mode == 1) // Giornaliero
			{
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
			} else if (mode == 2) {
				c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
			} else if (mode == 3) // IERI
			{
				c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

				c.set(Calendar.HOUR_OF_DAY, 22);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				c.add(Calendar.DAY_OF_YEAR, -1);

			}
		}

		Date stopDate = c.getTime();
		String stopDateAsString = sdf.format(stopDate);
		log.info(String.format("[%s] Stop date is " + stopDate, taskId));
		while (canWork()) {
			Boolean someTracePresent = true;

			TracesList tracesList = null;
			try {
				log.debug(String.format("[%s] Getting traces from WS", taskId));
				long startDate = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();

				log.info(String.format("Retrieving Traces with Token Info Id=[%d], Timestamp=[%s], StopDate=[%s], StopDateAsLong=[%d]",
						idStartToken,
						timestampToken,
						stopDateAsString,
						stopDate.getTime()));

				tracesList = tracesReader.getTraces(twsp, stopDate);
				log.debug(String.format("[%s] Obtained WS response. Seconds elapsed: "
						+ (Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis() - startDate) / 1000, taskId));

				if (tracesList != null && tracesList.getTraces() != null && tracesList.getTraces().size() != 0) {
					Trace firstTrace = tracesList.getTraces().get(0);
					String idTracedEntity = firstTrace.getIdTracedEntity();
					XMLGregorianCalendar whenRegistered = firstTrace.getWhenRegistered();
					String whenRegisteredAsString = null;
					if (whenRegistered != null) {
						Date whenHappenedAsDate = whenRegistered.toGregorianCalendar().getTime();
						whenRegisteredAsString = sdf.format(whenHappenedAsDate);
					}

					log.info(String.format("[%s] Found traces n. [%d], First Trace IdTracedEntity=[%s], WhenRegistered=[%s], Token Info Id=[%d], Timestamp=[%s]",
							taskId,
							tracesList.getTraces().size(),
							idTracedEntity,
							whenRegisteredAsString,
							idStartToken,
							timestampToken));

					List<String> sList = null;

					log.debug(String.format("[%s] Calling messageBuilder.create", taskId));
					startDate = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
					sList = messageBuilder.create(tracesList);
					log.debug(String.format("[%s] Called messageBuilder.create. Seconds elapsed: "
							+ (Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis() - startDate) / 1000, taskId));

					log.debug(String.format("[%s] Calling messageSender.send", taskId));
					startDate = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();
					messageSender.send(sList);

					log.debug(String.format("[%s] Called messageSender.send. Seconds elapsed: "
							+ (Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis() - startDate) / 1000, taskId));
				} else {
					log.debug(String.format("[%s] No traces found", taskId));
					someTracePresent = false;
				}
			} catch (Exception e) {
				log.error(
						String.format("[%s] Catched Exception with message=[%s], Token Info Id=[%d], Timestamp=[%s]",
								taskId,
								e.getMessage(),
								idStartToken,
								timestampToken), e);
				break;
			}

			// Salvataggio token per la prossima query
			log.debug(String.format("[%s] Acknowledge work", taskId));
			tracesReader.acknowledge(twsp);
			if (someTracePresent == false)
				break;
		}
		// operazioni generiche di chiusura lavoro (specifiche per EDWH2)
		messageSender.commitWork();
		// Rilascio del semaforo
		log.debug(String.format("[%s] Release work", taskId));
		tracesReader.release(twsp);
		log.debug(String.format("[%s] End work", taskId));

		long endTaskTime = System.currentTimeMillis();
		long elapsedTaskTime = endTaskTime - startTaskTime;
		String end = sdf.format(new Date());

		log.info(String.format("Executing Task with ID=[%1$s] DateTime=[%2$s], Elapsed Task Execution Time=[%3$s ms]"
				, taskId
		        , end
		        , elapsedTaskTime));
	}

	// Override su EDWH2
	protected boolean canWork(){
		return true;
	}
}
