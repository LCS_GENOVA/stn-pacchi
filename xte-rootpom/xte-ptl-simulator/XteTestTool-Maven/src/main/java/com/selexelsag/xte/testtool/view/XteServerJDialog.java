/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.common.BackgroundedImagePanelCreator;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteServerJDialog extends javax.swing.JDialog {

    private static Logger logger = Logger.getLogger(XteServerJDialog.class);
    private JPanel backgroundedImagePanel = null;
    private JPanel foregroundPanel= null;
    private XteTestToolApplicationFrameView parentFrame = null;

    public XteServerJDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);      
        parentFrame = (XteTestToolApplicationFrameView) parent;
        try {
            logger.info("XteServerJDialog start class creation");
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            UIManager.put("ScrollBar.width", new Integer(22));
            UIManager.put("ScrollBar.thumb", new Color(220, 200, 190));
            UIManager.put("ScrollBar.track", new Color(240, 240, 240));
            UIManager.put("Button.focus", new Color(255,204,51));
            UIManager.put("OptionPane.minimumSize", new Dimension(300,120));
            UIManager.put("OptionPane.messageFont", new Font("Tahoma", Font.BOLD, 14));
            UIManager.put("OptionPane.messageForeground", new Color(80, 80, 80));
            UIManager.put("OptionPane.buttonFont", new Font("Tahoma", Font.BOLD, 14));
            UIManager.put("OptionPane.informationIcon", new ImageIcon(getClass().getResource("/images/InfoIcon.png")));
            UIManager.put("OptionPane.errorIcon", new ImageIcon(getClass().getResource("/images/ErrorIcon.png")));
            UIManager.put("OptionPane.warningIcon", new ImageIcon(getClass().getResource("/images/WarningIcon.png")));
            UIManager.put("OptionPane.questionIcon", new ImageIcon(getClass().getResource("/images/QuestionIcon.png")));
            UIManager.put("OptionPane.cancelIcon", new ImageIcon(getClass().getResource("/images/Cancel.png")));
            UIManager.put("OptionPane.okIcon", new ImageIcon(getClass().getResource("/images/OK.png")));
            UIManager.put("OptionPane.noIcon", new ImageIcon(getClass().getResource("/images/Cancel.png")));
            UIManager.put("OptionPane.yesIcon", new ImageIcon(getClass().getResource("/images/OK.png")));
            UIManager.put("OptionPane.cancelButtonText", "Cancel");
            UIManager.put("OptionPane.yesButtonText", "Confirm");
            UIManager.put("OptionPane.okButtonText", "Ok");
            UIManager.put("OptionPane.noButtonText", "Cancel");
            UIManager.put("ComboBox.background", Color.white);
            UIManager.put("ComboBox.selectionBackground", new Color(255,204,102));
            UIManager.put("ComboBox.selectionForeground", Color.black);

            initComponents();
            initialize();
            logger.info("XteServerJDialog class created successfully");
        } catch (Exception ex) {
            logger.error("XteServerJDialog class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Xte Server Dialog creation failed", ex);
        }
    }

    public XteTestToolApplicationFrameView getParentFrame() {
        return parentFrame;
    }
    
    private void initialize() {

        logger.info("XteServerJDialog start initialize");

        this.setAlwaysOnTop(true);
        this.setTitle("Registrazione");
        ImageIcon icon = new ImageIcon(getClass().getResource("/images/World.png"));
        Image image = icon.getImage();
        this.setIconImage(image);

        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension d = kit.getScreenSize();

        this.setBounds(0, 0, 500, 500);
        this.setLocation((d.width - this.getSize().width) / 2, ((d.height - this.getSize().height) / 2));

        foregroundPanel = new XteServerPaneView(this);
        foregroundPanel.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
        foregroundPanel.setOpaque(false);
        foregroundPanel.setPreferredSize(new Dimension(500,500));
        foregroundPanel.setMinimumSize(new Dimension(500,500));
        foregroundPanel.setMaximumSize(new Dimension(500,500));

        BackgroundedImagePanelCreator background = new BackgroundedImagePanelCreator();
        backgroundedImagePanel = BackgroundedImagePanelCreator.wrapInBackgroundImage(foregroundPanel,
                new ImageIcon(getClass().getResource("/images/PosteBackground.png")));

        this.setContentPane(backgroundedImagePanel);

        this.pack();
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        logger.info("XteServerJDialog initialize completed successfully");
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
