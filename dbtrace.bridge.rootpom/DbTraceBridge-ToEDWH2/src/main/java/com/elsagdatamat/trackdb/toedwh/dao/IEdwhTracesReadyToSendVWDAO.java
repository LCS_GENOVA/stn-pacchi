package com.elsagdatamat.trackdb.toedwh.dao;

import java.util.List;

import com.elsagdatamat.trackdb.toedwh.entities.EdwhTracesReadyToSendVW;

public interface IEdwhTracesReadyToSendVWDAO {
	List<EdwhTracesReadyToSendVW> executeTraceQuery(Long id);
}