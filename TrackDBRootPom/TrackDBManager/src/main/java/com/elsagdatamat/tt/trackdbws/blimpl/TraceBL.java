package com.elsagdatamat.tt.trackdbws.blimpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.framework.search.Filter;
import com.elsagdatamat.framework.search.Search;
import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.elsagdatamat.tt.trackdbws.beans.TraceDetail;
import com.elsagdatamat.tt.trackdbws.bl.IDuplicatedTraceBL;
import com.elsagdatamat.tt.trackdbws.bl.IEnvironmentParameterBL;
import com.elsagdatamat.tt.trackdbws.bl.ITraceBL;
import com.elsagdatamat.tt.trackdbws.bl.ITraceBLSupport;
import com.elsagdatamat.tt.trackdbws.blimpl.EnvironmentParameterBL.DuplicateManagementStrategy;
import com.elsagdatamat.tt.trackdbws.dao.ITraceDAO;
import com.elsagdatamat.tt.trackdbws.dao.ITraceDetailDAO;
import com.elsagdatamat.tt.trackdbws.utilities.SortingSpecifications;

/*
 * @Author AM,  5/2010
 * @Author AM, 11/2010
 * @Author RL, 01/2011
 */

public class TraceBL implements ITraceBL {

	private static final String TRACE = "trace";

	private static final String PARAM_VALUE = "paramValue";

	private static final String PARAM_CLASS = "paramClass";

	private static final String SERVICE_NAME = "serviceName";

	private static final String WHERE_HAPPENED = "whereHappened";

	private static final String LABEL_TRACED_ENTITY = "labelTracedEntity";

	private static final String ID_CHANNEL = "idChannel";

	private static final String CHANNEL = "channel";

	private static final String TRACED_ENTITY = "tracedEntity";

	private static final String PACCO_LABEL = "PACCO";

	private static final String DISPACCIO_LABEL = "DISPACCIO";

	private static final String AVV_NSP = "AVV_NSP";

	private static final String TRACE_ID = "traceId";

	private static final String ACC_NSP = "ACC_NSP";

	private static final String WHAT_HAPPENED = "whatHappened";

	private static final String ID_TRACED_ENTITY = "idTracedEntity";

	private static final String WHEN_HAPPENED = "whenHappened";

	private static final String WHEN_REGISTERED = "whenRegistered";

	private static final String ID_CORRELAZIONE = "idCorrelazione";

	private static Filter[] filtersArray = {};

	Log log = LogFactory.getLog(getClass());

	@Autowired
	ITraceDAO traceDAO;

	@Autowired
	ITraceDetailDAO traceDetailDAO;

	@Autowired
	IEnvironmentParameterBL environmentParameterBL;

	@Autowired
	ITraceBLSupport traceBLSupport;

	@Autowired
	IDuplicatedTraceBL duplicatedTraceBL;

	private int maxAvvNspDispatch;

	private int minutesForAvvNsp;

	private int daysForMergeAvvNspAccNsp;

	@PostConstruct
	public void initProperties() {
		maxAvvNspDispatch = Integer.parseInt(environmentParameterBL.getParamValue("avvnsp.maxDispatch"));
		minutesForAvvNsp = Integer.parseInt(environmentParameterBL.getParamValue("avvnsp.deltaMinutes"));
		daysForMergeAvvNspAccNsp = Integer.parseInt(environmentParameterBL.getParamValue("avvnsp.daysMergeAccNsp"));
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public boolean insertTrace(Trace trace) {
		try {
			return traceBLSupport.insertTrace(trace);
		} catch (RuntimeException e) {
			log.info("insertTrace() - RuntimeException - Probabile tentativo di inserire traccia duplicata", e);
			DuplicateManagementStrategy strategy = environmentParameterBL.getDuplicateManagementStrategy();
			if (strategy == EnvironmentParameterBL.DuplicateManagementStrategy.SAFE) {
				// numTraces = manageDuplicate(listOfTraces);
				duplicatedTraceBL.singleInsertDuplicatedTrace(trace);
			}
		}
		return false;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public long insertTrace(List<Trace> listOfTraces) {
		// HZ: Refactoring del metodo
		log.trace("insertTrace() - Start");

		if (listOfTraces == null || listOfTraces.isEmpty()) {
			log.warn("insertTrace() - listOfTraces is null");
			return -1;
		}

		long numTraces = 0;
		try {
			numTraces = traceBLSupport.insertTrace(listOfTraces);
		} catch (RuntimeException e) {
			log.info("insertTrace() - RuntimeException - Probabile tentativo di inserire traccia duplicata", e);
			DuplicateManagementStrategy strategy = environmentParameterBL.getDuplicateManagementStrategy();
			if (strategy == EnvironmentParameterBL.DuplicateManagementStrategy.SAFE) {
				numTraces = manageDuplicate(listOfTraces);
			}
		}
		log.trace("insertTrace() - End");
		// ritorna la dimensione della lista inserita
		return numTraces;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	public long manageDuplicate(List<Trace> listOfTraces) {
		// HZ: Refactoring del metodo
		log.trace("manageDuplicate. Inizio");
		long traces = 0;
		for (Trace trace : listOfTraces) {
			try {
				traceBLSupport.insertTrace(trace, true);
				traces++;
			} catch (RuntimeException dataIntegrityViolationException) {
				duplicatedTraceBL.singleInsertDuplicatedTrace(trace);
			}
		}
		log.trace("manageDuplicate. Fine");
		return traces;
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Trace> getListOfNTraces(Date date, String fieldToFilterDate, Long traceId, Trace traceFilterByExampleEqual,
			Trace traceFilterByExampleNotEqual, Date dateTillto, int size, ArrayList<SortingSpecifications> sortBy) {

		log.info("getListOfNTraces. Inizio");

		ArrayList<Filter> equals = buildEqualFilter(traceFilterByExampleEqual, true);
		ArrayList<Filter> notEquals = buildEqualFilter(traceFilterByExampleNotEqual, true);

		// al posto di fieldToFilterDate c'era "whenRegistered"
		Filter stoppingDateFilter = Filter.lessThan(fieldToFilterDate, dateTillto);

		Filter startingDateGreaterThanFilter = Filter.greaterThan(fieldToFilterDate, date);

		Filter startingDateEqualFilter = Filter.equal(fieldToFilterDate, date);
		Filter startingIdFilter = Filter.greaterThan(TRACE_ID, traceId);

		Filter[] notEqualFiltersArray = notEquals != null ? notEquals.toArray(filtersArray) : null;
		Filter notEqualsFilter = notEqualFiltersArray != null ? Filter.not(Filter.and(notEqualFiltersArray)) : null;

		ArrayList<Filter> firstBranch = new ArrayList<Filter>();

		firstBranch.add(stoppingDateFilter);
		firstBranch.add(startingDateGreaterThanFilter);
		firstBranch.add(startingIdFilter);

		if (equals != null)
			firstBranch.addAll(equals);
		if (notEqualsFilter != null)
			firstBranch.add(notEqualsFilter);

		ArrayList<Filter> secondBranch = new ArrayList<Filter>();

		secondBranch.add(stoppingDateFilter);
		secondBranch.add(startingDateEqualFilter);
		secondBranch.add(startingIdFilter);
		if (equals != null)
			secondBranch.addAll(equals);
		if (notEqualsFilter != null)
			secondBranch.add(notEqualsFilter);

		// Verifico se e' necessario filtrare anche sui dettagli
		Set<TraceDetail> isEqualTraceDetailsSet = traceFilterByExampleEqual.getInternalDetailList();
		Filter isInFilter = null;

		if (isEqualTraceDetailsSet != null && isEqualTraceDetailsSet.size() != 0) {
			isInFilter = isInFilterFromTraceDetails(isEqualTraceDetailsSet, true);

			if (isInFilter == null) {

				log.info("getListOfNTraces. Fine. Insieme di dettagli vuoto nella clausola IN");
				return null;
			}

		}

		if (isInFilter != null) {
			firstBranch.add(isInFilter);
			secondBranch.add(isInFilter);
		}

		Set<TraceDetail> notEqualTraceDetailsSet = traceFilterByExampleNotEqual.getInternalDetailList();
		Filter notInFilter = notEqualTraceDetailsSet != null && notEqualTraceDetailsSet.size() != 0 ? isInFilterFromTraceDetails(
				notEqualTraceDetailsSet, false) : null;

		if (notInFilter != null) {
			firstBranch.add(notInFilter);
			secondBranch.add(notInFilter);
		}

		Filter[] firstBranchFilters = firstBranch.toArray(filtersArray);
		Filter[] secondBranchlFilters = secondBranch.toArray(filtersArray);

		Filter firstBranchFilter = Filter.and(firstBranchFilters);
		Filter secondBranchFilter = Filter.and(secondBranchlFilters);

		Filter[] filters = { firstBranchFilter, secondBranchFilter };

		Search search = new Search();
		search.addFilterOr(filters);

		if (sortBy == null)
			sortBy = new ArrayList<SortingSpecifications>();
		for (SortingSpecifications sortingSpecifications : sortBy) {
			Trace trace = new Trace();
			try {
				trace.getClass().getDeclaredField(sortingSpecifications.getField());
			} catch (NoSuchFieldException noSuchFieldException) {

				log.error("getListOfNTraces. Tentativo di sorting su campo inesistente [ " + sortingSpecifications.getField() + " ]. Fine");
				return null;
			}
			search.addSort(sortingSpecifications.getField(), sortingSpecifications.isDesc());
		}

		if (size != -1)
			search.setMaxResults(size);

		log.info("getListOfNTraces. Fine");
		return traceDAO.search(search);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Trace> getListOfNTraces(Date date, String fieldToFilterDate, Long traceId, List<Trace> filtersByExampleEqual,
			List<Trace> filtersByExampleNotEqual, Date dateTillto, int size, ArrayList<SortingSpecifications> sortBy) {

		log.info("getListOfNTrastartingDateEqualFilterces. Inizio");

		if (filtersByExampleEqual == null)
			filtersByExampleEqual = new ArrayList<Trace>();
		if (filtersByExampleNotEqual == null)
			filtersByExampleNotEqual = new ArrayList<Trace>();

		ArrayList<Filter> orOfEquals = new ArrayList<Filter>();
		ArrayList<Filter> equals;
		for (Trace filterByExampleEqual : filtersByExampleEqual) {
			equals = buildEqualFilter(filterByExampleEqual, true);

			if (equals == null)
				continue;

			Filter[] equalFiltersArray = equals.toArray(filtersArray);
			orOfEquals.add(Filter.and(equalFiltersArray));
		}

		ArrayList<Filter> andOfNotEquals = new ArrayList<Filter>();
		ArrayList<Filter> notEquals;
		for (Trace filterByExampleNotEqual : filtersByExampleNotEqual) {
			notEquals = buildEqualFilter(filterByExampleNotEqual, true);

			if (notEquals == null)
				continue;

			Filter[] notEqualFiltersArray = notEquals.toArray(filtersArray);
			andOfNotEquals.add(Filter.and(Filter.not(Filter.and(notEqualFiltersArray))));
		}

		Filter[] orOfEqualFilterArray = orOfEquals.size() != 0 ? orOfEquals.toArray(filtersArray) : null;
		Filter[] andOfNotEqualFilterArray = andOfNotEquals.size() != 0 ? andOfNotEquals.toArray(filtersArray) : null;

		Filter orOfEqualFilters = orOfEqualFilterArray != null ? Filter.or(orOfEqualFilterArray) : null;
		Filter andOfNotEqualFilters = andOfNotEqualFilterArray != null ? Filter.and(andOfNotEqualFilterArray) : null;

		Filter stoppingDateFilter = Filter.lessThan(fieldToFilterDate, dateTillto); // al
																					// psoto
																					// di
																					// fieldToFilterDate
																					// c'era
																					// "whenRegistered"

		Filter startingDateGreaterThanFilter = Filter.greaterThan(fieldToFilterDate, date);

		Filter startingDateEqualFilter = Filter.equal(fieldToFilterDate, date);
		Filter startingIdFilter = Filter.greaterThan(TRACE_ID, traceId);

		ArrayList<Filter> firstBranch = new ArrayList<Filter>();

		firstBranch.add(stoppingDateFilter);
		firstBranch.add(startingDateGreaterThanFilter);
		firstBranch.add(startingIdFilter);

		if (orOfEqualFilters != null)
			firstBranch.add(orOfEqualFilters);
		if (andOfNotEqualFilters != null)
			firstBranch.add(andOfNotEqualFilters);

		ArrayList<Filter> secondBranch = new ArrayList<Filter>();

		secondBranch.add(stoppingDateFilter);
		secondBranch.add(startingDateEqualFilter);
		secondBranch.add(startingIdFilter);
		if (orOfEqualFilters != null)
			secondBranch.add(orOfEqualFilters);
		if (andOfNotEqualFilters != null)
			secondBranch.add(andOfNotEqualFilters);

		Filter[] firstBranchFilters = firstBranch.toArray(filtersArray);
		Filter[] secondBranchlFilters = secondBranch.toArray(filtersArray);

		Filter firstBranchFilter = Filter.and(firstBranchFilters);
		Filter secondBranchFilter = Filter.and(secondBranchlFilters);

		Filter[] filters = { firstBranchFilter, secondBranchFilter };

		Search search = new Search();
		search.addFilterOr(filters);

		if (sortBy == null)
			sortBy = new ArrayList<SortingSpecifications>();
		for (SortingSpecifications sortingSpecifications : sortBy) {
			Trace trace = new Trace();
			try {
				trace.getClass().getDeclaredField(sortingSpecifications.getField());
			} catch (NoSuchFieldException noSuchFieldException) {

				log.error("getListOfNTraces. Tentativo di sorting su campo inesistente [ " + sortingSpecifications + " ]. Fine");
				return null;
			}
			search.addSort(sortingSpecifications.getField(), sortingSpecifications.isDesc());
		}

		if (size != -1)
			search.setMaxResults(size);

		log.info("getListOfNTraces. Fine");
		return traceDAO.search(search);
	}


	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Trace> nextSackTraceTillTo(Long traceId, Date date, Date dateTillto) {
        return nextSackTraceTillTo(traceId, date, dateTillto, null);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<Trace> nextSackTraceTillTo(Long traceId, Date date, Date dateTillto, String notChannel) {
		log.info("nextSackTraceTillTo. Inizio");

		List<Trace> listOfTrace = findTraces(traceId, date, notChannel);
		if (listOfTrace != null && !listOfTrace.isEmpty()) {
			filterTraces(listOfTrace);
			log.info("nextSackTraceTillTo. Fine");
			return listOfTrace;
		}
		log.info("nextSackTraceTillTo. Fine... No Traces found");
		return null;
	}

	private void filterTraces(List<Trace> listOfTrace) {
		Collections.sort(listOfTrace, new Comparator<Trace>() {
			@Override
			public int compare(Trace t1, Trace t2) {
				int compareDate = t1.getWhenRegistered().compareTo(t2.getWhenRegistered());
				if (compareDate != 0)
					return compareDate * -1;
				int compareId = new Long(t1.getIdTrace()).compareTo(t2.getIdTrace());
				if (compareId != 0)
					return compareId * -1;
				return 0;
			}
		});
		Date lastDate = listOfTrace.get(0).getWhenRegistered();
		Calendar oneMinuteAgo = new GregorianCalendar();
		oneMinuteAgo.add(Calendar.MINUTE, -1);
		if (listOfTrace.size() == maxAvvNspDispatch || lastDate.after(oneMinuteAgo.getTime())) {
			List<Trace> toDelete = new LinkedList<Trace>();
			for (Trace analizedTrace : listOfTrace) {
				if (analizedTrace.getWhenRegistered().equals(lastDate)) {
					toDelete.add(analizedTrace);
				} else {
					break;
				}
			}
			if (log.isDebugEnabled()) {
				log.debug(String.format("Rimosse %s tracce per la data %s", toDelete.size(),
						new SimpleDateFormat("dd/MM/yyyy hh:mm:ss:SSS").format(lastDate)));
			}
			if (maxAvvNspDispatch == toDelete.size()) {
				log.warn(String.format("Possibile perdita di tracce, trovate %s tracce per la data %s", maxAvvNspDispatch,
						new SimpleDateFormat("dd/MM/yyyy hh:mm:ss:SSS").format(lastDate)));
			} else {
				listOfTrace.removeAll(toDelete);
			}
		}
		if (listOfTrace.isEmpty()) {
			log.warn(String.format("Rimosse tutte le tracce (configurate max %s tracce da recuperare) per la data %s", maxAvvNspDispatch,
					new SimpleDateFormat("dd/MM/yyyy hh:mm:ss:SSS").format(lastDate)));
		}
	}

	protected List<Trace> findTraces(Long traceId, Date date) {
        return findTraces(traceId, date, null);
    }

    protected List<Trace> findTraces(Long traceId, Date date, String notChannel) {
		Calendar startDate = Calendar.getInstance();
		startDate.add(Calendar.MINUTE, -minutesForAvvNsp);

		Search search = new Search();
		search.addFilterGreaterOrEqual(WHEN_REGISTERED, date);
		search.addFilterGreaterOrEqual(WHEN_HAPPENED, startDate.getTime());
		search.addFilterGreaterThan(TRACE_ID, traceId);
		search.addFilterEqual(TRACED_ENTITY, DISPACCIO_LABEL);
		search.addFilterEqual(WHAT_HAPPENED, AVV_NSP);
		search.addFilterNotEqual(ID_CORRELAZIONE, null);
        if (notChannel != null && !notChannel.trim().isEmpty()) {
            search.addFilterNotEqual(CHANNEL, notChannel);
        }
		search.addSort(WHEN_REGISTERED, false);
		search.addSort(TRACE_ID, false);
		search.setMaxResults(maxAvvNspDispatch);

		List<Trace> listOfTrace = traceDAO.search(search);
		return listOfTrace;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private ArrayList<Filter> buildEqualFilter(Trace trace, boolean isEqual) {
		log.info("buildEqualFilter. Inizio");

		if (emptyExample(trace)) {
			log.info("buildEqualFilter. Filtro vuoto. Fine");
			return null;
		}

		String channel = trace.getChannel();
		String idChannel = trace.getIdChannel();
		String labelTracedEntity = trace.getLabelTracedEntity();
		String idTracedEntity = trace.getIdTracedEntity();
		String tracedEntity = trace.getTracedEntity();
		String whatHappened = trace.getWhatHappened();
		Date whenHappened = trace.getWhenHappened();
		Date whenRegistered = trace.getWhenRegistered();
		String whereHappened = trace.getWhereHappened();
		String serviceName = trace.getServiceName();

		ArrayList<Filter> filters = new ArrayList<Filter>();

		if (channel != null && channel.length() != 0)
			filters.add(isEqual ? Filter.equal(CHANNEL, channel) : Filter.notEqual(CHANNEL, channel));
		if (idChannel != null && idChannel.length() != 0)
			filters.add(isEqual ? Filter.equal(ID_CHANNEL, idChannel) : Filter.notEqual(ID_CHANNEL, idChannel));
		if (labelTracedEntity != null && labelTracedEntity.length() != 0)
			filters.add(isEqual ? Filter.equal(LABEL_TRACED_ENTITY, labelTracedEntity) : Filter.notEqual(LABEL_TRACED_ENTITY,
					labelTracedEntity));
		if (idTracedEntity != null && idTracedEntity.length() != 0)
			filters.add(isEqual ? Filter.equal(ID_TRACED_ENTITY, idTracedEntity) : Filter.notEqual("idTracedEntity", idTracedEntity));
		if (tracedEntity != null && tracedEntity.length() != 0)
			filters.add(isEqual ? Filter.equal(TRACED_ENTITY, tracedEntity) : Filter.notEqual(TRACED_ENTITY, tracedEntity));
		if (whatHappened != null && whatHappened.length() != 0)
			filters.add(isEqual ? Filter.equal(WHAT_HAPPENED, whatHappened) : Filter.notEqual("whatHappened", whatHappened));
		if (whenHappened != null)
			filters.add(isEqual ? Filter.equal(WHEN_HAPPENED, whenHappened) : Filter.notEqual("whenHappened", whenHappened));
		if (whenRegistered != null)
			filters.add(isEqual ? Filter.equal(WHEN_REGISTERED, whenRegistered) : Filter.notEqual("whenRegistered", whenRegistered));
		if (whereHappened != null && whereHappened.length() != 0)
			filters.add(isEqual ? Filter.equal(WHERE_HAPPENED, whereHappened) : Filter.notEqual(WHERE_HAPPENED, whereHappened));
		if (serviceName != null && serviceName.length() != 0)
			filters.add(isEqual ? Filter.equal(SERVICE_NAME, serviceName) : Filter.notEqual(SERVICE_NAME, serviceName));

		log.info("buildEqualFilter. Fine");
		return filters;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private boolean emptyExample(Trace trace) {
		log.info("emptyExample. Inizio");

		boolean isEmpty = true;

		String channel = trace.getChannel();
		String idChannel = trace.getIdChannel();
		String labelTracedEntity = trace.getLabelTracedEntity();
		String idTracedEntity = trace.getIdTracedEntity();
		String tracedEntity = trace.getTracedEntity();
		String whatHappened = trace.getWhatHappened();
		Date whenHappened = trace.getWhenHappened();
		Date whenRegistered = trace.getWhenRegistered();
		String whereHappened = trace.getWhereHappened();
		String serviceName = trace.getServiceName();

		if (channel != null && channel.length() != 0)
			isEmpty = false;
		if (idChannel != null && idChannel.length() != 0)
			isEmpty = false;
		if (labelTracedEntity != null && labelTracedEntity.length() != 0)
			isEmpty = false;
		if (idTracedEntity != null && idTracedEntity.length() != 0)
			isEmpty = false;
		if (tracedEntity != null && tracedEntity.length() != 0)
			isEmpty = false;
		if (whatHappened != null && whatHappened.length() != 0)
			isEmpty = false;
		if (whenHappened != null)
			isEmpty = false;
		if (whenRegistered != null)
			isEmpty = false;
		if (whereHappened != null && whereHappened.length() != 0)
			isEmpty = false;
		if (serviceName != null && serviceName.length() != 0)
			isEmpty = false;

		log.info("emptyExample. Fine");
		return isEmpty;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	private Filter isInFilterFromTraceDetails(Set<TraceDetail> detailList, boolean isIn) {
		log.info("isInFilterFromTraceDetails. Inizio");

		Collection<TraceDetail> traceDetails = null;
		for (TraceDetail traceDetail : detailList) {
			ArrayList<Filter> detailsFilter = new ArrayList<Filter>();

			detailsFilter.add(Filter.equal(PARAM_CLASS, traceDetail.getParamClass()));
			detailsFilter.add(Filter.equal(PARAM_VALUE, traceDetail.getParamValue()));
			if (traceDetails != null)
				detailsFilter.add(Filter.in(TRACE, traceDetails));

			Search search = new Search();

			Filter[] detailsFilterArray = detailsFilter.toArray(filtersArray);
			search.addFilterAnd(detailsFilterArray);

			traceDetails = traceDetailDAO.search(search);
		}

		Collection<Long> traceIds = new ArrayList<Long>();
		if (traceDetails != null) {
			for (TraceDetail traceDetail : traceDetails) {
				traceIds.add(traceDetail.getTrace().getTraceId());
			}
		}
		log.info("isInFilterFromTraceDetails. Fine");
		return traceIds.size() != 0 ? isIn ? Filter.in(TRACE_ID, traceIds) : Filter.notIn("traceId", traceIds) : null;
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Trace> fastSackImageFromNSPBySackCode(String parameterValue) {
		List<Trace> fastSackImageFromNSPByCode = fastSackImageFromNSPByCode(parameterValue, DISPACCIO_LABEL);
		for (Trace t : fastSackImageFromNSPByCode)
			mergeAvvNspTraceWithAccNspTrace(t);
		return fastSackImageFromNSPByCode;

	}

	private List<Trace> fastSackImageFromNSPByCode(String parameterValue, String tracedEntity) {
		log.info("fastSackImageFromNSPBySackCode. Inizio");

		Calendar startDate = Calendar.getInstance();
		startDate.add(Calendar.DAY_OF_MONTH, -16);
		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

		List<Trace> returnList = new ArrayList<Trace>();

		String findLastTrace = "SELECT t from Trace t where  idTracedEntity='" + parameterValue + "' " + "AND t.tracedEntity='"
				+ tracedEntity + "' AND t.whenHappened >= to_date('" + sdf.format(startDate.getTime())
				+ "','dd-mm-yyyy HH24-MI-SS') ORDER BY t.whenHappened desc";
		List<Trace> lastTraces = traceDAO.returnTraceListExecuteHqlQuery(findLastTrace);

		if (lastTraces == null || lastTraces.size() == 0)
			return returnList;

		Trace lastFound = lastTraces.get(0);

		if (!lastFound.getWhatHappened().equals("AVV_NSP"))
			return returnList;

		String hqlQuery = "SELECT t from Trace t where t.idCorrelazione='" + lastFound.getIdCorrelazione() + "' ORDER BY t.tracedEntity";

		returnList = traceDAO.returnTraceListExecuteHqlQuery(hqlQuery);

		// Query in uso fino al 16/01/2013; prendeva avv_nsp anche se non era l'
		// ultima traccia
		/**
		 * String hqlQuery = "SELECT t " +
		 * "FROM Trace t WHERE t.whatHappened = 'AVV_NSP' and t.idCorrelazione in( select t1.idCorrelazione from Trace t1 where idTracedEntity='"
		 * + parameterValue + "' " + "AND t1.tracedEntity='" + tracedEntity +
		 * "' AND t1.whenHappened >= to_date('" +
		 * sdf.format(startDate.getTime()) +
		 * "', 'DD-MM-YYYY HH24:mi:ss') and t1.whatHappened = 'AVV_NSP') " +
		 * " ORDER BY t.tracedEntity"; List<Trace> foundTraces =
		 * traceDAO.returnTraceListExecuteHqlQuery(hqlQuery);
		 * 
		 * Date maxDate = new Date(0); String idCorrelazione = null; for (Trace
		 * t : foundTraces) if (t.getWhenHappened().after(maxDate)) { maxDate =
		 * t.getWhenHappened(); idCorrelazione = t.getIdCorrelazione(); }
		 * 
		 * if (idCorrelazione != null) for (Trace t : foundTraces) if
		 * (idCorrelazione.equals(t.getIdCorrelazione())) returnList.add(t);
		 **/

		log.info("fastSackImageFromNSPBySackCode. Fine");
		return returnList;
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Trace> fastSackImageFromNSPByPackCode(String parameterValue) {
		List<Trace> fastSackImageFromNSPByCode = fastSackImageFromNSPByCode(parameterValue, PACCO_LABEL);
		for (Trace t : fastSackImageFromNSPByCode)
			mergeAvvNspTraceWithAccNspTrace(t);
		return fastSackImageFromNSPByCode;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void mergeAvvNspTraceWithAccNspTrace(Trace t) {
		log.debug("Entering mergeAvvTraceWithAccTrace ");
		if (t == null) {
			log.info("Leaving mergeAvvTraceWithAccTrace; null trace");
			return;
		}
		if (!t.getWhatHappened().equals(AVV_NSP)) {
			log.info("Leaving mergeAvvTraceWithAccTrace; wrong trace, expected: " + AVV_NSP + " instead of " + t.getWhatHappened());
			return;
		}

		Search s = new Search();
		s.addFilterNotEqual(TRACE_ID, t.getIdTrace());
		Calendar from = Calendar.getInstance();
		from.setTime(t.getWhenHappened());
		from.add(Calendar.DATE, -daysForMergeAvvNspAccNsp);
		s.addFilterGreaterOrEqual(WHEN_HAPPENED, from.getTime());
		s.addFilterEqual(ID_TRACED_ENTITY, t.getIdTracedEntity());
		s.addFilterEqual(WHAT_HAPPENED, ACC_NSP);
		s.addSort(WHEN_HAPPENED, true);
		log.info("Created Filter: traceEntityId: " + t.getIdTracedEntity() + " whenHappened: " + from.getTime());

		List<Trace> searchResult = traceDAO.search(s);
		if (searchResult.size() > 0) {
			log.debug("Trovati " + searchResult.size() + " risultati");
			traceDetailDAO.mergeTraceDetails(searchResult.get(0), t, false);
			log.info("Merge completato per " + searchResult.size() + " risultati");
		}
		log.debug("Leaving mergeAvvTraceWithAccTrace");
	}

}
