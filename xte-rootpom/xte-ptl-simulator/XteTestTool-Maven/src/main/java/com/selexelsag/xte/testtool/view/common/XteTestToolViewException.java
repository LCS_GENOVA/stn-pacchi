/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.common;

import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;


/**
 *
 * @author Da Procida
 */
public class XteTestToolViewException extends BaseException {

    public XteTestToolViewException(Group group, Severity severity) {
        super(group, severity);
    }

    public XteTestToolViewException(Group group, Severity severity, String message) {
        super(group, severity, message);
    }

    public XteTestToolViewException(Group group, Severity severity, String message, Throwable cause) {
        super(group, severity, message, cause);
    }

    public XteTestToolViewException(Group group, Severity severity, Throwable cause) {
        super(group, severity, cause);
    }
}
