package com.selexelsag.xte.persistence.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.selexelsag.xte.persistence.entities.AdditionalServices;
import com.selexelsag.xte.persistence.exceptions.PersistenceException;

/**
 * Home object for domain model class Events.
 * @see com.selexelsag.xte.persistence.entities.Events
 * @author Hibernate Tools
 */
public class AdditionalServicesDAO extends BaseDAO<AdditionalServices>
{
	private static final Log log = LogFactory.getLog(AdditionalServicesDAO.class);

	public AdditionalServicesDAO() 
	{
		super(AdditionalServices.class);
	}
	
	public Character findByPerif(char perifCode) throws PersistenceException 
	{
		log.debug("getting instance with perif code: " + perifCode);
		try 
		{
			Criteria criteria = sessionFactory.getCurrentSession()
					.createCriteria(AdditionalServices.class.getName());
			criteria.add(Restrictions.eq("perif", perifCode));
			List<AdditionalServices> instanceList;
				instanceList = (List<AdditionalServices>)findByCriteria(criteria);
			if (instanceList.size()==0) 
			{
				log.error("No value find for perifCode " + perifCode + ", update the data table!");
				return null;
			}
			else if (instanceList.size()>1) 
			{
				log.error("multiples value for perifCode " + perifCode + ", corrupted data table!");
			}
			
			return instanceList.get(0).getCdg();
		}
		catch (RuntimeException re) 
		{
			log.error("get failed", re);
			throw re;
		}
	}
}
