/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.pr;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.controller.common.XteTestToolFieldValidator;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XtePreAccettazione;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;
import com.selexelsag.xte.testtool.view.objectmodel.XtePreAccettazioneTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteTestToolDuplicateKeyException;
import com.selexelsag.xte.testtool.view.pr.XteTestToolPreAccettazionePRView;
import com.selexelsag.xte.testtool.view.pr.XteTestToolWebServicePRView;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata.PA;
import com.selexelsag.xtetesttool.xml.postaregistrata.pa.DatiInvio;
import com.selexelsag.xtetesttool.xml.postaregistrata.pa.ObjectFactory;
import com.selexelsag.xtetesttool.xml.postaregistrata.pa.Traccia;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.xml.ws.util.xml.CDATA;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

/**
 *
 * @author Tassara
 */
public class XteTestToolPreAccettazionePRController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener, ListSelectionListener, TableModelListener {

    private XteTestToolPreAccettazionePRView preAccettazionePRView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolPreAccettazionePRController.class);
    private XtePreAccettazione selectedPreAccettazione = null;
    private int currentSelectedRow = -1;
    private List<JTextComponent> preAccettazioneFieldList = new ArrayList<JTextComponent>();
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    
    private JAXBContext context;
    
    private static Traccia preAccettazioneTraccia;

    static {
        logger.debug("Creating Config Object Factory and Traccia");
        PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        PA config = postaRegistrataConfig.getPA();

        preAccettazioneTraccia = objectFactory.createTraccia();
        //preAccettazioneTraccia.setEvento(config.getEventName());
        preAccettazioneTraccia.setHeaderMazzetto(objectFactory.createHeaderMazzetto());
        preAccettazioneTraccia.setListaInvii(objectFactory.createListaInvii());
    }
    
    
    public XteTestToolPreAccettazionePRController(JPanel preAccettazionePRView) {
        try {
            logger.info("XteTestToolPreAccettazionePRController start class creation");
            this.preAccettazionePRView = (XteTestToolPreAccettazionePRView) preAccettazionePRView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.preAccettazionePRView.initializeView();
            
            updateView();
            
            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.postaregistrata.pa");
            
            logger.info("XteTestToolPreAccettazionePRController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPreAccettazionePRController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolPreAccettazionePRController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Posta Registrata controller creation failed", ex);
        }

    }
    
    private void updateDataModel() {
        
        preAccettazioneTraccia.getHeaderMazzetto().setTipoOggetti(preAccettazionePRView.getPATipoTextRef().getText());
        preAccettazioneTraccia.getHeaderMazzetto().setNumeroSpedizione(preAccettazionePRView.getPANumSpedTextRef().getText());
        preAccettazioneTraccia.getHeaderMazzetto().setCodiceCliente(preAccettazionePRView.getPACodeClienteTextRef().getText());
        preAccettazioneTraccia.getHeaderMazzetto().setNumeroOggetti(preAccettazionePRView.getPANumOggTextRef().getText());
        preAccettazioneTraccia.getHeaderMazzetto().setFrazionario(preAccettazionePRView.getPAFrazTextRef().getText());
        if(preAccettazionePRView.getPATipoMexJComboBoxRef().getSelectedItem() != null)
            preAccettazioneTraccia.getHeaderMazzetto().setTipoMessaggio(preAccettazionePRView.getPATipoMexJComboBoxRef().getSelectedItem().toString());
        
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss.SSS");
            //SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");            
            Date startDate = preAccettazionePRView.getPADatePickerRef().getDate();
            SpinnerDateModel startTimeSpinnerDateModel = (SpinnerDateModel) this.preAccettazionePRView.getPATimeSpinnerRef().getModel();
            Date startTime = startTimeSpinnerDateModel.getDate();
            String startDateStr = dateFormatter.format(startDate);
            String startTimeStr = timeFormatter.format(startTime);
            String startDateTimeStr = startDateStr + "T" + startTimeStr;   
            SimpleDateFormat lasttimeFormatter = new SimpleDateFormat("yyyy-MM-dd" + "'T'" + "HH:mm:ss");            
            GregorianCalendar c = new GregorianCalendar();

            try {
                c.setTime((Date) lasttimeFormatter.parse(startDateTimeStr));
                preAccettazioneTraccia.getHeaderMazzetto().setDataSpedizione(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            } catch (DatatypeConfigurationException ex) {
                logger.error("Error converting date " + c, ex);
                preAccettazioneTraccia.getHeaderMazzetto().setDataSpedizione(null);
            } catch (ParseException ex) {
              logger.error("Error converting date " + c, ex);
              preAccettazioneTraccia.getHeaderMazzetto().setDataSpedizione(null);
        }
            
        ObjectFactory objectFactory = new ObjectFactory();
        XtePreAccettazione[] entries = preAccettazionePRView.getPreAccettazioneTableModelRef().getMap().values().toArray(new XtePreAccettazione[0]);
        logger.debug("entries count: " + entries.length);        
        preAccettazioneTraccia.getListaInvii().getDatiInvio().clear();
        for (int i=0;i<entries.length;i++) {
        
            XtePreAccettazione item = entries[i];
            logger.debug("item " + i + ": " + item);
            if (item == null) 
                continue;
            DatiInvio obj = objectFactory.createDatiInvio();
            obj.setCodiceInvio(item.getPreAccettazioneCodInvio());
            obj.setCausale(item.getPreAccettazioneCausale());
            obj.setDestinazione(item.getPreAccettazioneDestinazione());
            obj.setDestinatario(item.getPreAccettazioneDestinatario());
            obj.setValoreassicurato(item.getPreAccettazioneValAss());
            if(item.getPreAccettazionePeso1() != 0)
                obj.setPeso1(item.getPreAccettazionePeso1());
            if(item.getPreAccettazionePeso2() != 0)
                obj.setPeso2(item.getPreAccettazionePeso2());
            if(item.getPreAccettazioneNumSigilli() != 0)
                obj.setNumerosigilli(item.getPreAccettazioneNumSigilli());
            if(item.getPreAccettazioneNumVaglia() != 0)
                obj.setNumerovaglia(item.getPreAccettazioneNumVaglia());
            obj.setEspresso(item.getPreAccettazioneEspresso());
            obj.setCAP(item.getPreAccettazioneCAP());
            obj.setIndirizzo(item.getPreAccettazioneIndirizzo());
            if(item.getPreAccettazioneImpContr() != 0)
                obj.setImportocontrassegno(item.getPreAccettazioneImpContr());
            obj.setCodiceAR(item.getPreAccettazioneCodAR());
            String SA = item.getPreAccettazioneSA();
            if(SA != null)
            {
                CDATA SA_CData =new CDATA("<SERVIZI><"+SA+"/></SERVIZI>");
                obj.setSA(SA_CData.getText());
            }
            preAccettazioneTraccia.getListaInvii().getDatiInvio().add(obj);
        }
    }
    
    
    private void updateView() {  

        preAccettazionePRView.getPATipoTextRef().setText(preAccettazioneTraccia.getHeaderMazzetto().getTipoOggetti());
        preAccettazionePRView.getPANumSpedTextRef().setText( preAccettazioneTraccia.getHeaderMazzetto().getNumeroSpedizione());
        preAccettazionePRView.getPACodeClienteTextRef().setText(preAccettazioneTraccia.getHeaderMazzetto().getCodiceCliente());
        preAccettazionePRView.getPANumOggTextRef().setText(preAccettazioneTraccia.getHeaderMazzetto().getNumeroOggetti());
        preAccettazionePRView.getPAFrazTextRef().setText(preAccettazioneTraccia.getHeaderMazzetto().getFrazionario());
        preAccettazionePRView.getPATipoMexJComboBoxRef().setSelectedItem(preAccettazioneTraccia.getHeaderMazzetto().getTipoMessaggio());
        //preAccettazionePRView.getPADatePickerRef().setDate(preAccettazioneTraccia.getHeaderMazzetto().getDataSpedizione().toGregorianCalendar().getTime());
        
        preAccettazionePRView.getPreAccettazioneTableModelRef().clearModel();
            
        for (int i=0;i<preAccettazioneTraccia.getListaInvii().getDatiInvio().size();i++) {
            DatiInvio item = preAccettazioneTraccia.getListaInvii().getDatiInvio().get(i);
            XtePreAccettazione obj = new XtePreAccettazione();
            obj.setPreAccettazioneCodInvio(item.getCodiceInvio());
            obj.setPreAccettazioneCausale(item.getCausale());
            obj.setPreAccettazioneDestinazione(item.getDestinazione());
            obj.setPreAccettazioneDestinatario(item.getDestinatario());
            if(item.getValoreassicurato() != null)
                obj.setPreAccettazioneValAss(item.getValoreassicurato());
            if(item.getPeso1() != null)
                obj.setPreAccettazionePeso1(item.getPeso1());
            if(item.getPeso2() != null)
                obj.setPreAccettazionePeso2(item.getPeso2());
            if(item.getNumerosigilli() != null)
                obj.setPreAccettazioneNumSigilli(item.getNumerosigilli());
            if(item.getNumerovaglia() != null)
                obj.setPreAccettazioneNumVaglia(item.getNumerovaglia());
            obj.setPreAccettazioneEspresso(item.getEspresso());
            obj.setPreAccettazioneCAP(item.getCAP());
            obj.setPreAccettazioneIndirizzo(item.getIndirizzo());
            if(item.getImportocontrassegno() != null)
                obj.setPreAccettazioneImpContr(item.getImportocontrassegno());
            obj.setPreAccettazioneCodAR(item.getCodiceAR());
            String SA = item.getSA();           
            if(SA != null)
            {
                SA = SA.replace("<SERVIZI><", "").replace("/></SERVIZI>", "");
                obj.setPreAccettazioneSA(SA);
            }
            preAccettazionePRView.getPreAccettazioneTableModelRef().addPreAccettazioneItem(obj);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = preAccettazionePRView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        preAccettazionePRView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.preAccettazionePRView.getPreAccettazionePanelRef().addAncestorListener(this);        
        this.preAccettazionePRView.getPATipoTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPANumSpedTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPACodeClienteTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPANumOggTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPAFrazTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPATipoMexJComboBoxRef().addFocusListener(this);
        
        this.preAccettazionePRView.getPACodiceInvioTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPACausaleTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPADestinazioneTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPADestinatarioTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPAValAssTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPAPeso1TextRef().addFocusListener(this);
        this.preAccettazionePRView.getPAPeso2TextRef().addFocusListener(this);
        this.preAccettazionePRView.getPANumSigilliTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPANumVagliaTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPAEspressoTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPACAPTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPAIndirizzoTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPAImpContrTextRef().addFocusListener(this);
        this.preAccettazionePRView.getPACodeARTextRef().addFocusListener(this);

        
        this.preAccettazionePRView.getPADatePickerRef().getEditor().addFocusListener(this);
        JFormattedTextField startTimejFormattedTextField = ((JSpinner.DateEditor)preAccettazionePRView.getPATimeSpinnerRef().getEditor()).getTextField();
        startTimejFormattedTextField.addFocusListener(this);
        this.preAccettazionePRView.getPreAccettazioneTableRef().getSelectionModel().addListSelectionListener(this);
        this.preAccettazionePRView.getPreAccettazioneTableRef().getModel().addTableModelListener(this);
        this.preAccettazionePRView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.preAccettazionePRView.getAggiungiItemButtonRef().addActionListener(this);
        this.preAccettazionePRView.getEliminaItemButtonRef().addActionListener(this);
        this.preAccettazionePRView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.preAccettazionePRView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.preAccettazionePRView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.preAccettazionePRView.getBackButtonRef().addActionListener(this);
        this.preAccettazionePRView.getHomeButtonRef().addActionListener(this);
        this.preAccettazionePRView.getExitButtonRef().addActionListener(this);
        this.preAccettazionePRView.getTestButtonRef().addActionListener(this);
        
             
        this.preAccettazionePRView.getPATipoTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPANumSpedTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPACodeClienteTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPANumOggTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPAFrazTextRef().getDocument().addDocumentListener(this);
        
        this.preAccettazionePRView.getPACodiceInvioTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPACausaleTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPADestinazioneTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPADestinatarioTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPAValAssTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPAPeso1TextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPAPeso2TextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPANumSigilliTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPANumVagliaTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPAEspressoTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPACAPTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPAIndirizzoTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPAImpContrTextRef().getDocument().addDocumentListener(this);
        this.preAccettazionePRView.getPACodeARTextRef().getDocument().addDocumentListener(this);
        
//        preAccettazioneFieldList.add(preAccettazionePRView.getPATipoTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPANumSpedTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPACodeClienteTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPANumOggTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPAFrazTextRef());
        preAccettazioneFieldList.add(preAccettazionePRView.getPACodiceInvioTextRef());
        preAccettazioneFieldList.add(preAccettazionePRView.getPACausaleTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPADestinazioneTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPADestinatarioTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPAValAssTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPAPeso1TextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPAPeso2TextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPANumSigilliTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPANumVagliaTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPAEspressoTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPACAPTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPAIndirizzoTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPACodeARTextRef());
//        preAccettazioneFieldList.add(preAccettazionePRView.getPAImpContrTextRef());
	this.salvaXmlFileChooser.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        
        if(source.equals(preAccettazionePRView.getPATipoMexJComboBoxRef()))
        {
            checkPreAccettazioneFieldsFull();
        }
        
        if(source.equals(preAccettazionePRView.getAggiungiItemButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazionePRController actionPerformed invoke addPreAccettazioneItem");
			
			updateDataModel();

            addPreAccettazioneItem();
        }
        
        if(source.equals(preAccettazionePRView.getEliminaItemButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazionePRController actionPerformed invoke removePreAccettazioneItem");

			updateDataModel();

            removePreAccettazioneItem();
        }

        if(source.equals(preAccettazionePRView.getTestButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazionePRController actionPerformed invoke autocomplete");

            AutoCompleteFields();
        }
        
        if(source.equals(preAccettazionePRView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazionePRController actionPerformed invoke generaXmlData");

			updateDataModel();

            generaXmlData();
        }

        if(source.equals(preAccettazionePRView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazionePRController actionPerformed invoke impostaDatiWebService");

			updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(preAccettazionePRView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazionePRController actionPerformed invoke salvaXmlData");

			updateDataModel();

            salvaXmlData();
        }

        if(source.equals(preAccettazionePRView.getBackButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazionePRController actionPerformed invoke back");

			updateDataModel();

            back();
        }

        if(source.equals(preAccettazionePRView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazionePRController actionPerformed invoke home");

			updateDataModel();

            home();
        }

        if(source.equals(preAccettazionePRView.getExitButtonRef()))
        {
            logger.debug("XteTestToolPreAccettazionePRController actionPerformed invoke exit");
			
			updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolPreAccettazionePRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(preAccettazionePRView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolPreAccettazionePRView)
        {
            logger.debug("XteTestToolPreAccettazionePRController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazionePRController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Posta Registrata controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazionePRController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Posta Registrata controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        checkPreAccettazioneFieldsFull();  
    }    
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object source = e.getSource();
        if(source.equals(preAccettazionePRView.getPreAccettazioneTableRef().getSelectionModel()))
        {               
            if (e.getValueIsAdjusting()) {
                    return;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
                        currentSelectedRow = preAccettazionePRView.getPreAccettazioneTableRef().getSelectedRow();
                        logger.debug("XteTestToolPreAccettazionePRController valueChanged currentSelectedRow = " + currentSelectedRow);
                        logger.debug("XteTestToolPreAccettazionePRController valueChanged: " + ((XtePreAccettazioneTableModel)preAccettazionePRView.getPreAccettazioneTableRef().getModel()).getMap().size());
                        if(currentSelectedRow!=-1)
                        {
                            List<Component> enableComponents = new ArrayList<Component>();
                            enableComponents.add(preAccettazionePRView.getEliminaItemButtonRef());
                            componentStatusManager.enableComponent(enableComponents);
                            selectedPreAccettazione = ((XtePreAccettazioneTableModel)preAccettazionePRView.getPreAccettazioneTableRef().getModel()).getSelectedItem(currentSelectedRow);
                        }
                        else
                        {
                            List<Component> disableComponents = new ArrayList<Component>();
                            disableComponents.add(preAccettazionePRView.getEliminaItemButtonRef());
                            componentStatusManager.disableComponent(disableComponents);                                                        
                            selectedPreAccettazione = null;
                        }
                    }
                } );
        }
        
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int eventType = e.getType();
        DefaultTableModel model = (DefaultTableModel) e.getSource();
        if(model.equals(preAccettazionePRView.getPreAccettazioneTableRef().getModel()))
        {               
            logger.debug("XteTestToolPreAccettazionePRController tableChanged: " + ((XtePreAccettazioneTableModel)preAccettazionePRView.getPreAccettazioneTableRef().getModel()).getMap().size());
        }
    }

    private void checkPreAccettazioneFieldsFull() {

        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
//        Object TypeMexobj = preAccettazionePRView.getPATipoMexJComboBoxRef().getSelectedItem();
//        String TypeMex = null;
//        if(TypeMexobj != null)
//            TypeMex = TypeMexobj.toString();
        List<Component> disableComponents = new ArrayList<Component>();
        disableComponents.add(preAccettazionePRView.getAggiungiItemButtonRef());
        
//        if(TypeMex == null)
//        {
//            componentStatusManager.disableComponent(disableComponents);                                                        
//            return;
//        }  
        
        for (JTextComponent field : preAccettazioneFieldList) {
            if (field.getText().trim().isEmpty()) {
                
                componentStatusManager.disableComponent(disableComponents);                                                        
                return;
            }
        }
        List<Component> enableComponents = new ArrayList<Component>();
        enableComponents.add(preAccettazionePRView.getAggiungiItemButtonRef());
        componentStatusManager.enableComponent(enableComponents);
    }
    
    private void AutoCompleteFields()
    {
        preAccettazionePRView.setPAFields();
        checkPreAccettazioneFieldsFull();
    }
    
    private void clearPreAccettazioneFields() {

        for (JTextComponent field : preAccettazioneFieldList) {
            if (!field.getText().trim().isEmpty()) {
                field.setText("");
            }
        }
        preAccettazionePRView.getPADestinazioneTextRef().setText("");
        preAccettazionePRView.getPADestinatarioTextRef().setText("");
        preAccettazionePRView.getPAValAssTextRef().setText("");
        preAccettazionePRView.getPAPeso1TextRef().setText("");
        preAccettazionePRView.getPAPeso2TextRef().setText("");
        preAccettazionePRView.getPANumSigilliTextRef().setText("");
        preAccettazionePRView.getPANumVagliaTextRef().setText("");
        preAccettazionePRView.getPAEspressoTextRef().setText("");
        preAccettazionePRView.getPACAPTextRef().setText("");
        preAccettazionePRView.getPAIndirizzoTextRef().setText("");
        preAccettazionePRView.getPACodeARTextRef().setText("");
        preAccettazionePRView.getPAImpContrTextRef().setText("");
        preAccettazionePRView.getPASATextRef().setText("");
        preAccettazionePRView.getPAPARAMTextRef().setText("");
        
        preAccettazionePRView.getPATipoMexJComboBoxRef().setSelectedIndex(-1);
    }
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        preAccettazionePRView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolPreAccettazionePRController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.preAccettazionePRView.customizeView();
            
            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(preAccettazionePRView);
            updateXteTestToolDataModel(dataModel);

            clearPreAccettazioneFields();
            clearMessageBar();
            
            Configuration configuration = XteTestToolConfiguration.getInstance().getConfiguration();
            List<String> TypeList = configuration.getList("PostaRegistrata.PAType.List");
            DefaultComboBoxModel model = new DefaultComboBoxModel(TypeList.toArray());                                    
            preAccettazionePRView.getPATipoMexJComboBoxRef().setModel(model);
            preAccettazionePRView.getPATipoMexJComboBoxRef().setSelectedIndex(-1);
            
            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(preAccettazionePRView.getAggiungiItemButtonRef());
            disabledComponents.add(preAccettazionePRView.getEliminaItemButtonRef());
            disabledComponents.add(preAccettazionePRView.getImpostaDatiWSButtonRef());
            disabledComponents.add(preAccettazionePRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            Calendar date = Calendar.getInstance();
            this.preAccettazionePRView.getPADatePickerRef().setDate(date.getTime());
            this.preAccettazionePRView.getPATimeSpinnerRef().setValue(date.getTime());


            logger.info("XteTestToolPreAccettazionePRController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPreAccettazionePRController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazionePRController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Posta Registrata controller init failed", ex);
        }

    }
    
    private void exit()
    {
        logger.info("XteTestToolPreAccettazionePRController start exit");

        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(preAccettazionePRView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolPreAccettazionePRController exit in progress");
            preAccettazionePRView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolPreAccettazionePRController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(preAccettazionePRView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(preAccettazionePRView.getPreAccettazionePanelRef().getParent(), preAccettazionePRView.getPreAccettazionePanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolPreAccettazionePRController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(preAccettazionePRView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(preAccettazionePRView.getPreAccettazionePanelRef().getParent(), preAccettazionePRView.getPreAccettazionePanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private void addPreAccettazioneItem()
    {
        logger.info("XteTestToolPreAccettazionePRController start addPreAccettazioneItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            
            clearMessageBar();
            
            XteTestToolMessageBar messageBar = new XteTestToolMessageBar();

            XteTestToolFieldValidator validator = new XteTestToolFieldValidator();            

                                    
            XtePreAccettazione item = new XtePreAccettazione();
            //String selectedItem = (String) preAccettazionePRView.getPreAccettazioneTypeComboBoxRef().getModel().getSelectedItem();
            
            //item.setPreAccettazioneType(selectedItem);           
            item.setPreAccettazioneCodInvio(preAccettazionePRView.getPACodiceInvioTextRef().getText());
            item.setPreAccettazioneCausale(preAccettazionePRView.getPACausaleTextRef().getText());
            if(!preAccettazionePRView.getPADestinazioneTextRef().getText().isEmpty())
                item.setPreAccettazioneDestinazione(preAccettazionePRView.getPADestinazioneTextRef().getText());
            if(!preAccettazionePRView.getPADestinatarioTextRef().getText().isEmpty())
                item.setPreAccettazioneDestinatario(preAccettazionePRView.getPADestinatarioTextRef().getText());
            if(!preAccettazionePRView.getPAValAssTextRef().getText().isEmpty())
                item.setPreAccettazioneValAss(Integer.parseInt(preAccettazionePRView.getPAValAssTextRef().getText()));
            if(!preAccettazionePRView.getPAPeso1TextRef().getText().isEmpty())
                item.setPreAccettazionePeso1(Integer.parseInt(preAccettazionePRView.getPAPeso1TextRef().getText()));
            if(!preAccettazionePRView.getPAPeso2TextRef().getText().isEmpty())
                item.setPreAccettazionePeso2(Integer.parseInt(preAccettazionePRView.getPAPeso2TextRef().getText()));
            if(!preAccettazionePRView.getPANumSigilliTextRef().getText().isEmpty())
                item.setPreAccettazioneNumSigilli(Integer.parseInt(preAccettazionePRView.getPANumSigilliTextRef().getText()));
            if(!preAccettazionePRView.getPANumVagliaTextRef().getText().isEmpty())
                item.setPreAccettazioneNumVaglia(Integer.parseInt(preAccettazionePRView.getPANumVagliaTextRef().getText()));
            if(!preAccettazionePRView.getPAEspressoTextRef().getText().isEmpty())
                item.setPreAccettazioneEspresso(preAccettazionePRView.getPAEspressoTextRef().getText());
            if(!preAccettazionePRView.getPACAPTextRef().getText().isEmpty())
                item.setPreAccettazioneCAP(preAccettazionePRView.getPACAPTextRef().getText());
            if(!preAccettazionePRView.getPAIndirizzoTextRef().getText().isEmpty())
                item.setPreAccettazioneIndirizzo(preAccettazionePRView.getPAIndirizzoTextRef().getText());
            if(!preAccettazionePRView.getPAImpContrTextRef().getText().isEmpty())
                item.setPreAccettazioneImpContr(Integer.parseInt(preAccettazionePRView.getPAImpContrTextRef().getText()));
            if(!preAccettazionePRView.getPACodeARTextRef().getText().isEmpty())
                item.setPreAccettazioneCodAR(preAccettazionePRView.getPACodeARTextRef().getText());
            if(!preAccettazionePRView.getPASATextRef().getText().isEmpty())
                item.setPreAccettazioneSA(preAccettazionePRView.getPASATextRef().getText());
            if(!preAccettazionePRView.getPAPARAMTextRef().getText().isEmpty())
                item.setPreAccettazionePARAM(preAccettazionePRView.getPAPARAMTextRef().getText());
            

            try 
            {
                this.preAccettazionePRView.getPreAccettazioneTableModelRef().addPreAccettazioneItem(item);
            }
            catch (XteTestToolDuplicateKeyException duplicateEx)
            {
                StyledDocument document = messageBar.putContentOnMessageBar("Attemp to insert duplicate key Item!", MessageType.WARNING);
                preAccettazionePRView.getMessageBarTextPaneRef().setDocument(document);
                this.preAccettazionePRView.getPATipoTextRef().requestFocus();
                return;
            }
            
            StyledDocument document = messageBar.putContentOnMessageBar("Item added successfully!", MessageType.CORRECT_PROCESSING);
            preAccettazionePRView.getMessageBarTextPaneRef().setDocument(document);
                        
            clearPreAccettazioneFields();
            preAccettazionePRView.getPATipoTextRef().requestFocus();            
            
            logger.info("XteTestToolPreAccettazionePRController addPreAccettazioneItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPreAccettazionePRController addPreAccettazioneItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazionePRController addPreAccettazioneItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Posta Registrata controller addPreAccettazioneItem failed", ex);
        }
    }
    
    private void removePreAccettazioneItem()
    {
        logger.info("XteTestToolPreAccettazionePRController start removePreAccettazioneItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            if(currentSelectedRow!=-1)
            {         
                XtePreAccettazioneTableModel model = ((XtePreAccettazioneTableModel)preAccettazionePRView.getPreAccettazioneTableRef().getModel());
                model.removeRow(currentSelectedRow);
                logger.debug("XteTestToolPreAccettazionePRController removePreAccettazioneItem: " + ((XtePreAccettazioneTableModel)preAccettazionePRView.getPreAccettazioneTableRef().getModel()).getMap().size());
            }

            logger.info("XteTestToolPreAccettazionePRController removePreAccettazioneItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPreAccettazionePRController removePreAccettazioneItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazionePRController removePreAccettazioneItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Posta Registrata controller removePreAccettazioneItem failed", ex);
        }
    }

	private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }

    private void generaXmlData()
    {
        logger.info("XteTestToolPreAccettazionePRController start generaXmlData");

        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();

        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            preAccettazionePRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(preAccettazionePRView.getImpostaDatiWSButtonRef());
            disableComponent.add(preAccettazionePRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            preAccettazionePRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(preAccettazionePRView.getImpostaDatiWSButtonRef());
            disableComponent.add(preAccettazionePRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(preAccettazioneTraccia, out);
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            preAccettazionePRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(preAccettazionePRView.getImpostaDatiWSButtonRef());
            disableComponent.add(preAccettazionePRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
            
        try
        {    
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString().replace("&lt;", "<").replace("&gt;", ">"));
            
            preAccettazionePRView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("XteTestToolPreAccettazionePRController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPreAccettazionePRController generaXmlData failed", baseEx);
			StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(baseEx.toString(), MessageType.ERROR);
            preAccettazionePRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(preAccettazionePRView.getImpostaDatiWSButtonRef());
            disableComponent.add(preAccettazionePRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazionePRController generaXmlData failed", ex);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            preAccettazionePRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(preAccettazionePRView.getImpostaDatiWSButtonRef());
            disableComponent.add(preAccettazionePRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Posta Registrata controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        preAccettazionePRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(preAccettazionePRView.getImpostaDatiWSButtonRef());
        enableComponent.add(preAccettazionePRView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolPreAccettazionePRController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServicePRView(preAccettazionePRView.getParentFrame());
            XteTestToolWebServicePRController controller = new XteTestToolWebServicePRController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
            PA config = postaRegistrataConfig.getPA();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(preAccettazionePRView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(preAccettazionePRView.getPreAccettazionePanelRef().getParent(), preAccettazionePRView.getPreAccettazionePanelRef(), webserviceview, new Rectangle(0,70,1000,690));

            logger.info("XteTestToolPreAccettazionePRController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolPreAccettazionePRController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazionePRController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Posta Registrata controller impostaDatiWebService failed", ex);
        }
    }
    
    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolPreAccettazionePRController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolPreAccettazionePRController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(preAccettazionePRView.getParentFrame());

            logger.info("XteTestToolPreAccettazionePRController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPreAccettazionePRController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPreAccettazionePRController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Posta Registrata controller salvaXmlData failed", ex);
        }
    }
}
