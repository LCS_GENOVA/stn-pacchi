package com.elsagdatamat.dbtrace.bl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.elsagdatamat.dbtrace.VO.PkConServiziAggiuntiviMapVO;
import com.elsagdatamat.dbtrace.dao.IPkConServiziAggiuntiviDAO;
import com.elsagdatamat.dbtrace.entity.PkConServiziAggiuntivi;


public class PkConServiziAggiuntiviBL implements IPkConServiziAggiuntiviBL {

	
	
	private IPkConServiziAggiuntiviDAO pkConServiziAggiuntiviDAO;

	@Override
	public PkConServiziAggiuntiviMapVO getAll(String sistema) {
	
		PkConServiziAggiuntiviMapVO hashConfServiziAggiuntivi=new PkConServiziAggiuntiviMapVO();
		
		List<PkConServiziAggiuntivi> list = pkConServiziAggiuntiviDAO.findAll();
		
		for (PkConServiziAggiuntivi pkConServiziAggiuntivi : list)
		{
			if(pkConServiziAggiuntivi.getSistemaEsterno().equalsIgnoreCase(sistema))
			{
				hashConfServiziAggiuntivi.put(pkConServiziAggiuntivi.getNomeTag() + PkConServiziAggiuntiviMapVO.separatore + pkConServiziAggiuntivi.getValoreTag(), pkConServiziAggiuntivi.getValoreMappato());
			}
		}
		
		return hashConfServiziAggiuntivi;
	}

	
	public IPkConServiziAggiuntiviDAO getPkConServiziAggiuntiviDAO() {
		return pkConServiziAggiuntiviDAO;
	}

	public void setPkConServiziAggiuntiviDAO(IPkConServiziAggiuntiviDAO pkConServiziAggiuntiviDAO) {
		this.pkConServiziAggiuntiviDAO = pkConServiziAggiuntiviDAO;
	}


}
