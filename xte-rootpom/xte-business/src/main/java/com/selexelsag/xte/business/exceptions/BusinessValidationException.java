package com.selexelsag.xte.business.exceptions;

public class BusinessValidationException extends BusinessException {

	public BusinessValidationException(String message, Throwable cause) {
		super(message, cause);
		setExceptionId(BUSINESS_SERVICEREQ_ID);
	}
	
	public BusinessValidationException(String msg) {
		super(msg);
	}
	
}
