package com.elsagdatamat.dbtrace.bridge.fromnsp.test;

import java.io.InputStream;
import java.util.Date;
import java.util.Hashtable;
import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.selexelsag.tt.dbtrace.bridge.properties.MessageProperties;

public class SendMsgOnQueue {

	private static Connection connection;
	private static Session session;
	private static Queue queue;
	
	static String messageType = "p1.xml";
	private static MockTextMessage mockText;
	
	static Log log = LogFactory.getLog("MessageDestination");

	public static void main(String[] args) throws JMSException {
		mockText = new MockTextMessage(readFile(messageType));
		System.out.println("Connessione coda: ["+openQueueConnection("localhost:1099","queue/pacchi-nsp-in")+"]");
		System.out.println("send message: ["+sendMessage(mockText.getText(), "id1")+"]");
		System.out.println("Chiudi connessione coda: ["+closeQueueConnection()+"]");
	}
	
	public static boolean openQueueConnection(String ip_port, String queueName) {
		try {
			/*
			 * Connecting to JBoss naming service running on local host and on
			 * default port 1099 the environment that should be created is like
			 * the one shown below :
			 */
			
			log.info("Inzio connessione alla coda: ["+queueName+"] indirizzo ip: [jnp://"+ip_port+"]");
			Hashtable<Object, Object> env = new Hashtable<Object, Object>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
			env.put(Context.PROVIDER_URL, "jnp://"+ip_port); //172.19.39.49:1099");
			env.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");

			// Create the initial context
			Context ctx = new InitialContext(env);

			// Lookup the JMS connection factory from the JBoss 5.1 object store
			ConnectionFactory connectionFactory = (ConnectionFactory) ctx.lookup("ConnectionFactory");

			// Lookup a queue from the JBoss 5.1 object store
			queue = (javax.jms.Queue) ctx.lookup(queueName);

			// Create a connection to the JBoss 5.1 Message Service
			connection = connectionFactory.createConnection();

			// Create a session within the connection
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			
		} catch (Exception ex) {
			ex.printStackTrace();			
			return false;
		}
		log.info("Connessione coda riuscita");
		return true;
	}

	public static boolean sendMessage(String objToSend, String id){
		String jmsCorrelationID=id+"-"+(new Date()).getTime();
		try{
			log.info("sendMessage - Inizio invio messaggio");
			// Create a message producer to put messages on the queue
			MessageProducer messageProducer = session.createProducer(queue);
	
			// Create and send a message to the queue
			TextMessage objMessage = session.createTextMessage();
			objMessage.setStringProperty(MessageProperties.EXTERNAL_SYSTEM_LABEL_PROPERTY, "Messaging.Tool");
			objMessage.setStringProperty(MessageProperties.MESSAGE_TYPE_PROPERTY, objToSend.getClass().getName());
			
			objMessage.setJMSCorrelationID(jmsCorrelationID);
			objMessage.setText(objToSend);	
			messageProducer.send(objMessage);
		} catch (JMSException e) {
			e.printStackTrace();			
			return false;
		}
		log.info("sendMessage - Messaggio con JMSCorrelationID = ["+jmsCorrelationID+"] inviato con successo");
		return true;
	}
	
	public static boolean closeQueueConnection() {
		// Close the session and connection resources.
		try {
			session.close();
			connection.close();
		} catch (JMSException e) {
			e.printStackTrace();			
			return false;
		}
		log.info("Disconnessione coda riuscita");
		return true;
	}

	private static String readFile(String fileName) {
		InputStream is = ClassLoader.getSystemResourceAsStream(fileName);
		StringBuffer sb = new StringBuffer();
		Scanner s = new Scanner(is);
		while (s.hasNext())
			sb.append(s.nextLine());
		return sb.toString();
	}
}