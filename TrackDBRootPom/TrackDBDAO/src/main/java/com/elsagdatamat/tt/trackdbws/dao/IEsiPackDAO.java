package com.elsagdatamat.tt.trackdbws.dao;

import java.util.List;

import com.elsagdatamat.framework.dao.IGenericDAO;
import com.elsagdatamat.tt.trackdbws.beans.EsiPackPIFilter;


public interface IEsiPackDAO extends IGenericDAO<EsiPackPIFilter,String> { 
	
	public List<EsiPackPIFilter> returnEsiPackListExecuteHqlQuery(String hqlQuery);
	
}