package com.selexelsag.tt.cruscotto.pacchi.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PkConProdottoPK implements Serializable {

	private static final long serialVersionUID = 6234831928024454303L;
	
	@Column(name = "COD_PRODOTTO", nullable = false)
	private String prodotto;

	@Column(name = "COD_CANALE", nullable = false)
	private String canale;

	@Column(name = "COD_TRACCIA", nullable = false)
	private String tipoTraccia;

	public PkConProdottoPK() {
	}

	public PkConProdottoPK(String canale,String prodotto, String tipoTraccia) {
		super();
		this.canale = canale;
		this.prodotto = prodotto;
		this.tipoTraccia = tipoTraccia;
	}

	public String getProdotto() {
		return prodotto;
	}

	public void setProdotto(String prodotto) {
		this.prodotto = prodotto;
	}

	public String getCanale() {
		return canale;
	}

	public void setCanale(String canale) {
		this.canale = canale;
	}

	public String getTipoTraccia() {
		return tipoTraccia;
	}

	public void setTipoTraccia(String tipoTraccia) {
		this.tipoTraccia = tipoTraccia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((canale == null) ? 0 : canale.hashCode());
		result = prime * result
				+ ((prodotto == null) ? 0 : prodotto.hashCode());
		result = prime * result
				+ ((tipoTraccia == null) ? 0 : tipoTraccia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PkConProdottoPK other = (PkConProdottoPK) obj;
		if (canale == null) {
			if (other.canale != null)
				return false;
		} else if (!canale.equals(other.canale))
			return false;
		if (prodotto == null) {
			if (other.prodotto != null)
				return false;
		} else if (!prodotto.equals(other.prodotto))
			return false;
		if (tipoTraccia == null) {
			if (other.tipoTraccia != null)
				return false;
		} else if (!tipoTraccia.equals(other.tipoTraccia))
			return false;
		return true;
	}
	
}
