package com.elsagdatamat.dbtrace.bridge.totgc;

import com.elsagdatamat.dbtrace.bridge.engine.IMessageBuilder;
import com.elsagdatamat.dbtrace.bridge.engine.IMessageSender;
import com.elsagdatamat.dbtrace.bridge.engine.ITracesReader;
import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;

public interface IToTGCMessageBuilder extends IMessageBuilder, IMessageSender,ITracesReader  {

	ToToken getToken();

	void setToken(ToToken token);

}
