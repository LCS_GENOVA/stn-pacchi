<?xml version='1.0'?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:ED="http://www.elsagdatamat.com/Tracking/Messages/xslt/P3toD1Transform">
   <xsl:output indent="yes" method="xml" omit-xml-declaration="no" encoding="UTF-8"/>
	<!-- INCLUDED STYLESHEET -->
	<xsl:include href="MessageConverter-Common.xsl" />

	<xsl:template match="/MSG">
      <xsl:element name="MSG">
		   <xsl:apply-templates select="HDR" mode="HDRTemplate" />
		   <xsl:apply-templates select="P3" mode="P3Template" />	
      </xsl:element>
   </xsl:template>
   
   <!-- TAG HDR -->
	<xsl:template mode="HDRTemplate" match="HDR">
      <xsl:element name="HDR">
         <xsl:call-template name="HDRAttributesTemplate">
			<xsl:with-param name="frazionarioParam" select="@AGNZ" />            
            <xsl:with-param name="swrelParam" select="$gExtSwRel" />            
         </xsl:call-template>
      </xsl:element>      
	</xsl:template>

	<!-- TAG DHDR -->
	<xsl:template mode="DHDRTemplate" match="DHDR" xmlns:p3ExtFun="xalan://com.elsagdatamat.tracktrace.core.infrastructure.trackmessages.converter.P3TrackMessageConverter">
      <xsl:element name="DHDR">
         <xsl:if test="string-length(./DCOD) != 0">
         	<xsl:element name="DID"><xsl:value-of select="DCOD"/></xsl:element>
         </xsl:if>
         <xsl:if test="string-length(./DCOD) = 0">
         	<xsl:element name="DID"><xsl:value-of select="p3ExtFun:generateDispatchCode(../../HDR/@AGNZ)"/></xsl:element>         	         	
         </xsl:if>
         <!-- La fase deve essere settata esternamente in funzione dela sorgente che ha prodotto il messaggio P3 -->
         <xsl:element name="DOP"><xsl:value-of select="DSP"/></xsl:element>
         <xsl:element name="DPH"><xsl:value-of select="$gExtPacchiPhaseAcc"/></xsl:element>    
         <!-- Non esiste l'ufficio di destinazione per i pacchi -->     
         <xsl:element name="OFCOTH"><xsl:value-of select="$gExtPacchiOFCOTH"/></xsl:element>
         <xsl:element name="DDT"><xsl:value-of select="DDT"/></xsl:element>
         <xsl:if test="string-length(COURIER/text())!=0">
         	<xsl:element name="COURIER"><xsl:value-of select="substring(COURIER, 1, 20)"/></xsl:element>
         </xsl:if>              
      </xsl:element>      
	</xsl:template>
	
	<!-- TAG P3 -->   
	<xsl:template mode="P3Template" match="P3">
	   <xsl:element name="D1">			
			<xsl:apply-templates select="DHDR" mode="DHDRTemplate" />
			<xsl:apply-templates select="OBJ" mode="OBJTemplate" />
	   </xsl:element>
	</xsl:template>
   	
	<!-- Attributi TAG HDR -->
	<xsl:template name="HDRAttributesTemplate">		
		<xsl:param name="frazionarioParam"/>		
		<xsl:param name="swrelParam"/>
			<xsl:attribute name="OFCID"><xsl:value-of select="$frazionarioParam" />
			</xsl:attribute>
			<xsl:attribute name="SWREL"><xsl:value-of select="$swrelParam" />
			</xsl:attribute>
	</xsl:template>   

	<xsl:template mode="OBJTemplate" match="OBJ">	
		<xsl:element name="OBJ">
			<xsl:element name="OBJID"><xsl:value-of select="OBJID" /></xsl:element>
			<xsl:element name="PH"><xsl:value-of select="$gExtPacchiPhaseAcc" /></xsl:element>
			<xsl:if test="FRM/text() = '1'">
				<xsl:element name="BTSF">B</xsl:element>
				<xsl:element name="PSTF"><xsl:value-of select="$gExtPacchiP2PstfRestituito" /></xsl:element>				
			</xsl:if>
			<xsl:if test="string-length(FRM) = 0">
				<xsl:element name="PSTF"><xsl:value-of select="$gExtPstf" /></xsl:element>
			</xsl:if>
			<xsl:element name="OP"><xsl:value-of select="$gExtPacchiOperator" /></xsl:element>
			<xsl:if test="string-length(TDA) != 0">
         		<xsl:element name="TDT"><xsl:value-of select="TDA" /></xsl:element>
         	</xsl:if>
			<xsl:if test="string-length(TDA) = 0">
				<!-- Se non c'e' la data prendo quella del Dispaccio -->
         		<xsl:element name="TDT"><xsl:value-of select="../DHDR/DDT" /></xsl:element>
         	</xsl:if>
         	<xsl:element name="OFCOTH"><xsl:value-of select="$gExtPacchiOFCOTH" /></xsl:element>
         	
			<xsl:element name="SUBC"><xsl:value-of select="PRD" /></xsl:element>						
		</xsl:element>	
	</xsl:template>   
</xsl:stylesheet>
