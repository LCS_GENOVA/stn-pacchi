package com.elsagdatamat.dbtrace.bridge.engine.bl;

import com.elsagdatamat.dbtrace.bridge.engine.entities.OfficeMapper;
import com.elsagdatamat.dbtrace.bridge.engine.entities.OfficeMapperPK;

public interface IBLOfficeMapper extends IManager<OfficeMapper, OfficeMapperPK> {

}
