package com.selexelsag.xte.persistence.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.selexelsag.xte.persistence.entities.AsyncMessages;
import com.selexelsag.xte.persistence.exceptions.PersistenceException;

/**
 * Home object for domain model class Events.
 * @see com.selexelsag.xte.persistence.entities.Events
 * @author Hibernate Tools
 */
@Transactional
public class AsyncMessagesDAO extends BaseDAO<AsyncMessages>
{
	private static final Log log = LogFactory.getLog(AsyncMessagesDAO.class);

	private int stalledTimeout = 1000*60*5;
	public int getStalledTimeout() {return stalledTimeout;}
	public void setStalledTimeout(int stalledTimeout) {this.stalledTimeout = stalledTimeout;}
	
	public AsyncMessagesDAO() 
	{
		super(AsyncMessages.class);
	}

	private List<Long> getMessagesIdList(String flowId, String lockerId, int maxResults) throws PersistenceException 
	{
		log.debug("getMessagesIdList for " + flowId);
		
		List<Long> ids;
		
		ArrayList<AsyncMessages> results = new ArrayList<AsyncMessages>();

		//TEST:
		// String sqlQuery = "select this_.ID from MESSAGES this_ where this_.SEND_DATE is null and LOCK_ID is null and rownum <= " + maxResults + " for update of this_.ID";
		// list = (List<BigDecimal>)sessionFactory.getCurrentSession().createSQLQuery(sqlQuery).list();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AsyncMessages.class.getName());
		criteria = criteria.add(Restrictions.and(Restrictions.isNull("sendTimestamp"),Restrictions.isNull("lockTimestamp")));
		criteria = criteria.add(Restrictions.eq("flowId", flowId));
		criteria = criteria.setMaxResults(maxResults);
		criteria = criteria.setProjection(Projections.id());
		//AGGIUNTO ordinamento FIFO
		criteria = criteria.addOrder(Order.asc("receivedTimestamp"));
		// criteria = criteria.setLockMode(LockMode.UPGRADE);

		ids = findByCriteria(criteria);
		log.debug("messages found: " + ids.size());
		return ids;
	}
	
	public int freeStalledMessagesId(String flowId) throws PersistenceException 
	{
		log.debug("freeStalledMessagesId timeout: " + stalledTimeout);
		
		List<AsyncMessages> results;

		int timeoutMillis = stalledTimeout; 
		
		Timestamp t = new Timestamp(System.currentTimeMillis()-timeoutMillis);
		Criteria criteria = sessionFactory.getCurrentSession()
				.createCriteria(AsyncMessages.class.getName());
		criteria = criteria.add(Restrictions.and(
				Restrictions.isNotNull("lockTimestamp"), 
				Restrictions.le("lockTimestamp", t)));
		criteria = criteria.add(Restrictions.eq("flowId", flowId));
		criteria = criteria.setLockMode(LockMode.UPGRADE);

		results = (List<AsyncMessages>)findByCriteria(criteria);
		
		log.debug("stalled messages found: " + results.size());
		
		for(int i=0;i<results.size();i++) 
		{
			results.get(i).setLockId(null);
			results.get(i).setLockTimestamp(null);
			attachClean(results.get(i));
		}
		sessionFactory.getCurrentSession().flush();
		return results.size();
	}
	
	@SuppressWarnings("unchecked")
	public List<AsyncMessages> getLockedIdList(String flowId, String lockerId, int maxResults) throws PersistenceException 
	{
		log.debug("getLockedIdList flow: " + flowId + "; locker:" + lockerId);

		List<Long> ids;
		
		ArrayList<AsyncMessages> results = new ArrayList<AsyncMessages>();

		ids = getMessagesIdList(flowId, lockerId, 5*maxResults);
		
		Timestamp t = new Timestamp(System.currentTimeMillis());
			
		for (int i=0;i<ids.size();i++) 
		{
			try {
				AsyncMessages entity = findById(ids.get(i).longValue(), LockMode.UPGRADE);
				if (entity.getLockId() != null) 
				{
					log.debug("instance id " + entity.getId() + " just locked by " + entity.getLockId() + " in date " + entity.getLockTimestamp());
//					continue;
				}
				entity.setLockId(lockerId);
				entity.setLockTimestamp(t);
				attachClean(entity);
				results.add(entity);
				if (results.size()>=maxResults) 
				{
					break;
				}
			}
			catch(RuntimeException re) 
			{
				log.debug("error processing entity id " + ids.get(i).longValue());
			} 
		}
		
		try 
		{
			sessionFactory.getCurrentSession().flush();
		}
		catch(RuntimeException re) 
		{
			PersistenceException pe = new PersistenceException("DB Error", re);
			throw pe;
		}
		
		log.debug("getLockedIdList successful, result size: "
				+ results.size());
		return results;
	}
}
