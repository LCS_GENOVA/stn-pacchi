package com.elsagdatamat.dbtrace.bridge.toedwh;

public class ToEDWHEsiPockToken extends ToEDWHToken {
	public static final String TOKEN_ID="DBTraceToEDWHEsiPockBridge";
	
	@Override
	public String getServiceId() {
		return TOKEN_ID;
	}
}
