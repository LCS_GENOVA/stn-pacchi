package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.DiscardedTrace;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface IDiscardedTraceDAO  extends IGenericDAO<DiscardedTrace, Long>{

}
