package com.elsagdatamat.tt.trackdbws.dao;

import com.elsagdatamat.framework.dao.IGenericDAO;
import com.elsagdatamat.tt.trackdbws.beans.EnvironmentParameter;

public interface IEnvironmentParameterDAO  extends IGenericDAO<EnvironmentParameter, String>{

}

