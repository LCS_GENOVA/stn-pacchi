package com.elsagdatamat.dbtrace.bridge.engine.entities;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;


/** 
* R.L. BeanGenerator - 22.05.2012 03:10:16
*/ 
@Entity
@Table(name="STATUS_PI_SDA")
@IdClass(StatusPiSdaPK.class)
public class StatusPiSda implements Serializable {

	private static final long serialVersionUID = 2991649696276408497L;

	@Id
	@Column(name = "PI_CHANNEL", nullable =false)
	private String piChannel;

	@Id
	@Column(name = "STATUS_PI", nullable =false)
	private String statusPI;

	@Column(name = "STATUS_SDA", nullable =false)
	private String statusSDA;

	public String getPiChannel() {
		return piChannel;
	}

	public void setPiChannel(String piChannel) {
		this.piChannel = piChannel;
	}

	public String getStatusPI() {
		return statusPI;
	}

	public void setStatusPI(String statusPI) {
		this.statusPI = statusPI;
	}

	public String getStatusSDA() {
		return statusSDA;
	}

	public void setStatusSDA(String statusSDA) {
		this.statusSDA = statusSDA;
	}

}