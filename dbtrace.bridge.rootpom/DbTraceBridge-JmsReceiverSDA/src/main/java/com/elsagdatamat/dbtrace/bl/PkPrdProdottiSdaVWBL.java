package com.elsagdatamat.dbtrace.bl;

import java.util.HashMap;
import java.util.List;

import com.elsagdatamat.dbtrace.dao.IPkPrdProdottiSdaVWDAO;
import com.elsagdatamat.dbtrace.entity.PkPrdProdottiSdaVW;

public class PkPrdProdottiSdaVWBL implements IPkPrdProdottiSdaVWBL {

	private IPkPrdProdottiSdaVWDAO pkPrdProdottiSdaVWDAO;

	@Override
	public HashMap<String, String> getAll() {
		List<PkPrdProdottiSdaVW> listProdottiSda = pkPrdProdottiSdaVWDAO.findAll();
		
		HashMap<String, String> hashProdottiSda=new HashMap<String, String>();
		if(listProdottiSda!=null) {
			for(PkPrdProdottiSdaVW prd :listProdottiSda ) {
				hashProdottiSda.put(prd.getCodProdotto(), prd.getDescProdotto());
			}
		} 
		return hashProdottiSda;
	}

	public IPkPrdProdottiSdaVWDAO getPkPrdProdottiSdaVWDAO() {
		return pkPrdProdottiSdaVWDAO;
	}

	public void setPkPrdProdottiSdaVWDAO(
			IPkPrdProdottiSdaVWDAO pkPrdProdottiSdaVWDAO) {
		this.pkPrdProdottiSdaVWDAO = pkPrdProdottiSdaVWDAO;
	}
}
