package com.selexelsag.dbtrace.bridge.to.ssip.test.manager;

import junit.framework.Assert;

import org.junit.Test;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.elsagdatamat.dbtrace.bridge.engine.entities.Token;
import com.selexelsag.dbtrace.bridge.to.ssip.manager.SSIPAbstractManager;

public class AbstractManagerSealingWrapperTest extends SSIPAbstractManager {

	@Test
	public void sealedProtectedMethod_ConvertTtoZ_BehaviourIsSameAsNewOne() {
		Token token = new Token() {{
			setServiceId("testServiceId");
			setWsToken("testWsToken");
		}};
		ToToken ToToken = convertTtoZ(token);
		Assert.assertEquals(toToToken(token).getServiceId(), ToToken.getServiceId());
		Assert.assertEquals(toToToken(token).getWsToken(), ToToken.getWsToken());
	}

	@Test
	public void sealedProtectedMethod_ConvertZtoT_BehaviourIsSameAsNewOne() {
		ToToken ToToken = new ToToken();
		ToToken.setWsToken("testWSToken");
		Token token = convertZtoT(ToToken);
		Assert.assertEquals(toToken(ToToken).getServiceId(), token.getServiceId());
		Assert.assertEquals(toToken(ToToken).getWsToken(), token.getWsToken());
	}

}
