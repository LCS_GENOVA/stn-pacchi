package com.elsagdatamat.dbtrace.bridge.toedwh;

public class ToEDWHStatoEsitoToken extends ToEDWHToken {
	public static final String TOKEN_ID="DBTraceToEDWHStatoEsitoBridge";
	
	@Override
	public String getServiceId() {
		return TOKEN_ID;
	}
}
