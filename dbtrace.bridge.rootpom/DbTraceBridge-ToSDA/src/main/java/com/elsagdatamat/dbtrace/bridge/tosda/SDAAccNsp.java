package com.elsagdatamat.dbtrace.bridge.tosda;

import java.util.Map;

import javax.xml.bind.annotation.XmlElement;

public class SDAAccNsp {
	@XmlElement(name = "VARTR")
	public String vartr;

	@XmlElement(name = "SPRD")
	public String sprd;
	
	@XmlElement(name = "NAZ")
	public String naz;
	
	@XmlElement(name = "P")
	public String p;
	
	@XmlElement(name = "SA")
	public String sa;
	
	@XmlElement(name = "CNTRS")
	public String cntrs;
	
	@XmlElement(name = "ASSIC")
	public String assic;
	
	@XmlElement(name = "IMPP")
	public String impp;
	
	@XmlElement(name = "TAS")
	public String tas;
	
	@XmlElement(name = "PREPG_ID")
	public String prepg_id;
	
	@XmlElement(name = "TRB")
	public String trb;
	
	@XmlElement(name = "TRSA")
	public String trsa;
	
	@XmlElement(name = "DIRD")
	public String dird;
	
	@XmlElement(name = "DESTZ")
	public String destz;
	
	@XmlElement(name = "DESTR")
	public String destr;
	
	@XmlElement(name = "ADDRS")
	public String addrs;
	
	@XmlElement(name = "ZIP")
	public String zip;
	
	@XmlElement(name = "DIST")
	public String dist;
	
	@XmlElement(name = "PROV")
	public String prov;
	
	@XmlElement(name = "LOCAL_ID")
	public String local_id;
	
	@XmlElement(name = "CONT_ID")
	public String cont_id;
	
	@XmlElement(name = "FSC")
	public String fsc;
	
	@XmlElement(name = "FRM")
	public String frm;
	
	@XmlElement(name = "MRES_ID")
	public String mres_id;
	
	@XmlElement(name = "IRES")
	public String ires;
	
	@XmlElement(name = "CODICE_SAP")
	public String codice_sap;
	
	@XmlElement(name = "PARTITA_IVA")
	public String partita_iva;
	
	@XmlElement(name = "CODICE_FISCALE")
	public String codice_fiscale;

	public SDAAccNsp()
	{
	}

	public SDAAccNsp(Map<String, String> map)
	{
		String key = "VARTR";
		this.vartr = map.get(key);

		key = "SPRD";
		this.sprd = map.get(key);
		
		key = "NAZ";
		this.naz = map.get(key);
		
		key = "P";
		this.p = map.get(key);
		
		key = "SA";
		this.sa = map.get(key);
		
		key = "CNTRS";
		this.cntrs = map.get(key);
		
		key = "ASSIC";
		this.assic = map.get(key);
		
		key = "IMPP";
		this.impp = map.get(key);
		
		key = "TAS";
		this.tas = map.get(key);
		
		key = "PREPG_ID";
		this.prepg_id = map.get(key);
		
		key = "TRB";
		this.trb = map.get(key);
		
		key = "TRSA";
		this.trsa = map.get(key);
		
		key = "DIRD";
		this.dird = map.get(key);
		
		key = "DESTZ";
		this.destz = map.get(key);
		
		key = "DESTR";
		this.destr = map.get(key);
		
		key = "ADDRS";
		this.addrs = map.get(key);
		
		key = "ZIP";
		this.zip = map.get(key);
		
		key = "DIST";
		this.dist = map.get(key);
		
		key = "PROV";
		this.prov = map.get(key);
		
		key = "LOCAL_ID";
		this.local_id = map.get(key);
		
		key = "CONT_ID";
		this.cont_id = map.get(key);
		
		key = "FSC";
		this.fsc = map.get(key);
		
		key = "FRM";
		this.frm = map.get(key);
		
		key = "MRES_ID";
		this.mres_id = map.get(key);
		
		key = "IRES";
		this.ires = map.get(key);
		
		key = "CODICE_SAP";
		this.codice_sap = map.get(key);
		
		key = "PARTITA_IVA";
		this.partita_iva = map.get(key);
		
		key = "CODICE_FISCALE";
		this.codice_fiscale = map.get(key);
	}
}