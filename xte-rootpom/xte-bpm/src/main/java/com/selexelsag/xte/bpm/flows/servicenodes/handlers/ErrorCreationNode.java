package com.selexelsag.xte.bpm.flows.servicenodes.handlers;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.drools.process.instance.WorkItemHandler;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemManager;

import com.selexelsag.xte.business.tracking.ITrasformation;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.model.flows.FlowResponse;
import com.selexelsag.xte.model.track.TrackResponse;
import com.selexelsag.xte.service.context.SpringApplicationContext;
import com.selexelsag.xte.service.resource.IMessageManager;

public class ErrorCreationNode implements WorkItemHandler {
	
	private Logger logger = LogManager.getLogger(ErrorCreationNode.class.getName());

	@Override
	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
		
	}

	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		
		IMessageManager messageManager = (IMessageManager)SpringApplicationContext.
		getBean(IMessageManager.TRACK_MSG_MANAGER_BEAN);

		StringBuilder data = (StringBuilder)workItem.getParameter("data");
		if (data==null) {
			logger.debug("data parameter is null!");
		}
		
		FlowParams params = (FlowParams)workItem.getParameter("flowParams");
		if (params==null) {
			logger.debug("flowParams parameter is null!");
		}
		
		FlowResponse flowResponse = (FlowResponse)workItem.getParameter("flowResponse");
		if (flowResponse==null) {
			logger.debug("flowResponse parameter is null!");
		}
		
		Throwable error = (Throwable)workItem.getParameter("error");
		if (error==null) {
			logger.debug("error parameter is null!");
		}
		
		boolean isInternalError = false; 
				
		String isInternalErrorString = (String)workItem.getParameter("isInternalError");
		if (isInternalErrorString==null) {
			logger.debug("isInternalError parameter is null!");
		}else {
			isInternalError = isInternalErrorString.equals("true");
		}
		
		Map<String, Object> results = null;
		
		try{
			flowResponse.setFlowResult(messageManager.getMessageText(
					IMessageManager.RESULT_NEGATIVO_KEY));
			// TODO: capire quale codice di errore si aspettino i sistemi a monte
			flowResponse.setFlowCodeResult("000");

			if (isInternalError) {
				flowResponse.setFlowDescResult(messageManager.getMessageText(
						IMessageManager.RESULT_ERROR_INTERNAL_KEY));
			}else {
				flowResponse.setFlowDescResult(messageManager.getMessageText(
						IMessageManager.RESULT_ERROR_NOK_KEY));
			}
			flowResponse.setFlowReiteration(messageManager.getMessageText(
					IMessageManager.RESULT_REITERATION_N_KEY));
			
			if (error!=null) {
				logger.error(error, error);
			}
			results = new HashMap<String, Object>();
			results.put("flowResponse", flowResponse);
			
		}catch (Throwable t){
			//TODO gestione eccezioni
			logger.error(t , t);
			
			// MOTTA non dovrebbe MAI passare di qui...
			results = new HashMap<String, Object>();
			results.put("flowResponse", flowResponse);
		}finally {
			manager.completeWorkItem(workItem.getId(), results);
		}

	}

}
