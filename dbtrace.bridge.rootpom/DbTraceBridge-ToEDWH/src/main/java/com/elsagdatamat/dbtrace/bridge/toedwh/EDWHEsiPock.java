package com.elsagdatamat.dbtrace.bridge.toedwh;

import java.util.List;

public class EDWHEsiPock {
	public EDWHHeader header;
	public List<EDWHEsiti> esiti;
	public EDWHFooter footer;

	/*
	public String toCsv()
	{
		String s = header.toCsv();
		for (EDWHEsiti e : esiti)
		{
			s+=e.toCsv();
		}
		s+=footer.toCsv();
		return s;
	}
	*/
	//SCOTT: INCREASING PERFORMANCE!
	public String toCsv()
	{
		
		StringBuilder stringBuilder = new StringBuilder(1024*1024*2);//2M
		
		stringBuilder.append(header.toCsv());
		for (EDWHEsiti e : esiti)
		{
			stringBuilder.append(e.toCsv());
		}
		stringBuilder.append(footer.toCsv());
		
		return stringBuilder.toString();
	}
}
