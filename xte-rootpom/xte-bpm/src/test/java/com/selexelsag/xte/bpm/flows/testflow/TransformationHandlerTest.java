package com.selexelsag.xte.bpm.flows.testflow;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.drools.KnowledgeBase;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextException;

import com.selexelsag.xte.bpm.flows.servicenodes.handlers.IncomingTrasformationNode;
import com.selexelsag.xte.model.flows.FlowParams;

public class TransformationHandlerTest {
	
	@Test
	public void test() {
		
		KnowledgeBase kbase = null;
		ApplicationContext appContext;
		try {

//			appContext = new ClassPathXmlApplicationContext(
//					new String[]{"applicationContextTest.xml",
//							"trackConfTest.xml"
//					});
			
			kbase = readKnowledgeBase();
			
			FlowParams flowParams = new FlowParams();
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("flowParams", flowParams);
			
			StringBuilder data = new StringBuilder(FileUtils.readFileToString(new File("D:\\Elsag\\Trasformata\\listacd_br.xml")));
			params.put("data", data);
			
			StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
			ksession.getWorkItemManager().registerWorkItemHandler("TrasformationNode", new IncomingTrasformationNode());
			ksession.startProcess("com.selexelsag.xte.bpm.trasformazione", params);
			
		}catch (BeansException be) {

			System.err.println("eccezione " + be);
			be.printStackTrace();
			throw new ApplicationContextException("Error loading context", be);
			
		}catch(Throwable e) {
			e.printStackTrace();
			return;
		}
		
	
		
	}
	
	
	private static KnowledgeBase readKnowledgeBase() throws Exception {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		kbuilder.add(ResourceFactory.newClassPathResource("trasformazione.bpmn"), ResourceType.BPMN2);
		return kbuilder.newKnowledgeBase();
	}

}
