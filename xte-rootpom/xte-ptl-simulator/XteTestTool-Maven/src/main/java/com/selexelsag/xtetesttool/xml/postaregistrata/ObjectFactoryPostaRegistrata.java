package com.selexelsag.xtetesttool.xml.postaregistrata;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.selexelsag.xtetesttool.xml.postaregistrata package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactoryPostaRegistrata {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.selexelsag.xtetesttool.xml.postaregistrata
     * 
     */
    public ObjectFactoryPostaRegistrata() {
    }

    /**
     * Create an instance of {@link PostaRegistrata.CM }
     * 
     */
    public PostaRegistrata.CM createPostaRegistrataCM() {
        return new PostaRegistrata.CM();
    }

    /**
     * Create an instance of {@link PostaRegistrata.SP }
     * 
     */
    public PostaRegistrata.SP createPostaRegistrataSP() {
        return new PostaRegistrata.SP();
    }
    
    /**
     * Create an instance of {@link PostaRegistrata.AP }
     * 
     */
    public PostaRegistrata.AP createPostaRegistrataAP() {
        return new PostaRegistrata.AP();
    }
    
    /**
     * Create an instance of {@link PostaRegistrata.PA }
     * 
     */
    public PostaRegistrata.PA createPostaRegistrataPA() {
        return new PostaRegistrata.PA();
    }
    
    /**
     * Create an instance of {@link PostaRegistrata.PO }
     * 
     */
    public PostaRegistrata.PO createPostaRegistrataPO() {
        return new PostaRegistrata.PO();
    }

    /**
     * Create an instance of {@link PostaRegistrata.LO }
     * 
     */
    public PostaRegistrata.LO createPostaRegistrataLO() {
        return new PostaRegistrata.LO();
    }

    /**
     * Create an instance of {@link PostaRegistrata.AM }
     * 
     */
    public PostaRegistrata.AM createPostaRegistrataAM() {
        return new PostaRegistrata.AM();
    }

    /**
     * Create an instance of {@link PostaRegistrata.NM }
     * 
     */
    public PostaRegistrata.NM createPostaRegistrataNM() {
        return new PostaRegistrata.NM();
    }

    /**
     * Create an instance of {@link PostaRegistrata.AC }
     * 
     */
    public PostaRegistrata.AC createPostaRegistrataAC() {
        return new PostaRegistrata.AC();
    }

    /**
     * Create an instance of {@link PostaRegistrata.LM }
     * 
     */
    public PostaRegistrata.LM createPostaRegistrataLM() {
        return new PostaRegistrata.LM();
    }

    /**
     * Create an instance of {@link PostaRegistrata }
     * 
     */
    public PostaRegistrata createPostaRegistrata() {
        return new PostaRegistrata();
    }

}
