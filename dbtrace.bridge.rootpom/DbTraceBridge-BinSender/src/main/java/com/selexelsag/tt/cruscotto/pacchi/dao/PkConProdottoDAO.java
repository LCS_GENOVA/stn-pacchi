package com.selexelsag.tt.cruscotto.pacchi.dao;

import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.selexelsag.tt.cruscotto.pacchi.bean.PkConProdotto;
import com.selexelsag.tt.cruscotto.pacchi.bean.PkConProdottoPK;

public class PkConProdottoDAO extends SpringHibernateJpaDAOBase<PkConProdotto, PkConProdottoPK> 
implements IPkConProdottoDAO{

}
