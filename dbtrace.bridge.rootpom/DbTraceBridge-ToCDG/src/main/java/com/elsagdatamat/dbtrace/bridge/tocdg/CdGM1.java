package com.elsagdatamat.dbtrace.bridge.tocdg;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "MSG")
public class CdGM1 extends CdGMessage {
/*
<?xml version="1.0"?>
<!DOCTYPE MSG SYSTEM "STARMSG.DTD">
<MSG>
<HDR SWREL="A0000" OFCID="11980"/>
<M1>
	<M1D>
		<OBJ>
		<OBJID>200000004494</OBJID>
		<PH>A</PH>
		<PSTF>0</PSTF>
		<OP>10</OP>
		<TDT>03/03/2004 15:50:00</TDT>
		<SUBC>R</SUBC>
		</OBJ>

		<ACC>
		<AOFC>11980</AOFC>
		<ADT>03/03/2004</ADT>
		<ZIP>16147</ZIP>
		<PERC>1000</PERC>
		<FARE>1000</FARE>
		<ACURR>L</ACURR>
		<REGT>A</REGT>
		<SA>SP</SA>
		</ACC>
	</M1D>
</M1>
</MSG>

 */

//	@XmlElement(name = "HDR")
//	public CdGHeader header;
	
	@XmlElementWrapper(name= "M1")
	@XmlElement(name = "M1D")
	public List<CdGM1D> m1dList;
}
