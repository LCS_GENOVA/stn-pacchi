package com.elsagdatamat.trackdb.toedwh.dao;

import com.elsagdatamat.framework.dao.IGenericDAO;
import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSend;
import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSendPK;


public interface ITraceTypesToSendDAO extends IGenericDAO<TraceTypesToSend, TraceTypesToSendPK> {

}