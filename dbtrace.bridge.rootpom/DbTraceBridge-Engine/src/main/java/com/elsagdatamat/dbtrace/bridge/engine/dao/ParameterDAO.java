package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.Parameter;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class ParameterDAO extends SpringHibernateJpaDAOBase<Parameter, String> implements IParameterDAO{

}
