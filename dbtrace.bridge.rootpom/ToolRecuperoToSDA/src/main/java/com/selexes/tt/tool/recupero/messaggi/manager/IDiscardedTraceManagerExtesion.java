package com.selexes.tt.tool.recupero.messaggi.manager;

import java.util.List;

import com.elsagdatamat.dbtrace.bridge.engine.bl.IDiscardedTraceManager;
import com.elsagdatamat.dbtrace.bridge.engine.entities.DiscardedTrace;

public interface IDiscardedTraceManagerExtesion extends IDiscardedTraceManager{
	
	public  List<Object> getMessages(String discardedProcess, String fromDate, String toDate);
	public DiscardedTrace getMessagesById(Long id);
		
}
