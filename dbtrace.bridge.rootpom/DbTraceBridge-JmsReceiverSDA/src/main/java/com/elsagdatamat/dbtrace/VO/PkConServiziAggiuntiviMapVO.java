package com.elsagdatamat.dbtrace.VO;

import java.util.HashMap;

public class PkConServiziAggiuntiviMapVO extends HashMap<String, String> {

	public static final String separatore= "___";
	public static final String any= "*";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getValoreMappatoTag(String tagName, String tagVal)
	{
		return get(tagName + separatore + tagVal);
	}

	public String getValoreMappatoTag(String tagName) {
		return get(tagName + separatore + any);
	}	
}
