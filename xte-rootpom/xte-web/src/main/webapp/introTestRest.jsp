<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>XTE - Rest call test</title>
</head>
<body>


	<h1>XTE REST call test</h1>

	<form action="testRest.jsp" method="post">
		<label for="service">Service:</label><br />
		<input type="text"  name="service" id="service"/>
		<br />
		<label for="channel">Channel:</label><br />
		<input type="text"  name="channel" id="channel"/>
		<br />
		<label for="sourceOfficeID">sourceOfficeID:</label><br />
		<input type="text"  name="sourceOfficeID" id="sourceOfficeID"/>
		<br />
		<label for="destinationOfficeID">destinationOfficeID:</label><br />
		<input type="text"  name="destinationOfficeID" id="destinationOfficeID"/>
		<br />
		<label for="objectID">objectID:</label><br />
		<input type="text"  name="objectID" id="objectID"/>
		<br />
		<label for="eventName">Event name:</label><br />
		<input type="text"  name="eventName" id="eventName"/>
		<br />
		<label for="version">Version:</label><br />
		<input type="text"  name="version" id="version"/>
		<br />
		<input type="submit" value="Send" />
	</form> 
 
 

</body>
</html>