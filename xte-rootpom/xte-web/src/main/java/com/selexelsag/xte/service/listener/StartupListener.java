package com.selexelsag.xte.service.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public class StartupListener implements ServletContextListener
{
	
	public final static String LOG_CONF_FILENAME = "log4jFileName";
	
    @Override
    public void contextDestroyed(ServletContextEvent event)
    {
        
    }
 
    @Override
    public void contextInitialized(ServletContextEvent sce)
    {
        Logger logger = null;
        ServletContext servletContext = sce.getServletContext();
        String log4jFile = servletContext.getInitParameter(LOG_CONF_FILENAME);
        DOMConfigurator.configure(log4jFile);
        logger = LogManager.getLogger(StartupListener.class.getName());
        logger.debug("Loaded: " + log4jFile);
    }
}