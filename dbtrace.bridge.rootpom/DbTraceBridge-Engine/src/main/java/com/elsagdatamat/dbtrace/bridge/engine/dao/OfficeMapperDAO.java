package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.OfficeMapper;
import com.elsagdatamat.dbtrace.bridge.engine.entities.OfficeMapperPK;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class OfficeMapperDAO extends SpringHibernateJpaDAOBase<OfficeMapper, OfficeMapperPK> implements IOfficeMapperDAO{

}
