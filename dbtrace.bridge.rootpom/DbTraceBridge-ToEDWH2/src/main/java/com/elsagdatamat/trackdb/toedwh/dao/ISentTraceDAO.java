package com.elsagdatamat.trackdb.toedwh.dao;

import java.util.List;

import com.elsagdatamat.trackdb.toedwh.entities.SentTrace;
import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSend;


public interface ISentTraceDAO {

	void insertSentTraces(List<SentTrace> vSentTraces);
	void insertSentTracesBatch(List<SentTrace> vSentTraces);

	void fillSentTraces(Long id, List<TraceTypesToSend> typesToSend);

}