package com.selexelsag.xte.extension.probing.servlet.postamassiva;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.selexelsag.commons.XmlUtils;
import com.selexelsag.xte.extension.probing.listener.ProbeServletContextListener;
import com.selexelsag.xte.extension.probing.model.XteProbeResponse;
import com.selexelsag.xte.extension.probing.postamassiva.IProbePostaMassiva;
import com.selexelsag.xte.extension.probing.postamassiva.model.XteProbeRequestPostaMassiva;
import com.selexelsag.xte.extension.probing.service.postamassiva.ProbePostaMassiva;
import com.selexelsag.xte.extension.probing.servlet.XteProbeBaseServlet;
import com.selexelsag.xte.extension.probing.servlet.exception.ProbeInvalidRequestException;
/**
 * Servlet punto di ingresso per le richieste riguardanti il flusso Posta Massiva (PM)
 * @author Frosi
 *
 */
public class XteProbePostaMassivaServlet extends XteProbeBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2717626620869118357L;
	private static Log _log = LogFactory.getLog(XteProbePostaMassivaServlet.class); 
	private IProbePostaMassiva _probeMassiva;

	public IProbePostaMassiva getProbeMassiva() {
		return _probeMassiva;
	}

	public void setProbeMassiva(IProbePostaMassiva probeMassiva) {
		_probeMassiva = probeMassiva;
	}

	protected XteProbeRequestPostaMassiva validateRequest(HttpServletRequest request) throws ProbeInvalidRequestException{
		XteProbeRequestPostaMassiva ans = new XteProbeRequestPostaMassiva();
		String timeoutParam =  request.getParameter(TIMEOUT_REQ_PARAM);
		try{
			if (timeoutParam != null)
				ans.setTimeout(Integer.parseInt(timeoutParam) * WSTIMEOUT_SCALEFACTOR);
		}catch (NumberFormatException nfe){
			try{
				float msec = (Float.parseFloat(timeoutParam)* (float)WSTIMEOUT_SCALEFACTOR);
				if (msec != (int)msec)
					throw new Exception();
				ans.setTimeout((int)msec);
			}catch (Throwable t){			
				throw new ProbeInvalidRequestException(String.format("Posta massiva servlet - Invalid request param: %1$s=[%2$s] - Timeout param must be an integer or float with max %3$s decimal digits", TIMEOUT_REQ_PARAM, timeoutParam, (String.valueOf(WSTIMEOUT_SCALEFACTOR).length() -1)));
			}
		}
		return ans;
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out = null;
		try{
			out = response.getWriter();
		}catch (IOException e){
			_log.error("Posta massiva servlet - Error getting servlet writer", e);
			return;
		}
		try{
			_log.info(String.format("Posta massiva servlet BEGINS: requestid=[%1$s],  url=[%2$s]",request.hashCode(), getUrl(request)));
			request.setCharacterEncoding("UTF-8");
			XteProbeRequestPostaMassiva inputWs = validateRequest(request);
			XteProbeResponse outputWs = new XteProbeResponse();
			IProbePostaMassiva service = new ProbePostaMassiva();
			service.setConfigurationProperties((Properties)getServletContext().getAttribute(ProbeServletContextListener.ATTRIBUTE_NAME));
			_log.info(String.format("Posta massiva servlet ws call BEGINS: requestid=[%1$s]", request.hashCode()));
			outputWs = service.probe(inputWs, outputWs);
			_log.info(String.format("Posta massiva servlet ws call ENDS: requestid=[%1$s]", request.hashCode()));
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/xml");
			_log.info(String.format("Posta massiva servlet send response BEGINS: requestid=[%1$s]", request.hashCode()));
			out.println(XmlUtils.serializeJaxbClass(XteProbeResponse.class, outputWs));
			_log.info(String.format("Posta massiva servlet send response ENDS: requestid=[%1$s]", request.hashCode()));
		}
		catch (ProbeInvalidRequestException reqExc){
			_log.error(ERROR_VALIDATION, reqExc);
			try{
				request.setAttribute("errorDesc", reqExc.getMessage());
				getServletContext().getRequestDispatcher("/WEB-INF/exceptionError.jsp").forward(request, response);
			}catch(Throwable t){
				_log.error(ERROR_JSP_FORWARD, t);
			}
		}catch (Throwable e){
			_log.error(ERROR_GENERIC, e);
			try{
				request.setAttribute("errorDesc", ERROR_GENERIC);
				getServletContext().getRequestDispatcher("/WEB-INF/exceptionError.jsp").forward(request, response);
			}catch(Throwable t){
				_log.error("Posta massiva servlet - Error forwarding to error response", t);
			}
		}finally{
			if (out != null){
				out.flush();
				out.close();
			}
		}
		_log.info(String.format("Posta massiva servlet ENDS: requestid=[%1$s]", request.hashCode()));
	}


}
