/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolContentPaneView extends XteTestToolPanelView {

    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolContentPaneView.class);
    private ImageIcon xteServerReadyIcon = null;
    private XteTestToolApplicationFrameView applicationFrame = null;
    private boolean useImageTimeMap = false;
    private Map<Character, ImageIcon> imageTimeMap = new HashMap<Character, ImageIcon>();
    public static final HashMap<Character, String> STRING_TIME_MAP = new HashMap<Character, String>();
    static {
        STRING_TIME_MAP.put('0', "0");
        STRING_TIME_MAP.put('1', "1");
        STRING_TIME_MAP.put('2', "2");
        STRING_TIME_MAP.put('3', "3");
        STRING_TIME_MAP.put('4', "4");
        STRING_TIME_MAP.put('5', "5");
        STRING_TIME_MAP.put('6', "6");
        STRING_TIME_MAP.put('7', "7");
        STRING_TIME_MAP.put('8', "8");
        STRING_TIME_MAP.put('9', "9");
        STRING_TIME_MAP.put(':', ":");
    }

    public XteTestToolContentPaneView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try
        {
            logger.info("XteTestToolContentPaneView start class creation");
            this.applicationFrame = applicationFrame;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.xteServerReadyIcon = new ImageIcon(getClass().getResource("/images/ServerReady1.png"));
            buildImageTimeMap();
            initComponents();
            logger.info("XteTestToolContentPaneView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolContentPaneView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Content Pane creation failed", ex);
        }
    }

    public JPanel getTotemContentPaneRef(){ return this; }
    public JLabel getTotemGateIDLabelRef(){ return this.xteTestToolTitlejLabel; }
    public JPanel getTotemGateDateTimePanelRef(){ return this.dataTimejPanel; }
    public JLabel getTotemGateDateLabelRef(){ return this.datejLabel; }
    public JPanel getTotemGateTimePanelRef(){ return this.timejPanel; }
    public JLabel getTotemGateTimeHour1LabelRef(){ return this.hour1jLabel; }
    public JLabel getTotemGateTimeHour2LabelRef(){ return this.hour2jLabel; }
    public JLabel getTotemGateTimeHMSeparatorLabelRef(){ return this.hourminuteSeparatorjLabel; }
    public JLabel getTotemGateTimeMinute1LabelRef(){ return this.minute1jLabel; }
    public JLabel getTotemGateTimeMinute2LabelRef(){ return this.minute2jLabel; }
    public JLabel getTotemGateTimeMSSeparatorLabelRef(){ return this.minutesecondSeparatorjLabel; }
    public JLabel getTotemGateTimeSecond1LabelRef(){ return this.second1jLabel; }
    public JLabel getTotemGateTimeSecond2LabelRef(){ return this.second2jLabel; }

    public Map<Character, ImageIcon> getImageTimeMap() {
        return imageTimeMap;
    }

    public boolean isUseImageTimeMap() {
        return useImageTimeMap;
    }
    
    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolContentPaneView start initializeView");
            this.xteServerReadyjLabel.setIcon(null);
            this.xteServerReadyjLabel.setText("");
            logger.info("XteTestToolContentPaneView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolContentPaneView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Content Pane initialization failed", ex);
        }
    }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolContentPaneView start customizeView");
            this.xteTestToolTitlejLabel.setText(messages.getString("header.view.title.label"));
            logger.info("XteTestToolContentPaneView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolContentPaneView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Content Pane customization failed", ex);
        }
    }

    private void buildImageTimeMap()
    {
        try{
            imageTimeMap.put('0', new ImageIcon(getClass().getResource("/images/datetime/0.png")));
            imageTimeMap.put('1', new ImageIcon(getClass().getResource("/images/datetime/1.png")));
            imageTimeMap.put('2', new ImageIcon(getClass().getResource("/images/datetime/2.png")));
            imageTimeMap.put('3', new ImageIcon(getClass().getResource("/images/datetime/3.png")));
            imageTimeMap.put('4', new ImageIcon(getClass().getResource("/images/datetime/4.png")));
            imageTimeMap.put('5', new ImageIcon(getClass().getResource("/images/datetime/5.png")));
            imageTimeMap.put('6', new ImageIcon(getClass().getResource("/images/datetime/6.png")));
            imageTimeMap.put('7', new ImageIcon(getClass().getResource("/images/datetime/7.png")));
            imageTimeMap.put('8', new ImageIcon(getClass().getResource("/images/datetime/8.png")));
            imageTimeMap.put('9', new ImageIcon(getClass().getResource("/images/datetime/9.png")));
            imageTimeMap.put(':', new ImageIcon(getClass().getResource("/images/datetime/points.png")));
            useImageTimeMap = true;
        }
        catch(Exception ex)
        {
            useImageTimeMap = false;
        }
    }

    public void notifyXteServerReady(String xteServerReady) {
        logger.debug("XteTestToolContentPaneView notifyXteServerReady");
        this.xteServerReadyjLabel.setIcon(xteServerReadyIcon);
        this.xteServerReadyjLabel.setText(xteServerReady);
}

    public void notifyXteServerReleased() {
        logger.debug("XteTestToolContentPaneView notifyXteServerReleased");
        this.xteServerReadyjLabel.setIcon(null);
        this.xteServerReadyjLabel.setText("");
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        headerjPanel = new javax.swing.JPanel();
        logoPosteItalianejLabel = new javax.swing.JLabel();
        logoSelexElsagjLabel = new javax.swing.JLabel();
        xteInfojPanel = new javax.swing.JPanel();
        xteTestToolTitlejPanel = new javax.swing.JPanel();
        xteTestToolTitlejLabel = new javax.swing.JLabel();
        xteServerReadyjPanel = new javax.swing.JPanel();
        xteServerReadyjLabel = new javax.swing.JLabel();
        dataTimejPanel = new javax.swing.JPanel();
        dateIconjLabel = new javax.swing.JLabel();
        timeIconjLabel = new javax.swing.JLabel();
        datejLabel = new javax.swing.JLabel();
        timejPanel = new javax.swing.JPanel();
        hour1jLabel = new javax.swing.JLabel();
        hour2jLabel = new javax.swing.JLabel();
        hourminuteSeparatorjLabel = new javax.swing.JLabel();
        minute1jLabel = new javax.swing.JLabel();
        minute2jLabel = new javax.swing.JLabel();
        minutesecondSeparatorjLabel = new javax.swing.JLabel();
        second1jLabel = new javax.swing.JLabel();
        second2jLabel = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEtchedBorder());
        setMaximumSize(new java.awt.Dimension(1000, 764));
        setMinimumSize(new java.awt.Dimension(1000, 764));
        setPreferredSize(new java.awt.Dimension(1000, 764));

        headerjPanel.setBackground(new java.awt.Color(0, 51, 255));
        headerjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        headerjPanel.setMaximumSize(new java.awt.Dimension(980, 70));
        headerjPanel.setMinimumSize(new java.awt.Dimension(980, 70));
        headerjPanel.setPreferredSize(new java.awt.Dimension(980, 70));

        logoPosteItalianejLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Poste Italiane Logo.png"))); // NOI18N
        logoPosteItalianejLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        logoPosteItalianejLabel.setFocusable(false);
        logoPosteItalianejLabel.setMaximumSize(new java.awt.Dimension(170, 48));
        logoPosteItalianejLabel.setMinimumSize(new java.awt.Dimension(170, 48));
        logoPosteItalianejLabel.setPreferredSize(new java.awt.Dimension(170, 48));

        logoSelexElsagjLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/SelexElsag Logo.png"))); // NOI18N
        logoSelexElsagjLabel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51)));
        logoSelexElsagjLabel.setFocusable(false);
        logoSelexElsagjLabel.setMaximumSize(new java.awt.Dimension(170, 48));
        logoSelexElsagjLabel.setMinimumSize(new java.awt.Dimension(170, 48));
        logoSelexElsagjLabel.setPreferredSize(new java.awt.Dimension(170, 48));

        xteInfojPanel.setBackground(new java.awt.Color(255, 255, 255));
        xteInfojPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        xteInfojPanel.setMaximumSize(new java.awt.Dimension(354, 50));
        xteInfojPanel.setMinimumSize(new java.awt.Dimension(354, 50));
        xteInfojPanel.setPreferredSize(new java.awt.Dimension(354, 50));
        xteInfojPanel.setLayout(new java.awt.GridBagLayout());

        xteTestToolTitlejPanel.setBackground(new java.awt.Color(255, 255, 255));
        xteTestToolTitlejPanel.setMaximumSize(new java.awt.Dimension(146, 48));
        xteTestToolTitlejPanel.setMinimumSize(new java.awt.Dimension(146, 48));
        xteTestToolTitlejPanel.setOpaque(false);
        xteTestToolTitlejPanel.setPreferredSize(new java.awt.Dimension(146, 48));
        xteTestToolTitlejPanel.setLayout(new java.awt.GridBagLayout());

        xteTestToolTitlejLabel.setBackground(new java.awt.Color(255, 255, 255));
        xteTestToolTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        xteTestToolTitlejLabel.setForeground(new java.awt.Color(102, 102, 102));
        xteTestToolTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        xteTestToolTitlejLabel.setText("XTE Test Tool");
        xteTestToolTitlejLabel.setFocusable(false);
        xteTestToolTitlejLabel.setIconTextGap(2);
        xteTestToolTitlejLabel.setMaximumSize(new java.awt.Dimension(146, 48));
        xteTestToolTitlejLabel.setMinimumSize(new java.awt.Dimension(146, 48));
        xteTestToolTitlejLabel.setPreferredSize(new java.awt.Dimension(146, 48));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        xteTestToolTitlejPanel.add(xteTestToolTitlejLabel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        xteInfojPanel.add(xteTestToolTitlejPanel, gridBagConstraints);

        xteServerReadyjPanel.setBackground(new java.awt.Color(255, 255, 255));
        xteServerReadyjPanel.setMaximumSize(new java.awt.Dimension(202, 48));
        xteServerReadyjPanel.setMinimumSize(new java.awt.Dimension(202, 48));
        xteServerReadyjPanel.setPreferredSize(new java.awt.Dimension(202, 48));
        xteServerReadyjPanel.setLayout(new java.awt.GridBagLayout());

        xteServerReadyjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        xteServerReadyjLabel.setForeground(new java.awt.Color(0, 153, 0));
        xteServerReadyjLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        xteServerReadyjLabel.setText("<html><body>Server: 172.31.30.120<br>Port: 8080<br>Version: 4.0.0.0<body><html>");
        xteServerReadyjLabel.setMaximumSize(new java.awt.Dimension(202, 48));
        xteServerReadyjLabel.setMinimumSize(new java.awt.Dimension(202, 48));
        xteServerReadyjLabel.setPreferredSize(new java.awt.Dimension(202, 48));
        xteServerReadyjLabel.setRequestFocusEnabled(false);
        xteServerReadyjPanel.add(xteServerReadyjLabel, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        xteInfojPanel.add(xteServerReadyjPanel, gridBagConstraints);

        dataTimejPanel.setBackground(new java.awt.Color(255, 255, 255));
        dataTimejPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        dataTimejPanel.setMaximumSize(new java.awt.Dimension(240, 50));
        dataTimejPanel.setMinimumSize(new java.awt.Dimension(240, 50));
        dataTimejPanel.setPreferredSize(new java.awt.Dimension(240, 50));
        dataTimejPanel.setLayout(new java.awt.GridLayout(2, 2));

        dateIconjLabel.setBackground(new java.awt.Color(230, 230, 230));
        dateIconjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        dateIconjLabel.setForeground(new java.awt.Color(51, 51, 51));
        dateIconjLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        dateIconjLabel.setText("DATA");
        dateIconjLabel.setIconTextGap(5);
        dateIconjLabel.setMaximumSize(new java.awt.Dimension(20, 48));
        dateIconjLabel.setMinimumSize(new java.awt.Dimension(20, 48));
        dateIconjLabel.setOpaque(true);
        dateIconjLabel.setPreferredSize(new java.awt.Dimension(20, 48));
        dataTimejPanel.add(dateIconjLabel);

        timeIconjLabel.setBackground(new java.awt.Color(230, 230, 230));
        timeIconjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        timeIconjLabel.setForeground(new java.awt.Color(51, 51, 51));
        timeIconjLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        timeIconjLabel.setText("ORA");
        timeIconjLabel.setMaximumSize(new java.awt.Dimension(20, 48));
        timeIconjLabel.setMinimumSize(new java.awt.Dimension(20, 48));
        timeIconjLabel.setOpaque(true);
        timeIconjLabel.setPreferredSize(new java.awt.Dimension(20, 48));
        dataTimejPanel.add(timeIconjLabel);

        datejLabel.setBackground(new java.awt.Color(0, 0, 0));
        datejLabel.setFont(new java.awt.Font("Tahoma", 1, 19)); // NOI18N
        datejLabel.setForeground(new java.awt.Color(255, 255, 255));
        datejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        datejLabel.setText("12/12/2011");
        datejLabel.setMaximumSize(new java.awt.Dimension(100, 48));
        datejLabel.setMinimumSize(new java.awt.Dimension(100, 48));
        datejLabel.setOpaque(true);
        datejLabel.setPreferredSize(new java.awt.Dimension(100, 48));
        dataTimejPanel.add(datejLabel);

        timejPanel.setBackground(new java.awt.Color(0, 0, 0));
        timejPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        timejPanel.setMaximumSize(new java.awt.Dimension(70, 48));
        timejPanel.setMinimumSize(new java.awt.Dimension(70, 48));
        timejPanel.setPreferredSize(new java.awt.Dimension(70, 48));
        timejPanel.setLayout(new java.awt.GridLayout(1, 0));

        hour1jLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        hour1jLabel.setForeground(new java.awt.Color(255, 255, 255));
        hour1jLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        hour1jLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        hour1jLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        hour1jLabel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        timejPanel.add(hour1jLabel);

        hour2jLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        hour2jLabel.setForeground(new java.awt.Color(255, 255, 255));
        hour2jLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        hour2jLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        hour2jLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        hour2jLabel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        timejPanel.add(hour2jLabel);

        hourminuteSeparatorjLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        hourminuteSeparatorjLabel.setForeground(new java.awt.Color(255, 255, 255));
        hourminuteSeparatorjLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        hourminuteSeparatorjLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        hourminuteSeparatorjLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        hourminuteSeparatorjLabel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        timejPanel.add(hourminuteSeparatorjLabel);

        minute1jLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        minute1jLabel.setForeground(new java.awt.Color(255, 255, 255));
        minute1jLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        minute1jLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        minute1jLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        minute1jLabel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        timejPanel.add(minute1jLabel);

        minute2jLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        minute2jLabel.setForeground(new java.awt.Color(255, 255, 255));
        minute2jLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        minute2jLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        minute2jLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        minute2jLabel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        timejPanel.add(minute2jLabel);

        minutesecondSeparatorjLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        minutesecondSeparatorjLabel.setForeground(new java.awt.Color(255, 255, 255));
        minutesecondSeparatorjLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        minutesecondSeparatorjLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        minutesecondSeparatorjLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        minutesecondSeparatorjLabel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        timejPanel.add(minutesecondSeparatorjLabel);

        second1jLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        second1jLabel.setForeground(new java.awt.Color(255, 255, 255));
        second1jLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        second1jLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        second1jLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        second1jLabel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        timejPanel.add(second1jLabel);

        second2jLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        second2jLabel.setForeground(new java.awt.Color(255, 255, 255));
        second2jLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        second2jLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        second2jLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        second2jLabel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        timejPanel.add(second2jLabel);

        dataTimejPanel.add(timejPanel);

        javax.swing.GroupLayout headerjPanelLayout = new javax.swing.GroupLayout(headerjPanel);
        headerjPanel.setLayout(headerjPanelLayout);
        headerjPanelLayout.setHorizontalGroup(
            headerjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerjPanelLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(logoPosteItalianejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(logoSelexElsagjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(xteInfojPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dataTimejPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        headerjPanelLayout.setVerticalGroup(
            headerjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headerjPanelLayout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addGroup(headerjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(logoPosteItalianejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(logoSelexElsagjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(headerjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(headerjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(dataTimejPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(xteInfojPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(headerjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(headerjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(690, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel dataTimejPanel;
    private javax.swing.JLabel dateIconjLabel;
    private javax.swing.JLabel datejLabel;
    private javax.swing.JPanel headerjPanel;
    private javax.swing.JLabel hour1jLabel;
    private javax.swing.JLabel hour2jLabel;
    private javax.swing.JLabel hourminuteSeparatorjLabel;
    private javax.swing.JLabel logoPosteItalianejLabel;
    private javax.swing.JLabel logoSelexElsagjLabel;
    private javax.swing.JLabel minute1jLabel;
    private javax.swing.JLabel minute2jLabel;
    private javax.swing.JLabel minutesecondSeparatorjLabel;
    private javax.swing.JLabel second1jLabel;
    private javax.swing.JLabel second2jLabel;
    private javax.swing.JLabel timeIconjLabel;
    private javax.swing.JPanel timejPanel;
    private javax.swing.JPanel xteInfojPanel;
    private javax.swing.JLabel xteServerReadyjLabel;
    private javax.swing.JPanel xteServerReadyjPanel;
    private javax.swing.JLabel xteTestToolTitlejLabel;
    private javax.swing.JPanel xteTestToolTitlejPanel;
    // End of variables declaration//GEN-END:variables


}
