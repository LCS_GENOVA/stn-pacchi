package com.elsagdatamat.dbtrace.bl;

import java.util.HashMap;

public interface IPkConServiziAggiuntiviBL {

	HashMap<String, String> getAll(String sistema);

}
