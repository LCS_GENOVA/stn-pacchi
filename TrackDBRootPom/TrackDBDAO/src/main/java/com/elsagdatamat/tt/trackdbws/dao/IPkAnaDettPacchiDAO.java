package com.elsagdatamat.tt.trackdbws.dao;


import java.util.List;

import com.elsagdatamat.framework.dao.IGenericDAO;
import com.elsagdatamat.tt.trackdbws.beans.VO.PackDataRetrieveVO;
import com.selexelsag.tt.cruscottopacchi.bean.PkAnaDettPacchi;
import com.selexelsag.tt.cruscottopacchi.bean.PkAnaDettPacchiPK;
/**
* R.L. BeanGenerator - 27.05.2012 18:17:43
* R.L. - Inseriti per il nuovo metodo del trackDbWS - packDataRetrieve
*/
public interface IPkAnaDettPacchiDAO extends IGenericDAO<PkAnaDettPacchi,PkAnaDettPacchiPK>{

	public List<PackDataRetrieveVO> getListOfPackDataRetrieveVO(String hqlQuery);

	List<PackDataRetrieveVO> getListOfPackDataRetrieveVONQ(String idPacco);

}