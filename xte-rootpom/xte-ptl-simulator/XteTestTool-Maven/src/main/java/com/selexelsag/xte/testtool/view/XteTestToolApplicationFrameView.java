 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view;

import com.selexelsag.xte.testtool.controller.XteTestToolContentPaneController;
import com.selexelsag.xte.testtool.controller.XteTestToolGlassPaneController;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.UIManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolApplicationFrameView extends JFrame {

    private XteTestToolDataModel applicationFrameModel;
    private XteTestToolContentPaneView contentPaneView;
    private XteTestToolGlassPaneView glassPaneView;
    private static Logger logger = Logger.getLogger(XteTestToolApplicationFrameView.class);

    public XteTestToolApplicationFrameView(XteTestToolDataModel model) {

        try {
            logger.info("XteTestToolApplicationFrameView start class creation");

            logger.debug("XteTestToolApplicationFrameView setting data model");
            this.applicationFrameModel = model;

            logger.debug("XteTestToolApplicationFrameView define and customize look and feel");
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            UIManager.put("ScrollBar.width", new Integer(22));
            UIManager.put("ScrollBar.thumb", new Color(200, 200, 190));
            UIManager.put("ScrollBar.track", new Color(240, 240, 240));
            UIManager.put("Button.focus", new Color(255,204,51));
            UIManager.put("OptionPane.minimumSize", new Dimension(300,120));
            UIManager.put("OptionPane.messageFont", new Font("Tahoma", Font.BOLD, 14));
            UIManager.put("OptionPane.messageForeground", new Color(80, 80, 80));
            UIManager.put("OptionPane.buttonFont", new Font("Tahoma", Font.BOLD, 14));
            UIManager.put("OptionPane.informationIcon", new ImageIcon(getClass().getResource("/images/InfoIcon.png")));
            UIManager.put("OptionPane.errorIcon", new ImageIcon(getClass().getResource("/images/ErrorIcon.png")));
            UIManager.put("OptionPane.warningIcon", new ImageIcon(getClass().getResource("/images/WarningIcon.png")));
            UIManager.put("OptionPane.questionIcon", new ImageIcon(getClass().getResource("/images/QuestionIcon.png")));
            UIManager.put("OptionPane.cancelIcon", new ImageIcon(getClass().getResource("/images/Cancel.png")));
            UIManager.put("OptionPane.okIcon", new ImageIcon(getClass().getResource("/images/OK.png")));
            UIManager.put("OptionPane.noIcon", new ImageIcon(getClass().getResource("/images/Cancel.png")));
            UIManager.put("OptionPane.yesIcon", new ImageIcon(getClass().getResource("/images/OK.png")));
            UIManager.put("OptionPane.cancelButtonText", "Cancel");
            UIManager.put("OptionPane.yesButtonText", "Confirm");
            UIManager.put("OptionPane.okButtonText", "Ok");
            UIManager.put("OptionPane.noButtonText", "Cancel");            
            UIManager.put("ComboBox.background", Color.white);
            UIManager.put("ComboBox.selectionBackground", new Color(255,204,102));
            UIManager.put("ComboBox.selectionForeground", Color.black);

            this.setUndecorated(false);
            this.setResizable(false);
            this.setAlwaysOnTop(true);

            logger.debug("XteTestToolApplicationFrameView setting ContentPane");
            contentPaneView = new XteTestToolContentPaneView(this);
            XteTestToolContentPaneController contentPaneController = new XteTestToolContentPaneController(contentPaneView);
            this.setContentPane(contentPaneView);

            logger.debug("XteTestToolApplicationFrameView setting GlassPane");
            glassPaneView = new XteTestToolGlassPaneView(this);
            XteTestToolGlassPaneController glassPaneController = new XteTestToolGlassPaneController(glassPaneView);
            glassPaneView.setOpaque(false);
            glassPaneView.setVisible(false);
            glassPaneView.setNeedToRedispatch(true);
            this.setGlassPane(glassPaneView);

            this.pack();
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setLocationRelativeTo(null);

            logger.info("XteTestToolApplicationFrameView class created successfully");

        } catch (Exception ex) {
            logger.error("XteTestToolApplicationFrameView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Application Frame creation failed", ex);
        }
    }

    public XteTestToolDataModel getApplicationFrameModel() {
        return applicationFrameModel;
    }

    public void setApplicationFrameModel(XteTestToolDataModel applicationFrameModel) {
        this.applicationFrameModel = applicationFrameModel;
    }

    public XteTestToolGlassPaneView getGlassPaneView() {
        return glassPaneView;
    }

    public XteTestToolContentPaneView getContentPaneView() {
        return contentPaneView;
    }
        
    public void restore()
    {
        try {
            logger.info("XteTestToolApplicationFrameView start restore method");
            XteTestToolPanelView cpw = this.applicationFrameModel.getCurrentXteTestToolPanelView();
            if(cpw!=null)
            {
                logger.debug("XteTestToolApplicationFrameView restore view: " + cpw.getClass().getName());
                if(this.getGlassPaneView().isVisible())
                {
                    this.getGlassPaneView().setVisible(false);
                    this.getGlassPaneView().setNeedToRedispatch(true);
                }
                cpw.restoreView();
            }
            else
            {
                logger.debug("XteTestToolApplicationFrameView restore method call shutdownSystem");
                System.exit(-1);
            }

            logger.info("XteTestToolApplicationFrameView restore method executed successfully");

        } catch (Exception ex) {
            logger.error("XteTestToolApplicationFrameView restore method failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Application restore failed", ex);
        }

    }
}
