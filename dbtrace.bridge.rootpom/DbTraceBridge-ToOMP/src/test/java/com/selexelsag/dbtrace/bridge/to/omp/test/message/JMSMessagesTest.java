/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.test.message;

import java.io.ByteArrayInputStream;
import java.util.zip.GZIPInputStream;

import javax.jms.BytesMessage;
import javax.jms.Session;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jms.core.MessageCreator;

import com.mockrunner.jms.ConfigurationManager;
import com.mockrunner.jms.DestinationManager;
import com.mockrunner.mock.jms.MockConnection;
import com.mockrunner.mock.jms.MockSession;
import com.selexelsag.dbtrace.bridge.to.omp.message.D1PCompressedMessageCreator;
import com.selexelsag.dbtrace.bridge.to.omp.message.JmsHeaderConstantsInterface;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1.DHDR;
import com.selexelsag.dbtrace.bridge.to.omp.message.trans.Transformations;

/**
 * @author arodriguez
 *
 */
public class JMSMessagesTest {

    private MockSession session;
    private MockConnection connection;
    
	@Before
	public void initMockedJMSSession() throws Exception {
		connection = new MockConnection(new DestinationManager(), new ConfigurationManager());
        session = new MockSession(connection, false, Session.AUTO_ACKNOWLEDGE);
	}
	
	@Test
	public void decompressGZippedMessageBodyIsSameAsOriginalOne()  {
		MessageFactoryTest msgFactoryTest = new MessageFactoryTest();
		Transformations transformations = new Transformations();
		
		try {
			String xmlMessageBody = transformations.marshallMessageToXml(msgFactoryTest.createOMPJaxbMessage(), null);

			System.out.println("Encoded:\n" + xmlMessageBody);
			
			MessageCreator messageCreator = new D1PCompressedMessageCreator(xmlMessageBody);
			Assert.assertNotNull(messageCreator);
			
			BytesMessage d1EncodedMessage = (BytesMessage)messageCreator.createMessage(session);
			Assert.assertNotNull(d1EncodedMessage);
			d1EncodedMessage.reset();

			byte[] body = new byte[(int)d1EncodedMessage.getBodyLength()];
			Assert.assertEquals(body.length, d1EncodedMessage.readBytes(body));
			
			GZIPInputStream inputStream = new GZIPInputStream(new ByteArrayInputStream(body));
			
			String result = "";
			byte[] buffer = new byte[1024];
			while (inputStream.read(buffer) > -1)
				result += new String(buffer);
			inputStream.close();

			System.out.println("Decoded:\n" + result);
			
			Assert.assertEquals(result.trim(), xmlMessageBody.trim());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new AssertionError(ex);
		}
	}
	
	@Test
	public void d1MessageHeadersAreSameAsMessageBody()  {
		MessageFactoryTest msgFactoryTest = new MessageFactoryTest();
		Transformations transformations = new Transformations();
		
		try {
			MSG d1TestMessage = msgFactoryTest.createOMPJaxbMessage();
			DHDR d1Header = ((D1)d1TestMessage.getHDROrD1().get(1)).getDHDR().get(0);
			String xmlMessageBody = transformations.marshallMessageToXml(d1TestMessage, null);

			System.out.println("Encoded values: OFCOTH=" + d1Header.getOFCOTH() + ", DID=" + d1Header.getDID());
			
			MessageCreator messageCreator = new D1PCompressedMessageCreator(xmlMessageBody, d1Header.getOFCOTH(), d1Header.getDID());
			Assert.assertNotNull(messageCreator);
			
			BytesMessage d1EncodedMessage = (BytesMessage)messageCreator.createMessage(session);
			Assert.assertNotNull(d1EncodedMessage);
			d1EncodedMessage.reset();

			System.out.println("Decoded values: destinationOfficeCode=" + 
								d1EncodedMessage.getStringProperty(JmsHeaderConstantsInterface.DESTINATION_OFFICE_CODE_KEY) + 
								", containerCode=" + 
								d1EncodedMessage.getStringProperty(JmsHeaderConstantsInterface.CONTAINER_CODE_KEY));
			
			Assert.assertEquals(d1Header.getOFCOTH(), d1EncodedMessage.getStringProperty(JmsHeaderConstantsInterface.DESTINATION_OFFICE_CODE_KEY));
			Assert.assertEquals(d1Header.getDID(), d1EncodedMessage.getStringProperty(JmsHeaderConstantsInterface.CONTAINER_CODE_KEY));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new AssertionError(ex);
		}
	}
	
}
