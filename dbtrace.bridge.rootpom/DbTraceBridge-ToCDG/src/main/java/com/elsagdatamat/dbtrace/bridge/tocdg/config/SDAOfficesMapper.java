package com.elsagdatamat.dbtrace.bridge.tocdg.config;

import java.util.Map;

public class SDAOfficesMapper {
	private Map<String, String> ufficiSDA;

	public Map<String, String> getUfficiSDA() {
		return ufficiSDA;
	}

	public void setUfficiSDA(Map<String, String> ufficiSDA) {
		this.ufficiSDA = ufficiSDA;
	}
}
