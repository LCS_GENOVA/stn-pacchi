package com.selexelsag.xte.extension.probing.listener;

import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.selexelsag.xte.service.context.SpringApplicationContext;
import com.selexelsag.xte.service.system.IAppConfig;

public class ProbeServletContextListener implements ServletContextListener {

	public static final String ATTRIBUTE_NAME = "config";
	private static Properties _config = new Properties();
	private static Log _log = LogFactory.getLog(ProbeServletContextListener.class); 
	
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	public void contextInitialized(ServletContextEvent event) {
		 _log.info("init configuration..");
		 try {
			 ServletContext ctx = event.getServletContext(); 
        	 
			 IAppConfig appconfig = (IAppConfig)SpringApplicationContext.getBean(IAppConfig.APP_CONFIG_BEAN_NAME);
			 _config = appconfig.getProbingProperties();
        	 
    		 _log.info(String.format("load configuration:[%1$s]", _config));
    		 ctx.setAttribute(ATTRIBUTE_NAME, _config);
         } catch (Throwable e) { 
             _log.error("Error reading configuration file ", e);
             e.printStackTrace();
         }
         _log.info("infiguration initialized");
     }

	public static ProbeServletContextListener getInstance(ServletContext context) {
		return (ProbeServletContextListener)context.getAttribute(ATTRIBUTE_NAME);
	}

	public Properties getProperty(String key) {
		return _config;
	}
}

