package com.elsagdatamat.dbtrace.bridge.totgc.manager;

import java.util.Date;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;

import com.selexelsag.tracktrace.trackdbws.xml.Trace;





public interface ITraceManager {

	List<Trace> loadNextTraces(Date lastFetchedRecordDate, Long lastFetchedRecordId, Date dateTillto, int maxRows) throws DatatypeConfigurationException;
	
	List<com.selexelsag.tracktrace.trackdbws.xml.Trace> filterByAdditionalServices(List<com.elsagdatamat.tt.trackdbws.beans.Trace> l) throws DatatypeConfigurationException;

	List<com.elsagdatamat.tt.trackdbws.beans.Trace> newLoadNextTraces(Date lastFetchedRecordDate, Long lastFetchedRecordId, Date dateTillto, int maxRows) throws DatatypeConfigurationException;
}
