package com.elsagdatamat.tt.trackdbws.blimpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.tt.trackdbws.beans.VO.EsiPackVO;
import com.elsagdatamat.tt.trackdbws.bl.IEsiPackBL;
import com.elsagdatamat.tt.trackdbws.dao.ITraceDAO;

/*
 * @Author AM,  5/2010
 * @Author AM, 11/2010
 */

public class EsiPackBL implements IEsiPackBL{
	
	@Autowired
	ITraceDAO traceDAO;
	
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<EsiPackVO> getListOfNEsiPacks_PI(Date date,Long esiPackId,
										Date dateTillto,int size) {

		Log log = LogFactory.getLog(getClass());	
		log.debug("getListOfNEsiPacks_PI. Inizio");
		
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String daData=df.format(date);
		String aData=df.format(dateTillto);
	
		
		String hqlQuery = "SELECT new com.elsagdatamat.tt.trackdbws.beans.VO.EsiPackVO(t,ePI)" +
		   				  "FROM Trace t, EsiPackPIFilter ePI " +
		   				  "WHERE t.traceId = ePI.tracesId " +
		   				  "AND t.whenRegistered > to_date('"+ daData+"', 'DD-MM-YYYY HH24:mi:ss') " +
		   				  "AND t.whenRegistered < to_date('"+aData+"', 'DD-MM-YYYY HH24:mi:ss') " +
		   				  "AND t.traceId > '" +esiPackId +"' ";
		if(size>0)
			hqlQuery+="AND rowNum<= '"+size+"' ";
		
		hqlQuery+= "ORDER BY t.whenRegistered, t.traceId";

		log.debug("getListOfNEsiPacks_PI. Fine");
		return traceDAO.getListOfEsiPackVO(hqlQuery);
	}
	
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<EsiPackVO> getListOfNEsiPacks_SDA(Date date,Long esiPackId,
										Date dateTillto,int size) {
		Log log = LogFactory.getLog(getClass());	
		log.debug("getListOfNEsiPacks_SDA. Inizio");
		
		SimpleDateFormat df=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String daData=df.format(date);
		String aData=df.format(dateTillto);
	
		
		String hqlQuery = "SELECT new com.elsagdatamat.tt.trackdbws.beans.VO.EsiPackVO(t,eSDA)" +
		   				  "FROM Trace t, EsiPackSDAFilter eSDA " +
		   				  "WHERE t.traceId = eSDA.tracesId " +
		   				  "AND t.whenRegistered > to_date('"+ daData+"', 'DD-MM-YYYY HH24:mi:ss') " +
		   				  "AND t.whenRegistered < to_date('"+aData+"', 'DD-MM-YYYY HH24:mi:ss') " +
		   				  "AND t.traceId > '" +esiPackId +"' ";
		if(size>0)
			hqlQuery+="AND rowNum<= '"+size+"' ";
		
		hqlQuery+= "ORDER BY t.whenRegistered, t.traceId";
		
		log.debug("getListOfNEsiPacks_SDA. Fine");
		return traceDAO.getListOfEsiPackVO(hqlQuery);
	}
	
}
