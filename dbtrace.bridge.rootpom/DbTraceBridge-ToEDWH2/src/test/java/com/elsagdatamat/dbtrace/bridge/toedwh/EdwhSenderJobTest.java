package com.elsagdatamat.dbtrace.bridge.toedwh;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elsagdatamat.dbtrace.bridge.wstojms.WsToJmsBridge;
import com.elsagdatamat.trackdb.toedwh.manager.ITraceManager;
import com.elsagdatamat.trackdb.toedwh.vo.EdwhTracesList;
import com.elsagdatamat.trackdb.toedwh.vo.TraceVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext.xml" })
public class EdwhSenderJobTest {

	@Resource(name = "dbTracceToEDWHBridge")
	WsToJmsBridge dbTracceToEDWHBridge;
	@Resource(name = "toEDWHBuilder")
	ToEdwhMessageBuilderInterface toEDWHBuilder;
	@Resource(name = "traceManager")
	ITraceManager traceManager;

	@Test
	public void test() {
		List<TraceVO> find = traceManager.find(-1L, 100000L, "PACCO");
		long traceCount = find.size();
		toEDWHBuilder.loadParams();

		long currentTimeMillis = System.currentTimeMillis();
		while (find.size() > 0) {
			toEDWHBuilder.create(new EdwhTracesList(find));
			long idTrace = find.get(find.size() - 1).getIdTrace();
			find = traceManager.find(idTrace, 100000L, "PACCO");
			traceCount += find.size();
		}
		currentTimeMillis = System.currentTimeMillis() - currentTimeMillis;
		System.out.print("Traccetotali " + traceCount + " in : " + currentTimeMillis);
		System.exit(0);
	}

}
