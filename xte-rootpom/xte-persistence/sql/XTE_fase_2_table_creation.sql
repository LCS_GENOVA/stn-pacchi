# drop sequence AS_SEQ;
CREATE SEQUENCE AS_SEQ NOCYCLE ORDER NOMAXVALUE MINVALUE 1 INCREMENT BY 1 START WITH 1;

#drop table ADDITIONAL_SERVICES CASCADE CONSTRAINTS;
CREATE TABLE ADDITIONAL_SERVICES
(ID NUMBER(10) NOT NULL
,PERIF CHAR(1 CHAR) NOT NULL
,CDG CHAR(1 CHAR) NOT NULL);

ALTER TABLE ADDITIONAL_SERVICES
ADD CONSTRAINT SA_PK PRIMARY KEY (ID) USING INDEX;

CREATE UNIQUE INDEX UQ_ADDITIONAL_SERVICES ON ADDITIONAL_SERVICES (PERIF);

#drop sequence AMSG_SEQ;
CREATE SEQUENCE AMSG_SEQ NOCYCLE ORDER NOMAXVALUE MINVALUE 1 INCREMENT BY 1 START WITH 1;

#drop table ASYNC_MESSAGES CASCADE CONSTRAINTS;

CREATE TABLE ASYNC_MESSAGES 
( ID NUMBER(10) NOT NULL,
  "FLOW_ID" VARCHAR2(16 byte) NOT NULL, 
  "SOURCE_OFFICE_ID" VARCHAR2(5 byte), 
	"DESTINATION_OFFICE_ID" VARCHAR2(5 byte) NOT NULL, 
	"OBJECT_ID" VARCHAR2(32 byte), 
  "ORIGINAL_MESSAGE" CLOB NOT NULL ENABLE,
  "PROCESSED_MESSAGE" CLOB,
  "RECEIVED_TIMESTAMP" DATE NOT NULL ENABLE,
  "SEND_TIMESTAMP" DATE,
  "LOCK_ID"   VARCHAR2(20 BYTE),
  "LOCK_TIMESTAMP" DATE,
  "ERR_DESCR" VARCHAR2(4000 BYTE));
  

ALTER TABLE ASYNC_MESSAGES ADD CONSTRAINT AMSG_PK PRIMARY KEY (ID) USING INDEX;

CREATE INDEX UQ_ASYNC_MESSAGES ON ASYNC_MESSAGES (FLOW_ID);

#drop table ASYNC_PROCESSED CASCADE CONSTRAINTS;

CREATE TABLE ASYNC_PROCESSED 
( ID NUMBER(10) NOT NULL,
  "FLOW_ID" VARCHAR2(16 byte) NOT NULL, 
  "SOURCE_OFFICE_ID" VARCHAR2(5 byte), 
	"DESTINATION_OFFICE_ID" VARCHAR2(5 byte) NOT NULL, 
	"OBJECT_ID" VARCHAR2(32 byte), 
  "ORIGINAL_MESSAGE" CLOB NOT NULL ENABLE,
  "PROCESSED_MESSAGE" CLOB,
  "RECEIVED_TIMESTAMP" DATE NOT NULL ENABLE,
  "SEND_TIMESTAMP" DATE,
  "LOCK_ID"   VARCHAR2(20 BYTE),
  "LOCK_TIMESTAMP" DATE,
  "ERR_DESCR" VARCHAR2(4000 BYTE));

ALTER TABLE ASYNC_PROCESSED ADD CONSTRAINT APRS_PK PRIMARY KEY (ID) USING INDEX;

CREATE INDEX UQ_ASYNC_PROCESSED ON ASYNC_PROCESSED (FLOW_ID);
 
#DROP SEQUENCE PROD_SEQ;
CREATE SEQUENCE PROD_SEQ NOCYCLE ORDER NOMAXVALUE MINVALUE 1 INCREMENT BY 1 START WITH 1;

#drop TABLE PRODUCTS CASCADE CONSTRAINTS;
CREATE TABLE PRODUCTS
(
  ID NUMBER(10) NOT NULL,
  PTYPE VARCHAR2(4 CHAR) NOT NULL,
  IS_A_PACK VARCHAR2(1) DEFAULT 'N' NOT NULL);
  
ALTER TABLE PRODUCTS ADD CONSTRAINT CK_PRODUCTS_IS_A_PACK CHECK ( IS_A_PACK IN ('N','S') );

ALTER TABLE PRODUCTS ADD CONSTRAINT PROD_PK PRIMARY KEY(ID) USING INDEX;

CREATE UNIQUE INDEX UQ_PRODUCTS ON PRODUCTS (PTYPE);

--------------------------------------------------------
--  DDL for Table OFFICES
--------------------------------------------------------

  CREATE TABLE "OFFICES" 
   (	"ID" NUMBER(10,0), 
	"OFFICE_ID" VARCHAR2(10 BYTE), 
	"OFF_REF_ID" VARCHAR2(10 BYTE), 
	"IP_HOST" VARCHAR2(30 BYTE), 
	"HOSTNAME" VARCHAR2(200 BYTE), 
	"FLAG_VALID" VARCHAR2(1 BYTE) DEFAULT 'V'
   );
--------------------------------------------------------
--  DDL for Table EVENTS
--------------------------------------------------------

  CREATE TABLE "EVENTS" 
   (	"ID" NUMBER(10,0), 
	"SERVICE" VARCHAR2(2 BYTE), 
	"CHANNEL" VARCHAR2(2 BYTE), 
	"EVENT_ID" VARCHAR2(3 BYTE), 
	"VERSION" VARCHAR2(20 BYTE), 
	"URL" VARCHAR2(300 BYTE)
   );

--------------------------------------------------------
--  DDL for Table SERVICES
--------------------------------------------------------

  CREATE TABLE "SERVICES" 
   (	"ID" NUMBER(10,0), 
	"SERVICE_ID" VARCHAR2(2 BYTE), 
	"IP_HOST" VARCHAR2(30 BYTE), 
	"HOSTNAME" VARCHAR2(200 BYTE)
   );

--------------------------------------------------------
--  DDL for Index OFF_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "OFF_PK" ON "OFFICES" ("ID", "OFFICE_ID");
--------------------------------------------------------
--  DDL for Index UQ_OFFICES
--------------------------------------------------------

  CREATE UNIQUE INDEX "UQ_OFFICES" ON "OFFICES" ("OFFICE_ID");
--------------------------------------------------------
--  DDL for Index EV_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "EV_PK" ON "EVENTS" ("ID");
--------------------------------------------------------
--  DDL for Index SER_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SER_PK" ON "SERVICES" ("ID");
--------------------------------------------------------
--  Constraints for Table OFFICES
--------------------------------------------------------

  ALTER TABLE "OFFICES" ADD CONSTRAINT "OFF_PK" PRIMARY KEY ("ID", "OFFICE_ID") ENABLE;
 
  ALTER TABLE "OFFICES" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "OFFICES" MODIFY ("OFFICE_ID" NOT NULL ENABLE);
 
  ALTER TABLE "OFFICES" MODIFY ("OFF_REF_ID" NOT NULL ENABLE);
 
  ALTER TABLE "OFFICES" MODIFY ("FLAG_VALID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table EVENTS
--------------------------------------------------------

  ALTER TABLE "EVENTS" ADD CONSTRAINT "EV_PK" PRIMARY KEY ("ID")  ENABLE;
 
  ALTER TABLE "EVENTS" MODIFY ("ID" NOT NULL ENABLE);

--------------------------------------------------------
--  Constraints for Table SERVICES
--------------------------------------------------------

  ALTER TABLE "SERVICES" ADD CONSTRAINT "SER_PK" PRIMARY KEY ("ID")  ENABLE;
 
  ALTER TABLE "SERVICES" MODIFY ("ID" NOT NULL ENABLE);
 

 create or replace
TRIGGER "TR_ASYNC_MESSAGES_DELETE" 
BEFORE DELETE ON ASYNC_MESSAGES 
REFERENCING OLD AS OLD NEW AS NEW 
FOR EACH ROW 
BEGIN
  INSERT INTO ASYNC_PROCESSED 
  (ID,FLOW_ID,SOURCE_OFFICE_ID,DESTINATION_OFFICE_ID,OBJECT_ID,ORIGINAL_MESSAGE,PROCESSED_MESSAGE,RECEIVED_TIMESTAMP,SEND_TIMESTAMP,LOCK_ID,LOCK_TIMESTAMP,ERR_DESCR)
  VALUES 
  (:OLD.ID,:OLD.FLOW_ID,:OLD.SOURCE_OFFICE_ID,:OLD.DESTINATION_OFFICE_ID,:OLD.OBJECT_ID,:OLD.ORIGINAL_MESSAGE,:OLD.PROCESSED_MESSAGE,:OLD.RECEIVED_TIMESTAMP,:OLD.SEND_TIMESTAMP,:OLD.LOCK_ID,:OLD.LOCK_TIMESTAMP,:OLD.ERR_DESCR);
END;
/
ALTER TRIGGER "TR_ASYNC_MESSAGES_DELETE" ENABLE;

create or replace
PROCEDURE PROCEDURA_CANCELLAZIONE IS 
  now TIMESTAMP;
  freq NUMBER;
BEGIN
  SELECT TO_NUMBER(prop_value)
    INTO freq
    FROM XTE_PROPERTIES
    WHERE prop_name = 'CLEANER_FREQ';
    
    DELETE FROM ASYNC_PROCESSED WHERE send_timestamp < current_timestamp-freq/(24*60);
END PROCEDURA_CANCELLAZIONE;
/
--------------------------------------------------------
--  DDL for Trigger TR_PROCESSED_INSERT
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "TR_ASYNC_PROCESSED_INSERT" 
BEFORE INSERT ON ASYNC_PROCESSED 
REFERENCING OLD AS OLD NEW AS NEW 
FOR EACH ROW 
DECLARE
  rowCount number;
  olderId number;
BEGIN
  SELECT count(*) INTO rowCount FROM ASYNC_PROCESSED;
  IF (rowCount > 150) then
    SELECT min(ID) INTO olderId FROM ASYNC_PROCESSED;
    delete from ASYNC_PROCESSED WHERE ID=olderID;
  end if;
END;
/
ALTER TRIGGER "TR_ASYNC_PROCESSED_INSERT" ENABLE;
 
 
 BEGIN
    SYS.DBMS_SCHEDULER.CREATE_JOB (
            job_name => '"CANCELLAZIONE_JOB"',
            job_type => 'PLSQL_BLOCK',
            job_action => 'PROCEDURA_CANCELLAZIONE;',
            number_of_arguments => 0,
            job_class => 'DEFAULT_JOB_CLASS',
            enabled => true,
            auto_drop => true,
            comments => NULL,
            credential_name => NULL,
            destination_name => NULL);
END;
/





Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'O','I');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'C','R');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'A','S');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'V','S');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'I','C');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'M','U');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'T','B');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'D','J');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'H','H');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'L','L');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'K','O');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'R','R');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'S','S');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'X','X');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'P','P');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'Y','P');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'E','E');
Insert into "ADDITIONAL_SERVICES" ("ID","PERIF","CDG") 
 values (as_seq.nextval,'J','D');
 
 
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'ARC','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'CB4','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'CI1','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'CJ1','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'CJ3','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'CM2','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'COE','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'COI','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'COR','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'EMS','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'EPG','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'HB4','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'PCE','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'PI1','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'PJ1','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'PJ3','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'PM2','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'POE','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'POI','S');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'POR','S');


Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'N','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'REL','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'CAN','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'ABP','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'ACV','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'AEE','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'AFE','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'AG','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'ANL','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'APT','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'AR','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'ARI','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'AVL','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'AVO','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'AXX','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'CD','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'EE','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'MN','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'MR','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'PC','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'PR','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'R','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'RC','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'RD','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'RP','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'RV','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'S','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'SP','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'TLG','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'TLB','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'B','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'M','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'CEE','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'Q','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'MNE','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'MRE','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'K','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'P','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'E','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'D2D','N');
Insert into "PRODUCTS" ("ID","PTYPE","IS_A_PACK") 
 values (prod_seq.nextval,'L','N');
 
 
REM INSERTING into OFFICES
Insert into OFFICES (ID,OFFICE_ID,OFF_REF_ID,IP_HOST,HOSTNAME,FLAG_VALID) values (987,'21314','21314','172.24.79.150','172.24.79.150','V');
Insert into OFFICES (ID,OFFICE_ID,OFF_REF_ID,IP_HOST,HOSTNAME,FLAG_VALID) values (4,'68999','68999','172.31.29.83','172.31.29.83','V');
Insert into OFFICES (ID,OFFICE_ID,OFF_REF_ID,IP_HOST,HOSTNAME,FLAG_VALID) values (2,'14100','14100','172.31.30.202','172.31.30.202','V');
Insert into OFFICES (ID,OFFICE_ID,OFF_REF_ID,IP_HOST,HOSTNAME,FLAG_VALID) values (3,'13999','13999','172.31.29.215','172.31.29.215','V');
Insert into OFFICES (ID,OFFICE_ID,OFF_REF_ID,IP_HOST,HOSTNAME,FLAG_VALID) values (1293,'51204','51204','172.24.16.126Â Â  ','172.24.16.126Â Â  ','V');
Insert into OFFICES (ID,OFFICE_ID,OFF_REF_ID,IP_HOST,HOSTNAME,FLAG_VALID) values (1344,'55697','55697','172.25.42.75','172.25.42.75','V');
Insert into OFFICES (ID,OFFICE_ID,OFF_REF_ID,IP_HOST,HOSTNAME,FLAG_VALID) values (1545,'82700','82700','172.26.205.242','172.26.205.242','V');
Insert into OFFICES (ID,OFFICE_ID,OFF_REF_ID,IP_HOST,HOSTNAME,FLAG_VALID) values (1546,'82701','82701','172.26.204.195','172.26.204.195','V');
Insert into OFFICES (ID,OFFICE_ID,OFF_REF_ID,IP_HOST,HOSTNAME,FLAG_VALID) values (1,'25999','25999','172.31.30.202','172.31.30.202','V');
Insert into OFFICES (ID,OFFICE_ID,OFF_REF_ID,IP_HOST,HOSTNAME,FLAG_VALID) values (5,'28428','28428','172.31.30.201','172.31.30.201
','V');
REM INSERTING into EVENTS
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (17,'PR','TP','SP','1.1','/STAREXT/TerminalePortalettere/ScaricoPezzi.asp');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (2,'DP','TP','NM','1.1','/postamassiva/PltRequestManagerWS/TrackProductsServlet');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (3,'DP','TP','PR','1.1','/postamassiva/PltRequestManagerWS/TrackProductsServlet');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (4,'DP','TP','AM','1.1','/postamassiva/PltRequestManagerWS/TrackProductsServlet');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (5,'DP','TP','LM','1.1','/postamassiva/PltRequestManagerWS/TrackProductsServlet');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (6,'DP','TP','SM','1.1','/postamassiva/PltRequestManagerWS/TrackProductsServlet');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (7,'PR','TP','LM','1.1','/STAR/RilasciaMazzetto_TP/RilasciaMazzetto.asp');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (8,'PR','TP','LO','1.1','/STAR/LasciaOggetto_TP/LasciaOggetto.asp');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (9,'PR','TP','CM','1.1','/STAR/ConsegnaMazzetto_TP/ConsegnaMazzetto.asp');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (10,'PR','TP','AC','1.1','/STAR/AcquisizioneConfigurazione_TP/AcquisizioneConfigurazione.asp');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (11,'PR','TP','AM','1.1','/STAR/AcquisizioneMazzetto_TP/AcquisizioneMazzetto.asp');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (12,'PR','TP','AM','1.0','/STAR/AcquisizioneMazzetto_TP/AcquisizioneMazzetto.asp');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (13,'PR','TP','NM','1.1','/STAR/NotificaMazzetto_TP/NotificaMazzetto.asp');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (14,'PR','TP','DU','1.1','/STAR/DatiUfficio_TP/DatiUfficio.asp');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (15,'PR','TP','DM','1.1','/STAR/DatiMazzetti/DatiMazzetti.asp');
Insert into EVENTS (ID,SERVICE,CHANNEL,EVENT_ID,VERSION,URL) values (16,'PR','AO','AC','1.0','/STAR/AcquisizioneConfigurazione_AO/AcquisizioneConfigurazione.asp');
REM INSERTING into SERVICES
Insert into SERVICES (ID,SERVICE_ID,IP_HOST,HOSTNAME) values (1,'DP','172.31.30.202','172.31.30.202');

 