package com.selexelsag.xte.task.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.system.ServiceMBeanSupport;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class TskService extends ServiceMBeanSupport implements TskServiceMBean {

	private static final Log log = LogFactory.getLog(TskService.class);

	private boolean created;
	private boolean started;
	private String springConfigPath;
	private long errorCount = 0;
	private Date lastStartDate = new Date();

	private ClassPathXmlApplicationContext springContext;

	private void serviceStartInits() {
		lastStartDate = new Date();
	}

	private void createSpringContext() throws Exception {
		log.info("Start the Tsk Spring context. . .");
		springContext = new ClassPathXmlApplicationContext(springConfigPath);
		springContext.registerShutdownHook();
	}

	@Override
	public void create() throws Exception {
		log.info("Create service invoked. . .");
		if (!created) {
			try {

				created = true;
			} catch (Exception ex) {
				errorCount++;
				created = false;
				log.error("Unable to start Spring Context", ex);
			}
		}
		log.info("Service created.");
	}

	@Override
	public void destroy() {
		log.info("Destroy service invoked; stopping Spring context. . .");
		if (created) {
			try {
				stop();
				if (springContext!=null && springContext.isActive()) {
					springContext.close();
					springContext.destroy();
					springContext = null;
				}
				created = false;
			} catch (Throwable ex) {
				log.error(" Failed to destroy service. ", ex);
			}
		}
		log.info("Service destroyed.");
	}

	@Override
	public void start() throws Exception {
		if (!started) {
			if (created) // added because of service dependencies
				createSpringContext();
			log.info("Starting Spring ...");
			try {
				if (!springContext.isActive()) {
					springContext.refresh();
					springContext.start();
				}
				started = true;
				serviceStartInits();
				log.info("Spring context started. OK");
			} catch (Throwable ex) {
				errorCount++;
				started = false;
				log.error("Failed to start Spring context ", ex);
			}
			return;
		}
		log.info("Spring already started");
	}

	@Override
	public void stop() {
		if (started) {
			log.info("Stopping Spring ...");
			try {
				springContext.close();
				started = false;
				log.info("Spring context stopped. OK");
			} catch (Throwable ex) {
				errorCount++;
				started = false;
				log.error("Failed to stop Spring context ", ex);
			}
			return;
		}
		log.info("Spring already stopped");
	}

	@Override
	public void setSpringConfigPath(String path) {
		springConfigPath = path;
	}

	@Override
	public String getSpringConfigPath() {
		return springConfigPath;
	}

	@Override
	public boolean isSpringLoaded() {
		return (springContext != null) ? springContext.isActive() : false;
	}

	@Override
	public boolean isStarted() {
		return started;
	}

	@Override
	public long getErrorCount() {
		return errorCount;
	}

	@Override
	public String getLastStartDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss");
		return sdf.format(lastStartDate);
	}
}
