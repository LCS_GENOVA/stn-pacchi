/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.GZIPOutputStream;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

/**
 * @author arodriguez
 *
 */
public class D1PCompressedMessageCreator extends OMPAbstractMessageCreator implements Serializable {

	private String destinationOfficeCode = "OMP";
	private String containerCode = null;
	
	/**
	 * @param textMessage
	 */
	public D1PCompressedMessageCreator(String textMessage) {
		super(textMessage);
	}

	/**
	 * @param destinationOfficeCode
	 * @param containerCode
	 */
	public D1PCompressedMessageCreator(String textMessage, String destinationOfficeCode, String containerCode) {
		this(textMessage);
		this.destinationOfficeCode = destinationOfficeCode;
		this.containerCode = containerCode;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#getSentDate()
	 */
	@Override
	protected String getSentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		return sdf.format(new Date());
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#getSourceOfficeCode()
	 */
	@Override
	protected String getSourceOfficeCode() {
		return DESTINATION_DBTRACE;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#getDestinationOfficeCode()
	 */
	@Override
	protected String getDestinationOfficeCode() {
		return destinationOfficeCode;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#getMessageType()
	 */
	@Override
	protected String getMessageType() {
		return "D1P";
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#isTestMessage()
	 */
	@Override
	protected boolean isTestMessage() {
		return false;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#getContainerCode()
	 */
	@Override
	protected String getContainerCode() {
		return containerCode;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#isCompressed()
	 */
	@Override
	protected boolean isCompressed() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.message.OMPAbstractMessageCreator#createMessage(javax.jms.Session)
	 */
	@Override
	public Message createMessage(Session jmsSession) throws JMSException {
		BytesMessage jmsBytesMessage = jmsSession.createBytesMessage();
		buildMessageHeader(jmsBytesMessage);
		try {
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			GZIPOutputStream gzipOut = new GZIPOutputStream(bout);
			gzipOut.write(textMessage.getBytes());
			gzipOut.finish();
			jmsBytesMessage.writeBytes(bout.toByteArray());
			gzipOut.close();
		} catch (IOException e) {
			throw new JMSException(e.getMessage());
		}
		return jmsBytesMessage;
	}
	
	private void buildMessageHeader(BytesMessage message) throws JMSException {
		message.setStringProperty(SENT_DATE_KEY, getSentDate());
		message.setStringProperty(MESSAGE_TYPE_KEY, getMessageType());
		if (getSourceOfficeCode() != null)
			message.setStringProperty(SOURCE_OFFICE_CODE_KEY, getSourceOfficeCode());
		if (getDestinationOfficeCode() != null)
			message.setStringProperty(DESTINATION_OFFICE_CODE_KEY, getDestinationOfficeCode());
		if (getContainerCode() != null)
			message.setStringProperty(CONTAINER_CODE_KEY, getContainerCode());
		if (isTestMessage())
			message.setBooleanProperty(TEST_MESSAGE_KEY, isTestMessage());
		if (isCompressed())
			message.setBooleanProperty(COMPRESSED_KEY, isCompressed());
	}	

}
