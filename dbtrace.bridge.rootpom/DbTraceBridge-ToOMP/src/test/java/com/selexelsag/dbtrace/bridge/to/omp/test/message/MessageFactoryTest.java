package com.selexelsag.dbtrace.bridge.to.omp.test.message;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;

import junit.framework.Assert;

import org.junit.Test;

import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1.DHDR;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1.MOBJ;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1.MOBJ.ACC;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.HDR;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory;
import com.selexelsag.dbtrace.bridge.to.omp.message.trans.Transformations;

public class MessageFactoryTest {

	@Test
    public void marshalledOMPMessageToXmlAndUnmarshalledAreIdentical() {
		MSG ompMessage;
		Transformations transf = new Transformations();
		try {
			ompMessage = createOMPJaxbMessage();
 
	        String xmlDocument = transf.marshallMessageToXml(ompMessage, null);
	        Assert.assertNotNull(xmlDocument);
	        Assert.assertTrue(xmlDocument.contains("SDA01"));
	        
	        System.out.println(xmlDocument);
	 
	        MSG unmarshalledMessage = transf.unmarshallXmlToMessage(xmlDocument, null);
	        
	        HDR header = (HDR)ompMessage.getHDROrD1().get(0);
	        D1 d1 = (D1)ompMessage.getHDROrD1().get(1);

	        HDR unmHeader = (HDR)unmarshalledMessage.getHDROrD1().get(0);
	        D1 unmD1 = (D1)unmarshalledMessage.getHDROrD1().get(1);

	        Assert.assertEquals(header.getOFCID(), unmHeader.getOFCID());
	        Assert.assertEquals(header.getSWREL(), unmHeader.getSWREL());
	        Assert.assertEquals(d1.getDHDR().get(0).getDID(), unmD1.getDHDR().get(0).getDID());
	        Assert.assertEquals(d1.getDHDR().get(0).getDT(), unmD1.getDHDR().get(0).getDT());
	        Assert.assertEquals(d1.getDHDR().get(0).getOFCOTH(), unmD1.getDHDR().get(0).getOFCOTH());
	        Assert.assertEquals(d1.getDHDR().get(0).getOFNAME(), unmD1.getDHDR().get(0).getOFNAME());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new AssertionError(ex);
		}
    }

	@Test
    public void marshalledOMPMessageWithNullsDontEncodeNullTags() {
		MSG ompMessage;
		Transformations transf = new Transformations();
		try {
			ompMessage = createOMPJaxbMessage();
 
	        String xmlDocument = transf.marshallMessageToXml(ompMessage, null);
	        Assert.assertNotNull(xmlDocument);
	        Assert.assertTrue(xmlDocument.contains("SDA01"));
	        
	        System.out.println(xmlDocument);
	 
	        MSG unmarshalledMessage = transf.unmarshallXmlToMessage(xmlDocument, null);
	        
	        HDR header = (HDR)ompMessage.getHDROrD1().get(0);
	        D1 d1 = (D1)ompMessage.getHDROrD1().get(1);

	        HDR unmHeader = (HDR)unmarshalledMessage.getHDROrD1().get(0);
	        D1 unmD1 = (D1)unmarshalledMessage.getHDROrD1().get(1);

	        MOBJ unmMOBJ = (MOBJ)d1.getMOBJ().get(0);
	        
	        Assert.assertEquals(header.getOFCID(), unmHeader.getOFCID());
	        Assert.assertEquals(header.getSWREL(), unmHeader.getSWREL());
	        Assert.assertEquals(d1.getDHDR().get(0).getDID(), unmD1.getDHDR().get(0).getDID());
	        Assert.assertEquals(d1.getDHDR().get(0).getDT(), unmD1.getDHDR().get(0).getDT());
	        Assert.assertEquals(d1.getDHDR().get(0).getOFCOTH(), unmD1.getDHDR().get(0).getOFCOTH());
	        Assert.assertEquals(d1.getDHDR().get(0).getOFNAME(), unmD1.getDHDR().get(0).getOFNAME());
	        
	        Assert.assertNull(unmMOBJ.getACC().get(0).getPERC());
	        Assert.assertFalse(xmlDocument.contains("<PERC>"));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new AssertionError(ex);
		}
    }

    public MSG createOMPJaxbMessage() throws DatatypeConfigurationException {
    	ObjectFactory objFactory = new ObjectFactory();
		MSG ompMessage = objFactory.createMSG();
 
		HDR msgHeader = objFactory.createMSGHDR();
		msgHeader.setOFCID("FRZ");
		msgHeader.setSWREL("SDA01");

		DHDR dhdrHeader = objFactory.createMSGD1DHDR();
		dhdrHeader.setDID("012345678901234");
		dhdrHeader.setOFCOTH("28428");
		
		ACC acc = objFactory.createMSGD1MOBJACC();
		acc.setADT("ADT");
		acc.setAOFC("AOFC");
		acc.setPERC(null);
		
		MOBJ mobj = objFactory.createMSGD1MOBJ();
		mobj.getACC().add(acc);
		
		GregorianCalendar now = (GregorianCalendar)GregorianCalendar.getInstance();
		
		dhdrHeader.setDT(Transformations.XMLDateFormat.format(now.getTime()));
		dhdrHeader.setOFNAME("Destinatario");
		
		D1 d1Container = objFactory.createMSGD1();
		d1Container.getDHDR().add(dhdrHeader);
		d1Container.getMOBJ().add(mobj);
		
		ompMessage.getHDROrD1().add(msgHeader);
		ompMessage.getHDROrD1().add(d1Container);
 
        return ompMessage;
    }

}
