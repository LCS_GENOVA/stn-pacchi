package com.selexelsag.tt.dbtrace.bridge.sender.update.core;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.selexelsag.tt.cruscotto.pacchi.bean.PkConParametro;
import com.selexelsag.tt.cruscotto.pacchi.bean.PkConProdotto;
import com.selexelsag.tt.cruscotto.pacchi.bl.IPkConParametroBL;
import com.selexelsag.tt.cruscotto.pacchi.bl.IPkConProdottoBL;

public class Configuration {
	
	private IPkConParametroBL 		pkConParametroBL;
	private IPkConProdottoBL		pkConProdottoBL;
	// variabili utilizzate per i seguenti motivi:
	// 1. ogni quanto aggiornare la hastable dei prodotti
	// 2. ogni quanto controllare se inviare oppure no le tracce alla coda per il cruscotto
	private boolean flagSender = false; // boolean che indica se inviare (true) i dati alla coda oppure no (false)
	private boolean flagAnagraphicSender = false; // boolean che indica se inviare (true) i dati alla coda della Anagrafica oppure no (false)
	private String parameterName;
	private GregorianCalendar whenReUpdateProducts = new GregorianCalendar(); // data che rappresenta quando ricariare la hash (dataDiUltimoAggiornamento + valore da Db)
	private GregorianCalendar whenReUpdateFlagSender = new GregorianCalendar(); // data che rappresenta quando ricariare il flag di invio dei dati al cruscotto (dataDiUltimoAggiornamento + valore da Db)	
	private HashMap<ProductKey, Boolean> productsMap; // hashmap che contiene per ogni singolo prodotto (chiave) un valore di tipo boolean: 'true' se quel prodotto e' di una qualche utilit� 
												 // al cruscotto e deve essere inviato, 'false' altrimenti
	
	// valori di default se i parametri di configurazione sul DB sono nulli
	private int updateFlagSeder_default = 120; // espresso in minuti (2h)
	private int updateProducts_default = 180; // espresso in minuti 

	private Log log = LogFactory.getLog(this.getClass());


	public void loadConfiguration(){
		this.reloadFlagSenderConfiguration();
		this.reloadProductsConfiguration();
	}
	
	
	/**
	 * Metodo che restituisce il valore di flagSender e se necessario (data ultimo aggiornamento < di data attuale) legge da DB la nuova configurazione
	 * 
	 * @return boolean - true se il valore di FlagSender � true
	 * 					 false altrimenti
	 */
	public boolean isFlagSender() {
		if(whenReUpdateFlagSender==null || whenReUpdateFlagSender.getTime().before(new Date())){
			log.info("Upadate del flag per inviare i dati al cruscotto pacchi");
			this.reloadFlagSenderConfiguration();
		}
		return this.flagSender;
	}
	
	/**
	 * Metodo che restituisce il valore di flagAnagraphicSender e se necessario (data ultimo aggiornamento < di data attuale) legge da DB la nuova configurazione
	 * 
	 * @return boolean - true se il valore di FlagAnagraphicSender � true
	 * 					 false altrimenti
	 */
	public boolean isFlagAnagraphicSender() {
		if(whenReUpdateFlagSender==null || whenReUpdateFlagSender.getTime().before(new Date())){
			log.info("Upadate del flag per inviare i dati all'anagrafica");
			this.reloadFlagSenderConfiguration();
		}
		return this.flagAnagraphicSender;
	}
	
	/**

	/**
	 *  Meotodo per aggiornare la data e il flag che indica se inviare  meno le tracce del DbTraceBridge al cruscotto pacchi
	 * 
	 * @return boolean - se i due 
	 */
	private boolean reloadFlagSenderConfiguration(){			
		String valore;
		int updateFlagSeder = updateFlagSeder_default; // intervallo in minuti ogni quanto deve essere aggiornato il flagSender - letto dal DB o valore di default
		
		if((valore=getValoreByNome("Q_UPDATE_FLAG_SENDER"))!= null){
			try{
				updateFlagSeder = Integer.parseInt(valore);
			}catch (NumberFormatException e) {
				e.printStackTrace();
				log.debug("Errore durante la lettura parametro Q_UPDATE_FLAG_SENDER. Il valore corrispondenre non e' un numero valido ["+valore+"]. Utilizzato valore di default ["+updateFlagSeder_default+"]");
				updateFlagSeder = updateFlagSeder_default;
			}
		}
		this.whenReUpdateFlagSender.add(Calendar.MINUTE, updateFlagSeder);
		
		if((valore=getValoreByNome("Q_FLAG_SENDER_VALUE"))!= null)
			this.flagSender=("true".equals(valore)?true:false); // flagSender e' true solo se sul DB il valore � 'true'
		if((valore=getValoreByNome("Q_ANAGRAPHIC_FLAG_SENDER_VALUE"))!= null)
			this.flagAnagraphicSender=("true".equals(valore)?true:false); // flagSender e' true solo se sul DB il valore � 'true'
		return true;
	}
	
	/**
	 * Questo metodo restituisce la configurazione di quel prodotto se esiste sul DB,
	 * null altrimenti
	 * 
	 * @param pk - chiave dell'hashmap (prodotto, flusso, tipo_traccia)
	 * @return boolean - true se la traccia � utile al cruscotto, false se la traccia non � utile al cruscotto 
	 * 					 null se la configurazione non � presente sul DB
	 */
	public boolean getProductConfig(ProductKey key){
		String prodotto = key.getProdotto(); // Carico queste stringhe perch� se non trovo nessun prodotto scrivo un log ad errore con i valori non trovati.
		String canale = key.getCanale(); // non posso usare direttamente l'oggetto key perch� viene sovrascritto durante la fase di recupero informazioni dal carattere '*'
		String tipoTraccia = key.getTraccia();
		
		if(productsMap==null || whenReUpdateProducts==null || whenReUpdateProducts.getTime().before(new Date()))
			this.reloadProductsConfiguration();
		
		Boolean isAttivata = getScalareProductConfig(key);
		if(isAttivata==null){
			Boolean pp = caricaNuovaConfigurazioneProdotti(key);
			if(pp==null){
				log.error("non trovata configurazione per: prodotto ["+prodotto+"] - canale ["+canale+"] - tipo traccia ["+tipoTraccia+"]");
				return false;
			}
			else{
				isAttivata = pp;
				productsMap.put(key, pp);
			}
		}
		return isAttivata.booleanValue();
	}

	
	/**
	 * Meotodo per aggiornare la data e l'hashmap che contiene la configurazione dei prodotti
	 * per il cruscotto pacchi
	 * 
	 */
	private boolean reloadProductsConfiguration(){		
		String valore;
		int updateHash = updateProducts_default; // intervallo in minuti ogni quanto deve essere aggiornato la configurazioen dei prodotti (hashmap) - letto dal DB o valore di default
		
		if((valore=getValoreByNome("Q_UPDATE_FLAG_SENDER"))!= null){
			try{
				updateHash = Integer.parseInt(valore);
			}catch (NumberFormatException e) {
				e.printStackTrace();
				log.debug("Errore durante la lettura parametro Q_UPDATE_FLAG_SENDER. Il valore corrispondenre non e' un numero valido ["+valore+"]. Utilizzato valroe di default [" +updateProducts_default +"]");
				updateHash = updateProducts_default;
			}
		}
		this.whenReUpdateProducts.add(Calendar.MINUTE, updateHash);
		
		productsMap = fromListToMap(pkConProdottoBL.getLista());
		
		return true;
	}
	
	private HashMap<ProductKey, Boolean> fromListToMap(List<PkConProdotto> lista) {
		HashMap<ProductKey, Boolean> conProdotti = new HashMap<ProductKey, Boolean>(0);

		for (PkConProdotto pkConProdotto : lista) {
			conProdotti.put(new ProductKey(pkConProdotto.getProdotto(), pkConProdotto.getCanale(), pkConProdotto.getTipoTraccia()),pkConProdotto.isFlag());
		}
		
		return conProdotti;
	}

	/** 
	 * Restituisce il valore di un parametro dato il suo nome (chiave)
	 * 
	 * @param nome - nome del parametro
	 * @return valore - valore del parametro
	 */
	private String getValoreByNome(String pNome){
		PkConParametro pkConParametro = pkConParametroBL.getElemento(pNome); // Valore in minuti
		if(pkConParametro!=null) return pkConParametro.getValore();
		return null;
	}

	/**
	 * Controllo se esiste per key = Prodotto - Canale - TipoTraccia
	 * se false, controllo per Prodotto - Canale - *
	 * se false, controllo per Prodotto - * - *
	 * se false return null 
	 * 
	 * @param key
	 * @return Boolean 
	 */
	private Boolean getScalareProductConfig(ProductKey key){
		if(productsMap.containsKey(key))
			return  productsMap.get(key);
		
		key.setTraccia("*");
		if(productsMap.containsKey(key))
			return  productsMap.get(key);
		
		// Ringraziamenti a Matteo Tabbone (compagno d banco e di mille avventure) 
		key.setCanale("*");
		if(productsMap.containsKey(key))
			return  productsMap.get(key);

		return null;
	}
	
	/**
	 * Controllo se esiste per key = Prodotto - Canale - TipoTraccia
	 * se false, controllo per Prodotto - Canale - *
	 * se false, controllo per Prodotto - * - *
	 * se false return null 
	 * 
	 * @param key
	 * @return Boolean 
	 */
	private Boolean caricaNuovaConfigurazioneProdotti(ProductKey key){

		Boolean pp = pkConProdottoBL.getElemento(key.getCanale(), key.getProdotto(), key.getTraccia());
		if(pp!=null){
			productsMap.put(key, pp);
			log.info("Trovata configurazione ["+pp+"] per prodotto ["+key.getProdotto()+"] canale ["+key.getCanale()+"] tipo traccia ["+key.getTraccia()+"]");
			return  pp;
		}
		
		key.setTraccia("*");
		pp = pkConProdottoBL.getElemento(key.getCanale(), key.getProdotto(), key.getTraccia());
		if(pp!=null){
			productsMap.put(key, pp);
			log.info("Trovata configurazione ["+pp+"] per prodotto ["+key.getProdotto()+"] canale ["+key.getCanale()+"] tipo traccia ["+key.getTraccia()+"]");
			return  pp;
		}
		
		key.setTraccia("*");
		pp = pkConProdottoBL.getElemento(key.getCanale(), key.getProdotto(), key.getTraccia());
		if(pp!=null){
			productsMap.put(key, pp);
			log.info("Trovata configurazione ["+pp+"] per prodotto ["+key.getProdotto()+"] canale ["+key.getCanale()+"] tipo traccia ["+key.getTraccia()+"]");
			return  pp;
		}
		return null;
	}

	/*
	 * Spring injection
	 */
	public IPkConParametroBL getPkConParametroBL() {
		return pkConParametroBL;
	}

	public void setPkConParametroBL(IPkConParametroBL pkConParametroBL) {
		this.pkConParametroBL = pkConParametroBL;
	}

	public IPkConProdottoBL getPkConProdottoBL() {
		return pkConProdottoBL;
	}

	public void setPkConProdottoBL(IPkConProdottoBL pkConProdottoBL) {
		this.pkConProdottoBL = pkConProdottoBL;
	}


	public String getParameterName() {
		return parameterName;
	}


	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public void setFlagAnagraphicSender(boolean flagAnagraphicSender) {
		this.flagAnagraphicSender = flagAnagraphicSender;
	}

}
