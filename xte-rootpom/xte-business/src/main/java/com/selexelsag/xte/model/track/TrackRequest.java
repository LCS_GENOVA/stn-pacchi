package com.selexelsag.xte.model.track;

import java.io.Serializable;

/**
 * Classe rappresentante la richiesta di tracciatura di un evento.
 * @author Frosi
 *
 */
public class TrackRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6752702419788294859L;
	
	private StringBuilder service;
	private StringBuilder channel;
	private StringBuilder destinationOfficeId;
	private StringBuilder sourceOfficeId;
	private StringBuilder objectId;
	private StringBuilder eventName;
	private StringBuilder version;
	private StringBuilder trackData;
	private StringBuilder requestId;
	public StringBuilder getService() {
		return service;
	}
	public void setService(StringBuilder service) {
		this.service = service;
	}
	public StringBuilder getChannel() {
		return channel;
	}
	public void setChannel(StringBuilder channel) {
		this.channel = channel;
	}
	public StringBuilder getDestinationOfficeId() {
		return destinationOfficeId;
	}
	public void setDestinationOfficeId(StringBuilder destinationOfficeId) {
		this.destinationOfficeId = destinationOfficeId;
	}
	public StringBuilder getSourceOfficeId() {
		return sourceOfficeId;
	}
	public void setSourceOfficeId(StringBuilder sourceOfficeId) {
		this.sourceOfficeId = sourceOfficeId;
	}
	public StringBuilder getObjectId() {
		return objectId;
	}
	public void setObjectId(StringBuilder objectId) {
		this.objectId = objectId;
	}
	public StringBuilder getEventName() {
		return eventName;
	}
	public void setEventName(StringBuilder eventName) {
		this.eventName = eventName;
	}
	public StringBuilder getVersion() {
		return version;
	}
	public void setVersion(StringBuilder version) {
		this.version = version;
	}
	public StringBuilder getTrackData() {
		return trackData;
	}
	public void setTrackData(StringBuilder trackData) {
		this.trackData = trackData;
	}
	public StringBuilder getRequestId() {
		return requestId;
	}
	public void setRequestId(StringBuilder requestId) {
		this.requestId = requestId;
	}
	@Override
	public String toString() {
		return String
				.format("TrackRequest [service=%s, channel=%s, channelId=%s, eventName=%s, version=%s, trackData=%s, requestId=%s]",
						service, channel, destinationOfficeId, eventName, version,
						trackData, requestId);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((channel == null) ? 0 : channel.hashCode());
		result = prime * result
				+ ((destinationOfficeId == null) ? 0 : destinationOfficeId.hashCode());
		result = prime * result
				+ ((eventName == null) ? 0 : eventName.hashCode());
		result = prime * result
				+ ((requestId == null) ? 0 : requestId.hashCode());
		result = prime * result + ((service == null) ? 0 : service.hashCode());
		result = prime * result
				+ ((trackData == null) ? 0 : trackData.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrackRequest other = (TrackRequest) obj;
		if (channel == null) {
			if (other.channel != null)
				return false;
		} else if (!channel.equals(other.channel))
			return false;
		if (destinationOfficeId == null) {
			if (other.destinationOfficeId != null)
				return false;
		} else if (!destinationOfficeId.equals(other.destinationOfficeId))
			return false;
		if (eventName == null) {
			if (other.eventName != null)
				return false;
		} else if (!eventName.equals(other.eventName))
			return false;
		if (requestId == null) {
			if (other.requestId != null)
				return false;
		} else if (!requestId.equals(other.requestId))
			return false;
		if (service == null) {
			if (other.service != null)
				return false;
		} else if (!service.equals(other.service))
			return false;
		if (trackData == null) {
			if (other.trackData != null)
				return false;
		} else if (!trackData.equals(other.trackData))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}
	
	
	

}
