package com.elsagdatamat.tt.trackdbws.daoimpl;

import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.tt.trackdbws.beans.EnvironmentParameter;
import com.elsagdatamat.tt.trackdbws.dao.IEnvironmentParameterDAO;

public class EnvironmentParameterDAO extends SpringHibernateJpaDAOBase<EnvironmentParameter, String> implements IEnvironmentParameterDAO {

}
