package com.elsagdatamat.dbtrace.bridge.engine.bl;

import com.elsagdatamat.dbtrace.bridge.engine.dao.AptItemPositionDao;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition;
import com.elsagdatamat.framework.dao.IGeneralDAO;

public class AptItemPositionBLImpl implements AptItemPositionBL {

	private static final boolean BLOCKING = false;

	private AptItemPositionDao aptItemPositionDao;

	private IGeneralDAO generalDao;

	public IGeneralDAO getGeneralDao() {
		return generalDao;
	}

	public void setGeneralDao(IGeneralDAO generalDao) {
		this.generalDao = generalDao;
	}

	public AptItemPositionDao getAptItemPositionDao() {
		return aptItemPositionDao;
	}

	public void setAptItemPositionDao(AptItemPositionDao aptItemPositionDao) {
		this.aptItemPositionDao = aptItemPositionDao;
	}

	@Override
	public void insertOrUpdate(AptItemPosition itemP){
		aptItemPositionDao.insertOrUpdate(itemP);
	}

	@Override
	public void update(AptItemPosition itemP){
		aptItemPositionDao.update(itemP);
	}

	@Override
	public void insert(AptItemPosition itemP) {
		aptItemPositionDao.insert(itemP);
	}

	@Override
	public AptItemPosition find(String code) {
		return aptItemPositionDao.findById(code, BLOCKING);
	}
}
