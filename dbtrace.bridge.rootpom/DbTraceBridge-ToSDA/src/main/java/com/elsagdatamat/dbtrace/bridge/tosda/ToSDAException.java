package com.elsagdatamat.dbtrace.bridge.tosda;

public class ToSDAException extends Exception {

	private static final long serialVersionUID = 15365L;

	public ToSDAException (String message) {
		super(message);
	}
}
