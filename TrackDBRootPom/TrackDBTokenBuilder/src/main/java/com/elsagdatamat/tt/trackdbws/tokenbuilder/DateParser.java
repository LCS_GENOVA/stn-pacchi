package com.elsagdatamat.tt.trackdbws.tokenbuilder;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;


public class DateParser {

		public static Date parseADate(String willBeADate) {
			
			DateFormat italianDataFormat = 
				DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.ITALIAN);
			Date date = null;
			
			try {
				date = italianDataFormat.parse(willBeADate);
			} catch (ParseException parseException) {
				System.out.print("Errore formato data. Formato corretto " + italianDataFormat.format(new Date()));
			}
		
			return date;
		}
		
}
