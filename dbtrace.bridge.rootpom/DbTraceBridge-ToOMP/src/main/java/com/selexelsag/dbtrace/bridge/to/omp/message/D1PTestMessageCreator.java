/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.message;


/**
 * @author arodriguez
 *
 */
public class D1PTestMessageCreator extends D1PMessageCreator {

	public static final String EMPTY_MESSAGE_BODY = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
			"<MSG><HDR OFCID=\"FRZ\" SWREL=\"SDA01\"/><D1><DHDR>" +
            "<DID>012345678901234</DID><OFCOTH>28428</OFCOTH><DT>02/53/2012 09:05:53</DT><OFNAME>Destinatario</OFNAME>" +
            "</DHDR></D1></MSG>";
	
	/**
	 * @param textMessage
	 */
	public D1PTestMessageCreator(String textMessage) {
		super(textMessage);
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.D1PMessageCreator.message.TPAMessageCreator#isTestMessage()
	 */
	@Override
	protected boolean isTestMessage() {
		return true;
	}

}
