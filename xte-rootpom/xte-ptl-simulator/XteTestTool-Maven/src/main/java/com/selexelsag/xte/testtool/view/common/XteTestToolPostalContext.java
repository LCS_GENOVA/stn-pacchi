/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.common;

import com.selexelsag.xte.testtool.datamodel.MessageType;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/**
 *
 * @author Da Procida
 */
public class XteTestToolPostalContext {

    private StyleContext context = null;
    private StyledDocument generatedXmldocument = null;
    private Style generatedXmlStyle = null;
    
    public XteTestToolPostalContext() {
        this.context = new StyleContext();
        this.generatedXmldocument = new DefaultStyledDocument(context);
        defineGeneratedXmlStyle();
    }

    private void defineGeneratedXmlStyle() {
        this.generatedXmlStyle = context.addStyle("XteTestToolGeneratedXmlStyle", null);
        this.generatedXmlStyle.addAttribute(StyleConstants.FontFamily, "tahoma");
        this.generatedXmlStyle.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        this.generatedXmlStyle.addAttribute(StyleConstants.FontSize, new Integer(12));
        this.generatedXmlStyle.addAttribute(StyleConstants.Bold, false);
        this.generatedXmlStyle.addAttribute(StyleConstants.Italic, false);
        this.generatedXmlStyle.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
    }

    
    public StyledDocument putContentOnGeneratedXml(String message) {
        try {
            generatedXmldocument.remove(0, generatedXmldocument.getLength());

            generatedXmldocument.insertString(generatedXmldocument.getLength(), message.trim(), generatedXmlStyle);

            return generatedXmldocument;

        } catch (BadLocationException badLocationException) {
            return null;
        }

    }

    public StyledDocument removeContentFromGeneratedXml() {
        try {
            generatedXmldocument.remove(0, generatedXmldocument.getLength());
            return generatedXmldocument;
        } catch (BadLocationException badLocationException) {
            return null;
        }

    }

}
