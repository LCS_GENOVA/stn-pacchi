<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ED="http://www.selexelsag.com/Tracking/Messages/xslt/CTtoC1Transform">
	<xsl:output indent="yes" method="xml" omit-xml-declaration="no" encoding="UTF-8"/>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="CT">
		<xsl:element name="C1">
			<xsl:apply-templates select="@*|node()"/>
		</xsl:element>
	</xsl:template>	
</xsl:stylesheet>
