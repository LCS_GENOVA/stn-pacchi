package com.selexelsag.xte.task;

public interface IRunOnce {
	public void runOnce();
}
