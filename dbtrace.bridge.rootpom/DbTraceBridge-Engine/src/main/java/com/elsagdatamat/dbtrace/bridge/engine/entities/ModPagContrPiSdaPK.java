package com.elsagdatamat.dbtrace.bridge.engine.entities;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;


/** 
* R.L. BeanGenerator - 22.05.2012 03:10:16
*/ 
@Embeddable
public class ModPagContrPiSdaPK implements Serializable {

	private static final long serialVersionUID = 2991649696276408497L;

	@Column(name = "PI_CODE", nullable =false)
	private String piCode;

	@Column(name = "PI_CHANNEL", nullable =false)
	private String piChannel;
	
	public String getPiCode(){
		return piCode;
	}

	public void setPiCode(String piCode){
		this.piCode=piCode;
	}

	public String getPiChannel() {
		return piChannel;
	}

	public void setPiChannel(String piChannel) {
		this.piChannel = piChannel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((piChannel == null) ? 0 : piChannel.hashCode());
		result = prime * result + ((piCode == null) ? 0 : piCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModPagContrPiSdaPK other = (ModPagContrPiSdaPK) obj;
		if (piChannel == null) {
			if (other.piChannel != null)
				return false;
		} else if (!piChannel.equals(other.piChannel))
			return false;
		if (piCode == null) {
			if (other.piCode != null)
				return false;
		} else if (!piCode.equals(other.piCode))
			return false;
		return true;
	}

}