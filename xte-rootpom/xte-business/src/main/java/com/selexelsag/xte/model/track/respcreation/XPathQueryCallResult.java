package com.selexelsag.xte.model.track.respcreation;

import java.io.Serializable;

public class XPathQueryCallResult implements Serializable{

	public static final String XPATH_ESITO = "/Risultato/@Esito";
	
	public static final String XPATH_DESC_ESITO = "/Risultato/@DescrizioneEsito";
	
	public static final String XPATH_CODICE_ESITO = "/Risultato/@CodiceEsito";
	
	public static final String XPATH_REITERABILITA = "/Risultato/@Reiterabilita";

	/**
	 * 
	 */
	private static final long serialVersionUID = -2775526393921754768L;
	
	private String callResultXpath;
	
	private String callResultDescXpath;
	
	private String callResultCodeXpath;
	
	private String callResultReiteration;

	public String getCallResultXpath() {
		return callResultXpath;
	}

	public void setCallResultXpath(String callResultXpath) {
		this.callResultXpath = callResultXpath;
	}

	public String getCallResultDescXpath() {
		return callResultDescXpath;
	}

	public void setCallResultDescXpath(String callResultDescXpath) {
		this.callResultDescXpath = callResultDescXpath;
	}

	public String getCallResultCodeXpath() {
		return callResultCodeXpath;
	}

	public void setCallResultCodeXpath(String callResultCodeXpath) {
		this.callResultCodeXpath = callResultCodeXpath;
	}

	public String getcallResultReiteration() {
		return callResultReiteration;
	}

	public void setcallResultReiteration(String callResultReiteration) {
		this.callResultReiteration = callResultReiteration;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return String
				.format("XPathQueryCallResult [callResultXpath=%s, callResultDescXpath=%s, callResultCodeXpath=%s, callResultReiteration=%s]",
						callResultXpath, callResultDescXpath,
						callResultCodeXpath, callResultReiteration);
	}

	public XPathQueryCallResult(String callResultXpath,
			String callResultDescXpath, String callResultCodeXpath,
			String callResultReiteration) {
		super();
		this.callResultXpath = callResultXpath;
		this.callResultDescXpath = callResultDescXpath;
		this.callResultCodeXpath = callResultCodeXpath;
		this.callResultReiteration = callResultReiteration;
	}

	
}
