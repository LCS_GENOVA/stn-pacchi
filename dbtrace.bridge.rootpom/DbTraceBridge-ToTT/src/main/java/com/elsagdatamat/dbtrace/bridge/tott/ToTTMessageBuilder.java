package com.elsagdatamat.dbtrace.bridge.tott;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.dbtrace.bridge.engine.MessageBuilderBase;
import com.elsagdatamat.dbtrace.bridge.engine.bl.IBLFromTo;
import com.selexelsag.tracktrace.extension.bl.servises.PacchiService;
import com.selexelsag.tracktrace.extension.bl.servises.TraceDispatchResult;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TokenTilltoDateByFiltersList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesListNextToken;

public class ToTTMessageBuilder extends MessageBuilderBase {

	protected final Log log = LogFactory.getLog(getClass());

	private PacchiService pacchiService;

	public PacchiService getPacchiService() {
		return pacchiService;
	}

	public void setPacchiService(PacchiService pacchiService) {
		this.pacchiService = pacchiService;
	}

	// TODO: springare
	private IBLFromTo blFromNspToCmp;

	public void setBlFromNspToCmp(IBLFromTo blFromNspToCmp) {
		this.blFromNspToCmp = blFromNspToCmp;
	}

	// IMessageBuilder
	@Override
	@Transactional(value = "dbTraceBridge-transactionManager", propagation = Propagation.REQUIRED)
	public List<String> create(TracesList input) {

		List<TraceDispatchResult> analyzeTraces = pacchiService.analyzeBatchTracesList(input);
		for (TraceDispatchResult traceDispatch : analyzeTraces) {
			String nspSource = traceDispatch.getFrazionarioMittente();

			try {
				// 2 Ricavare il destinatario dal mittente -->uso FromNSPToCMP
				String cmpDestination = blFromNspToCmp.getCmpDestinationFromNspSource(nspSource);
				if ((cmpDestination == null) || (cmpDestination.isEmpty())) {
					log.error("Found dispatch from " + nspSource + ". Destination is unknown");

					discardedTraceManager.saveDiscardedTraces(token.getServiceId(), "Found dispatch " + traceDispatch.getDispatchCode()
							+ " from " + nspSource
							+ ". Destination is unknown", new Exception("Found dispatch from " + nspSource + ". Destination is unknown"));
					return null;
				}

				// 3 Costruire l'immagine elettronica --> uso
				// preadvisingOrchestraror
				pacchiService.createElectronicalImage(cmpDestination, traceDispatch, input);

				discardedTraceManager.saveDiscardedTraces(token.getServiceId(), "Found dispatch " + traceDispatch.getDispatchCode()
						+ " from " + nspSource + ". Destination is " + cmpDestination, null);

			} catch (Exception e) {
				log.error(e.getMessage(), e);
				discardedTraceManager.saveDiscardedTraces(token.getServiceId(), e.getClass().getName(), e);
			}
		}
		log.debug("Leaving create");
		return null;
	}

	// IMessageSender
	@Override
	public void send(final List<String> strList) throws IOException {
	}

	@Override
	public TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate) {
		log.debug(String.format("Entering getTraces for service '%s' (wsToken='%s'): stopDate=%s;", token.getServiceId(), token.getWsToken(), stopDate));
		// Costruisco il parametro per la richiesta delle tracce
		// Voglio tutte le tracce a partire dal token precedentemente ottenuto
		// fino a stopDate (passato dal motore che lo inizializza una volta per
		// tutte)....
		TokenTilltoDateByFiltersList getTracesInput = new TokenTilltoDateByFiltersList();
		getTracesInput.setTilltoDate(getGCalendar(stopDate));
		getTracesInput.setToken(token.getWsToken());
		log.debug("Calling web service getTraces");
		getTracesInput.setTracesFilterByExampleEqual(null);
		getTracesInput.setTracesFilterByExampleNotEqual(null);

		log.debug("TilltoDate: " + getTracesInput.getTilltoDate().toString());

		TracesListNextToken tlnt = twsp.getNextSackImageFromNSP(getTracesInput);

		log.debug("Called web service getTraces");

		TracesList tracesList = tlnt.getTraceList();
		if ((tracesList != null) && (tracesList.getTraces() != null))
			log.debug("Obtained traces n.: " + tracesList.getTraces().size());
		else
			log.debug("Obtained traces n.: 0");
		// Prese le tracce, aggiorno il wsToken dentro lo SDAToken
		token.setWsToken(tlnt.getNextToken());

		log.debug("Leaving getTraces");
		return tracesList;
	}

}
