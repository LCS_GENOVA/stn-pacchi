/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.am;

import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.controller.dp.*;
import com.selexelsag.xte.testtool.datamodel.XteGeneraMazzettiGenericObj;
import com.selexelsag.xte.testtool.datamodel.XteOffice;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.am.XteTestToolGeneraMazzettiAMView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.objectmodel.XteOfficeTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteTipoMazzettiTableModel;
import com.selexelsag.xte.testtool.view.objectrenderer.XteTableHeaderButtonRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolGeneraMazzettiAMController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener, ListSelectionListener, TableModelListener {

    private XteTestToolGeneraMazzettiAMView generaMazzettiAMView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolItemOracleDPController.class);
    private List<JTextComponent> mailPieceFieldList = new ArrayList<JTextComponent>();
    private XteOffice selectedOffice = null;
    private int currentOfficeTableSelectedRow = -1;
    
    public XteTestToolGeneraMazzettiAMController(JPanel generaMazzettiAMView) {
        try {
            logger.info("XteTestToolGeneraMazzettiAMController start class creation");
            this.generaMazzettiAMView = (XteTestToolGeneraMazzettiAMView) generaMazzettiAMView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.generaMazzettiAMView.initializeView();
            addViewObjectsListeners();
            logger.info("XteTestToolGeneraMazzettiAMController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolGeneraMazzettiAMController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolGeneraMazzettiAMController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Genera Mazzetti Acquisizione Mazzetti controller creation failed", ex);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = generaMazzettiAMView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        generaMazzettiAMView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.generaMazzettiAMView.getGeneraMazzettiPanelRef().addAncestorListener(this);        
                
        this.generaMazzettiAMView.getOfficeTableRef().getSelectionModel().addListSelectionListener(this);
        this.generaMazzettiAMView.getOfficeTableRef().getModel().addTableModelListener(this);

        this.generaMazzettiAMView.getThreadNumberTextRef().addFocusListener(this);
        
        this.generaMazzettiAMView.getGeneraMazzettiButtonRef().addActionListener(this);
        this.generaMazzettiAMView.getTestWSButtonRef().addActionListener(this);
        this.generaMazzettiAMView.getBackButtonRef().addActionListener(this);
        this.generaMazzettiAMView.getHomeButtonRef().addActionListener(this);
        this.generaMazzettiAMView.getExitButtonRef().addActionListener(this);
                
        this.generaMazzettiAMView.getThreadNumberTextRef().getDocument().addDocumentListener(this);
        
        this.generaMazzettiAMView.getTipoMazzettiTableRef().getTableHeader().addMouseListener(this);
        this.generaMazzettiAMView.getPortaLettereTableRef().getTableHeader().addMouseListener(this);
        this.generaMazzettiAMView.getPercorsoTableRef().getTableHeader().addMouseListener(this);
        this.generaMazzettiAMView.getMessiNotificatoriTableRef().getTableHeader().addMouseListener(this);
                
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(generaMazzettiAMView.getGeneraMazzettiButtonRef()))
        {
            logger.debug("XteTestToolGeneraMazzettiAMController actionPerformed invoke generaXmlData");
            generaMazzetti();
        }

        if(source.equals(generaMazzettiAMView.getTestWSButtonRef()))
        {
            logger.debug("XteTestToolGeneraMazzettiAMController actionPerformed invoke impostaDatiWebService");
            testWebService();
        }

        if(source.equals(generaMazzettiAMView.getBackButtonRef()))
        {
            logger.debug("XteTestToolGeneraMazzettiAMController actionPerformed invoke back");
            back();
        }

        if(source.equals(generaMazzettiAMView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolGeneraMazzettiAMController actionPerformed invoke home");
            home();
        }

        if(source.equals(generaMazzettiAMView.getExitButtonRef()))
        {
            logger.debug("XteTestToolGeneraMazzettiAMController actionPerformed invoke exit");
            exit();
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolGeneraMazzettiAMView)
        {
            logger.debug("XteTestToolGeneraMazzettiAMController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolGeneraMazzettiAMController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Genera Mazzetti Acquisizione Mazzetti controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolGeneraMazzettiAMController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Genera Mazzetti Acquisizione Mazzetti controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
            
        Object source = e.getSource();
        JTableHeader header;
        XteTableHeaderButtonRenderer renderer;
        
        if(source.equals(generaMazzettiAMView.getTipoMazzettiTableRef().getTableHeader()))
        {
        
            header = generaMazzettiAMView.getTipoMazzettiTableRef().getTableHeader();
            renderer = (XteTableHeaderButtonRenderer) generaMazzettiAMView.getTipoMazzettiTableRef().getTableHeader().getDefaultRenderer();
            int col = header.columnAtPoint(e.getPoint());
            int sortCol = header.getTable().convertColumnIndexToModel(col);
            renderer.setPressedColumn(col);
            renderer.setSelectedColumn(col);
            header.repaint();

            if (header.getTable().isEditing()) {
                header.getTable().getCellEditor().stopCellEditing();
            }

            boolean isAscent;
            if (XteTableHeaderButtonRenderer.DOWN == renderer.getState(col)) {
                isAscent = true;
            } else {
                isAscent = false;
            }
            ((XteTipoMazzettiTableModel) header.getTable().getModel())
                    .sortByColumn(sortCol, isAscent);
        }

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }    
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object source = e.getSource();
        if(source.equals(generaMazzettiAMView.getOfficeTableRef().getSelectionModel()))
        {               
            if (e.getValueIsAdjusting()) {
                    return;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
                        currentOfficeTableSelectedRow = generaMazzettiAMView.getOfficeTableRef().getSelectedRow();
                        logger.debug("XteTestToolGeneraMazzettiAMController valueChanged currentSelectedRow = " + currentOfficeTableSelectedRow);
                        logger.debug("XteTestToolGeneraMazzettiAMController valueChanged: " + ((XteOfficeTableModel)generaMazzettiAMView.getOfficeTableRef().getModel()).getMap().size());
                        if(currentOfficeTableSelectedRow!=-1)
                        {
                            selectedOffice = ((XteOfficeTableModel)generaMazzettiAMView.getOfficeTableRef().getModel()).getSelectedItem(currentOfficeTableSelectedRow);
                        }
                        else
                        {
                            selectedOffice = null;
                        }
                    }
                } );
        }
        
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int eventType = e.getType();
        DefaultTableModel model = (DefaultTableModel) e.getSource();
        if(model.equals(generaMazzettiAMView.getOfficeTableRef().getModel()))
        {               
            logger.debug("XteTestToolGeneraMazzettiAMController tableChanged: " + ((XteOfficeTableModel)generaMazzettiAMView.getOfficeTableRef().getModel()).getMap().size());
        }
    }

    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        generaMazzettiAMView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolGeneraMazzettiAMController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.generaMazzettiAMView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(generaMazzettiAMView);
            updateXteTestToolDataModel(dataModel);

            clearMessageBar();

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(generaMazzettiAMView.getGeneraMazzettiButtonRef());
            disabledComponents.add(generaMazzettiAMView.getTestWSButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            XteOffice item1 = new XteOffice();            
            item1.setUfficio("172.31.11.11");
            item1.setNumeroInvii(new Long(10));
            item1.setNumeroMazzetti(new Long(0));
            item1.setStato(Boolean.TRUE);
            item1.setProgresso("");

            XteOffice item2 = new XteOffice();            
            item2.setUfficio("172.31.11.12");
            item2.setNumeroInvii(new Long(10));
            item2.setNumeroMazzetti(new Long(0));
            item2.setStato(Boolean.FALSE);
            item2.setProgresso("");
                        
            this.generaMazzettiAMView.getOfficeTableModelRef().addOfficeItem(item1);
            this.generaMazzettiAMView.getOfficeTableModelRef().addOfficeItem(item2);
            
            XteGeneraMazzettiGenericObj tipoMazzetti1 = new XteGeneraMazzettiGenericObj();
            tipoMazzetti1.setTipo("Mazzetto 1");
            tipoMazzetti1.setNumero(new Long(10));

            XteGeneraMazzettiGenericObj tipoMazzetti2 = new XteGeneraMazzettiGenericObj();
            tipoMazzetti2.setTipo("Mazzetto 2");
            tipoMazzetti2.setNumero(new Long(8));
            
            this.generaMazzettiAMView.getTipoMazzettiTableModelRef().addTipoMazzettoItem(tipoMazzetti1);
            this.generaMazzettiAMView.getTipoMazzettiTableModelRef().addTipoMazzettoItem(tipoMazzetti2);
            
            generaMazzettiAMView.getOfficeTableRef().requestFocus();

            logger.info("XteTestToolGeneraMazzettiAMController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolGeneraMazzettiAMController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolGeneraMazzettiAMController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Genera Mazzetti Acquisizione Mazzetti controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolGeneraMazzettiAMController start exit");
        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(generaMazzettiAMView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolGeneraMazzettiAMController exit in progress");
            generaMazzettiAMView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolGeneraMazzettiAMController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(generaMazzettiAMView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(generaMazzettiAMView.getGeneraMazzettiPanelRef().getParent(), generaMazzettiAMView.getGeneraMazzettiPanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolGeneraMazzettiAMController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(generaMazzettiAMView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(generaMazzettiAMView.getGeneraMazzettiPanelRef().getParent(), generaMazzettiAMView.getGeneraMazzettiPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private void generaMazzetti()
    {
        logger.info("XteTestToolGeneraMazzettiAMController start generaMazzetti");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            logger.info("XteTestToolGeneraMazzettiAMController generaMazzetti executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolGeneraMazzettiAMController generaMazzetti failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolGeneraMazzettiAMController generaMazzetti failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Genera Mazzetti Acquisizione Mazzetti controller generaMazzetti failed", ex);
        }
    }
    
    private void testWebService()
    {
        try {
            logger.info("XteTestToolGeneraMazzettiAMController start testWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            logger.info("XteTestToolGeneraMazzettiAMController testWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolGeneraMazzettiAMController testWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolGeneraMazzettiAMController testWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Genera Mazzetti Acquisizione Mazzetti controller testWebService failed", ex);
        }
    }

}
