/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.logging;

import org.apache.log4j.PatternLayout;
import org.apache.log4j.helpers.PatternParser;

/**
 *
 * @author Da Procida
 */
public class XteTestToolLoggerPatternLayout extends PatternLayout {


    public XteTestToolLoggerPatternLayout() {
        this(DEFAULT_CONVERSION_PATTERN);
    }

    public XteTestToolLoggerPatternLayout(String pattern) {
        super(pattern);
    } 

    @Override
    public PatternParser createPatternParser(String pattern) {
        return new XteTestToolLoggerPatternParser(pattern == null ? DEFAULT_CONVERSION_PATTERN : pattern);
    }
}
