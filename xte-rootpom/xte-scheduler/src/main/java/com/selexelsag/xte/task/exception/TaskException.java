package com.selexelsag.xte.task.exception;

public class TaskException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7553442141607889214L;

	public TaskException() {
	}

	public TaskException(String message) {
		super(message);
	}

	public TaskException(String message, Throwable cause) {
		super(message, cause);
	}
}