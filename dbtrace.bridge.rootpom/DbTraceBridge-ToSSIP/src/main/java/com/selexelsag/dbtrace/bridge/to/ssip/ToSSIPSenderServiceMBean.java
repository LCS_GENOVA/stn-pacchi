package com.selexelsag.dbtrace.bridge.to.ssip;
import org.jboss.system.ServiceMBean;

/**
 * 
 */

/**
 * @author arodriguez
 *
 */
public interface ToSSIPSenderServiceMBean extends ServiceMBean {

	public void reloadConfiguration();
	
	public void setMinutesToRecovery(Integer minValue);

	public Integer getMinutesToRecovery();

	public void setMinutesToRestart(Integer minValue);

	public Integer getMinutesToRestart();

    public void setSpringConfigPath(String path);

    public String getSpringConfigPath();

    public void setConfigPath(String configPath);

    public String getConfigPath();

    public boolean isSpringLoaded();
    
    public boolean isStarted();

    public long getErrorCount();
    
    public String getNumberOfRuns();
    
    public String getLastStartDate();
    
    public void sendTestMessage();
    
}
