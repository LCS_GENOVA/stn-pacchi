package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TAG_TO_CONVERT_SUBP")
public class TagToConvertSubP implements Serializable {

	private static final long serialVersionUID = -385902249706841565L;
	
	@Id
	@Column(name="WHAT_HAPPENED")
	private String tipoTraccia;
	
	@Column(name="SUBP_TAG")
	private String tagProdotto;

	public String getTipoTraccia() {
		return tipoTraccia;
	}

	public void setTipoTraccia(String tipoTraccia) {
		this.tipoTraccia = tipoTraccia;
	}

	public String getTagProdotto() {
		return tagProdotto;
	}

	public void setTagProdotto(String tagProdotto) {
		this.tagProdotto = tagProdotto;
	}

	
}
