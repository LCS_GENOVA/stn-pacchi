package com.elsagdatamat.tt.trackdbws.dao;

import com.elsagdatamat.framework.dao.IGenericDAO;
import com.elsagdatamat.tt.trackdbws.beans.Seal;

public interface ISealDao extends IGenericDAO<Seal, Long> {


}