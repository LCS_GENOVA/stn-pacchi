package com.selexelsag.xte.business.tracking;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.selexelsag.messagetobedelivered.entity.MessageToBeDelivered;
import com.selexelsag.messagetobedelivered.vo.MessageDestinationType;
import com.selexelsag.messagetobedelivered.vo.TrackingMessageException;
import com.selexelsag.xte.persistence.entities.AsyncMessages;
import com.selexelsag.xte.persistence.exceptions.PersistenceException;

public interface IMessageDBManager {

	void sendMessage(AsyncMessages message, MessageDestinationType destType, String processedMessage, String messageType, String queueName, boolean compressed, HashMap<String, Serializable> additionalData) throws TrackingMessageException, PersistenceException, Exception;

	void sendMultipleMessage(AsyncMessages message, List<MessageToBeDelivered> mtbdList, Boolean deleteAsyncMessages) throws Exception;

	MessageToBeDelivered buildMessage(MessageDestinationType destType, String processedMessage, String messageType, String queueName, boolean compressed, HashMap<String, Serializable> additionalData);

	void moveToDiscardedMessages(AsyncMessages message, String errorDescr) throws Exception;

	void sendMultipleMessageAndMoveToDiscardedMessages(AsyncMessages message, List<MessageToBeDelivered> mtbdList, String errorDescr) throws Exception;
}
