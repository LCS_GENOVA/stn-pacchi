package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.util.Map;

import com.elsagdatamat.dbtrace.bridge.engine.entities.CodiciProdottoPICodiciProdottoSDA;
import com.elsagdatamat.dbtrace.bridge.engine.entities.CodiciProdottoPICodiciProdottoSDAPK;

public interface ICodiciProdottoPICodiciProdottoSDAManager extends IManager<CodiciProdottoPICodiciProdottoSDA, CodiciProdottoPICodiciProdottoSDAPK> {

	public Map<String,String> getConversionMap();

	String getSdaCode(String objectCode);

	String getPiCode(String objectCode);

}
