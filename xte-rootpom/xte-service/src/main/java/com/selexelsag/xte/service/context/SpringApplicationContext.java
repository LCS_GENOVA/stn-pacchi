package com.selexelsag.xte.service.context;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Contesto dei bean di spring
 *
 */
public class SpringApplicationContext implements ApplicationContextAware 
{
	private static ApplicationContext appContext;
	
	private SpringApplicationContext() {}
	
	public static Object getBean(String beanName) 
	{
		return appContext.getBean(beanName);
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException 
	{
		appContext = applicationContext;
	}
}