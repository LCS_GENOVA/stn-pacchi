package com.selexelsag.xte.bpm.flows;

import java.io.File;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.drools.KnowledgeBase;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.event.process.ProcessCompletedEvent;
import org.drools.event.process.ProcessEventListener;
import org.drools.event.process.ProcessNodeLeftEvent;
import org.drools.event.process.ProcessNodeTriggeredEvent;
import org.drools.event.process.ProcessStartedEvent;
import org.drools.event.process.ProcessVariableChangedEvent;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.xte.bpm.exceptions.BpmException;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.AckCreationNode;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.CapNormalizationNode;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.CheckSyncFLowNode;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.DataParsingNode;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.ErrorCreationNode;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.IncomingTrasformationNode;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.OutgoingTrasformationNode;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.PersistentSaveNode;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.PostHttpNode;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.PreparazioneInoltroNode;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.ResponseCreationNode;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.SemanticValidationNode;
import com.selexelsag.xte.bpm.flows.servicenodes.handlers.SintatticValidationNode;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.service.system.IAppConfig;

public class ProcessManager implements IProcessManager 
{

	private Logger logger = LogManager.getLogger(ProcessManager.class.getName());
	
	@Autowired
	private IAppConfig appConfig;

	private KnowledgeBase knowledgeBase;
	
	private StatefulKnowledgeSession ksession;
	public StatefulKnowledgeSession getKsession(){return ksession;}
	public void setKsession(StatefulKnowledgeSession ksession) {this.ksession = ksession;}
	
	public void init()
	{
		try
		{
			String pathResources = appConfig.getBpmParam(IAppConfig.PATH_JBPM_RESOURCES);
			
			logger.debug("configuration of ProcessManager");
			logger.debug("directory that contain jbpm resource is: "+pathResources);

			knowledgeBase = readKnowledgeBase(pathResources);
			ksession = knowledgeBase.newStatefulKnowledgeSession();
			
			registerServiceTaskHandlers(ksession);
			registerProcessListener(ksession);
		}
		catch (Throwable t)
		{
			logger.error("Errore inizializzazione",t);
		}
	}

	@Override
	public String getProcessId(FlowParams flowParams) throws BpmException 
	{
		String processId = null;
	
		StatefulKnowledgeSession session = knowledgeBase.newStatefulKnowledgeSession();
		try 
		{
			StringBuilder procId = new StringBuilder();
			session.insert(flowParams);

			session.insert(procId);
			session.fireAllRules();
			
			processId = procId.toString();
		}
		catch (Throwable e) 
		{
			BpmException bfme = new BpmException("Errore in recupero del processId ", e);
			throw bfme;
		}
		finally 
		{
			session.dispose();
		}
		
		if ((processId==null)  || (processId.equals("")))
		{
			BpmException bfme = new BpmException("Flusso non trovato " + flowParams, null);
			throw bfme;
		}
		
		return processId;
	}
	

	private void registerProcessListener(StatefulKnowledgeSession ksession) 
	{
		ksession.addEventListener(new ProcessEventListener() 
		{
			private boolean isLoggingProcess = false;
			private boolean isLoggingVariables = false;
			private boolean isLoggingNodes = false;

			@Override
			public void beforeVariableChanged(ProcessVariableChangedEvent arg0) 
			{
				if (isLoggingVariables) 
				{
					logger.info("beforeVariableChanged " + arg0.getVariableInstanceId()+ " " + arg0.getVariableId() + ": from " + arg0.getOldValue() + " to " + arg0.getNewValue());
				}
			}

			@Override
			public void beforeProcessStarted(ProcessStartedEvent arg0) 
			{
				if (isLoggingProcess) 
				{
					logger.info("beforeProcessStarted " + arg0.getProcessInstance().getProcessName() + " " + arg0.getProcessInstance().getProcessId());
				}
			}

			@Override
			public void beforeProcessCompleted(ProcessCompletedEvent arg0) 
			{
				if (isLoggingProcess) 
				{
					logger.info("beforeProcessCompleted " + arg0.getProcessInstance().getProcessName() + " " + arg0.getProcessInstance().getProcessId());
				}
			}

			@Override
			public void beforeNodeTriggered(ProcessNodeTriggeredEvent arg0) 
			{
				if (isLoggingNodes) 
				{
					logger.info("beforeNodeTriggered " + arg0.getNodeInstance().getNodeName() + " " + arg0.getNodeInstance().getNodeId());
				}
			}

			@Override
			public void beforeNodeLeft(ProcessNodeLeftEvent arg0) 
			{
				if (isLoggingNodes) 
				{
					logger.info("beforeNodeLeft " + arg0.getNodeInstance().getNodeName() + " " + arg0.getNodeInstance().getNodeId());
				}
			}

			@Override
			public void afterVariableChanged(ProcessVariableChangedEvent arg0) 
			{
				if (isLoggingVariables) 
				{
					logger.info("afterVariableChanged " + arg0.getVariableInstanceId()+ " "  +  arg0.getVariableId() + ": from " + arg0.getOldValue() + " to " + arg0.getNewValue());
				}
			}

			@Override
			public void afterProcessStarted(ProcessStartedEvent arg0) 
			{
				if (isLoggingProcess) 
				{
					logger.info("afterProcessStarted " + arg0.getProcessInstance().getProcessName() + " " + arg0.getProcessInstance().getProcessId());
				}
			}

			@Override
			public void afterProcessCompleted(ProcessCompletedEvent arg0) 
			{
				if (isLoggingProcess) 
				{
					logger.info("afterProcessCompleted " + arg0.getProcessInstance().getProcessName() + " " + arg0.getProcessInstance().getProcessId());
				}
			}

			@Override
			public void afterNodeTriggered(ProcessNodeTriggeredEvent arg0) 
			{
				if (isLoggingNodes) 
				{
					logger.info("afterNodeTriggered " + arg0.getNodeInstance().getNodeName() + " " + arg0.getNodeInstance().getNodeId());
				}
			}

			@Override
			public void afterNodeLeft(ProcessNodeLeftEvent arg0) 
			{
				if (isLoggingNodes) 
				{
					logger.info("afterNodeLeft " + arg0.getNodeInstance().getNodeName() + " " + arg0.getNodeInstance().getNodeId());
				}
			}
		});
	}
	
	private void registerServiceTaskHandlers(StatefulKnowledgeSession ksession) {
		ksession.getWorkItemManager().registerWorkItemHandler("PreparazioneInoltroNode", new PreparazioneInoltroNode());
		ksession.getWorkItemManager().registerWorkItemHandler("PostHttpNode", new PostHttpNode());
		ksession.getWorkItemManager().registerWorkItemHandler("SintatticValidationNode", new SintatticValidationNode());
		ksession.getWorkItemManager().registerWorkItemHandler("SemanticValidationNode", new SemanticValidationNode());
		ksession.getWorkItemManager().registerWorkItemHandler("PersistentSaveNode", new PersistentSaveNode());
		ksession.getWorkItemManager().registerWorkItemHandler("DataParsingNode", new DataParsingNode());
		ksession.getWorkItemManager().registerWorkItemHandler("IncomingTrasformationNode", new IncomingTrasformationNode());
		ksession.getWorkItemManager().registerWorkItemHandler("OutgoingTrasformationNode", new OutgoingTrasformationNode());
		ksession.getWorkItemManager().registerWorkItemHandler("CapNormalizationNode", new CapNormalizationNode());
		ksession.getWorkItemManager().registerWorkItemHandler("ResponseCreationNode", new ResponseCreationNode());
		ksession.getWorkItemManager().registerWorkItemHandler("AckCreationNode", new AckCreationNode());
		ksession.getWorkItemManager().registerWorkItemHandler("CheckSyncFLowNode", new CheckSyncFLowNode());
		ksession.getWorkItemManager().registerWorkItemHandler("ErrorCreationNode", new ErrorCreationNode());
	}
	
	private static KnowledgeBase readKnowledgeBase(String path) throws Exception {
		
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		
		kbuilder.add(ResourceFactory.newFileResource(path + File.separatorChar + "main_process.bpmn"), ResourceType.BPMN2);
		kbuilder.add(ResourceFactory.newFileResource(path + File.separatorChar + "sync.default.bpmn"), ResourceType.BPMN2);
		kbuilder.add(ResourceFactory.newFileResource(path + File.separatorChar + "async.default.bpmn"), ResourceType.BPMN2);
		kbuilder.add(ResourceFactory.newFileResource(path + File.separatorChar + "composizione_risposta.bpmn"), ResourceType.BPMN2);
		kbuilder.add(ResourceFactory.newFileResource(path + File.separatorChar + "inoltro.bpmn"), ResourceType.BPMN2);
		kbuilder.add(ResourceFactory.newFileResource(path + File.separatorChar + "salvataggio.bpmn"), ResourceType.BPMN2);
		kbuilder.add(ResourceFactory.newFileResource(path + File.separatorChar + "trasformazione.bpmn"), ResourceType.BPMN2);
		kbuilder.add(ResourceFactory.newFileResource(path + File.separatorChar + "validazione.bpmn"), ResourceType.BPMN2);
		kbuilder.add(ResourceFactory.newFileResource(path + File.separatorChar + "norm_cap.bpmn"), ResourceType.BPMN2);
		kbuilder.add(ResourceFactory.newFileResource(path + File.separatorChar + "ProcessIdResolver.drl"), ResourceType.DRL);
		return kbuilder.newKnowledgeBase();
	}
}
