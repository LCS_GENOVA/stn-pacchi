/**
 * TrackingServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.selexelsag.xte.soap.track;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.selexelsag.xte.restler.track.TrackRestService;
import com.selexelsag.xte.service.context.SpringApplicationContext;
import com.selexelsag.xte.service.system.IAppConfig;

public class TrackingServiceLocator extends org.apache.axis.client.Service implements com.selexelsag.xte.soap.track.TrackingService {
	/**
	 * 
	 */
	private static final long serialVersionUID = 104706570391434323L;
	private static Log log = LogFactory.getLog(TrackRestService.class); 
	
	
    public TrackingServiceLocator() {
    	try{
    		if (log.isDebugEnabled())
    			log.debug("soap service locator initialization BEGINS");
    			
    		TrackingPort_address = (String)((IAppConfig)SpringApplicationContext.getBean(IAppConfig.APP_CONFIG_BEAN_NAME)).getGeneralParam(IAppConfig.GENERAL_ENDPOINT_URL_KEY);
    		
    		if (log.isDebugEnabled())
    			log.debug("soap service locator initialization ENDS");
    		
    	}catch (Throwable t){
    		log.fatal("soap service locator error loading endpoint", t);
    	}
    }

    public TrackingServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    	try{
    		if (log.isDebugEnabled())
    			log.debug("soap service locator initialization BEGINS");
    		
    		TrackingPort_address = (String)((IAppConfig)SpringApplicationContext.getBean(IAppConfig.APP_CONFIG_BEAN_NAME)).getGeneralParam(IAppConfig.GENERAL_ENDPOINT_URL_KEY);
    		if (log.isDebugEnabled())
    			log.debug("soap service locator initialization ENDS");
    		
    	}catch (Throwable t){
    		log.fatal("soap service locator error loading endpoint", t);
    	}
    }

    public TrackingServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
        try{
        	if (log.isDebugEnabled())
    			log.debug("soap service locator initialization BEGINS");
    		
    		TrackingPort_address = (String)((IAppConfig)SpringApplicationContext.getBean(IAppConfig.APP_CONFIG_BEAN_NAME)).getGeneralParam(IAppConfig.GENERAL_ENDPOINT_URL_KEY);
    		if (log.isDebugEnabled())
    			log.debug("soap service locator initialization ENDS");
    		
    	}catch (Throwable t){
    		log.fatal("soap service locator error loading endpoint", t);
    	}
    }

    // Use to get a proxy class for TrackingPort
    private java.lang.String TrackingPort_address = "http://";

    public java.lang.String getTrackingPortAddress() {
        return TrackingPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String TrackingPortWSDDServiceName = "TrackingPort";

    public java.lang.String getTrackingPortWSDDServiceName() {
        return TrackingPortWSDDServiceName;
    }

    public void setTrackingPortWSDDServiceName(java.lang.String name) {
        TrackingPortWSDDServiceName = name;
    }

    public com.selexelsag.xte.soap.track.Tracking getTrackingPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(TrackingPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getTrackingPort(endpoint);
    }

    public com.selexelsag.xte.soap.track.Tracking getTrackingPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.selexelsag.xte.soap.track.TrackingBindingStub _stub = new com.selexelsag.xte.soap.track.TrackingBindingStub(portAddress, this);
            _stub.setPortName(getTrackingPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setTrackingPortEndpointAddress(java.lang.String address) {
        TrackingPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.selexelsag.xte.soap.track.Tracking.class.isAssignableFrom(serviceEndpointInterface)) {
                com.selexelsag.xte.soap.track.TrackingBindingStub _stub = new com.selexelsag.xte.soap.track.TrackingBindingStub(new java.net.URL(TrackingPort_address), this);
                _stub.setPortName(getTrackingPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("TrackingPort".equals(inputPortName)) {
            return getTrackingPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservices.tt.elsagdatamat.com/", "TrackingService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservices.tt.elsagdatamat.com/", "TrackingPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("TrackingPort".equals(portName)) {
            setTrackingPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
