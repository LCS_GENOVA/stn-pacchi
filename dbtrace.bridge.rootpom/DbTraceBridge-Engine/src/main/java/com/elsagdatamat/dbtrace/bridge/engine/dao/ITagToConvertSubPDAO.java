package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.TagToConvertSubP;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface ITagToConvertSubPDAO extends IGenericDAO<TagToConvertSubP,String>{

}
