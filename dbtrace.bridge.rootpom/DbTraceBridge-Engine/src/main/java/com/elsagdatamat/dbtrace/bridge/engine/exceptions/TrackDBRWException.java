package com.elsagdatamat.dbtrace.bridge.engine.exceptions;

public class TrackDBRWException extends RuntimeException {

	private static final long serialVersionUID = -6917033600395706624L;

	public TrackDBRWException(){}

	public TrackDBRWException(String message, Throwable cause){
		super(message, cause);
	}
	
	public TrackDBRWException(String message){
		super(message);
	}
}
