/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.dp;

import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.XteInviiCausale;
import com.selexelsag.xte.testtool.datamodel.XteMailPiece;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.dp.XteTestToolItemOracleDPView;
import com.selexelsag.xte.testtool.view.objectmodel.XteInviiCausaleTableModel;
import java.awt.Color;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolItemOracleDPController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener, ListSelectionListener, TableModelListener {

    private XteTestToolItemOracleDPView itemOracleDPView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolItemOracleDPController.class);
    private XteMailPiece selectedMailPiece = null;
    private int currentMailPieceTableSelectedRow = -1;
    private List<JTextComponent> mailPieceFieldList = new ArrayList<JTextComponent>();
    private XteInviiCausale selectedInvioCausale = null;
    private int currentInviiCausaleTableSelectedRow = -1;
    
    public XteTestToolItemOracleDPController(JPanel itemOracleDPView) {
        try {
            logger.info("XteTestToolItemOracleDPController start class creation");
            this.itemOracleDPView = (XteTestToolItemOracleDPView) itemOracleDPView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.itemOracleDPView.initializeView();
            addViewObjectsListeners();
            logger.info("XteTestToolItemOracleDPController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolItemOracleDPController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolItemOracleDPController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Item Oracle Data Posta controller creation failed", ex);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = itemOracleDPView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        itemOracleDPView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.itemOracleDPView.getItemOraclePanelRef().addAncestorListener(this);        
        this.itemOracleDPView.getBundleIdTextRef().addFocusListener(this);
        this.itemOracleDPView.getBundleTPIdTextRef().addFocusListener(this);
                
        this.itemOracleDPView.getInviiCausaleTableRef().getSelectionModel().addListSelectionListener(this);
        this.itemOracleDPView.getInviiCausaleTableRef().getModel().addTableModelListener(this);

        this.itemOracleDPView.getTotaleInviiTextRef().addFocusListener(this);
        this.itemOracleDPView.getInviiMazzettoTextRef().addFocusListener(this);
        this.itemOracleDPView.getNumeroMazzettiTextRef().addFocusListener(this);
        this.itemOracleDPView.getMailPieceLatitudeTextRef().addFocusListener(this);
        this.itemOracleDPView.getMailPieceLongitudeTextRef().addFocusListener(this);
        
        this.itemOracleDPView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.itemOracleDPView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.itemOracleDPView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.itemOracleDPView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.itemOracleDPView.getBackButtonRef().addActionListener(this);
        this.itemOracleDPView.getHomeButtonRef().addActionListener(this);
        this.itemOracleDPView.getExitButtonRef().addActionListener(this);
                
        this.itemOracleDPView.getTotaleInviiTextRef().getDocument().addDocumentListener(this);
        this.itemOracleDPView.getInviiMazzettoTextRef().getDocument().addDocumentListener(this);
        this.itemOracleDPView.getNumeroMazzettiTextRef().getDocument().addDocumentListener(this);
        this.itemOracleDPView.getMailPieceLatitudeTextRef().getDocument().addDocumentListener(this);
        this.itemOracleDPView.getMailPieceLongitudeTextRef().getDocument().addDocumentListener(this);
                
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(itemOracleDPView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolItemOracleDPController actionPerformed invoke generaXmlData");
            generaXmlData();
        }

        if(source.equals(itemOracleDPView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolItemOracleDPController actionPerformed invoke impostaDatiWebService");
            impostaDatiWebService();
        }

        if(source.equals(itemOracleDPView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolItemOracleDPController actionPerformed invoke salvaXmlData");
            salvaXmlData();
        }

        if(source.equals(itemOracleDPView.getBackButtonRef()))
        {
            logger.debug("XteTestToolItemOracleDPController actionPerformed invoke back");
            back();
        }

        if(source.equals(itemOracleDPView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolItemOracleDPController actionPerformed invoke home");
            home();
        }

        if(source.equals(itemOracleDPView.getExitButtonRef()))
        {
            logger.debug("XteTestToolItemOracleDPController actionPerformed invoke exit");
            exit();
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolItemOracleDPView)
        {
            logger.debug("XteTestToolItemOracleDPController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolItemOracleDPController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Item Oracle Data Posta controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolItemOracleDPController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Item Oracle Data Posta controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }    
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object source = e.getSource();
        if(source.equals(itemOracleDPView.getInviiCausaleTableRef().getSelectionModel()))
        {               
            if (e.getValueIsAdjusting()) {
                    return;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
                        currentInviiCausaleTableSelectedRow = itemOracleDPView.getInviiCausaleTableRef().getSelectedRow();
                        logger.debug("XteTestToolItemOracleDPController valueChanged currentSelectedRow = " + currentInviiCausaleTableSelectedRow);
                        logger.debug("XteTestToolItemOracleDPController valueChanged: " + ((XteInviiCausaleTableModel)itemOracleDPView.getInviiCausaleTableRef().getModel()).getMap().size());
                        if(currentInviiCausaleTableSelectedRow!=-1)
                        {
                            selectedInvioCausale = ((XteInviiCausaleTableModel)itemOracleDPView.getInviiCausaleTableRef().getModel()).getSelectedItem(currentInviiCausaleTableSelectedRow);
                        }
                        else
                        {
                            selectedInvioCausale = null;
                        }
                    }
                } );
        }
        
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int eventType = e.getType();
        DefaultTableModel model = (DefaultTableModel) e.getSource();
        if(model.equals(itemOracleDPView.getInviiCausaleTableRef().getModel()))
        {               
            logger.debug("XteTestToolItemOracleDPController tableChanged: " + ((XteInviiCausaleTableModel)itemOracleDPView.getInviiCausaleTableRef().getModel()).getMap().size());
        }
    }

    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        itemOracleDPView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolItemOracleDPController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.itemOracleDPView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(itemOracleDPView);
            updateXteTestToolDataModel(dataModel);

            clearMessageBar();

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(itemOracleDPView.getImpostaDatiWSButtonRef());
            disabledComponents.add(itemOracleDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            XteInviiCausale item1 = new XteInviiCausale();            
            item1.setCausale("DP1");
            item1.setDescrizione("RECAPITATO");
            item1.setNumeroInvii(new Long(0));

            XteInviiCausale item2 = new XteInviiCausale();            
            item2.setCausale("DP2");
            item2.setDescrizione("DESTINATARIO SCONOSCIUTO");
            item2.setNumeroInvii(new Long(0));

            XteInviiCausale item3 = new XteInviiCausale();            
            item3.setCausale("DP3");
            item3.setDescrizione("DESTINATARIO TRASFERITO");
            item3.setNumeroInvii(new Long(0));

            XteInviiCausale item4 = new XteInviiCausale();            
            item4.setCausale("DP4");
            item4.setDescrizione("DESTINATARIO IRREPERIBILE");
            item4.setNumeroInvii(new Long(0));
                        
            this.itemOracleDPView.getInviiCausaleTableModelRef().addInviiCausaleItem(item1);
            this.itemOracleDPView.getInviiCausaleTableModelRef().addInviiCausaleItem(item2);
            this.itemOracleDPView.getInviiCausaleTableModelRef().addInviiCausaleItem(item3);
            this.itemOracleDPView.getInviiCausaleTableModelRef().addInviiCausaleItem(item4);
            
            itemOracleDPView.getBundleIdTextRef().requestFocus();

            logger.info("XteTestToolItemOracleDPController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolItemOracleDPController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolItemOracleDPController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Item Oracle Data Posta controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolItemOracleDPController start exit");
        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(itemOracleDPView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolItemOracleDPController exit in progress");
            itemOracleDPView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolItemOracleDPController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(itemOracleDPView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(itemOracleDPView.getItemOraclePanelRef().getParent(), itemOracleDPView.getItemOraclePanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolItemOracleDPController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(itemOracleDPView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(itemOracleDPView.getItemOraclePanelRef().getParent(), itemOracleDPView.getItemOraclePanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private void generaXmlData()
    {
        logger.info("XteTestToolItemOracleDPController start generaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            logger.info("XteTestToolItemOracleDPController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolItemOracleDPController generaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolItemOracleDPController generaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Item Oracle Data Posta controller generaXmlData failed", ex);
        }
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolItemOracleDPController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            logger.info("XteTestToolItemOracleDPController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolItemOracleDPController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolItemOracleDPController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Item Oracle Data Posta controller impostaDatiWebService failed", ex);
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolItemOracleDPController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            logger.info("XteTestToolItemOracleDPController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolItemOracleDPController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolItemOracleDPController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Item Oracle Data Posta controller salvaXmlData failed", ex);
        }
    }
}
