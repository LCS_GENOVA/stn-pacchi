package com.elsagdatamat.dbtrace.bridge.tocdg;

public class ToCDGToken {
	private String wsToken;
	protected String serviceId;
	public String getWsToken() {
		return wsToken;
	}
	public void setWsToken(String wsToken) {
		this.wsToken = wsToken;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
}
