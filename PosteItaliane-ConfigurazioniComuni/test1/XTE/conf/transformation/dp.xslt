<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"> 
<xsl:output indent="yes"/>
 <xsl:strip-space elements="*"/>
 <xsl:param name="channelID" />
 <xsl:param name="channel" />
 <xsl:param name="eventName" />
 <xsl:param name="serviceName" />
 <xsl:param name="version" />
 
 <xsl:template match="node()|@*" name="identity">
  <xsl:copy>
   <xsl:apply-templates select="node()|@*"/>
  </xsl:copy>
 </xsl:template>
 
 <xsl:template match="Traccia">
	<Traccia>
	<xsl:attribute name="channelID">
		<xsl:value-of select="$channelID"/>
	</xsl:attribute>
	<xsl:attribute name="channel">
		<xsl:value-of select="$channel"/>
	</xsl:attribute>
	<xsl:attribute name="eventName">
		<xsl:value-of select="$eventName"/>
	</xsl:attribute>
	<xsl:attribute name="serviceName">
		<xsl:value-of select="$serviceName"/>
	</xsl:attribute>
	<xsl:attribute name="version">
		<xsl:value-of select="$version"/>
	</xsl:attribute>		
	<xsl:apply-templates select="node()|@*"/>
	</Traccia>
 </xsl:template>


 
</xsl:stylesheet>
