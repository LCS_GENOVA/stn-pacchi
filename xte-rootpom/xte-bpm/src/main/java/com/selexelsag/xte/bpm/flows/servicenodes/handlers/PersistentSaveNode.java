package com.selexelsag.xte.bpm.flows.servicenodes.handlers;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.drools.process.instance.WorkItemHandler;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemManager;

import com.selexelsag.xte.bpm.model.ValidationResults;
import com.selexelsag.xte.business.tracking.IPersistentAccess;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.service.context.SpringApplicationContext;

public class PersistentSaveNode implements WorkItemHandler {

	private Logger logger = LogManager.getLogger(PersistentSaveNode.class);
	
	@Override
	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
		
	}

	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
	
		logger.debug("executeWorkItem START");
		
		Map<String, Object> results = null;

		StringBuilder data = (StringBuilder)workItem.getParameter("data");
		if (data==null) {
			logger.debug("data parameter is null!");
		}
		
		FlowParams params = (FlowParams)workItem.getParameter("flowParams");
		if (params==null) {
			logger.debug("flowParams parameter is null!");
		}
		
		ValidationResults valres = (ValidationResults)workItem.getParameter("validationResults");
		if (valres==null) {
			logger.debug("valres parameter is null!");
		}
		
		try{
 			results = new HashMap<String, Object>();
 			IPersistentAccess paccess = (IPersistentAccess)SpringApplicationContext.
					getBean(IPersistentAccess.PERSISTENT_ACCESS_BEAN_NAME);

 			paccess.save(params, valres.getDataExtract(), data);
 			
		}catch (Throwable e){	
			logger.error(e, e);
			results = new HashMap<String, Object>();
			results.put("error", e);
			
		}finally{
			
			// logger.debug("executeWorkItem "+ getClass().getName() +" END, result is " + results.get("semanticValidationSuccess"));	
			manager.completeWorkItem(workItem.getId(), results);
			logger.debug("executeWorkItem END");
		}
	}

}
