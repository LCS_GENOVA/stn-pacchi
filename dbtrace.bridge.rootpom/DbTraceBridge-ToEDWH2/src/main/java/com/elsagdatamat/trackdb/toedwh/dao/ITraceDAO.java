package com.elsagdatamat.trackdb.toedwh.dao;

import java.util.List;

import com.elsagdatamat.trackdb.toedwh.entities.Trace;
import com.elsagdatamat.trackdb.toedwh.vo.TraceVO;
import com.elsagdatamat.tt.trackdbws.beans.VO.EsiPackVO;

public interface ITraceDAO {

	public List<com.elsagdatamat.tt.trackdbws.beans.Trace> returnTraceListExecuteHqlQuery(String hqlQuery);

	public List<EsiPackVO> getListOfEsiPackVO(String hqlQuery);

	public Trace merge(com.elsagdatamat.trackdb.toedwh.entities.Trace entity);

	public List<TraceVO> executeQuery(String hqlQuery);

	public Long getId(String hqlQuery);

	public int bulkDeleteTraces(Integer timeHorizon, String traceType);

	public int deleteTraces(Integer timeHorizon);

	List<TraceVO> executeTraceQuery(String hqlQuery);
}