package com.elsagdatamat.dbtrace.bridge.engine;

import java.io.IOException;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface IMessageSender {

	@Transactional(value = "dbTraceBridge-transactionManager", propagation = Propagation.REQUIRES_NEW)
	public void send(List<String> s) throws IOException;


	public void commitWork();
}
