/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.manager.token;

import java.io.Serializable;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.elsagdatamat.dbtrace.bridge.engine.entities.Token;


/**
 * @author arodriguez
 *
 */
public class OMPTokenDecorator extends ToToken implements Serializable {

	private ToToken decoratedOMPToken = null;
	
	/**
	 * hided default constructor
	 */
	private OMPTokenDecorator() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param decoratedOMPTokn
	 * @param token
	 */
	public OMPTokenDecorator(ToToken decoratedOMPToken, Token token) {
		this.decoratedOMPToken = decoratedOMPToken;
		this.decoratedOMPToken.setServiceId(token.getServiceId());
		this.decoratedOMPToken.setWsToken(token.getWsToken());
	}

	/**
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.manager.token.OMPToken#getWsToken()
	 */
	public String getWsToken() {
		return decoratedOMPToken.getWsToken();
	}

	/**
	 * @param wsToken
	 * @see com.selexelsag.dbtrace.bridge.to.omp.manager.token.OMPToken#setWsToken(java.lang.String)
	 */
	public void setWsToken(String wsToken) {
		decoratedOMPToken.setWsToken(wsToken);
	}

	/**
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.omp.manager.token.OMPToken#getServiceId()
	 */
	public String getServiceId() {
		return decoratedOMPToken.getServiceId();
	}

	/**
	 * @param serviceId
	 * @see com.selexelsag.dbtrace.bridge.to.omp.manager.token.OMPToken#setServiceId(java.lang.String)
	 */
	public void setServiceId(String serviceId) {
		decoratedOMPToken.setServiceId(serviceId);
	}

}
