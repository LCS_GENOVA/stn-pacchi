package com.selexelsag.tt.cruscotto.pacchi.dao;

import com.elsagdatamat.framework.dao.IGenericDAO;
import com.selexelsag.tt.cruscotto.pacchi.bean.PkConProdotto;
import com.selexelsag.tt.cruscotto.pacchi.bean.PkConProdottoPK;

public interface IPkConProdottoDAO extends IGenericDAO<PkConProdotto, PkConProdottoPK>{

}
