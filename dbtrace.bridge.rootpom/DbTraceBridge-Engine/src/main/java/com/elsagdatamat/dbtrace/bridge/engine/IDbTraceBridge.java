package com.elsagdatamat.dbtrace.bridge.engine;

import com.elsagdatamat.dbtrace.bridge.engine.exceptions.InsertTracesException;
import com.elsagdatamat.dbtrace.bridge.engine.exceptions.TrackDBRWException;

public interface IDbTraceBridge<T> {
	
	void insertTraces(T input) throws InsertTracesException, TrackDBRWException;
	
}
