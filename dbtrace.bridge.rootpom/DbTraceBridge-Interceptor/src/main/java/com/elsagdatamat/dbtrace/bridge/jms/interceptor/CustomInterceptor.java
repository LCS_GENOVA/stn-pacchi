package com.elsagdatamat.dbtrace.bridge.jms.interceptor;

import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;

/**
 * Interceptor custom che ha il compito di far caricare, all'EJB su cui e' applicato,
 * l'application context specificato dalla FactoryLocatorKey.
 * Tale chiave viene utilizzata per permettere all'EJB di caricare un contesto
 * "padre" prima di quello specifico per il progetto.
 * E' utilizzato in ambito di integrazione SPRING + J2EE per fare in modo di condividere
 * all'interno di un EAR le stesse librerie per i progetti in esso contenuti (WAR, MDB, EJB).
 * In questo modo l'EAR carica una volta sola i contesti per tutte le applicazioni che ne hanno bisogno.
 * Tipico esempio e' quello di condivisione delle librerie di servizio o di DAL.
 * In questo workspace il progetto contenente i servizi e': demo-services. 
 * 
 * Vedere link:
 * http://static.springsource.org/spring/docs/2.5.x/reference/ejb.html
 * 
 * @author Barisone Flavio
 *
 */
public class CustomInterceptor extends SpringBeanAutowiringInterceptor {

	/**
	 * Chiave che rappresenta l'id di un bean definito in un file di contesto 
	 * specifico il cui nome DEVE essere beanRefContext.xml. 
	 * Permette di caricare contesti di Spring differenti in funzione della chiave (non e' dinamico).
	 * 
	 * Link:
	 * http://techo-ecco.com/blog/spring-application-context-hierarchy-and-contextsingletonbeanfactorylocator/
	 */
	public final static String LOCATOR_CONTEXT_KEY = "mdb.context";
	
	@Override
	protected String getBeanFactoryLocatorKey(Object target) {
		
		String localtorKey = super.getBeanFactoryLocatorKey(target);
		String newLocaltorKey = LOCATOR_CONTEXT_KEY;
		return newLocaltorKey;
	}
}
