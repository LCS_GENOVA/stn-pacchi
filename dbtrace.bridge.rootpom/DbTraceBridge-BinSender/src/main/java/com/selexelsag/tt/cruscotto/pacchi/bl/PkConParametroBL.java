package com.selexelsag.tt.cruscotto.pacchi.bl;

import com.selexelsag.tt.cruscotto.pacchi.bean.PkConParametro;
import com.selexelsag.tt.cruscotto.pacchi.dao.IPkConParametroDAO;

public class PkConParametroBL implements IPkConParametroBL{

	IPkConParametroDAO pkConParametroDAO;

	@Override
	public PkConParametro getElemento(String pNome) {
		return pkConParametroDAO.findById(pNome, false);
	}

	public IPkConParametroDAO getPkConParametroDAO() {
		return pkConParametroDAO;
	}

	public void setPkConParametroDAO(IPkConParametroDAO pkConParametroDAO) {
		this.pkConParametroDAO = pkConParametroDAO;
	}

		
}
