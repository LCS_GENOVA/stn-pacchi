/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.common;

import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.plaf.basic.BasicScrollBarUI;

/**
 *
 * @author Da Procida
 */
public class XteTestToolScrollBarUI extends BasicScrollBarUI {

    public static int VERTICAL_SCROLLBAR = 1;
    public static int HORIZONTAL_SCROLLBAR = 2;
    private int scrollbarOrientation;
    private Dimension scrollbarButtonDimension;

    public static final String VERTICAL_SCROLLBAR_INCR_BUTTON = "VERTICAL_SCROLLBAR_INCR_BUTTON";
    public static final String VERTICAL_SCROLLBAR_DECR_BUTTON = "VERTICAL_SCROLLBAR_DECR_BUTTON";
    public static final String HORIZONTAL_SCROLLBAR_INCR_BUTTON = "HORIZONTAL_SCROLLBAR_INCR_BUTTON";
    public static final String HORIZONTAL_SCROLLBAR_DECR_BUTTON = "HORIZONTAL_SCROLLBAR_DECR_BUTTON";

    public XteTestToolScrollBarUI(int scrollbarOrientation, Dimension scrollbarButtonDimension) {
        this.scrollbarOrientation = scrollbarOrientation;
        this.scrollbarButtonDimension = scrollbarButtonDimension;
    }
    
    @Override
    protected JButton createIncreaseButton(int orientation) {
        ImageIcon infoIcon;

        if (scrollbarOrientation == VERTICAL_SCROLLBAR) {
            infoIcon = new ImageIcon(getClass().getResource("/images/Scrollbar_ArrowDown.png"));
        } else {
            infoIcon = new ImageIcon(getClass().getResource("/images/Scrollbar_ArrowRight.png"));
        }
        JButton button = createScrollBarButton(infoIcon);
        return button;
    }

    public JButton getIncrButton() {
        if (scrollbarOrientation == VERTICAL_SCROLLBAR)
            incrButton.setName(VERTICAL_SCROLLBAR_INCR_BUTTON);
        else
            incrButton.setName(HORIZONTAL_SCROLLBAR_INCR_BUTTON);

        return incrButton;
    }

    public JButton getDecrButton() {
        if (scrollbarOrientation == VERTICAL_SCROLLBAR)
            decrButton.setName(VERTICAL_SCROLLBAR_DECR_BUTTON);
        else
            decrButton.setName(HORIZONTAL_SCROLLBAR_DECR_BUTTON);

        return decrButton;
    }
    
    @Override
    protected JButton createDecreaseButton(int orientation) {
        ImageIcon infoIcon;

        if (scrollbarOrientation == VERTICAL_SCROLLBAR) {
            infoIcon = new ImageIcon(getClass().getResource("/images/Scrollbar_ArrowUp.png"));
        } else {
            infoIcon = new ImageIcon(getClass().getResource("/images/Scrollbar_ArrowLeft.png"));
        }
        JButton button = createScrollBarButton(infoIcon);
        return button;
    }

    private JButton createScrollBarButton(ImageIcon infoIcon) {
        JButton button = new JButton("");
        if(this.scrollbarButtonDimension!= null)
        {
            button.setPreferredSize(scrollbarButtonDimension);
            button.setMinimumSize(scrollbarButtonDimension);
            button.setMaximumSize(scrollbarButtonDimension);
        }

        button.setIcon(infoIcon);
        button.setFocusable(false);
        button.setFocusPainted(false);
        button.setHorizontalAlignment(SwingConstants.CENTER);
        button.setVerticalAlignment(SwingConstants.CENTER);
        button.setHorizontalTextPosition(SwingConstants.TRAILING);
        return button;
    }

}

