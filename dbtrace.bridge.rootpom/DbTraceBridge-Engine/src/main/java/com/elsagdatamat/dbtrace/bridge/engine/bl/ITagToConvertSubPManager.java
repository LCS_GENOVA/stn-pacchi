package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.util.Map;

import com.elsagdatamat.dbtrace.bridge.engine.entities.TagToConvertSubP;

public interface ITagToConvertSubPManager extends IManager<TagToConvertSubP, String> {

	public Map<String,String> getAllMapping();

}
