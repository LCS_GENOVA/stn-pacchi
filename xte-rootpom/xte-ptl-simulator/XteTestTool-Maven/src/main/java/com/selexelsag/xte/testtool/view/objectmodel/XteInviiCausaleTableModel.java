/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectmodel;

 
import com.selexelsag.xte.testtool.datamodel.XteInviiCausale;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Da Procida
 */
public class XteInviiCausaleTableModel extends DefaultTableModel {

    private String[] columnNames = new String[]{
        "",
        ResourceBundle.getBundle("bundles/messages").getString("inviicausale.table.header.causale"),
        ResourceBundle.getBundle("bundles/messages").getString("inviicausale.table.header.descrizione"),
        ResourceBundle.getBundle("bundles/messages").getString("inviicausale.table.header.invii"),
    };

    
    private Map<String, XteInviiCausale> map = new HashMap<String, XteInviiCausale>();
    
    public XteInviiCausaleTableModel() {

        super();
        this.setColumnCount(columnNames.length);
        this.setColumnIdentifiers(columnNames);

    }

    public void addInviiCausaleItem(XteInviiCausale item) throws XteTestToolDuplicateKeyException{
        if(map.containsKey(item.getCausale()))
        {
            throw new XteTestToolDuplicateKeyException(Group.GENERIC, Severity.WARNING, "Invii Causale Table Model duplicate key exception");
        }
        else
        {
            addRow(new Object[]{"", item.getCausale(), item.getDescrizione(), Long.toString(item.getNumeroInvii())});
            map.put(item.getCausale(), item);            
        }        
    }
    

    public void setModelData(Map<String, XteInviiCausale> items) {
        getDataVector().clear();
        map.clear();                
        for (XteInviiCausale item : items.values()) {
            addRow(new Object[]{"", item.getCausale(), item.getDescrizione(), Long.toString(item.getNumeroInvii())});
            map.put(item.getCausale(), item);            
        }
    }

    public XteInviiCausale getSelectedItem(int rowIndex) {
        
        String causale = (String) getValueAt(rowIndex, 0);
        if(map.containsKey(causale))
        {
            return map.get(causale);
        }
        else
        {
            return null;
        }
    }

    public void clearModel() {
        getDataVector().clear();
        map.clear();
    }

    public Map<String, XteInviiCausale> getMap() {
        return map;
    }

    
    @Override
    public boolean isCellEditable(int row, int column) {
        return (false);
    }
    
}
