package com.selexelsag.xtetesttool.xml.dataposta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AM">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="NM">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LM">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PR">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SM">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                   &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "am",
    "nm",
    "ao",
    "lm",
    "pr",
    "sm"
})
@XmlRootElement(name = "DataPosta")
public class DataPosta {

    @XmlElement(name = "AM", required = true)
    protected DataPosta.AM am;
    @XmlElement(name = "NM", required = true)
    protected DataPosta.NM nm;
    @XmlElement(name = "AO", required = true)
    protected DataPosta.AO ao;
    @XmlElement(name = "LM", required = true)
    protected DataPosta.LM lm;
    @XmlElement(name = "PR", required = true)
    protected DataPosta.PR pr;
    @XmlElement(name = "SM", required = true)
    protected DataPosta.SM sm;

    /**
     * Gets the value of the am property.
     * 
     * @return
     *     possible object is
     *     {@link DataPosta.AM }
     *     
     */
    public DataPosta.AM getAM() {
        return am;
    }

    /**
     * Sets the value of the am property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataPosta.AM }
     *     
     */
    public void setAM(DataPosta.AM value) {
        this.am = value;
    }

    /**
     * Gets the value of the nm property.
     * 
     * @return
     *     possible object is
     *     {@link DataPosta.NM }
     *     
     */
    public DataPosta.NM getNM() {
        return nm;
    }

    /**
     * Sets the value of the nm property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataPosta.NM }
     *     
     */
    public void setNM(DataPosta.NM value) {
        this.nm = value;
    }
    
    /**
     * Gets the value of the ao property.
     * 
     * @return
     *     possible object is
     *     {@link DataPosta.AO }
     *     
     */
    public DataPosta.AO getAO() {
        return ao;
    }

    /**
     * Sets the value of the ao property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataPosta.AO }
     *     
     */
    public void setAO(DataPosta.AO value) {
        this.ao = value;
    }

    /**
     * Gets the value of the lm property.
     * 
     * @return
     *     possible object is
     *     {@link DataPosta.LM }
     *     
     */
    public DataPosta.LM getLM() {
        return lm;
    }

    /**
     * Sets the value of the lm property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataPosta.LM }
     *     
     */
    public void setLM(DataPosta.LM value) {
        this.lm = value;
    }

    /**
     * Gets the value of the pr property.
     * 
     * @return
     *     possible object is
     *     {@link DataPosta.PR }
     *     
     */
    public DataPosta.PR getPR() {
        return pr;
    }

    /**
     * Sets the value of the pr property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataPosta.PR }
     *     
     */
    public void setPR(DataPosta.PR value) {
        this.pr = value;
    }

    /**
     * Gets the value of the sm property.
     * 
     * @return
     *     possible object is
     *     {@link DataPosta.SM }
     *     
     */
    public DataPosta.SM getSM() {
        return sm;
    }

    /**
     * Sets the value of the sm property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataPosta.SM }
     *     
     */
    public void setSM(DataPosta.SM value) {
        this.sm = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class AM {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class LM {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class NM {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }
    
    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class AO {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class PR {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EventNameTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *         &lt;element name="XsdFileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "service",
        "channel",
        "eventName",
        "eventNameTag",
        "version",
        "xsdFileName"
    })
    public static class SM {

        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Channel", required = true)
        protected String channel;
        @XmlElement(name = "EventName", required = true)
        protected String eventName;
        @XmlElement(name = "EventNameTag", required = true)
        protected String eventNameTag;
        @XmlElement(name = "Version")
        protected float version;
        @XmlElement(name = "XsdFileName", required = true)
        protected String xsdFileName;

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the channel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannel() {
            return channel;
        }

        /**
         * Sets the value of the channel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannel(String value) {
            this.channel = value;
        }

        /**
         * Gets the value of the eventName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventName() {
            return eventName;
        }

        /**
         * Sets the value of the eventName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventName(String value) {
            this.eventName = value;
        }

        /**
         * Gets the value of the eventNameTag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEventNameTag() {
            return eventNameTag;
        }

        /**
         * Sets the value of the eventNameTag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEventNameTag(String value) {
            this.eventNameTag = value;
        }

        /**
         * Gets the value of the version property.
         * 
         */
        public float getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         */
        public void setVersion(float value) {
            this.version = value;
        }

        /**
         * Gets the value of the xsdFileName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXsdFileName() {
            return xsdFileName;
        }

        /**
         * Sets the value of the xsdFileName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXsdFileName(String value) {
            this.xsdFileName = value;
        }

    }

}
