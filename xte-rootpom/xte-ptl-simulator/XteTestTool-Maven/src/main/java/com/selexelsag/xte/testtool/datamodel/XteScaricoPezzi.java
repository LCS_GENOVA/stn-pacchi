/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.datamodel;

import java.math.BigInteger;
import java.util.Date;


/**
 *
 * @author Tassara
 */
public class XteScaricoPezzi {

    private String ScaricoPezziCode;
    private String ScaricoPezziProduct;
    private Date ScaricoPezziDateTime;
    private String ScaricoPezziPeso;
    private int ScaricoPezziPrezzo;
    private int ScaricoPezziPercetto;
    private String ScaricoPezziDestinazione;
    private String ScaricoPezziDestinatario;
    private String ScaricoPezziIndirizzo;
    private String ScaricoPezziCAP;
    private String ScaricoPezziNomeMitt;
    private String ScaricoPezziDestMitt;
    private String ScaricoPezziIndMitt;
    private String ScaricoPezziCAPMitt;
    private String ScaricoPezziSA;
    private int ScaricoPezziValAss;
    private int ScaricoPezziImpContr;
    private String ScaricoPezziCodAR;
    private String ScaricoPezziIBAN;
    private String ScaricoPezziOraTimeDef;
    private String ScaricoPezziSmarrito;


    public String getScaricoPezziCode() {
        return ScaricoPezziCode;
    }

    public void setScaricoPezziCode(String ScaricoPezziCode) {
        this.ScaricoPezziCode = ScaricoPezziCode;
    }
    
    public String getScaricoPezziProduct() {
        return ScaricoPezziProduct;
    }

    public void setScaricoPezziProduct(String ScaricoPezziProduct) {
        this.ScaricoPezziProduct = ScaricoPezziProduct;
    }  
    
    public Date getScaricoPezziDateTime() {
        return ScaricoPezziDateTime;
    }

    public void setScaricoPezziDateTime(Date ScaricoPezziDateTime) {
        this.ScaricoPezziDateTime = ScaricoPezziDateTime;
    }
    
    
    public String getScaricoPezziPeso() {
        return ScaricoPezziPeso;
    }

    public void setScaricoPezziPeso(String ScaricoPezziPeso) {
        this.ScaricoPezziPeso = ScaricoPezziPeso;
    }  
    
    public int getScaricoPezziPrezzo() {
        return ScaricoPezziPrezzo;
    }

    public void setScaricoPezziPrezzo(int ScaricoPezziPrezzo) {
        this.ScaricoPezziPrezzo = ScaricoPezziPrezzo;
    }  
    
    public int getScaricoPezziPercetto() {
        return ScaricoPezziPercetto;
    }

    public void setScaricoPezziPercetto(int ScaricoPezziPercetto) {
        this.ScaricoPezziPercetto = ScaricoPezziPercetto;
    }  
    
    public String getScaricoPezziDestinazione() {
        return ScaricoPezziDestinazione;
    }

    public void setScaricoPezziDestinazione(String ScaricoPezziDestinazione) {
        this.ScaricoPezziDestinazione = ScaricoPezziDestinazione;
    }  
    
    public String getScaricoPezziDestinatario() {
        return ScaricoPezziDestinatario;
    }

    public void setScaricoPezziDestinatario(String ScaricoPezziDestinatario) {
        this.ScaricoPezziDestinatario = ScaricoPezziDestinatario;
    }  
    
    public String getScaricoPezziIndirizzo() {
        return ScaricoPezziIndirizzo;
    }

    public void setScaricoPezziIndirizzo(String ScaricoPezziIndirizzo) {
        this.ScaricoPezziIndirizzo = ScaricoPezziIndirizzo;
    }  
    
    public String getScaricoPezziCAP() {
        return ScaricoPezziCAP;
    }

    public void setScaricoPezziCAP(String ScaricoPezziCAP) {
        this.ScaricoPezziCAP = ScaricoPezziCAP;
    }  
    
    public String getScaricoPezziNomeMitt() {
        return ScaricoPezziNomeMitt;
    }

    public void setScaricoPezziNomeMitt(String ScaricoPezziNomeMitt) {
        this.ScaricoPezziNomeMitt = ScaricoPezziNomeMitt;
    }  
    
    public String getScaricoPezziDestMitt() {
        return ScaricoPezziDestMitt;
    }

    public void setScaricoPezziDestMitt(String ScaricoPezziDestMitt) {
        this.ScaricoPezziDestMitt = ScaricoPezziDestMitt;
    }  
    
    public String getScaricoPezziIndMitt() {
        return ScaricoPezziIndMitt;
    }

    public void setScaricoPezziIndMitt(String ScaricoPezziIndMitt) {
        this.ScaricoPezziIndMitt = ScaricoPezziIndMitt;
    }  
    
    public String getScaricoPezziCAPMitt() {
        return ScaricoPezziCAPMitt;
    }

    public void setScaricoPezziCAPMitt(String ScaricoPezziCAPMitt) {
        this.ScaricoPezziCAPMitt = ScaricoPezziCAPMitt;
    }  
    
    public String getScaricoPezziSA() {
        return ScaricoPezziSA;
    }

    public void setScaricoPezziSA(String ScaricoPezziSA) {
        this.ScaricoPezziSA = ScaricoPezziSA;
    }  
    
    public int getScaricoPezziValAss() {
        return ScaricoPezziValAss;
    }

    public void setScaricoPezziValAss(int ScaricoPezziValAss) {
        this.ScaricoPezziValAss = ScaricoPezziValAss;
    }  
    public int getScaricoPezziImpContr() {
        return ScaricoPezziImpContr;
    }

    public void setScaricoPezziImpContr(int ScaricoPezziImpContr) {
        this.ScaricoPezziImpContr = ScaricoPezziImpContr;
    } 
    public String getScaricoPezziCodAR() {
        return ScaricoPezziCodAR;
    }

    public void setScaricoPezziCodAR(String ScaricoPezziCodAR) {
        this.ScaricoPezziCodAR = ScaricoPezziCodAR;
    } 
    public String getScaricoPezziIBAN() {
        return ScaricoPezziIBAN;
    }

    public void setScaricoPezziIBAN(String ScaricoPezziIBAN) {
        this.ScaricoPezziIBAN = ScaricoPezziIBAN;
    } 
    
    public String getScaricoPezziOraTimeDef() {
        return ScaricoPezziOraTimeDef;
    }

    public void setScaricoPezziOraTimeDef(String ScaricoPezziOraTimeDef) {
        this.ScaricoPezziOraTimeDef = ScaricoPezziOraTimeDef;
    } 
    public String getScaricoPezziSmarrito() {
        return ScaricoPezziSmarrito;
    }

    public void setScaricoPezziSmarrito(String ScaricoPezziSmarrito) {
        this.ScaricoPezziSmarrito = ScaricoPezziSmarrito;
    } 


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.ScaricoPezziCode != null ? this.ScaricoPezziCode.hashCode() : 0);
        hash = 97 * hash + (this.ScaricoPezziDateTime != null ? this.ScaricoPezziDateTime.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final XteScaricoPezzi other = (XteScaricoPezzi) obj;
        if ((this.ScaricoPezziCode == null) ? (other.ScaricoPezziCode != null) : !this.ScaricoPezziCode.equals(other.ScaricoPezziCode)) {
            return false;
        }
        if ((this.ScaricoPezziDateTime == null) ? (other.ScaricoPezziDateTime != null) : !this.ScaricoPezziDateTime.equals(other.ScaricoPezziDateTime)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "XteScaricoPezziPR{" + "ScaricoPezziCode=" + ScaricoPezziCode + ", ScaricoPezziDateTime=" + ScaricoPezziDateTime + '}';
    }
    

}
