package com.selexes.tt.tool.recupero.messaggi.manager;

import java.util.List;

import com.elsagdatamat.dbtrace.bridge.engine.bl.DiscardedTraceManager;
import com.elsagdatamat.dbtrace.bridge.engine.entities.DiscardedTrace;

public class DiscardedTraceManagerExtesion extends DiscardedTraceManager implements IDiscardedTraceManagerExtesion{
	
	@Override
	public List<Object> getMessages(String discardedProcess, String fromDate, String toDate){
		String hqlQuery = "SELECT dt.id FROM DiscardedTrace dt" +
						  " WHERE dt.insertDate > trunc(to_date('"+fromDate+"', 'yyyy/mm/dd'))" +
						  " and dt.insertDate < trunc(to_date('"+toDate+"', 'yyyy/mm/dd'))" +
						  " and dt.discardingProcess = '"+discardedProcess+"'"+
						  " and dt.discardingReason not like '%OK%'";
		
		return dao.executeHqlQuery(hqlQuery);
	}
	
	@Override
	public DiscardedTrace getMessagesById(Long id){
		return dao.findById(id, false);
	}

}
