package com.elsagdatamat.dbtrace.bridge.toedwh;

public class EDWHFatturazioni {

	public String letteraDiVettura="";
	public String idRitiro="";
	public String dataRitiro="";
	public String codiceSap="";
//	public String partitaIva="";
//	public String codiceFiscale="";
	public String partitaIvaOCodiceFiscale="";
	public String cap="";
	public String via="";
	public String localita="";
	public String provincia="";
	public String numeroLetteraDiVetturaDa="";
	public String numeroLetteraDiVetturaA="";
	public String tipoCliente="";
	public String importoSpedizione="";
	public String importoRitiro="";
	public String importoTd="";
	public String flagServizioCa="";
	public String flagServizioAss="";
	public String codiceCre="";
	public String codiceFiliale="";
	public String accountQ="";
	public String accountP="";
	public String accountServAccCAssegnoCC="";
	public String accountServAccAssicurazione=""; 
	public String codiceProdotto="";
	public String codiceCausaleDiFatturazione="";
	public String dataFatturazione="";
	public String codiceUfficioPostale=""; 
	public String tariffa=""; 
	 
	public String toCsv()
	{
		return
			"\""+ ((letteraDiVettura!=null)?letteraDiVettura:"")+"\","+
			"\""+ ((idRitiro!=null)?idRitiro:"")+"\","+
			"\""+ ((dataRitiro!=null)?dataRitiro:"")+"\","+
			"\""+ ((codiceSap!=null)?codiceSap:"")+"\","+
//			"\""+ ((partitaIva!=null)?partitaIva:"")+"\","+
//			"\""+ ((codiceFiscale!=null)?codiceFiscale:"")+"\","+
			"\""+ ((partitaIvaOCodiceFiscale!=null)?partitaIvaOCodiceFiscale:"")+"\","+
			"\""+ ((cap!=null)?cap:"")+"\","+
			"\""+ ((via!=null)?via:"")+"\","+
			"\""+ ((localita!=null)?localita:"")+"\","+
			"\""+ ((provincia!=null)?provincia:"")+"\","+
			"\""+ ((numeroLetteraDiVetturaDa!=null)?numeroLetteraDiVetturaDa:"")+"\","+
			"\""+ ((numeroLetteraDiVetturaA!=null)?numeroLetteraDiVetturaA:"")+"\","+
			"\""+ ((tipoCliente!=null)?tipoCliente:"")+"\","+
			"\""+ ((importoSpedizione!=null)?importoSpedizione:"")+"\","+
			"\""+ ((importoRitiro!=null)?importoRitiro:"")+"\","+
			"\""+ ((importoTd!=null)?importoTd:"")+"\","+
			"\""+ ((flagServizioCa!=null)?flagServizioCa:"")+"\","+
			"\""+ ((flagServizioAss!=null)?flagServizioAss:"")+"\","+
			"\""+ ((codiceCre!=null)?codiceCre:"")+"\","+
			"\""+ ((codiceFiliale!=null)?codiceFiliale:"")+"\","+
			"\""+ ((accountQ!=null)?accountQ:"")+"\","+
			"\""+ ((accountP!=null)?accountP:"")+"\","+
			"\""+ ((accountServAccCAssegnoCC!=null)?accountServAccCAssegnoCC:"")+"\","+
			"\""+ ((accountServAccAssicurazione!=null)?accountServAccAssicurazione:"")+"\","+ 
			"\""+ ((codiceProdotto!=null)?codiceProdotto:"")+"\","+
			"\""+ ((codiceCausaleDiFatturazione!=null)?codiceCausaleDiFatturazione:"")+"\","+
			"\""+ ((dataFatturazione!=null)?dataFatturazione:"")+"\","+
			"\""+ ((codiceUfficioPostale!=null)?codiceUfficioPostale:"")+"\","+ 
			"\""+ ((tariffa!=null)?tariffa:"")+"\"\n";
	}

}
