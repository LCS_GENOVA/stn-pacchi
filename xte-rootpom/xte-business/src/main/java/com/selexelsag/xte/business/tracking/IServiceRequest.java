package com.selexelsag.xte.business.tracking;

import com.selexelsag.xte.business.exceptions.BusinessServiceRequestException;
import com.selexelsag.xte.model.flows.FlowParams;

/**
 * 
 * @author Motta
 * Effettua la chiamata al servizio http.
 * TODO: gestione delle eccezioni
 */
public interface IServiceRequest {
	
	public static final String SERVICE_REQUEST_BEAN_NAME = "baseServiceRequest";
	
	public static final String CONNECTION_TIMEOUT="com.selexelsag.xte.business.tracking.resources.service.request.connection_timeout";
	
	public static final String RESPONSE_TIMEOUT="com.selexelsag.xte.business.tracking.resources.service.request.response_timeout";
	
	/** inoltra una chiamata http con i parametri specificati, e 
	 * restituisce il messaggio di risposta.
	 * @param flowParams parametri specifici al flusso.
	 * @param data i dati da inserire nella chiamata.
	 * @param connectionTimeout timeout di connessione
	 * @param requestTimeout timeout per la risposta
	 * @return i dati ricevuti in risposta dal servizio http.
	 */
	StringBuilder getHttpResponse(FlowParams flowParams, StringBuilder data, int connectionTimeout, int requestTimeout) throws BusinessServiceRequestException;
	
	/** inoltra una chiamata http con i parametri specificati, e 
	 * restituisce il messaggio di risposta.
	 * @param flowParams parametri specifici al flusso.
	 * @param data i dati da inserire nella chiamata.
	 * @return i dati ricevuti in risposta dal servizio http.
	 */
	StringBuilder getHttpResponse(FlowParams flowParams, StringBuilder data) throws BusinessServiceRequestException;
	
}
