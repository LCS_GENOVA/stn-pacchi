package com.elsagdatamat.dbtrace.bridge.engine.dao;

import org.apache.log4j.MDC;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionStatus;



public class MdcTransactionManager extends JpaTransactionManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1002609461012492403L;

	//private Log log = LogFactory.getLog(this.getClass());
	
	private static final String TID = "transactionId";
	
	@Override
	protected void doBegin(Object transaction, TransactionDefinition definition) {
		super.doBegin(transaction, definition);
		/*
		if (definition.getTimeout() != TransactionDefinition.TIMEOUT_DEFAULT) 
		{ 
			Object transactionData = new BeanWrapperImpl(transaction).getPropertyValue("transactionData");
			Field sessionField = ReflectionUtils.findField(transactionData.getClass(), "session");
			ReflectionUtils.makeAccessible(sessionField);
			Object sessionObject = ReflectionUtils.getField(sessionField,transactionData);
			
			Session session = (Session) sessionObject;
			session.getTransaction().setTimeout( definition.getTimeout()); 
		}
		*/
		MDC.put(TID, getTransactionId(transaction));
		//logger.debug(String.format("@@@@@ doBegin: %s", transaction.toString()));
	}

	@Override
	protected void doResume(Object transaction, Object suspendedResources) {
		MDC.put(TID, getTransactionId(transaction));
		super.doResume(transaction, suspendedResources);
		//logger.debug(String.format("@@@@@ doResume: %s", transaction.toString()));
		
	}
	
	@Override
	protected Object doSuspend(Object transaction) {
		MDC.put(TID, "");
		//logger.debug(String.format("@@@@@ doSuspend: %s", transaction.toString()));
		return super.doSuspend(transaction);
	}

	@Override
	protected void doCommit(DefaultTransactionStatus status) {
		MDC.put(TID, "");
		super.doCommit(status);
		//logger.debug(String.format("@@@@@ doCommit: %s", status.toString()));
	}

	@Override
	protected void doRollback(DefaultTransactionStatus status) {
		MDC.put(TID, "");
		super.doRollback(status);
		//logger.debug(String.format("@@@@@ doRollback: %s", status.toString()));
	}
	
//	@Override
//	protected Object doGetTransaction() {
//		Object transaction =  super.doGetTransaction();
//		logger.debug(String.format("@@@@@ doGetTransaction: %s", transaction.toString()));
//		return transaction;
//	}
	
	private static String getTransactionId(Object transaction)
	{
		return ""+transaction.hashCode();
	}
}
