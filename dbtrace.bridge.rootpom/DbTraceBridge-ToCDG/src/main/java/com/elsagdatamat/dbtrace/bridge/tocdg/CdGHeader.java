package com.elsagdatamat.dbtrace.bridge.tocdg;

import javax.xml.bind.annotation.XmlAttribute;

public class CdGHeader {
/*
SWREL
OFCID
*/
	@XmlAttribute(name = "SWREL")
	public String swrel;
	
	@XmlAttribute(name = "OFCID")
	public String ofcid;
	
}
