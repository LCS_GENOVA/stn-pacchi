package com.selexelsag.xte.task;

import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.xte.business.exceptions.BusinessMessageProcessorException;
import com.selexelsag.xte.business.impl.MessageProcessorFactory;
import com.selexelsag.xte.business.tracking.IMessageDBManager;
import com.selexelsag.xte.model.message_processor.MessageProcessor;
import com.selexelsag.xte.persistence.XteMessagesDBAccess;
import com.selexelsag.xte.persistence.entities.AsyncMessages;
import com.selexelsag.xte.persistence.exceptions.PersistenceException;
import com.selexelsag.xte.service.system.IAppConfig;
 /***
  * 
  * @author Lanciano
  *
  */
public class JmsTask {
	
	private static Logger _log = Logger.getLogger(JmsTask.class);

	@Autowired
	private XteMessagesDBAccess managerDb;
	
	@Autowired
	private MessageProcessorFactory processorFactory;
	
	@Autowired
	private IAppConfig appConfig;
	
	@Autowired
	private IMessageDBManager messageManager;
	
	private String[] flowIds;
	
	@PostConstruct
	public void init() {
		flowIds = processorFactory.getManagedFlows();
	}
	
	public void processAll() {
		try {
			Properties prop = appConfig.getMessageProcessorProperties();
			Boolean processAllAvalailableRecord = Boolean.parseBoolean(prop.getProperty("MessageProcessor.processAllAvalailableRecord"));
			
			if (processAllAvalailableRecord) {
				processAllRecord();
			} else {
				processSomeRecord();
			}
		} catch (Exception e) {
			_log.error("Error processAll(): " + e.getMessage(), e);
		}
	}

	private void processSomeRecord() {
		try {
			for (int i=0;i<flowIds.length;i++) {
				_log.debug("Processing flowId " + flowIds[i]);
				int rowCount = processorFactory.getRowCount(flowIds[i]);
				
				List<AsyncMessages> xmlInputTrackList;
				try {
					xmlInputTrackList = managerDb.getMessages(flowIds[i], rowCount);
				} catch (PersistenceException e) {
					throw new RuntimeException("Error in db query for flowid " + flowIds[i], e);
				}

				if(xmlInputTrackList == null){
					_log.debug("List for flowId "+flowIds[i] + " is null, but we received no exception");
					return;
				}
				
				_log.info("FlowID " + flowIds[i] + ": found messages count " + xmlInputTrackList.size());
				
				MessageProcessor processor;
				try {
					processor = processorFactory.getMessageProcessor(flowIds[i]);
				} catch (BusinessMessageProcessorException e) {
					throw new RuntimeException("Configuration error: no processor defined for " + flowIds[i], e);
				}
				processor.setDBAccess(managerDb);
				processor.setMessageSender(messageManager);
				processor.setMessageProcessorProperties(appConfig.getMessageProcessorProperties());
				
				for (AsyncMessages message : xmlInputTrackList) {
					try {
						processor.processMessage(message);
					} catch (BusinessMessageProcessorException e) {
						_log.error("Error processing message " + message, e);
					} catch (Exception ex) {
						_log.error("Error processing message " + message, ex);
					}
				}
				_log.debug("Processed flowId " + flowIds[i]);
			}
		} catch (Exception e) {
			_log.error("Error processSomeRecord(): " + e.getMessage(), e);
		}
	}

	private void processAllRecord() {
		try {
			Boolean continua = true;
			_log.debug("Starting processAllRecord...");
			while (continua) {
				int[] msgList = new int[flowIds.length];
				for (int i=0;i<flowIds.length;i++) {
					_log.debug("Processing flowId " + flowIds[i]);
					int rowCount = processorFactory.getRowCount(flowIds[i]);
					
					List<AsyncMessages> xmlInputTrackList;
					try {
						xmlInputTrackList = managerDb.getMessages(flowIds[i], rowCount);
					} catch (PersistenceException e) {
						throw new RuntimeException("Error in db query for flowid " + flowIds[i], e);
					}

					if(xmlInputTrackList == null){
						msgList[i] = 0;
						_log.debug("List for flowId "+flowIds[i] + " is null, but we received no exception");
						break;
					}
					if(xmlInputTrackList.size() == 0){
						msgList[i] = 0;
						_log.debug("List for flowId "+flowIds[i] + " does not contains element");
						continue;
					}
					
					msgList[i] = xmlInputTrackList.size();
					
					_log.info("FlowID " + flowIds[i] + ": found messages count " + xmlInputTrackList.size());
					
					MessageProcessor processor;
					try {
						processor = processorFactory.getMessageProcessor(flowIds[i]);
					} catch (BusinessMessageProcessorException e) {
						throw new RuntimeException("Configuration error: no processor defined for " + flowIds[i], e);
					}
					processor.setDBAccess(managerDb);
					processor.setMessageSender(messageManager);
					processor.setMessageProcessorProperties(appConfig.getMessageProcessorProperties());
					
					for (AsyncMessages message : xmlInputTrackList) {
						try {
							processor.processMessage(message);
						} catch (BusinessMessageProcessorException e) {
							_log.error("Error processing message " + message, e);
						} catch (Exception ex) {
							_log.error("Error processing message " + message, ex);
						}
					}
					_log.debug("Processed flowId " + flowIds[i]);
				}
				
				Integer msgSizeSum = 0;
				for (int msgSize : msgList) {
					msgSizeSum += msgSize;
				}
				if (msgSizeSum.equals(0)) {
					_log.debug("No more messages to process...");
					continua = false;
				}
				else
				{
					_log.debug("Processing messages to be continued...");
				}
			}
			_log.debug("Leaving processAllRecord");
			
		} catch (Exception e) {
			_log.error("Error processAllRecord(): " + e.getMessage(), e);
		}
	}
}