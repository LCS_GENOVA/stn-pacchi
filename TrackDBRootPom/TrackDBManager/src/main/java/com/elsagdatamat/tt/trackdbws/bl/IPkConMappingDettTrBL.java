package com.elsagdatamat.tt.trackdbws.bl;

import com.selexelsag.tt.cruscottopacchi.bean.PkConMappingDettTr;


/** 
* R.L. BeanGenerator - 27.05.2012 18:17:43
*/ 
public interface IPkConMappingDettTrBL {

	public void insert(PkConMappingDettTr dettaglio);
}