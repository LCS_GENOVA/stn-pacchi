package com.elsagdatamat.dbtrace.bridge.tocdg;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "MSG")
public class CdGMessage {

	@XmlElement(name = "HDR")
	public CdGHeader header;
}
