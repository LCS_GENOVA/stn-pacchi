package com.elsagdatamat.dbtrace.bridge.engine.bl;

public enum WhatHappenedNotOnDBT {
	// R.L. - 26062014 - Tracce che vanno inseriti sul TT_APPLICATION.APT_ITEM e NON su DBT
	PRE_SDA;

	public static boolean contains(String value) {
	    for (WhatHappenedNotOnDBT c : WhatHappenedNotOnDBT.values()) {
	        if (c.name().equals(value)) {
	            return true;
	        }
	    }
	    return false;
	}
}
