package com.selexelsag.xte.business.impl;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.xte.business.exceptions.BusinessServiceRequestException;
import com.selexelsag.xte.business.tracking.IServiceRequest;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.service.system.IAppConfig;

public class BaseServiceRequest implements IServiceRequest {

	private Logger logger = LogManager.getLogger(BaseServiceRequest.class.getName());

	private static int defaultConnectionTimeout;

	private static int defaultRequestTimeout;

	private static boolean connectionParametersInitialized = false;

	// TODO: rimuovere, solo per il test
	// private static final String LOCAL_URL = null;
	private static final String LOCAL_URL = null; //"http://localhost:8081/XtePISimulator/EchoServlet";

	@Autowired
	private IAppConfig appConfig;

	public IAppConfig getAppConfig() {
		return appConfig;
	}

	public void setAppConfig(IAppConfig appConfig) {
		this.appConfig = appConfig;
	}

	public void initConnectionParams() {
		try {
			String t1 = appConfig.getGeneralParam(CONNECTION_TIMEOUT);
			defaultConnectionTimeout = Integer.parseInt(t1);
		}catch (NumberFormatException nfe) {
			logger.error("intero malformato per la chiave " + CONNECTION_TIMEOUT);
			defaultConnectionTimeout = -1;
		}
		try {
			defaultRequestTimeout = Integer.parseInt(appConfig.getGeneralParam(RESPONSE_TIMEOUT));
		}catch (NumberFormatException nfe) {
			logger.error("intero malformato per la chiave " + RESPONSE_TIMEOUT);
			defaultRequestTimeout = -1;
		}
	}

	private HttpUriRequest getPostRequest(FlowParams flowParams, StringBuilder data, int connectionTimeout, int requestTimeout)throws BusinessServiceRequestException {
		logger.debug("getPostRequest " + flowParams);

		// Prepare a request object
		HttpPost httppost = new HttpPost(flowParams.getHttpServiceUrl().toString());
		// httpget.setParams(params);
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		nvps.add(new BasicNameValuePair("XML", data.toString()));
		if (flowParams.getOffRefID()!=null && !flowParams.getOffRefID().isEmpty()) {
			nvps.add(new BasicNameValuePair("OFF_ID", flowParams.getOffRefID()));
		}

		try {
			httppost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
		} catch (UnsupportedEncodingException e1) {
			BusinessServiceRequestException bsre = new BusinessServiceRequestException("Exception in service request", e1);
			throw bsre;
		}

		return httppost;

	}

	private HttpUriRequest getGetRequest(FlowParams flowParams, StringBuilder data, int connectionTimeout, int requestTimeout)throws BusinessServiceRequestException {
		logger.debug("getGetRequest " + flowParams);

		List<NameValuePair> qparams = new ArrayList<NameValuePair>();
		qparams.add(new BasicNameValuePair("XML", data.toString()));
		if (flowParams.getOffRefID()!=null && !flowParams.getOffRefID().isEmpty()) {
			qparams.add(new BasicNameValuePair("OFF_ID", flowParams.getOffRefID()));
		}

		URI uri = null;
		try {
			uri = new URI(flowParams.getHttpServiceUrl().toString() + "?" + URLEncodedUtils.format(qparams, "UTF-8"));
			logger.debug("URL is " + uri);
		}catch (URISyntaxException use) {
			BusinessServiceRequestException bsre = new BusinessServiceRequestException("Exception in service request", use);
			throw bsre;
		}

		HttpGet httpget = new HttpGet(uri);

		return httpget;

	}

	@Override
	public StringBuilder getHttpResponse(FlowParams flowParams, StringBuilder data, int connectionTimeout, int requestTimeout)throws BusinessServiceRequestException {
		HttpClient httpclient = null;
		try {
			// TODO: capire dove e come settare le chiamate da fare in GET
			String callMethod = "POST";
	
			logger.debug("getHttpResponse(" + flowParams + ", " + data);
	
			if (!connectionParametersInitialized) {
				initConnectionParams();
			}
	
			httpclient = new DefaultHttpClient();
	
			HttpParams httpParams = httpclient.getParams();
	
			if (defaultConnectionTimeout < connectionTimeout) {
				HttpConnectionParams.setConnectionTimeout(httpParams, connectionTimeout);
			} else {
				HttpConnectionParams.setConnectionTimeout(httpParams, defaultConnectionTimeout);
			}
			if (defaultRequestTimeout < requestTimeout) {
				HttpConnectionParams.setConnectionTimeout(httpParams, requestTimeout);
			} else {
				HttpConnectionParams.setConnectionTimeout(httpParams, defaultRequestTimeout);
			}
	
			/* MOTTA */
			// TODO: capire come provare con i sistemi reali
			if (LOCAL_URL!=null) {
				logger.debug("ONLY FOR DEBUG: changing url " + flowParams.getHttpServiceUrl() + " whith " + LOCAL_URL);
				try {
					flowParams.setHttpServiceUrl(new URL(LOCAL_URL));
				} catch (MalformedURLException e) {
					logger.error("url " + LOCAL_URL + " malformed");
				}
			}
	
			HttpUriRequest request = null; 
			if (callMethod.equals("POST")) {
				request = getPostRequest(flowParams, data, connectionTimeout, requestTimeout);
			}else if (callMethod.equals("GET")) {
				request = getGetRequest(flowParams, data, connectionTimeout, requestTimeout);
			}else {
				BusinessServiceRequestException bsre = new BusinessServiceRequestException("Metodo sconosciuto " + callMethod, null);
				throw bsre;
			}
	
			HttpEntity entity = null;
			// Execute the request
			try {
				if (logger.isDebugEnabled()) {
					if (request instanceof HttpPost) {
						HttpPost preq = (HttpPost)request;
						StringWriter sw = new StringWriter();
						BufferedReader br = new BufferedReader(new InputStreamReader(preq.getEntity().getContent()));
						char[] buffer = new char[1024];
						while (br.ready()) {
							int size = br.read(buffer);
							sw.write(buffer, 0 , size);
						}
						sw.close();
						br.close();
						logger.debug("sending request entity: " + StringEscapeUtils.unescapeXml(sw.toString()));		
					}
				}
				HttpResponse response = httpclient.execute(request);
				// Examine the response status
				if (response.getStatusLine().getStatusCode()!=200) {
					BusinessServiceRequestException bsre = new BusinessServiceRequestException("Service request error: url: \"" + flowParams.getHttpServiceUrl() + "\" returns " + response.getStatusLine(), null);
					throw bsre;
				}
				logger.debug(response.getStatusLine());
				// Get hold of the response entity
				entity = response.getEntity();
			} catch (ClientProtocolException e) {
				BusinessServiceRequestException bsre = new BusinessServiceRequestException("Exception in service request", e);
				throw bsre;
			} catch (IOException e) {
				BusinessServiceRequestException bsre = new BusinessServiceRequestException("Exception in service request", e);
				throw bsre;
			}
	
			if (entity == null) {
				BusinessServiceRequestException bsre = new BusinessServiceRequestException("Exception in service request, content is null", null);
				throw bsre;
			}
	
			StringBuilder result = new StringBuilder();
	
			try {
				logger.debug("Entity content length is " + entity.getContentLength());
	
				ByteArrayOutputStream arrayOut = new ByteArrayOutputStream();
				entity.writeTo(arrayOut);
				result.append(arrayOut.toString("UTF-8"));
				logger.debug("Result was " + result.length() + " characters long");
			} catch (IOException ex) {
				BusinessServiceRequestException bsre = new BusinessServiceRequestException("Exception in service request", ex);
				bsre.initCause(ex);
				throw bsre;
	
			} catch (RuntimeException ex) {
	
				// In case of an unexpected exception you may want to abort
				// the HTTP request in order to shut down the underlying
				// connection and release it back to the connection manager.
				request.abort();
				BusinessServiceRequestException bsre = new BusinessServiceRequestException("Exception in service request", ex);
				bsre.initCause(ex);
				throw bsre;
	
			}
	
			return result;
		} catch (BusinessServiceRequestException e) {
			throw e;
		} catch (RuntimeException re) {
			BusinessServiceRequestException bsre = new BusinessServiceRequestException("Exception in service request", re);
			throw bsre;
	    } finally {
			try {
				// When HttpClient instance is no longer needed,
				// shut down the connection manager to ensure
				// immediate deallocation of all system resources
				httpclient.getConnectionManager().shutdown();
			} catch (Exception e) { }
	    }
	}

	@Override
	public StringBuilder getHttpResponse(FlowParams flowParams, StringBuilder data)throws BusinessServiceRequestException {
		logger.debug("TODO: BaseServiceRequest.getResponse(" + flowParams + ", " + data);
		// TODO: esiste un criterio per stabilire se una chiamata va in GET o POST?
		return getHttpResponse(flowParams, data, -1, -1);
	}

}
