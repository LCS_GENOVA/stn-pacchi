package com.selexelsag.tt.cruscottopacchi.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * R.L. BeanGenerator - 28.05.2012 16:22:59
 * R.L. - Inseriti per il nuovo metodo del trackDbWS - packDataRetrieve
 */
@Embeddable
public class PkConMappingDettTrPK implements Serializable {

	private static final long serialVersionUID = 2991649696276408497L;

	@Column(name = "COD_TRACCIA", nullable = false)
	private String codTraccia;

	@Column(name = "DETT_ORIG", nullable = false)
	private String dettOrig;

	public String getCodTraccia() {
		return codTraccia;
	}

	public void setCodTraccia(String codTraccia) {
		this.codTraccia = codTraccia;
	}

	public String getDettOrig() {
		return dettOrig;
	}

	public void setDettOrig(String dettOrig) {
		this.dettOrig = dettOrig;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codTraccia == null) ? 0 : codTraccia.hashCode());
		result = prime * result
				+ ((dettOrig == null) ? 0 : dettOrig.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PkConMappingDettTrPK other = (PkConMappingDettTrPK) obj;
		if (codTraccia == null) {
			if (other.codTraccia != null)
				return false;
		} else if (!codTraccia.equals(other.codTraccia))
			return false;
		if (dettOrig == null) {
			if (other.dettOrig != null)
				return false;
		} else if (!dettOrig.equals(other.dettOrig))
			return false;
		return true;
	}

}