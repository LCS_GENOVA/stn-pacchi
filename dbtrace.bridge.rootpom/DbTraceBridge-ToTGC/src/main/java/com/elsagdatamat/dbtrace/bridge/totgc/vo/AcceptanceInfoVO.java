package com.elsagdatamat.dbtrace.bridge.totgc.vo;

public class AcceptanceInfoVO {
	private String whatHappened;
	private String additionalService;
	
	public String getWhatHappened() {
		return whatHappened;
	}

	public void setWhatHappened(String whatHappened) {
		this.whatHappened = whatHappened;
	}

	public String getAdditionalService() {
		return additionalService;
	}

	public void setAdditionalService(String additionalService) {
		this.additionalService = additionalService;
	}

	public AcceptanceInfoVO() {
		
	}

	public AcceptanceInfoVO(String whatHappened, String additionalService) {
		super();
		this.whatHappened = whatHappened;
		this.additionalService = additionalService;
	}
}
