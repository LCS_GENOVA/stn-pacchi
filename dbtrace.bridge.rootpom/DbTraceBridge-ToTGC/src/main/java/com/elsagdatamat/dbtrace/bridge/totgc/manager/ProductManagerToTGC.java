package com.elsagdatamat.dbtrace.bridge.totgc.manager;

import java.util.ArrayList;
import java.util.List;

//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;

import com.elsagdatamat.hibernate.QueryMode;
import com.selexelsag.tracktrace.extension.entities.runtime.EAdditionalService;
import com.selexelsag.tracktrace.extension.entities.runtime.EProduct;
import com.selexelsag.tracktrace.extension.managers.ProductManager;
import com.selexelsag.tracktrace.extension.managers.ProductManagerImpl;
import com.selexelsag.tracktrace.extension.objectmodel.Product;

//@NamedQueries(value = {
//        @NamedQuery(name = ProductManagerToTGC.GET_INTERESTING_PRODUCTS,
//                query = "select new com.selexelsag.tracktrace.extension.entities.runtime.CodeRangeRuleVO"
//                		+ "(crr.min, crr.max, crr.rule, crrp.pk.productId)"
//                		+ " from ECodeRangeRule crr, ECodeRangeRuleProduct crrp "
//                		+ "where crr.rangeId = crrp.pk.rangeId")
//        		query = "select p from EProduct p, EProductProperty pp "
//        				+ "where p.group=4 and p.id = pp.pk.productId and pp.pk.key=? and pp.value='T'")
//})
public class ProductManagerToTGC extends ProductManagerImpl implements IProductManagerToTGC {
	
	private final static String GET_INTERESTING_PRODUCTS_HQL = 
		"select p from EProduct p, EProductProperty pp "
      + "where p.group=4 and p.id = pp.pk.productId and pp.pk.key=? and pp.value='T'";
	
	
	private final static String GET_INTERESTING_SA_HQL = 
		"select a from EAdditionalService a "
      + "where a.duedcomm='T'";
	
//	protected final static String GET_INTERESTING_PRODUCTS = "listPacchiProductsWithAR0";
	
	@Override
	public List<Product> getInterestingProducts() {
//		List<EProduct> pList = getProductDao().find(
//				GET_INTERESTING_PRODUCTS, 
//				QueryMode.namedQuery,
//				new Object[]{ProductManager.KnownProperties.FLAG_CODE2DCOMM.name()});
		List<EProduct> pList = getProductDao().find(
				GET_INTERESTING_PRODUCTS_HQL, 
				QueryMode.hql,
				new Object[]{ProductManager.KnownProperties.FLAG_CODE2DCOMM.name()});
		
		return createObjs(pList);
	}
	
	@Override
	public ListOfSA getInterestingAdditionalServices() {
//		List<EProduct> pList = getProductDao().find(
//				GET_INTERESTING_PRODUCTS, 
//				QueryMode.namedQuery,
//				new Object[]{ProductManager.KnownProperties.FLAG_CODE2DCOMM.name()});
		List<EAdditionalService> pList = getAdditionalServiceDao().find(
				GET_INTERESTING_SA_HQL, 
				QueryMode.hql,
				new Object[]{});
		
		List<String> keywordsPerif = new ArrayList<String>();
		List<String> keywordsCdg = new ArrayList<String>();
		List<String> keywordsSdp = new ArrayList<String>();
		
		for (EAdditionalService as : pList) {
			if (as.getIdsamsmqPerif() != null) {
				keywordsPerif.add(as.getIdsamsmqPerif());
			}
			if (as.getIdsamsmqCdg() != null) {
				keywordsCdg.add(as.getIdsamsmqCdg());
			}
			if (as.getIdsaSdp() != null) {
				keywordsSdp.add(as.getIdsaSdp());
			}
		}
		
		return new ListOfSA(keywordsPerif, keywordsCdg, keywordsSdp);
	}
	
	class ListOfSA {
		List<String> perif;
		List<String> cdg;
		List<String> sdp;
		public ListOfSA(List<String> perif, List<String> cdg, List<String> sdp) {
			super();
			this.perif = perif;
			this.cdg = cdg;
			this.sdp = sdp;
		}
	}
}
