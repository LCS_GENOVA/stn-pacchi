package com.selexelsag.xte.business.exceptions;

public class BusinessPersistentAccessException extends BusinessException {
	
	public BusinessPersistentAccessException(String message, Throwable cause) {
		super(message, cause);
		setExceptionId(BUSINESS_PERSISTENCE_ID);
	}
	
}
