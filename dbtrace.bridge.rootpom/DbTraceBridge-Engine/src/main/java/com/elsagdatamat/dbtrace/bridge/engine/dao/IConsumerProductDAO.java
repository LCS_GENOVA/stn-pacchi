package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.ConsumerProduct;
import com.elsagdatamat.dbtrace.bridge.engine.entities.ConsumerProductPK;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface IConsumerProductDAO extends IGenericDAO<ConsumerProduct, ConsumerProductPK>{

}
