package com.elsagdatamat.dbtrace.bridge.engine;

import java.util.Date;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public interface ITracesReader {
	
	public TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate);
	public String getserviceId();
	@Transactional(value = "dbTraceBridge-transactionManager", propagation = Propagation.REQUIRES_NEW)
	public Object getToken(TrackDBRWServicePortType twsp);
	
	@Transactional(value = "dbTraceBridge-transactionManager", propagation = Propagation.REQUIRES_NEW)
	public void acknowledge(TrackDBRWServicePortType twsp);

	@Transactional(value = "dbTraceBridge-transactionManager", propagation = Propagation.REQUIRES_NEW)
	public void release(TrackDBRWServicePortType twsp);
}
