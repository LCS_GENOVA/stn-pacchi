package com.selexelsag.xte.business.tracking;

import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.model.track.TrackResponse;

public interface IInteropForward {

	public static final String FLOW_INTEROP_BEAN_NAME = "interopForward";
	

	/**
	 * Verifica se la chiamata deve essere inoltrata a INTEROPERABILITA
	 * @param l'oggetto contenente i parametri identificativi 
	 * del flusso richiesto.
	 * @return se la chiamata deve essere inoltrata a INTEROPERABILITA
	 * @throws Exception
	 */
	boolean checkforInterop(FlowParams flowParams) throws Exception;
	
	/**
	 *  Compone la  request e la response in un messaggio secondo gli xsd forniti da INTEROPERABILITA
		Effettua l'inoltro tramite la MESSAGE_TO_BE_DELIVERED
	 * @param 
	 * @return
	 * @throws Exception
	 *  Scrive in un file di log apposito
	 */
	void manageInterop(TrackRequest trackRequest, TrackResponse trackResponse) throws Exception;
}
