package com.selexelsag.xte.business.exceptions;

import com.selexelsag.xte.service.exceptions.BaseException;

public class BusinessException extends BaseException {

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
		setExceptionId(BUSINESS_ID);
	}
	
	public BusinessException(String msg) {
		super(msg);
	}
	
}
