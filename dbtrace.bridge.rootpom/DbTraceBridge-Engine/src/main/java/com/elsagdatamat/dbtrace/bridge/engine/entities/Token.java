package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DBTRACE_CONSUMER_TOKEN")
public class Token implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9061058309841509925L;
	public static final String SERVICE_ID = "serviceId";
	public static final String WS_TOKEN = "wsToken";
	public static final String ACCESS_DATE = "accessDate";
	public static final String RELEASE_DATE = "releaseDate";
	
	@Id
	@Column(name="SERVICE_ID")
	private String serviceId;
	
	@Column(name="WS_TOKEN")
	private String wsToken;

	@Column(name="ACCESS_DATE")
	private Date accessDate;

	@Column(name="RELEASE_DATE")
	private Date releaseDate;

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getWsToken() {
		return wsToken;
	}

	public void setWsToken(String wsToken) {
		this.wsToken = wsToken;
	}

	public Date getAccessDate() {
		return accessDate;
	}

	public void setAccessDate(Date accessDate) {
		this.accessDate = accessDate;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
}
