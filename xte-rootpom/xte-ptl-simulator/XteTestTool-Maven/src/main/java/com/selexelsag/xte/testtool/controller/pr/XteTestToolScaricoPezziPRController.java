/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.pr;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.controller.common.XteTestToolFieldValidator;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XteScaricoPezzi;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;
import com.selexelsag.xte.testtool.view.objectmodel.XteScaricoPezziTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteTestToolDuplicateKeyException;
import com.selexelsag.xte.testtool.view.pr.XteTestToolScaricoPezziPRView;
import com.selexelsag.xte.testtool.view.pr.XteTestToolWebServicePRView;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata.SP;
import com.selexelsag.xtetesttool.xml.postaregistrata.sp.DatiInvio;
import com.selexelsag.xtetesttool.xml.postaregistrata.sp.ObjectFactory;
import com.selexelsag.xtetesttool.xml.postaregistrata.sp.Traccia;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

/**
 *
 * @author Tassara
 */
public class XteTestToolScaricoPezziPRController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener, ListSelectionListener, TableModelListener {

    private XteTestToolScaricoPezziPRView scaricoPezziPRView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolScaricoPezziPRController.class);
    private XteScaricoPezzi selectedMailPiece = null;
    private int currentSelectedRow = -1;
    private List<JTextComponent> scaricoPezziFieldList = new ArrayList<JTextComponent>();
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    
    private JAXBContext context;
    
    private static Traccia scaricoPezziTraccia;

    static {
        logger.debug("Creating Config Object Factory and Traccia");
        PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        SP config = postaRegistrataConfig.getSP();

        scaricoPezziTraccia = objectFactory.createTraccia();
        scaricoPezziTraccia.setEvento(config.getEventName());
        scaricoPezziTraccia.setHeaderMazzetto(objectFactory.createHeaderMazzetto());
        scaricoPezziTraccia.setListaInvii(objectFactory.createListaInvii());
    }
    
    
    public XteTestToolScaricoPezziPRController(JPanel scaricoPezziPRView) {
        try {
            logger.info("XteTestToolScaricoPezziPRController start class creation");
            this.scaricoPezziPRView = (XteTestToolScaricoPezziPRView) scaricoPezziPRView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.scaricoPezziPRView.initializeView();
            
            updateView();
            
            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.postaregistrata.sp");
            
            logger.info("XteTestToolScaricoPezziPRController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolScaricoPezziPRController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolScaricoPezziPRController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller creation failed", ex);
        }

    }
    
    private void updateDataModel() {
        String id = scaricoPezziPRView.getSPCodiceTextRef().getText();
        String opid = scaricoPezziPRView.getBundleTPIdTextRef().getText();
        logger.debug("updateDataModel - " + id + "," + opid);
        scaricoPezziTraccia.getHeaderMazzetto().setIdOperatoreTP(new String(opid));
        scaricoPezziTraccia.getHeaderMazzetto().setIdUnivoco(scaricoPezziPRView.getIDUnivocoTextRef().getText());
        scaricoPezziTraccia.getHeaderMazzetto().setOfficeFrazionario(scaricoPezziPRView.getFrazionariTextRef().getText());
        ObjectFactory objectFactory = new ObjectFactory();
        XteScaricoPezzi[] entries = scaricoPezziPRView.getScaricoPezziTableModelRef().getMap().values().toArray(new XteScaricoPezzi[0]);
        logger.debug("entries count: " + entries.length);        
        scaricoPezziTraccia.getListaInvii().getDatiInvio().clear();
        for (int i=0;i<entries.length;i++) {
        
            XteScaricoPezzi item = entries[i];
            logger.debug("item " + i + ": " + item);
            if (item == null) 
                continue;
            DatiInvio obj = objectFactory.createDatiInvio();
            obj.setCodice(item.getScaricoPezziCode());
            obj.setProdotto(item.getScaricoPezziProduct());
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(item.getScaricoPezziDateTime());
            try {
                obj.setDataOraRitiro(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            } catch (DatatypeConfigurationException ex) {
                logger.error("Error converting date " + c, ex);
                obj.setDataOraRitiro(null);
            }
            obj.setPeso(item.getScaricoPezziPeso());
            obj.setPrezzo(item.getScaricoPezziPrezzo());
            obj.setPercetto(item.getScaricoPezziPercetto());
            obj.setDestinazione(item.getScaricoPezziDestinazione());
            obj.setDestinatario(item.getScaricoPezziDestinatario());
            obj.setIndirizzo(item.getScaricoPezziIndirizzo());
            obj.setCap(item.getScaricoPezziCAP());
            obj.setNominativoMittente(item.getScaricoPezziNomeMitt());
            obj.setDestinazioneMittente(item.getScaricoPezziDestMitt());
            obj.setIndirizzoMittente(item.getScaricoPezziIndMitt());
            obj.setCapMittente(item.getScaricoPezziCAPMitt());
            obj.setSA(item.getScaricoPezziSA());
            if(item.getScaricoPezziCodAR() != null && !item.getScaricoPezziCodAR().equals(""))
                obj.setCodiceAR(item.getScaricoPezziCodAR());
            if(item.getScaricoPezziValAss() != 0)
                obj.setValoreAssicurato(item.getScaricoPezziValAss());
            if(item.getScaricoPezziImpContr() != 0)
                obj.setImportoContrassegno(item.getScaricoPezziImpContr());
            if(item.getScaricoPezziIBAN() != null && !item.getScaricoPezziIBAN().equals(""))
                obj.setIBAN(item.getScaricoPezziIBAN());
            obj.setOraTimeDefinite(item.getScaricoPezziOraTimeDef());
            obj.setSmarrito(item.getScaricoPezziSmarrito());
            scaricoPezziTraccia.getListaInvii().getDatiInvio().add(obj);
        }
    }
    
    
    private void updateView() {  
        String id = scaricoPezziTraccia.getHeaderMazzetto().getIdUnivoco();
        String opid = scaricoPezziTraccia.getHeaderMazzetto().getIdOperatoreTP();
        logger.debug("updateView - " + id + "," + opid);
                
        if (id != null) {
            scaricoPezziPRView.getIDUnivocoTextRef()
                    .setText(id);
        }

        if (opid != null) {
            scaricoPezziPRView.getBundleTPIdTextRef()
                    .setText(opid);
        }

        scaricoPezziPRView.getScaricoPezziTableModelRef().clearModel();
            
        for (int i=0;i<scaricoPezziTraccia.getListaInvii().getDatiInvio().size();i++) {
            DatiInvio item = scaricoPezziTraccia.getListaInvii().getDatiInvio().get(i);
            XteScaricoPezzi obj = new XteScaricoPezzi();
            obj.setScaricoPezziCode(item.getCodice());
            obj.setScaricoPezziDateTime(item.getDataOraRitiro().toGregorianCalendar().getTime());
            obj.setScaricoPezziProduct(item.getProdotto());
            obj.setScaricoPezziPeso(item.getPeso());
            obj.setScaricoPezziPrezzo(item.getPrezzo());
            obj.setScaricoPezziPercetto(item.getPercetto());
            obj.setScaricoPezziDestinazione(item.getDestinazione());
            obj.setScaricoPezziDestinatario(item.getDestinatario());
            obj.setScaricoPezziIndirizzo(item.getIndirizzo());
            obj.setScaricoPezziCAP(item.getCap());
            obj.setScaricoPezziNomeMitt(item.getNominativoMittente());
            obj.setScaricoPezziDestMitt(item.getDestinazioneMittente());
            obj.setScaricoPezziIndMitt(item.getIndirizzoMittente());
            obj.setScaricoPezziCAPMitt(item.getCapMittente());
            obj.setScaricoPezziSA(item.getSA());
            obj.setScaricoPezziCodAR(item.getCodiceAR());
            if(item.getValoreAssicurato() != null)
                obj.setScaricoPezziValAss(item.getValoreAssicurato());
            if(item.getImportoContrassegno() != null)
                obj.setScaricoPezziImpContr(item.getImportoContrassegno());
            obj.setScaricoPezziIBAN(item.getIBAN());
            obj.setScaricoPezziOraTimeDef(item.getOraTimeDefinite());
            obj.setScaricoPezziSmarrito(item.getSmarrito());
            scaricoPezziPRView.getScaricoPezziTableModelRef().addScaricoPezziItem(obj);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = scaricoPezziPRView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        scaricoPezziPRView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.scaricoPezziPRView.getScaricoPezziPanelRef().addAncestorListener(this);        
        this.scaricoPezziPRView.getIDUnivocoTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getBundleTPIdTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getIDUnivocoTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPProductTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPPesoTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPPrezzoTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPPercettoTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPDestinazioneTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPDestinatarioTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPIndirizzoTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPCAPTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPNomeMittenteTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPDestMittenteTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPIndMittenteTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPCAPMittTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPSAJComboBoxRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPValAssTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPImpContrTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPOraTimeDefTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPCodARTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPIBANTextRef().addFocusListener(this);
        this.scaricoPezziPRView.getSPSmarritoCheckRef().addFocusListener(this);
        this.scaricoPezziPRView.getFrazionariTextRef().addFocusListener(this);
        
        this.scaricoPezziPRView.getMailPieceDatePickerRef().getEditor().addFocusListener(this);
        JFormattedTextField startTimejFormattedTextField = ((JSpinner.DateEditor)scaricoPezziPRView.getMailPieceTimeSpinnerRef().getEditor()).getTextField();
        startTimejFormattedTextField.addFocusListener(this);
        this.scaricoPezziPRView.getMailPieceTableRef().getSelectionModel().addListSelectionListener(this);
        this.scaricoPezziPRView.getMailPieceTableRef().getModel().addTableModelListener(this);
        this.scaricoPezziPRView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.scaricoPezziPRView.getAggiungiItemButtonRef().addActionListener(this);
        this.scaricoPezziPRView.getEliminaItemButtonRef().addActionListener(this);
        this.scaricoPezziPRView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.scaricoPezziPRView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.scaricoPezziPRView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.scaricoPezziPRView.getBackButtonRef().addActionListener(this);
        this.scaricoPezziPRView.getHomeButtonRef().addActionListener(this);
        this.scaricoPezziPRView.getExitButtonRef().addActionListener(this);
        this.scaricoPezziPRView.getTestButtonRef().addActionListener(this);
        
        this.scaricoPezziPRView.getSPCodiceTextRef().getDocument().addDocumentListener(this);
        this.scaricoPezziPRView.getSPProductTextRef().getDocument().addDocumentListener(this);
        this.scaricoPezziPRView.getMailPieceDatePickerRef().getEditor().getDocument().addDocumentListener(this);
        ((JSpinner.DateEditor)scaricoPezziPRView.getMailPieceTimeSpinnerRef().getEditor()).getTextField().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPPesoTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPPrezzoTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPPercettoTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPDestinazioneTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPDestinatarioTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPIndirizzoTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPCAPTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPNomeMittenteTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPDestMittenteTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPIndMittenteTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPCAPMittTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPSAJComboBoxRef().addActionListener(this);
        scaricoPezziPRView.getSPValAssTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPImpContrTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPOraTimeDefTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPCodARTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPIBANTextRef().getDocument().addDocumentListener(this);
        scaricoPezziPRView.getSPSmarritoCheckRef().addActionListener(this);
        
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPCodiceTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPProductTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getMailPieceDatePickerRef().getEditor());
        scaricoPezziFieldList.add(((JSpinner.DateEditor)scaricoPezziPRView.getMailPieceTimeSpinnerRef().getEditor()).getTextField());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPPesoTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPPrezzoTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPPercettoTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPDestinazioneTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPDestinatarioTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPIndirizzoTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPCAPTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPNomeMittenteTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPDestMittenteTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPIndMittenteTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPCAPMittTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPCodARTextRef());
        //mailPieceFieldList.add(scaricoPezziPRView.getSPSATextRef());
        //scaricoPezziFieldList.add(scaricoPezziPRView.getSPValAssTextRef());
        //scaricoPezziFieldList.add(scaricoPezziPRView.getSPImpContrTextRef());
        scaricoPezziFieldList.add(scaricoPezziPRView.getSPOraTimeDefTextRef());
        //mailPieceFieldList.add(scaricoPezziPRView.getSPSmarritoCheck().isSelected() ? "T" : "F");
	this.salvaXmlFileChooser.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        
        if(source.equals(scaricoPezziPRView.getSPSAJComboBoxRef()))
        {
            checkScaricoPezziFieldsFull();
        }
        
        if(source.equals(scaricoPezziPRView.getAggiungiItemButtonRef()))
        {
            logger.debug("XteTestToolScaricoPezziPRController actionPerformed invoke addMailPieceItem");
			
			updateDataModel();

            addMailPieceItem();
        }
        
        if(source.equals(scaricoPezziPRView.getEliminaItemButtonRef()))
        {
            logger.debug("XteTestToolScaricoPezziPRController actionPerformed invoke removeMailPieceItem");

			updateDataModel();

            removeMailPieceItem();
        }

        if(source.equals(scaricoPezziPRView.getTestButtonRef()))
        {
            logger.debug("XteTestToolScaricoPezziPRController actionPerformed invoke autocomplete");

            AutoCompleteFields();
        }
        
        if(source.equals(scaricoPezziPRView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolScaricoPezziPRController actionPerformed invoke generaXmlData");

			updateDataModel();

            generaXmlData();
        }

        if(source.equals(scaricoPezziPRView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolScaricoPezziPRController actionPerformed invoke impostaDatiWebService");

			updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(scaricoPezziPRView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolScaricoPezziPRController actionPerformed invoke salvaXmlData");

			updateDataModel();

            salvaXmlData();
        }

        if(source.equals(scaricoPezziPRView.getBackButtonRef()))
        {
            logger.debug("XteTestToolScaricoPezziPRController actionPerformed invoke back");

			updateDataModel();

            back();
        }

        if(source.equals(scaricoPezziPRView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolScaricoPezziPRController actionPerformed invoke home");

			updateDataModel();

            home();
        }

        if(source.equals(scaricoPezziPRView.getExitButtonRef()))
        {
            logger.debug("XteTestToolScaricoPezziPRController actionPerformed invoke exit");
			
			updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolScaricoPezziPRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(scaricoPezziPRView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolScaricoPezziPRView)
        {
            logger.debug("XteTestToolScaricoPezziPRController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolScaricoPezziPRController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolScaricoPezziPRController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        checkScaricoPezziFieldsFull();  
    }    
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object source = e.getSource();
        if(source.equals(scaricoPezziPRView.getMailPieceTableRef().getSelectionModel()))
        {               
            if (e.getValueIsAdjusting()) {
                    return;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
                        currentSelectedRow = scaricoPezziPRView.getMailPieceTableRef().getSelectedRow();
                        logger.debug("XteTestToolScaricoPezziPRController valueChanged currentSelectedRow = " + currentSelectedRow);
                        logger.debug("XteTestToolScaricoPezziPRController valueChanged: " + ((XteScaricoPezziTableModel)scaricoPezziPRView.getMailPieceTableRef().getModel()).getMap().size());
                        if(currentSelectedRow!=-1)
                        {
                            List<Component> enableComponents = new ArrayList<Component>();
                            enableComponents.add(scaricoPezziPRView.getEliminaItemButtonRef());
                            componentStatusManager.enableComponent(enableComponents);
                            selectedMailPiece = ((XteScaricoPezziTableModel)scaricoPezziPRView.getMailPieceTableRef().getModel()).getSelectedItem(currentSelectedRow);
                        }
                        else
                        {
                            List<Component> disableComponents = new ArrayList<Component>();
                            disableComponents.add(scaricoPezziPRView.getEliminaItemButtonRef());
                            componentStatusManager.disableComponent(disableComponents);                                                        
                            selectedMailPiece = null;
                        }
                    }
                } );
        }
        
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int eventType = e.getType();
        DefaultTableModel model = (DefaultTableModel) e.getSource();
        if(model.equals(scaricoPezziPRView.getMailPieceTableRef().getModel()))
        {               
            logger.debug("XteTestToolScaricoPezziPRController tableChanged: " + ((XteScaricoPezziTableModel)scaricoPezziPRView.getMailPieceTableRef().getModel()).getMap().size());
        }
    }

    private void checkScaricoPezziFieldsFull() {

        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        Object SAobj = scaricoPezziPRView.getSPSAJComboBoxRef().getSelectedItem();
        String SA = null;
        if(SAobj != null)
            SA = SAobj.toString();
        List<Component> disableComponents = new ArrayList<Component>();
        disableComponents.add(scaricoPezziPRView.getAggiungiItemButtonRef());
        
        //CodiceAR Facoltativo se SA == AR2
        //Valore assicurato Obligatorio se SA == AS0
        //Imp contrassegno Obligatorio se SA == CA0
        //Iban Obligatorio se SA == CA3
        if(SA == null)
        {
            componentStatusManager.disableComponent(disableComponents);                                                        
            return;
        }
        if(SA.equals("AR2"))
            scaricoPezziFieldList.remove(scaricoPezziPRView.getSPCodARTextRef());
                
        if(SA.equals("AS0") && scaricoPezziPRView.getSPValAssTextRef().getText().equals(""))
        {
            componentStatusManager.disableComponent(disableComponents);                                                        
            return;
        }
        if(SA.equals("CA0") && scaricoPezziPRView.getSPImpContrTextRef().getText().equals(""))
        {
            componentStatusManager.disableComponent(disableComponents);                                                        
            return;
        }
        if(SA.equals("CA3") && scaricoPezziPRView.getSPIBANTextRef().getText().equals(""))
        {
            componentStatusManager.disableComponent(disableComponents);                                                        
            return;
        }  
        
        for (JTextComponent field : scaricoPezziFieldList) {
            if (field.getText().trim().isEmpty()) {
                
                componentStatusManager.disableComponent(disableComponents);                                                        
                return;
            }
        }
        List<Component> enableComponents = new ArrayList<Component>();
        enableComponents.add(scaricoPezziPRView.getAggiungiItemButtonRef());
        componentStatusManager.enableComponent(enableComponents);
    }
    
    private void AutoCompleteFields()
    {
        scaricoPezziPRView.setSPFields();
        checkScaricoPezziFieldsFull();
    }
    
    private void clearMailPieceFields() {

        for (JTextComponent field : scaricoPezziFieldList) {
            if (!field.getText().trim().isEmpty()) {
                field.setText("");
            }
        }
        scaricoPezziPRView.getSPValAssTextRef().setText("");
        scaricoPezziPRView.getSPIBANTextRef().setText("");
        scaricoPezziPRView.getSPCodARTextRef().setText("");
        scaricoPezziPRView.getSPValAssTextRef().setText("");
        scaricoPezziPRView.getSPImpContrTextRef().setText("");
        scaricoPezziPRView.getSPSmarritoCheckRef().setSelected(false);
        scaricoPezziPRView.getSPSAJComboBoxRef().setSelectedIndex(-1);
    }
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        scaricoPezziPRView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolScaricoPezziPRController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.scaricoPezziPRView.customizeView();
            
            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(scaricoPezziPRView);
            updateXteTestToolDataModel(dataModel);

            clearMailPieceFields();
            clearMessageBar();
            
            Configuration configuration = XteTestToolConfiguration.getInstance().getConfiguration();
            List<String> frazList = configuration.getList("PostaRegistrata.SA.List");
            DefaultComboBoxModel model = new DefaultComboBoxModel(frazList.toArray());                                    
            scaricoPezziPRView.getSPSAJComboBoxRef().setModel(model);
            scaricoPezziPRView.getSPSAJComboBoxRef().setSelectedIndex(-1);
            
            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(scaricoPezziPRView.getAggiungiItemButtonRef());
            disabledComponents.add(scaricoPezziPRView.getEliminaItemButtonRef());
            disabledComponents.add(scaricoPezziPRView.getImpostaDatiWSButtonRef());
            disabledComponents.add(scaricoPezziPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            Calendar date = Calendar.getInstance();
            this.scaricoPezziPRView.getMailPieceDatePickerRef().setDate(date.getTime());
            this.scaricoPezziPRView.getMailPieceTimeSpinnerRef().setValue(date.getTime());

            scaricoPezziPRView.getIDUnivocoTextRef().requestFocus();

            logger.info("XteTestToolScaricoPezziPRController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolScaricoPezziPRController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolScaricoPezziPRController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller init failed", ex);
        }

    }
    
    private void exit()
    {
        logger.info("XteTestToolScaricoPezziPRController start exit");

        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(scaricoPezziPRView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolScaricoPezziPRController exit in progress");
            scaricoPezziPRView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolScaricoPezziPRController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(scaricoPezziPRView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(scaricoPezziPRView.getScaricoPezziPanelRef().getParent(), scaricoPezziPRView.getScaricoPezziPanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolScaricoPezziPRController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(scaricoPezziPRView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(scaricoPezziPRView.getScaricoPezziPanelRef().getParent(), scaricoPezziPRView.getScaricoPezziPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private void addMailPieceItem()
    {
        logger.info("XteTestToolScaricoPezziPRController start addMailPieceItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            
            clearMessageBar();
            
            XteTestToolMessageBar messageBar = new XteTestToolMessageBar();

            XteTestToolFieldValidator validator = new XteTestToolFieldValidator();            

                                    
            XteScaricoPezzi item = new XteScaricoPezzi();
            //String selectedItem = (String) scaricoPezziPRView.getScaricoPezziTypeComboBoxRef().getModel().getSelectedItem();
            
            //item.setScaricoPezziType(selectedItem);
            item.setScaricoPezziCode(scaricoPezziPRView.getSPCodiceTextRef().getText());
            item.setScaricoPezziProduct(scaricoPezziPRView.getSPProductTextRef().getText());
            item.setScaricoPezziPeso(scaricoPezziPRView.getSPPesoTextRef().getText());
            item.setScaricoPezziPrezzo(Integer.parseInt(scaricoPezziPRView.getSPPrezzoTextRef().getText()));
            item.setScaricoPezziPercetto(Integer.parseInt(scaricoPezziPRView.getSPPercettoTextRef().getText()));
            item.setScaricoPezziDestinazione(scaricoPezziPRView.getSPDestinazioneTextRef().getText());
            item.setScaricoPezziDestinatario(scaricoPezziPRView.getSPDestinatarioTextRef().getText());
            item.setScaricoPezziIndirizzo(scaricoPezziPRView.getSPIndirizzoTextRef().getText());
            item.setScaricoPezziCAP(scaricoPezziPRView.getSPCAPTextRef().getText());
            item.setScaricoPezziNomeMitt(scaricoPezziPRView.getSPNomeMittenteTextRef().getText());
            item.setScaricoPezziDestMitt(scaricoPezziPRView.getSPDestMittenteTextRef().getText());
            item.setScaricoPezziIndMitt(scaricoPezziPRView.getSPIndMittenteTextRef().getText());
            item.setScaricoPezziCAPMitt(scaricoPezziPRView.getSPCAPMittTextRef().getText());
            item.setScaricoPezziSA(scaricoPezziPRView.getSPSAJComboBoxRef().getSelectedItem().toString());
            item.setScaricoPezziCodAR(scaricoPezziPRView.getSPCodARTextRef().getText());
            if(!scaricoPezziPRView.getSPValAssTextRef().getText().isEmpty())
                item.setScaricoPezziValAss(Integer.parseInt(scaricoPezziPRView.getSPValAssTextRef().getText()));
            if(!scaricoPezziPRView.getSPImpContrTextRef().getText().isEmpty())
                item.setScaricoPezziImpContr(Integer.parseInt(scaricoPezziPRView.getSPImpContrTextRef().getText()));
            item.setScaricoPezziIBAN(scaricoPezziPRView.getSPIBANTextRef().getText());
            item.setScaricoPezziOraTimeDef(scaricoPezziPRView.getSPOraTimeDefTextRef().getText());
            item.setScaricoPezziSmarrito(scaricoPezziPRView.getSPSmarritoCheckRef().isSelected() ? "T" : "F");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss.SSS");
            //SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");            
            Date startDate = this.scaricoPezziPRView.getMailPieceDatePickerRef().getDate();
            SpinnerDateModel startTimeSpinnerDateModel = (SpinnerDateModel) this.scaricoPezziPRView.getMailPieceTimeSpinnerRef().getModel();
            Date startTime = startTimeSpinnerDateModel.getDate();
            String startDateStr = dateFormatter.format(startDate);
            String startTimeStr = timeFormatter.format(startTime);
            String startDateTimeStr = startDateStr + "T" + startTimeStr;   
            SimpleDateFormat lasttimeFormatter = new SimpleDateFormat("yyyy-MM-dd" + "'T'" + "HH:mm:ss");
            item.setScaricoPezziDateTime((Date) lasttimeFormatter.parse(startDateTimeStr));
            
            DecimalFormat numberFormat = new DecimalFormat();

            try 
            {
                this.scaricoPezziPRView.getScaricoPezziTableModelRef().addScaricoPezziItem(item);
            }
            catch (XteTestToolDuplicateKeyException duplicateEx)
            {
                StyledDocument document = messageBar.putContentOnMessageBar("Attemp to insert duplicate key Item!", MessageType.WARNING);
                scaricoPezziPRView.getMessageBarTextPaneRef().setDocument(document);
                this.scaricoPezziPRView.getSPCodiceTextRef().requestFocus();
                return;
            }
            
            StyledDocument document = messageBar.putContentOnMessageBar("Item added successfully!", MessageType.CORRECT_PROCESSING);
            scaricoPezziPRView.getMessageBarTextPaneRef().setDocument(document);
                        
            clearMailPieceFields();
            scaricoPezziPRView.getSPCodiceTextRef().requestFocus();            
            
            logger.info("XteTestToolScaricoPezziPRController addMailPieceItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolScaricoPezziPRController addMailPieceItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolScaricoPezziPRController addMailPieceItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller addMailPieceItem failed", ex);
        }
    }
    
    private void removeMailPieceItem()
    {
        logger.info("XteTestToolScaricoPezziPRController start removeMailPieceItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            if(currentSelectedRow!=-1)
            {         
                XteScaricoPezziTableModel model = ((XteScaricoPezziTableModel)scaricoPezziPRView.getMailPieceTableRef().getModel());
                model.removeRow(currentSelectedRow);
                logger.debug("XteTestToolScaricoPezziPRController removeMailPieceItem: " + ((XteScaricoPezziTableModel)scaricoPezziPRView.getMailPieceTableRef().getModel()).getMap().size());
            }

            logger.info("XteTestToolScaricoPezziPRController removeMailPieceItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolScaricoPezziPRController removeMailPieceItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolScaricoPezziPRController removeMailPieceItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller removeMailPieceItem failed", ex);
        }
    }

	private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }

    private void generaXmlData()
    {
        logger.info("XteTestToolScaricoPezziPRController start generaXmlData");

        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();

        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            scaricoPezziPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(scaricoPezziPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(scaricoPezziPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            scaricoPezziPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(scaricoPezziPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(scaricoPezziPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(scaricoPezziTraccia, out);
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            scaricoPezziPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(scaricoPezziPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(scaricoPezziPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
            
        try
        {            
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString());
            scaricoPezziPRView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("XteTestToolScaricoPezziPRController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolScaricoPezziPRController generaXmlData failed", baseEx);
			StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(baseEx.toString(), MessageType.ERROR);
            scaricoPezziPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(scaricoPezziPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(scaricoPezziPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolScaricoPezziPRController generaXmlData failed", ex);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            scaricoPezziPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(scaricoPezziPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(scaricoPezziPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        scaricoPezziPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(scaricoPezziPRView.getImpostaDatiWSButtonRef());
        enableComponent.add(scaricoPezziPRView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolScaricoPezziPRController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServicePRView(scaricoPezziPRView.getParentFrame());
            XteTestToolWebServicePRController controller = new XteTestToolWebServicePRController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
            SP config = postaRegistrataConfig.getSP();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(scaricoPezziPRView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(scaricoPezziPRView.getScaricoPezziPanelRef().getParent(), scaricoPezziPRView.getScaricoPezziPanelRef(), webserviceview, new Rectangle(0,70,1000,690));

            logger.info("XteTestToolScaricoPezziPRController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolScaricoPezziPRController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolScaricoPezziPRController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller impostaDatiWebService failed", ex);
        }
    }
    
    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolScaricoPezziPRController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolScaricoPezziPRController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(scaricoPezziPRView.getParentFrame());

            logger.info("XteTestToolScaricoPezziPRController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolScaricoPezziPRController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolScaricoPezziPRController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller salvaXmlData failed", ex);
        }
    }
}
