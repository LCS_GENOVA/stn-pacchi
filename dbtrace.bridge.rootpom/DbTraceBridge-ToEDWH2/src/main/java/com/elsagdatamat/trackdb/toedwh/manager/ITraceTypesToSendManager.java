package com.elsagdatamat.trackdb.toedwh.manager;

import java.util.List;

import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSend;

public interface ITraceTypesToSendManager {

	public List<TraceTypesToSend> findByDestinationTracedEntityChannel(String destination,String tracedEntity,String channel);
}
