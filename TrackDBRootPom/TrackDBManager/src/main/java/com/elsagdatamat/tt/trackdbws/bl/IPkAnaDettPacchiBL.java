package com.elsagdatamat.tt.trackdbws.bl;

import java.util.List;

import com.elsagdatamat.tt.trackdbws.beans.VO.PackDataRetrieveVO;
import com.selexelsag.tt.cruscottopacchi.bean.PkAnaDettPacchi;


/** 
* R.L. BeanGenerator - 27.05.2012 18:17:43
*/ 
public interface IPkAnaDettPacchiBL {

	public void insert(PkAnaDettPacchi dettaglio);
	public List<PackDataRetrieveVO> findConfDetsByIdPacco(String IdPacco);
}