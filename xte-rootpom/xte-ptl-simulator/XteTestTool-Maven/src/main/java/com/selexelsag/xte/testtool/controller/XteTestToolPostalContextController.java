/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller;

import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.PostalContext;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.xml.Function;
import java.awt.Color;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolPostalContextController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, ListSelectionListener, PropertyChangeListener, DocumentListener, ChangeListener {

    private XteTestToolPostalContextView postalContextView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolPostalContextController.class);
    private List<Function> postaRegistrataFunctions = null;
    private List<Function> dataPostaFunctions = null;
    private List<Function> acquisizioneMazzettiFunctions = null;
    private List<Function> accettazioneOnlineFunctions = null;

    public XteTestToolPostalContextController(XteTestToolPostalContextView postalContextView) {
        try {
            logger.info("XteTestToolPostalContextController start class creation");
            this.postalContextView = postalContextView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.postalContextView.initializeView();
            addViewObjectsListeners();
            
            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            List<Function> globalPostalFunctions = dataModel.getPostalFunctionsList();

            postaRegistrataFunctions = new ArrayList<Function>();
            dataPostaFunctions = new ArrayList<Function>();
            acquisizioneMazzettiFunctions = new ArrayList<Function>();
            accettazioneOnlineFunctions = new ArrayList<Function>();
            
            if(globalPostalFunctions!=null && !globalPostalFunctions.isEmpty())
            {
                for (Function postalFunction : globalPostalFunctions) {
                    
                    if(postalFunction.getContext().equalsIgnoreCase(PostalContext.POSTA_REGISTRATA.getCode()))
                    {
                        postaRegistrataFunctions.add(postalFunction);
                    }
                    if(postalFunction.getContext().equalsIgnoreCase(PostalContext.DATA_POSTA.getCode()))
                    {
                        dataPostaFunctions.add(postalFunction);
                    }
                    if(postalFunction.getContext().equalsIgnoreCase(PostalContext.ACQUISIZIONE_MAZZETTI.getCode()))
                    {
                        acquisizioneMazzettiFunctions.add(postalFunction);
                    }
                    if(postalFunction.getContext().equalsIgnoreCase(PostalContext.ACCETTAZIONE_ONLINE.getCode()))
                    {
                        accettazioneOnlineFunctions.add(postalFunction);
                    }

                }            
            }
            
            logger.info("XteTestToolPostalContextController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPostalContextController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolPostalContextController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Postal Context controller creation failed", ex);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = postalContextView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        postalContextView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.postalContextView.getPostalContextPanelRef().addAncestorListener(this);
        
        this.postalContextView.getPostalContextTabbedPaneRef().addChangeListener(this);
        
        this.postalContextView.getPostaRegistrataEventsListRef().addListSelectionListener(this);
        this.postalContextView.getDataPostaEventsListRef().addListSelectionListener(this);
        this.postalContextView.getAcquisizioneMazzettiEventsListRef().addListSelectionListener(this);
        this.postalContextView.getAccettazioneOnlineEventsListRef().addListSelectionListener(this);

        this.postalContextView.getOkButtonRef().addActionListener(this);
        this.postalContextView.getHomeButtonRef().addActionListener(this);
        this.postalContextView.getExitButtonRef().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(postalContextView.getOkButtonRef()))
        {
            logger.debug("XteTestToolPostalContextController actionPerformed invoke loadPostalFunctionView");
            loadPostalFunctionView();
        }

        if(source.equals(postalContextView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolPostalContextController actionPerformed invoke home");
            home();
        }

        if(source.equals(postalContextView.getExitButtonRef()))
        {
            logger.debug("XteTestToolPostalContextController actionPerformed invoke exit");
            exit();
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolPostalContextView)
        {
            logger.debug("XteTestToolPostalContextController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        if(comp instanceof JTextField)
        {
            ((JTextField)comp).setBackground(new Color(255,204,102));
        }
        if(comp instanceof JPasswordField)
        {
            ((JPasswordField)comp).setBackground(new Color(255,204,102));
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        if(comp instanceof JTextField)
        {
            if(((JTextField)comp).isEditable()) {
                ((JTextField)comp).setBackground(Color.WHITE);
            }
            else {
                ((JTextField)comp).setBackground(new Color(220, 220, 220));
            }
        }
        if(comp instanceof JPasswordField)
        {
            if(((JPasswordField)comp).isEditable()) {
                ((JPasswordField)comp).setBackground(Color.WHITE);
            }
            else {
                ((JPasswordField)comp).setBackground(new Color(220, 220, 220));
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
            
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        JList source = (JList) e.getSource();
        if(source.equals(postalContextView.getPostaRegistrataEventsListRef()) || 
           source.equals(postalContextView.getDataPostaEventsListRef()) || 
           source.equals(postalContextView.getAcquisizioneMazzettiEventsListRef()) ||
           source.equals(postalContextView.getAccettazioneOnlineEventsListRef()))
        {
            if(source.getSelectedIndex()!=-1) {
                selectPostalFunction((Function) source.getModel().getElementAt(source.getSelectedIndex()));
            }
            else {
                deselectPostalFunction();
            }
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        Object source = (Object) e.getSource();
        if(source.equals(postalContextView.getPostalContextTabbedPaneRef()))
        {
            JTabbedPane sourceTabbedPane;
            sourceTabbedPane = (JTabbedPane) e.getSource();
            
            selectPostalContextTab(sourceTabbedPane.getSelectedIndex());
        }

    }
    
    private void selectPostalContextTab(int tabIndex)
    {
        XteTestToolDataModel dataModel = getXteTestToolDataModel();        
        
        try
        {                    
            if(tabIndex == 0)
            {
                //POSTA REGISTRATA
                dataModel.setCurrentPostalContext(PostalContext.POSTA_REGISTRATA);
                updateXteTestToolDataModel(dataModel);
                
                JList postaRegistrataListComponent = postalContextView.getPostaRegistrataEventsListRef();
                if (!postaRegistrataFunctions.isEmpty() && postaRegistrataFunctions != null) {
                    ((DefaultListModel)(postaRegistrataListComponent.getModel())).clear();
                    for (Function postaRegistrataFunction : postaRegistrataFunctions) {
                        ((DefaultListModel)(postaRegistrataListComponent.getModel())).addElement(postaRegistrataFunction);
                    }
                }                
            }

            if(tabIndex == 1)
            {
                //DATA POSTA
                dataModel.setCurrentPostalContext(PostalContext.DATA_POSTA);
                updateXteTestToolDataModel(dataModel);

                JList dataPostaListComponent = postalContextView.getDataPostaEventsListRef();
                if (!dataPostaFunctions.isEmpty() && dataPostaFunctions != null) {
                    ((DefaultListModel)(dataPostaListComponent.getModel())).clear();
                    for (Function dataPostaFunction : dataPostaFunctions) {
                        ((DefaultListModel)(dataPostaListComponent.getModel())).addElement(dataPostaFunction);
                    }
                }                
            }

            if(tabIndex == 2)
            {
                //ACQUISIZIONE MAZZETTI
                dataModel.setCurrentPostalContext(PostalContext.ACQUISIZIONE_MAZZETTI);
                updateXteTestToolDataModel(dataModel);

                JList acquisizioneMazzettiListComponent = postalContextView.getAcquisizioneMazzettiEventsListRef();
                if (!acquisizioneMazzettiFunctions.isEmpty() && acquisizioneMazzettiFunctions != null) {
                    ((DefaultListModel)(acquisizioneMazzettiListComponent.getModel())).clear();
                    for (Function acquisizioneMazzettiFunction : acquisizioneMazzettiFunctions) {
                        ((DefaultListModel)(acquisizioneMazzettiListComponent.getModel())).addElement(acquisizioneMazzettiFunction);
                    }
                }                
            }

            if(tabIndex == 3)
            {
                //ACCETTAZIONE ONLINE
                dataModel.setCurrentPostalContext(PostalContext.ACCETTAZIONE_ONLINE);
                updateXteTestToolDataModel(dataModel);

                JList accettazioneOnlineListComponent = postalContextView.getAccettazioneOnlineEventsListRef();
                if (!accettazioneOnlineFunctions.isEmpty() && accettazioneOnlineFunctions != null) {
                    ((DefaultListModel)(accettazioneOnlineListComponent.getModel())).clear();
                    for (Function accettazioneOnlineFunction : accettazioneOnlineFunctions) {
                        ((DefaultListModel)(accettazioneOnlineListComponent.getModel())).addElement(accettazioneOnlineFunction);
                    }
                }                
            }
        } catch(Exception ex)
        {
            dataModel.setCurrentPostalContext(null);
            updateXteTestToolDataModel(dataModel);
            logger.error("XteTestToolPostalContextController selectPostalContextTab failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Postal Context controller selectPostalContextTab failed", ex);
        }
        
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolPostalContextController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.postalContextView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(postalContextView);
            updateXteTestToolDataModel(dataModel);
            
            
            PostalContext currentPostalContext = dataModel.getCurrentPostalContext();
            if(currentPostalContext==null)
            {
                postalContextView.getPostalContextTabbedPaneRef().setSelectedIndex(0);
                selectPostalContextTab(0);
            }
            else
            {            
                if(currentPostalContext.equals(PostalContext.POSTA_REGISTRATA))
                {
                    postalContextView.getPostalContextTabbedPaneRef().setSelectedIndex(0);
                    selectPostalContextTab(0);
                }
                else if(currentPostalContext.equals(PostalContext.DATA_POSTA))
                {
                    postalContextView.getPostalContextTabbedPaneRef().setSelectedIndex(1);
                    selectPostalContextTab(1);
                }
                else if(currentPostalContext.equals(PostalContext.ACQUISIZIONE_MAZZETTI))
                {
                    postalContextView.getPostalContextTabbedPaneRef().setSelectedIndex(2);
                    selectPostalContextTab(2);
                }
                else if(currentPostalContext.equals(PostalContext.ACCETTAZIONE_ONLINE))
                {
                    postalContextView.getPostalContextTabbedPaneRef().setSelectedIndex(3);
                    selectPostalContextTab(3);
                }
                else
                {
                    postalContextView.getPostalContextTabbedPaneRef().setSelectedIndex(0);
                    selectPostalContextTab(0);
                }
            }            
                       
            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(postalContextView.getOkButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            postalContextView.getPostaRegistrataEventsListRef().requestFocus();
            
            logger.info("XteTestToolPostalContextController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPostalContextController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolPostalContextController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Postal Context controller init failed", ex);
        }
    }

    private void selectPostalFunction(Function selectedPostalFunction)
    {
        try{
            logger.info("XteTestToolPostalContextController start selectPostalFunction");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            logger.debug("XteTestToolPostalContextController selectPostalFunction Postal Function selected = " + selectedPostalFunction.getName());

            List<Component> enabledComponents  = new ArrayList<Component>();
            enabledComponents.add(postalContextView.getOkButtonRef());
            componentStatusManager.enableComponent(enabledComponents);

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentPostalFunction(selectedPostalFunction);
            updateXteTestToolDataModel(dataModel);

            logger.info("XteTestToolPostalContextController selectPostalFunction executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPostalContextController selectPostalFunction failed", baseEx);
            throw baseEx;
        } catch (Exception ex)
        {
            logger.error("XteTestToolPostalContextController selectPostalFunction failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Postal Context controller selection failed", ex);
        }
        
    }

    private void deselectPostalFunction()
    {
        try{
            logger.info("XteTestToolPostalContextController start deselectXteServer");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentPostalFunction(null);
            updateXteTestToolDataModel(dataModel);

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(postalContextView.getOkButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            logger.info("XteTestToolPostalContextController deselectXteServer executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolPostalContextController deselectXteServer failed", baseEx);
            throw baseEx;
        } catch (Exception ex)
        {
            logger.error("XteTestToolPostalContextController deselectXteServer failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Postal Context controller deselection failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolPostalContextController start exit");
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(postalContextView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolPostalContextController exit in progress");
            postalContextView.exit();
        }
    }

    private void home()
    {
        logger.info("XteTestToolPostalContextController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        XteTestToolHomeView homeView = new XteTestToolHomeView(postalContextView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(postalContextView.getPostalContextPanelRef().getParent(), postalContextView.getPostalContextPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }
    
    private void loadPostalFunctionView()
    {
        try {
            logger.info("XteTestToolPostalContextController start loadPostalFunctionView");

            Function selectedPostalFunction = getXteTestToolDataModel().getCurrentPostalFunction();
            PanelLoader panelLoader = PanelLoader.getInstance();
            
            Class viewClass;
            String viewRootPackage = this.postalContextView.getClass().getPackage().getName();        
            String packageExtension = XteTestToolDataModel.getPackageExtByPostaContext(PostalContext.getByCode(selectedPostalFunction.getContext()));            
            String viewPackageFull = viewRootPackage + "." + packageExtension;
            
            logger.debug("XteTestToolPostalContextController loadPostalFunctionView postal function View: " + viewPackageFull + "." + selectedPostalFunction.getViewclass());
            viewClass = XteTestToolPostalContextController.class.getClassLoader().loadClass(viewPackageFull + "." + selectedPostalFunction.getViewclass());
            
            Constructor viewClassConstructor = viewClass.getDeclaredConstructor(XteTestToolApplicationFrameView.class);
            viewClassConstructor.setAccessible(true);
            JPanel postalFunctionView = (JPanel) viewClassConstructor.newInstance(postalContextView.getParentFrame());
            logger.debug("XteTestToolPostalContextController loadPostalFunctionView loaded postal function View: " + postalFunctionView.getUIClassID());

            Class controllerClass;
            String controllerRootPackage = this.getClass().getPackage().getName();        
            String controllerPackageFull = controllerRootPackage + "." + packageExtension;
            
            logger.debug("XteTestToolPostalContextController loadPostalFunctionView postal function Controller: " + controllerPackageFull + "." + selectedPostalFunction.getControllerclass());
            controllerClass = XteTestToolPostalContextController.class.getClassLoader().loadClass(controllerPackageFull + "." + selectedPostalFunction.getControllerclass());
            
            Constructor controllerClassConstructor = controllerClass.getDeclaredConstructor(JPanel.class);
            controllerClassConstructor.setAccessible(true);
            XteTestToolController postalFunctionController = (XteTestToolController) controllerClassConstructor.newInstance(postalFunctionView);
            logger.debug("XteTestToolPostalContextController loadPostalFunctionView loaded postal function Controller: " + postalFunctionController.getClass().getName());

            panelLoader.changePanel(postalContextView.getPostalContextPanelRef().getParent(), postalContextView.getPostalContextPanelRef(), postalFunctionView, new Rectangle(0,70,1000,690));
        
            logger.info("XteTestToolPostalContextController loadPostalFunctionView executed successfully");
            
        } catch (InstantiationException ex) {
            logger.error("XteTestToolPostalContextController loadPostalFunctionView Instantiation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Postal Function view/controller instantiation failed", ex);
        } catch (IllegalAccessException ex) {
            logger.error("XteTestToolPostalContextController loadPostalFunctionView IllegalAccess", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Postal Function view/controller illegal access", ex);
        } catch (IllegalArgumentException ex) {
            logger.error("XteTestToolPostalContextController loadPostalFunctionView IllegalArgument", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Postal Function view/controller illegal argument", ex);
        } catch (InvocationTargetException ex) {
            logger.error("XteTestToolPostalContextController loadPostalFunctionView InvocationTarget", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Postal Function view/controller invocation target failed", ex);
        } catch (NoSuchMethodException ex) {
            logger.error("XteTestToolPostalContextController loadPostalFunctionView NoSuchMethod", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Postal Function view/controller no such method", ex);
        } catch (SecurityException ex) {
            logger.error("XteTestToolPostalContextController loadPostalFunctionView Security problem", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Postal Function view/controller security problem", ex);
        } catch (ClassNotFoundException ex) {
            logger.error("XteTestToolPostalContextController loadPostalFunctionView ClassNotFound", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Postal Function view/controller class not found", ex);
        }
    }

}
