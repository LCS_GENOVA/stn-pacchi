/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectmodel;

 
import com.selexelsag.xte.testtool.datamodel.XteScaricoPezzi;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Tassara
 */
public class XteScaricoPezziTableModel extends DefaultTableModel {

    private String[] columnNames = new String[]{
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.codice"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.prodotto"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.dataora"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.peso"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.prezzo"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.percetto"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.destinazione"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.destinatario"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.indirizzo"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.cap"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.nomemittente"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.destinazionemittente"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.indirizzomittente"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.capmittente"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.sa"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.codicear"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.valoreassicurato"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.importocontrassegno"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.iban"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.oratimedefinite"),
        ResourceBundle.getBundle("bundles/messages").getString("scaricopezzi.table.header.smarrito"),
    };

    private Map<String, XteScaricoPezzi> map = new HashMap<String, XteScaricoPezzi>();
    
    public XteScaricoPezziTableModel() {

        super();
        this.setColumnCount(columnNames.length);
        this.setColumnIdentifiers(columnNames);

    }

    public void addScaricoPezziItem(XteScaricoPezzi item) throws XteTestToolDuplicateKeyException{
        if(map.containsKey(item.getScaricoPezziCode()))
        {
            throw new XteTestToolDuplicateKeyException(Group.GENERIC, Severity.WARNING, "Scarico Pezzi Table Model duplicate key exception");
        }
        else
        {
            addRow(new Object[]{item.getScaricoPezziCode(), item.getScaricoPezziProduct(), item.getScaricoPezziDateTime(), item.getScaricoPezziPeso(), item.getScaricoPezziPrezzo(), item.getScaricoPezziPercetto(), item.getScaricoPezziDestinazione(), item.getScaricoPezziDestinatario(), item.getScaricoPezziIndirizzo(), item.getScaricoPezziCAP(), item.getScaricoPezziNomeMitt(), item.getScaricoPezziDestMitt(), item.getScaricoPezziIndMitt(), item.getScaricoPezziCAPMitt(), item.getScaricoPezziSA(), item.getScaricoPezziCodAR(), item.getScaricoPezziValAss(), item.getScaricoPezziImpContr(), item.getScaricoPezziIBAN(), item.getScaricoPezziOraTimeDef(), item.getScaricoPezziSmarrito()});
            map.put(item.getScaricoPezziCode(), item);            
        }        
    }
    

    public void setModelData(Map<String, XteScaricoPezzi> items) {
        getDataVector().clear();
        map.clear();                
        for (XteScaricoPezzi item : items.values()) {
            addRow(new Object[]{item.getScaricoPezziCode(), item.getScaricoPezziProduct(), item.getScaricoPezziDateTime(), item.getScaricoPezziPeso(), item.getScaricoPezziPrezzo(), item.getScaricoPezziPercetto(), item.getScaricoPezziDestinazione(), item.getScaricoPezziDestinatario(), item.getScaricoPezziIndirizzo(), item.getScaricoPezziCAP(), item.getScaricoPezziNomeMitt(), item.getScaricoPezziDestMitt(), item.getScaricoPezziIndMitt(), item.getScaricoPezziCAPMitt(), item.getScaricoPezziSA(), item.getScaricoPezziCodAR(), item.getScaricoPezziValAss(), item.getScaricoPezziImpContr(), item.getScaricoPezziIBAN(), item.getScaricoPezziOraTimeDef(), item.getScaricoPezziSmarrito()});
            map.put(item.getScaricoPezziCode(), item);
        }
    }

    public XteScaricoPezzi getSelectedItem(int rowIndex) {
        
        String ScaricoPezziCode = (String) getValueAt(rowIndex, 0);
        if(map.containsKey(ScaricoPezziCode))
        {
            return map.get(ScaricoPezziCode);
        }
        else
        {
            return null;
        }
    }

    @Override
    public void removeRow(int row) {
        String ScaricoPezziCode = (String) getValueAt(row, 0);
        if(map.containsKey(ScaricoPezziCode))
        {
            map.remove(ScaricoPezziCode);
        }        
        getDataVector().removeElementAt(row);
        fireTableDataChanged();
    }

    public void clearModel() {
        getDataVector().clear();
        map.clear();
    }

    public Map<String, XteScaricoPezzi> getMap() {
        return map;
    }

    
    @Override
    public boolean isCellEditable(int row, int column) {
        return (false);
    }
    
}
