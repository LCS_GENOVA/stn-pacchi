/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.datamodel;


/**
 *
 * @author Da Procida
 */
public class XteOffice {

    private String ufficio;
    private Long numeroMazzetti;
    private Long numeroInvii;
    private Boolean stato;
    private String progresso;

    public String getUfficio() {
        return ufficio;
    }

    public void setUfficio(String ufficio) {
        this.ufficio = ufficio;
    }

    public Long getNumeroMazzetti() {
        return numeroMazzetti;
    }

    public void setNumeroMazzetti(Long numeroMazzetti) {
        this.numeroMazzetti = numeroMazzetti;
    }

    public Long getNumeroInvii() {
        return numeroInvii;
    }

    public void setNumeroInvii(Long numeroInvii) {
        this.numeroInvii = numeroInvii;
    }

    public Boolean getStato() {
        return stato;
    }

    public void setStato(Boolean stato) {
        this.stato = stato;
    }

    public String getProgresso() {
        return progresso;
    }

    public void setProgresso(String progresso) {
        this.progresso = progresso;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.ufficio != null ? this.ufficio.hashCode() : 0);
        hash = 53 * hash + (this.numeroMazzetti != null ? this.numeroMazzetti.hashCode() : 0);
        hash = 53 * hash + (this.numeroInvii != null ? this.numeroInvii.hashCode() : 0);
        hash = 53 * hash + (this.stato != null ? this.stato.hashCode() : 0);
        hash = 53 * hash + (this.progresso != null ? this.progresso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final XteOffice other = (XteOffice) obj;
        if ((this.ufficio == null) ? (other.ufficio != null) : !this.ufficio.equals(other.ufficio)) {
            return false;
        }
        if (this.numeroMazzetti != other.numeroMazzetti && (this.numeroMazzetti == null || !this.numeroMazzetti.equals(other.numeroMazzetti))) {
            return false;
        }
        if (this.numeroInvii != other.numeroInvii && (this.numeroInvii == null || !this.numeroInvii.equals(other.numeroInvii))) {
            return false;
        }
        if (this.stato != other.stato && (this.stato == null || !this.stato.equals(other.stato))) {
            return false;
        }
        if ((this.progresso == null) ? (other.progresso != null) : !this.progresso.equals(other.progresso)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "XteOffice{" + "ufficio=" + ufficio + ", numeroMazzetti=" + numeroMazzetti + ", numeroInvii=" + numeroInvii + ", stato=" + stato + ", progresso=" + progresso + '}';
    }

}
