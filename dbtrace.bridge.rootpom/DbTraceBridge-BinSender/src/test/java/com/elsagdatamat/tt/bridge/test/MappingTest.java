package com.elsagdatamat.tt.bridge.test; 

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.selexelsag.tt.dbtrace.bridge.sender.impl.MessageDestination;
import com.selexelsag.tt.dbtrace.bridge.serial.object.Trace;
import com.selexelsag.tt.dbtrace.bridge.serial.object.TraceDetail;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
public class MappingTest {

	@Resource
	private MessageDestination messageDestination;
	
	@Test
	public void createMessageTest(){
		try {
			
			Trace tracciaSDA = creaTracciaSDA(); 
			messageDestination.send(tracciaSDA, "fromSDA");
//			messageDestination.send("ciao", "fromSDA");
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	// Esempio di traccia SDA
	private Trace creaTracciaSDA(){
		Trace trace = new Trace();
		trace.setTracedEntity("PACCO");
		trace.setIdTracedEntity("PACCO1-PROVA-JMS");
		trace.setWhatHappened("TRA_SDA");
		trace.setWhenHappened(new Date());
		trace.setWhenRegistered(new Date());
		trace.setWhereHappened("SDA");
		trace.setChannel("SDA");
		trace.setIdChannel("12345");
		trace.setServiceName("TracciaturaPacchi");
		trace.setLabelTracedEntity("POE");
		trace.setIdStatus("R");
		trace.setIdCorrelazione("");
		trace.setIdTracedExternal("");
		trace.setIdForwardTo("");
		trace.setSysForwardTo("");
		
		TraceDetail td1 = new TraceDetail();
		td1.setParamClass("DESTINATION_TYPE");
		td1.setParamValue("FS");
		
		TraceDetail td2 = new TraceDetail();
		td2.setParamClass("WHERE_DESCRIPTION");
		td2.setParamValue("descrizione 12345");
		
		TraceDetail td3 = new TraceDetail();
		td3.setParamClass("DESTINATION_DESCR");
		td3.setParamValue("12347");
		
		Set<TraceDetail> tds = new HashSet<TraceDetail>(0);
		tds.add(td1);
		tds.add(td2);
		tds.add(td3);
	
		trace.setInternalDetailList(tds);
		
		return trace;
	}
}
