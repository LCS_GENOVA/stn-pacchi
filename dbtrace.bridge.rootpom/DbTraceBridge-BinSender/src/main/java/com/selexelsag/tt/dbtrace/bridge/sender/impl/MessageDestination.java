package com.selexelsag.tt.dbtrace.bridge.sender.impl;


import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.selexelsag.tt.dbtrace.bridge.properties.MessageProperties;
import com.selexelsag.tt.dbtrace.bridge.sender.IMessageDestination;
import com.selexelsag.tt.dbtrace.bridge.sender.update.core.Configuration;
import com.selexelsag.tt.dbtrace.bridge.sender.update.core.ProductKey;

public class MessageDestination implements IMessageDestination {	
	
	private JmsTemplate      	jmsTemplate;
	private Configuration 		configuration;
	
	/**
	 * Metodo invocato per inviare un messaggio jms (objectMessage contenente il bean SerialTrace con i relativi dettagli)
	 * 
	 * @param obj - contenuto del messaggio da inviare
	 * @param externalSystem - scodatore o sistema che invia il messaggio
	 */
	public void send(final Serializable obj, final String externalSystem) {

		jmsTemplate.send(new MessageCreator()
		{
			public Message createMessage(Session session) throws JMSException {
				
				ObjectMessage objectMessage = session.createObjectMessage();
				objectMessage.setJMSCorrelationID(UUID.randomUUID().toString());
	
				objectMessage.setStringProperty(MessageProperties.EXTERNAL_SYSTEM_LABEL_PROPERTY, externalSystem);
				objectMessage.setStringProperty(MessageProperties.MESSAGE_TYPE_PROPERTY, obj.getClass().getName());
				
				Date JMSDate = new Date();
				objectMessage.setJMSTimestamp(JMSDate.getTime());

				objectMessage.setObject(obj);
			
				return objectMessage;
			}
		});

	}
	
	
	/**
	 * Metodo invocato per inviare un messaggio testuale jms (objectMessage contenente il bean SerialTrace con i relativi dettagli)
	 * 
	 * @param obj - contenuto del messaggio da inviare
	 * @param externalSystem - scodatore o sistema che invia il messaggio
	 */
	public void send(final String msg, final String externalSystem) {

		jmsTemplate.send(new MessageCreator()
		{
			public Message createMessage(Session session) throws JMSException {
				
				TextMessage textMessage = session.createTextMessage();
				textMessage.setJMSCorrelationID(UUID.randomUUID().toString());
	
				textMessage.setStringProperty(MessageProperties.EXTERNAL_SYSTEM_LABEL_PROPERTY, externalSystem);
				textMessage.setStringProperty(MessageProperties.MESSAGE_TYPE_PROPERTY, msg.getClass().getName());
				
				Date JMSDate = new Date();
				textMessage.setJMSTimestamp(JMSDate.getTime());

				textMessage.setText(msg);
			
				return textMessage;
			}
		});

	}
	
	/**
	 * Metodo il valore del flag che indica se attivare o meno il canale di comunicazione con il cruscotto pacchi
	 * 
	 * @return boolean - true se il valore dul db � true, false se il valore sul db � false
	 * 					 oppure se la configurazione non � presente sul DB
	 */
	public boolean isFlagSender() {
		return configuration.isFlagSender();
	}
	
	/**
	 * Metodo il valore del flag che indica se attivare o meno il canale di comunicazione con l'anagrafica
	 * 
	 * @return boolean - true se il valore del db � true, false se il valore sul db � false
	 * 					 oppure se la configurazione non � presente sul DB
	 */
	public boolean isFlagAnagraphicSender() {
		return configuration.isFlagAnagraphicSender();
	}
	
	/**
	 * Metodo che restituisce la configurazione di un prodotto
	 * 
	 * @param pk - chiave dell'hashmap (prodotto, flusso, tipo_traccia)
	 * @return boolean - true se la traccia � utile al cruscotto, false se la traccia non � utile al cruscotto 
	 * 					 oppure se la configurazione non � presente sul DB
	 */
	public boolean getProductConfig(ProductKey key) {
		return configuration.getProductConfig(key);
	}
	
	/**
	 *  Metodo init chiamato in fase di costruzione dell'incodatore (Spring injection)
	 *
	 */
	public void init(){		
		configuration.loadConfiguration();
	}
	
	/*
	 *  Spring injection
	 */
	public JmsTemplate getJmsTemplate() {
		return jmsTemplate;
	}

	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
	
}