package com.elsagdatamat.dbtrace.bridge.engine.entities;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;


/** 
* R.L. BeanGenerator - 22.05.2012 03:10:16
*/ 
@Entity
@Table(name="MOD_PAG_CONTR_PI_SDA")
@IdClass(ModPagContrPiSdaPK.class)
public class ModPagContrPiSda implements Serializable {

	private static final long serialVersionUID = 2991649696276408497L;

	@Id
	@Column(name = "PI_CODE", nullable =false)
	private String piCode;

	@Column(name = "SDA_CODE", nullable =false)
	private String sdaCode;

	@Id
	@Column(name = "PI_CHANNEL", nullable =false)
	private String piChannel;
	
	@Column(name = "REVERSE_MAPPING", nullable =false)
	private int reverseMapping;

	public int getReverseMapping(){
		return reverseMapping;
	}

	public String getPiCode(){
		return piCode;
	}

	public String getSdaCode(){
		return sdaCode;
	}

	public void setReverseMapping(int reverseMapping){
		this.reverseMapping=reverseMapping;
	}

	public void setPiCode(String piCode){
		this.piCode=piCode;
	}

	public void setSdaCode(String sdaCode){
		this.sdaCode=sdaCode;
	}

	public String getPiChannel() {
		return piChannel;
	}

	public void setPiChannel(String piChannel) {
		this.piChannel = piChannel;
	}

}