package com.elsagdatamat.tt.trackdbws.beans;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "DUPLICATED_TRACEDETAILS")
public class DuplicatedTraceDetail implements Serializable {	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="ID", nullable=false)
	@SequenceGenerator(name="DTD_SEQ", sequenceName="DTD_SEQ", allocationSize=50)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DTD_SEQ")
	private Long traceDetailId;
	
	@Column(name="PARAM_CLASS", nullable=false)
	private String paramClass;
	
	@Column(name="PARAM_VALUE", nullable=false)
	private String paramValue;
	
	@ManyToOne
	@JoinColumn(name="TRA_ID", nullable=false)
	private DuplicatedTrace duplicatedTrace;

	public Long getTraceDetailId() {
		return traceDetailId;
	}

	public void setTraceDetailId(Long traceDetailId) {
		this.traceDetailId = traceDetailId;
	}

	public String getParamClass() {
		return paramClass;
	}

	public void setParamClass(String paramClass) {
		this.paramClass = paramClass;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public DuplicatedTrace getDuplicatedTrace() {
		return duplicatedTrace;
	}

	public void setDuplicatedTrace(DuplicatedTrace duplicatedTrace) {
		this.duplicatedTrace = duplicatedTrace;
	}

}
