package com.selexelsag.dbtrace.bridge.to.ssip.test.message;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import junit.framework.Assert;

import org.junit.Test;

import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Header;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Traces;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Traces.Trace;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory;
import com.selexelsag.dbtrace.bridge.to.ssip.message.trans.NullableObjectFactoryDecorator;

public class MessageFactoryTest {

	@Test
    public void marshalledSSIPMessageToXmlAndUnmarshalledAreIdentical() {
		Message ssipMessage;
		try {
			ssipMessage = createSSIPJaxbMessage();
 
	        String xmlDocument = marshallMessageToXml(ssipMessage);
	        Assert.assertNotNull(xmlDocument);
	        Assert.assertTrue(xmlDocument.contains("PACCHI"));
	        
	        System.out.println(xmlDocument);
	 
	        Message unmarshalledMessage = unmarshallXmlToMessage(xmlDocument);
	        Assert.assertEquals(ssipMessage.getHeader().getSERVICE(), unmarshalledMessage.getHeader().getSERVICE());
	        Assert.assertEquals(ssipMessage.getHeader().getCHANNEL(), unmarshalledMessage.getHeader().getCHANNEL());
	        Assert.assertEquals(ssipMessage.getHeader().getMSGID(), unmarshalledMessage.getHeader().getMSGID());
	        Assert.assertEquals(ssipMessage.getHeader().getMSGTYPE(), unmarshalledMessage.getHeader().getMSGTYPE());
	        Assert.assertEquals(ssipMessage.getHeader().getMSGDATE(), unmarshalledMessage.getHeader().getMSGDATE());
	        Assert.assertEquals(ssipMessage.getHeader().getVERSION(), unmarshalledMessage.getHeader().getVERSION());
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new AssertionError(ex);
		}
    }
 
	@Test
    public void marshalledSSIPMessageWithNullsDontEncodeNullTags() {
		Message ssipMessage;
		try {
			ssipMessage = createSSIPJaxbMessage();
 
	        String xmlDocument = marshallMessageToXml(ssipMessage);
	        Assert.assertNotNull(xmlDocument);
	        Assert.assertTrue(xmlDocument.contains("PACCHI"));
	        
	        System.out.println(xmlDocument);
	 
	        Message unmarshalledMessage = unmarshallXmlToMessage(xmlDocument);
	        Assert.assertEquals(ssipMessage.getHeader().getSERVICE(), unmarshalledMessage.getHeader().getSERVICE());
	        Assert.assertEquals(ssipMessage.getHeader().getCHANNEL(), unmarshalledMessage.getHeader().getCHANNEL());
	        Assert.assertEquals(ssipMessage.getHeader().getMSGID(), unmarshalledMessage.getHeader().getMSGID());
	        Assert.assertEquals(ssipMessage.getHeader().getMSGTYPE(), unmarshalledMessage.getHeader().getMSGTYPE());
	        Assert.assertEquals(ssipMessage.getHeader().getMSGDATE(), unmarshalledMessage.getHeader().getMSGDATE());
	        Assert.assertEquals(ssipMessage.getHeader().getVERSION(), unmarshalledMessage.getHeader().getVERSION());
	        
	        Assert.assertNull(ssipMessage.getTraces().getTrace().get(1).getWHEREHAPPENEDDETAIL());
	        Assert.assertFalse(xmlDocument.contains("<WHERE_HAPPENED_DETAIL>"));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new AssertionError(ex);
		}
    }


    public Message createSSIPJaxbMessage() throws DatatypeConfigurationException {
    	NullableObjectFactoryDecorator objFactory = new NullableObjectFactoryDecorator(new ObjectFactory());
		Message ssipMessage = objFactory.createMessage();
 
		Header msgHeader = objFactory.createMessageHeader();
		msgHeader.setSERVICE("PACCHI");
		msgHeader.setCHANNEL("SSIP");
		msgHeader.setMSGID("1234567890ABCDEFG");
		msgHeader.setMSGTYPE("TPA");
		
		GregorianCalendar now = (GregorianCalendar)GregorianCalendar.getInstance();
		XMLGregorianCalendar msgDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(now);
		
		msgHeader.setMSGDATE(msgDate);
		msgHeader.setVERSION("1.0");
		
		Traces msgTraces = objFactory.createMessageTraces();
		
		Trace msgTrace1 = objFactory.createMessageTracesTrace();
		msgTrace1.setIDDISPACCIO(objFactory.createMessageTracesTraceIDDISPACCIO("UPU20CHARSCODE-1"));
		msgTrace1.setIDSTATUS("AUP");
		msgTrace1.setTRACEDENTITY("PACCO");
		msgTrace1.setLABELTRACEDENTITY("POE");
		msgTrace1.setWHATHAPPENED("ACC_NSP");
		msgTrace1.setWHEREHAPPENED("UPOS");
		msgTrace1.setWHENHAPPENED(msgDate);

		Trace msgTrace2 = objFactory.createMessageTracesTrace();
		msgTrace2.setIDDISPACCIO(objFactory.createMessageTracesTraceIDDISPACCIO("UPU20CHARSCODE-2"));
		msgTrace2.setIDSTATUS("SPE");
		msgTrace2.setTRACEDENTITY("PACCO");
		msgTrace2.setLABELTRACEDENTITY("EPG");
		msgTrace2.setWHATHAPPENED("SSIP_TO_TT");
		msgTrace2.setWHEREHAPPENED("FSDA");
		msgTrace2.setWHENHAPPENED(msgDate);
		msgTrace2.setWHEREHAPPENEDDETAIL(null);

		msgTraces.getTrace().add(msgTrace1);
		msgTraces.getTrace().add(msgTrace2);
		
		ssipMessage.setHeader(msgHeader);
		ssipMessage.setTraces(msgTraces);
 
        return ssipMessage;
    }

    public Message unmarshallXmlToMessage(String xmlDocument) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Message.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        StringReader reader = new StringReader(xmlDocument);
        Message unmarshalledMessage = (Message) unmarshaller.unmarshal(reader);
        return unmarshalledMessage;
    } 

    public String marshallMessageToXml(Message ssipMessage) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Message.class);
        Marshaller marshaller = context.createMarshaller();
        StringWriter writer = new StringWriter();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(ssipMessage, writer);
        return writer.toString();
    }
}
