package com.elsagdatamat.dbtrace.bridge.tocdg;

import javax.xml.bind.annotation.XmlElement;

public class CdGACC {
	
	
/*
PERC 
ACURR
REGT

 */
	@XmlElement(name = "PERC")
	public String perc;

	@XmlElement(name = "ACURR")
	public String acurr;
	
	@XmlElement(name = "REGT")
	public String regt;
	
	public CdGACC()
	{
	}
}