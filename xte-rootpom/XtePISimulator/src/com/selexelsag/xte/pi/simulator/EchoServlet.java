package com.selexelsag.xte.pi.simulator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.Socket;
import java.net.URL;
import java.util.Enumeration;

import javax.net.SocketFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EchoServlet
 */
public class EchoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private String response01;
	private String response02;
	
	private static String getResourceContent(String resourceName) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader  in;
		try {
			URL url = new URL(resourceName);
			in = new BufferedReader(
					new InputStreamReader(
							url.openStream()));
		}catch (NullPointerException npe) {
			return "Risorsa " + resourceName + " non trovata";
		}
		if (in!=null) {
			char[] cbuf = new char[1024];
			while (in.ready()) {
				int cbufsize = in.read(cbuf);
				sb.append(cbuf, 0, cbufsize);
			}
			in.close();
		}
		return sb.toString();
	}
	
	{
		try {
			response01 = getResourceContent("http://localhost:8081/XtePISimulator/data/response_01.txt");
			response02 = getResourceContent("http://localhost:8081/XtePISimulator/data/response_02.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public EchoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private void manageRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	Enumeration<String> paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {
			System.out.println("param: " + paramNames.nextElement());
		}
		
    	
		PrintWriter writer = response.getWriter();
		String xmlContent = request.getParameter("XML");
		String officeId = request.getParameter("OFF_ID");
				
		StringReader reader = new StringReader(response02);
		char[] buffer = new char[1024];
		
		while (reader.ready()) {
			int charsRead = reader.read(buffer);
			if (charsRead==-1)
				break;
			writer.write(buffer, 0, charsRead);
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doGet " + request + ", " + response);
		manageRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost " + request + ", " + response);
		manageRequest(request, response);
	}

}
