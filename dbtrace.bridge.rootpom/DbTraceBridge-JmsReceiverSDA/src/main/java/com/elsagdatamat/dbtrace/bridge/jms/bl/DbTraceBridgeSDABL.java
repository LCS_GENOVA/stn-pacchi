package com.elsagdatamat.dbtrace.bridge.jms.bl;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.BeanUtils;

import com.elsagdatamat.dbtrace.VO.PkConServiziAggiuntiviMapVO;
import com.elsagdatamat.dbtrace.bl.PkConServiziAggiuntiviBL;
import com.elsagdatamat.dbtrace.bl.PkPrdProdottiSdaVWBL;
import com.elsagdatamat.dbtrace.bridge.engine.CustomTraceEntity;
import com.elsagdatamat.dbtrace.bridge.engine.bl.AptItemBL;
import com.elsagdatamat.dbtrace.bridge.engine.bl.AptItemPositionBL;
import com.elsagdatamat.dbtrace.bridge.engine.bl.WhatHappenedNotOnDBT;
import com.elsagdatamat.dbtrace.bridge.engine.bl.WhatHappenedOnDBTOnTT;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItem;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition.AptStatusEnum;
import com.elsagdatamat.dbtrace.bridge.engine.exceptions.InsertTracesException;
import com.elsagdatamat.dbtrace.bridge.engine.exceptions.TrackDBRWException;
import com.elsagdatamat.dbtrace.fromsda.bean.HEADER;
import com.elsagdatamat.dbtrace.fromsda.bean.Message;
import com.elsagdatamat.dbtrace.fromsda.bean.MessageJson;
import com.elsagdatamat.dbtrace.fromsda.bean.TRACE;
import com.elsagdatamat.framework.entities.EntityCreationException;
import com.elsagdatamat.framework.entities.IEntityBuilder;
import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.elsagdatamat.tt.trackdbws.beans.TraceDetail;
import com.elsagdatamat.tt.trackdbws.bl.ITraceBL;
import com.selexelsag.tracktrace.extension.managers.AdditionalServiceManager;
import com.selexelsag.tracktrace.extension.managers.MailpieceManager;
import com.selexelsag.tt.dbtrace.bridge.sender.impl.MessageDestination;
import com.selexelsag.tt.dbtrace.bridge.sender.update.core.ProductKey;

public class DbTraceBridgeSDABL {

	private static final String PARAMETERS_DETAIL_CLASS = "PARAMETERS";
	private static final String ADDITIONAL_SERVICE_DETAIL_CLASS = "SA";
	private static final String TIPO_DIGIPOD_SDA = "111";
	private static final String PROD_TYPE_PKR = "PKR";
	private static final String JSON_FORMAT = "json";
	private static final String FRAZIONARIO_99999 = "99999";
	private static final String SEPARATORE_SERVIZI_AGGIUNTIVI = ",";
	
	//queste tag devono essere uguali a quelle del messaggio json altrimenti non vengono trovate (nei dettagli delle tracce) quando viene creato l'oggetto da inserire in APT_ITEM
	private static final String barcode_servizio = "BARCODE_SERVIZIO";
	private static final String dim_L ="DIM_L";
	private static final String dim_W = "DIM_W";
	private static final String dim_H = "DIM_H";
	private static final String peso_colli = "PESO_COLLI";
	private static final String dest_intestatario = "DEST_INTESTATARIO";
	private static final String dest_email = "DEST_EMAIL";
	private static final String dest_telefono = "DEST_TELEFONO";
	
	// R.L. - 05.2012
	private MessageDestination messageDestination;
	private MessageDestinationSDA messageDestinationAnagraphic;
	private boolean flagSender;
	private boolean flagAnagraphicSender;
	private AdditionalServiceManager additionalServiceManager;
	private MailpieceManager mailpieceManager;
	// R.L. - 062014 - chioschi (scrittura su SC per i messaggi PRE_SDA di sda)
	private AptItemBL aptItemBL;

	private AptItemPositionBL aptItemPositionBL;
	private PkPrdProdottiSdaVWBL pkPrdProdottiSdaVWBL;
	PkConServiziAggiuntiviBL pkConServiziAggiuntiviBL;
	

	protected static Log log = LogFactory.getLog(DbTraceBridgeSDABL.class);

	private IEntityBuilder<StringReader, List<CustomTraceEntity>> traceBuilder;

	private ITraceBL traceBL;

	public AptItemBL getAptItemBL() {
		return aptItemBL;
	}

	public void setAptItemBL(AptItemBL aptItemBL) {
		this.aptItemBL = aptItemBL;
	}

	public AptItemPositionBL getAptItemPositionBL() {
		return aptItemPositionBL;
	}

	public void setAptItemPositionBL(AptItemPositionBL aptItemPositionBL) {
		this.aptItemPositionBL = aptItemPositionBL;
	} 

	public MailpieceManager getMailpieceManager() {
		return mailpieceManager;
	}

	public void setMailpieceManager(MailpieceManager mailpieceManager) {
		this.mailpieceManager = mailpieceManager;
	}

	public ITraceBL getTraceBL() {
		return traceBL;
	}

	public AdditionalServiceManager getAdditionalServiceManager() {
		return additionalServiceManager;
	}

	public void setAdditionalServiceManager(AdditionalServiceManager additionalServiceManager) {
		this.additionalServiceManager = additionalServiceManager;
	}

	public void setTraceBL(ITraceBL traceBL) {
		this.traceBL = traceBL;
	}

	public void setTraceBuilder(IEntityBuilder<StringReader, List<CustomTraceEntity>> traceBuilder) {
		this.traceBuilder = traceBuilder;
	}

	public IEntityBuilder<StringReader, List<CustomTraceEntity>> getTraceBuilder() {
		return traceBuilder;
	}

	public MessageDestination getMessageDestination() {
		return messageDestination;
	}

	public void setMessageDestination(MessageDestination messageDestination) {
		this.messageDestination = messageDestination;
	}

	public void init() {
		log.info("Initializing DbTraceBridgeManager...");

		if (traceBL == null)
			throw new NullPointerException("traceBL not set");
		if (traceBuilder == null)
			throw new NullPointerException("traceBuilder not set");

		// Aggiunt R.L. - CruscottoPacchi - 05.2012
		// if (messageDestination == null)
		// throw new NullPointerException("messageDestination not set");
		log.info("DbTraceBridgeManager initialized");
	}

	/***
	 * 
	 * @param json
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	private MessageJson deserializeJson(String json ) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
			
			MessageJson messageJson = mapper.readValue(json, MessageJson.class);
			return messageJson;

	}
	
	/***
	 * 
	 * @param input
	 * @param jmsFormatProperty
	 * @throws InsertTracesException
	 * @throws TrackDBRWException
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 * @throws JAXBException 
	 */
	public void insertTraces(String input, String jmsFormatProperty) throws InsertTracesException, TrackDBRWException, JsonParseException, JsonMappingException, IOException, JAXBException {		
		MessageJson messageJson = null;
		Message msg = null;
		
		Map<String,Message> msgAnagraphicMap = new HashMap<String,Message>();
		
		Message msgAnagraphic0 = new Message();
		
		if (input == null)
			throw new IllegalArgumentException("Input is null");

		log.info("Message received: " + input.toString());
   
		HashMap<String, String> hashProdottiSda = pkPrdProdottiSdaVWBL.getAll();
		List<CustomTraceEntity> lst = null;
		if(DbTraceBridgeSDABL.JSON_FORMAT.equalsIgnoreCase(jmsFormatProperty)) {
			messageJson = deserializeJson(input);
			if(messageJson==null) {
				return;
			}		
			msgAnagraphic0.setHeader(messageJson.getHEADER());
			if(messageJson!=null && (messageJson.getTRACES()!=null))
			{
				lst = generateCustumTraceFromJSONBean(messageJson);
			}
		}
		else {
			msg = unmarshal(input);
			if(msg==null) {
				return;
			}
			msgAnagraphic0.setHeader(msg.getHeader());
			try {
				synchronized (traceBuilder) {
					lst = traceBuilder.create(new StringReader(input));
				}
			} catch (EntityCreationException e) {
				throw new InsertTracesException("Impossibile creare le tracce", e);
			}
		}
		
		if ((lst == null) || lst.isEmpty()) {
			throw new InsertTracesException("Nessuna traccia estraibile dall'input");
		}
		
		flagSender = false;
		if (messageDestination != null)
			flagSender = messageDestination.isFlagSender(); // Controllo

		flagAnagraphicSender = false;
		if (messageDestinationAnagraphic != null)
			flagAnagraphicSender = messageDestinationAnagraphic.isFlagAnagraphicSender(); // Controllo
		
		long lResult = 0;
		long lSent = 0;
		List<Trace> traces = createObjs(lst);
		ArrayList<com.selexelsag.tt.dbtrace.bridge.serial.object.Trace> serialTraceArrayList = null;
		try {
			serialTraceArrayList = new ArrayList<com.selexelsag.tt.dbtrace.bridge.serial.object.Trace>(0);
			int numTraces = traces.size();
			for (int i=0; i<numTraces;i++) {
				Trace trace = traces.get(i);
				
				if(flagAnagraphicSender && checkSendToAnagraphic(trace, hashProdottiSda)) {
					
					String whatHappened = trace.getWhatHappened();
					
					Message msgAnagraphic = msgAnagraphicMap.get(whatHappened);
					
					msgAnagraphic = newMessageAnagraphic(msgAnagraphicMap,
							msgAnagraphic0, whatHappened, msgAnagraphic);
					
					if(msg!=null) {
						msgAnagraphic.getTraces().getTrace().add(msg.getTraces().getTrace().get(i));
					} else {
						msgAnagraphic = convertJSONBean2XMLBean(msgAnagraphic, messageJson.getHEADER(), messageJson.getTRACES().get(i));	
					}
				}
				handlerRTZ_APT(trace);  
				
				boolean insertedT = traceBL.insertTrace(trace);
				handlerUFI_SDA(trace);
				if (insertedT) {
					lResult++;

				if (flagSender) {
					ProductKey key = new ProductKey(trace.getLabelTracedEntity(), trace.getChannel(), trace.getWhatHappened());
					boolean isAttivata = false;
					if (messageDestination != null)
						isAttivata = messageDestination.getProductConfig(key);

						if (isAttivata) {
							serialTraceArrayList.add(convertTrace2SerialTrace(trace));
							++lSent;
						}
					}
				}
			}
			if (flagSender && serialTraceArrayList != null && serialTraceArrayList.size() > 0)
				sendListOfSerialTrace(serialTraceArrayList);
			
			for (Map.Entry<String, Message> entry : msgAnagraphicMap.entrySet())
			{
				if(flagAnagraphicSender && existTraces(entry)) {
					sendToAnagraphic(marshal(entry.getValue()), entry.getKey());
				}	
			}
			
		} catch (Exception e) {
			throw new TrackDBRWException("Errore durante l'inserimento delle tracce", e);
		}
		log.info("Inserite [" + lResult + "]. Inviate in una sola lista [" + lSent + "]. Previste [" + lst.size() + "]");
	}

	/***
	 * 
	 * @param msgAnagraphicMap
	 * @param msgAnagraphic0
	 * @param whatHappened
	 * @param msgAnagraphic
	 * @return
	 */
	private Message newMessageAnagraphic(Map<String, Message> msgAnagraphicMap,
			Message msgAnagraphic0, String whatHappened, Message msgAnagraphic) {
		if(msgAnagraphic==null)
		{
			msgAnagraphic = new Message();
			msgAnagraphic.setHeader(msgAnagraphic0.getHeader());
			msgAnagraphic.getHeader().setMSGID(whatHappened);
			msgAnagraphic.setTraces(new Message.Traces());
			msgAnagraphicMap.put(whatHappened, msgAnagraphic);
		}
		return msgAnagraphic;
	}

	
	/***
	 * 
	 * @param entry
	 * @return
	 */
	boolean existTraces (Map.Entry<String, Message> entry)
	{
		return (entry.getValue() != null) &&
		(entry.getValue().getTraces() != null ) &&
		(entry.getValue().getTraces().getTrace() != null ) &&
	    (entry.getValue().getTraces().getTrace().size()>0);
	}
	
	
	/***
	 * 
	 * @param trace
	 */
	private void handlerUFI_SDA(Trace trace) {
		try {
			if (WhatHappenedOnDBTOnTT.contains(trace.getWhatHappened())) {
				String WHERE_DESCRIPTION = trace.getDetailList().get("WHERE_DESCRIPTION");
				if (WHERE_DESCRIPTION != null && WHERE_DESCRIPTION.equals("RTZ")) {
					log.info("Pacco RTZ. Codice ["+trace.getIdTracedEntity()+"] WhatHappened ["+trace.getWhatHappened()+"]");
					AptItem item = getAptItemNoPreSdaFromTrace(trace);
					AptItemPosition itemPosition = getAptItemPositionFromTrace(trace);
					if (itemPosition.getFrazionarioGeopost() != null) {
						insertAptItemAndAptItemPositionUfiSDA(item, itemPosition);
						log.info("Inseriti o aggiornati AptItem e AptItemPosition per un pacco RTZ. Codice ["+item.getCode()+"]");
					}
				}
			}
		} catch (RuntimeException exc){
			log.error("Errore per l'oggetto con codice %s durante Inserimento o aggiornamento AptItem e AptItemPosition Pacco RTZ. Codice ["+trace.getIdTracedEntity()+"] WhatHappened ["+trace.getWhatHappened()+"]",exc);
		} catch (Exception exc){
			log.error("Errore per l'oggetto con codice %s durante Inserimento o aggiornamento AptItem e AptItemPosition Pacco RTZ. Codice ["+trace.getIdTracedEntity()+"] WhatHappened ["+trace.getWhatHappened()+"]",exc);
		}
	}

	private void handlerRTZ_APT(Trace trace) {
		String barcodeServizio = trace.getDetailList().get(barcode_servizio);
		if (WhatHappenedNotOnDBT.contains(trace.getWhatHappened()) &&
			("RTZ".equals(barcodeServizio) || "APT".equals(barcodeServizio))) {
			AptItem item = getAptItemFromTrace(trace);
			if (item.getSubPtype() == null) {
				log.error("PRE_SDA: Non esiste SUbPType per il pacco [" + item.getCode() + "] - oggetto non inserito su APT_ITEM");
			} else {
				try{
					//verifico se presente il frazionario da inserire in AptItemPosition
					String fraz = trace.getIdChannel();
					if (fraz != null) {
						log.info("Pacco RTZ. Codice ["+item.getCode()+"] Fraz ["+fraz+"]");
						AptItemPosition itemPosition = getAptItemPositionFromTrace(trace);
						AptItemPosition itemPositionF = aptItemPositionBL.find(trace.getIdTracedEntity());
						if (itemPositionF == null) {
							insertAptItemAndAptItemPositionPreSDA(item, itemPosition);
							log.info("Inseriti o updatati dati per un pacco. Codice ["+item.getCode()+"] fraz presente, itemPos == null - aggiornato frazionario.");
						} else {
							aptItemBL.insertOrUpdate(item);
							log.info("Inseriti o updatati dati per un pacco. Codice ["+item.getCode()+"] fraz presente ma non aggiornato.");
						}
					} else {
						aptItemBL.insertOrUpdate(item);
						log.info("Inseriti o updatati dati per un pacco. Codice ["+item.getCode()+"] fraz non presente.");
					}
				} catch (RuntimeException exc){
					log.error(String.format("Errore per l'oggetto AptItem codice %s", item.getCode()),exc);
				} catch (Exception exc){
					log.error(String.format("Errore per l'oggetto AptItem codice %s", item.getCode()),exc);
				}
			}
		}
	}

	
	/***
	 * 
	 * @param messageJson
	 * @return
	 */
	private List<CustomTraceEntity> generateCustumTraceFromJSONBean(MessageJson messageJson) {
		List<CustomTraceEntity> lst = new ArrayList<CustomTraceEntity>(messageJson.getTRACES().size());
		
		List<String> listaNoDettagli = new ArrayList<String>(10);
		listaNoDettagli.add("ID_LDV");
		listaNoDettagli.add("ID_PRODOTTO");
		listaNoDettagli.add("DATA_EMISSIONE");
		
		for(TRACE t : messageJson.getTRACES()){
			CustomTraceEntity customTraceEntity = new CustomTraceEntity();
			customTraceEntity.setChannel(messageJson.getHEADER().getCHANNEL());//non c'e' nel mapping perche' e' dell'header
			customTraceEntity.setTracedEntity("PACCO");
			customTraceEntity.setServiceName("TracciaturaPacchi");
			customTraceEntity.setIdTracedEntity(t.getTRACE().getPRESDA().getIDLDV()); 		
			customTraceEntity.setIdChannel(FRAZIONARIO_99999);
			customTraceEntity.setLabelTracedEntity(t.getTRACE().getPRESDA().getIDPRODOTTO());
			customTraceEntity.setWhatHappened("PRE_SDA");
			customTraceEntity.setWhenHappened(t.getTRACE().getPRESDA().getDATAEMISSIONE()); //attenzione alla data
			customTraceEntity.setWhereHappened("FS");
			//dettagli:
			
			PropertyDescriptor[] pdesclist = BeanUtils.getPropertyDescriptors(t.getTRACE().getPRESDA().getClass());
			
			for (PropertyDescriptor pdesc : pdesclist  ) {
				 
				if("class".equalsIgnoreCase(pdesc.getName()))
					continue;
				
				String fieldName = pdesc.getWriteMethod().getAnnotation(JsonProperty.class).value();
				
				if(fieldName==null)
					log.error("generateCustumTraceFromJSONBean: campo nullo"); 
				else{
					if(listaNoDettagli.contains(fieldName.toUpperCase()))
						continue;
				}
				
				try {
					Object val = pdesc.getReadMethod().invoke(t.getTRACE().getPRESDA());
					if(val!=null){
						customTraceEntity.addDetails(fieldName, val.toString());
					}
				}catch (Exception e) {
					log.error("generateCustumTraceFromJSONBean " + e.getMessage(),e);
				}
			}
			
			lst.add(customTraceEntity);
		}
		return lst;
	}

	
	/***
	 * 
	 * @param msgAnagraphic
	 * @param traceJSON
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	private Message convertJSONBean2XMLBean(Message msgAnagraphic, HEADER headerJSON, TRACE traceJSON)
			throws DatatypeConfigurationException {
		com.elsagdatamat.dbtrace.fromsda.bean.Message.Traces.Trace t = new com.elsagdatamat.dbtrace.fromsda.bean.Message.Traces.Trace();
		
		t.setIDTRACEDENTITY(traceJSON.getTRACE().getPRESDA().getIDLDV());
		t.setLABELTRACEDENTITY(traceJSON.getTRACE().getPRESDA().getIDPRODOTTO());
		t.setTRACEDENTITY("PACCO");		
		t.setWHATHAPPENED("PRE_SDA");
		
		if(traceJSON.getTRACE().getPRESDA().getDATAEMISSIONE()!=null)
		{			
			GregorianCalendar whenHappenedGC = new GregorianCalendar();
			whenHappenedGC.setTime(traceJSON.getTRACE().getPRESDA().getDATAEMISSIONE());
			t.setWHENHAPPENED(DatatypeFactory.newInstance().newXMLGregorianCalendar(whenHappenedGC));
		}	
		
		setWhatHappenedDetail(traceJSON, t);
		
		t.setWHEREHAPPENED("FS");
		
		Message.Traces.Trace.WHEREHAPPENEDDETAIL whereHappenedDetail = new Message.Traces.Trace.WHEREHAPPENEDDETAIL();
		Message.Traces.Trace.WHEREHAPPENEDDETAIL.OFFICE office = new Message.Traces.Trace.WHEREHAPPENEDDETAIL.OFFICE();
		
		office.setWHEREID(FRAZIONARIO_99999);
		whereHappenedDetail.setOFFICE(office);
								
		t.setWHEREHAPPENEDDETAIL(whereHappenedDetail);
		
		msgAnagraphic.getTraces().getTrace().add(t);
		return msgAnagraphic;
	}

	
	/***
	 * 
	 * @param traceJSON
	 * @param t
	 * @throws DatatypeConfigurationException
	 */
	private void setWhatHappenedDetail(TRACE traceJSON,
			com.elsagdatamat.dbtrace.fromsda.bean.Message.Traces.Trace t)
			throws DatatypeConfigurationException {
		Message.Traces.Trace.WHATHAPPENEDDETAIL whatHappenedDetail = new Message.Traces.Trace.WHATHAPPENEDDETAIL();
		Message.Traces.Trace.WHATHAPPENEDDETAIL.PRESDA preSDA = new Message.Traces.Trace.WHATHAPPENEDDETAIL.PRESDA();

		com.elsagdatamat.dbtrace.fromsda.bean.ObjectFactory factory = new com.elsagdatamat.dbtrace.fromsda.bean.ObjectFactory();
	 	
		JAXBElement<BigInteger> jaxBElementBigInteger = null;
		JAXBElement<String> jaxBElementString = null;
		JAXBElement<XMLGregorianCalendar> jaxBElementDate = null;
		JAXBElement<Boolean> jaxBElementBoolean = null;
		
		if(traceJSON.getTRACE().getPRESDA().getBARCODESERVIZIO()!=null)	{
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDABARCODESERVIZIO(traceJSON.getTRACE().getPRESDA().getBARCODESERVIZIO());
			preSDA.setBARCODESERVIZIO(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getCASELLAPOSTALECLIENTE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDACASELLAPOSTALECLIENTE(traceJSON.getTRACE().getPRESDA().getCASELLAPOSTALECLIENTE());
			preSDA.setCASELLAPOSTALECLIENTE(jaxBElementString);
		}
	
		if(traceJSON.getTRACE().getPRESDA().getCCPOSTECLIENTE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDACCPOSTECLIENTE(traceJSON.getTRACE().getPRESDA().getCCPOSTECLIENTE());
			preSDA.setCCPOSTECLIENTE(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getCODICECLIENTESAP()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDACODICECLIENTESAP(traceJSON.getTRACE().getPRESDA().getCODICECLIENTESAP());
			preSDA.setCODICECLIENTESAP(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getCODICECONTRATTOPOSTE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDACODICECONTRATTOPOSTE(traceJSON.getTRACE().getPRESDA().getCODICECONTRATTOPOSTE());
			preSDA.setCODICECONTRATTOPOSTE(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getCODICECONTRATTOSDA()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDACODICECONTRATTOSDA(traceJSON.getTRACE().getPRESDA().getCODICECONTRATTOSDA());
			preSDA.setCODICECONTRATTOSDA(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getCODICEFISCALE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDACODICEFISCALE(traceJSON.getTRACE().getPRESDA().getCODICEFISCALE());
			preSDA.setCODICEFISCALE(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getCODRIFCOLLO()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDACODRIFCOLLO(traceJSON.getTRACE().getPRESDA().getCODRIFCOLLO());
			preSDA.setCODRIFCOLLO(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getIDSERVIZIO()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAIDSERVIZIO(traceJSON.getTRACE().getPRESDA().getIDSERVIZIO());
			preSDA.setIDSERVIZIO(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getCODTIPOPAGAMENTO()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDACODTIPOPAGAMENTO(traceJSON.getTRACE().getPRESDA().getCODTIPOPAGAMENTO());
			preSDA.setCODTIPOPAGAMENTO(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getCONTENUTO()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDACONTENUTO(traceJSON.getTRACE().getPRESDA().getCONTENUTO());
			preSDA.setCONTENUTO(jaxBElementString);
		}	
			
		//LA DATA DI EMISSIONE non e' gestita perche' VIENE INSERITA NELL'HEADER DEL MESSAGGIO XML
		
		if(traceJSON.getTRACE().getPRESDA().getDATAINS()!=null){
			GregorianCalendar dataInsGC = new GregorianCalendar();
			dataInsGC.setTime(traceJSON.getTRACE().getPRESDA().getDATAINS());
			jaxBElementDate = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADATAINS(DatatypeFactory.newInstance().newXMLGregorianCalendar(dataInsGC));
			preSDA.setDATAINS(jaxBElementDate);
		}	
		
		if(traceJSON.getTRACE().getPRESDA().getCODUFFFRAZIONARIO()!=null){
		jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDACODUFFFRAZIONARIO(traceJSON.getTRACE().getPRESDA().getCODUFFFRAZIONARIO());
		preSDA.setCODUFFFRAZIONARIO(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getDESTCAP()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTCAP(traceJSON.getTRACE().getPRESDA().getDESTCAP());
			preSDA.setDESTCAP(jaxBElementString);
		}	
		
		if(traceJSON.getTRACE().getPRESDA().getDESTCELLULARE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTCELLULARE(traceJSON.getTRACE().getPRESDA().getDESTCELLULARE());
			preSDA.setDESTCELLULARE(jaxBElementString);
		}		

		if(traceJSON.getTRACE().getPRESDA().getDESTCITTA()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTCITTA(traceJSON.getTRACE().getPRESDA().getDESTCITTA());
			preSDA.setDESTCITTA(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getDESTEMAIL()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTEMAIL(traceJSON.getTRACE().getPRESDA().getDESTEMAIL());
			preSDA.setDESTEMAIL(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getDESTFAX()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTFAX(traceJSON.getTRACE().getPRESDA().getDESTFAX());
			preSDA.setDESTFAX(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getDESTINDIRIZZO()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTINDIRIZZO(traceJSON.getTRACE().getPRESDA().getDESTINDIRIZZO());
			preSDA.setDESTINDIRIZZO(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getDESTINFO1()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTINFO1(traceJSON.getTRACE().getPRESDA().getDESTINFO1());
			preSDA.setDESTINFO1(jaxBElementString);
		}			
		
		if(traceJSON.getTRACE().getPRESDA().getDESTINFO2()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTINFO2(traceJSON.getTRACE().getPRESDA().getDESTINFO2());
			preSDA.setDESTINFO2(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getDESTINTESTATARIO()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTINTESTATARIO(traceJSON.getTRACE().getPRESDA().getDESTINTESTATARIO());
			preSDA.setDESTINTESTATARIO(jaxBElementString);
		}	
		
		if(traceJSON.getTRACE().getPRESDA().getDESTNAZIONE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTNAZIONE(traceJSON.getTRACE().getPRESDA().getDESTNAZIONE());
			preSDA.setDESTNAZIONE(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getDESTPROVINCIA()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTPROVINCIA(traceJSON.getTRACE().getPRESDA().getDESTPROVINCIA());
			preSDA.setDESTPROVINCIA(jaxBElementString);
		}	
		
		if(traceJSON.getTRACE().getPRESDA().getDESTREFERENTE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTREFERENTE(traceJSON.getTRACE().getPRESDA().getDESTREFERENTE());
			preSDA.setDESTREFERENTE(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getDESTTELEFONO()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADESTTELEFONO(traceJSON.getTRACE().getPRESDA().getDESTTELEFONO());
			preSDA.setDESTTELEFONO(jaxBElementString);
		}	
		
		if(traceJSON.getTRACE().getPRESDA().getDETTAGLIOBS()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADETTAGLIOBS(traceJSON.getTRACE().getPRESDA().getDETTAGLIOBS());
			preSDA.setDETTAGLIOBS(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getDIMH()!=null){
			BigInteger altezzaB = getBigIntger(traceJSON.getTRACE().getPRESDA().getDIMH());
			jaxBElementBigInteger = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADIMH(altezzaB);
			preSDA.setDIMH(jaxBElementBigInteger);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getDIML()!=null){
			BigInteger larghezzaB = getBigIntger(traceJSON.getTRACE().getPRESDA().getDIML());
			jaxBElementBigInteger = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADIML(larghezzaB);
			preSDA.setDIML(jaxBElementBigInteger);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getDIMW()!=null){
			BigInteger prof = getBigIntger(traceJSON.getTRACE().getPRESDA().getDIMW());
			jaxBElementBigInteger = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDADIMW(prof);
			preSDA.setDIMW(jaxBElementBigInteger);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getIMPORTOASS()!=null){
			BigInteger imp = getBigIntger(traceJSON.getTRACE().getPRESDA().getIMPORTOASS());
			jaxBElementBigInteger = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAIMPORTOASS(imp);
			preSDA.setIMPORTOASS(jaxBElementBigInteger);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getIMPORTOCONTRASS()!=null){
			BigInteger impAss = getBigIntger(traceJSON.getTRACE().getPRESDA().getIMPORTOCONTRASS());
			jaxBElementBigInteger = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAIMPORTOCONTRASS(impAss);
			preSDA.setIMPORTOCONTRASS(jaxBElementBigInteger);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getISASSPERCENTUALE()!=null){
			jaxBElementBoolean = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAISASSPERCENTUALE("S".equalsIgnoreCase(traceJSON.getTRACE().getPRESDA().getISASSPERCENTUALE()));
			preSDA.setISASSPERCENTUALE(jaxBElementBoolean);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getMANCATACONSEGNA()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMANCATACONSEGNA(traceJSON.getTRACE().getPRESDA().getMANCATACONSEGNA());
			preSDA.setMANCATACONSEGNA(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getMITTCAP()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTCAP(traceJSON.getTRACE().getPRESDA().getMITTCAP());
			preSDA.setMITTCAP(jaxBElementString);
		}

		if(traceJSON.getTRACE().getPRESDA().getMITTCELLULARE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTCELLULARE(traceJSON.getTRACE().getPRESDA().getMITTCELLULARE());
			preSDA.setMITTCELLULARE(jaxBElementString);
		}	
		
		if(traceJSON.getTRACE().getPRESDA().getMITTCITTA()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTCITTA(traceJSON.getTRACE().getPRESDA().getMITTCITTA());
			preSDA.setMITTCITTA(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getMITTEMAIL()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTEMAIL(traceJSON.getTRACE().getPRESDA().getMITTEMAIL());
			preSDA.setMITTEMAIL(jaxBElementString);
		}	
		
		if(traceJSON.getTRACE().getPRESDA().getMITTFAX()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTFAX(traceJSON.getTRACE().getPRESDA().getMITTFAX());
			preSDA.setMITTFAX(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getMITTINDIRIZZO()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTINDIRIZZO(traceJSON.getTRACE().getPRESDA().getMITTINDIRIZZO());
			preSDA.setMITTINDIRIZZO(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getMITTINFO1()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTINFO1(traceJSON.getTRACE().getPRESDA().getMITTINFO1());
			preSDA.setMITTINFO1(jaxBElementString);
		}			
		
		if(traceJSON.getTRACE().getPRESDA().getMITTINFO2()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTINFO2(traceJSON.getTRACE().getPRESDA().getMITTINFO2());
			preSDA.setMITTINFO2(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getMITTINTESTATARIO()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTINTESTATARIO(traceJSON.getTRACE().getPRESDA().getMITTINTESTATARIO());
			preSDA.setMITTINTESTATARIO(jaxBElementString);
		}	
		
		if(traceJSON.getTRACE().getPRESDA().getMITTNAZIONE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTNAZIONE(traceJSON.getTRACE().getPRESDA().getMITTNAZIONE());
			preSDA.setMITTNAZIONE(jaxBElementString);
		}	
		
		if(traceJSON.getTRACE().getPRESDA().getMITTPROVINCIA()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTPROVINCIA(traceJSON.getTRACE().getPRESDA().getMITTPROVINCIA());
			preSDA.setMITTPROVINCIA(jaxBElementString);
		}	
		
		if(traceJSON.getTRACE().getPRESDA().getMITTREFERENTE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTREFERENTE(traceJSON.getTRACE().getPRESDA().getMITTREFERENTE());
			preSDA.setMITTREFERENTE(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getMITTTELEFONO()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAMITTTELEFONO(traceJSON.getTRACE().getPRESDA().getMITTTELEFONO());
			preSDA.setMITTTELEFONO(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getNOTE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDANOTE(traceJSON.getTRACE().getPRESDA().getNOTE());
			preSDA.setNOTE(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getNUMEROCOLLI()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDANUMEROCOLLI(traceJSON.getTRACE().getPRESDA().getNUMEROCOLLI());
			preSDA.setNUMEROCOLLI(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getNUMORDINE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDANUMORDINE(traceJSON.getTRACE().getPRESDA().getNUMORDINE());
			preSDA.setNUMORDINE(jaxBElementString);
		}			
		
		if(traceJSON.getTRACE().getPRESDA().getPAGONERI()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAPAGONERI(traceJSON.getTRACE().getPRESDA().getPAGONERI());
			preSDA.setPAGONERI(jaxBElementString);
		}		
		
		if(traceJSON.getTRACE().getPRESDA().getPESOCOLLI()!=null){
			BigInteger peso = getBigIntger(traceJSON.getTRACE().getPRESDA().getPESOCOLLI());
			jaxBElementBigInteger = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAPESOCOLLI(peso);
			preSDA.setPESOCOLLI(jaxBElementBigInteger);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getPIVA()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAPIVA(traceJSON.getTRACE().getPRESDA().getPIVA());
			preSDA.setPIVA(jaxBElementString);
		}	
		
		if(traceJSON.getTRACE().getPRESDA().getRAGIONESOCIALECLIENTE()!=null){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDARAGIONESOCIALECLIENTE(traceJSON.getTRACE().getPRESDA().getRAGIONESOCIALECLIENTE());
			preSDA.setRAGIONESOCIALECLIENTE(jaxBElementString);
		}		
		
		if(PROD_TYPE_PKR.equalsIgnoreCase(traceJSON.getTRACE().getPRESDA().getIDPRODOTTO())){
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDATIPODIGIPOD(TIPO_DIGIPOD_SDA);
			preSDA.setTIPODIGIPOD(jaxBElementString);
		}
		
		if(traceJSON.getTRACE().getPRESDA().getVALOREDICH()!=null){
			BigInteger valDich = getBigIntger(traceJSON.getTRACE().getPRESDA().getVALOREDICH());
			jaxBElementBigInteger = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDAVALOREDICH(valDich);
			preSDA.setVALOREDICH(jaxBElementBigInteger);
		}	
		
		/*ID_LDV non gestito perche' inserito nell'header...*/
		/*ID_PRODOTTO non gestito perche' inserito nell'HEADER: LABELTRACEDENTITY */
		
		
		PkConServiziAggiuntiviMapVO mappaCfgServiziAggiuntivi = pkConServiziAggiuntiviBL.getAll("SDA");

		String barcodeServizioVal = mappaCfgServiziAggiuntivi.getValoreMappatoTag(barcode_servizio, traceJSON.getTRACE().getPRESDA().getBARCODESERVIZIO());
		
		String importoAssicuratoVal = (traceJSON.getTRACE().getPRESDA().getIMPORTOASS()!= null) ? mappaCfgServiziAggiuntivi.getValoreMappatoTag("IMPORTO_ASSICURATO") : null;
		
		valorizzaServiziAggiuntivi(traceJSON, preSDA, factory, barcodeServizioVal, importoAssicuratoVal);	
		
		whatHappenedDetail.setPRESDA(preSDA);
		t.setWHATHAPPENEDDETAIL(whatHappenedDetail);
	}

	
	/**
	 * 
	 * @param traceJSON
	 * @param preSDA
	 * @param factory
	 * @param barcodeServizioVal
	 * @param importoAssicuratoVal
	 */
	private void valorizzaServiziAggiuntivi(TRACE traceJSON,
			Message.Traces.Trace.WHATHAPPENEDDETAIL.PRESDA preSDA,
			com.elsagdatamat.dbtrace.fromsda.bean.ObjectFactory factory,
			String barcodeServizioVal, String importoAssicuratoVal) {
		
		JAXBElement<String> jaxBElementString;
		String[] serviziAggiuntivi = new String[3];
		
		int i=0;
		if(barcodeServizioVal!=null){
			serviziAggiuntivi[i]=barcodeServizioVal;
			i++;
		}		
		
		if(importoAssicuratoVal!=null){
			serviziAggiuntivi[i]=importoAssicuratoVal;
			i++;
		} 
		
		if(traceJSON.getTRACE().getPRESDA().getSERVIZI()!=null){
			serviziAggiuntivi[i]=traceJSON.getTRACE().getPRESDA().getSERVIZI();
			i++;
		}
		
		if(i>0){
			
			String serviziAggiuntiviStr=""; 
			
			for(int i2=0; i2<i;){
				serviziAggiuntiviStr+=serviziAggiuntivi[i2];
				i2++;
				if(i2<i){
					serviziAggiuntiviStr += SEPARATORE_SERVIZI_AGGIUNTIVI;
				}
			}
			
			jaxBElementString = factory.createMessageTracesTraceWHATHAPPENEDDETAILPRESDASERVIZI(serviziAggiuntiviStr);
			preSDA.setSERVIZI(jaxBElementString);
		}
	}


	
	/***
	 * 
	 * @param d
	 * @return
	 */
	private BigInteger getBigIntger(Double d) {
		BigDecimal altezzaD = new BigDecimal(d);
		BigInteger altezzaB = altezzaD.toBigInteger();
		return altezzaB;
	}
	
	/***
	 * 
	 * @param trace
	 * @param hashProdottiSDA
	 * @return
	 */
	private boolean checkSendToAnagraphic(Trace trace, HashMap<String, String> hashProdottiSDA) {
		if(WhatHappenedFowardAnagraphic.contains(trace.getWhatHappened()) && hashProdottiSDA.get(trace.getLabelTracedEntity())==null) {
			if(trace.getWhatHappened().equals("ESI_SDA")) {
				if ((trace.getDetailList().get("DF") != null) && (trace.getDetailList().get("DF").equals("F"))) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/***
	 * 
	 * @param msg
	 * @return
	 */
	private String marshal(Message msg)  {
		java.io.StringWriter sw = new StringWriter();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(com.elsagdatamat.dbtrace.fromsda.bean.Message.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
			jaxbMarshaller.marshal(msg,sw);
		} catch (JAXBException e) { 
			log.error("Errore durante marshal:"+e.getMessage());
			return null;
		}
		return sw.getBuffer().toString();
	}
	
	/***
	 * 
	 * @param str
	 * @return
	 * @throws JAXBException 
	 */
	public Message unmarshal(String str) throws JAXBException{
		
        Unmarshaller unmarshaller=null;
        Message msg=null;
 
		JAXBContext jaxbContext = JAXBContext.newInstance(com.elsagdatamat.dbtrace.fromsda.bean.Message.class);
	    unmarshaller = jaxbContext.createUnmarshaller();       
	    msg = (Message)unmarshaller.unmarshal( new StringReader(str));
		
		return msg;          
	}

	private AptItem getAptItemNoPreSdaFromTrace(Trace trace) {
		AptItem item = new AptItem();

		//2015/02/09 D.P. tronco i caratteri a 13
		String code = trace.getIdTracedEntity();
		if (code.length()==16){
			code = code.substring(3);
		}

		item.setCode(code);
		//ST_TTRACE-8800 - TT 16.0.7 - CL&CO - UFI_SDA APT_ITEM_POSITION
		//aggiunti i due campi non nullable per errore insert in assenza di PRE_SDA
		item.setSubPtype(aptItemBL.getSubPType(item.getCode()));
		item.setPtype(trace.getLabelTracedEntity());

		return item;
	}

	private AptItemPosition getAptItemPositionFromTrace(Trace trace) {
		AptItemPosition itemPosition = new AptItemPosition();

		//tronco i caratteri a 13
		String code = trace.getIdTracedEntity();
		if (code.length()==16){
			code = code.substring(3);
		}

		itemPosition.setCode(code);
		String fraz = trace.getIdChannel();
		itemPosition.setFrazionarioGeopost(fraz);
		itemPosition.setStatus(AptStatusEnum.COURIER);
		itemPosition.setTimestamp(new Date());

		return itemPosition;
	}

	public void insertAptItemAndAptItemPositionUfiSDA(AptItem item, AptItemPosition itemPosition) {
		try{
			AptItem itemS = aptItemBL.find(item.getCode());

			if (itemS == null) {
				AptItemPosition itemPositionF = aptItemPositionBL.find(item.getCode());
				if (itemPositionF == null) {
					aptItemBL.insertAptItemAndAptItemPosition(item, itemPosition);
					log.info("Inseriti dati nelle tabelle aptItem e aptItemPosition per un pacco RTZ. Codice ["+item.getCode()+"]");
				} else {
					aptItemBL.insertAptItemAndUpdateAptItemPosition(item, itemPosition);
					log.info("Inseriti dati nella tabella aptItem e aggiornata aptItemPosition per un pacco RTZ. Codice ["+item.getCode()+"]");
				}
			} else {
				AptItemPosition itemPositionF = aptItemPositionBL.find(item.getCode());
				if (itemPositionF == null) {
					aptItemPositionBL.insert(itemPosition);
					log.info("Inseriti dati nella tabella aptItemPosition per un pacco RTZ. Codice ["+item.getCode()+"]");
				} else {
					itemPositionF.setFrazionarioGeopost(itemPosition.getFrazionarioGeopost());
					aptItemPositionBL.update(itemPositionF);
					log.info("Aggiornata tabella aptItemPosition per un pacco RTZ. Codice ["+item.getCode()+"]");
				}
			}
			log.info("Inseriti o updatati dati nelle tabelle aptItem e aptItemPosition per un pacco RTZ. Codice ["+item.getCode()+"]");
		} catch (RuntimeException exc){
			log.error(String.format("Errore per l'inserimento degli oggetti in AptItem e AptItemPosition codice %s", item.getCode()),exc);
		} catch (Exception exc){
			log.error(String.format("Errore per l'inserimento degli oggetti in AptItem e AptItemPosition codice %s", item.getCode()),exc);
		}
	}

	public void insertAptItemAndAptItemPositionPreSDA(AptItem item, AptItemPosition itemPosition) {
		try{
			aptItemBL.insertAptItemAndAptItemPosition(item, itemPosition);
		} catch (RuntimeException exc){
			log.error(String.format("Errore per l'inserimento degli oggetti in AptItem e AptItemPosition codice %s", item.getCode()),exc);
		} catch (Exception e) {
			log.error(String.format("Errore per l'inserimento degli oggetti in AptItem e AptItemPosition codice %s", item.getCode()),e);
		}
	}

	private static Boolean servicesInCode(CustomTraceEntity entity){
		Boolean res="PACCO".equals(entity.getTracedEntity()) && entity.getIdTracedEntity()!=null && entity.getIdTracedEntity().length() == 16;
		log.debug("servicesInCode for "+entity.getTracedEntity()+" "+entity.getIdTracedEntity()+" "+res);
		return res;
	}
	protected Trace createObj(CustomTraceEntity entity) {
		Trace obj = new Trace();
		BeanUtils.copyProperties(entity, obj);
		obj.setInternalDetailList(new HashSet<TraceDetail>(0));
		String service = entity.getDetailList().get(ADDITIONAL_SERVICE_DETAIL_CLASS);
		String param = entity.getDetailList().get(PARAMETERS_DETAIL_CLASS);
		if ( servicesInCode(entity)) {
			List<String> generateServiceList = generateServiceList(entity.getIdTracedEntity(), service, obj.getChannel());
			param = generateParams(generateServiceList, param);
			service = additionalServiceManager.encodeTraceListServices(generateServiceList);
			service = service.replaceFirst(";", "");
			obj.setIdTracedEntity(obj.getIdTracedEntity().substring(3));
		}

		if (service != null && service.length() > 0)
			addDetail(ADDITIONAL_SERVICE_DETAIL_CLASS, service, obj);
		if (param != null && param.length() > 0)
			addDetail(PARAMETERS_DETAIL_CLASS, param, obj);
		for (TraceDetail traceDetail : entity.getInternalDetailList()) {
			if (!traceDetail.getParamClass().equals(ADDITIONAL_SERVICE_DETAIL_CLASS)
					&& !traceDetail.getParamClass().equals(PARAMETERS_DETAIL_CLASS))
				addDetail(traceDetail.getParamClass(), traceDetail.getParamValue(), obj);
		}
		return obj;
	}

	private static void addDetail(String pClass, String value, Trace obj) {
		if (value == null || value.trim().equals("")) {
			return;
		}
		TraceDetail newTraceDetail = new TraceDetail();
		newTraceDetail.setParamClass(pClass);
		newTraceDetail.setParamValue(value);
		newTraceDetail.setTrace(obj);
		obj.getInternalDetailList().add(newTraceDetail);
	}

	protected List<Trace> createObjs(List<CustomTraceEntity> entities) {
		List<Trace> objs = new ArrayList<Trace>(entities.size());
		for (CustomTraceEntity entity : entities)
			objs.add(createObj(entity));
		return objs;
	}

	// AGGIUNTO R.L. - CruscottoPacchi - 05.2012
	/**
	 * Metodo che invia una traccia alla coda che carica i dati per il cruscotto pacchi. La traccia arriva a questo metodo soltanto se �
	 * stata correttametne persistita sul DB
	 *
	 * Prima di inviare la traccia vengono effettuati due controlli: Il primo riguarda il flag sender: indica se e' abilitato il canale di
	 * comunicazione con la coda per il cruscotto pacchi. Vale <b>true</b> se il canale � attivo, <b>false</b> altrimenti. Il parametro �
	 * letto dal DB (tale controllo � stato implementato nel chiamante - valore del flagSender). Il secondo controllo � invece relativo alla
	 * configurazione dei prodotti. Superato il primo controllo si fa una cernita delle tracce da inviare al cruscotto. Solo quelle di
	 * accettazione piuttosto che di smistamento ecc.
	 *
	 * Il flag sender e la configurazione dei prodotti vengono lette da db ad intervalli regolari e configurabili.
	 *
	 */
	// R.L. - 30.01.2013 modificato per inviare una lista di tracce e non una
	// traccia come capitava prima
	// private boolean sendSerialTrace(Trace trace){
	//
	// ProductKey key = new ProductKey(trace.getLabelTracedEntity(),
	// trace.getChannel(), trace.getWhatHappened());
	// boolean isAttivata= messageDestination.getProductConfig(key);
	//
	// if(!isAttivata) return false;
	//
	// try{
	// com.selexelsag.tt.dbtrace.bridge.serial.object.Trace serialTrace =
	// convertTrace2SerialTrace(trace);
	// messageDestination.send(serialTrace,"DbTraceBridge.DbTraceBridgeBL");
	// } catch (Exception e){
	// log.error("Problemi nell'invio della traccia al CruscottoPacchi - ObjectQueue",e);
	// return false;
	// }
	// return true;
	// }

	private boolean sendListOfSerialTrace(ArrayList<com.selexelsag.tt.dbtrace.bridge.serial.object.Trace> serialTraceList) {

		try {
			messageDestination.send(serialTraceList, "DbTraceBridge.DbTraceBridgeBL-SerialList");
		} catch (Exception e) {
			log.error("Problemi nell'invio della traccia al CruscottoPacchi - ObjectQueue", e);
			return false;
		}
		return true;
	}
	
	private boolean sendToAnagraphic(String strMessage, String whatHappened) {

		try {
			messageDestinationAnagraphic.send(strMessage, whatHappened, "DbTraceBridge.DbTraceBridgeBL-SerialList");
		} catch (Exception e) {
			log.error("Problemi nell'invio della traccia all'Anagrafica - AnagraphicQueue", e);
			return false;
		}
		return true;
	}

	/**
	 * Questo metodo mappa una traccia memorizzata in trace e tracedetails (entioty di hibernate) in una struttura simmetrica che poi sara'
	 * inviata su una coda jms - quella relativa al cruscotto pacchi
	 *
	 * @param Trace
	 *            - entity del trackDbWs
	 * @return Trace - entity serializzata per la objectQueue
	 */
	private com.selexelsag.tt.dbtrace.bridge.serial.object.Trace convertTrace2SerialTrace(Trace trace) {
		com.selexelsag.tt.dbtrace.bridge.serial.object.Trace serialTrace = new com.selexelsag.tt.dbtrace.bridge.serial.object.Trace();
		serialTrace.setTracedEntity(trace.getTracedEntity());
		serialTrace.setIdTracedEntity(trace.getIdTracedEntity());
		serialTrace.setWhatHappened(trace.getWhatHappened());
		serialTrace.setWhenHappened(trace.getWhenHappened());
		serialTrace.setWhenRegistered(trace.getWhenRegistered());
		serialTrace.setWhereHappened(trace.getWhereHappened());
		serialTrace.setChannel(trace.getChannel());
		serialTrace.setIdChannel(trace.getIdChannel());
		serialTrace.setServiceName(trace.getServiceName());
		serialTrace.setLabelTracedEntity(trace.getLabelTracedEntity());
		serialTrace.setIdStatus(trace.getIdStatus());
		serialTrace.setIdCorrelazione(trace.getIdCorrelazione());
		serialTrace.setIdTracedExternal(trace.getIdTracedExternal());
		serialTrace.setIdForwardTo(trace.getIdForwardTo());
		serialTrace.setSysForwardTo(trace.getSysForwardTo());

		Set<TraceDetail> tds = trace.getInternalDetailList();
		Set<com.selexelsag.tt.dbtrace.bridge.serial.object.TraceDetail> serialTds = new HashSet<com.selexelsag.tt.dbtrace.bridge.serial.object.TraceDetail>(0);
		for (TraceDetail traceDetail : tds) {
			com.selexelsag.tt.dbtrace.bridge.serial.object.TraceDetail serialTd = new com.selexelsag.tt.dbtrace.bridge.serial.object.TraceDetail();
			serialTd.setParamClass(traceDetail.getParamClass());
			serialTd.setParamValue(traceDetail.getParamValue());
			serialTds.add(serialTd);
		}
		serialTrace.setInternalDetailList(serialTds);

		return serialTrace;
	}

	private List<String> generateServiceList(String code, String services, String channel) {
		additionalServiceManager.decodeTraceListServices(services);
		List<String> decodedServices = additionalServiceManager.decodeTraceListServices(services);
		decodedServices.addAll(additionalServiceManager.findAdditionalServiceByDictionary(code.substring(0, 3)));
		return decodedServices;
	}

	private String generateParams(List<String> decodedServices, String params) {
		Set<String> servicesSet = new HashSet<String>();
		servicesSet.addAll(decodedServices);
		if (params == null)
			params = "";
		return mailpieceManager.setDefaultForCustomData(servicesSet, params);
	}

	private AptItem getAptItemFromTrace(Trace trace){
		AptItem item = new AptItem();

		//2015/02/09 D.P. tronco i caratteri a 13
		String code = trace.getIdTracedEntity();
		if (code.length()==16){
			code = code.substring(3);
		}

		//item.setItemCode(trace.getIdTracedEntity());
		item.setCode(code);
		item.setSubPtype(aptItemBL.getSubPType(item.getCode()));
		item.setPtype(trace.getLabelTracedEntity());
		Map<String, String> details = trace.getDetailList();
		//ST_TTRACE-5934 - TT 15.1 - CR APT - Messaggio PRE_SDA e campi numerici nulli
		if (details.get(dim_L) != null)
			item.setDepth(Double.valueOf(details.get(dim_L)).intValue());
		else
			item.setDepth(new Integer(0));
		if (details.get(dim_W) != null)
			item.setWidth(Double.valueOf(details.get(dim_W)).intValue());
		else
			item.setWidth(new Integer(0));
		if (details.get(dim_H) != null)
			item.setHeight(Double.valueOf(details.get(dim_H)).intValue());
		else
			item.setHeight(new Integer(0));
		if (details.get(peso_colli) != null)
			item.setWeight(Double.valueOf(details.get(peso_colli)).intValue());
		else
			item.setWeight(new Integer(0));
		item.setReceiverName(details.get(dest_intestatario));
		item.setAddressee(details.get(dest_intestatario));
		item.setReceiverEmail(details.get(dest_email));
		item.setReceiverPhoneNumber(details.get(dest_telefono));
		return item;
	}

	public MessageDestinationSDA getMessageDestinationAnagraphic() {
		return messageDestinationAnagraphic;
	}

	public void setMessageDestinationAnagraphic(
			MessageDestinationSDA messageDestinationAnagraphic) {
		this.messageDestinationAnagraphic= messageDestinationAnagraphic;
	}

	public PkPrdProdottiSdaVWBL getPkPrdProdottiSdaVWBL() {
		return pkPrdProdottiSdaVWBL;
	}

	public void setPkPrdProdottiSdaVWBL(PkPrdProdottiSdaVWBL pkPrdProdottiSdaVWBL) {
		this.pkPrdProdottiSdaVWBL = pkPrdProdottiSdaVWBL;
	}

	public PkConServiziAggiuntiviBL getPkConServiziAggiuntiviBL() {
		return pkConServiziAggiuntiviBL;
	}

	public void setPkConServiziAggiuntiviBL(
			PkConServiziAggiuntiviBL pkConServiziAggiuntiviBL) {
		this.pkConServiziAggiuntiviBL = pkConServiziAggiuntiviBL;
	}
	


}
