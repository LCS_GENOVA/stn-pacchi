package com.elsagdatamat.dbtrace.bridge.tosda;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetailsList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public class ConvertUtil {

	protected static XMLGregorianCalendar getGCalendar(Date date) {
		if (date == null)
			return null;
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);

		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		} catch (DatatypeConfigurationException e) {
			return null;
		}
	}
	
	
	public static TracesList tracesToTraceList(List<com.elsagdatamat.tt.trackdbws.beans.Trace> newTraces) {
		TracesList ret = new TracesList();
		
		if (newTraces != null)
		for (com.elsagdatamat.tt.trackdbws.beans.Trace trace:newTraces) {
			com.selexelsag.tracktrace.trackdbws.xml.Trace t = new com.selexelsag.tracktrace.trackdbws.xml.Trace();
//			BeanUtils.copyProperties(trace, t);
			
			t.setChannel(trace.getChannel());
			t.setIdChannel(trace.getIdChannel());
			t.setIdCorrelazione(trace.getIdCorrelazione());
			t.setIdForwardTo(trace.getIdForwardTo());
			t.setIdStatus(trace.getIdStatus());
			t.setIdTracedEntity(trace.getIdTracedEntity());
			t.setIdTracedExternal(trace.getIdTracedExternal());
			t.setLabelTracedEntity(trace.getLabelTracedEntity());
			t.setServiceName(trace.getServiceName());
			t.setSysForwardTo(trace.getSysForwardTo());
			t.setTracedEntity(trace.getTracedEntity());
			TraceDetailsList tdl = new TraceDetailsList();
			t.setTraceDetailsList(tdl);
			for (Entry<String, String> s:trace.getDetailList().entrySet()) {
				TraceDetail td = new TraceDetail();
				td.setParamClass(s.getKey());
				td.setParamValue(s.getValue());
				t.getTraceDetailsList().getTraceDetails().add(td);
			}
			t.setWhatHappened(trace.getWhatHappened());
			t.setWhenHappened(getGCalendar(trace.getWhenHappened()));
			t.setWhenRegistered(getGCalendar(trace.getWhenRegistered()));
			t.setWhereHappened(trace.getWhereHappened());
			
			
			ret.getTraces().add(t);
		}

		return ret;
	}
}
