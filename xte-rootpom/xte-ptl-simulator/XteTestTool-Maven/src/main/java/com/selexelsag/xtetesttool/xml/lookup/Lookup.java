package com.selexelsag.xtetesttool.xml.lookup;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="causali">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="causale" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                           &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ufficiDBServer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ufficio" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                           &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="ipAddress" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="dbType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="password" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="dbName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="sid" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="frazionario" type="{http://www.w3.org/2001/XMLSchema}short" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="codiciProdotti">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="prodotto" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                           &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="min" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="max" type="{http://www.w3.org/2001/XMLSchema}long" />
 *                           &lt;attribute name="codes" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="prodottiMessi">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="prodotto">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                           &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="codes" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "causali",
    "ufficiDBServer",
    "codiciProdotti",
    "prodottiMessi"
})
@XmlRootElement(name = "lookup")
public class Lookup {

    @XmlElement(required = true)
    protected Lookup.Causali causali;
    @XmlElement(required = true)
    protected Lookup.UfficiDBServer ufficiDBServer;
    @XmlElement(required = true)
    protected Lookup.CodiciProdotti codiciProdotti;
    @XmlElement(required = true)
    protected Lookup.ProdottiMessi prodottiMessi;

    /**
     * Gets the value of the causali property.
     * 
     * @return
     *     possible object is
     *     {@link Lookup.Causali }
     *     
     */
    public Lookup.Causali getCausali() {
        return causali;
    }

    /**
     * Sets the value of the causali property.
     * 
     * @param value
     *     allowed object is
     *     {@link Lookup.Causali }
     *     
     */
    public void setCausali(Lookup.Causali value) {
        this.causali = value;
    }

    /**
     * Gets the value of the ufficiDBServer property.
     * 
     * @return
     *     possible object is
     *     {@link Lookup.UfficiDBServer }
     *     
     */
    public Lookup.UfficiDBServer getUfficiDBServer() {
        return ufficiDBServer;
    }

    /**
     * Sets the value of the ufficiDBServer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Lookup.UfficiDBServer }
     *     
     */
    public void setUfficiDBServer(Lookup.UfficiDBServer value) {
        this.ufficiDBServer = value;
    }

    /**
     * Gets the value of the codiciProdotti property.
     * 
     * @return
     *     possible object is
     *     {@link Lookup.CodiciProdotti }
     *     
     */
    public Lookup.CodiciProdotti getCodiciProdotti() {
        return codiciProdotti;
    }

    /**
     * Sets the value of the codiciProdotti property.
     * 
     * @param value
     *     allowed object is
     *     {@link Lookup.CodiciProdotti }
     *     
     */
    public void setCodiciProdotti(Lookup.CodiciProdotti value) {
        this.codiciProdotti = value;
    }

    /**
     * Gets the value of the prodottiMessi property.
     * 
     * @return
     *     possible object is
     *     {@link Lookup.ProdottiMessi }
     *     
     */
    public Lookup.ProdottiMessi getProdottiMessi() {
        return prodottiMessi;
    }

    /**
     * Sets the value of the prodottiMessi property.
     * 
     * @param value
     *     allowed object is
     *     {@link Lookup.ProdottiMessi }
     *     
     */
    public void setProdottiMessi(Lookup.ProdottiMessi value) {
        this.prodottiMessi = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="causale" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                 &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "causale"
    })
    public static class Causali {

        protected List<Lookup.Causali.Causale> causale;

        /**
         * Gets the value of the causale property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the causale property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCausale().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Lookup.Causali.Causale }
         * 
         * 
         */
        public List<Lookup.Causali.Causale> getCausale() {
            if (causale == null) {
                causale = new ArrayList<Lookup.Causali.Causale>();
            }
            return this.causale;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class Causale {

            @XmlValue
            protected String value;
            @XmlAttribute
            protected String name;
            @XmlAttribute
            protected String description;

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the description property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Sets the value of the description property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="prodotto" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                 &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="min" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="max" type="{http://www.w3.org/2001/XMLSchema}long" />
     *                 &lt;attribute name="codes" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "prodotto"
    })
    public static class CodiciProdotti {

        protected List<Lookup.CodiciProdotti.Prodotto> prodotto;

        /**
         * Gets the value of the prodotto property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the prodotto property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProdotto().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Lookup.CodiciProdotti.Prodotto }
         * 
         * 
         */
        public List<Lookup.CodiciProdotti.Prodotto> getProdotto() {
            if (prodotto == null) {
                prodotto = new ArrayList<Lookup.CodiciProdotti.Prodotto>();
            }
            return this.prodotto;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="min" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="max" type="{http://www.w3.org/2001/XMLSchema}long" />
         *       &lt;attribute name="codes" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class Prodotto {

            @XmlValue
            protected String value;
            @XmlAttribute
            protected String name;
            @XmlAttribute
            protected Long min;
            @XmlAttribute
            protected Long max;
            @XmlAttribute
            protected String codes;

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the min property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getMin() {
                return min;
            }

            /**
             * Sets the value of the min property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setMin(Long value) {
                this.min = value;
            }

            /**
             * Gets the value of the max property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getMax() {
                return max;
            }

            /**
             * Sets the value of the max property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setMax(Long value) {
                this.max = value;
            }

            /**
             * Gets the value of the codes property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodes() {
                return codes;
            }

            /**
             * Sets the value of the codes property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodes(String value) {
                this.codes = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="prodotto">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                 &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="codes" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "prodotto"
    })
    public static class ProdottiMessi {

        @XmlElement(required = true)
        protected Lookup.ProdottiMessi.Prodotto prodotto;

        /**
         * Gets the value of the prodotto property.
         * 
         * @return
         *     possible object is
         *     {@link Lookup.ProdottiMessi.Prodotto }
         *     
         */
        public Lookup.ProdottiMessi.Prodotto getProdotto() {
            return prodotto;
        }

        /**
         * Sets the value of the prodotto property.
         * 
         * @param value
         *     allowed object is
         *     {@link Lookup.ProdottiMessi.Prodotto }
         *     
         */
        public void setProdotto(Lookup.ProdottiMessi.Prodotto value) {
            this.prodotto = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="codes" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class Prodotto {

            @XmlValue
            protected String value;
            @XmlAttribute
            protected String name;
            @XmlAttribute
            protected String codes;

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the codes property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodes() {
                return codes;
            }

            /**
             * Sets the value of the codes property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodes(String value) {
                this.codes = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ufficio" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                 &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="ipAddress" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="dbType" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="password" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="dbName" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="sid" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                 &lt;attribute name="frazionario" type="{http://www.w3.org/2001/XMLSchema}short" />
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ufficio"
    })
    public static class UfficiDBServer {

        protected List<Lookup.UfficiDBServer.Ufficio> ufficio;

        /**
         * Gets the value of the ufficio property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the ufficio property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUfficio().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Lookup.UfficiDBServer.Ufficio }
         * 
         * 
         */
        public List<Lookup.UfficiDBServer.Ufficio> getUfficio() {
            if (ufficio == null) {
                ufficio = new ArrayList<Lookup.UfficiDBServer.Ufficio>();
            }
            return this.ufficio;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="ipAddress" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="dbType" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="password" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="dbName" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="sid" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
         *       &lt;attribute name="frazionario" type="{http://www.w3.org/2001/XMLSchema}short" />
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class Ufficio {

            @XmlValue
            protected String value;
            @XmlAttribute
            protected String name;
            @XmlAttribute
            protected String ipAddress;
            @XmlAttribute
            protected String dbType;
            @XmlAttribute
            protected String userId;
            @XmlAttribute
            protected String password;
            @XmlAttribute
            protected String dbName;
            @XmlAttribute
            protected String sid;
            @XmlAttribute
            protected String description;
            @XmlAttribute
            protected Short frazionario;

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the ipAddress property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIpAddress() {
                return ipAddress;
            }

            /**
             * Sets the value of the ipAddress property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIpAddress(String value) {
                this.ipAddress = value;
            }

            /**
             * Gets the value of the dbType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDbType() {
                return dbType;
            }

            /**
             * Sets the value of the dbType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDbType(String value) {
                this.dbType = value;
            }

            /**
             * Gets the value of the userId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUserId() {
                return userId;
            }

            /**
             * Sets the value of the userId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUserId(String value) {
                this.userId = value;
            }

            /**
             * Gets the value of the password property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassword() {
                return password;
            }

            /**
             * Sets the value of the password property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassword(String value) {
                this.password = value;
            }

            /**
             * Gets the value of the dbName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDbName() {
                return dbName;
            }

            /**
             * Sets the value of the dbName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDbName(String value) {
                this.dbName = value;
            }

            /**
             * Gets the value of the sid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSid() {
                return sid;
            }

            /**
             * Sets the value of the sid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSid(String value) {
                this.sid = value;
            }

            /**
             * Gets the value of the description property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Sets the value of the description property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Gets the value of the frazionario property.
             * 
             * @return
             *     possible object is
             *     {@link Short }
             *     
             */
            public Short getFrazionario() {
                return frazionario;
            }

            /**
             * Sets the value of the frazionario property.
             * 
             * @param value
             *     allowed object is
             *     {@link Short }
             *     
             */
            public void setFrazionario(Short value) {
                this.frazionario = value;
            }

        }

    }

}
