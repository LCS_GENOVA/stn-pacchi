package com.selexelsag.xte.xte_presentation_logic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.xte.bpm.orchestrator.IBPMOrchestrator;
import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.model.track.TrackResponse;
import com.selexelsag.xte.service.logging.ILogHandler;
import com.selexelsag.xte.service.resource.IMessageManager;
import com.selexelsag.xte.service.system.IAppConfig;

public class TrackService implements ITrackService 
{
	@Autowired
	private IBPMOrchestrator bPMOrchestator;
	@Autowired
	private IMessageManager trackMessageManager;

	private static Log log = LogFactory.getLog(TrackService.class); 

	@Override
	public TrackResponse processRequest(TrackRequest trackRequest)
	{
		TrackResponse ans;
		
		try
		{
			if (trackRequest.getRequestId() == null || trackRequest.getRequestId().toString().isEmpty())
				trackRequest.setRequestId(new StringBuilder(requestIdToLog(trackRequest)));
			MDC.put(ILogHandler.REQUESTID_TOKEN_NAME, trackRequest.getRequestId());
			ans = bPMOrchestator.processRequest(trackRequest);
		}
		catch (Throwable t)
		{
			log.error(String.format("Error processing request : TrackRequest=[%1$s]", trackRequest), t);
			String errorDesc = "";
			String errorCode = "";
			
			if (trackMessageManager != null){
				errorDesc = trackMessageManager.getMessageText(ITrackService.RESULT_ERROR_INTERNAL_KEY);
				errorCode = trackMessageManager.getMessageText(ITrackService.RESULT_ERROR_NOK_KEY);
			}
			
			ans = new TrackResponse();
			if (trackRequest != null)
			{
				ans.setDestinationOfficeId(trackRequest.getDestinationOfficeId().toString());
				ans.setServiceName(trackRequest.getService().toString());
				ans.setChannel(trackRequest.getChannel().toString());
				ans.setDestinationOfficeId(trackRequest.getDestinationOfficeId().toString());
				if (trackRequest.getSourceOfficeId()!=null) 
				{
					ans.setSourceOfficeId(trackRequest.getSourceOfficeId().toString());
				}
				else 
				{
					ans.setSourceOfficeId(null);
				}
				if (trackRequest.getObjectId()!=null) 
				{
					ans.setObjectId(trackRequest.getObjectId().toString());
				}
				else 
				{
					ans.setObjectId(null);
				}
				ans.setEventName(trackRequest.getEventName().toString());
			}
			ans.setErrorCode(errorCode);
			ans.setErrorDesc(errorDesc);
		}
		finally
		{
			MDC.remove(ILogHandler.REQUESTID_TOKEN_NAME);
		}
		return ans;
	}
	
	private String requestIdToLog(TrackRequest trackRequest)
	{
		if (trackRequest != null && trackRequest.getRequestId()!=null && !trackRequest.getRequestId().toString().isEmpty())
			return trackRequest.getRequestId().toString();
		return String.format("%1$s[%2$s]",java.util.UUID.randomUUID().toString(), IAppConfig.APP_ID);
	}
}
