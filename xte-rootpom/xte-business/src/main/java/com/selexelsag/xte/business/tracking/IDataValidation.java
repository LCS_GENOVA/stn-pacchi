package com.selexelsag.xte.business.tracking;

import java.util.HashMap;

import com.selexelsag.xte.business.exceptions.BusinessValidationException;
import com.selexelsag.xte.model.flows.FlowParams;

/**
 * @author Motta
 *	Metodi per effettuare la validazione sintattica e semantica dei dati.
 * TODO: gestione delle eccezioni
 */
public interface IDataValidation {
	
	public static final String DATA_VALIDATION_BEAN_NAME = "baseDataValidation";
	/**
	 * @param flowParams parametri specifici al flusso
	 * @param data i dati da validare
	 * @return ritorna true se i dati sono formalmente validi
	 */
	boolean isValidFormat(FlowParams flowParams, StringBuilder data) throws BusinessValidationException;
	
	/**
	 * @param flowParams parametri specifici al flusso
	 * @param data i dati da validare
	 * @return ritorna true se il contenuto dei dati è coerente
	 */
	// TODO: implementazione di controllo semantico deve chiamare una servlet: definire pure interfaccia
	HashMap<String, String> getSemanticData(FlowParams flowParams, StringBuilder data) throws BusinessValidationException;
	
}
