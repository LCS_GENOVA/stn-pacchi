package com.elsagdatamat.dbtrace.bridge.tosda.entities;

import java.io.Serializable;
import javax.persistence.Column;

public class TraceTypesToSendPK implements Serializable{
    
    private static final long serialVersionUID = 3217638377175578865L;
    @Column(name="WHAT_HAPPENED")
    private String whatHappened;
    @Column
    private String destination;
    public String getWhatHappened() {
        return whatHappened;
    }
    public void setWhatHappened(String whatHappened) {
        this.whatHappened = whatHappened;
    }
    public String getDestination() {
        return destination;
    }
    public void setDestination(String destination) {
        this.destination = destination;
    }
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((destination == null) ? 0 : destination.hashCode());
		result = prime * result
				+ ((whatHappened == null) ? 0 : whatHappened.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TraceTypesToSendPK other = (TraceTypesToSendPK) obj;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (whatHappened == null) {
			if (other.whatHappened != null)
				return false;
		} else if (!whatHappened.equals(other.whatHappened))
			return false;
		return true;
	}
    
    
}
