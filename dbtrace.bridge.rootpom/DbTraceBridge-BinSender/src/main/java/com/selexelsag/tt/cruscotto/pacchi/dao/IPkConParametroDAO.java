package com.selexelsag.tt.cruscotto.pacchi.dao;

import com.elsagdatamat.framework.dao.IGenericDAO;
import com.selexelsag.tt.cruscotto.pacchi.bean.PkConParametro;

public interface IPkConParametroDAO extends IGenericDAO<PkConParametro, String>{

}
