package com.elsagdatamat.dbtrace.bridge.tosda;

import javax.xml.bind.annotation.XmlElement;

public class SDATraceWhereDetail {
	@XmlElement(name = "OFFICE")
	public SDAOffice office;
}
