package com.elsagdatamat.dbtrace.bridge.fromssip.test;

import java.io.InputStream;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elsagdatamat.dbtrace.bridge.engine.ws.CustomTraceWs;
import com.elsagdatamat.dbtrace.bridge.engine.ws.ITrackDBRWServiceManager;
import com.elsagdatamat.dbtrace.bridge.engine.ws.XmlTraceWsBuilder;
import com.elsagdatamat.framework.entities.EntityCreationException;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetailsList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
public class MappingTest {

	@Resource
	private XmlTraceWsBuilder traceBuilder;

	@Resource
	private ITrackDBRWServiceManager trackDBRWServiceManager;

	@Test
	public void createMessageTest() {
		try {
			// R.L. - nuova traccia SSIP_TO_TT - 24.04.2012
			MockTextMessage msg;
			TracesList traceList;
			
			msg= new MockTextMessage(readFile("SSIP_TO_TT.xml"));

			traceList = createTraces(msg);
			printToConsole(traceList);

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void loopMessageTest() {
		for (int i = 1; i <= 1000; i++) {
			messageToDbTraceWsTest();
			System.out.println((i / 10) + "%/100%");
		}
	}

	@Test
	public void messageToDbTraceWsTest() {
		try {
			MockTextMessage msg = new MockTextMessage(readFile("RIT_SDA_n.xml"));

			TracesList traceList = createTraces(msg);
			sendMessage(traceList);

			msg = new MockTextMessage(readFile("RIT_SDA_1-TRA_SDA.xml"));

			traceList = createTraces(msg);
			sendMessage(traceList);

			msg = new MockTextMessage(readFile("RIT_SDA_1.xml"));

			traceList = createTraces(msg);
			sendMessage(traceList);

			msg = new MockTextMessage(readFile("TRA_SDA-ESI_SDA-FAT_SDA.xml"));

			traceList = createTraces(msg);
			sendMessage(traceList);

			msg = new MockTextMessage(readFile("TRA_SDA-ESI_SDA.xml"));

			traceList = createTraces(msg);
			sendMessage(traceList);

			msg = new MockTextMessage(readFile("SUBP1.xml"));

			traceList = createTraces(msg);
			sendMessage(traceList);
			printToConsole(traceList);
			msg = new MockTextMessage(readFile("SUBP2.xml"));

			traceList = createTraces(msg);
			sendMessage(traceList);
			printToConsole(traceList);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	private void sendMessage(TracesList lst) {
		try {
			TrackDBRWServicePortType twsp = trackDBRWServiceManager.getServicePort();
			Assert.assertNotNull(twsp);

			BigInteger result = twsp.putTraces(lst);
			Assert.assertTrue((result.doubleValue() != -1));

			if (result.intValue() < lst.getTraces().size()) {
				System.out.println("Errore durante l'inserimento delle tracce: inserite " + result.intValue() + " su "
						+ lst.getTraces().size() + " tracce previste");
				Assert.fail();
			}

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void performanceTest() {
		try {
			Date t1 = new Date();
			System.out.println("START at " + t1.toString());
			MockTextMessage msg = new MockTextMessage(readFile("test-prod.xml"));

			TracesList traceList = createTraces(msg);
			Assert.assertNotNull(traceList);
			Date t2 = new Date();
			long seconds = (t2.getTime() - t1.getTime());
			System.out.println("STOP in " + seconds);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void multiThreadPerformanceTest() {
		try {
			int iteration = 100;
			Thread[] threads = new Thread[iteration];
			final Object synkObj=new Object();
			final MockTextMessage msg = new MockTextMessage(readFile("test-prod.xml"));
			for (int i = 0; i < iteration; i++) {
				Thread thread = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							synchronized (synkObj) {
								System.out.println("START "+Thread.currentThread().getName()+" at " + (new Date()).toString());
								createTraces(msg);
								System.out.println("FINISH "+Thread.currentThread().getName()+" at " + (new Date()).toString());
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				threads[i] = thread;
			}

			Date t1 = new Date();
			System.out.println("START at " + t1.toString());
			for (int i = 0; i < threads.length; i++) {
				Thread thread = threads[i];
				thread.start();
			}

			for (int i = 0; i < threads.length; i++) {
				Thread thread = threads[i];
				thread.join();
			}

			Date t2 = new Date();
			long seconds = (t2.getTime() - t1.getTime());
			System.out.println("FINISH in " + seconds);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	private void printToConsole(TracesList lst) throws IllegalAccessException, InvocationTargetException {
		System.out.println("RISULTATO TEST:");
		List<Trace> traces = lst.getTraces();
		for (Trace trace : traces) {
			System.out.println("	TRACE: ");
			String get = "get";
			Method[] getters = trace.getClass().getMethods();
			for (Method getter : getters) {
				if (getter.getName().contains(get)) {
					String value = String.valueOf(getter.invoke(trace));
					if (!getter.getName().contentEquals("getClass")) {
						if (getter.getName().contentEquals("getTraceDetailsList")) {
							TraceDetailsList traceDetailsList = (TraceDetailsList) getter.invoke(trace);
							System.out.println("		TRACEDETAIL: ");
							List<TraceDetail> traceDetails = traceDetailsList.getTraceDetails();
							for (TraceDetail traceDetail : traceDetails) {
								System.out.println("			" + traceDetail.getParamClass() + ": " + traceDetail.getParamValue());
							}
							System.out.println("		TRACEDETAIL FINE");
						} else {
							System.out.println("		" + getter.getName() + ": " + value);
						}
					}
				}
			}
			System.out.println("	TRACE FINE");
		}
		System.out.println("FINE");
	}

	private String readFile(String fileName) {
		InputStream is = ClassLoader.getSystemResourceAsStream(fileName);
		StringBuffer sb = new StringBuffer();
		Scanner s = new Scanner(is);
		while (s.hasNext())
			sb.append(s.nextLine());
		return sb.toString();
	}

	private TracesList createTraces(TextMessage message) throws EntityCreationException, JMSException, InterruptedException {
		Thread.sleep(100);
		List<CustomTraceWs> lst = traceBuilder.create(new StringReader(message.getText()));
		// List<CustomTraceWs> lst = traceBuilder.create(new StringInputStream(message.getText()));

		Assert.assertNotNull(lst);
		TracesList traceList = new TracesList();
		traceList.getTraces().addAll(lst);
		return traceList;
	}
}
