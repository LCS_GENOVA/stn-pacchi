package com.elsagdatamat.dbtrace.bridge.tosda;

import java.util.Map;

import javax.xml.bind.annotation.XmlElement;

public class SDAAvvNsp {
	@XmlElement(name = "DCOD")
	public String dcod;

	@XmlElement(name = "DDEST")
	public String ddest;

	@XmlElement(name = "COURIER")
	public String courier;

	public SDAAvvNsp(){}
	
	public SDAAvvNsp(Map<String, String> map)
	{
		String key = "DCOD";
		this.dcod = map.get(key);
		key = "DDEST";
		this.ddest = map.get(key);
		key = "COURIER";
		this.courier = map.get(key);
	}
}
