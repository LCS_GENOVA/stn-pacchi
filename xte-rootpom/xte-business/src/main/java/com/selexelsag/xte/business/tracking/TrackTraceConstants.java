package com.selexelsag.xte.business.tracking;

/**
 * Classe container per le costanti stringa in uso dal sistema.
 * @author Motta
 *
 */
public class TrackTraceConstants {

	public static final String SERVICE_DATA_POSTA = "DP";	
	public static final String SERVICE_POSTA_REGISTRATA = "PR";
	
	public static final String CHANNEL_TERMINALE_PORTALETTERE = "TP";
	public static final String CHANNEL_ACCETTAZIONE_ONLINE = "AO";
	
	public static final String EVENT_ACQUISIZIONE_CONFIGURAZIONE = "AC";
	public static final String EVENT_CONFIGURAZIONE_PORTALETTERE = "CP";
	public static final String EVENT_ACQUISIZIONE_MAZZETTO = "AM";
	public static final String EVENT_CONSEGNA_MAZZETTO = "CM";
	public static final String EVENT_LASCIA_OGGETTO = "LO";
	public static final String EVENT_LASCIA_MAZZETTO = "LM";
	public static final String EVENT_NOTIFICA_MAZZETTO = "NM";
	public static final String EVENT_PRE_ACCETTAZIONE= "PA";
	public static final String EVENT_PRONTO_RECAPITO = "PR";
	public static final String EVENT_SUBENTRA_MAZZETTO = "SM";
	public static final String EVENT_SCARICO_PEZZI = "SP";
	public static final String EVENT_ACCETTAZIONE_PEZZI = "AP";
	public static final String EVENT_ACQUISIZIONE_OGGETTO = "AO";
	
	
	public static final String CHANNEL_ID_KEY_TRANSFORMATION = "channelID";
	public static final String CHANNEL_KEY_TRANSFORMATION = "channel";
	public static final String EVENTNAME_KEY_TRANSFORMATION = "eventName";
	public static final String SERVICENAME_KEY_TRANSFORMATION = "serviceName";
	public static final String VERSION_KEY_TRANSFORMATION = "version";
	
	
}
