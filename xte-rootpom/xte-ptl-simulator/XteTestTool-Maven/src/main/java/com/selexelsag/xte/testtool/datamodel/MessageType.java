/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.datamodel;

/**
 *
 * @author Da Procida
 */
public enum MessageType {

    FATAL("X"),
    ERROR("E"),
    WARNING("W"),
    CORRECT_PROCESSING("S");
    
    private final String code;

    private MessageType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static String getCode(MessageType messageType){
        if(messageType == null) {
            return null;
        }
        return messageType.getCode();
    }

    public static MessageType getByCode(String code){
         if(FATAL.getCode().equalsIgnoreCase(code)) {
             return FATAL;
         }
         if(ERROR.getCode().equalsIgnoreCase(code)) {
             return ERROR;
         }
         if(WARNING.getCode().equalsIgnoreCase(code)) {
             return WARNING;
         }
         if(CORRECT_PROCESSING.getCode().equalsIgnoreCase(code)) {
             return CORRECT_PROCESSING;
         }
         return null;
    }
}
