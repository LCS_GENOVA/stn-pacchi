/**
 * Tracking.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.selexelsag.xte.soap.track;

import com.selexelsag.xte.model.track.soap.TrackResponse;

public interface Tracking extends java.rmi.Remote {

	public TrackResponse trackMessage(java.lang.String serviceName, java.lang.String channel, java.lang.String channelID, java.lang.String eventName, java.lang.String version, java.lang.String trackData) throws java.rmi.RemoteException;
}
