/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.message.trans;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Map;

/**
 * @author arodriguez
 *
 */
public enum D1PTransformationValues implements Serializable {

	COURIER("COURIER"),	MC("MC"), OFNAME("OFNAME"),
	BTSF("BTSF"), AOFC("AOFC"),	ADT("ADT"), ACURR("ACURR"),
	ZIP_DIST("ZIP_DIST"), PERC("PERC"),	FARE("FARE"),		
	AOFCNAME("AOFCNAME"), ADEST("ADEST"), DESTR("DESTR"),
	ADDRS("ADDRS"),	SA("SA"), REGT("REGT"), DESTZ("DESTZ"),
	CNTRS("CNTRS"),	INSVAL("INSVAL"), RCURR("RCURR"),
	WB("WB"), WA("WA"),	NSEALS("NSEALS"),
	NCHECK("NCHECK"), CUSTCODE("CUSTCODE"),	CODEAR("CODEAR"),
	// L.C. 26/05/2015 R15.2 - Inizio
	SANEW("MODALITA_PAGAMENTO"),
	// L.C. 26/05/2015 R15.2 - Fine
	NUM_MANCATA_CONSEGNA("NUM_MANCATA_CONSEGNA"), FILIALE_SDA("FILIALE_SDA"),
	CONTO_CORRENTE_DEST("CONTO_CORRENTE_DEST"),	NUM_GIORNI_GIACENZA("NUM_GIORNI_GIACENZA"),
	COD_ISTR("COD_ISTR"), FLAG_RRR("FLAG_RRR"),	FLAG_ANN("FLAG_ANN"),
	IMP_DIRITTI_POSTALI("IMP_DIRITTI_POSTALI"),	IMP_ONERI_DOGANALI("IMP_ONERI_DOGANALI"),
	WHERE_ID("WHERE_ID");
	
	private String key = null;
	private String val = null;
	
	/**
	 * private enum constructor
	 * @param key  String key to match value
	 */
	private D1PTransformationValues(String key) {
		this.key = key;
	}

	/**
	 * return enum instance filling-in values map 
	 * @param sourceMap the Map values to set
	 * @return this D1PTransformationValues object
	 */
	public D1PTransformationValues map(Map<String, String> sourceMap) {
		if (sourceMap.containsKey(key))
			val = sourceMap.get(key);
		else  // reimposto altrimenti mantiene il valore vecchio
			val = null;
		return this;
	}

	/**
	 * value of specified enum key 
	 * @return String value
	 */
	public String value() {
		return val;
	}
	
	/**
	 * int converted value of specified enum key 
	 * @return BigInteger value if numeric, null otherwise
	 */
	public BigInteger intValue() {
		try {
			return new BigInteger(val);
		} catch(Exception nfe) {
		}
		return null;
	}
}
