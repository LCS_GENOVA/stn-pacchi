package com.selexes.tt.tool.recupero.messaggi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.elsagdatamat.dbtrace.bridge.engine.entities.DiscardedTrace;
import com.selexes.tt.tool.recupero.messaggi.manager.IDiscardedTraceManagerExtesion;
import com.selexes.tt.tool.utils.FileProperties;

public class RecuperoToSDaTool {
	
	static String discardingProcess = "DBTraceToSDABridge";
	static String fromDate = "yyyy/mm/dd"; // nel formato yyyy/mm/dd
	static String toDate = "yyyy/mm/dd"; // nel formato yyyy/mm/dd
	static String nomefileScarti = "scartati.txt";
	static String schemaFile = "";
	static String path =  new File(".").getAbsolutePath()+File.separator+"OK"+File.separator;
	static String fileProperties = "toSDA.properties";
	
	static Log log;
	static ApplicationContext context;
	
	public static void main(String[] args) {
		DOMConfigurator.configure("toolLog4j.xml");
		log = LogFactory.getLog(RecuperoToSDaTool.class);
		log.info("inzio recupero tracce");
		
		
		loadProperties(fileProperties);
			
	    context = new ClassPathXmlApplicationContext("applicationContext-bl.xml");

	    if(args==null || args.length==0)
	    	tracceDaDb();
	    else 
	    	tracceDaFile(args[0]);
	    
		log.info("terminata esecuzione tool");
	}
	
	
	private static void tracceDaFile(String path){
		String elemChild = "trace";
		File pathFile = new File(path);
		if(!pathFile.exists())
			pathFile.mkdirs(); // directpry per memorizzare i file
		
		File pathFileDaRecuperare = new File(path);
		
		String[] fileXml = pathFileDaRecuperare.list(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				return (name.toLowerCase()).endsWith(".xml");
			}});
		
		String nomefile = null;
		int numMess = 1;
		String dataString=null;
		JDomParser jDomParser = new JDomParser(schemaFile);
		for (String file: fileXml) {
			log.info("Recupero "+(numMess)+" Messaggio ["+file+"]");
				nomefile=file+"_OK.xml";
			try{
				jDomParser.parseNStore(new StringReader(stringFromFile(new File(path+File.separator+file))), elemChild, path+nomefile, path+nomefileScarti);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			log.info("Terminata elaborazione "+(numMess)+" file con nome ["+file+"] salvato in ["+nomefile+"]");
			numMess+=1;
		}			
	}
	
	
	private static void tracceDaDb(){
		String elemChild = "trace";
		IDiscardedTraceManagerExtesion discardedTraceManagerExtesion = (IDiscardedTraceManagerExtesion) context.getBean("discardedTraceManagerExtesion");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		List<Object> discardedTraceIds = discardedTraceManagerExtesion.getMessages(discardingProcess, fromDate, toDate);
		if(discardedTraceIds==null || !(discardedTraceIds.size()>0)){
			log.info("non ci sono tracce da recuperare dal ["+fromDate+"] al ["+toDate+"] per il ["+discardingProcess+"]");
			System.exit(0);
		}
		
		// controllo se esiste la drectory per memorizzare i file, se non c'� la creo
		File pathFile = new File(path);
		if(!pathFile.exists())
			pathFile.mkdirs();
			
		String nomefile = null;
		int numMess = 1;
		int numfilePerData=0;
		String dataString=null;
		JDomParser jDomParser = new JDomParser(schemaFile);
		for (Object object : discardedTraceIds) {
			Long idMessage = (Long) object;
			log.info("Recupero "+(numMess)+" Messaggio ["+idMessage+"]");
			DiscardedTrace discardedTrace = discardedTraceManagerExtesion.getMessagesById(idMessage);
			if(nomefile==null || dataString==null || !dataString.equals(sdf.format(discardedTrace.getInsertDate()))){
				dataString = sdf.format(discardedTrace.getInsertDate());
				numfilePerData=0;
			}
				nomefile=dataString+"."+(++numfilePerData)+".xml";
			try{
				jDomParser.parseNStore(new StringReader(discardedTrace.getTraceBody()), elemChild, path+nomefile, path+nomefileScarti);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			log.info("Terminata elaborazione "+(numMess)+" Messaggio con id ["+idMessage+"] - nomefile ["+nomefile+"]");
			numMess+=1;
		}			
	}
	
	 public static String stringFromFile(File file) throws IOException {
	        StringBuffer buf = new StringBuffer();
	        BufferedReader in = new BufferedReader(new FileReader(file));

	        try {
	            String line = null;
	            while ((line = in.readLine()) != null) {
	                buf.append(line);
	            }
	        }
	        finally {
	            in.close();
	        }
	        return buf.toString();
	    }

	 
	
	private static void loadProperties(String fileProperties){
		FileProperties props=new FileProperties(fileProperties); 
		discardingProcess = props.getProp("discardingProcess");
		fromDate = props.getProp("dalGiorno");
		toDate = props.getProp("alGiorno");
		nomefileScarti = props.getProp("nomefileScarti");
		schemaFile = props.getProp("schemaFile");
		path = props.getProp("path");
		//		path =  new File(".").getAbsolutePath()+File.separator+"OK"+File.separator;
	}
	
}
