package com.selexelsag.xte.business.impl;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.selexelsag.messagetobedelivered.entity.MessageToBeDelivered;
import com.selexelsag.messagetobedelivered.entity.MessageToBeDelivered.Status;
import com.selexelsag.messagetobedelivered.vo.CompressionType;
import com.selexelsag.messagetobedelivered.vo.MessageDestinationType;
import com.selexelsag.messagetobedelivered.vo.TrackingMessage;
import com.selexelsag.xte.business.tracking.IMessageDBManager;
import com.selexelsag.xte.persistence.dao.AsyncDiscardedMessagesDAO;
import com.selexelsag.xte.persistence.dao.AsyncMessagesDAO;
import com.selexelsag.xte.persistence.dao.AsyncProcessedDAO;
import com.selexelsag.xte.persistence.dao.MessageToBeDeliveredDao;
import com.selexelsag.xte.persistence.entities.AsyncDiscardedMessages;
import com.selexelsag.xte.persistence.entities.AsyncMessages;
import com.selexelsag.xte.persistence.entities.AsyncProcessed;

public class MessageDBManagerImpl implements IMessageDBManager {

	private static Logger logger = Logger.getLogger(MessageDBManagerImpl.class);

	@Autowired
	private AsyncMessagesDAO asyncMessagesDAO;

	@Autowired
	private AsyncProcessedDAO asyncProcessedDAO;

	@Autowired
    protected MessageToBeDeliveredDao messageToBeDeliveredDao;

	@Autowired
	private AsyncDiscardedMessagesDAO asyncDiscardedMessagesDAO;

	public AsyncDiscardedMessagesDAO getAsyncDiscardedMessagesDAO() {
		return asyncDiscardedMessagesDAO;
	}

	public void setAsyncDiscardedMessagesDAO(
			AsyncDiscardedMessagesDAO asyncDiscardedMessagesDAO) {
		this.asyncDiscardedMessagesDAO = asyncDiscardedMessagesDAO;
	}

	public AsyncMessagesDAO getAsyncMessagesDAO() {
		return asyncMessagesDAO;
	}

	public void setAsyncMessagesDAO(AsyncMessagesDAO asyncMessagesDAO) {
		this.asyncMessagesDAO = asyncMessagesDAO;
	}

	public AsyncProcessedDAO getAsyncProcessedDAO() {
		return asyncProcessedDAO;
	}

	public void setAsyncProcessedDAO(AsyncProcessedDAO asyncProcessedDAO) {
		this.asyncProcessedDAO = asyncProcessedDAO;
	}

	public MessageToBeDeliveredDao getMessageToBeDeliveredDao() {
		return messageToBeDeliveredDao;
	}

	public void setMessageToBeDeliveredDao(
			MessageToBeDeliveredDao messageToBeDeliveredDao) {
		this.messageToBeDeliveredDao = messageToBeDeliveredDao;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void sendMessage(
		AsyncMessages message,
		MessageDestinationType destType,
		String processedMessage,
		String messageType,
		String queueName,
		boolean compressed,
		HashMap<String, Serializable> additionalData)
		throws Exception
	{
		logger.debug("sendMessage start message_id: " + message.getId());

		TrackingMessage msg = new TrackingMessage(destType, queueName, messageType, processedMessage);

		msg.setCompressionType(compressed ? CompressionType.STANDARD_COMPRESSION : CompressionType.NOT_COMPRESSED);

		MessageToBeDelivered entity = new MessageToBeDelivered();
		BeanUtils.copyProperties(msg, entity);
		entity.setCreationDate(new Date());
		entity.setStatus(Status.T);
		if (additionalData != null)
			entity.setAdditionalData(additionalData);
		switch (msg.getCompressionType()) {
		case NOT_COMPRESSED:
			entity.setCompressed('N');
			break;
		case LEGACY_COMPRESSION:
			entity.setCompressed('L');
			break;
		case STANDARD_COMPRESSION:
			entity.setCompressed('S');
			break;
		}
		logger.debug("sendMessage messageToBeDeliveredDao inserting message_id: " + message.getId());
		messageToBeDeliveredDao.insert(entity);
		logger.debug("sendMessage messageToBeDeliveredDao inserted message_id: " + message.getId());

		Timestamp ts = new Timestamp(System.currentTimeMillis());

		AsyncProcessed msgProc = new AsyncProcessed(
			message.getId(),
			message.getFlowId(),
			message.getSourceOfficeId(),
			message.getDestinationOfficeId(),
			message.getObjectId(),
			message.getOriginalMessage(),
			processedMessage,
			message.getReceivedTimestamp(),
			ts,
			message.getLockId(),
			message.getLockTimestamp(),
			message.getErrDescr());
		
		logger.debug("sendMessage asyncProcessedDAO persisting id: " + msgProc.getId());
		asyncProcessedDAO.persist(msgProc);
		logger.debug("sendMessage asyncProcessedDAO persisted id: " + msgProc.getId());
		//asyncMessagesDAO.delete(message);
		//logger.debug("sendMessage asyncMessagesDAO deleted id: " + message.getId());
		message.setLockTimestamp(null);
		message.setSendTimestamp(ts);
		asyncMessagesDAO.merge(message);
		logger.debug("sendMessage asyncMessagesDAO updated id: " + message.getId());

		logger.debug("sendMessage end message_id: " + message.getId());
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void sendMultipleMessage(AsyncMessages message, List<MessageToBeDelivered> mtbdList, Boolean deleteAsyncMessages) throws Exception {
		logger.debug("sendMultipleMessage start message_id: " + message.getId());

		if (mtbdList.size() > 0) {
			logger.debug("sendMultipleMessage messageToBeDeliveredDao insert start");
			for (int i = 0; i < mtbdList.size(); i++) {
				logger.trace("sendMultipleMessage insert n: " + i);
				messageToBeDeliveredDao.insert(mtbdList.get(i));
				/* transaction not rollback flushed record
				if ( i % 20 == 0 ) { //20, same as the JDBC batch size
			        //flush a batch of inserts and release memory:
					messageToBeDeliveredDao.flushAndClear();
					logger.trace("sendMultipleMessage flushAndClear");
			    }
			    */
			}
			logger.debug("sendMultipleMessage messageToBeDeliveredDao insert end recordNum: " + mtbdList.size());
		}

		if (deleteAsyncMessages) {
			Timestamp ts = new Timestamp(System.currentTimeMillis());

			AsyncProcessed msgProc = new AsyncProcessed(message.getId(),
					message.getFlowId(),
					message.getSourceOfficeId(),
					message.getDestinationOfficeId(),
					message.getObjectId(),
					message.getOriginalMessage(),
					"-",
					message.getReceivedTimestamp(),
					ts,
					message.getLockId(),
					message.getLockTimestamp(),
					message.getErrDescr());
			logger.debug("sendMultipleMessage asyncProcessedDAO persisting id: " + msgProc.getId());
			asyncProcessedDAO.persist(msgProc);
			logger.debug("sendMultipleMessage asyncProcessedDAO persisted id: " + msgProc.getId());
			//asyncMessagesDAO.delete(message);
			//logger.debug("sendMultipleMessage asyncMessagesDAO deleted id: " + message.getId());
			message.setLockTimestamp(null);
			message.setSendTimestamp(ts);
			asyncMessagesDAO.merge(message);
			logger.debug("sendMultipleMessage asyncMessagesDAO updated id: " + message.getId());
		}

		logger.debug("sendMultipleMessage end message_id: " + message.getId());
	}

	@Override
	public MessageToBeDelivered buildMessage(
		MessageDestinationType destType,
		String processedMessage,
		String messageType,
		String queueName,
		boolean compressed,
		HashMap<String, Serializable> additionalData)
	{
		TrackingMessage msg = new TrackingMessage(destType,
				  queueName,
				  messageType,
				  processedMessage);

		msg.setCompressionType(compressed ? CompressionType.STANDARD_COMPRESSION : CompressionType.NOT_COMPRESSED);

		additionalData.put("Label", "Acc-online");

		MessageToBeDelivered entity = new MessageToBeDelivered();
		BeanUtils.copyProperties(msg, entity);
		entity.setCreationDate(new Date());
		entity.setStatus(Status.T);
		entity.setAdditionalData(additionalData);
		switch (msg.getCompressionType()) {
		case NOT_COMPRESSED:
		    entity.setCompressed('N');
		    break;
		case LEGACY_COMPRESSION:
		    entity.setCompressed('L');
		    break;
		case STANDARD_COMPRESSION:
		    entity.setCompressed('S');
		    break;
		}

		return entity;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void moveToDiscardedMessages(AsyncMessages message, String errorDescr) throws Exception {
		logger.debug("moveToDiscardedMessages start message_id: " + message.getId());

		AsyncDiscardedMessages msgDisc = new AsyncDiscardedMessages(message.getId(),
				message.getFlowId(),
				message.getSourceOfficeId(),
				message.getDestinationOfficeId(),
				message.getObjectId(),
				message.getOriginalMessage(),
				message.getReceivedTimestamp(),
				message.getSendTimestamp(),
				message.getLockId(),
				message.getLockTimestamp(),
				message.getErrDescr() == null ? errorDescr : message.getErrDescr() + "\n " + errorDescr);
		logger.debug("moveToDiscardedMessages asyncDiscardedMessagesDAO persisting id: " + msgDisc.getId());
		asyncDiscardedMessagesDAO.persist(msgDisc);
		logger.debug("moveToDiscardedMessages asyncDiscardedMessagesDAO persisted id: " + msgDisc.getId());
		asyncMessagesDAO.delete(message);
		logger.debug("moveToDiscardedMessages asyncMessagesDAO deleted id: " + message.getId());
		logger.debug("moveToDiscardedMessages end message_id: " + message.getId());
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void sendMultipleMessageAndMoveToDiscardedMessages(AsyncMessages message, List<MessageToBeDelivered> mtbdList, String errorDescr) throws Exception {
		logger.debug("sendMultipleMessageAndMoveToDiscardedMessages start message_id: " + message.getId());
		logger.debug("sendMultipleMessageAndMoveToDiscardedMessages mtbdList.size() = : " + mtbdList.size());
		
		if (mtbdList.size() > 0) {
			logger.debug("sendMultipleMessageAndMoveToDiscardedMessages messageToBeDeliveredDao insert start");
			for (int i = 0; i < mtbdList.size(); i++) {
				logger.trace("sendMultipleMessageAndMoveToDiscardedMessages insert n: " + i);
				messageToBeDeliveredDao.insert(mtbdList.get(i));
			}
			logger.debug("sendMultipleMessageAndMoveToDiscardedMessages messageToBeDeliveredDao insert end recordNum: " + mtbdList.size());
		}

		AsyncDiscardedMessages msgDisc = new AsyncDiscardedMessages(message.getId(),
				message.getFlowId(),
				message.getSourceOfficeId(),
				message.getDestinationOfficeId(),
				message.getObjectId(),
				message.getOriginalMessage(),
				message.getReceivedTimestamp(),
				message.getSendTimestamp(),
				message.getLockId(),
				message.getLockTimestamp(),
				message.getErrDescr() + "\n " + errorDescr);
		logger.debug("sendMultipleMessageAndMoveToDiscardedMessages asyncDiscardedMessagesDAO persisting id: " + msgDisc.getId());
		asyncDiscardedMessagesDAO.persist(msgDisc);
		logger.debug("sendMultipleMessageAndMoveToDiscardedMessages asyncDiscardedMessagesDAO persisted id: " + msgDisc.getId() + "\n moveToDiscardedMessages asyncMessagesDAO deleting id: " + message.getId());
		asyncMessagesDAO.delete(message);
		logger.debug("sendMultipleMessageAndMoveToDiscardedMessages asyncMessagesDAO deleted id: " + message.getId());

		logger.debug("sendMultipleMessageAndMoveToDiscardedMessages end message_id: " + message.getId());
	}
}
