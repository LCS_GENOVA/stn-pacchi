package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.elsagdatamat.framework.dao.IGeneralDAO;
import com.elsagdatamat.framework.dao.IGenericDAO;
import com.elsagdatamat.framework.reflection.ReflectionUtils;
import com.elsagdatamat.framework.search.Search;
import com.elsagdatamat.framework.utils.Pair;
import com.elsagdatamat.framework.utils.PairLinkedList;

public abstract class BaseManager<T, K extends Serializable> implements IManager<T, K> {

	protected Log log = LogFactory.getLog(this.getClass());
	protected IGenericDAO<T, K> dao;
	protected IGeneralDAO generalDao;

	protected Class<T> managedClass;

	public BaseManager() {
		this.managedClass = ReflectionUtils.getClassGenericType(getClass(), 0);
	}

	public IGenericDAO<T, K> getDao() {
		return dao;
	}

	public void setDao(IGenericDAO<T, K> dao) {
		this.dao = dao;
	}

	public IGeneralDAO getGeneralDao() {
		return generalDao;
	}

	public void setGeneralDao(IGeneralDAO generalDao) {
		this.generalDao = generalDao;
	}

	@Override
	public Class<T> getManagedClass() {
		return managedClass;
	}

	@Override
	public List<T> findByProperties(Collection<Pair<String, Object>> properties) {
		return this.dao.findByProperties(properties);
	}

	@Override
	public T findByID(K objectID) {
		return dao.findById(objectID, false);
	}

	@Override
	public void delete(T entity) {
		dao.delete(entity);
	}

	@Override
	public int deleteBatch(Search condition) {
		return dao.deleteBatch(condition);
	}

	@Override
	public void update(T entity) {
		dao.update(entity);
	}

	@Override
	public void insert(T entity) {
		dao.insert(entity);
	}

	@Override
	public Collection<T> find(Search search) {
		return dao.search(search);
	}

	@Override
	public Object findMin(Class<T> clazz, String minPropertyName, Collection<Pair<String, Object>> properties) {
		return findMin(clazz, minPropertyName, properties, false);
	}

	@Override
	public Object findMin(Class<T> clazz, String minPropertyName, Collection<Pair<String, Object>> properties, boolean lock) {
		return dao.findMin(clazz, minPropertyName, properties, lock);
	}

	@Override
	public Object findMax(Class<T> clazz, String maxPropertyName, Collection<Pair<String, Object>> properties) {
		return findMax(clazz, maxPropertyName, properties, false);
	}

	@Override
	public Object findMax(Class<T> clazz, String maxPropertyName, Collection<Pair<String, Object>> properties, boolean lock) {
		return dao.findMax(clazz, maxPropertyName, properties, lock);
	}

	public Object sum(Class<T> clazz, String maxPropertyName, Collection<Pair<String, Object>> properties, boolean lock) {
		return dao.sum(clazz, maxPropertyName, properties);
	}

	@Override
	public int getCount(Search search) {
		PairLinkedList<String, Object> additionalFilter = getAdditionalFilter();
		if (additionalFilter != null && additionalFilter.size() > 0) {
			if (search == null)
				search = new Search();
			for (Pair<String, Object> prop : additionalFilter)
				search.addFilterEqual(prop.name, prop.value);
		}
		return dao.searchLength(search);
	}

	@Override
	public int updateBatch(Collection<Pair<String, Object>> setProperties, Collection<Pair<String, Object>> conditionProperties) {
		return dao.updateBatch(setProperties, conditionProperties);
	}

	@Override
	public int updateBatch(Collection<Pair<String, Object>> setProperties, Search condition) {
		return dao.updateBatch(setProperties, condition);
	}

	@Override
	public int updateBatch(Collection<Pair<String, Object>> newProperties, Search condition, Integer maxObjectAffected) {
		return dao.updateBatch(newProperties, condition, maxObjectAffected);
	}

	protected PairLinkedList<String, Object> getAdditionalFilter() {
		return null;
	}

	@Override
	public void refresh(T object) {
		dao.refresh(object);
	}

	protected List<T> getList(String namedQuery, String[] namedParams, Object[] params) {
		return dao.getList(namedQuery, namedParams, params);
	}

	protected Long getNumber(String namedQuery, String[] namedParams, Object[] params) {
		return dao.getNumber(namedQuery, namedParams, params);
	}

	protected Object getScalar(String namedQuery, String[] namedParams, Object[] params) {
		return dao.getScalar(namedQuery, namedParams, params);
	}
}
