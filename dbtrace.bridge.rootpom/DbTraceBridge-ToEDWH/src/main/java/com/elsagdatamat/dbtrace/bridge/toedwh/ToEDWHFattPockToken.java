package com.elsagdatamat.dbtrace.bridge.toedwh;

public class ToEDWHFattPockToken extends ToEDWHToken {
	public static final String TOKEN_ID="DBTraceToEDWHFattPockBridge";
	
	@Override
	public String getServiceId() {
		return TOKEN_ID;
	}
}
