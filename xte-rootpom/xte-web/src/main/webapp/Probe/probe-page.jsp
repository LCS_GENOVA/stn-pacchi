<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
	<%!
			String getFormattedDate()
    		{
				java.util.Date now = new java.util.Date();
				String nowFormatted = "";
				java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
				nowFormatted = df.format(now);
        		return nowFormatted;
    		}
		%>
	<h2>
		Web Service XTE v1.0.0 - It works! The time is now
		<%= getFormattedDate() %></h2>
</body>
</html>