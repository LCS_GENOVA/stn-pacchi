package com.elsagdatamat.dbtrace.bridge.engine.bl;

import com.elsagdatamat.dbtrace.bridge.engine.dao.AptItemDao;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItem;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition;
import com.elsagdatamat.framework.dao.IGeneralDAO;

public class AptItemBLImpl implements AptItemBL {

	private static final boolean BLOCKING = false;

	private AptItemDao aptItemDao;

	private IGeneralDAO generalDao;

	public IGeneralDAO getGeneralDao() {
		return generalDao;
	}

	public void setGeneralDao(IGeneralDAO generalDao) {
		this.generalDao = generalDao;
	}

	public AptItemDao getAptItemDao() {
		return aptItemDao;
	}

	public void setAptItemDao(AptItemDao aptItemDao) {
		this.aptItemDao = aptItemDao;
	}

	public void insertOrUpdate(AptItem item){
			aptItemDao.insertOrUpdate(item);
	}

	public String getSubPType(String codPacco){
		String sqlQuery = "select cast(sp.id as varchar2(4)) "+
             "from p_tipocodice tc "+
             "join p_tipocodice_sottoprodotto tcs on (tc.id = tcs.tipocodice_id) "+
             "join p_sottoprodotto sp on (tcs.sottoprodotto_id = sp.id) "+
             "where regexp_like ('"+codPacco+"', tc.code_format_regex)";

		Object obj = generalDao.executeNativeSqlSingleValue(sqlQuery);
		if(obj == null)
			return null;
		return String.valueOf(obj);
	}

	@Override
	public void insert(AptItem item){
			aptItemDao.insert(item);
	}

	@Override
	public AptItem find(String itemCode) {
		return aptItemDao.findById(itemCode, BLOCKING);
	}

	@Override
	public void insertAptItemAndAptItemPosition(AptItem item, AptItemPosition itemPosition) {
		aptItemDao.insertAptItemAndAptItemPosition(item, itemPosition);
	}

	@Override
	public void insertAptItemAndUpdateAptItemPosition(AptItem item, AptItemPosition itemPosition) {
		aptItemDao.insertAptItemAndUpdateAptItemPosition(item, itemPosition);
	}
}
