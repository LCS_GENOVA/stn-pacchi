package com.selexelsag.xte.persistence.dao;

import com.selexelsag.xte.persistence.entities.Services;

/**
 * Home object for domain model class Services.
 * @see com.selexelsag.xte.persistence.entities.Services
 * @author Hibernate Tools
 */
public class ServicesDAO extends BaseDAO<Services>
{

//	private static final Log log = LogFactory.getLog(ServicesDAO.class);

	public ServicesDAO() 
	{
		super(Services.class);
	}
}
