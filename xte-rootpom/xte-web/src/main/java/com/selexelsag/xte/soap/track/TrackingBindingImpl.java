/**
 * TrackingBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.selexelsag.xte.soap.track;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.model.track.soap.TrackResponse;
import com.selexelsag.xte.service.context.SpringApplicationContext;
import com.selexelsag.xte.xte_presentation_logic.ITrackService;

public class TrackingBindingImpl implements com.selexelsag.xte.soap.track.Tracking{

	private static Log log = LogFactory.getLog(TrackingBindingImpl.class); 

	static CharsetEncoder asciiEncoder = Charset.forName("US-ASCII").newEncoder();

	private static boolean isPureAscii(String v) {
		return asciiEncoder.canEncode(v);
	}

	public TrackResponse trackMessage(java.lang.String serviceName, java.lang.String channel, java.lang.String channelID, java.lang.String eventName, java.lang.String version, java.lang.String trackData) throws java.rmi.RemoteException {
		try{
			if (isPureAscii(trackData)) {
				log.info(String.format("SOAP Service BEGINS: service=[%1$s], channel=[%2$s], channelId=[%3$s], eventName=[%4$s], version=[%5$s]", serviceName, channel, channelID, eventName, version));
			} else {
				log.info(String.format("SOAP Service BEGINS: service=[%1$s], channel=[%2$s], channelId=[%3$s], eventName=[%4$s], version=[%5$s], trackData=[%6$s]", serviceName, channel, channelID, eventName, version, trackData));
			}
			
			ITrackService trackService = (ITrackService)SpringApplicationContext.getBean(ITrackService.TRACK_SRV_BEAN_NAME); 
			com.selexelsag.xte.model.track.TrackResponse restResponse = trackService.processRequest(getTrackRequest(serviceName, channel, channelID, eventName, version, trackData));
			
			log.info(String.format("SOAP Service ENDS: trackResponse=[%1$s]", restResponse));
			return new TrackResponse(restResponse);
		}catch (Throwable t){
			log.error(String.format("Unexpected error processing soap request: serviceName=[%1$s] channel=[%2$s], channelID=[%3$s], eventName=[%4$s], version=[%5$s]", serviceName, channel, channelID, eventName, version), t);
			TrackResponse ans = new TrackResponse(serviceName, channel, channelID, eventName);
			ans.setErrorCode(ITrackService.RESULT_ERROR_INTERNAL_KEY);
			return ans;
		}
	}

	private TrackRequest getTrackRequest(String service,String channel,String channelId,String eventName,String version,String trackData){
		TrackRequest ans = new TrackRequest();
		ans.setService(new StringBuilder(service));
		ans.setChannel(new StringBuilder(channel));
		if (channelId != null && !channelId.isEmpty()) {
			ans.setSourceOfficeId(new StringBuilder(channelId));
		} else {
			ans.setSourceOfficeId(new StringBuilder("PTL"));
		}
		ans.setDestinationOfficeId(new StringBuilder(channelId));
		ans.setEventName(new StringBuilder(eventName));
		ans.setVersion(new StringBuilder(version));
		ans.setTrackData(new StringBuilder(trackData));
		return ans;
	}

	

}
