package com.selexelsag.xte.business.tracking;

import java.util.Map;

import com.selexelsag.xte.business.exceptions.BusinessMessageProcessorException;
import com.selexelsag.xte.model.message_processor.MessageProcessor;

public interface IMessageProcessorFactory {

	public static final String MESSAGE_PROCESSOR_FACTORY_BEAN_NAME = "messageProcessorFactory";
	
	/**
	 * Restituisce la lista dei flussi asincroni gestiti dallo scheduler
	 * @return String array
	 */
	public String[] getManagedFlows();
	
	/**
	 * Restituisce il numero di righe che lo scheduler deve processare
	 * per ogni ciclo e per uno specifico flusso.
	 * @param flowId il flowid del flusso.
	 * @return il numero di messaggi.
	 */
	public int getRowCount(String flowId);
	
	/**
	 * Restituisce la classe deputata al processamento specifico 
	 * di un flusso asincrono.
	 * @param flowId il flowId del flusso
	 * @return l'implementazione specifica al flusso specificato
	 * @throws BusinessMessageProcessorException in caso di errore nel recupero della classe.
	 */
	public MessageProcessor getMessageProcessor(String flowId) throws BusinessMessageProcessorException;

}
