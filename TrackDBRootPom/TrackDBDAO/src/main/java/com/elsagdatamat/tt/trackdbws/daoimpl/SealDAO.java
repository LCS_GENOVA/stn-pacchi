package com.elsagdatamat.tt.trackdbws.daoimpl;

import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.tt.trackdbws.beans.Seal;
import com.elsagdatamat.tt.trackdbws.dao.ISealDao;


public class SealDAO  extends SpringHibernateJpaDAOBase<Seal, Long> implements ISealDao{

}
