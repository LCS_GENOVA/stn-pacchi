package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.CodiciProdottoPICodiciProdottoSDA;
import com.elsagdatamat.dbtrace.bridge.engine.entities.CodiciProdottoPICodiciProdottoSDAPK;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface ICodiciProdottoPICodiciProdottoSDADAO extends IGenericDAO<CodiciProdottoPICodiciProdottoSDA, CodiciProdottoPICodiciProdottoSDAPK>{

}
