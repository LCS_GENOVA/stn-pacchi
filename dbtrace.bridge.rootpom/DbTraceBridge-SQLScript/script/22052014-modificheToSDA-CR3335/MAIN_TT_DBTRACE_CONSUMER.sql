WHENEVER OSERROR EXIT ROLLBACK;

WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK;

spool MAIN_TT_DBTRACE_CONSUMER.lst

prompt ===>>>>
prompt ===>>>>
prompt ===>>>>  Se in fondo al file di spool NON compare la scritta FINE  significa che si � verificato un ERRORE
prompt ===>>>>  NON PROSEGUIRE L'INSTALLAZIONE
prompt ===>>>>
prompt ===>>>>

prompt === creazione nuove tabelle e sequence (script: CREATE_TABLE.sql)

@CREATE_TABLE.sql

COMMIT;

prompt ==
prompt ========================  ========================  ========================  
prompt ========================            FINE            ========================
prompt ========================  ========================  ========================  
prompt ==

spool off

