package com.elsagdatamat.dbtrace.bridge.toedwh;


public class ToEDWHToken{
	private String wsToken;
	protected String serviceId;
	public String getWsToken() {
		return wsToken;
	}
	public void setWsToken(String wsToken) {
		this.wsToken = wsToken;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
}
