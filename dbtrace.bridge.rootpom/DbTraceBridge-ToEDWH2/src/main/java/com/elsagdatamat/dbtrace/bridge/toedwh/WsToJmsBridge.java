package com.elsagdatamat.dbtrace.bridge.toedwh;

import java.util.Calendar;
import java.util.TimeZone;

public class WsToJmsBridge extends com.elsagdatamat.dbtrace.bridge.wstojms.WsToJmsBridge {

	public void initEDWH2() {
		// parametri per EDWH
		if (parameterManager != null) {
			Integer modetmp = getIntParameter("DBTraceToEDWHBridgeMode");
			if (modetmp != null)
				mode = modetmp;
			Integer windowSizeTmp = getIntParameter("DBTraceToEDWHBridgeWindowSize");
			if (windowSizeTmp != null)
				windowSize = windowSizeTmp;
			Integer windowFromTmp = getIntParameter("DBTraceToEDWHBridgeWindowFrom");
			if (windowFromTmp != null)
				windowFrom = windowFromTmp;
		}
	}

	@Override
	public void unicoMetodoDaChiamareDaQuartz() {
		if (parameterManager != null && !parameterManager.isEnabled(tracesReader.getserviceId())) {
			log.info(String.format("Service %s is disabled", tracesReader.getserviceId()));
			return;
		}
		initEDWH2();
		super.unicoMetodoDaChiamareDaQuartz();
	}


	/**
	 * Gestione per modalita' 3 - finestra temporale
	 * deve essere applicata non solo all'orario di partenza ma
	 * deve fermare il task quando l'orario � finito
	 * @return
	 */
	@Override
	protected boolean canWork(){
		boolean canWork = true;
		initEDWH2(); // rileggo i parametri
		if (mode != null && mode == 3) {
			canWork = false;
			Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			Integer windowTo = (windowFrom + windowSize) % 24;

			log.debug(String.format("UTC NOW=%d; WindowFrom=%d; WindowTo=%d", c.get(Calendar.HOUR_OF_DAY), windowFrom, windowTo));

			if (windowTo > windowFrom) {
				canWork = c.get(Calendar.HOUR_OF_DAY) >= windowFrom && c.get(Calendar.HOUR_OF_DAY) < windowTo;

			} else {
				canWork = c.get(Calendar.HOUR_OF_DAY) >= windowFrom || c.get(Calendar.HOUR_OF_DAY) < windowTo;

			}
		}
		return canWork;
	}

}
