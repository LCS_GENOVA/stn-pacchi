package com.elsagdatamat.tt.trackdbws.beans.VO;

import java.io.Serializable;

import com.elsagdatamat.tt.trackdbws.beans.EsiPackPIFilter;
import com.elsagdatamat.tt.trackdbws.beans.EsiPackSDAFilter;
import com.elsagdatamat.tt.trackdbws.beans.Trace;

public class EsiPackVO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Trace trace;
	private EsiPackPIFilter esiPackPIFilter;
	private EsiPackSDAFilter esiPackSDAFilter;
	
	public EsiPackVO(Object trace,  EsiPackPIFilter esiPackPIFilter){
		this.trace= (Trace) trace;
		this.esiPackPIFilter=esiPackPIFilter;
	}
	
	public EsiPackVO(Object trace,  EsiPackSDAFilter esiPackSDAFilter){
		this.trace= (Trace) trace;
		this.esiPackSDAFilter=esiPackSDAFilter;
	}
	
	
	public Trace getTrace() {
		return trace;
	}
	public void setTrace(Trace trace) {
		this.trace = trace;
	}
	public String getTraceType() {
		
		return (esiPackPIFilter!=null?String.valueOf(esiPackPIFilter.getTracesType()):
			(esiPackSDAFilter!=null?String.valueOf(esiPackSDAFilter.getTracesType()):null));
	}
		
}
