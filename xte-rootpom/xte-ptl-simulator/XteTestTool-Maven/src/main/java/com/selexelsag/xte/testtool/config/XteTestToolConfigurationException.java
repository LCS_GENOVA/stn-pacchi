/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.config;

import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;


/**
 *
 * @author Da Procida
 */
public class XteTestToolConfigurationException extends BaseException {

    public XteTestToolConfigurationException(Group group, Severity severity) {
        super(group, severity);
    }

    public XteTestToolConfigurationException(Group group, Severity severity, String message) {
        super(group, severity, message);
    }

    public XteTestToolConfigurationException(Group group, Severity severity, String message, Throwable cause) {
        super(group, severity, message, cause);
    }

    public XteTestToolConfigurationException(Group group, Severity severity, Throwable cause) {
        super(group, severity, cause);
    }

}
