package com.elsagdatamat.dbtrace.bridge.tosda;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RemovedTrace")
public class SDARemovedTrace{
	
	@XmlElement(name = "trace")
	private SDATrace trace;

	public SDARemovedTrace() {
		super();
	}

	public SDARemovedTrace(SDATrace trace) {
		super();
		this.trace = trace;
	}
	
}
