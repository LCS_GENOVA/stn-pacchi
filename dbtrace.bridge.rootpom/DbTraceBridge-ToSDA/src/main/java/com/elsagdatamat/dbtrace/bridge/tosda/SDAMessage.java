package com.elsagdatamat.dbtrace.bridge.tosda;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "message")
public class SDAMessage {
	@XmlElement(name = "header")
	public SDAHeader header;
	
	@XmlElementWrapper(name= "traces")
	@XmlElement(name = "trace")
	public List<SDATrace> traces;
}
