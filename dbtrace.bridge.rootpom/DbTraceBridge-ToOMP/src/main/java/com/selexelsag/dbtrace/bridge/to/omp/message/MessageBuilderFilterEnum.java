/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.message;

import java.io.Serializable;

/**
 * @author arodriguez
 *
 */
public enum MessageBuilderFilterEnum implements Serializable {

	ALLOWED (".allowed"),
	NOTALLOWED (".notallowed");
	
	private String value;
	
	private MessageBuilderFilterEnum(String value) {
		this.value = value;
	}
	
	public final String value() {
		return value;
	}
}
