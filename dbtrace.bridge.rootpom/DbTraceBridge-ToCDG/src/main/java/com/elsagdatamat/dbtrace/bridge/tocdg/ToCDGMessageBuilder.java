package com.elsagdatamat.dbtrace.bridge.tocdg;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.validation.Schema;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.elsagdatamat.dbtrace.bridge.engine.MessageBuilderBase;
import com.elsagdatamat.dbtrace.bridge.engine.bl.IBLOfficeMapper;
import com.elsagdatamat.dbtrace.bridge.engine.bl.IConsumerProductManager;
import com.elsagdatamat.dbtrace.bridge.engine.entities.ConsumerProduct;
import com.elsagdatamat.framework.resources.ClassPathResource;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TokenTilltoDateByFiltersList;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesListNextToken;


public class ToCDGMessageBuilder extends MessageBuilderBase {
	protected final Log log = LogFactory.getLog(getClass());

	private JmsTemplate jmsTemplate;

	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	private ClassPathResource xsdPath;

	public void setXsdPath(ClassPathResource xsdPath) {
		this.xsdPath = xsdPath;
	}

	private String m1OrM2;

	public void setM1OrM2(String m1OrM2) {
		this.m1OrM2 = m1OrM2;
	}

	private IBLOfficeMapper blOfficeMapper;

	public void setBlOfficeMapper(IBLOfficeMapper blOfficeMapper) {
		this.blOfficeMapper = blOfficeMapper;
	}

	public void setConsumerProductManager(IConsumerProductManager consumerProductManager) {
		this.consumerProductManager = consumerProductManager;
	}

	private IConsumerProductManager consumerProductManager;

	// IMessageBuilder
	@Override
	public List<String> create(TracesList input) { // throws
		// SAXException,
		log.debug("Entering create");
		List<String> sList = new ArrayList<String>();

		try {
			Schema schema = null;
			if (xsdPath != null) {
				log.debug("Calling getSchema");
				schema = CdGConversionUtility.getSchemaFromXSD(xsdPath.getURL());
			}
			List<CdGMessage> cdgMsgList = null;
			if (m1OrM2.equals("M1")) {
				log.debug("Calling convertToCdGM1");
				cdgMsgList = CdGConversionUtility.convertToCdGM1(input.getTraces(), blOfficeMapper);
			} else if (m1OrM2.equals("M2")) {
				log.debug("Calling convertToCdGM2");
				cdgMsgList = CdGConversionUtility.convertToCdGM2(input.getTraces(), blOfficeMapper);
			} else {
				log.error("Errore: messaggio non previsto: " + m1OrM2);
			}

			if (cdgMsgList==null) {
				log.debug("Leaving create");
				return sList;
			}
			for (CdGMessage m : cdgMsgList) {
				log.debug("Calling marshal");
				try {
					String s = CdGConversionUtility.marshal(m, schema);
					if (isSaveTraces()) {
						discardedTraceManager.saveDiscardedTraces( token.getServiceId(), s, null);
					}
					sList.add(s);
				} catch (Exception e) {
					String str = CdGConversionUtility.marshal(m, null);
					discardedTraceManager.saveDiscardedTraces( token.getServiceId(), str, e);
				}
			}

		} catch (Exception e) {
			log.debug("Leaving create");
			log.error(e.getMessage());
			return sList;
		}
		log.debug("Leaving create");
		return sList;
	}

	// IMessageSender
	@Override
	public void send(final List<String> strList) {
		log.debug("Entering send");
		for (final String str : strList) {
			jmsTemplate.send(new MessageCreator() {
				public Message createMessage(Session session) throws JMSException {
					TextMessage tm = session.createTextMessage();
					tm.setText(str);
					return tm;
				}
			});
		}
		log.debug("Leaving send");
	}

	// ITracesReader
	@Override
	public TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate) {
		log.debug(String.format("Entering getTraces for service '%s' (wsToken='%s'): stopDate=%s;", token.getServiceId(), token.getWsToken(), stopDate));

		// Costruisco il parametro per la richiesta delle tracce
		// Voglio tutte le tracce a partire dal token precedentemente ottenuto
		// fino a stopDate (passato dal motore che lo inizializza una volta per
		// tutte)....
		TracesList traceEqualList = new TracesList();

		List<ConsumerProduct> involvedProductList = consumerProductManager.listInvolvedProducts( token
				.getServiceId());
		for (ConsumerProduct product : involvedProductList) {
			Trace traceEqual = new Trace();
			traceEqual.setTracedEntity("PACCO");
			traceEqual.setChannel("SDA");
			if (m1OrM2.equals("M1")) {
				traceEqual.setWhatHappened("RIT_SDA");
			} else if (m1OrM2.equals("M2")) {
				traceEqual.setWhatHappened("ESI_SDA");
			} else {
				log.error("Errore: messaggio non previsto: " + m1OrM2);
				return null;
			}
			traceEqual.setLabelTracedEntity(product.getProductType());
			traceEqualList.getTraces().add(traceEqual);
		}

		TokenTilltoDateByFiltersList getTracesInput = new TokenTilltoDateByFiltersList();
		getTracesInput.setTilltoDate(getGCalendar(stopDate));

		getTracesInput.setTracesFilterByExampleEqual(traceEqualList);
		getTracesInput.setToken( token.getWsToken());
		log.debug("Calling web service getTracesByFiltersList");
		TracesListNextToken tlnt = twsp.getTracesByFiltersList(getTracesInput);

		log.debug("Called web service getTracesByFiltersList");
		TracesList tracesList = tlnt.getTraceList();
		// Prese le tracce, aggiorno il wsToken dentro lo SDAToken
		token.setWsToken(tlnt.getNextToken());

		log.debug("Leaving getTraces");
		return tracesList;
	}
}
