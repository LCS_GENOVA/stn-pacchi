package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "APT_ITEM_POSITION", schema="TT_APPLICATION")
public class AptItemPosition{
	public enum AptStatusEnum {
		CUSTOMER,
		COURIER
	}

	public enum LockerSizeEnum {
		S,
		M,
		L
	}

	@Id
	@Column(name = "ITEM_CODE")
	private String code;
	@Column(name = "FRAZIONARIO_GEO")
	private String frazionarioGeopost;
	@Column(name = "LOCKER_NUMBER", insertable=false)
	private int number;
	@Column(name = "LOCKER_SIZE", insertable=false)
	@Enumerated(EnumType.STRING)
	private LockerSizeEnum size;
	@Column(name = "APT_STATUS")
	@Enumerated(EnumType.STRING)
	private AptStatusEnum status;
	@Column(name = "POSITION_DATE")
	private Date timestamp;
	@Column(name = "PIN")
	private String pin;
	@Column(name = "QR_CODE")
	private String qrCode;
	@Column(name = "POSITION_OPERATOR")
	private Long operator;
    @Column(name = "DELIVERED")
    @Type(type = "true_false")
    private Boolean delivered;

    public Boolean getDelivered() {
        return delivered;
    }

    public void setDelivered(Boolean delivered) {
        this.delivered = delivered;
    }

    public AptItemPosition() {
    }

	public AptItemPosition(String code, String frazionario) {
		this.code = code;
		this.frazionarioGeopost = frazionario;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public AptStatusEnum getStatus() {
		return status;
	}

	public void setStatus(AptStatusEnum status) {
		this.status = status;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public String getFrazionarioGeopost() {
		return frazionarioGeopost;
	}

	public void setFrazionarioGeopost(String frazionarioGeopost) {
		this.frazionarioGeopost = frazionarioGeopost;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public LockerSizeEnum getSize() {
		return size;
	}

	public void setSize(LockerSizeEnum size) {
		this.size = size;
	}

	public Long getOperator() {
		return operator;
	}

	public void setOperator(Long operator) {
		this.operator = operator;
	}
}
