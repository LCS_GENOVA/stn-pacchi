package com.selexelsag.xte.persistence.dao;

import com.selexelsag.xte.persistence.entities.Events;

/**
 * Home object for domain model class Events.
 * @see com.selexelsag.xte.persistence.entities.Events
 * @author Hibernate Tools
 */
public class EventsDAO extends BaseDAO<Events>
{
//	private static final Log log = LogFactory.getLog(EventsDAO.class);

	public EventsDAO() 
	{
		super(Events.class);
	}
}
