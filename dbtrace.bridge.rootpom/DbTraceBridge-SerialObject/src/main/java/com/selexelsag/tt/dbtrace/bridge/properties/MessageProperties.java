package com.selexelsag.tt.dbtrace.bridge.properties;

public class MessageProperties {

	public final static String EXTERNAL_SYSTEM_LABEL_PROPERTY = "System";
	public final static String MESSAGE_TYPE_PROPERTY = "ObjectType";

}
