package com.elsagdatamat.dbtrace.bridge.engine.bl;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.dbtrace.bridge.engine.entities.RemovedTrace;


public interface IRemovedTraceBL {
	
	@Transactional(value="dbTraceBridge-transactionManager", propagation = Propagation.REQUIRES_NEW)
	void insert(RemovedTrace trace);
}
 