/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.pr;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.controller.common.XteTestToolFieldValidator;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XteAccettazionePezzi;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;
import com.selexelsag.xte.testtool.view.objectmodel.XteAccettazionePezziTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteTestToolDuplicateKeyException;
import com.selexelsag.xte.testtool.view.pr.XteTestToolAccettazionePezziPRView;
import com.selexelsag.xte.testtool.view.pr.XteTestToolWebServicePRView;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata.AP;
import com.selexelsag.xtetesttool.xml.postaregistrata.ap.DatiInvio;
import com.selexelsag.xtetesttool.xml.postaregistrata.ap.ObjectFactory;
import com.selexelsag.xtetesttool.xml.postaregistrata.ap.Traccia;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

/**
 *
 * @author Tassara
 */
public class XteTestToolAccettazionePezziPRController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener, ListSelectionListener, TableModelListener {

    private XteTestToolAccettazionePezziPRView accettazionePezziPRView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolAccettazionePezziPRController.class);
    private XteAccettazionePezzi selectedAccettazionePezzi = null;
    private int currentSelectedRow = -1;
    private List<JTextComponent> accettazionePezziFieldList = new ArrayList<JTextComponent>();
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    
    private JAXBContext context;
    
    private static Traccia accettazionePezziTraccia;

    static {
        logger.debug("Creating Config Object Factory and Traccia");
        PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        AP config = postaRegistrataConfig.getAP();

        accettazionePezziTraccia = objectFactory.createTraccia();
        accettazionePezziTraccia.setEvento(config.getEventName());
        //accettazionePezziTraccia.setHeaderMazzetto(objectFactory.createHeaderMazzetto());
        accettazionePezziTraccia.setListaInvii(objectFactory.createListaInvii());
    }
    
    
    public XteTestToolAccettazionePezziPRController(JPanel accettazionePezziPRView) {
        try {
            logger.info("XteTestToolAccettazionePezziPRController start class creation");
            this.accettazionePezziPRView = (XteTestToolAccettazionePezziPRView) accettazionePezziPRView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.accettazionePezziPRView.initializeView();
            
            updateView();
            
            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.postaregistrata.ap");
            
            logger.info("XteTestToolAccettazionePezziPRController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAccettazionePezziPRController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolAccettazionePezziPRController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller creation failed", ex);
        }

    }
    
    private void updateDataModel() {
        //String id = accettazionePezziPRView.getAPCodiceTextRef().getText();
        //String opid = accettazionePezziPRView.getAPIdOpTextRef().getText();
        //logger.debug("updateDataModel - " + id + "," + opid);
        //accettazionePezziTraccia.getHeaderMazzetto().setIdOperatoreTP(new String(opid));
        //accettazionePezziTraccia.getHeaderMazzetto().setOfficeFrazionario(accettazionePezziPRView.getAPFrazionariTextRef().getText());
        ObjectFactory objectFactory = new ObjectFactory();
        XteAccettazionePezzi[] entries = accettazionePezziPRView.getAccettazionePezziTableModelRef().getMap().values().toArray(new XteAccettazionePezzi[0]);
        logger.debug("entries count: " + entries.length);        
        accettazionePezziTraccia.getListaInvii().getDatiInvio().clear();
        for (int i=0;i<entries.length;i++) {
        
            XteAccettazionePezzi item = entries[i];
            logger.debug("item " + i + ": " + item);
            if (item == null) 
                continue;
            DatiInvio obj = objectFactory.createDatiInvio();
            obj.setTipoMessaggio(item.getAccettazionePezziTipo());
            obj.setidOperatore(item.getAccettazionePezziIdOp());
            obj.setOfficeFrazionario(item.getAccettazionePezziOffFraz());
            obj.setNumeroSpedizione(item.getAccettazionePezziNumSped());
            obj.setCodiceCliente(item.getAccettazionePezziCodeCliente());
            obj.setCodice(item.getAccettazionePezziCode());
            obj.setProdotto(item.getAccettazionePezziProduct());
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(item.getAccettazionePezziDateTime());
            try {
                obj.setDataOraRitiro(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            } catch (DatatypeConfigurationException ex) {
                logger.error("Error converting date " + c, ex);
                obj.setDataOraRitiro(null);
            }
            obj.setPeso(item.getAccettazionePezziPeso());
            obj.setPrezzo(item.getAccettazionePezziPrezzo());
            obj.setPercetto(item.getAccettazionePezziPercetto());
            obj.setDestinazione(item.getAccettazionePezziDestinazione());
            obj.setDestinatario(item.getAccettazionePezziDestinatario());
            obj.setIndirizzo(item.getAccettazionePezziIndirizzo());
            obj.setCap(item.getAccettazionePezziCAP());
            obj.setNominativoMittente(item.getAccettazionePezziNomeMitt());
            obj.setDestinazioneMittente(item.getAccettazionePezziDestMitt());
            obj.setIndirizzoMittente(item.getAccettazionePezziIndMitt());
            obj.setCapMittente(item.getAccettazionePezziCAPMitt());
            obj.setSA(item.getAccettazionePezziSA());
            if(item.getAccettazionePezziCodAR() != null && !item.getAccettazionePezziCodAR().equals(""))
                obj.setCodiceAR(item.getAccettazionePezziCodAR());
            if(item.getAccettazionePezziValAss() != 0)
                obj.setValoreAssicurato(item.getAccettazionePezziValAss());
            if(item.getAccettazionePezziImpContr() != 0)
            obj.setImportoContrassegno(item.getAccettazionePezziImpContr());
            if(item.getAccettazionePezziIBAN() != null && !item.getAccettazionePezziIBAN().equals(""))
                obj.setIBAN(item.getAccettazionePezziIBAN());
            obj.setOraTimeDefinite(item.getAccettazionePezziOraTimeDef());
            accettazionePezziTraccia.getListaInvii().getDatiInvio().add(obj);
        }
    }
    
    
    private void updateView() {  
        //String id = accettazionePezziTraccia.getHeaderMazzetto().getIdUnivoco();
        //String opid = accettazionePezziTraccia.getHeaderMazzetto().getIdOperatoreTP();
        //logger.debug("updateView - " + id + "," + opid);
                
//        if (id != null) {
//            accettazionePezziPRView.getIDUnivocoTextRef()
//                    .setText(id);
//        }

//        if (opid != null) {
//            accettazionePezziPRView.getAPIdOpTextRef()
//                    .setText(opid);
//        }

        accettazionePezziPRView.getAccettazionePezziTableModelRef().clearModel();
            
        for (int i=0;i<accettazionePezziTraccia.getListaInvii().getDatiInvio().size();i++) {
            DatiInvio item = accettazionePezziTraccia.getListaInvii().getDatiInvio().get(i);
            XteAccettazionePezzi obj = new XteAccettazionePezzi();
            obj.setAccettazionePezziCode(item.getCodice());
            obj.setAccettazionePezziDateTime(item.getDataOraRitiro().toGregorianCalendar().getTime());
            obj.setAccettazionePezziTipo(item.getTipoMessaggio());
            obj.setAccettazionePezziIdOp(item.getidOperatore());
            obj.setAccettazionePezziOffFraz(item.getOfficeFrazionario());
            obj.setAccettazionePezziNumSped(item.getNumeroSpedizione());
            obj.setAccettazionePezziCodeCliente(item.getCodiceCliente());
            obj.setAccettazionePezziProduct(item.getProdotto());
            obj.setAccettazionePezziPeso(item.getPeso());
            obj.setAccettazionePezziPrezzo(item.getPrezzo());
            obj.setAccettazionePezziPercetto(item.getPercetto());
            obj.setAccettazionePezziDestinazione(item.getDestinazione());
            obj.setAccettazionePezziDestinatario(item.getDestinatario());
            obj.setAccettazionePezziIndirizzo(item.getIndirizzo());
            obj.setAccettazionePezziCAP(item.getCap());
            obj.setAccettazionePezziNomeMitt(item.getNominativoMittente());
            obj.setAccettazionePezziDestMitt(item.getDestinazioneMittente());
            obj.setAccettazionePezziIndMitt(item.getIndirizzoMittente());
            obj.setAccettazionePezziCAPMitt(item.getCapMittente());
            obj.setAccettazionePezziSA(item.getSA());
            obj.setAccettazionePezziCodAR(item.getCodiceAR());
            if(item.getValoreAssicurato() != null)
                obj.setAccettazionePezziValAss(item.getValoreAssicurato());
            if(item.getImportoContrassegno() != null)
                obj.setAccettazionePezziImpContr(item.getImportoContrassegno());        
            obj.setAccettazionePezziIBAN(item.getIBAN());
            obj.setAccettazionePezziOraTimeDef(item.getOraTimeDefinite());
            accettazionePezziPRView.getAccettazionePezziTableModelRef().addAccettazionePezziItem(obj);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = accettazionePezziPRView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        accettazionePezziPRView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.accettazionePezziPRView.getAccettazionePezziPanelRef().addAncestorListener(this);        
        this.accettazionePezziPRView.getAPIdOpTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPFrazionariTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPNumSpedTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPCodiceClienteTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPProductTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPPesoTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPPrezzoTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPPercettoTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPDestinazioneTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPDestinatarioTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPIndirizzoTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPCAPTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPNomeMittenteTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPDestMittenteTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPIndMittenteTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPCAPMittTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPSAJComboBoxRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPValAssTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPImpContrTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPOraTimeDefTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPCodARTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPIBANTextRef().addFocusListener(this);
        this.accettazionePezziPRView.getAPFrazionariTextRef().addFocusListener(this);
        
        this.accettazionePezziPRView.getAccettazionePezziDatePickerRef().getEditor().addFocusListener(this);
        JFormattedTextField startTimejFormattedTextField = ((JSpinner.DateEditor)accettazionePezziPRView.getAccettazionePezziTimeSpinnerRef().getEditor()).getTextField();
        startTimejFormattedTextField.addFocusListener(this);
        this.accettazionePezziPRView.getAccettazionePezziTableRef().getSelectionModel().addListSelectionListener(this);
        this.accettazionePezziPRView.getAccettazionePezziTableRef().getModel().addTableModelListener(this);
        this.accettazionePezziPRView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.accettazionePezziPRView.getAggiungiItemButtonRef().addActionListener(this);
        this.accettazionePezziPRView.getEliminaItemButtonRef().addActionListener(this);
        this.accettazionePezziPRView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.accettazionePezziPRView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.accettazionePezziPRView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.accettazionePezziPRView.getBackButtonRef().addActionListener(this);
        this.accettazionePezziPRView.getHomeButtonRef().addActionListener(this);
        this.accettazionePezziPRView.getExitButtonRef().addActionListener(this);
        this.accettazionePezziPRView.getTestButtonRef().addActionListener(this);
        this.accettazionePezziPRView.getAPSAJComboBoxRef().addActionListener(this);
        this.accettazionePezziPRView.getAPTypeComboBoxRef().addActionListener(this);
        
        this.accettazionePezziPRView.getAPCodiceTextRef().getDocument().addDocumentListener(this);
        this.accettazionePezziPRView.getAPProductTextRef().getDocument().addDocumentListener(this);
        this.accettazionePezziPRView.getAccettazionePezziDatePickerRef().getEditor().getDocument().addDocumentListener(this);
        ((JSpinner.DateEditor)accettazionePezziPRView.getAccettazionePezziTimeSpinnerRef().getEditor()).getTextField().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPPesoTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPPrezzoTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPPercettoTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPDestinazioneTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPDestinatarioTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPIndirizzoTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPCAPTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPNomeMittenteTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPDestMittenteTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPIndMittenteTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPCAPMittTextRef().getDocument().addDocumentListener(this);
        
        accettazionePezziPRView.getAPValAssTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPImpContrTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPOraTimeDefTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPCodARTextRef().getDocument().addDocumentListener(this);
        accettazionePezziPRView.getAPIBANTextRef().getDocument().addDocumentListener(this);
        
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPIdOpTextRef());
        //accettazionePezziFieldList.add(accettazionePezziPRView.getAPNumSpedTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPFrazionariTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPCodiceTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPCodiceClienteTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPProductTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAccettazionePezziDatePickerRef().getEditor());
        accettazionePezziFieldList.add(((JSpinner.DateEditor)accettazionePezziPRView.getAccettazionePezziTimeSpinnerRef().getEditor()).getTextField());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPPesoTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPPrezzoTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPPercettoTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPDestinazioneTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPDestinatarioTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPIndirizzoTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPCAPTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPNomeMittenteTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPDestMittenteTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPIndMittenteTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPCAPMittTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPCodARTextRef());
        //mailPieceFieldList.add(accettazionePezziPRView.getAPSATextRef());
        //accettazionePezziFieldList.add(accettazionePezziPRView.getAPValAssTextRef());
        //accettazionePezziFieldList.add(accettazionePezziPRView.getAPImpContrTextRef());
        accettazionePezziFieldList.add(accettazionePezziPRView.getAPOraTimeDefTextRef());
        //mailPieceFieldList.add(accettazionePezziPRView.getAPSmarritoCheck().isSelected() ? "T" : "F");
	this.salvaXmlFileChooser.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        
        if(source.equals(accettazionePezziPRView.getAPSAJComboBoxRef()) || source.equals(accettazionePezziPRView.getAPTypeComboBoxRef()))
        {
            checkAccettazionePezziFieldsFull();
        }
        
        if(source.equals(accettazionePezziPRView.getAggiungiItemButtonRef()))
        {
            logger.debug("XteTestToolAccettazionePezziPRController actionPerformed invoke addAccettazionePezziItem");
			
			updateDataModel();

            addAccettazionePezziItem();
        }
        
        if(source.equals(accettazionePezziPRView.getEliminaItemButtonRef()))
        {
            logger.debug("XteTestToolAccettazionePezziPRController actionPerformed invoke removeAccettazionePezziItem");

			updateDataModel();

            removeAccettazionePezziItem();
        }

        if(source.equals(accettazionePezziPRView.getTestButtonRef()))
        {
            logger.debug("XteTestToolAccettazionePezziPRController actionPerformed invoke autocomplete");

            AutoCompleteFields();
        }
        
        if(source.equals(accettazionePezziPRView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolAccettazionePezziPRController actionPerformed invoke generaXmlData");

			updateDataModel();

            generaXmlData();
        }

        if(source.equals(accettazionePezziPRView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolAccettazionePezziPRController actionPerformed invoke impostaDatiWebService");

			updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(accettazionePezziPRView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolAccettazionePezziPRController actionPerformed invoke salvaXmlData");

			updateDataModel();

            salvaXmlData();
        }

        if(source.equals(accettazionePezziPRView.getBackButtonRef()))
        {
            logger.debug("XteTestToolAccettazionePezziPRController actionPerformed invoke back");

			updateDataModel();

            back();
        }

        if(source.equals(accettazionePezziPRView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolAccettazionePezziPRController actionPerformed invoke home");

			updateDataModel();

            home();
        }

        if(source.equals(accettazionePezziPRView.getExitButtonRef()))
        {
            logger.debug("XteTestToolAccettazionePezziPRController actionPerformed invoke exit");
			
			updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolAccettazionePezziPRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(accettazionePezziPRView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolAccettazionePezziPRView)
        {
            logger.debug("XteTestToolAccettazionePezziPRController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAccettazionePezziPRController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAccettazionePezziPRController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        checkAccettazionePezziFieldsFull();  
    }    
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object source = e.getSource();
        if(source.equals(accettazionePezziPRView.getAccettazionePezziTableRef().getSelectionModel()))
        {               
            if (e.getValueIsAdjusting()) {
                    return;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
                        currentSelectedRow = accettazionePezziPRView.getAccettazionePezziTableRef().getSelectedRow();
                        logger.debug("XteTestToolAccettazionePezziPRController valueChanged currentSelectedRow = " + currentSelectedRow);
                        logger.debug("XteTestToolAccettazionePezziPRController valueChanged: " + ((XteAccettazionePezziTableModel)accettazionePezziPRView.getAccettazionePezziTableRef().getModel()).getMap().size());
                        if(currentSelectedRow!=-1)
                        {
                            List<Component> enableComponents = new ArrayList<Component>();
                            enableComponents.add(accettazionePezziPRView.getEliminaItemButtonRef());
                            componentStatusManager.enableComponent(enableComponents);
                            selectedAccettazionePezzi = ((XteAccettazionePezziTableModel)accettazionePezziPRView.getAccettazionePezziTableRef().getModel()).getSelectedItem(currentSelectedRow);
                        }
                        else
                        {
                            List<Component> disableComponents = new ArrayList<Component>();
                            disableComponents.add(accettazionePezziPRView.getEliminaItemButtonRef());
                            componentStatusManager.disableComponent(disableComponents);                                                        
                            selectedAccettazionePezzi = null;
                        }
                    }
                } );
        }
        
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int eventType = e.getType();
        DefaultTableModel model = (DefaultTableModel) e.getSource();
        if(model.equals(accettazionePezziPRView.getAccettazionePezziTableRef().getModel()))
        {               
            logger.debug("XteTestToolAccettazionePezziPRController tableChanged: " + ((XteAccettazionePezziTableModel)accettazionePezziPRView.getAccettazionePezziTableRef().getModel()).getMap().size());
        }
    }

    private void checkAccettazionePezziFieldsFull() {

        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        Object SAobj = accettazionePezziPRView.getAPSAJComboBoxRef().getSelectedItem();
        String SA = null;
        if(SAobj != null)
            SA = SAobj.toString();
        List<Component> disableComponents = new ArrayList<Component>();
        disableComponents.add(accettazionePezziPRView.getAggiungiItemButtonRef());
        
        //CodiceAR Facoltativo se SA == AR2
        //Valore assicurato Obligatorio se SA == AS0
        //Imp contrassegno Obligatorio se SA == CA0
        //Iban Obligatorio se SA == CA3
        if(SA == null)
        {
            componentStatusManager.disableComponent(disableComponents);                                                        
            return;
        }
        if(SA.equals("AR2"))
            accettazionePezziFieldList.remove(accettazionePezziPRView.getAPCodARTextRef());
                
        if(SA.equals("AS0") && accettazionePezziPRView.getAPValAssTextRef().getText().equals(""))
        {
            componentStatusManager.disableComponent(disableComponents);                                                        
            return;
        }
        if(SA.equals("CA0") && accettazionePezziPRView.getAPImpContrTextRef().getText().equals(""))
        {
            componentStatusManager.disableComponent(disableComponents);                                                        
            return;
        }
        if(SA.equals("CA3") && accettazionePezziPRView.getAPIBANTextRef().getText().equals(""))
        {
            componentStatusManager.disableComponent(disableComponents);                                                        
            return;
        }  
        
        for (JTextComponent field : accettazionePezziFieldList) {
            if (field.getText().trim().isEmpty()) {
                
                componentStatusManager.disableComponent(disableComponents);                                                        
                return;
            }
        }
        List<Component> enableComponents = new ArrayList<Component>();
        enableComponents.add(accettazionePezziPRView.getAggiungiItemButtonRef());
        componentStatusManager.enableComponent(enableComponents);
    }
    
    private void AutoCompleteFields()
    {
        accettazionePezziPRView.setAPFields();
        checkAccettazionePezziFieldsFull();
    }
    
    private void clearAccettazionePezziFields() {

        for (JTextComponent field : accettazionePezziFieldList) {
            if (!field.getText().trim().isEmpty()) {
                field.setText("");
            }
        }
        accettazionePezziPRView.getAPNumSpedTextRef().setText("");
        accettazionePezziPRView.getAPValAssTextRef().setText("");
        accettazionePezziPRView.getAPIBANTextRef().setText("");
        accettazionePezziPRView.getAPCodARTextRef().setText("");
        accettazionePezziPRView.getAPValAssTextRef().setText("");
        accettazionePezziPRView.getAPImpContrTextRef().setText("");
        accettazionePezziPRView.getAPSAJComboBoxRef().setSelectedIndex(-1);
        accettazionePezziPRView.getAPTypeComboBoxRef().setSelectedIndex(-1);
    }
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        accettazionePezziPRView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolAccettazionePezziPRController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.accettazionePezziPRView.customizeView();
            
            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(accettazionePezziPRView);
            updateXteTestToolDataModel(dataModel);

            clearAccettazionePezziFields();
            clearMessageBar();
            
            Configuration configuration = XteTestToolConfiguration.getInstance().getConfiguration();
            List<String> frazList = configuration.getList("PostaRegistrata.SA.List");
            DefaultComboBoxModel model = new DefaultComboBoxModel(frazList.toArray());                                    
            accettazionePezziPRView.getAPSAJComboBoxRef().setModel(model);
            accettazionePezziPRView.getAPSAJComboBoxRef().setSelectedIndex(-1);
            List<String> TypeList = configuration.getList("PostaRegistrata.APType.List");
            DefaultComboBoxModel model2 = new DefaultComboBoxModel(TypeList.toArray());                                    
            accettazionePezziPRView.getAPTypeComboBoxRef().setModel(model2);
            accettazionePezziPRView.getAPTypeComboBoxRef().setSelectedIndex(-1);
            
            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(accettazionePezziPRView.getAggiungiItemButtonRef());
            disabledComponents.add(accettazionePezziPRView.getEliminaItemButtonRef());
            disabledComponents.add(accettazionePezziPRView.getImpostaDatiWSButtonRef());
            disabledComponents.add(accettazionePezziPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            Calendar date = Calendar.getInstance();
            this.accettazionePezziPRView.getAccettazionePezziDatePickerRef().setDate(date.getTime());
            this.accettazionePezziPRView.getAccettazionePezziTimeSpinnerRef().setValue(date.getTime());


            logger.info("XteTestToolAccettazionePezziPRController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAccettazionePezziPRController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAccettazionePezziPRController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller init failed", ex);
        }

    }
    
    private void exit()
    {
        logger.info("XteTestToolAccettazionePezziPRController start exit");

        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(accettazionePezziPRView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolAccettazionePezziPRController exit in progress");
            accettazionePezziPRView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolAccettazionePezziPRController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(accettazionePezziPRView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(accettazionePezziPRView.getAccettazionePezziPanelRef().getParent(), accettazionePezziPRView.getAccettazionePezziPanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolAccettazionePezziPRController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(accettazionePezziPRView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(accettazionePezziPRView.getAccettazionePezziPanelRef().getParent(), accettazionePezziPRView.getAccettazionePezziPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private void addAccettazionePezziItem()
    {
        logger.info("XteTestToolAccettazionePezziPRController start addAccettazionePezziItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            
            clearMessageBar();
            
            XteTestToolMessageBar messageBar = new XteTestToolMessageBar();

            XteTestToolFieldValidator validator = new XteTestToolFieldValidator();            

                                    
            XteAccettazionePezzi item = new XteAccettazionePezzi();
            //String selectedItem = (String) accettazionePezziPRView.getAccettazionePezziTypeComboBoxRef().getModel().getSelectedItem();
            
            //item.setAccettazionePezziType(selectedItem);
            
            item.setAccettazionePezziTipo(accettazionePezziPRView.getAPTypeComboBoxRef().getSelectedItem().toString());
            item.setAccettazionePezziIdOp(accettazionePezziPRView.getAPIdOpTextRef().getText());
            item.setAccettazionePezziOffFraz(accettazionePezziPRView.getAPFrazionariTextRef().getText());
            item.setAccettazionePezziNumSped(accettazionePezziPRView.getAPNumSpedTextRef().getText());
            item.setAccettazionePezziCodeCliente(accettazionePezziPRView.getAPCodiceClienteTextRef().getText());
            item.setAccettazionePezziCode(accettazionePezziPRView.getAPCodiceTextRef().getText());
            item.setAccettazionePezziProduct(accettazionePezziPRView.getAPProductTextRef().getText());
            item.setAccettazionePezziPeso(accettazionePezziPRView.getAPPesoTextRef().getText());
            item.setAccettazionePezziPrezzo(Integer.parseInt(accettazionePezziPRView.getAPPrezzoTextRef().getText()));
            item.setAccettazionePezziPercetto(Integer.parseInt(accettazionePezziPRView.getAPPercettoTextRef().getText()));
            item.setAccettazionePezziDestinazione(accettazionePezziPRView.getAPDestinazioneTextRef().getText());
            item.setAccettazionePezziDestinatario(accettazionePezziPRView.getAPDestinatarioTextRef().getText());
            item.setAccettazionePezziIndirizzo(accettazionePezziPRView.getAPIndirizzoTextRef().getText());
            item.setAccettazionePezziCAP(accettazionePezziPRView.getAPCAPTextRef().getText());
            item.setAccettazionePezziNomeMitt(accettazionePezziPRView.getAPNomeMittenteTextRef().getText());
            item.setAccettazionePezziDestMitt(accettazionePezziPRView.getAPDestMittenteTextRef().getText());
            item.setAccettazionePezziIndMitt(accettazionePezziPRView.getAPIndMittenteTextRef().getText());
            item.setAccettazionePezziCAPMitt(accettazionePezziPRView.getAPCAPMittTextRef().getText());
            item.setAccettazionePezziSA(accettazionePezziPRView.getAPSAJComboBoxRef().getSelectedItem().toString());
            item.setAccettazionePezziCodAR(accettazionePezziPRView.getAPCodARTextRef().getText());
            if(!accettazionePezziPRView.getAPValAssTextRef().getText().isEmpty())
                item.setAccettazionePezziValAss(Integer.parseInt(accettazionePezziPRView.getAPValAssTextRef().getText()));
            if(!accettazionePezziPRView.getAPImpContrTextRef().getText().isEmpty())
                item.setAccettazionePezziImpContr(Integer.parseInt(accettazionePezziPRView.getAPImpContrTextRef().getText()));
            item.setAccettazionePezziIBAN(accettazionePezziPRView.getAPIBANTextRef().getText());
            item.setAccettazionePezziOraTimeDef(accettazionePezziPRView.getAPOraTimeDefTextRef().getText());
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss.SSS");
            //SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");            
            Date startDate = this.accettazionePezziPRView.getAccettazionePezziDatePickerRef().getDate();
            SpinnerDateModel startTimeSpinnerDateModel = (SpinnerDateModel) this.accettazionePezziPRView.getAccettazionePezziTimeSpinnerRef().getModel();
            Date startTime = startTimeSpinnerDateModel.getDate();
            String startDateStr = dateFormatter.format(startDate);
            String startTimeStr = timeFormatter.format(startTime);
            String startDateTimeStr = startDateStr + "T" + startTimeStr;   
            SimpleDateFormat lasttimeFormatter = new SimpleDateFormat("yyyy-MM-dd" + "'T'" + "HH:mm:ss");
            item.setAccettazionePezziDateTime((Date) lasttimeFormatter.parse(startDateTimeStr));
            
            DecimalFormat numberFormat = new DecimalFormat();

            try 
            {
                this.accettazionePezziPRView.getAccettazionePezziTableModelRef().addAccettazionePezziItem(item);
            }
            catch (XteTestToolDuplicateKeyException duplicateEx)
            {
                StyledDocument document = messageBar.putContentOnMessageBar("Attemp to insert duplicate key Item!", MessageType.WARNING);
                accettazionePezziPRView.getMessageBarTextPaneRef().setDocument(document);
                this.accettazionePezziPRView.getAPCodiceTextRef().requestFocus();
                return;
            }
            
            StyledDocument document = messageBar.putContentOnMessageBar("Item added successfully!", MessageType.CORRECT_PROCESSING);
            accettazionePezziPRView.getMessageBarTextPaneRef().setDocument(document);
                        
            clearAccettazionePezziFields();
            accettazionePezziPRView.getAPCodiceTextRef().requestFocus();            
            
            logger.info("XteTestToolAccettazionePezziPRController addAccettazionePezziItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAccettazionePezziPRController addAccettazionePezziItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAccettazionePezziPRController addAccettazionePezziItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller addAccettazionePezziItem failed", ex);
        }
    }
    
    private void removeAccettazionePezziItem()
    {
        logger.info("XteTestToolAccettazionePezziPRController start removeAccettazionePezziItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            if(currentSelectedRow!=-1)
            {         
                XteAccettazionePezziTableModel model = ((XteAccettazionePezziTableModel)accettazionePezziPRView.getAccettazionePezziTableRef().getModel());
                model.removeRow(currentSelectedRow);
                logger.debug("XteTestToolAccettazionePezziPRController removeAccettazionePezziItem: " + ((XteAccettazionePezziTableModel)accettazionePezziPRView.getAccettazionePezziTableRef().getModel()).getMap().size());
            }

            logger.info("XteTestToolAccettazionePezziPRController removeAccettazionePezziItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAccettazionePezziPRController removeAccettazionePezziItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAccettazionePezziPRController removeAccettazionePezziItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller removeAccettazionePezziItem failed", ex);
        }
    }

	private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }

    private void generaXmlData()
    {
        logger.info("XteTestToolAccettazionePezziPRController start generaXmlData");

        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();

        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            accettazionePezziPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(accettazionePezziPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(accettazionePezziPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            accettazionePezziPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(accettazionePezziPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(accettazionePezziPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(accettazionePezziTraccia, out);
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            accettazionePezziPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(accettazionePezziPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(accettazionePezziPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
            
        try
        {            
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString());
            accettazionePezziPRView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("XteTestToolAccettazionePezziPRController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAccettazionePezziPRController generaXmlData failed", baseEx);
			StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(baseEx.toString(), MessageType.ERROR);
            accettazionePezziPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(accettazionePezziPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(accettazionePezziPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAccettazionePezziPRController generaXmlData failed", ex);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            accettazionePezziPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(accettazionePezziPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(accettazionePezziPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        accettazionePezziPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(accettazionePezziPRView.getImpostaDatiWSButtonRef());
        enableComponent.add(accettazionePezziPRView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolAccettazionePezziPRController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServicePRView(accettazionePezziPRView.getParentFrame());
            XteTestToolWebServicePRController controller = new XteTestToolWebServicePRController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
            AP config = postaRegistrataConfig.getAP();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(accettazionePezziPRView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(accettazionePezziPRView.getAccettazionePezziPanelRef().getParent(), accettazionePezziPRView.getAccettazionePezziPanelRef(), webserviceview, new Rectangle(0,70,1000,690));

            logger.info("XteTestToolAccettazionePezziPRController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolAccettazionePezziPRController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAccettazionePezziPRController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller impostaDatiWebService failed", ex);
        }
    }
    
    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolAccettazionePezziPRController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolAccettazionePezziPRController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(accettazionePezziPRView.getParentFrame());

            logger.info("XteTestToolAccettazionePezziPRController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAccettazionePezziPRController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAccettazionePezziPRController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata controller salvaXmlData failed", ex);
        }
    }
}
