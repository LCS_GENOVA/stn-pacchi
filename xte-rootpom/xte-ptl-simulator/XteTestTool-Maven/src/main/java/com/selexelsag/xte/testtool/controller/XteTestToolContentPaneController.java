/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolContentPaneView;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import org.apache.log4j.Logger;
/**
 *
 * @author Da Procida
 */
public class XteTestToolContentPaneController implements XteTestToolController, AncestorListener{

    private XteTestToolContentPaneView  contentPaneView;
    private static Logger logger = Logger.getLogger(XteTestToolContentPaneController.class);

    public XteTestToolContentPaneController(XteTestToolContentPaneView view) {
        try{
            logger.info("XteTestToolContentPaneController start class creation");
            this.contentPaneView  = view;
            this.contentPaneView.initializeView();
            addViewObjectsListeners();
            logger.info("XteTestToolContentPaneController class created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolContentPaneController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolContentPaneController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Content Pane controller creation failed", ex);
        }
    }
    
    @Override
    public void addViewObjectsListeners() {

        this.contentPaneView.addAncestorListener(this);
    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolContentPaneView)
        {
            init();
        }
    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    private void init()
    {
        try{
            logger.info("XteTestToolContentPaneController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.contentPaneView.customizeView();

            logger.info("XteTestToolContentPaneController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolContentPaneController init failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolContentPaneController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Content Pane controller init failed", ex);
        }
    }

}
