/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.common;

import java.awt.Color;
import java.awt.Component;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

/**
 *
 * @author Da Procida
 */
public class ViewComponentStatusManager {

    private static ViewComponentStatusManager componentStatusManager = null;

    private ViewComponentStatusManager() {
    }

    public static ViewComponentStatusManager getInstance() {
        return (componentStatusManager == null) ? componentStatusManager = new ViewComponentStatusManager() : componentStatusManager;
    }

    public void hideComponent(List<Component> componentList) {

        for (Component component : componentList) {
            if (component instanceof JPanel) {
                ((JPanel) component).setVisible(false);
                Component[] panelComps = ((JPanel) component).getComponents();
                if(panelComps != null)
                {
                    for(Component panelComp:panelComps)
                    {
                        if(panelComp.isFocusable())
                            panelComp.setFocusable(false);
                    }
                }
            }
            if (component instanceof JLabel) {
                ((JLabel) component).setVisible(false);
            }
            if (component instanceof JList) {
                if (((JList) component).isFocusable()) {
                    ((JList) component).setFocusable(false);
                }
                ((JList) component).setVisible(false);
            }
            if (component instanceof JTable) {
                if (((JTable) component).isFocusable()) {
                    ((JTable) component).setFocusable(false);
                }
                ((JTable) component).setVisible(false);
            }
            if (component instanceof JTextField) {
                if (((JTextField) component).isFocusable()) {
                    ((JTextField) component).setFocusable(false);
                }
                ((JTextField) component).setVisible(false);
            }
            if (component instanceof JButton) {
                if (((JButton) component).isFocusable()) {
                    ((JButton) component).setFocusable(false);
                }
                ((JButton) component).setVisible(false);
            }
        }
    }

    public void showComponent(List<Component> componentList) {

        for (Component component : componentList) {
            if (component instanceof JPanel) {
                ((JPanel) component).setVisible(true);
            }
            if (component instanceof JLabel) {
                ((JLabel) component).setVisible(true);
            }
            if (component instanceof JList) {
                ((JList) component).setVisible(true);
            }
            if (component instanceof JTable) {
                ((JTable) component).setVisible(true);
            }
            if (component instanceof JTextField) {
                ((JTextField) component).setVisible(true);
                if (((JTextField) component).isEditable()) {
                    ((JTextField) component).setBackground(Color.WHITE);
                } else {
                    ((JTextField) component).setBackground(new Color(240, 240, 240));
                }
            }
            if (component instanceof JButton) {
                ((JButton) component).setVisible(true);
                ((JButton) component).setForeground(new Color(51, 51, 51));
            }
        }
    }

    public void disableComponent(List<Component> componentList) {

        for (Component component : componentList) {
            if (component instanceof JLabel) {
                ((JLabel) component).setEnabled(false);
//                ((JLabel) component).setForeground(new Color(204, 204, 204));
            }
            if (component instanceof JList) {
                ((JList) component).setEnabled(false);
                if (((JList) component).isFocusable()) {
                    ((JList) component).setFocusable(false);
                }
            }
            if (component instanceof JTable) {
                ((JTable) component).setEnabled(false);
                ((JTable) component).getTableHeader().setBackground(new Color(240, 240, 240));
                ((JTable) component).getTableHeader().setForeground(new Color(204, 204, 204));
                if (((JTable) component).isFocusable()) {
                    ((JTable) component).setFocusable(false);
                }
            }
            if (component instanceof JTextField) {
                ((JTextField) component).setEnabled(false);
                ((JTextField) component).setBackground(new Color(200, 200, 200));
                if (((JTextField) component).isFocusable()) {
                    ((JTextField) component).setFocusable(false);
                }
            }
            if (component instanceof JButton) {
                ((JButton) component).setEnabled(false);
                ((JButton) component).setForeground(new Color(204, 204, 204));
                if (((JButton) component).isFocusable()) {
                    ((JButton) component).setFocusable(false);
                }
            }
        }
    }

    public void enableComponent(List<Component> componentList) {

        for (Component component : componentList) {
            if (component instanceof JLabel) {
                ((JLabel) component).setEnabled(true);
//                ((JLabel) component).setForeground(new Color(51, 51, 51));
            }
            if (component instanceof JList) {
                ((JList) component).setEnabled(true);
            }
            if (component instanceof JTable) {
                ((JTable) component).setEnabled(true);
                ((JTable) component).getTableHeader().setBackground(new Color(204, 204, 255));
                ((JTable) component).getTableHeader().setForeground(new Color(90, 40, 0));
            }
            if (component instanceof JTextField) {
                ((JTextField) component).setEnabled(true);
                if (((JTextField) component).hasFocus()) {
                    ((JTextField)component).setBackground(new Color(255,204,102));
                }
                else
                {
                    if (((JTextField) component).isEditable()) {
                        ((JTextField) component).setBackground(Color.WHITE);
                    } else {
                        ((JTextField) component).setBackground(new Color(240, 240, 240));
                    }
                }
            }
            if (component instanceof JButton) {
                ((JButton) component).setEnabled(true);
                ((JButton) component).setForeground(new Color(51, 51, 51));
            }
        }
    }

    public void focusableComponent(List<Component> componentList) {

        for (Component component : componentList) {
            if (component instanceof JList) {
                ((JList) component).setFocusable(true);
            }
            if (component instanceof JTable) {
                ((JTable) component).setFocusable(true);
            }
            if (component instanceof JTextField) {
                ((JTextField) component).setFocusable(true);
            }
            if (component instanceof JButton) {
                ((JButton) component).setFocusable(true);
            }
        }
    }

    public void markComponent(List<Component> componentList) {

        for (Component component : componentList) {
            if (component instanceof JLabel) {
                ((JLabel) component).setForeground(Color.RED);
            }
            if (component instanceof JTextField) {
                ((JTextField) component).setBorder(new LineBorder(Color.RED, 2));
            }
        }
    }

    public void unmarkComponent(List<Component> componentList) {

        for (Component component : componentList) {
            if (component instanceof JLabel) {
                ((JLabel) component).setForeground(new Color(51, 51, 51));
            }
            if (component instanceof JTextField) {
                ((JTextField) component).setBorder(new LineBorder(new Color(102, 102, 102), 1));
            }
        }
    }

    public void clearComponent(List<Component> componentList) {

        for (Component component : componentList) {
            if (component instanceof JTextField) {
                ((JTextField) component).setText("");
            }
            if (component instanceof JTextArea) {
                ((JTextArea) component).setText("");
            }
            if (component instanceof JPasswordField) {
                ((JPasswordField) component).setText("");
            }
        }
    }
}
