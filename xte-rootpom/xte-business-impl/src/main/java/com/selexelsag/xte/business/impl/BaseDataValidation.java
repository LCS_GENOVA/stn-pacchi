package com.selexelsag.xte.business.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.xte.business.exceptions.BusinessTrasformationException;
import com.selexelsag.xte.business.exceptions.BusinessValidationException;
import com.selexelsag.xte.business.tracking.IDataValidation;
import com.selexelsag.xte.business.utility.XmlUtils;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.service.cache.LRUCacheManager;
import com.selexelsag.xte.service.cache.PropertiesCacheManager;

public class BaseDataValidation implements IDataValidation {
	
	private static Logger _log = Logger.getLogger(BaseDataValidation.class);
	
	@Autowired
	private LRUCacheManager validationCache;
	
	@Autowired
	private PropertiesCacheManager propertiesCache;
	
	@Override
	public boolean isValidFormat(FlowParams flowParams, StringBuilder data) throws BusinessValidationException {

		if ((data == null) || (data.length() == 0)){
			throw new IllegalArgumentException("xmlData");
		}

		File fileXsd = null;
		try {
			try{
				String pathFileXsd = flowParams.getRequestValidationSchema();
				_log.debug("path of xsd: "+ pathFileXsd);
				fileXsd =(File)validationCache.getItem(pathFileXsd);
				return XmlUtils.validateXml(data, new FileInputStream(fileXsd));

			}catch(BusinessValidationException ex){

				/* MOTTA if response tranformation fails, try to patch for 
				 * eventually bad xml codification
				 */
				if (_log.isDebugEnabled()) {
					_log.debug("request validation gives error: " + data.toString());
					ByteArrayOutputStream outStream = new ByteArrayOutputStream();
					PrintWriter out = new PrintWriter(outStream);
					ex.printStackTrace(out);
					_log.debug(outStream.toString());
					_log.debug("try to fix it");
				}
				String unescapedRespose = data.toString();

				String xmlHeader = "";
				if (unescapedRespose.startsWith("<?xml")) {
					String closingTag = "?>";
					int xmlTagEnd=unescapedRespose.indexOf(closingTag) + closingTag.length();
					xmlHeader = unescapedRespose.substring(0, xmlTagEnd);
					unescapedRespose = unescapedRespose.substring(xmlTagEnd);
				}
				unescapedRespose = unescapedRespose.replaceAll("((&\\#)(?!([0-9]{1,3})(\\;)))", "&amp;#");
				unescapedRespose = unescapedRespose.replaceAll("(&)(?!([\\Qamp;\\E])|([\\Q#\\E]))", "$1amp;");
				unescapedRespose = unescapedRespose.replaceAll("(<)(?!(([a-zA-Z]{1,30})|(\\/([a-zA-Z]{1,30})\\>)))", "&lt;");
				unescapedRespose = unescapedRespose.replaceAll("(([^\1a-zA-Z][^\1\\Q/\\E]))(\\>)", "$1&gt;");

				unescapedRespose = xmlHeader + unescapedRespose;
				_log.debug("Modified response is: " + unescapedRespose);
				data.delete(0, data.length());
				data.append(unescapedRespose);
				return XmlUtils.validateXml(data, new FileInputStream(fileXsd));
			}
		}catch(IOException ex){

			throw new BusinessValidationException("problem to load file xsd", ex);

		}catch (Exception ex) {

			throw new BusinessValidationException("problem to load path of xsd",ex);

		}


	}

	@Override
	public HashMap<String, String> getSemanticData(FlowParams flowParams, StringBuilder data) throws BusinessValidationException {
		if ((data == null) || (data.length() == 0)){
			throw new BusinessValidationException("data is null");
		}
		
		// XPathFactory factory = XPathFactory.newInstance();

		String dataString = data.toString();

		HashMap<String, String> extractedData = new HashMap<String, String>();
		String propertiesFilePath = flowParams.getRequestValidationProperties();
		
		_log.debug("path of properties file: "+ propertiesFilePath);
		Properties prop = null;
		try {
			prop = propertiesCache.getItem(propertiesFilePath);
		} catch (Exception e) {
			BusinessValidationException bve = new BusinessValidationException("Errore sul recupero dei dati di validazione semantica", e);
			throw bve;
		}

		Enumeration keys = prop.keys();
		while (keys.hasMoreElements()) {
			String key = (String)keys.nextElement();
			String xQuery = prop.getProperty(key);
			_log.debug("Executing " + key + " -> " + xQuery);
			try {
				String extraxtedValue = XmlUtils.extractValue(xQuery, dataString);
				extractedData.put(key, extraxtedValue);
				_log.debug("added parameter " + key + " -> " + extraxtedValue);
			} catch (BusinessTrasformationException e) {
				BusinessValidationException bve = new BusinessValidationException("Errore nella estrazione della chiave " + key, e);
				throw bve;
			}
		}

		return extractedData;
	}

}
