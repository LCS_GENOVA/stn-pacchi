package com.elsagdatamat.dbtrace.dao;

import com.elsagdatamat.dbtrace.entity.PkConServiziAggiuntivi;
import com.elsagdatamat.dbtrace.entity.PkConServiziAggiuntiviPk;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class PkConServiziAggiuntiviDAO extends SpringHibernateJpaDAOBase<PkConServiziAggiuntivi, PkConServiziAggiuntiviPk> 
implements IPkConServiziAggiuntiviDAO{

}
