/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.message.trans;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.MessageEOFException;

/**
 * @author arodriguez
 * 
 */
public class BytesMessageInputStream extends InputStream implements Serializable {

	private BytesMessage message;
	
	/**
	 * 
	 */
	private BytesMessageInputStream() {
		super();
	}

	public BytesMessageInputStream(BytesMessage message) {
		this.message = message;
	}

	@Override
	public boolean markSupported() {
		return false;
	}

	@Override
	public int read() throws IOException {
		try {
			return this.message.readByte();
		} catch (MessageEOFException ex) {
			return -1;
		} catch (JMSException ex) {
			throw new IOException(ex);
		}
	}

	@Override
	public int read(byte[] buf) throws IOException {
		try {
			return this.message.readBytes(buf);
		} catch (JMSException ex) {
			throw new IOException(ex);
		}
	}	
	
	public long getLength() throws JMSException {
		return this.message.getBodyLength();
	}
}
