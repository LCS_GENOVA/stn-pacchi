/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.XteServer;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteServerJDialog;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.xml.Function;
import java.awt.Color;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolHomeController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, ListSelectionListener, PropertyChangeListener, DocumentListener {

    private XteTestToolHomeView homeView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolHomeController.class);

    public XteTestToolHomeController(XteTestToolHomeView homeView) {
        try {
            logger.info("XteTestToolHomeController start class creation");
            this.homeView = homeView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.homeView.initializeView();
            addViewObjectsListeners();
            logger.info("XteTestToolHomeController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolHomeController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolHomeController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Home controller creation failed", ex);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = homeView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        homeView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.homeView.getXteServerListPanelRef().addAncestorListener(this);
        this.homeView.getXteServerListRef().addListSelectionListener(this);
        this.homeView.getOkButtonRef().addActionListener(this);
        this.homeView.getRefreshButtonRef().addActionListener(this);
        this.homeView.getConfigButtonRef().addActionListener(this);
        this.homeView.getExitButtonRef().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(homeView.getOkButtonRef()))
        {
            logger.debug("XteTestToolHomeController actionPerformed invoke loadPostalContextView");
            loadPostalContextView();
        }

        if(source.equals(homeView.getRefreshButtonRef()))
        {
            logger.debug("XteTestToolHomeController actionPerformed invoke refreshXteServerList");
            refreshXteServerList();
        }

        if(source.equals(homeView.getConfigButtonRef()))
        {
            logger.debug("XteTestToolHomeController actionPerformed invoke loadSettingsDialog");
            loadSettingsDialog();
        }

        if(source.equals(homeView.getExitButtonRef()))
        {
            logger.debug("XteTestToolHomeController actionPerformed invoke exit");
            exit();
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolHomeView)
        {
            logger.debug("XteTestToolHomeController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        if(comp instanceof JTextField)
        {
            ((JTextField)comp).setBackground(new Color(255,204,102));
        }
        if(comp instanceof JPasswordField)
        {
            ((JPasswordField)comp).setBackground(new Color(255,204,102));
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        if(comp instanceof JTextField)
        {
            if(((JTextField)comp).isEditable()) {
                ((JTextField)comp).setBackground(Color.WHITE);
            }
            else {
                ((JTextField)comp).setBackground(new Color(220, 220, 220));
            }
        }
        if(comp instanceof JPasswordField)
        {
            if(((JPasswordField)comp).isEditable()) {
                ((JPasswordField)comp).setBackground(Color.WHITE);
            }
            else {
                ((JPasswordField)comp).setBackground(new Color(220, 220, 220));
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
    
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
            
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        JList source = (JList) e.getSource();
        if(source.equals(homeView.getXteServerListRef()))
        {
            if(source.getSelectedIndex()!=-1)
                selectXteServer((XteServer) source.getModel().getElementAt(source.getSelectedIndex()));
            else
                deselectXteServer();
        }
    }

    private void init()
    {
        try{
            logger.info("XteTestToolHomeController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.homeView.customizeView();

            this.homeView.getParentFrame().getContentPaneView().notifyXteServerReleased();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteServer(null);
            dataModel.setCurrentXteTestToolPanelView(homeView);
            updateXteTestToolDataModel(dataModel);

            logger.debug("XteTestToolHomeController init populate XTE Server List");
            JList xteServerListComponent = homeView.getXteServerListRef();
            List<XteServer> xteServerList = getXteTestToolDataModel().getXteServerList();
            if (!xteServerList.isEmpty() && xteServerList != null) {
                ((DefaultListModel)(xteServerListComponent.getModel())).clear();
                for (XteServer xteServer : xteServerList) {
                    ((DefaultListModel)(xteServerListComponent.getModel())).addElement(xteServer);
                }
            }

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(homeView.getOkButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            homeView.getXteServerListRef().requestFocus();

            logger.info("XteTestToolHomeController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolHomeController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolHomeController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Home controller init failed", ex);
        }
    }

    private void selectXteServer(XteServer selectedXteServer)
    {
        try{
            logger.info("XteTestToolHomeController start selectXteServer");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            logger.debug("XteTestToolHomeController selectXteServer Xte Server selected = " + selectedXteServer.getServerBaseURL());

            List<Component> enabledComponents  = new ArrayList<Component>();
            enabledComponents.add(homeView.getOkButtonRef());
            componentStatusManager.enableComponent(enabledComponents);

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteServer(selectedXteServer);
            updateXteTestToolDataModel(dataModel);

            logger.info("XteTestToolHomeController selectXteServer executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolHomeController selectXteServer failed", baseEx);
            throw baseEx;
        } catch (Exception ex)
        {
            logger.error("XteTestToolHomeController selectXteServer failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Home controller selection failed", ex);
        }
    }

    private void deselectXteServer()
    {
        try{
            logger.info("XteTestToolHomeController start deselectXteServer");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(homeView.getOkButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteServer(null);
            updateXteTestToolDataModel(dataModel);

            logger.info("XteTestToolHomeController deselectXteServer executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolHomeController deselectXteServer failed", baseEx);
            throw baseEx;
        } catch (Exception ex)
        {
            logger.error("XteTestToolHomeController deselectXteServer failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Home controller deselection failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolHomeController start exit");
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(homeView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolHomeController exit in progress");
            homeView.exit();
        }
    }

    private void refreshXteServerList()
    {
        logger.info("XteTestToolHomeController start refreshXteServerList");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            logger.debug("XteTestToolHomeController refreshXteServerList populate XTE Server List");
            JList xteServerListComponent = homeView.getXteServerListRef();
            List<XteServer> xteServerList = getXteTestToolDataModel().getXteServerList();
            if (!xteServerList.isEmpty() && xteServerList != null) {
                ((DefaultListModel)(xteServerListComponent.getModel())).clear();
                for (XteServer xteServer : xteServerList) {
                    ((DefaultListModel)(xteServerListComponent.getModel())).addElement(xteServer);
                }
            }

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(homeView.getOkButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            homeView.getXteServerListRef().requestFocus();

            logger.info("XteTestToolHomeController refreshXteServerList executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolHomeController refreshXteServerList failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolHomeController refreshXteServerList failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Home controller refreshXteServerList failed", ex);
        }
    }
    
    private void loadPostalContextView()
    {
        try {
            logger.info("XteTestToolHomeController start loadPostalContextView");
            PanelLoader panelLoader = PanelLoader.getInstance();
            XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(homeView.getParentFrame());
            XteTestToolPostalContextController transazioneInterrogazioneController = new XteTestToolPostalContextController(postalContextView);
            panelLoader.changePanel(homeView.getParent(), homeView, postalContextView, new Rectangle(0,70,1000,690));
            
            StringBuilder xteServerReady = new StringBuilder();
            XteServer selectedXteServer = getXteTestToolDataModel().getCurrentXteServer();
            String urlStr = selectedXteServer.getServerBaseURL();
            URL url = new URL(urlStr);
            
            xteServerReady.append("<html><body>Server: ").append(url.getHost());
            xteServerReady.append("<br>");
            xteServerReady.append("Port: ").append(url.getPort());
            xteServerReady.append("<br>");
            xteServerReady.append("Version: ").append(selectedXteServer.getServerVersion());
            xteServerReady.append("<body><html>");
            
            this.homeView.getParentFrame().getContentPaneView().notifyXteServerReady(xteServerReady.toString());
            
            logger.info("XteTestToolHomeController loadPostalContextView executed successfully");
        } catch (MalformedURLException ex) {
            logger.error("XteTestToolHomeController loadPostalContextView Malformed URL error", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Home controller loadPostalContextView failed", ex);
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolHomeController loadPostalContextView failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Home controller loadPostalContextView failed", ex);
        }
    }

    private void loadSettingsDialog()
    {
        logger.info("XteTestToolHomeController start loadSettingsDialog");
        XteServerJDialog configurationDialog = new XteServerJDialog(homeView.getParentFrame(), true);
        configurationDialog.setLocationRelativeTo(JOptionPane.getRootFrame());
        configurationDialog.setVisible(true);

        XteTestToolConfiguration configuration = XteTestToolConfiguration.getInstance();
        configuration.updateConfiguration();

        List<XteServer> xteServerListReload = new ArrayList<XteServer>();

        String[] xteServers;
        xteServers = configuration.getConfiguration().getStringArray("XteServer.BaseUrl");
        for(int i=0; i < xteServers.length; i++){                       
            //check if string array contains the string
            if(!xteServers[i].isEmpty()){
                XteServer xteServer = new XteServer();
                xteServer.setServerBaseURL(xteServers[i]);        
                xteServer.setServerVersion("4.0.0.0");
                xteServer.setServerDescription("XTE Server " + Integer.toString(i+1));
                xteServer.setActive(true);                
                xteServerListReload.add(xteServer);
            }
        }        

        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setXteServerList(xteServerListReload);            

        List<Function> postalFunctions = configuration.getPostalFunctions();        
        dataModel.setPostalFunctionsList(postalFunctions);
        
        updateXteTestToolDataModel(dataModel);
                        
        refreshXteServerList();
        logger.info("XteTestToolHomeController loadSettingsDialog executed successfully");
        
    }

}
