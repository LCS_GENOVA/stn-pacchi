package com.elsagdatamat.trackdb.toedwh.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.framework.dao.hibernate.HibernatePrimitiveDAO;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.trackdb.toedwh.entities.TraceDetail;


public class TraceDetailDAO extends SpringHibernateJpaDAOBase<TraceDetail, Long> implements ITraceDetailDAO {
	@Override
	public List<TraceDetail> executeQuery(String hqlQuery) {
		class MyJpaCallback<TraceDetail> implements JpaCallback {
			@SuppressWarnings("hiding")
			String hqlQuery;

			public MyJpaCallback(String hqlQuery){
				this.hqlQuery = hqlQuery;
			}

			@SuppressWarnings("unchecked")
			@Override
			@Transactional(propagation=Propagation.SUPPORTS)
			public List<TraceDetail> doInJpa(EntityManager em) throws PersistenceException {
				return (List<TraceDetail>)HibernatePrimitiveDAO._executeHqlQuery(
															((HibernateEntityManager) em).getSession(),
															hqlQuery
															);
			}
		}

		MyJpaCallback<TraceDetail> myJpaCallbak = new MyJpaCallback<TraceDetail>(hqlQuery);
		List<TraceDetail> listOfTObject = (List<TraceDetail>) getJpaTemplate().execute(myJpaCallbak,true);

		return listOfTObject;
	}

}
