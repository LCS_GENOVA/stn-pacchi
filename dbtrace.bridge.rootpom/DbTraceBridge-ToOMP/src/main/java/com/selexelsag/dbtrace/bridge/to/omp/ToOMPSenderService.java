/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.system.ServiceMBeanSupport;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.selexelsag.dbtrace.bridge.to.omp.manager.OMPManager;
import com.selexelsag.dbtrace.bridge.to.omp.manager.OMPManagerInterface;
import com.selexelsag.dbtrace.bridge.to.omp.message.OMPMessageBuilder;
import com.selexelsag.dbtrace.bridge.to.omp.message.OMPMessageBuilderInterface;

/**
 * @author arodriguez
 * 
 */
public class ToOMPSenderService extends ServiceMBeanSupport implements ToOMPSenderServiceMBean {

	private static final Log log = LogFactory.getLog(ToOMPSenderService.class);

	private boolean created;
	private boolean started;
	private String configPath;
	private String springConfigPath;
	private long errorCount = 0;
	private Date lastStartDate = new Date();

	private ClassPathXmlApplicationContext springContext;
	private OMPManagerInterface ompManager;
	private OMPMessageBuilderInterface ompMBuilder;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss");

	/**
	 * init operations at server startup
	 */
	private void serviceStartInits() {
		lastStartDate = new Date();
		ompManager = bindOMPManager();
		ompMBuilder = bindOMPMessageBuilder();
		if (ompMBuilder != null) {
			ompMBuilder.setNumberOfRuns(new long[]{0,0,0});
			ompMBuilder.loadQueryParams();
		}
		if (ompManager != null)
			log.info("Check DID code generator: " + (ompManager.getNextSequenceCode().compareTo(-1L) == 0 ? "KO" : "OK"));
	}

	/**
	 * start SpringContext
	 * @throws Exception
	 */
	private void createSpringContext() throws Exception {
		log.info("Starting Spring context. . .");
		springContext = new ClassPathXmlApplicationContext(springConfigPath);
		springContext.registerShutdownHook();
	}

	/*
	 * (non-Javadoc)
	 * @see org.jboss.system.ServiceMBeanSupport#create()
	 */
	@Override
	public void create() throws Exception {
		log.info("Create service invoked. . .");
		if (!created) {
			try {
				// TODO: put some inits here or remove try/catch
				created = true;
			} catch (Exception ex) {
				errorCount++;
				created = false;
				log.error("Unable to start Spring Context", ex);
			}
		}
		log.info("Service created.");
	}

	/*
	 * (non-Javadoc)
	 * @see org.jboss.system.ServiceMBeanSupport#destroy()
	 */
	@Override
	public void destroy() {
		log.info("Destroy service invoked; stopping Spring context. . .");
		if (created) {
			try {
				stop();
				if (springContext.isActive()) {
					springContext.close();
					springContext.destroy();
					springContext = null;
				}
				created = false;
			} catch (Throwable ex) {
				log.error(" Failed to destroy service. ", ex);
			}
		}
		log.info("Service destroyed.");
	}

	/*
	 * (non-Javadoc)
	 * @see org.jboss.system.ServiceMBeanSupport#start()
	 */
	@Override
	public void start() throws Exception {
		if (!started) {
			if (created) // added because of service dependencies
				createSpringContext();
			log.info("Starting Spring ...");
			try {
				if (!springContext.isActive()) {
					springContext.refresh();
					springContext.start();
				}
				started = true;
				serviceStartInits();
				log.info("Spring context started. OK");
			} catch (Throwable ex) {
				errorCount++;
				started = false;
				log.error("Failed to start Spring context ", ex);
			}
			return;
		}
		log.info("Spring already started");
	}

	/*
	 * (non-Javadoc)
	 * @see org.jboss.system.ServiceMBeanSupport#stop()
	 */
	@Override
	public void stop() {
		if (started) {
			log.info("Stopping Spring ...");
			try {
				springContext.close();
				started = false;
				log.info("Spring context stopped. OK");
			} catch (Throwable ex) {
				errorCount++;
				started = false;
				log.error("Failed to stop Spring context ", ex);
			}
			return;
		}
		log.info("Spring already stopped");
	}

	private OMPManagerInterface bindOMPManager() {
		if (springContext.containsBean(OMPManagerInterface.DEFAULT_NAME)) {
			return (OMPManagerInterface)springContext.getBean(OMPManagerInterface.DEFAULT_NAME);
		} 
		return springContext.getBean(OMPManager.class); // not found, try with concrete class
	}

	private OMPMessageBuilderInterface bindOMPMessageBuilder() {
		if (springContext.containsBean(OMPMessageBuilderInterface.DEFAULT_NAME)) {
			return (OMPMessageBuilderInterface)springContext.getBean(OMPMessageBuilderInterface.DEFAULT_NAME);
		} 
		return springContext.getBean(OMPMessageBuilder.class); // not found, try with concrete class
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.ToOMPSenderServiceMBean#reloadConfiguration()
	 */
	public void reloadConfiguration() {
		if (ompMBuilder != null) 
			ompMBuilder.loadQueryParams();
	}

	/* (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.ToOMPSenderServiceMBean#setConfigPath(java.lang.String)
	 */
	@Override
	public void setConfigPath(String configPath) {
		this.configPath = configPath;
	}

	/**
	 * @return the configPath
	 */
	public String getConfigPath() {
		return configPath;
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.ToOMPSenderServiceMBean#setMinutesToRecovery(java.lang.Integer)
	 */
	@Override
	public void setMinutesToRecovery(Integer minValue) {
		if (ompManager != null) 
			ompManager.setMinutesToRecoveryMinValue(minValue);
	}

	@Override
	public Integer getMinutesToRecovery() {
		if (ompManager != null) 
			return ompManager.getMinutesToRecoveryMinValue();
		return -1;
	}
	
	/* (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.ToOMPSenderServiceMBean#getNumberOfRuns()
	 */
	@Override
	public String getNumberOfRuns() {
		String result = "<counters not accessible>";
		if (ompMBuilder != null) {
			result = "";
			for (long nr : ompMBuilder.getNumberOfRuns())
				result += nr + " "; 
		}
		return result; 
	}

	/* (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.ToOMPSenderServiceMBean#sendTestMessage()
	 */
	@Override
	public void sendTestMessage() {
		if (ompMBuilder != null) 
			ompMBuilder.sendTestMessage();
	}
	
	/* (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.ToOMPSenderServiceMBean#getLastStartDate()
	 */
	@Override
	public String getLastStartDate() {
		return sdf.format(lastStartDate);
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.ToOMPSenderServiceMBean#setMinutesToRestart(java.lang.Integer)
	 */
	@Override
	public void setMinutesToRestart(Integer minValue) {
		if (ompManager != null) 
			ompManager.setMinutesToRestartMinValue(minValue);
	}

	@Override
	public Integer getMinutesToRestart() {
		if (ompManager != null) 
			return ompManager.getMinutesToRestartMinValue();
		return -1;
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.ToOMPSenderServiceMBean#setSpringConfigPath(java.lang.String)
	 */
	@Override
	public void setSpringConfigPath(String path) {
		springConfigPath = path;
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.ToOMPSenderServiceMBean#getSpringConfigPath()
	 */
	@Override
	public String getSpringConfigPath() {
		return springConfigPath;
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.ToOMPSenderServiceMBean#isSpringLoaded()
	 */
	@Override
	public boolean isSpringLoaded() {
		return (springContext != null) ? springContext.isActive() : false;
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.ToOMPSenderServiceMBean#isStarted()
	 */
	@Override
	public boolean isStarted() {
		return started;
	}

	/*
	 * (non-Javadoc)
	 * @see com.selexelsag.dbtrace.bridge.to.omp.ToOMPSenderServiceMBean#getErrorCount()
	 */
	@Override
	public long getErrorCount() {
		return errorCount;
	}

}
