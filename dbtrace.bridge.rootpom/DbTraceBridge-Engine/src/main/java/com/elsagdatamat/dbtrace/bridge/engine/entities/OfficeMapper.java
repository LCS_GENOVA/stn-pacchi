package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="OFFICE_MAPPER")
@IdClass(OfficeMapperPK.class)
public class OfficeMapper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8254444034202895870L;

	public static final String FRAZIONARIO_ESTERNO = "frazionarioEsterno";
	public static final String FRAZIONARIO_INTERNO = "frazionarioInterno";
	public static final String FAMILY = "family";

	private String frazionarioEsterno; 
	private String frazionarioInterno;
	private String family;
	
	@Id
	@Column(name="FRAZIONARIO_EXT")
	public String getFrazionarioEsterno() {
		return frazionarioEsterno;
	}
	public void setFrazionarioEsterno(String frazionarioEsterno) {
		this.frazionarioEsterno = frazionarioEsterno;
	}
	
	@Column(name="FRAZIONARIO_PI")
	public String getFrazionarioInterno() {
		return frazionarioInterno;
	}
	public void setFrazionarioInterno(String frazionarioInterno) {
		this.frazionarioInterno = frazionarioInterno;
	}
	
	@Id
	@Column(name="FAMILY")
	public String getFamily() {
		return family;
	}
	public void setFamily(String family) {
		this.family = family;
	}
}
