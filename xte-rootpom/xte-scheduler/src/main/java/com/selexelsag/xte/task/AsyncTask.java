package com.selexelsag.xte.task;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

public class AsyncTask extends SpringBeanAutowiringSupport implements IRunOnce {
	
private static Logger logger = Logger.getLogger(AsyncTask.class);
	
	@Autowired
	private JmsTask jmsTask;
	

	public void setJmsTask(JmsTask jmsTask) {
		this.jmsTask = jmsTask;
	}

	@Override
	public void runOnce() {
		try {
			jmsTask.processAll();
		} catch (Exception e) {
			logger.error("Error processAll(): " + e.getMessage(), e);
		}
	}
}
