package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;

import javax.persistence.Column;

public class CodiciProdottoPICodiciProdottoSDAPK implements Serializable {

	private static final long serialVersionUID = 8501772881250851826L;

	public static final String PI_CODE = "piCode";
	public static final String SDA_CODE = "sdaCode";

	private String piCode;
	private String sdaCode;
	private String codeFormatRegex;

	@Column(name="PI_CODE")
	public String getPiCode() {
		return piCode;
	}
	public void setPiCode(String piCode) {
		this.piCode = piCode;
	}

	@Column(name="SDA_CODE")
	public String getSdaCode() {
		return sdaCode;
	}
	public void setSdaCode(String sdaCode) {
		this.sdaCode = sdaCode;
	}
	
	@Column(name="CODE_FORMAT_REGEX")
	public String getCodeFormatRegex() {
		return codeFormatRegex;
	}
	
	public void setCodeFormatRegex(String codeFormatRegex) {
		this.codeFormatRegex = codeFormatRegex;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codeFormatRegex == null) ? 0 : codeFormatRegex.hashCode());
		result = prime * result + ((piCode == null) ? 0 : piCode.hashCode());
		result = prime * result + ((sdaCode == null) ? 0 : sdaCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodiciProdottoPICodiciProdottoSDAPK other = (CodiciProdottoPICodiciProdottoSDAPK) obj;
		if (codeFormatRegex == null) {
			if (other.codeFormatRegex != null)
				return false;
		} else if (!codeFormatRegex.equals(other.codeFormatRegex))
			return false;
		if (piCode == null) {
			if (other.piCode != null)
				return false;
		} else if (!piCode.equals(other.piCode))
			return false;
		if (sdaCode == null) {
			if (other.sdaCode != null)
				return false;
		} else if (!sdaCode.equals(other.sdaCode))
			return false;
		return true;
	}
	
	

}
