package com.selexes.tt.tool.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

public class TimeUtils {
	
	static public Calendar resetTime(Calendar cal) {
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.AM_PM, Calendar.AM);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}
	
	static public Date parseDate(String date) {
		if(date==null) return null;
		if(date.length()!=5 && date.length()!=10) return null;
		
		StringTokenizer tokenizerDate = new StringTokenizer(date,"-");
		if (tokenizerDate.countTokens()<2) return null;
		
		int day   = Integer.parseInt(tokenizerDate.nextToken());
		int month = (Integer.parseInt(tokenizerDate.nextToken()))-1;
		int year  = (Integer.parseInt(tokenizerDate.nextToken()));
		
		Calendar c=Calendar.getInstance();
		c.set(year, month, day);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.AM_PM, Calendar.AM);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return c.getTime();
	}
}
