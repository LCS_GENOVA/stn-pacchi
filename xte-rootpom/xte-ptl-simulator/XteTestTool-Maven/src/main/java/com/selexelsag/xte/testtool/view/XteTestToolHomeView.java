/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.common.XteTestToolFocusTraversalPolicy;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import com.selexelsag.xte.testtool.view.objectmodel.XteListSelectionModel;
import com.selexelsag.xte.testtool.view.objectrenderer.XteServerListRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolHomeView extends XteTestToolPanelView{
    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolHomeView.class);
    private DefaultListModel xteServerListModel = new DefaultListModel();
    
    public XteTestToolHomeView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try {
            logger.info("XteTestToolHomeView start class creation");
            this.messages = ResourceBundle.getBundle("bundles/messages");
            initComponents();
            logger.info("XteTestToolHomeView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolHomeView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Xte Test Tool Home view creation failed", ex);
        }
    }

    public JPanel getXteServerListPanelRef(){ return this; }
    public JLabel getXteServerListTitleLabelRef(){ return this.panelTitlejLabel; }
    public JList getXteServerListRef(){ return this.xteServerjList; }
    public JButton getOkButtonRef(){ return this.okjButton; }
    public JButton getRefreshButtonRef(){ return this.refreshjButton; }
    public JButton getConfigButtonRef(){ return this.configjButton; }
    public JButton getExitButtonRef(){ return this.exitjButton; }
    public JTextPane getMessageTextPaneRef(){ return this.messageBarjTextPane; }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolHomeView start customizeView");

            this.panelTitlejLabel .setText(messages.getString("home.view.title.label"));
            this.okjButton.setText(messages.getString("view.ok.button"));
            this.refreshjButton.setText(messages.getString("view.refresh.button"));
            this.configjButton.setText(messages.getString("home.view.config.button"));
            this.exitjButton.setText(messages.getString("view.exit.button"));

            logger.info("XteTestToolHomeView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolHomeView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Xte Test Tool Home List view customization failed", ex);
        }
    }

    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolHomeView start initializeView");

            //buildComponentSortedMap();

            //this.setFocusCycleRoot(true);
            //this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

            JLabel cornelLabel = new JLabel("");
            cornelLabel.setOpaque(true);
            cornelLabel.setBackground(new Color(240, 240, 240));

            this.xteServerjScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.xteServerjScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.xteServerjScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.xteServerjScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.xteServerjScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            xteServerjList.setSelectionModel(new XteListSelectionModel());
            xteServerjList.setModel(xteServerListModel);
            xteServerjList.setCellRenderer(new XteServerListRenderer());

            logger.info("XteTestToolHomeView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolHomeView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Xte Test Tool Home List view initialization failed", ex);
        }
    }

    @Override
    public void restoreView() {
        logger.debug("XteTestToolHomeView restoreView");
        this.getExitButtonRef().doClick();
    }

    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.xteServerjList);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        commandjPanel = new javax.swing.JPanel();
        okjButton = new javax.swing.JButton();
        messageBarjScrollPane = new javax.swing.JScrollPane();
        messageBarjTextPane = new javax.swing.JTextPane();
        configjButton = new javax.swing.JButton();
        exitjButton = new javax.swing.JButton();
        refreshjButton = new javax.swing.JButton();
        workingjPanel = new javax.swing.JPanel();
        panelTitlejLabel = new javax.swing.JLabel();
        xteServerjScrollPane = new javax.swing.JScrollPane();
        xteServerjList = new javax.swing.JList();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setMaximumSize(new java.awt.Dimension(1000, 690));
        setMinimumSize(new java.awt.Dimension(1000, 690));
        setPreferredSize(new java.awt.Dimension(1000, 690));

        commandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        commandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        commandjPanel.setMaximumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setMinimumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setPreferredSize(new java.awt.Dimension(980, 80));

        okjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        okjButton.setForeground(new java.awt.Color(51, 51, 51));
        okjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/OK.png"))); // NOI18N
        okjButton.setText("OK");
        okjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        okjButton.setFocusPainted(false);
        okjButton.setFocusable(false);
        okjButton.setIconTextGap(5);
        okjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        okjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        okjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        messageBarjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        messageBarjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageBarjScrollPane.setMaximumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setMinimumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setPreferredSize(new java.awt.Dimension(350, 50));

        messageBarjTextPane.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        messageBarjTextPane.setFocusable(false);
        messageBarjScrollPane.setViewportView(messageBarjTextPane);

        configjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        configjButton.setForeground(new java.awt.Color(51, 51, 51));
        configjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Config4.png"))); // NOI18N
        configjButton.setText("<html><body>Settings<body><html>");
        configjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        configjButton.setFocusPainted(false);
        configjButton.setFocusable(false);
        configjButton.setIconTextGap(5);
        configjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        configjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        configjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("<html><body>&nbsp;Exit<body><html>");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setFocusPainted(false);
        exitjButton.setFocusable(false);
        exitjButton.setIconTextGap(5);
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        refreshjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        refreshjButton.setForeground(new java.awt.Color(51, 51, 51));
        refreshjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Ripeti1.png"))); // NOI18N
        refreshjButton.setText("<html><body>&nbsp;Refresh<body><html>");
        refreshjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        refreshjButton.setFocusPainted(false);
        refreshjButton.setFocusable(false);
        refreshjButton.setIconTextGap(5);
        refreshjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        refreshjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        refreshjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout commandjPanelLayout = new javax.swing.GroupLayout(commandjPanel);
        commandjPanel.setLayout(commandjPanelLayout);
        commandjPanelLayout.setHorizontalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(okjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(refreshjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(configjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 162, Short.MAX_VALUE)
                .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandjPanelLayout.setVerticalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(okjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(refreshjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(configjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        workingjPanel.setBackground(new java.awt.Color(230, 250, 0));
        workingjPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20)));
        workingjPanel.setMaximumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setMinimumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setPreferredSize(new java.awt.Dimension(980, 580));

        panelTitlejLabel.setBackground(new java.awt.Color(240, 240, 240));
        panelTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        panelTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        panelTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelTitlejLabel.setText("SERVER XTE");
        panelTitlejLabel.setFocusable(false);
        panelTitlejLabel.setIconTextGap(30);
        panelTitlejLabel.setMaximumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setMinimumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setPreferredSize(new java.awt.Dimension(920, 20));

        xteServerjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        xteServerjScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        xteServerjScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xteServerjScrollPane.setMaximumSize(new java.awt.Dimension(700, 450));
        xteServerjScrollPane.setMinimumSize(new java.awt.Dimension(700, 450));
        xteServerjScrollPane.setPreferredSize(new java.awt.Dimension(700, 450));

        xteServerjList.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xteServerjList.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        xteServerjList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "QA02", "QA12" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        xteServerjList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        xteServerjScrollPane.setViewportView(xteServerjList);

        javax.swing.GroupLayout workingjPanelLayout = new javax.swing.GroupLayout(workingjPanel);
        workingjPanel.setLayout(workingjPanelLayout);
        workingjPanelLayout.setHorizontalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workingjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(8, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(xteServerjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(113, 113, 113))
        );
        workingjPanelLayout.setVerticalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(xteServerjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(39, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel commandjPanel;
    private javax.swing.JButton configjButton;
    private javax.swing.JButton exitjButton;
    private javax.swing.JScrollPane messageBarjScrollPane;
    private javax.swing.JTextPane messageBarjTextPane;
    private javax.swing.JButton okjButton;
    private javax.swing.JLabel panelTitlejLabel;
    private javax.swing.JButton refreshjButton;
    private javax.swing.JPanel workingjPanel;
    private javax.swing.JList xteServerjList;
    private javax.swing.JScrollPane xteServerjScrollPane;
    // End of variables declaration//GEN-END:variables


}
