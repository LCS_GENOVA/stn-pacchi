package com.elsagdatamat.dbtrace.bridge.engine.ws;

import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;

public interface ITrackDBRWServiceManager {

	public abstract TrackDBRWServicePortType getServicePort();

}