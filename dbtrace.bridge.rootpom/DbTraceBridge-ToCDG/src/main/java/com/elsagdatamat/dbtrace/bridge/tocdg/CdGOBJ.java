package com.elsagdatamat.dbtrace.bridge.tocdg;

import javax.xml.bind.annotation.XmlElement;

public class CdGOBJ {
	
	
/*
OBJID
PH
PSTF
OP
TDT
SUBC
TESTF


BTSF
DF
DELREASON
ADDR
DADDR
ZIP
DEST
 */
	//comune
	@XmlElement(name = "OBJID")
	public String objid;

	//comune
	@XmlElement(name = "PH")
	public String ph;
	
	//comune
	@XmlElement(name = "PSTF")
	public String pstf;
	
	//comune
	@XmlElement(name = "OP")
	public String op;
	
	//comune
	@XmlElement(name = "TDT")
	public String tdt;
	
	//comune
	@XmlElement(name = "SUBC")
	public String subc;
	
	//comune
	@XmlElement(name = "TESTF")
	public String testf;
	

	//tipici di M2
	@XmlElement(name = "BTSF")
	public String btsf;
	
	@XmlElement(name = "DF")
	public String df;
	
	@XmlElement(name = "DELREASON")
	public String delreason;
	
	@XmlElement(name = "ADDR")
	public String addr;
	
	@XmlElement(name = "DADDR")
	public String daddr;
	
	@XmlElement(name = "ZIP")
	public String zip;
	
	@XmlElement(name = "DEST")
	public String dest;

	

	public CdGOBJ()
	{
	}
}