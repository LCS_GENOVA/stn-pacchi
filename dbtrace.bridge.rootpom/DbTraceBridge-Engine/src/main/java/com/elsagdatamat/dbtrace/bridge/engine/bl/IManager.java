package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.elsagdatamat.framework.search.Search;
import com.elsagdatamat.framework.utils.Pair;


public interface IManager<T, K extends Serializable>{

	
	public Class<T> getManagedClass();
	
	public List<T> findByProperties(Collection<Pair<String, Object>> properties);
	
	public Collection<T> find(Search search);
	
	public T findByID(K objectID);
	
	public void delete(T entity);
	
	public int deleteBatch(Search condition);
	
	public void update(T entity);
	
	public int updateBatch(Collection<Pair<String, Object>> setProperties, Collection<Pair<String, Object>> conditionProperties);
	
	public int updateBatch(Collection<Pair<String, Object>> setProperties, Search condition);
	
	public int updateBatch(Collection<Pair<String, Object>> newProperties,
			Search condition, Integer maxObjectAffected);
	
	public void insert(T entity);
	
	public Object findMin(Class<T> clazz, String minPropertyName, Collection<Pair<String, Object>> properties);
	
	public Object findMin(Class<T> clazz, String minPropertyName, Collection<Pair<String, Object>> properties, boolean lock);
	
	public Object findMax(Class<T> clazz, String maxPropertyName, Collection<Pair<String, Object>> properties);
	
	public Object findMax(Class<T> clazz, String maxPropertyName, Collection<Pair<String, Object>> properties, boolean lock) ;
	
	public int getCount(Search search);
	
	public void refresh(T object);
}
