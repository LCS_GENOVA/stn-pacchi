package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.elsagdatamat.dbtrace.bridge.engine.entities.TagToConvertSubP;

public class TagToConvertSubPManager extends BaseManager<TagToConvertSubP, String> implements ITagToConvertSubPManager {
private Map<String,String> conversionMap =null;
	
	@Override
	public synchronized Map<String,String> getAllMapping()
	{
		if (conversionMap == null)
		{
			List<TagToConvertSubP> list =
			  this.generalDao.findAll(TagToConvertSubP.class);
			
			conversionMap = new HashMap<String, String>();
			for(TagToConvertSubP r : list)
			{
				conversionMap.put(r.getTipoTraccia(), r.getTagProdotto());
			}
		}
		return conversionMap;
	}
	
}
