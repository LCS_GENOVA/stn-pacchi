package com.selexelsag.dbtrace.bridge.to.ssip.manager;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author arodriguez
 * 
 */

@Transactional(value = "dbTraceBridge-transactionManager", propagation = Propagation.REQUIRED)
public interface SSIPManagerInterface {

	public static final String DEFAULT_NAME = "ssipTokenManager";
	
	public void setMinutesToRecoveryMinValue(Integer minutesToRecoveryMinValue);
		
	public void setMinutesToRestartMinValue(Integer minutesToRestartMinValue);

	public Integer getMinutesToRecoveryMinValue();
	
	public Integer getMinutesToRestartMinValue();
		
	public String getIdStatusTranscoded(String keyCode, String keyChannel);
}
