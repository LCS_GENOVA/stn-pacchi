package com.elsagdatamat.tt.trackdbws.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ENVIRONMENT_PARAMETERS")
public class EnvironmentParameter implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PARAM_NAME", nullable=false)
	private String paramName;

	@Column(name="PARAM_VALUE", nullable=false)
	private String paramValue;
	
	@Transient
	@Column(name="PARAM_DESC", nullable=false)
	private String paramDesc;

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getParamDesc() {
		return paramDesc;
	}

	public void setParamDesc(String paramDesc) {
		this.paramDesc = paramDesc;
	}
	
}
