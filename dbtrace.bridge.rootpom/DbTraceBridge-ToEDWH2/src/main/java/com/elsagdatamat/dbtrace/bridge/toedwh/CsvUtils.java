package com.elsagdatamat.dbtrace.bridge.toedwh;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import com.elsagdatamat.trackdb.toedwh.vo.TraceDetailVO;
import com.elsagdatamat.trackdb.toedwh.vo.TraceVO;


public class CsvUtils {

    private static final StringBuffer SEPARATOR =  new StringBuffer(",");
    private static final StringBuffer EMPTY = new StringBuffer();
    private static StringBuffer toCsvValue(Object o){
    	return toCsvValue(o, false);
    }

    /**
     * Sostituisce il carattere separatore con uno spazio vuoto
     * @param o
     * @param last
     * @return
     */
    private static StringBuffer toCsvValue(Object o, boolean last){
    	StringBuffer s=new StringBuffer();
		if(o==null)
		    s.append(EMPTY);
		else{
			// escape for character ","
			String escaped = o.toString();
			if(escaped.contains(SEPARATOR.toString())){
				escaped = escaped.replaceAll(SEPARATOR.toString(), " ");
			}

			s.append(escaped);
		}
		if(!last){
			s.append(SEPARATOR);
		}
		return s;

    }

    public static StringBuffer toCsv(TraceVO t){
		StringBuffer s=new StringBuffer();
		s.append(toCsvValue(t.getTracedEntity()));
		s.append(toCsvValue(t.getIdTracedEntity()));
		s.append(toCsvValue(t.getWhenHappened()));
		s.append(toCsvValue(t.getWhenRegistered()));
		s.append(toCsvValue(t.getWhereHappened()));
		s.append(toCsvValue(t.getChannel()));
		s.append(toCsvValue(t.getIdChannel()));
		s.append(toCsvValue(t.getWhatHappened()));
		s.append(toCsvValue(t.getServiceName()));
		s.append(toCsvValue(t.getLabelTracedEntity()));
		s.append(toCsvValue(t.getIdStatus()));

		s.append(toCsvValue(t.getIdTracedExternal()));
		s.append(toCsvValue(t.getIdForwardTo()));
		if(t.getDetailsVO()!=null && t.getDetailsVO().size()>0){
			s.append(toCsvValue(t.getSysForwardTo()));

			Iterator<TraceDetailVO> iter = t.getDetailsVO().iterator();
			for(int i=0;i<t.getDetailsVO().size();i++){
				TraceDetailVO td = iter.next();
			    addDetailToCsv(td,s, i==t.getDetailsVO().size()-1);
			}
		}
		else{
			s.append(toCsvValue(t.getSysForwardTo(), false));
		}
		return s;
    }
    private static void addDetailToCsv(TraceDetailVO td, StringBuffer traceCsv, boolean isLast){
		traceCsv.append(toCsvValue(td.getParamClass()));
		traceCsv.append(toCsvValue(td.getParamValue(), isLast));
    }


	public static void main(String[] args) throws Exception {
		System.out.print("\n start \n");

		TraceVO t = new TraceVO();
		t.setTracedEntity("dsfsdm");
		t.setIdTracedEntity("dsfcasd");
		t.setWhenHappened(new Date());
		t.setWhenRegistered(new Date());
		t.setWhereHappened("dfsf");
		t.setChannel("dss");
		t.setIdChannel("asdfas");
		t.setWhatHappened(" dsaf , dsaf");
		t.setServiceName("asdf  fsda");
		t.setLabelTracedEntity("fasd  \"sa");
		t.setIdStatus("asf  sfa");

		t.setIdTracedExternal("a fsd");
		t.setIdForwardTo("asdf ");
		TraceDetailVO td = new TraceDetailVO();
		td.setParamClass("as df");
		td.setParamValue("a sf");

		ArrayList<TraceDetailVO> arr = new ArrayList<TraceDetailVO>();
		arr.add(td);
		t.setDetailsVO(arr);
		StringBuffer sb = toCsv(t);

		System.out.print("\n sb"+ sb);

		System.exit(0);
	}

}
