/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.test.message;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.xml.bind.JAXBException;

import junit.framework.Assert;

import org.junit.Test;

import com.selexelsag.dbtrace.bridge.to.omp.manager.OMPManager;
import com.selexelsag.dbtrace.bridge.to.omp.manager.OMPManagerInterface;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG;
import com.selexelsag.dbtrace.bridge.to.omp.message.trans.D1PTransformationValues;
import com.selexelsag.dbtrace.bridge.to.omp.message.trans.Transformations;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetailsList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

/**
 * @author arodriguez
 *
 */
public class TransformationsTest {

	@Test
	public void transformationsTracesListToMessageBodyCreateValidMSG() {
		
		MockedOMPManager ompManager = new MockedOMPManager();		
		Transformations transformation = new Transformations(ompManager);
		
		TracesList tracesList = new TracesList();
		List<Trace> traces = new ArrayList<Trace>();
		
		for (int i=0; i < 10; i++) {
			Trace trace = new Trace();
			trace.setChannel("OMP");
			trace.setIdChannel("ID" + i);
			trace.setIdForwardTo("11111" + i);
			trace.setIdTracedEntity("00100");
			trace.setIdStatus("001");
			trace.setTracedEntity("PACCO");
			trace.setServiceName("PACCHI");
			trace.setLabelTracedEntity("LABEL" + i);
			trace.setIdCorrelazione("" + i);
			
			TraceDetailsList details = new TraceDetailsList();

			int j = 0;
			for (D1PTransformationValues key : D1PTransformationValues.values()) {
			    TraceDetail detail = new TraceDetail();
			    
				detail.setParamClass(key.toString());
				detail.setParamValue("test" + j);
				details.getTraceDetails().add(detail);
				j++;
			}
			trace.setTraceDetailsList(details);
			tracesList.getTraces().add(trace);
		}
		
		MSG d1pMessage = transformation.traceListToMessage("1001", tracesList.getTraces());
		Assert.assertNotNull(d1pMessage);
		
		try {
			String xmlMessage = transformation.marshallMessageToXml(d1pMessage, null);
			Assert.assertNotNull(xmlMessage);
			
			System.out.println(xmlMessage);
			
			MSG d1pUnmarshalled = transformation.unmarshallXmlToMessage(xmlMessage, null);
			Assert.assertNotNull(d1pUnmarshalled);
			String xmlUnmarshalled = transformation.marshallMessageToXml(d1pUnmarshalled, null);
			Assert.assertEquals(xmlMessage, xmlUnmarshalled);
		} catch (JAXBException e) {
			Assert.fail(e.getMessage());
		}
	}

	class MockedOMPManager extends OMPManager implements OMPManagerInterface {

		private Random rnd = new Random(System.currentTimeMillis());
		
		/* (non-Javadoc)
		 * @see com.selexelsag.dbtrace.bridge.to.omp.manager.OMPManager#getNextSequenceCode()
		 */
		@Override
		public Long getNextSequenceCode() {
			return rnd.nextLong() + 1;
		}		
		
	}
}
