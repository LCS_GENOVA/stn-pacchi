package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="REMOVED_TRACE")
public class RemovedTrace implements Serializable {

	private static final long serialVersionUID = 8909281602496909398L;

	public static final String ID = "id";
	public static final String TRACED = "traced";
	public static final String REASON = "reason";
	public static final String ID_DISCARDED_TRACE = "idDiscardedTrace";
	public static final String DISCARDING_PROCESS = "discardingProcess";

	private long id;
	private String discardingProcess;
	private long idDiscardedTrace;
	private String traced; 
	private String reason;
	public RemovedTrace(){
	}
			
	public RemovedTrace(String traced, String reason, String discardingProcess) {
		super();
		this.traced = traced;
		this.reason = reason;
		this.discardingProcess = discardingProcess;
	}
	
	@Id
	@SequenceGenerator(name="REMOVED_TRACE_SEQ",sequenceName="REMOVED_TRACE_SEQ", allocationSize=1, initialValue=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="REMOVED_TRACE_SEQ")
    @Column(name = "ID", nullable = false)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Lob
	@Column(name="TRACED")
	public String getTraced() {
		return traced;
	}
	public void setTraced(String traced) {
		this.traced = traced;
	}
	
	@Column(name="DISCARDING_PROCESS", length=50)
	public String getDiscardingProcess() {
		return discardingProcess;
	}
	public void setDiscardingProcess(String discardingProcess) {
		this.discardingProcess = discardingProcess;
	}

	@Lob
	@Column(name="REASON")
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name="ID_DISCARDED_TRACE")
	public long getIdDiscardedTrace() {
		return idDiscardedTrace;
	}
	public void setIdDiscardedTrace(long idDiscardedTrace) {
		this.idDiscardedTrace = idDiscardedTrace;
	} 
}
