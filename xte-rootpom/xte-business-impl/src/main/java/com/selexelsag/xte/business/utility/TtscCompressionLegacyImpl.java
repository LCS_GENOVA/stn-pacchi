package com.selexelsag.xte.business.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.lang3.StringUtils;

public class TtscCompressionLegacyImpl {
	
	public String compressionType;
	public String encoding;
	
	
	public byte[] compress(String uncompressedString) throws IOException {
		return compress(uncompressedString, compressionType);
	}
	
	
	public byte[] compress(String uncompressedString, String compressionType)
			throws IOException {
		String header = generateBodyMessageHeader();
		return legacyCompress(uncompressedString, compressionType, header);
	}

	
	private String generateBodyMessageHeader() {
		String uuid = UUID.randomUUID().toString();
		// UUID id universale
		// Es:
		// 550e8400-e29b-41d4-a716-446655440000 = 36 caratteri (32 digits e 4
		// '-').
		String prefixHeader = StringUtils.replace(uuid, "-", "");
		// Elimino i caratteri '-' dalla stringa precedente
		// 550e8400e29b41d4a716446655440000 = 32 caratteri.
		String messageBodyHeader = String.format("%1$s.txt", prefixHeader);
		return messageBodyHeader;
	}
	
	

	private byte[] legacyCompress(String uncompressedString,
			String compressionType, String header) throws IOException {
		byte[] encodedBytes = uncompressedString.getBytes(encoding);
		ByteArrayInputStream encodedByteStream = new ByteArrayInputStream(
				encodedBytes);
		
		return legacyCompress(encodedByteStream, compressionType, header);
	}
	
	private byte[] legacyCompress(InputStream uncompressedStream,
			String compressionType, String header) throws IOException {
		// Gestione parte del body da comprimere
		byte[] internalCompressWithoutHeader = internalCompress(uncompressedStream);
		// Appendo l'header ali byte compressi
		byte[] legacyCompressedBytes = appendHeaderToCompressedBytes(header,
				internalCompressWithoutHeader);
		return legacyCompressedBytes;
	}
	
	
	protected byte[] internalCompress(InputStream uncompressedStream)
			throws IOException {

		ByteArrayOutputStream zippedOutputStream = new ByteArrayOutputStream();
		GZIPOutputStream zippedStream = null;
		try {
			zippedStream = new GZIPOutputStream(zippedOutputStream);
			byte[] uncompressChunk = new byte[1024];
			int bytesReadCount = 0;
			while ((bytesReadCount = uncompressedStream.read(uncompressChunk)) != -1) {
				zippedStream.write(uncompressChunk, 0, bytesReadCount);
			}
		} finally {
			zippedStream.close();
		}
		return zippedOutputStream.toByteArray();
	}
	
	
	private byte[] appendHeaderToCompressedBytes(String header,
			byte[] compressedBytesWithoutHeader) throws IOException {
		// Gestione header come prefisso del body
		byte[] headerBytes = new byte[0];
		if (header != null) {
			headerBytes = header.getBytes(encoding);
		}
		// int headerArrayLength = headerBytes.length;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		// Appendo l'header
		bos.write(headerBytes);
		// appendo il body compresso
		bos.write(compressedBytesWithoutHeader);
		byte[] legacyCompressedBytes = bos.toByteArray();
		return legacyCompressedBytes;
	}


	
	
	
	
	
	
	public String getCompressionType() {
		return compressionType;
	}


	public void setCompressionType(String compressionType) {
		this.compressionType = compressionType;
	}


	public String getEncoding() {
		return encoding;
	}


	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
	
	
	
	
}
