package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="PRODUCT_PI_SDA_EX")
@IdClass(CodiciProdottoPICodiciProdottoSDAPK.class)
public class CodiciProdottoPICodiciProdottoSDA implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -385702249706841565L;

	public static final String PI_CODE = "piCode";
	public static final String SDA_CODE = "sdaCode";
	public static final String REGEX = "codeFormatRegex";

	private String piCode;
	private String sdaCode;
	private String codeFormatRegex;

	@Id
	@Column(name="PI_CODE")
	public String getPiCode() {
		return piCode;
	}
	
	public void setPiCode(String piCode) {
		this.piCode = piCode;
	}

	@Id
	@Column(name="SDA_CODE")
	public String getSdaCode() {
		return sdaCode;
	}
	
	public void setSdaCode(String sdaCode) {
		this.sdaCode = sdaCode;
	}
	
	@Id
	@Column(name="CODE_FORMAT_REGEX")
	public String getCodeFormatRegex() {
		return codeFormatRegex;
	}
	
	public void setCodeFormatRegex(String codeFormatRegex) {
		this.codeFormatRegex = codeFormatRegex;
	}
	

}
