package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface AptItemPositionDao extends IGenericDAO<AptItemPosition, String> {

}
