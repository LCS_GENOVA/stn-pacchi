/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.datamodel;


/**
 *
 * @author Da Procida
 */
public class XteInviiCausale {

    private String causale;
    private String descrizione;
    private Long numeroInvii;

    public String getCausale() {
        return causale;
    }

    public void setCausale(String causale) {
        this.causale = causale;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Long getNumeroInvii() {
        return numeroInvii;
    }

    public void setNumeroInvii(Long numeroInvii) {
        this.numeroInvii = numeroInvii;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + (this.causale != null ? this.causale.hashCode() : 0);
        hash = 43 * hash + (this.descrizione != null ? this.descrizione.hashCode() : 0);
        hash = 43 * hash + (this.numeroInvii != null ? this.numeroInvii.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final XteInviiCausale other = (XteInviiCausale) obj;
        if ((this.causale == null) ? (other.causale != null) : !this.causale.equals(other.causale)) {
            return false;
        }
        if ((this.descrizione == null) ? (other.descrizione != null) : !this.descrizione.equals(other.descrizione)) {
            return false;
        }
        if (this.numeroInvii != other.numeroInvii && (this.numeroInvii == null || !this.numeroInvii.equals(other.numeroInvii))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "XteInviiCausale{" + "causale=" + causale + ", descrizione=" + descrizione + ", numeroInvii=" + numeroInvii + '}';
    }

}
