/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.common;

import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Da Procida
 */
public class XteTestToolFieldValidator {
    private static NumberFormat numberFormatDecimalLocal =  NumberFormat.getNumberInstance();
    private static NumberFormat numberFormatIntegerLocal = NumberFormat.getIntegerInstance();
    private static DecimalFormatSymbols decimalFormatSymbolsLocal = DecimalFormatSymbols.getInstance();

    public XteTestToolFieldValidator() {

    }

    public static DecimalFormatSymbols getDecimalFormatSymbolsLocal() {
        return decimalFormatSymbolsLocal;
    }

    public static NumberFormat getNumberFormatIntegerLocal() {
        return numberFormatIntegerLocal;
    }

    public static NumberFormat getNumberFormatDecimalLocal() {
        return numberFormatDecimalLocal;
    }
        
    public boolean validateLatitude(String latitudeStr)
    {        
        try {
            if (latitudeStr == null || latitudeStr.isEmpty()) {
                return false;
            }

            String decimalSeparator = Character.toString(decimalFormatSymbolsLocal.getDecimalSeparator());
            String regex = "^[+-]?([1-8]?[0-9]\\" + decimalSeparator + "{1}?\\d{1,6}?$|90\\" + decimalSeparator + "{1}0{1,6}$)";

            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(latitudeStr);

            return matcher.matches();
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean validateLongitude(String longitudeStr)
    {        
        try {
            if (longitudeStr == null || longitudeStr.isEmpty()) {
                return false;
            }

            String decimalSeparator = Character.toString(decimalFormatSymbolsLocal.getDecimalSeparator());
            String regex = "^[+-]?((([1]?[0-7][0-9]|[1-9]?[0-9])\\" + decimalSeparator + "{1}\\d{1,6}$)|[1]?[1-8][0]\\" + decimalSeparator + "{1}0{1,6}$)";

            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(longitudeStr);

            return matcher.matches();
        } catch (Exception ex) {
            return false;
        }
    }

    public static int convertObjectToInteger(Object obj)
    {
        Number num;
        try {
            if(obj==null) {
                return 0;
            }

            String strNum = String.valueOf(obj);
            num = numberFormatIntegerLocal.parse(strNum);
            return num.intValue();
        } catch (Exception ex) {
            return 0;
        }
    }

    public static double convertObjectToDouble(Object obj)
    {
        Number num;
        try {
            if(obj==null) {
                return 0;
            }

            String strNum = String.valueOf(obj);
            num = numberFormatDecimalLocal.parse(strNum);
            return num.doubleValue();
        } catch (Exception ex) {
            return 0;
        }
    }

    public static float convertObjectToFloat(Object obj)
    {
        Number num;
        try {
            if(obj==null) {
                return 0;
            }

            String strNum = String.valueOf(obj);
            num = numberFormatDecimalLocal.parse(strNum);
            return num.floatValue();
        } catch (Exception ex) {
            return 0;
        }
    }

}
