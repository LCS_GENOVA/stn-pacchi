/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.dp;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XteMailPiece;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;

import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;

import com.selexelsag.xte.testtool.view.dp.XteTestToolAcquisizioneMazzettiDPView;

import com.selexelsag.xte.testtool.view.dp.XteTestToolWebServiceDPView;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta.AM;
import com.selexelsag.xtetesttool.xml.dataposta.am.ObjectFactory;
import com.selexelsag.xtetesttool.xml.dataposta.am.Traccia;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolAcquisizioneMazzettiDPController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener {

    private XteTestToolAcquisizioneMazzettiDPView acquisizioneMazzettiDPView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolAcquisizioneMazzettiDPController.class);
    private XteMailPiece selectedMailPiece = null;
    private int currentSelectedRow = -1;
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    private JAXBContext context;
    
    private static Traccia acquisizioneMazzettiTraccia;
    
    static {
        logger.debug("Creating Config Object Factory and Traccia");
        DataPosta dataPostaConfig = XteTestToolConfiguration.getInstance().getDataPostaConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        AM config = dataPostaConfig.getAM();

        acquisizioneMazzettiTraccia = objectFactory.createTraccia();
        acquisizioneMazzettiTraccia.setEvento(config.getEventName());
        acquisizioneMazzettiTraccia.setHeaderMazzetto(objectFactory.createHeaderMazzetto());
    }

    public XteTestToolAcquisizioneMazzettiDPController(JPanel acquisizioneMazzettiDPView) {
        try {
            logger.info("XteTestToolAcquisizioneMazzettiDPController start class creation");
            this.acquisizioneMazzettiDPView = (XteTestToolAcquisizioneMazzettiDPView) acquisizioneMazzettiDPView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.acquisizioneMazzettiDPView.initializeView();

            updateView();

            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.dataposta.am");

            logger.info("XteTestToolAcquisizioneMazzettiDPController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneMazzettiDPController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolAcquisizioneMazzettiDPController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Data Posta controller creation failed", ex);
        }
    }
    
    private void updateDataModel() {
        String id = acquisizioneMazzettiDPView.getBundleIdTextRef().getText();
        String opid = acquisizioneMazzettiDPView.getBundleTPIdTextRef().getText();
        Date date = acquisizioneMazzettiDPView.getBundleDatePickerRef().getDate();
        Date time = (Date)acquisizioneMazzettiDPView.getBundleTimeSpinnerRef().getValue();
        
        logger.debug("updateDataModel - " + id + "," + opid + "," + date + "," + time);
        acquisizioneMazzettiTraccia.getHeaderMazzetto().setCodiceIdentificativo(id);
        acquisizioneMazzettiTraccia.getHeaderMazzetto().setIdOperatoreTP(opid);
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);
        
        GregorianCalendar timeCalendar = new GregorianCalendar();
        timeCalendar.setTime(time);
        c.set(Calendar.HOUR_OF_DAY, timeCalendar.get(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));
        c.set(Calendar.SECOND, timeCalendar.get(Calendar.SECOND));
        c.set(Calendar.MILLISECOND, timeCalendar.get(Calendar.MILLISECOND));
        
        try {
            acquisizioneMazzettiTraccia.getHeaderMazzetto().setDataOraScansione(
                    DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException ex) {
            logger.error("Error converting date " + c, ex);
            acquisizioneMazzettiTraccia.getHeaderMazzetto().setDataOraScansione(null);
        }
    }
    
    
    private void updateView() {  
        String id = acquisizioneMazzettiTraccia.getHeaderMazzetto().getCodiceIdentificativo();
        String opid = acquisizioneMazzettiTraccia.getHeaderMazzetto().getIdOperatoreTP();
        XMLGregorianCalendar date = acquisizioneMazzettiTraccia.getHeaderMazzetto().getDataOraScansione();
        logger.debug("updateView - " + id + "," + opid + "," + date);
                
        if (id != null) {
            acquisizioneMazzettiDPView.getBundleIdTextRef()
                    .setText(id);
        }

        if (opid != null) {
            acquisizioneMazzettiDPView.getBundleTPIdTextRef()
                    .setText(opid);
        }
    
        if (date != null) {
            acquisizioneMazzettiDPView.getBundleDatePickerRef().setDate(
                date.toGregorianCalendar().getTime());
            acquisizioneMazzettiDPView.getBundleTimeSpinnerRef().setValue(
                date.toGregorianCalendar().getTime());
        }
    }
        
    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = acquisizioneMazzettiDPView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        acquisizioneMazzettiDPView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.acquisizioneMazzettiDPView.getAcquisizioneMazzettiPanelRef().addAncestorListener(this);        
        this.acquisizioneMazzettiDPView.getBundleIdTextRef().addFocusListener(this);
        this.acquisizioneMazzettiDPView.getBundleTPIdTextRef().addFocusListener(this);
        this.acquisizioneMazzettiDPView.getBundleDatePickerRef().getEditor().addFocusListener(this);
        JFormattedTextField startTimejFormattedTextField = ((JSpinner.DateEditor)acquisizioneMazzettiDPView.getBundleTimeSpinnerRef().getEditor()).getTextField();
        startTimejFormattedTextField.addFocusListener(this);
        this.acquisizioneMazzettiDPView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.acquisizioneMazzettiDPView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.acquisizioneMazzettiDPView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.acquisizioneMazzettiDPView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.acquisizioneMazzettiDPView.getBackButtonRef().addActionListener(this);
        this.acquisizioneMazzettiDPView.getHomeButtonRef().addActionListener(this);
        this.acquisizioneMazzettiDPView.getExitButtonRef().addActionListener(this);
                
        this.acquisizioneMazzettiDPView.getBundleDatePickerRef().getEditor().getDocument().addDocumentListener(this);
        ((JSpinner.DateEditor)acquisizioneMazzettiDPView.getBundleTimeSpinnerRef().getEditor()).getTextField().getDocument().addDocumentListener(this);

		this.salvaXmlFileChooser.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(acquisizioneMazzettiDPView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiDPController actionPerformed invoke generaXmlData");

            updateDataModel();

            generaXmlData();
        }

        if(source.equals(acquisizioneMazzettiDPView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiDPController actionPerformed invoke impostaDatiWebService");

            updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(acquisizioneMazzettiDPView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiDPController actionPerformed invoke salvaXmlData");

            updateDataModel();

            salvaXmlData();
        }

        if(source.equals(acquisizioneMazzettiDPView.getBackButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiDPController actionPerformed invoke back");

            updateDataModel();

            back();
        }

        if(source.equals(acquisizioneMazzettiDPView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiDPController actionPerformed invoke home");

            updateDataModel();

            home();
        }

        if(source.equals(acquisizioneMazzettiDPView.getExitButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiDPController actionPerformed invoke exit");

            updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(acquisizioneMazzettiDPView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolAcquisizioneMazzettiDPView)
        {
            logger.debug("XteTestToolAcquisizioneMazzettiDPController ancestorAdded invoke init");
            init();
            
            updateView();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneMazzettiDPController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Data Posta controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneMazzettiDPController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Data Posta controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }    
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        acquisizioneMazzettiDPView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolAcquisizioneMazzettiDPController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.acquisizioneMazzettiDPView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(acquisizioneMazzettiDPView);
            updateXteTestToolDataModel(dataModel);

            clearMessageBar();

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(acquisizioneMazzettiDPView.getImpostaDatiWSButtonRef());
            disabledComponents.add(acquisizioneMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            Calendar date = Calendar.getInstance();
            this.acquisizioneMazzettiDPView.getBundleDatePickerRef().setDate(date.getTime());
            this.acquisizioneMazzettiDPView.getBundleTimeSpinnerRef().setValue(date.getTime());

            acquisizioneMazzettiDPView.getBundleIdTextRef().requestFocus();

            logger.info("XteTestToolAcquisizioneMazzettiDPController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneMazzettiDPController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneMazzettiDPController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Data Posta controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolAcquisizioneMazzettiDPController start exit");
        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(acquisizioneMazzettiDPView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolAcquisizioneMazzettiDPController exit in progress");
            acquisizioneMazzettiDPView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolAcquisizioneMazzettiDPController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(acquisizioneMazzettiDPView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(acquisizioneMazzettiDPView.getAcquisizioneMazzettiPanelRef().getParent(), acquisizioneMazzettiDPView.getAcquisizioneMazzettiPanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolAcquisizioneMazzettiDPController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(acquisizioneMazzettiDPView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(acquisizioneMazzettiDPView.getAcquisizioneMazzettiPanelRef().getParent(), acquisizioneMazzettiDPView.getAcquisizioneMazzettiPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }
    
    private void generaXmlData()
    {
        logger.info("XteTestToolAcquisizioneMazzettiDPController start generaXmlData");

        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();
        
        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(acquisizioneMazzettiTraccia, out);
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
        try
        {            
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString());
            acquisizioneMazzettiDPView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("XteTestToolAcquisizioneMazzettiDPController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneMazzettiDPController generaXmlData failed", baseEx);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(baseEx.toString(), MessageType.ERROR);
            acquisizioneMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneMazzettiDPController generaXmlData failed", ex);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneMazzettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneMazzettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Data Posta controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        acquisizioneMazzettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(acquisizioneMazzettiDPView.getImpostaDatiWSButtonRef());
        enableComponent.add(acquisizioneMazzettiDPView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolAcquisizioneMazzettiDPController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServiceDPView(acquisizioneMazzettiDPView.getParentFrame());
            XteTestToolWebServiceDPController controller = new XteTestToolWebServiceDPController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            DataPosta dataPostaConfig = XteTestToolConfiguration.getInstance().getDataPostaConfig();
            AM config = dataPostaConfig.getAM();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(acquisizioneMazzettiDPView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(acquisizioneMazzettiDPView.getAcquisizioneMazzettiPanelRef().getParent(), acquisizioneMazzettiDPView.getAcquisizioneMazzettiPanelRef(), webserviceview, new Rectangle(0,70,1000,690));

            logger.info("XteTestToolAcquisizioneMazzettiDPController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolAcquisizioneMazzettiDPController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneMazzettiDPController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Data Posta controller impostaDatiWebService failed", ex);
        }
    }

    
    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolAcquisizioneMazzettiDPController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolAcquisizioneMazzettiDPController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(acquisizioneMazzettiDPView.getParentFrame());

            logger.info("XteTestToolAcquisizioneMazzettiDPController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneMazzettiDPController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneMazzettiDPController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Data Posta controller salvaXmlData failed", ex);
        }
    }
}
