/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.ssip.message;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.jms.core.MessageCreator;

/**
 * @author arodriguez
 *
 */
public abstract class SSIPAbstractMessageCreator implements MessageCreator, JmsHeaderConstantsInterface {

	private String textMessage;

	/**
	 * default constructor
	 */
	public SSIPAbstractMessageCreator() {
		super();
	}

	/**
	 * @param textMessage
	 */
	public SSIPAbstractMessageCreator(String textMessage) {
		this.textMessage = textMessage;
	}

	@Override
	public Message createMessage(Session jmsSession) throws JMSException {
		TextMessage jmsTextMessage = jmsSession.createTextMessage();
		buildMessageHeader(jmsTextMessage);
		jmsTextMessage.setText(textMessage);
		return jmsTextMessage;
	}

	private void buildMessageHeader(TextMessage message) throws JMSException {
		message.setStringProperty(SENT_DATE_KEY, getSentDate());
		message.setStringProperty(MESSAGE_TYPE_KEY, getMessageType());
		if (getSourceOfficeCode() != null)
			message.setStringProperty(SOURCE_OFFICE_CODE_KEY, getSourceOfficeCode());
		if (getDestinationOfficeCode() != null)
			message.setStringProperty(DESTINATION_OFFICE_CODE_KEY, getDestinationOfficeCode());
		if (getContainerCode() != null)
			message.setStringProperty(CONTAINER_CODE_KEY, getContainerCode());
		if (isTestMessage())
			message.setBooleanProperty(TEST_MESSAGE_KEY, isTestMessage());
		if (isCompressed())
			message.setBooleanProperty(COMPRESSED_KEY, isCompressed());
	}
	
	protected abstract String getSentDate();
	
	protected abstract String getSourceOfficeCode();
	
	protected abstract String getDestinationOfficeCode();
	
	protected abstract String getMessageType(); 
	
	protected abstract boolean isTestMessage();
	
	protected abstract String getContainerCode();
	
	protected abstract boolean isCompressed();
	
}
