/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.dp;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.view.XteTestToolPanelView;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolIntegerDocument;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolNumericDocument;
import com.selexelsag.xte.testtool.view.objectmodel.XteInviiCausaleTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteListSelectionModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteMailPieceTableModel;
import com.selexelsag.xte.testtool.view.objectrenderer.XteInviiCausaleTableCellRenderer;
import com.selexelsag.xte.testtool.view.objectrenderer.XteMailPieceTableCellRenderer;
import com.selexelsag.xte.testtool.view.objectrenderer.XteTableHeaderRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolItemOracleDPView extends XteTestToolPanelView{
    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolItemOracleDPView.class);
    private XteMailPieceTableModel xteMailPieceTableModel = new XteMailPieceTableModel();;
    private XteInviiCausaleTableModel xteInviiCausaleTableModel = new XteInviiCausaleTableModel();;
    
    public XteTestToolItemOracleDPView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try {
            logger.info("XteTestToolItemOracleDPView start class creation");
            this.messages = ResourceBundle.getBundle("bundles/messages");
            initComponents();
            logger.info("XteTestToolItemOracleDPView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolItemOracleDPView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Item Oracle Data Posta view creation failed", ex);
        }
    }

    public JPanel getItemOraclePanelRef(){ return this; }        
    public JLabel getItemOracleTitleLabelRef(){ return this.panelTitlejLabel; }
    
    public JPanel getBundleDataPaneRef(){ return this.bundleDatajPanel; }
    public JTextField getBundleIdTextRef(){ return this.bundleIdjTextField; }
    public JTextField getBundleTPIdTextRef(){ return this.bundleTPIdjTextField; }
    
    public JPanel getMailPieceDataPaneRef(){ return this.mailPieceDatajPanel; }
    public JTable getMailPieceTableRef(){ return this.mailPiecejTable; }
    public XteMailPieceTableModel getMailPieceTableModelRef(){ return (XteMailPieceTableModel) this.mailPiecejTable.getModel(); }

    public JTable getInviiCausaleTableRef(){ return this.inviiCausalejTable; }
    public XteInviiCausaleTableModel getInviiCausaleTableModelRef(){ return (XteInviiCausaleTableModel) this.inviiCausalejTable.getModel(); }

    public ButtonGroup getTipologiaButtonGroupRef(){ return this.tipologiaButtonGroup; }
    public JRadioButton getNotificaMazzettiRadioButtonRef(){ return this.notificaMazzettojRadioButton; }
    public JRadioButton getProntoRecapitoRadioButtonRef(){ return this.prontoRecapitojRadioButton; }

    public JTextField getTotaleInviiTextRef(){ return this.totInviijTextField; }
    public JTextField getInviiMazzettoTextRef(){ return this.inviiMazzettojTextField; }
    public JTextField getNumeroMazzettiTextRef(){ return this.numeroMazzettijTextField; }
    
    public JTextField getMailPieceLatitudeTextRef(){ return this.latitudinejTextField; }
    public JTextField getMailPieceLongitudeTextRef(){ return this.longitudinejTextField; }

    public JPanel getXmlDataPaneRef(){ return this.xmlDatajPanel; }
    public JScrollPane getXmlDataScrollPaneRef(){ return this.xmlDatajScrollPane; }
    public JTextArea getXmlDataTextAreaRef(){ return this.xmlDatajTextArea; }
    public JPanel getXmlDataCommandPaneRef(){ return this.xmlDataCommandjPanel; }    
    public JButton getGeneraXmlDataButtonRef(){ return this.generaXmlDatajButton; }
    public JButton getImpostaDatiWSButtonRef(){ return this.impostaDatiWSjButton; }
    public JButton getSalvaXmlDataButtonRef(){ return this.salvaXmlDatajButton; }
    
    public JButton getBackButtonRef(){ return this.backjButton; }
    public JButton getHomeButtonRef(){ return this.homejButton; }
    public JButton getExitButtonRef(){ return this.exitjButton; }
    public JTextPane getMessageBarTextPaneRef(){ return this.messageBarjTextPane; }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolItemOracleDPView start customizeView");
            //CUSTOMIZE TITLE
            this.panelTitlejLabel .setText(messages.getString("dataposta.itemoracle.view.title.label"));

            //CUSTOMIZE BUTTON
            this.generaXmlDatajButton.setText(messages.getString("dataposta.itemoracle.view.generaxmldata.button"));
            this.impostaDatiWSjButton.setText(messages.getString("dataposta.itemoracle.view.impostadatiws.button"));
            this.salvaXmlDatajButton.setText(messages.getString("dataposta.itemoracle.view.salvaxmldata.button"));
            this.backjButton.setText(messages.getString("view.back.button"));
            this.homejButton.setText(messages.getString("view.home.button"));
            this.exitjButton.setText(messages.getString("view.exit.button"));

            logger.info("XteTestToolItemOracleDPView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolItemOracleDPView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Item Oracle Data Posta view customization failed", ex);
        }
    }

    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolItemOracleDPView start initializeView");

//            buildComponentSortedMap();
//
//            this.setFocusCycleRoot(true);
//            this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

            defineTabPolicyForTextArea(xmlDatajTextArea);
            
            JLabel cornelLabel = new JLabel("");
            cornelLabel.setOpaque(true);
            cornelLabel.setBackground(new Color(240, 240, 240));

            this.mailPieceDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.mailPieceDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.mailPieceDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.mailPieceDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.mailPieceDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            customizeTable(this.mailPiecejTable);
            this.mailPiecejTable.setModel(xteMailPieceTableModel);

            List<String> mailPieceColumnWidthValues = new ArrayList<String>();
            mailPieceColumnWidthValues.add("0");
            mailPieceColumnWidthValues.add("150");
            mailPieceColumnWidthValues.add("0");
            mailPieceColumnWidthValues.add("200");
            mailPieceColumnWidthValues.add("125");
            mailPieceColumnWidthValues.add("125");
            mailPieceColumnWidthValues.add("0");

            this.mailPiecejTable.getTableHeader().setDefaultRenderer(new XteTableHeaderRenderer(mailPieceColumnWidthValues));
            this.mailPiecejTable.setDefaultRenderer(Object.class, new XteMailPieceTableCellRenderer(mailPieceColumnWidthValues));            
            
            this.inviiCausalejScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.inviiCausalejScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.inviiCausalejScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.inviiCausalejScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.inviiCausalejScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            customizeTable(this.inviiCausalejTable);
            this.inviiCausalejTable.setModel(xteInviiCausaleTableModel);

            List<String> inviiCausaleColumnWidthValues = new ArrayList<String>();
            inviiCausaleColumnWidthValues.add("25");
            inviiCausaleColumnWidthValues.add("100");
            inviiCausaleColumnWidthValues.add("200");
            inviiCausaleColumnWidthValues.add("80");

            this.inviiCausalejTable.getTableHeader().setDefaultRenderer(new XteTableHeaderRenderer(inviiCausaleColumnWidthValues));
            this.inviiCausalejTable.setDefaultRenderer(Object.class, new XteInviiCausaleTableCellRenderer(inviiCausaleColumnWidthValues));
            
            this.xmlDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.xmlDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.xmlDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.xmlDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.xmlDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            this.totInviijTextField.setDocument(new XteTestToolIntegerDocument(6));            
            this.totInviijTextField.getDocument().putProperty("owner", totInviijTextField);
            this.inviiMazzettojTextField.setDocument(new XteTestToolIntegerDocument(6));            
            this.inviiMazzettojTextField.getDocument().putProperty("owner", inviiMazzettojTextField);
            this.numeroMazzettijTextField.setDocument(new XteTestToolIntegerDocument(6));            
            this.numeroMazzettijTextField.getDocument().putProperty("owner", numeroMazzettijTextField);
            this.latitudinejTextField.setDocument(new XteTestToolNumericDocument(10));            
            this.latitudinejTextField.getDocument().putProperty("owner", latitudinejTextField);
            this.longitudinejTextField.setDocument(new XteTestToolNumericDocument(11));            
            this.longitudinejTextField.getDocument().putProperty("owner", longitudinejTextField);
            
            logger.info("XteTestToolItemOracleDPView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolItemOracleDPView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Item Oracle Data Posta view initialization failed", ex);
        }
    }

    @Override
    public void restoreView() {
        logger.debug("XteTestToolItemOracleDPView restoreView");
        this.getExitButtonRef().doClick();
    }

    private void customizeTable(JTable table) {
        table.getTableHeader().setReorderingAllowed(false);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setRowHeight(20);
        table.setSelectionModel(new XteListSelectionModel());
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    private  static void defineTabPolicyForTextArea(final JTextArea textarea) {

        final Object tabActionKey = new Object();
        final Object shiftTabActionKey = new Object();
        KeyStroke tabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0, false);
        KeyStroke shiftTabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_DOWN_MASK, false);

        Action tabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusNextComponent((Component) source);
                }
            }
        };

        Action shiftTabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent((Component) source);
                }
            }
        };

        textarea.getActionMap().put(tabActionKey, tabAction);
        textarea.getInputMap().put(tabKeyStroke, tabActionKey);

        textarea.getActionMap().put(shiftTabActionKey, shiftTabAction);
        textarea.getInputMap().put(shiftTabKeyStroke, shiftTabActionKey);

    }
    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.bundleIdjTextField);
        componentSortedMap.put(2, this.bundleTPIdjTextField);
        componentSortedMap.put(3, this.mailPiecejTable);
        componentSortedMap.put(4, this.notificaMazzettojRadioButton);
        componentSortedMap.put(5, this.prontoRecapitojRadioButton);
        componentSortedMap.put(6, this.totInviijTextField);
        componentSortedMap.put(7, this.inviiMazzettojTextField);
        componentSortedMap.put(8, this.numeroMazzettijTextField);        
        componentSortedMap.put(9, this.longitudinejTextField);
        componentSortedMap.put(10, this.latitudinejTextField);
        componentSortedMap.put(11, this.inviiCausalejTable);
        componentSortedMap.put(12, this.xmlDatajTextArea);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        tipologiaButtonGroup = new javax.swing.ButtonGroup();
        commandjPanel = new javax.swing.JPanel();
        messageBarjScrollPane = new javax.swing.JScrollPane();
        messageBarjTextPane = new javax.swing.JTextPane();
        homejButton = new javax.swing.JButton();
        exitjButton = new javax.swing.JButton();
        backjButton = new javax.swing.JButton();
        workingjPanel = new javax.swing.JPanel();
        panelTitlejLabel = new javax.swing.JLabel();
        xmlDatajPanel = new javax.swing.JPanel();
        xmlDatajScrollPane = new javax.swing.JScrollPane();
        xmlDatajTextArea = new javax.swing.JTextArea();
        xmlDataCommandjPanel = new javax.swing.JPanel();
        generaXmlDatajButton = new javax.swing.JButton();
        impostaDatiWSjButton = new javax.swing.JButton();
        salvaXmlDatajButton = new javax.swing.JButton();
        bundleDatajPanel = new javax.swing.JPanel();
        bundleTPIdjLabel = new javax.swing.JLabel();
        bundleTPIdjTextField = new javax.swing.JTextField();
        bundleIdjTextField = new javax.swing.JTextField();
        bundleIdjLabel = new javax.swing.JLabel();
        mailPieceDatajPanel = new javax.swing.JPanel();
        inviiCausalejPanel = new javax.swing.JPanel();
        inviiCausalejScrollPane = new javax.swing.JScrollPane();
        inviiCausalejTable = new javax.swing.JTable();
        tipologiajPanel = new javax.swing.JPanel();
        notificaMazzettojRadioButton = new javax.swing.JRadioButton();
        prontoRecapitojRadioButton = new javax.swing.JRadioButton();
        coordinatejPanel = new javax.swing.JPanel();
        longitudinejLabel = new javax.swing.JLabel();
        latitudinejLabel = new javax.swing.JLabel();
        longitudinejTextField = new javax.swing.JTextField();
        latitudinejTextField = new javax.swing.JTextField();
        itemsjPanel = new javax.swing.JPanel();
        totInviijLabel = new javax.swing.JLabel();
        inviiMazzettojLabel = new javax.swing.JLabel();
        totInviijTextField = new javax.swing.JTextField();
        inviiMazzettojTextField = new javax.swing.JTextField();
        numeroMazzettijLabel = new javax.swing.JLabel();
        numeroMazzettijTextField = new javax.swing.JTextField();
        mailPieceDatajScrollPane = new javax.swing.JScrollPane();
        mailPiecejTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setMaximumSize(new java.awt.Dimension(1000, 690));
        setMinimumSize(new java.awt.Dimension(1000, 690));
        setPreferredSize(new java.awt.Dimension(1000, 690));

        commandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        commandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        commandjPanel.setMaximumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setMinimumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setPreferredSize(new java.awt.Dimension(980, 80));

        messageBarjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageBarjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        messageBarjScrollPane.setMaximumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setMinimumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setPreferredSize(new java.awt.Dimension(350, 50));

        messageBarjTextPane.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        messageBarjTextPane.setFocusable(false);
        messageBarjScrollPane.setViewportView(messageBarjTextPane);

        homejButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        homejButton.setForeground(new java.awt.Color(51, 51, 51));
        homejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Home3.png"))); // NOI18N
        homejButton.setText("<html><body>&nbsp;Home<body><html>");
        homejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        homejButton.setFocusPainted(false);
        homejButton.setFocusable(false);
        homejButton.setIconTextGap(5);
        homejButton.setMaximumSize(new java.awt.Dimension(100, 50));
        homejButton.setMinimumSize(new java.awt.Dimension(100, 50));
        homejButton.setPreferredSize(new java.awt.Dimension(100, 50));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("<html><body>&nbsp;Exit<body><html>");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setFocusPainted(false);
        exitjButton.setFocusable(false);
        exitjButton.setIconTextGap(5);
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        backjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        backjButton.setForeground(new java.awt.Color(51, 51, 51));
        backjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/BackToMenu.png"))); // NOI18N
        backjButton.setText("<html><body>&nbsp;Back<body><html>");
        backjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        backjButton.setFocusPainted(false);
        backjButton.setFocusable(false);
        backjButton.setIconTextGap(5);
        backjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        backjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        backjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout commandjPanelLayout = new javax.swing.GroupLayout(commandjPanel);
        commandjPanel.setLayout(commandjPanelLayout);
        commandjPanelLayout.setHorizontalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 272, Short.MAX_VALUE)
                .addComponent(backjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandjPanelLayout.setVerticalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandjPanelLayout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homejButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exitjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        workingjPanel.setBackground(new java.awt.Color(230, 250, 0));
        workingjPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20)));
        workingjPanel.setMaximumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setMinimumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setPreferredSize(new java.awt.Dimension(980, 580));

        panelTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        panelTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelTitlejLabel.setText("Item da Oracle");
        panelTitlejLabel.setBackground(new java.awt.Color(240, 240, 240));
        panelTitlejLabel.setFocusable(false);
        panelTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        panelTitlejLabel.setIconTextGap(30);
        panelTitlejLabel.setMaximumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setMinimumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setPreferredSize(new java.awt.Dimension(920, 20));

        xmlDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "XML Data", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        xmlDatajPanel.setFocusable(false);
        xmlDatajPanel.setMaximumSize(new java.awt.Dimension(860, 170));
        xmlDatajPanel.setMinimumSize(new java.awt.Dimension(860, 170));
        xmlDatajPanel.setOpaque(false);
        xmlDatajPanel.setPreferredSize(new java.awt.Dimension(860, 170));

        xmlDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        xmlDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        xmlDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 125));
        xmlDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 125));
        xmlDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 125));

        xmlDatajTextArea.setColumns(20);
        xmlDatajTextArea.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        xmlDatajTextArea.setForeground(new java.awt.Color(51, 51, 51));
        xmlDatajTextArea.setLineWrap(true);
        xmlDatajTextArea.setRows(5);
        xmlDatajTextArea.setWrapStyleWord(true);
        xmlDatajTextArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setViewportView(xmlDatajTextArea);

        xmlDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        xmlDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        xmlDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        generaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        generaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        generaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml6.png"))); // NOI18N
        generaXmlDatajButton.setText("<html><body>&nbsp;Genera<br>XML Data<body><html>");
        generaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        generaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        generaXmlDatajButton.setFocusPainted(false);
        generaXmlDatajButton.setFocusable(false);
        generaXmlDatajButton.setIconTextGap(2);
        generaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        xmlDataCommandjPanel.add(generaXmlDatajButton, gridBagConstraints);

        impostaDatiWSjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        impostaDatiWSjButton.setForeground(new java.awt.Color(51, 51, 51));
        impostaDatiWSjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/WebService5.png.png"))); // NOI18N
        impostaDatiWSjButton.setText("<html><body>&nbsp;Imposta<br>&nbsp;Dati WS<body><html>");
        impostaDatiWSjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        impostaDatiWSjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        impostaDatiWSjButton.setFocusPainted(false);
        impostaDatiWSjButton.setFocusable(false);
        impostaDatiWSjButton.setIconTextGap(1);
        impostaDatiWSjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        xmlDataCommandjPanel.add(impostaDatiWSjButton, gridBagConstraints);

        salvaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        salvaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        salvaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml7.png"))); // NOI18N
        salvaXmlDatajButton.setText("<html><body>&nbsp;Salva<br>XML Data<body><html>");
        salvaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        salvaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        salvaXmlDatajButton.setFocusPainted(false);
        salvaXmlDatajButton.setFocusable(false);
        salvaXmlDatajButton.setIconTextGap(2);
        salvaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        xmlDataCommandjPanel.add(salvaXmlDatajButton, gridBagConstraints);

        javax.swing.GroupLayout xmlDatajPanelLayout = new javax.swing.GroupLayout(xmlDatajPanel);
        xmlDatajPanel.setLayout(xmlDatajPanelLayout);
        xmlDatajPanelLayout.setHorizontalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        xmlDatajPanelLayout.setVerticalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        bundleDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Informazioni Mazzetto", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        bundleDatajPanel.setMaximumSize(new java.awt.Dimension(860, 50));
        bundleDatajPanel.setMinimumSize(new java.awt.Dimension(860, 50));
        bundleDatajPanel.setOpaque(false);
        bundleDatajPanel.setPreferredSize(new java.awt.Dimension(860, 50));

        bundleTPIdjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleTPIdjLabel.setText("ID Operatore TP:");
        bundleTPIdjLabel.setFocusable(false);
        bundleTPIdjLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        bundleTPIdjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjTextField.setText("Mario Rossi");
        bundleTPIdjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        bundleTPIdjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        bundleTPIdjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        bundleTPIdjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        bundleIdjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleIdjTextField.setText("38999123456789012789");
        bundleIdjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        bundleIdjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        bundleIdjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        bundleIdjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        bundleIdjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleIdjLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleIdjLabel.setText("Codice Identificativo:");
        bundleIdjLabel.setFocusable(false);
        bundleIdjLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        bundleIdjLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        bundleIdjLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        javax.swing.GroupLayout bundleDatajPanelLayout = new javax.swing.GroupLayout(bundleDatajPanel);
        bundleDatajPanel.setLayout(bundleDatajPanelLayout);
        bundleDatajPanelLayout.setHorizontalGroup(
            bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bundleIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addComponent(bundleTPIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleTPIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        bundleDatajPanelLayout.setVerticalGroup(
            bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bundleTPIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bundleTPIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bundleIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bundleIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        mailPieceDatajPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        mailPieceDatajPanel.setMaximumSize(new java.awt.Dimension(860, 290));
        mailPieceDatajPanel.setMinimumSize(new java.awt.Dimension(860, 290));
        mailPieceDatajPanel.setOpaque(false);
        mailPieceDatajPanel.setPreferredSize(new java.awt.Dimension(860, 290));

        inviiCausalejPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Invii per Causale", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(0, 51, 255))); // NOI18N
        inviiCausalejPanel.setMaximumSize(new java.awt.Dimension(420, 188));
        inviiCausalejPanel.setMinimumSize(new java.awt.Dimension(420, 188));
        inviiCausalejPanel.setOpaque(false);
        inviiCausalejPanel.setPreferredSize(new java.awt.Dimension(420, 188));

        inviiCausalejScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        inviiCausalejScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        inviiCausalejScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        inviiCausalejScrollPane.setMaximumSize(new java.awt.Dimension(400, 150));
        inviiCausalejScrollPane.setMinimumSize(new java.awt.Dimension(400, 150));
        inviiCausalejScrollPane.setPreferredSize(new java.awt.Dimension(400, 150));

        inviiCausalejTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        inviiCausalejScrollPane.setViewportView(inviiCausalejTable);

        javax.swing.GroupLayout inviiCausalejPanelLayout = new javax.swing.GroupLayout(inviiCausalejPanel);
        inviiCausalejPanel.setLayout(inviiCausalejPanelLayout);
        inviiCausalejPanelLayout.setHorizontalGroup(
            inviiCausalejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inviiCausalejPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(inviiCausalejScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        inviiCausalejPanelLayout.setVerticalGroup(
            inviiCausalejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inviiCausalejPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(inviiCausalejScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tipologiajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Tipologia", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(0, 51, 255))); // NOI18N
        tipologiajPanel.setMaximumSize(new java.awt.Dimension(400, 50));
        tipologiajPanel.setMinimumSize(new java.awt.Dimension(400, 50));
        tipologiajPanel.setOpaque(false);
        tipologiajPanel.setPreferredSize(new java.awt.Dimension(400, 50));

        tipologiaButtonGroup.add(notificaMazzettojRadioButton);
        notificaMazzettojRadioButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        notificaMazzettojRadioButton.setSelected(true);
        notificaMazzettojRadioButton.setText("Notifica Mazzetto");
        notificaMazzettojRadioButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/RadioButton_Off2.png"))); // NOI18N
        notificaMazzettojRadioButton.setMaximumSize(new java.awt.Dimension(160, 25));
        notificaMazzettojRadioButton.setMinimumSize(new java.awt.Dimension(160, 25));
        notificaMazzettojRadioButton.setOpaque(false);
        notificaMazzettojRadioButton.setPreferredSize(new java.awt.Dimension(160, 25));
        notificaMazzettojRadioButton.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/images/RadioButton_On2.png"))); // NOI18N

        tipologiaButtonGroup.add(prontoRecapitojRadioButton);
        prontoRecapitojRadioButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        prontoRecapitojRadioButton.setText("Pronto per il Recapito");
        prontoRecapitojRadioButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/RadioButton_Off2.png"))); // NOI18N
        prontoRecapitojRadioButton.setMaximumSize(new java.awt.Dimension(160, 25));
        prontoRecapitojRadioButton.setMinimumSize(new java.awt.Dimension(160, 25));
        prontoRecapitojRadioButton.setOpaque(false);
        prontoRecapitojRadioButton.setPreferredSize(new java.awt.Dimension(160, 25));
        prontoRecapitojRadioButton.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/images/RadioButton_On2.png"))); // NOI18N

        javax.swing.GroupLayout tipologiajPanelLayout = new javax.swing.GroupLayout(tipologiajPanel);
        tipologiajPanel.setLayout(tipologiajPanelLayout);
        tipologiajPanelLayout.setHorizontalGroup(
            tipologiajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tipologiajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(notificaMazzettojRadioButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                .addComponent(prontoRecapitojRadioButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        tipologiajPanelLayout.setVerticalGroup(
            tipologiajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tipologiajPanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(tipologiajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(notificaMazzettojRadioButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(prontoRecapitojRadioButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        coordinatejPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Coordinate", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(0, 51, 255))); // NOI18N
        coordinatejPanel.setMaximumSize(new java.awt.Dimension(400, 50));
        coordinatejPanel.setMinimumSize(new java.awt.Dimension(400, 50));
        coordinatejPanel.setOpaque(false);
        coordinatejPanel.setPreferredSize(new java.awt.Dimension(400, 50));

        longitudinejLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        longitudinejLabel.setForeground(new java.awt.Color(51, 51, 51));
        longitudinejLabel.setText("Longitudine:");
        longitudinejLabel.setFocusable(false);
        longitudinejLabel.setMaximumSize(new java.awt.Dimension(90, 20));
        longitudinejLabel.setMinimumSize(new java.awt.Dimension(90, 20));
        longitudinejLabel.setPreferredSize(new java.awt.Dimension(90, 20));

        latitudinejLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        latitudinejLabel.setForeground(new java.awt.Color(51, 51, 51));
        latitudinejLabel.setText("Latitudine:");
        latitudinejLabel.setFocusable(false);
        latitudinejLabel.setMaximumSize(new java.awt.Dimension(70, 20));
        latitudinejLabel.setMinimumSize(new java.awt.Dimension(70, 20));
        latitudinejLabel.setPreferredSize(new java.awt.Dimension(70, 20));

        longitudinejTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        longitudinejTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        longitudinejTextField.setMaximumSize(new java.awt.Dimension(90, 20));
        longitudinejTextField.setMinimumSize(new java.awt.Dimension(90, 20));
        longitudinejTextField.setPreferredSize(new java.awt.Dimension(90, 20));

        latitudinejTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        latitudinejTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        latitudinejTextField.setMaximumSize(new java.awt.Dimension(90, 20));
        latitudinejTextField.setMinimumSize(new java.awt.Dimension(90, 20));
        latitudinejTextField.setPreferredSize(new java.awt.Dimension(90, 20));

        javax.swing.GroupLayout coordinatejPanelLayout = new javax.swing.GroupLayout(coordinatejPanel);
        coordinatejPanel.setLayout(coordinatejPanelLayout);
        coordinatejPanelLayout.setHorizontalGroup(
            coordinatejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(coordinatejPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(longitudinejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(longitudinejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addComponent(latitudinejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(latitudinejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );
        coordinatejPanelLayout.setVerticalGroup(
            coordinatejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(coordinatejPanelLayout.createSequentialGroup()
                .addGroup(coordinatejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(coordinatejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(latitudinejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(latitudinejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(coordinatejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(longitudinejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(longitudinejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 2, Short.MAX_VALUE))
        );

        itemsjPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Items", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(0, 51, 255))); // NOI18N
        itemsjPanel.setMaximumSize(new java.awt.Dimension(400, 75));
        itemsjPanel.setMinimumSize(new java.awt.Dimension(400, 75));
        itemsjPanel.setOpaque(false);
        itemsjPanel.setPreferredSize(new java.awt.Dimension(400, 75));

        totInviijLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        totInviijLabel.setForeground(new java.awt.Color(51, 51, 51));
        totInviijLabel.setText("Tot. Invii:");
        totInviijLabel.setFocusable(false);
        totInviijLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        totInviijLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        totInviijLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        inviiMazzettojLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        inviiMazzettojLabel.setForeground(new java.awt.Color(51, 51, 51));
        inviiMazzettojLabel.setText("Invii per Mazzetto:");
        inviiMazzettojLabel.setFocusable(false);
        inviiMazzettojLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        inviiMazzettojLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        inviiMazzettojLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        totInviijTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        totInviijTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        totInviijTextField.setMaximumSize(new java.awt.Dimension(60, 20));
        totInviijTextField.setMinimumSize(new java.awt.Dimension(60, 20));
        totInviijTextField.setPreferredSize(new java.awt.Dimension(60, 20));

        inviiMazzettojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        inviiMazzettojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        inviiMazzettojTextField.setMaximumSize(new java.awt.Dimension(60, 20));
        inviiMazzettojTextField.setMinimumSize(new java.awt.Dimension(60, 20));
        inviiMazzettojTextField.setPreferredSize(new java.awt.Dimension(60, 20));

        numeroMazzettijLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        numeroMazzettijLabel.setForeground(new java.awt.Color(51, 51, 51));
        numeroMazzettijLabel.setText("Num. Mazzetti:");
        numeroMazzettijLabel.setFocusable(false);
        numeroMazzettijLabel.setMaximumSize(new java.awt.Dimension(100, 20));
        numeroMazzettijLabel.setMinimumSize(new java.awt.Dimension(100, 20));
        numeroMazzettijLabel.setPreferredSize(new java.awt.Dimension(100, 20));

        numeroMazzettijTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        numeroMazzettijTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        numeroMazzettijTextField.setMaximumSize(new java.awt.Dimension(60, 20));
        numeroMazzettijTextField.setMinimumSize(new java.awt.Dimension(60, 20));
        numeroMazzettijTextField.setPreferredSize(new java.awt.Dimension(60, 20));

        javax.swing.GroupLayout itemsjPanelLayout = new javax.swing.GroupLayout(itemsjPanel);
        itemsjPanel.setLayout(itemsjPanelLayout);
        itemsjPanelLayout.setHorizontalGroup(
            itemsjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(itemsjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(itemsjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(itemsjPanelLayout.createSequentialGroup()
                        .addComponent(totInviijLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(totInviijTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(numeroMazzettijLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(numeroMazzettijTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(itemsjPanelLayout.createSequentialGroup()
                        .addComponent(inviiMazzettojLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(inviiMazzettojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        itemsjPanelLayout.setVerticalGroup(
            itemsjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(itemsjPanelLayout.createSequentialGroup()
                .addGroup(itemsjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totInviijLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totInviijTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(numeroMazzettijLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(numeroMazzettijTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(itemsjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(inviiMazzettojLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(inviiMazzettojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        mailPieceDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        mailPieceDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        mailPieceDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        mailPieceDatajScrollPane.setMaximumSize(new java.awt.Dimension(420, 150));
        mailPieceDatajScrollPane.setMinimumSize(new java.awt.Dimension(420, 150));
        mailPieceDatajScrollPane.setPreferredSize(new java.awt.Dimension(420, 50));

        mailPiecejTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        mailPieceDatajScrollPane.setViewportView(mailPiecejTable);

        javax.swing.GroupLayout mailPieceDatajPanelLayout = new javax.swing.GroupLayout(mailPieceDatajPanel);
        mailPieceDatajPanel.setLayout(mailPieceDatajPanelLayout);
        mailPieceDatajPanelLayout.setHorizontalGroup(
            mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mailPieceDatajScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tipologiajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(coordinatejPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(itemsjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                        .addComponent(inviiCausalejPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        mailPieceDatajPanelLayout.setVerticalGroup(
            mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mailPieceDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mailPieceDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 66, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(inviiCausalejPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mailPieceDatajPanelLayout.createSequentialGroup()
                        .addComponent(tipologiajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(itemsjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(coordinatejPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout workingjPanelLayout = new javax.swing.GroupLayout(workingjPanel);
        workingjPanel.setLayout(workingjPanelLayout);
        workingjPanelLayout.setHorizontalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workingjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(workingjPanelLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bundleDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        workingjPanelLayout.setVerticalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(mailPieceDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backjButton;
    private javax.swing.JPanel bundleDatajPanel;
    private javax.swing.JLabel bundleIdjLabel;
    private javax.swing.JTextField bundleIdjTextField;
    private javax.swing.JLabel bundleTPIdjLabel;
    private javax.swing.JTextField bundleTPIdjTextField;
    private javax.swing.JPanel commandjPanel;
    private javax.swing.JPanel coordinatejPanel;
    private javax.swing.JButton exitjButton;
    private javax.swing.JButton generaXmlDatajButton;
    private javax.swing.JButton homejButton;
    private javax.swing.JButton impostaDatiWSjButton;
    private javax.swing.JPanel inviiCausalejPanel;
    private javax.swing.JScrollPane inviiCausalejScrollPane;
    private javax.swing.JTable inviiCausalejTable;
    private javax.swing.JLabel inviiMazzettojLabel;
    private javax.swing.JTextField inviiMazzettojTextField;
    private javax.swing.JPanel itemsjPanel;
    private javax.swing.JLabel latitudinejLabel;
    private javax.swing.JTextField latitudinejTextField;
    private javax.swing.JLabel longitudinejLabel;
    private javax.swing.JTextField longitudinejTextField;
    private javax.swing.JPanel mailPieceDatajPanel;
    private javax.swing.JScrollPane mailPieceDatajScrollPane;
    private javax.swing.JTable mailPiecejTable;
    private javax.swing.JScrollPane messageBarjScrollPane;
    private javax.swing.JTextPane messageBarjTextPane;
    private javax.swing.JRadioButton notificaMazzettojRadioButton;
    private javax.swing.JLabel numeroMazzettijLabel;
    private javax.swing.JTextField numeroMazzettijTextField;
    private javax.swing.JLabel panelTitlejLabel;
    private javax.swing.JRadioButton prontoRecapitojRadioButton;
    private javax.swing.JButton salvaXmlDatajButton;
    private javax.swing.ButtonGroup tipologiaButtonGroup;
    private javax.swing.JPanel tipologiajPanel;
    private javax.swing.JLabel totInviijLabel;
    private javax.swing.JTextField totInviijTextField;
    private javax.swing.JPanel workingjPanel;
    private javax.swing.JPanel xmlDataCommandjPanel;
    private javax.swing.JPanel xmlDatajPanel;
    private javax.swing.JScrollPane xmlDatajScrollPane;
    private javax.swing.JTextArea xmlDatajTextArea;
    // End of variables declaration//GEN-END:variables


}
