package com.selexelsag.xte.model.flows;

import java.io.Serializable;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.selexelsag.xte.model.track.respcreation.XPathQueryCallResult;

/**
 * @author Lanciano
 *
 */
public class FlowParams implements Serializable {
	
	public static final String REQ_PROVIDER_SOURCE = "REQ_PROVIDER_SOURCE";
	
	public static final String REQ_PROVIDER_KEY_1 = "REQ_PROVIDER_KEY_1";
	
	public static final String REQ_PROVIDER_KEY_2 = "REQ_PROVIDER_KEY_2";
	
	public static final String REQ_PROVIDER_KEY_3 = "REQ_PROVIDER_KEY_3";
	
	public static final String REQ_PROVIDER_KEY_4 = "REQ_PROVIDER_KEY_4";
	
	public static final String REQ_PROVIDER_KEY_5 = "REQ_PROVIDER_KEY_5";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7903603613101149481L;

	/**
	 * identificativo del tipo flusso in esecuzione
	 */
	private String flowId;
	
	/**
	 * Nome della classe che dovra' essere serializzata e spedita di ritorno al client
	 */
	private String returnClassName;
	/**
	 * Oggetto di ritorno
	 */
	private Object returnObject;
	
	private boolean waitingForResponse;
	
	private String requestValidationSchema;
	
	private String requestValidationProperties;
	
	private String requestTrasformationModel;
	
	private HashMap<String, String> paramTransf;
	
	private String responseTrasformationModel;
	
	private String offRefID;
	
	private boolean doTransformation;
	
	private Map<String, String> serviceRequestProviderParameters;
	
	private XPathQueryCallResult queriesCallResult;
	

	/* questi parametri dovrebbero confluire nel httpServiceUrl
	private String officeHostName;
	private String officeIP;
	private String aspURL;
	*/
	private URL httpServiceUrl;
	
	private String fowardToInt;
	
	public String getFlowId() {
		return flowId;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	
	public boolean isWaitingForResponse() {
		return waitingForResponse;
	}
	public void setWaitingForResponse(boolean waitingForResponse) {
		this.waitingForResponse = waitingForResponse;
	}
	
	public String getRequestValidationSchema() {
		return requestValidationSchema;
	}
	public void setRequestValidationSchema(String requestValidationSchema) {
		this.requestValidationSchema = requestValidationSchema;
	}
	public String getRequestValidationProperties() {
		return requestValidationProperties;
	}
	public void setRequestValidationProperties(String requestValidationProperties) {
		this.requestValidationProperties = requestValidationProperties;
	}
	public String getRequestTrasformationModel() {
		return requestTrasformationModel;
	}
	public void setRequestTrasformationModel(String requestTrasformationModel) {
		this.requestTrasformationModel = requestTrasformationModel;
	}
	public String getResponseTrasformationModel() {
		return responseTrasformationModel;
	}
	public void setResponseTrasformationModel(String responseTrasformationModel) {
		this.responseTrasformationModel = responseTrasformationModel;
	}
	public String getOffRefID() {
		return offRefID;
	}
	public void setOffRefID(String offRefID) {
		this.offRefID = offRefID;
	}
	public URL getHttpServiceUrl() {
		return httpServiceUrl;
	}
	public void setHttpServiceUrl(URL httpServiceUrl) {
		this.httpServiceUrl = httpServiceUrl;
	}
	
	public String getReturnClassName() {
		return returnClassName;
	}
	public void setReturnClassName(String returnClassName) {
		this.returnClassName = returnClassName;
	}
	public Object getReturnObject() {
		return returnObject;
	}
	public void setReturnObject(Object returnObject) {
		this.returnObject = returnObject;
	}
	
	public boolean isDoTransformation() {
		return doTransformation;
	}
	public void setDoTransformation(boolean doTransformation) {
		this.doTransformation = doTransformation;
	}
	
	
	public Map<String, String> getServiceRequestProviderParameters() {
		return serviceRequestProviderParameters;
	}
	
	public void setServiceRequestProviderParameters(Map<String, String> serviceRequestProviderParameters) {
		this.serviceRequestProviderParameters = serviceRequestProviderParameters;
	}
	
	public XPathQueryCallResult getQueriesCallResult() {
		return queriesCallResult;
	}
	public void setQueriesCallResult(XPathQueryCallResult queriesCallResult) {
		this.queriesCallResult = queriesCallResult;
	}
	
	public HashMap<String, String> getParamTransf() {
		return paramTransf;
	}
	public void setParamTransf(HashMap<String, String> paramTransf) {
		this.paramTransf = paramTransf;
	}
	
	
	@Override
	public String toString() {
		return String
				.format("FlowParams [flowId=%s, returnClassName=%s, returnObject=%s, waitingForResponse=%s, requestValidationSchema=%s, requestTrasformationModel=%s, responseTrasformationModel=%s, offRefID=%s, doTransformation=%s, serviceRequestProviderParameters=%s, queriesCallResult=%s, httpServiceUrl=%s]",
						flowId, returnClassName, returnObject,
						waitingForResponse, requestValidationSchema,
						requestTrasformationModel, responseTrasformationModel,
						offRefID, doTransformation,
						serviceRequestProviderParameters, queriesCallResult,
						httpServiceUrl);
	}
	
	public String getFowardToInt() {
		return fowardToInt;
	}
	
	public void setFowardToInt(String fowardToInt) {
		this.fowardToInt = fowardToInt;
	}
}
