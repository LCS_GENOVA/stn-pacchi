/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.ao;

import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.ao.XteTestToolAcquisizioneConfigurazioneAOView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import java.awt.Color;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolAcquisizioneConfigurazioneAOController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener {

    private XteTestToolAcquisizioneConfigurazioneAOView acquisizioneConfigurazioneAOView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolAcquisizioneConfigurazioneAOController.class);

    public XteTestToolAcquisizioneConfigurazioneAOController(JPanel acquisizioneConfigurazioneAOView) {
        try {
            logger.info("XteTestToolAcquisizioneConfigurazioneAOController start class creation");
            this.acquisizioneConfigurazioneAOView = (XteTestToolAcquisizioneConfigurazioneAOView) acquisizioneConfigurazioneAOView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.acquisizioneConfigurazioneAOView.initializeView();
            addViewObjectsListeners();
            logger.info("XteTestToolAcquisizioneConfigurazioneAOController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneConfigurazioneAOController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolAcquisizioneConfigurazioneAOController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Configurazione Posta Registrata controller creation failed", ex);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = acquisizioneConfigurazioneAOView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        acquisizioneConfigurazioneAOView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.acquisizioneConfigurazioneAOView.getAcquisizioneConfigurazionePanelRef().addAncestorListener(this);
        this.acquisizioneConfigurazioneAOView.getXmlDataTextAreaRef().addFocusListener(this);
        this.acquisizioneConfigurazioneAOView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.acquisizioneConfigurazioneAOView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.acquisizioneConfigurazioneAOView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.acquisizioneConfigurazioneAOView.getBackButtonRef().addActionListener(this);
        this.acquisizioneConfigurazioneAOView.getHomeButtonRef().addActionListener(this);
        this.acquisizioneConfigurazioneAOView.getExitButtonRef().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(acquisizioneConfigurazioneAOView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneConfigurazioneAOController actionPerformed invoke generaXmlData");
            generaXmlData();
        }

        if(source.equals(acquisizioneConfigurazioneAOView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneConfigurazioneAOController actionPerformed invoke impostaDatiWebService");
            impostaDatiWebService();
        }

        if(source.equals(acquisizioneConfigurazioneAOView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneConfigurazioneAOController actionPerformed invoke salvaXmlData");
            salvaXmlData();
        }

        if(source.equals(acquisizioneConfigurazioneAOView.getBackButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneConfigurazioneAOController actionPerformed invoke back");
            back();
        }

        if(source.equals(acquisizioneConfigurazioneAOView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneConfigurazioneAOController actionPerformed invoke home");
            home();
        }

        if(source.equals(acquisizioneConfigurazioneAOView.getExitButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneConfigurazioneAOController actionPerformed invoke exit");
            exit();
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolAcquisizioneConfigurazioneAOView)
        {
            logger.debug("XteTestToolAcquisizioneConfigurazioneAOController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        if(comp instanceof JTextField)
        {
            ((JTextField)comp).setBackground(new Color(255,204,102));
        }
        if(comp instanceof JPasswordField)
        {
            ((JPasswordField)comp).setBackground(new Color(255,204,102));
        }
        if(comp instanceof JTextArea)
        {
            ((JTextArea)comp).setBackground(new Color(255,204,102));
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        if(comp instanceof JTextField)
        {
            if(((JTextField)comp).isEditable()) {
                ((JTextField)comp).setBackground(Color.WHITE);
            }
            else {
                ((JTextField)comp).setBackground(new Color(220, 220, 220));
            }
        }
        if(comp instanceof JPasswordField)
        {
            if(((JPasswordField)comp).isEditable()) {
                ((JPasswordField)comp).setBackground(Color.WHITE);
            }
            else {
                ((JPasswordField)comp).setBackground(new Color(220, 220, 220));
            }
        }
        if(comp instanceof JTextArea)
        {
            if(((JTextArea)comp).isEditable()) {
                ((JTextArea)comp).setBackground(Color.WHITE);
            }
            else {
                ((JTextArea)comp).setBackground(new Color(220, 220, 220));
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
            
    }

    private void init()
    {
        try{
            logger.info("XteTestToolAcquisizioneConfigurazioneAOController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.acquisizioneConfigurazioneAOView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(acquisizioneConfigurazioneAOView);
            updateXteTestToolDataModel(dataModel);

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(acquisizioneConfigurazioneAOView.getImpostaDatiWSButtonRef());
            disabledComponents.add(acquisizioneConfigurazioneAOView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            acquisizioneConfigurazioneAOView.getXmlDataTextAreaRef().requestFocus();

            logger.info("XteTestToolAcquisizioneConfigurazioneAOController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneConfigurazioneAOController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneConfigurazioneAOController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Configurazione Posta Registrata controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolAcquisizioneConfigurazioneAOController start exit");
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(acquisizioneConfigurazioneAOView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolAcquisizioneConfigurazioneAOController exit in progress");
            acquisizioneConfigurazioneAOView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolAcquisizioneMazzettiPRController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(acquisizioneConfigurazioneAOView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(acquisizioneConfigurazioneAOView.getAcquisizioneConfigurazionePanelRef().getParent(), acquisizioneConfigurazioneAOView.getAcquisizioneConfigurazionePanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolAcquisizioneConfigurazioneAOController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        XteTestToolHomeView homeView = new XteTestToolHomeView(acquisizioneConfigurazioneAOView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(acquisizioneConfigurazioneAOView.getAcquisizioneConfigurazionePanelRef().getParent(), acquisizioneConfigurazioneAOView.getAcquisizioneConfigurazionePanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private void generaXmlData()
    {
        logger.info("XteTestToolAcquisizioneConfigurazioneAOController start generaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();


            logger.info("XteTestToolAcquisizioneConfigurazioneAOController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneConfigurazioneAOController generaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneConfigurazioneAOController generaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Configurazione Posta Registrata controller generaXmlData failed", ex);
        }
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolAcquisizioneConfigurazioneAOController start impostaDatiWebService");
            
            logger.info("XteTestToolAcquisizioneConfigurazioneAOController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolAcquisizioneConfigurazioneAOController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneConfigurazioneAOController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Configurazione Posta Registrata controller impostaDatiWebService failed", ex);
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolAcquisizioneConfigurazioneAOController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();


            logger.info("XteTestToolAcquisizioneConfigurazioneAOController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneConfigurazioneAOController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneConfigurazioneAOController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Configurazione Posta Registrata controller salvaXmlData failed", ex);
        }
    }

}
