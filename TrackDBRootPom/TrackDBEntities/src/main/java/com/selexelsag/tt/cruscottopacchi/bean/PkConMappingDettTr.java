package com.selexelsag.tt.cruscottopacchi.bean;


import java.io.Serializable;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;


/** 
* R.L. BeanGenerator - 28.05.2012 16:22:59
* R.L. - Inseriti per il nuovo metodo del trackDbWS - packDataRetrieve
*/ 
@Entity
@Table(name="PK_CON_MAPPING_DETT_TR")
@IdClass(PkConMappingDettTrPK.class)
public class PkConMappingDettTr implements Serializable {

	private static final long serialVersionUID = 2991649696276408497L;

	@Id
	@Column(name = "COD_TRACCIA", nullable =false)
	private String codTraccia;
	
	@Id
	@Column(name = "DETT_ORIG", nullable =false)
	private String dettOrig;

	@Column(name = "DETT_NUOVO", nullable =false)
	private String dettNuovo;

	public String getCodTraccia() {
		return codTraccia;
	}

	public void setCodTraccia(String codTraccia) {
		this.codTraccia = codTraccia;
	}

	public String getDettOrig() {
		return dettOrig;
	}

	public void setDettOrig(String dettOrig) {
		this.dettOrig = dettOrig;
	}

	public String getDettNuovo() {
		return dettNuovo;
	}

	public void setDettNuovo(String dettNuovo) {
		this.dettNuovo = dettNuovo;
	}

	
	}