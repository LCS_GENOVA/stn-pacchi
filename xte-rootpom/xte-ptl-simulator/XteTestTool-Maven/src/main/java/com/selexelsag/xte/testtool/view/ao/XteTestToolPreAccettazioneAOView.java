/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.ao;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.view.XteTestToolPanelView;
import com.selexelsag.xte.testtool.view.common.XteTestToolFocusTraversalPolicy;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolPreAccettazioneAOView extends XteTestToolPanelView{
    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolPreAccettazioneAOView.class);
    
    public XteTestToolPreAccettazioneAOView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try {
            logger.info("XteTestToolPreAccettazioneAOView start class creation");
            this.messages = ResourceBundle.getBundle("bundles/messages");
            initComponents();
            logger.info("XteTestToolPreAccettazioneAOView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolPreAccettazioneAOView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "PreAccettazione Accettazione Online view creation failed", ex);
        }
    }

    public JPanel getPreAccettazionePanelRef(){ return this; }        
    public JLabel getPreAccettazioneTitleLabelRef(){ return this.panelTitlejLabel; }
    
    public JPanel getXmlDataPaneRef(){ return this.xmlDatajPanel; }
    public JScrollPane getXmlDataScrollPaneRef(){ return this.xmlDatajScrollPane; }
    public JTextArea getXmlDataTextAreaRef(){ return this.xmlDatajTextArea; }
    public JPanel getXmlDataCommandPaneRef(){ return this.xmlDataCommandjPanel; }    

    public JButton getPulisciRichiestaButtonRef(){ return this.pulisciRichiestajButton; }
    public JButton getPulisciRispostaButtonRef(){ return this.pulisciRispostajButton; }
    public JButton getChiamaWSButtonRef(){ return this.chiamaWSjButton; }
    public JButton getCaricaFolderButtonRef(){ return this.caricaFolderjButton; }
    public JButton getCaricaXmlButtonRef(){ return this.caricaXmljButton; }

    public JPanel getWSDataPaneRef(){ return this.wsDatajPanel; }
    public JScrollPane getWSDataScrollPaneRef(){ return this.wsDatajScrollPane; }
    public JTextArea getWsDataTextAreaRef(){ return this.wsDatajTextArea; }
    
    public JButton getBackButtonRef(){ return this.backjButton; }
    public JButton getHomeButtonRef(){ return this.homejButton; }
    public JButton getExitButtonRef(){ return this.exitjButton; }
    public JTextPane getMessageBarTextPaneRef(){ return this.messageBarjTextPane; }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolPreAccettazioneAOView start customizeView");
            //CUSTOMIZE TITLE
            this.panelTitlejLabel .setText(messages.getString("accettazioneonline.preaccettazione.view.title.label"));

            //CUSTOMIZE BUTTON
            this.pulisciRichiestajButton.setText(messages.getString("accettazioneonline.preaccettazione.view.puliscirichiesta.button"));
            this.pulisciRispostajButton.setText(messages.getString("accettazioneonline.preaccettazione.view.puliscirisposta.button"));
            this.chiamaWSjButton.setText(messages.getString("accettazioneonline.preaccettazione.view.chiamaws.button"));
            this.caricaFolderjButton.setText(messages.getString("accettazioneonline.preaccettazione.view.caricafolder.button"));
            this.caricaXmljButton.setText(messages.getString("accettazioneonline.preaccettazione.view.caricaxml.button"));
            
            this.backjButton.setText(messages.getString("view.back.button"));
            this.homejButton.setText(messages.getString("view.home.button"));
            this.exitjButton.setText(messages.getString("view.exit.button"));

            logger.info("XteTestToolPreAccettazioneAOView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolPreAccettazioneAOView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "PreAccettazione Accettazione Online view customization failed", ex);
        }
    }

    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolPreAccettazioneAOView start initializeView");

//            buildComponentSortedMap();
//
//            this.setFocusCycleRoot(true);
//            this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

            defineTabPolicyForTextArea(xmlDatajTextArea);
            defineTabPolicyForTextArea(wsDatajTextArea);
            
            JLabel cornelLabel = new JLabel("");
            cornelLabel.setOpaque(true);
            cornelLabel.setBackground(new Color(240, 240, 240));

            this.xmlDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.xmlDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.xmlDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.xmlDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.xmlDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            this.wsDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.wsDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.wsDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.wsDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.wsDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));
            
            logger.info("XteTestToolPreAccettazioneAOView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolPreAccettazioneAOView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "PreAccettazione Accettazione Online view initialization failed", ex);
        }
    }

    @Override
    public void restoreView() {
        logger.debug("XteTestToolPreAccettazioneAOView restoreView");
        this.getExitButtonRef().doClick();
    }

    private  static void defineTabPolicyForTextArea(final JTextArea textarea) {

        final Object tabActionKey = new Object();
        final Object shiftTabActionKey = new Object();
        KeyStroke tabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0, false);
        KeyStroke shiftTabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_DOWN_MASK, false);

        Action tabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusNextComponent((Component) source);
                }
            }
        };

        Action shiftTabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent((Component) source);
                }
            }
        };

        textarea.getActionMap().put(tabActionKey, tabAction);
        textarea.getInputMap().put(tabKeyStroke, tabActionKey);

        textarea.getActionMap().put(shiftTabActionKey, shiftTabAction);
        textarea.getInputMap().put(shiftTabKeyStroke, shiftTabActionKey);

    }
    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.xmlDatajTextArea);
        componentSortedMap.put(2, this.wsDatajTextArea);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        commandjPanel = new javax.swing.JPanel();
        messageBarjScrollPane = new javax.swing.JScrollPane();
        messageBarjTextPane = new javax.swing.JTextPane();
        homejButton = new javax.swing.JButton();
        exitjButton = new javax.swing.JButton();
        backjButton = new javax.swing.JButton();
        workingjPanel = new javax.swing.JPanel();
        panelTitlejLabel = new javax.swing.JLabel();
        xmlDatajPanel = new javax.swing.JPanel();
        xmlDatajScrollPane = new javax.swing.JScrollPane();
        xmlDatajTextArea = new javax.swing.JTextArea();
        xmlDataCommandjPanel = new javax.swing.JPanel();
        chiamaWSjButton = new javax.swing.JButton();
        caricaFolderjButton = new javax.swing.JButton();
        caricaXmljButton = new javax.swing.JButton();
        pulisciRispostajButton = new javax.swing.JButton();
        pulisciRichiestajButton = new javax.swing.JButton();
        wsDatajPanel = new javax.swing.JPanel();
        wsDatajScrollPane = new javax.swing.JScrollPane();
        wsDatajTextArea = new javax.swing.JTextArea();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setMaximumSize(new java.awt.Dimension(1000, 690));
        setMinimumSize(new java.awt.Dimension(1000, 690));
        setPreferredSize(new java.awt.Dimension(1000, 690));

        commandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        commandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        commandjPanel.setMaximumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setMinimumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setPreferredSize(new java.awt.Dimension(980, 80));

        messageBarjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageBarjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        messageBarjScrollPane.setMaximumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setMinimumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setPreferredSize(new java.awt.Dimension(350, 50));

        messageBarjTextPane.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        messageBarjTextPane.setFocusable(false);
        messageBarjScrollPane.setViewportView(messageBarjTextPane);

        homejButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        homejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Home3.png"))); // NOI18N
        homejButton.setText("<html><body>&nbsp;Home<body><html>");
        homejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        homejButton.setFocusPainted(false);
        homejButton.setFocusable(false);
        homejButton.setForeground(new java.awt.Color(51, 51, 51));
        homejButton.setIconTextGap(5);
        homejButton.setMaximumSize(new java.awt.Dimension(100, 50));
        homejButton.setMinimumSize(new java.awt.Dimension(100, 50));
        homejButton.setPreferredSize(new java.awt.Dimension(100, 50));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("<html><body>&nbsp;Exit<body><html>");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setFocusPainted(false);
        exitjButton.setFocusable(false);
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIconTextGap(5);
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        backjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        backjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/BackToMenu.png"))); // NOI18N
        backjButton.setText("<html><body>&nbsp;Back<body><html>");
        backjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        backjButton.setFocusPainted(false);
        backjButton.setFocusable(false);
        backjButton.setForeground(new java.awt.Color(51, 51, 51));
        backjButton.setIconTextGap(5);
        backjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        backjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        backjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout commandjPanelLayout = new javax.swing.GroupLayout(commandjPanel);
        commandjPanel.setLayout(commandjPanelLayout);
        commandjPanelLayout.setHorizontalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 272, Short.MAX_VALUE)
                .addComponent(backjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandjPanelLayout.setVerticalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandjPanelLayout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homejButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exitjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        workingjPanel.setBackground(new java.awt.Color(230, 250, 0));
        workingjPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20)));
        workingjPanel.setMaximumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setMinimumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setPreferredSize(new java.awt.Dimension(980, 580));

        panelTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        panelTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelTitlejLabel.setText("Pre-Accettazione");
        panelTitlejLabel.setBackground(new java.awt.Color(240, 240, 240));
        panelTitlejLabel.setFocusable(false);
        panelTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        panelTitlejLabel.setIconTextGap(30);
        panelTitlejLabel.setMaximumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setMinimumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setPreferredSize(new java.awt.Dimension(920, 20));

        xmlDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "XML Data", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        xmlDatajPanel.setFocusable(false);
        xmlDatajPanel.setMaximumSize(new java.awt.Dimension(860, 260));
        xmlDatajPanel.setMinimumSize(new java.awt.Dimension(860, 260));
        xmlDatajPanel.setPreferredSize(new java.awt.Dimension(860, 260));
        xmlDatajPanel.setOpaque(false);

        xmlDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        xmlDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        xmlDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 200));
        xmlDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 200));
        xmlDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 200));

        xmlDatajTextArea.setColumns(20);
        xmlDatajTextArea.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        xmlDatajTextArea.setLineWrap(true);
        xmlDatajTextArea.setRows(5);
        xmlDatajTextArea.setWrapStyleWord(true);
        xmlDatajTextArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajTextArea.setForeground(new java.awt.Color(51, 51, 51));
        xmlDatajScrollPane.setViewportView(xmlDatajTextArea);

        xmlDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        xmlDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        xmlDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 200));
        xmlDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 200));
        xmlDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 200));
        xmlDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        chiamaWSjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        chiamaWSjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/WebService5.png.png"))); // NOI18N
        chiamaWSjButton.setText("<html><body>&nbsp;Chiama<br>&nbsp;&nbsp;&nbsp;WS<body><html>");
        chiamaWSjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        chiamaWSjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        chiamaWSjButton.setFocusPainted(false);
        chiamaWSjButton.setFocusable(false);
        chiamaWSjButton.setForeground(new java.awt.Color(51, 51, 51));
        chiamaWSjButton.setIconTextGap(1);
        chiamaWSjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        chiamaWSjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        chiamaWSjButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 3, 0, 0);
        xmlDataCommandjPanel.add(chiamaWSjButton, gridBagConstraints);

        caricaFolderjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        caricaFolderjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Folder1.png"))); // NOI18N
        caricaFolderjButton.setText("<html><body>&nbsp;Carica<br>&nbsp;Folder<body><html>");
        caricaFolderjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        caricaFolderjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        caricaFolderjButton.setFocusPainted(false);
        caricaFolderjButton.setFocusable(false);
        caricaFolderjButton.setForeground(new java.awt.Color(51, 51, 51));
        caricaFolderjButton.setIconTextGap(2);
        caricaFolderjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        caricaFolderjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        caricaFolderjButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(4, 3, 0, 0);
        xmlDataCommandjPanel.add(caricaFolderjButton, gridBagConstraints);

        caricaXmljButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        caricaXmljButton.setForeground(new java.awt.Color(51, 51, 51));
        caricaXmljButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml6.png"))); // NOI18N
        caricaXmljButton.setText("<html><body>&nbsp;Carica<br>&nbsp;&nbsp;&nbsp;XML<body><html>");
        caricaXmljButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        caricaXmljButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        caricaXmljButton.setFocusPainted(false);
        caricaXmljButton.setFocusable(false);
        caricaXmljButton.setIconTextGap(2);
        caricaXmljButton.setMaximumSize(new java.awt.Dimension(100, 35));
        caricaXmljButton.setMinimumSize(new java.awt.Dimension(100, 35));
        caricaXmljButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 3, 0, 0);
        xmlDataCommandjPanel.add(caricaXmljButton, gridBagConstraints);

        pulisciRispostajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        pulisciRispostajButton.setForeground(new java.awt.Color(51, 51, 51));
        pulisciRispostajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Clear3.png"))); // NOI18N
        pulisciRispostajButton.setText("<html><body>&nbsp;&nbsp;Pulisci<br>&nbsp;Risposta<body><html>");
        pulisciRispostajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        pulisciRispostajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        pulisciRispostajButton.setFocusPainted(false);
        pulisciRispostajButton.setFocusable(false);
        pulisciRispostajButton.setIconTextGap(1);
        pulisciRispostajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        pulisciRispostajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        pulisciRispostajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 3, 0, 0);
        xmlDataCommandjPanel.add(pulisciRispostajButton, gridBagConstraints);

        pulisciRichiestajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        pulisciRichiestajButton.setForeground(new java.awt.Color(51, 51, 51));
        pulisciRichiestajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Clear3.png"))); // NOI18N
        pulisciRichiestajButton.setText("<html><body>&nbsp;&nbsp;Pulisci<br>&nbsp;Richiesta<body><html>");
        pulisciRichiestajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        pulisciRichiestajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        pulisciRichiestajButton.setFocusPainted(false);
        pulisciRichiestajButton.setFocusable(false);
        pulisciRichiestajButton.setIconTextGap(1);
        pulisciRichiestajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        pulisciRichiestajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        pulisciRichiestajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 0, 0);
        xmlDataCommandjPanel.add(pulisciRichiestajButton, gridBagConstraints);

        javax.swing.GroupLayout xmlDatajPanelLayout = new javax.swing.GroupLayout(xmlDatajPanel);
        xmlDatajPanel.setLayout(xmlDatajPanelLayout);
        xmlDatajPanelLayout.setHorizontalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        xmlDatajPanelLayout.setVerticalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        wsDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Risposta Web Service", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        wsDatajPanel.setFocusable(false);
        wsDatajPanel.setMaximumSize(new java.awt.Dimension(860, 220));
        wsDatajPanel.setMinimumSize(new java.awt.Dimension(860, 220));
        wsDatajPanel.setPreferredSize(new java.awt.Dimension(860, 220));
        wsDatajPanel.setOpaque(false);

        wsDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        wsDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        wsDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        wsDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 170));
        wsDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 170));
        wsDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 170));

        wsDatajTextArea.setEditable(false);
        wsDatajTextArea.setBackground(new java.awt.Color(230, 230, 230));
        wsDatajTextArea.setColumns(20);
        wsDatajTextArea.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        wsDatajTextArea.setForeground(new java.awt.Color(51, 51, 51));
        wsDatajTextArea.setLineWrap(true);
        wsDatajTextArea.setRows(5);
        wsDatajTextArea.setWrapStyleWord(true);
        wsDatajTextArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        wsDatajScrollPane.setViewportView(wsDatajTextArea);

        javax.swing.GroupLayout wsDatajPanelLayout = new javax.swing.GroupLayout(wsDatajPanel);
        wsDatajPanel.setLayout(wsDatajPanelLayout);
        wsDatajPanelLayout.setHorizontalGroup(
            wsDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, wsDatajPanelLayout.createSequentialGroup()
                .addContainerGap(154, Short.MAX_VALUE)
                .addComponent(wsDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        wsDatajPanelLayout.setVerticalGroup(
            wsDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, wsDatajPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(wsDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout workingjPanelLayout = new javax.swing.GroupLayout(workingjPanel);
        workingjPanel.setLayout(workingjPanelLayout);
        workingjPanelLayout.setHorizontalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workingjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(workingjPanelLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(wsDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(8, Short.MAX_VALUE))
        );
        workingjPanelLayout.setVerticalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(wsDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backjButton;
    private javax.swing.JButton caricaFolderjButton;
    private javax.swing.JButton caricaXmljButton;
    private javax.swing.JButton chiamaWSjButton;
    private javax.swing.JPanel commandjPanel;
    private javax.swing.JButton exitjButton;
    private javax.swing.JButton homejButton;
    private javax.swing.JScrollPane messageBarjScrollPane;
    private javax.swing.JTextPane messageBarjTextPane;
    private javax.swing.JLabel panelTitlejLabel;
    private javax.swing.JButton pulisciRichiestajButton;
    private javax.swing.JButton pulisciRispostajButton;
    private javax.swing.JPanel workingjPanel;
    private javax.swing.JPanel wsDatajPanel;
    private javax.swing.JScrollPane wsDatajScrollPane;
    private javax.swing.JTextArea wsDatajTextArea;
    private javax.swing.JPanel xmlDataCommandjPanel;
    private javax.swing.JPanel xmlDatajPanel;
    private javax.swing.JScrollPane xmlDatajScrollPane;
    private javax.swing.JTextArea xmlDatajTextArea;
    // End of variables declaration//GEN-END:variables


}
