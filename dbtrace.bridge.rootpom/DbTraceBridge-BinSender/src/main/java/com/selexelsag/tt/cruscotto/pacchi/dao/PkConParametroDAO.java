package com.selexelsag.tt.cruscotto.pacchi.dao;

import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.selexelsag.tt.cruscotto.pacchi.bean.PkConParametro;

public class PkConParametroDAO extends SpringHibernateJpaDAOBase<PkConParametro, String> 
implements IPkConParametroDAO{

}
