package com.elsagdatamat.tt.trackdbws.blimpl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.elsagdatamat.tt.trackdbws.bl.ITraceBLSupport;
import com.elsagdatamat.tt.trackdbws.dao.ITraceDAO;

/*
 * @Author HZ,  11/2011
 */
public class TraceBLSupport implements ITraceBLSupport {

	Log log = LogFactory.getLog(getClass());

	@Autowired
	ITraceDAO traceDAO;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public long insertTrace(List<Trace> listOfTraces) {
		log.trace("insertTrace. Inizio");

		long numTraces = 0;
		for (Trace trace : listOfTraces) {
			if(this.insertTrace(trace))
				numTraces++;
		}
		log.trace("insertTrace. Fine");
		return numTraces;
	}
	

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean insertTrace(Trace trace) {
		return this.insertTrace(trace, false);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean insertTrace(Trace trace, boolean mergeDetachedEntity) {
		log.trace("insertTrace. Inizio");

		String tracedEntity = trace.getTracedEntity();
		String idTracedEntity = trace.getIdTracedEntity();

		if (!validatedTrace(trace)) {
			log.info("insertTrace() - Scartata traccia con campo nullo relativa a "
					+ (tracedEntity != null ? tracedEntity : "[entita' tracciata nulla]") + " identificata da "
					+ (idTracedEntity != null ? idTracedEntity : "[id nullo per l'entita' tracciata]"));
			return false;
		}

		if (trace.getWhenRegistered() == null){
			trace.setWhenRegistered(local2utc());
		}
		
		if (mergeDetachedEntity){
			 traceDAO.merge(trace);
		} else {
			traceDAO.insert(trace); 
		}

		log.trace("insertTrace. Fine");
		return true;
	}
	

	public static boolean validatedTrace(Trace inputTrace) {

		boolean isValidable = true;

		String channel = inputTrace.getChannel();
		String idChannel = inputTrace.getIdChannel();
		String idTracedEntity = inputTrace.getIdTracedEntity();
		String tracedEntity = inputTrace.getTracedEntity();
		String whatHappened = inputTrace.getWhatHappened();
		Date whenHappened = inputTrace.getWhenHappened();
		String whereHappened = inputTrace.getWhereHappened();
		String serviceName = inputTrace.getServiceName();

		if (channel == null || channel.length() == 0)
			isValidable = false;
		if (idChannel == null || idChannel.length() == 0)
			isValidable = false;
		if (idTracedEntity == null || idTracedEntity.length() == 0)
			isValidable = false;
		if (tracedEntity == null || tracedEntity.length() == 0)
			isValidable = false;
		if (whatHappened == null || whatHappened.length() == 0)
			isValidable = false;
		if (whenHappened == null)
			isValidable = false;
		if (whereHappened == null || whereHappened.length() == 0)
			isValidable = false;
		if (serviceName == null || serviceName.length() == 0)
			isValidable = false;

		return isValidable;
	}

	public static Date local2utc() {
		Calendar utcTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		Calendar localTime = Calendar.getInstance();

		localTime.set(Calendar.YEAR, utcTime.get(Calendar.YEAR));
		localTime.set(Calendar.MONTH, utcTime.get(Calendar.MONTH));
		localTime.set(Calendar.DAY_OF_MONTH, utcTime.get(Calendar.DAY_OF_MONTH));
		localTime.set(Calendar.HOUR_OF_DAY, utcTime.get(Calendar.HOUR_OF_DAY));
		localTime.set(Calendar.MINUTE, utcTime.get(Calendar.MINUTE));
		localTime.set(Calendar.SECOND, utcTime.get(Calendar.SECOND));
		localTime.set(Calendar.MILLISECOND, utcTime.get(Calendar.MILLISECOND));

		return localTime.getTime();
	}

}
