package com.elsagdatamat.trackdb.toedwh.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.hibernate.SQLQuery;
import org.hibernate.Transaction;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.trackdb.toedwh.entities.SentTrace;
import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSend;

public class SentTraceDAO extends SpringHibernateJpaDAOBase<SentTrace, Long> implements ISentTraceDAO {

	@Override
	@Deprecated
	public void insertSentTraces(List<SentTrace> vSentTraces) {
		// molto lenta, sarebbe meglio l'operazione batch insertSentTracesBatch
		Date data = new Date();
		for(SentTrace st : vSentTraces){
			st.setTime(data);
			insert(st);
		}
	}


	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public void insertSentTracesBatch(List<SentTrace> vSentTraces) {

		MyJpaBatchCallback<Integer> myJpaCallbak = new MyJpaBatchCallback(vSentTraces);
		getJpaTemplate().execute(myJpaCallbak,true);

	}



	@SuppressWarnings("deprecation")
	@Override
	public void fillSentTraces(Long id, List<TraceTypesToSend> typesToSend) {

		StringBuffer conditions = new StringBuffer();
		boolean first = true;
		conditions.append("(");
		for (TraceTypesToSend traceTypesToSend : typesToSend) {
			if (!first) {
				conditions.append(" or ");
			}
			conditions.append("(");
			conditions.append("t.traced_Entity='" + traceTypesToSend.getTracedEntity() + "'");
			// if no channel is specified, just consider all channels
			if (traceTypesToSend.getChannel() != null) {
				conditions.append("and t.channel='" + traceTypesToSend.getChannel() + "'");
			}
			conditions.append("and t.what_Happened='" + traceTypesToSend.getWhatHappened() + "'");
			conditions.append(")");
			first = false;
		}
		conditions.append(")");

		final String sqlQuery = "insert into Sent_Trace (id, time) select t.Id, sysdate from Traces t "
				+" where t.id<="+id+" and t.id not in(select st1.Id from Sent_Trace st1 ) and "+ conditions;
		logger.debug(sqlQuery);

		MyJpaCallback<Integer> myJpaCallbak = new MyJpaCallback<Integer>(sqlQuery);
		Object res = getJpaTemplate().execute(myJpaCallbak,true);

		logger.debug(res);

	}

	@SuppressWarnings({ "hiding", "deprecation", "rawtypes" })
	class MyJpaCallback<Integer> implements JpaCallback {
		String hqlQuery;

		public MyJpaCallback(String hqlQuery){
			this.hqlQuery = hqlQuery;
		}

		@Override
		@Transactional(propagation=Propagation.SUPPORTS)
		public Object doInJpa(EntityManager em) throws PersistenceException {
			org.hibernate.Session session = ((HibernateEntityManager) em).getSession();
			SQLQuery query = session.createSQLQuery(hqlQuery);

			return query.executeUpdate();
		}
	}



	class MyJpaBatchCallback<Integer> implements JpaCallback {

		List<SentTrace> vSentTraces;

		public MyJpaBatchCallback(List<SentTrace> vSentTraces){
			this.vSentTraces = vSentTraces;
		}

		@Override
		@Transactional(propagation=Propagation.SUPPORTS)
		public Object doInJpa(EntityManager em) throws PersistenceException {
			org.hibernate.Session session = ((HibernateEntityManager) em).getSession();

			Transaction tx = session.beginTransaction();

			for ( int i=0; i<vSentTraces.size(); i++ ) {
			    session.save(vSentTraces.get(i));
			    if ( i % 1000 == 0 ) { //20, same as the JDBC batch size
			        //flush a batch of inserts and release memory:
			        session.flush();
			        session.clear();
			    }
			}

			tx.commit();
			session.close();

			Date data = new Date();
			for(SentTrace st : vSentTraces){
				st.setTime(data);
				insert(st);
			}

			return null;
		}
	}


}
