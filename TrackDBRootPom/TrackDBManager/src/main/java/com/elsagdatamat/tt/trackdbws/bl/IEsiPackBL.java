package com.elsagdatamat.tt.trackdbws.bl;

import java.util.Date;
import java.util.List;

import com.elsagdatamat.tt.trackdbws.beans.VO.EsiPackVO;

public interface IEsiPackBL {
	
	public List<EsiPackVO> getListOfNEsiPacks_PI(Date date,Long esiPackId, Date dateTillto,int size);
	public List<EsiPackVO> getListOfNEsiPacks_SDA(Date date,Long esiPackId, Date dateTillto,int size);
}
