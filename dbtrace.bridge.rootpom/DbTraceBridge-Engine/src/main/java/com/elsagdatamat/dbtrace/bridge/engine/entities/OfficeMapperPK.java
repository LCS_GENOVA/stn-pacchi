package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;

import javax.persistence.Column;

public class OfficeMapperPK implements Serializable {

	private static final long serialVersionUID = -1266440897517889374L;

	public static final String FRAZIONARIO_ESTERNO = "frazionarioEsterno";
	public static final String FAMILY = "family";

	private String frazionarioEsterno;
	private String family;

	@Column(name = "FRAZIONARIO_EXT")
	public String getFrazionarioEsterno() {
		return frazionarioEsterno;
	}

	public void setFrazionarioEsterno(String frazionarioEsterno) {
		this.frazionarioEsterno = frazionarioEsterno;
	}

	@Column(name = "FAMILY")
	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((family == null) ? 0 : family.hashCode());
		result = prime * result + ((frazionarioEsterno == null) ? 0 : frazionarioEsterno.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OfficeMapperPK other = (OfficeMapperPK) obj;
		if (family == null) {
			if (other.family != null)
				return false;
		} else if (!family.equals(other.family))
			return false;
		if (frazionarioEsterno == null) {
			if (other.frazionarioEsterno != null)
				return false;
		} else if (!frazionarioEsterno.equals(other.frazionarioEsterno))
			return false;
		return true;
	}
}
