/**
 * 
 */
package com.elsagdatamat.dbtrace.bridge.tosda;

import java.math.BigInteger;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author arodriguez
 *
 */
public class SDAFromSsip {

    @XmlElement(name = "ID_TRACED_EXTERNAL", required = true, nillable = true)
    protected String idtracedexternal;
    
    @XmlElement(name = "ID_STATUS", required = true)
    protected String idstatus;
    
    @XmlElement(name = "DESC_STATUS")
    protected String descstatus;
    
    @XmlElement(name = "DESC_CAUSALE")
    protected String desccausale;
    
    @XmlElement(name = "ID_CAUSALE")
    protected String idcausale;
    
    @XmlElement(name = "DIRITTI_POSTALI")
    protected BigInteger dirittipostali;
    
    @XmlElement(name = "IMPORTO_CONTRASSEGNO")
    protected BigInteger importocontrassegno;
    
    @XmlElement(name = "ONERI_DOGANALI")
    protected BigInteger oneridoganali;
    
    @XmlElement(name = "NOTE")
    protected String note;

    
    
    /**
	 * default constructor
	 */
	private SDAFromSsip() {
		super();
	}

	public SDAFromSsip(Map<String, String> map) {
		
		setIDTRACEDEXTERNAL(map.get("ID_TRACED_EXTERNAL"));
		setIDSTATUS(map.get("ID_STATUS"));
		
		if (map.containsKey("DESC_STATUS"))
			setDESCSTATUS(map.get("DESC_STATUS"));

		if (map.containsKey("DESC_CAUSALE"))
			setDESCCAUSALE(map.get("DESC_CAUSALE"));

		if (map.containsKey("ID_CAUSALE"))
			setIDCAUSALE(map.get("ID_CAUSALE"));

		if (map.containsKey("NOTE"))
			setNOTE(map.get("NOTE"));

		// TODO: verifica formattazione decimali, se presenti
		if (map.containsKey("DIRITTI_POSTALI"))
			try {
				setDIRITTIPOSTALI(new BigInteger(map.get("DIRITTI_POSTALI")));  
			} catch(Exception fe) {
			}

		if (map.containsKey("IMPORTO_CONTRASSEGNO"))
			try {
				setIMPORTOCONTRASSEGNO(new BigInteger(map.get("IMPORTO_CONTRASSEGNO")));
			} catch (Exception fe) {
			}
			
		if (map.containsKey("ONERI_DOGANALI"))
			try {
				setONERIDOGANALI(new BigInteger(map.get("ONERI_DOGANALI")));
			} catch (Exception fe) {
			}

	}

    /**
     * Gets the value of the idtracedexternal property.
     * @return possible object is{@link String }
     */
    public String getIDTRACEDEXTERNAL() {
        return idtracedexternal;
    }

    /**
     * Sets the value of the idtracedexternal property.
     * @param value allowed object is {@link String }
     */
    protected void setIDTRACEDEXTERNAL(String value) {
        this.idtracedexternal = value;
    }

    /**
     * Gets the value of the idstatus property.
     * @return  possible object is {@link String }
     */
    public String getIDSTATUS() {
        return idstatus;
    }

    /**
     * Sets the value of the idstatus property.
     * @param value allowed object is {@link String }
     */
    protected void setIDSTATUS(String value) {
        this.idstatus = value;
    }

    /**
     * Gets the value of the descstatus property.
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public String getDESCSTATUS() {
        return descstatus;
    }

    /**
     * Sets the value of the descstatus property.
     * @param value allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    protected void setDESCSTATUS(String value) {
        this.descstatus = value;
    }

    /**
     * Gets the value of the desccausale property.
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public String getDESCCAUSALE() {
        return desccausale;
    }

    /**
     * Sets the value of the desccausale property.
     * @param value allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    protected void setDESCCAUSALE(String value) {
        this.desccausale = value;
    }

    /**
     * Gets the value of the idcausale property.
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public String getIDCAUSALE() {
        return idcausale;
    }

    /**
     * Sets the value of the idcausale property.
     * @param value allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    protected void setIDCAUSALE(String value) {
        this.idcausale = value;
    }

    /**
     * Gets the value of the dirittipostali property.
     * @return possible object is {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    public BigInteger getDIRITTIPOSTALI() {
        return dirittipostali;
    }

    /**
     * Sets the value of the dirittipostali property.
     * @param value allowed object is {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    protected void setDIRITTIPOSTALI(BigInteger value) {
        this.dirittipostali = value;
    }

    /**
     * Gets the value of the importocontrassegno property.
     * @return  possible object is {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    public BigInteger getIMPORTOCONTRASSEGNO() {
        return importocontrassegno;
    }

    /**
     * Sets the value of the importocontrassegno property.
     * @param value allowed object is {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    protected void setIMPORTOCONTRASSEGNO(BigInteger value) {
        this.importocontrassegno = value;
    }

    /**
     * Gets the value of the oneridoganali property.
     * @return possible object is {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    public BigInteger getONERIDOGANALI() {
        return oneridoganali;
    }

    /**
     * Sets the value of the oneridoganali property.
     * @param value allowed object is {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     */
    protected void setONERIDOGANALI(BigInteger value) {
        this.oneridoganali = value;
    }

    /**
     * Gets the value of the note property.
     * @return possible object is {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    public String getNOTE() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * @param value allowed object is {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    protected void setNOTE(String value) {
        this.note = value;
    }

}
