package com.elsagdatamat.dbtrace.bridge.engine.dao;
 
import com.elsagdatamat.dbtrace.bridge.engine.entities.FromTo;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface IFromToDAO  extends IGenericDAO<FromTo, String>{

}
