package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.TagToConvertSubP;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class TagToConvertSubPDAO extends SpringHibernateJpaDAOBase<TagToConvertSubP,String> implements ITagToConvertSubPDAO{

}
