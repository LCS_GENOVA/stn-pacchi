package com.selexelsag.xte.business.test;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeFactory;

import org.apache.commons.lang3.StringEscapeUtils;
import org.junit.Test;

import com.selexelsag.xte.business.tracking.TrackTraceConstants;
import com.selexelsag.xte.business.xml.entities.ObjectFactory;
import com.selexelsag.xte.business.xml.entities.TMessage;
import com.selexelsag.xte.business.xml.entities.TPayload;
import com.selexelsag.xte.business.xml.entities.TrackMessage;
import com.selexelsag.xte.business.xml.entities.TrackMessageResponse;
import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.model.track.TrackResponse;

public class MarshallerTest {
	
	@Test
	public void test() {
	String trackData = "&lt;Traccia channelID=\"38001\" channel=\"TP\" eventName=\"NM\" serviceName=\"DP\" version=\"1.1\" Evento=\"NotificaMazzetto\"&gt;" +
		"&lt;HeaderMazzetto&gt;"+
		"&lt;CodiceIdentificativo/&gt;"+
		"&lt;IdOperatoreTP&gt;Mario Rossi&lt;/IdOperatoreTP&gt;"+
		"&lt;/HeaderMazzetto&gt;"+
		"&lt;ListaInvii&gt;"+
		"&lt;DatiInvio&gt;"+
		"&lt;Codice&gt;VD90123456ZCUT116011    401311234BCA9552536FSJ   BO0001                 &lt;/Codice&gt;"+
		"&lt;TipoCodice&gt;N&lt;/TipoCodice&gt;"+
		"&lt;Causale&gt;DP1&lt;/Causale&gt;"+
		"&lt;DataOraNotifica&gt;2015-04-14T14:55:18.000+02:00&lt;/DataOraNotifica&gt;"+
		"&lt;Latitudine&gt;1.1&lt;/Latitudine&gt;"+
		"&lt;Longitudine&gt;1.1&lt;/Longitudine&gt;"+
		"&lt;CoordinateGPSCorrenti&gt;N&lt;/CoordinateGPSCorrenti&gt;"+
		"&lt;/DatiInvio&gt;"+
		"&lt;/ListaInvii&gt;"+
		"&lt;/Traccia&gt;";
		TrackRequest tr = new TrackRequest();
		tr.setService(new StringBuilder("DP"));
		tr.setChannel(new StringBuilder("TP"));
		tr.setEventName(new StringBuilder("NM"));
		tr.setVersion(new StringBuilder("1.1"));
		tr.setTrackData(new StringBuilder(trackData));
		tr.setSourceOfficeId(new StringBuilder("boh"));
		
		TrackResponse tresp = new TrackResponse();
		tresp.setChannel("TP");
		tresp.setErrorCode("Positivo");
		tresp.setErrorDesc("Traccia acquisita con successo");
		tresp.setErrorId("0");
		tresp.setEventName("NM");
		
		try {
			getProcessedMessage(tr, tresp);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private String getProcessedMessage(TrackRequest trackRequest, TrackResponse trackResponse) throws Exception
	{
		ObjectFactory of = new ObjectFactory();
		
		// TMessage element
		TMessage tMessage = new TMessage();
		tMessage.setData(DatatypeFactory.newInstance().newXMLGregorianCalendar(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(new Date())));

		// TPayload element
		TPayload tPayload = new TPayload();

		// TrackMessage element		
		TrackMessage trackMessage = new TrackMessage();
		
		if(trackRequest.getChannel()!=null){
			trackMessage.setChannel(trackRequest.getChannel().toString());
			
		}else{
			trackMessage.setChannel(null);
		}
		
		if(trackRequest.getSourceOfficeId()!=null){
			trackMessage.setChannelID(trackRequest.getSourceOfficeId().toString());
			
		}else{
			trackMessage.setChannelID(null);
		}
		
		if(trackRequest.getEventName()!=null){
			trackMessage.setEventName(trackRequest.getEventName().toString());
			
		}else{
			trackMessage.setEventName(null);
		}
		
		if(trackRequest.getService()!=null){
			trackMessage.setServiceName(trackRequest.getService().toString());
			
		}else{
		trackMessage.setServiceName(null);
		}
		
		if(trackRequest.getTrackData()!=null){
			trackMessage.setTrackData(trackRequest.getTrackData().toString());
			
		}else{
		trackMessage.setTrackData(null);
			
		}
		
		if(trackRequest.getVersion()!=null){
			trackMessage.setVersion(trackRequest.getVersion().toString());
			
			
		}else{
		trackMessage.setVersion(null);
			
		}
		
		
		tPayload.setTrackMessage(trackMessage);
		
		// TrackMessageResponse element
		TrackMessageResponse trackMessageResponse = new TrackMessageResponse();
		
		com.selexelsag.xte.business.xml.entities.TrackResponse tr = new com.selexelsag.xte.business.xml.entities.TrackResponse();
		tr.setChannel(trackResponse.getChannel());
		tr.setChannelId(trackResponse.getSourceOfficeId());
		tr.setErrorCode(trackResponse.getErrorCode());
		tr.setErrorDesc(trackResponse.getErrorDesc());
		tr.setErrorId(trackResponse.getErrorId());
		tr.setEventName(trackResponse.getEventName());
		tr.setRetryFlag(trackResponse.getRetryFlag());
		tr.setServiceName(trackResponse.getServiceName());
		//tr.setOutputParam(trackResponse.getOutputParam());
		tr.setOutputParam(StringEscapeUtils.unescapeXml(trackResponse.getOutputParam()));
				
		trackMessageResponse.setReturnTrackMessage(tr);
	
		tPayload.setTrackMessageResponse(trackMessageResponse);
		
		tMessage.setPayload(tPayload);
		
		JAXBElement<TMessage> jb = of.createInputMessage(tMessage);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JAXBContext jc;
		try 
		{
			 jc = JAXBContext.newInstance(TMessage.class);
			 Marshaller marshaller = jc.createMarshaller();
			 marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			 marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			 marshaller.marshal(jb, baos);
		}
		catch (JAXBException e) 
		{
			throw e;
		}
		String trasf =  baos.toString();
		return trasf;
	}
}
