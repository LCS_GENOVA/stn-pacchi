package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.dbtrace.bridge.engine.CustomTraceEntity;
import com.elsagdatamat.dbtrace.bridge.engine.IDbTraceBridge;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItem;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition.AptStatusEnum;
import com.elsagdatamat.dbtrace.bridge.engine.exceptions.InsertTracesException;
import com.elsagdatamat.dbtrace.bridge.engine.exceptions.TrackDBRWException;
import com.elsagdatamat.framework.entities.EntityCreationException;
import com.elsagdatamat.framework.entities.IEntityBuilder;
import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.elsagdatamat.tt.trackdbws.beans.TraceDetail;
import com.elsagdatamat.tt.trackdbws.bl.ITraceBL;
import com.selexelsag.tracktrace.extension.managers.AdditionalServiceManager;
import com.selexelsag.tracktrace.extension.managers.MailpieceManager;
import com.selexelsag.tt.dbtrace.bridge.sender.impl.MessageDestination;
import com.selexelsag.tt.dbtrace.bridge.sender.update.core.ProductKey;

public class DbTraceBridgeBL<T> implements IDbTraceBridge<T> {

	private static final String PARAMETERS_DETAIL_CLASS = "PARAMETERS";
	private static final String ADDITIONAL_SERVICE_DETAIL_CLASS = "SA";
	// R.L. - 05.2012
	private MessageDestination messageDestination;
	private boolean flagSender;
	private AdditionalServiceManager additionalServiceManager;
	private MailpieceManager mailpieceManager;
	// R.L. - 062014 - chioschi (scrittura su SC per i messaggi PRE_SDA di sda)
	private AptItemBL aptItemBL;

	private AptItemPositionBL aptItemPositionBL;

	protected static Log log = LogFactory.getLog(DbTraceBridgeBL.class);

	private IEntityBuilder<T, List<CustomTraceEntity>> traceBuilder;

	private ITraceBL traceBL;

	public AptItemBL getAptItemBL() {
		return aptItemBL;
	}

	public void setAptItemBL(AptItemBL aptItemBL) {
		this.aptItemBL = aptItemBL;
	}

	public AptItemPositionBL getAptItemPositionBL() {
		return aptItemPositionBL;
	}

	public void setAptItemPositionBL(AptItemPositionBL aptItemPositionBL) {
		this.aptItemPositionBL = aptItemPositionBL;
	}

	public MailpieceManager getMailpieceManager() {
		return mailpieceManager;
	}

	public void setMailpieceManager(MailpieceManager mailpieceManager) {
		this.mailpieceManager = mailpieceManager;
	}

	public ITraceBL getTraceBL() {
		return traceBL;
	}

	public AdditionalServiceManager getAdditionalServiceManager() {
		return additionalServiceManager;
	}

	public void setAdditionalServiceManager(AdditionalServiceManager additionalServiceManager) {
		this.additionalServiceManager = additionalServiceManager;
	}

	public void setTraceBL(ITraceBL traceBL) {
		this.traceBL = traceBL;
	}

	public void setTraceBuilder(IEntityBuilder<T, List<CustomTraceEntity>> traceBuilder) {
		this.traceBuilder = traceBuilder;
	}

	public IEntityBuilder<T, List<CustomTraceEntity>> getTraceBuilder() {
		return traceBuilder;
	}

	public MessageDestination getMessageDestination() {
		return messageDestination;
	}

	public void setMessageDestination(MessageDestination messageDestination) {
		this.messageDestination = messageDestination;
	}

	public void init() {
		log.info("Initializing DbTraceBridgeManager...");

		if (traceBL == null)
			throw new NullPointerException("traceBL not set");
		if (traceBuilder == null)
			throw new NullPointerException("traceBuilder not set");

		// Aggiunt R.L. - CruscottoPacchi - 05.2012
		// if (messageDestination == null)
		// throw new NullPointerException("messageDestination not set");

		log.info("DbTraceBridgeManager initialized");
	}

	@Override
	public void insertTraces(T input) throws InsertTracesException, TrackDBRWException {

		if (input == null)
			throw new IllegalArgumentException("Input is null");

		log.info("Message received: " + input.toString());

		List<CustomTraceEntity> lst;
		try {
			synchronized (traceBuilder) {
				lst = traceBuilder.create(input);
			}
		} catch (EntityCreationException e) {
			throw new InsertTracesException("Impossibile creare le tracce", e);
		}

		if (lst.isEmpty()) {
			throw new InsertTracesException("Nessuna traccia estraibile dall'input");
		}

		// R.L. - CRUSCOTTOPACCHI - 05.2012
		// Vecchia versione - senza l'evoluzione sul cruscotto pacchi
		// long lResult;
		// try {
		// lResult = traceBL.insertTrace(createObjs(lst));
		// } catch (Exception e) {
		// throw new
		// TrackDBRWException("Errore durante l'inserimento delle tracce", e);
		// }
		//
		// if (lResult < lst.size()) {
		// log.info("Inserite " + lResult + " su " + lst.size() +
		// " tracce previste");
		// }

		// Nuova versione:
		// dopo aver tentato di inserire le tracce seguendo il flusso standard
		// degli scodatori fromXXX, chiamo un metodo che mappa
		// le informazioni di TRACE e TRACEDETAILS in un nuovo bean serializzato
		// da inviaread una coda con un messaggio di tipo objectMessage.
		// Tale informazione sar� scodata e gestita da uno scodatore per il
		// cruscotto pacchi.
		flagSender = false;
		if (messageDestination != null)
			flagSender = messageDestination.isFlagSender(); // Controllo
		// sull'abilitazione del canale - true invia tracce alla coda, false altrimenti

		long lResult = 0;
		long lSent = 0;
		List<Trace> traces = createObjs(lst);
		// R.L. - 30.01.2013 - modifica al modulo che invia i messaggi sulla
		// coda per il cruscotto pacchi. Non invia pi� un messaggio per ogni
		// traccia ma una lista di messaggi per ogni xml inviato da SDA, TT,
		// NSP, OMP ...
		ArrayList<com.selexelsag.tt.dbtrace.bridge.serial.object.Trace> serialTraceArrayList = null;
		// com.selexelsag.tt.dbtrace.bridge.serial.object.Trace � l'oggetto da inviare sulle code per il cruscotto pacchi
		try {
			serialTraceArrayList = new ArrayList<com.selexelsag.tt.dbtrace.bridge.serial.object.Trace>(0);

			for (Trace trace : traces) {

				//Modificato - Nello - gen2016 per gestione RTZ
				if (WhatHappenedNotOnDBT.contains(trace.getWhatHappened())) {
					AptItem item = getAptItemFromTrace(trace);
					if (item.getSubPtype() == null) {
						log.error("PRE_SDA: Non esiste SUbPType per il pacco [" + item.getCode() + "] - oggetto non inserito su APT_ITEM");
					} else {
						try{
							//verifico se presente il frazionario da inserire in AptItemPosition
							String fraz = trace.getIdChannel();
							if (fraz != null) {
								log.info("Pacco RTZ. Codice ["+item.getCode()+"] Fraz ["+fraz+"]");
								AptItemPosition itemPosition = getAptItemPositionFromTrace(trace);
								AptItemPosition itemPositionF = aptItemPositionBL.find(trace.getIdTracedEntity());
								if (itemPositionF == null) {
									insertAptItemAndAptItemPositionPreSDA(item, itemPosition);
									log.info("Inseriti o updatati dati per un pacco. Codice ["+item.getCode()+"] fraz presente, itemPos == null - aggiornato frazionario.");
								} else {
									aptItemBL.insertOrUpdate(item);
									log.info("Inseriti o updatati dati per un pacco. Codice ["+item.getCode()+"] fraz presente ma non aggiornato.");
								}
							} else {
								aptItemBL.insertOrUpdate(item);
								log.info("Inseriti o updatati dati per un pacco. Codice ["+item.getCode()+"] fraz non presente.");
							}
						} catch (RuntimeException exc){
							log.error(String.format("Errore per l'oggetto AptItem codice %s", item.getCode()),exc);
						} catch (Exception exc){
							log.error(String.format("Errore per l'oggetto AptItem codice %s", item.getCode()),exc);
						}
					}
				} else { // se non � di tipo pre_sda allora inseriscilo sul DBT
					//Modificato - Nello - gen2016 per gestione RTZ
					boolean insertedT = traceBL.insertTrace(trace);
					try {
						if (WhatHappenedOnDBTOnTT.contains(trace.getWhatHappened())) {
							String WHERE_DESCRIPTION = trace.getDetailList().get("WHERE_DESCRIPTION");
							if (WHERE_DESCRIPTION != null && WHERE_DESCRIPTION.equals("RTZ")) {
								log.info("Pacco RTZ. Codice ["+trace.getIdTracedEntity()+"] WhatHappened ["+trace.getWhatHappened()+"]");
								AptItem item = getAptItemNoPreSdaFromTrace(trace);
								AptItemPosition itemPosition = getAptItemPositionFromTrace(trace);
								if (itemPosition.getFrazionarioGeopost() != null) {
									insertAptItemAndAptItemPositionUfiSDA(item, itemPosition);
									log.info("Inseriti o aggiornati AptItem e AptItemPosition per un pacco RTZ. Codice ["+item.getCode()+"]");
								}
							}
						}
					} catch (RuntimeException exc){
						log.error("Errore per l'oggetto con codice %s durante Inserimento o aggiornamento AptItem e AptItemPosition Pacco RTZ. Codice ["+trace.getIdTracedEntity()+"] WhatHappened ["+trace.getWhatHappened()+"]",exc);
					} catch (Exception exc){
						log.error("Errore per l'oggetto con codice %s durante Inserimento o aggiornamento AptItem e AptItemPosition Pacco RTZ. Codice ["+trace.getIdTracedEntity()+"] WhatHappened ["+trace.getWhatHappened()+"]",exc);
					}
					if (insertedT) {
						lResult++;
						// R.L. - se non deve essere inviato nessun messaggio alla
						// coda del cruscotto pacchi allora si passa alla traccia
						// successiva
						// altrimenti si aprono due strade:
						// se la traccia viene correttamente persistita sul DB, la
						// stessa viene anche contertita (serializzata) ed inviata
						// alla coda che alimenta i dati per il cruscotto pacchi
						// mentre se la persistenza genera qalche eccezione
						// (DataIntegrityViolation, ConstraintViolationException,
						// PersistenceException ... ) la traccia non viene inviata
						// al cuscotto pacchi
						// if(flagSender)
						// if(sendSerialTrace(trace))
						// ++lSent;

					// R.L. - 30.01.2013 - modifica per inviare una lista di
					// traccia invece che una sola traccia per volta
					if (flagSender) {
						ProductKey key = new ProductKey(trace.getLabelTracedEntity(), trace.getChannel(), trace.getWhatHappened());
						boolean isAttivata = false;
						if (messageDestination != null)
							isAttivata = messageDestination.getProductConfig(key);

							if (isAttivata) {
								serialTraceArrayList.add(convertTrace2SerialTrace(trace));
								++lSent;
							}
						}
					}
				}
			}
			// R.L. - 30.01.2013 - il metodo per inviare le tracce (in formato
			// lista) viene spostato dopo il ciclo (che costruisce la lista)
			if (flagSender && serialTraceArrayList != null && serialTraceArrayList.size() > 0)
				sendListOfSerialTrace(serialTraceArrayList);
		} catch (Exception e) {
			throw new TrackDBRWException("Errore durante l'inserimento delle tracce", e);
		}

		// if (lResult <= lst.size())
		log.info("Inserite [" + lResult + "]. Inviate in una sola lista [" + lSent + "]. Previste [" + lst.size() + "]");

	}

	private AptItem getAptItemNoPreSdaFromTrace(Trace trace) {
		AptItem item = new AptItem();

		//2015/02/09 D.P. tronco i caratteri a 13
		String code = trace.getIdTracedEntity();
		if (code.length()==16){
			code = code.substring(3);
		}

		item.setCode(code);
		//ST_TTRACE-8800 - TT 16.0.7 - CL&CO - UFI_SDA APT_ITEM_POSITION
		//aggiunti i due campi non nullable per errore insert in assenza di PRE_SDA
		item.setSubPtype(aptItemBL.getSubPType(item.getCode()));
		item.setPtype(trace.getLabelTracedEntity());

		return item;
	}

	private AptItemPosition getAptItemPositionFromTrace(Trace trace) {
		AptItemPosition itemPosition = new AptItemPosition();

		//tronco i caratteri a 13
		String code = trace.getIdTracedEntity();
		if (code.length()==16){
			code = code.substring(3);
		}

		itemPosition.setCode(code);
		String fraz = trace.getIdChannel();
		itemPosition.setFrazionarioGeopost(fraz);
		itemPosition.setStatus(AptStatusEnum.COURIER);
		itemPosition.setTimestamp(new Date());

		return itemPosition;
	}

	public void insertAptItemAndAptItemPositionUfiSDA(AptItem item, AptItemPosition itemPosition) {
		try{
			AptItem itemS = aptItemBL.find(item.getCode());

			if (itemS == null) {
				AptItemPosition itemPositionF = aptItemPositionBL.find(item.getCode());
				if (itemPositionF == null) {
					aptItemBL.insertAptItemAndAptItemPosition(item, itemPosition);
					log.info("Inseriti dati nelle tabelle aptItem e aptItemPosition per un pacco RTZ. Codice ["+item.getCode()+"]");
				} else {
					aptItemBL.insertAptItemAndUpdateAptItemPosition(item, itemPosition);
					log.info("Inseriti dati nella tabella aptItem e aggiornata aptItemPosition per un pacco RTZ. Codice ["+item.getCode()+"]");
				}
			} else {
				AptItemPosition itemPositionF = aptItemPositionBL.find(item.getCode());
				if (itemPositionF == null) {
					aptItemPositionBL.insert(itemPosition);
					log.info("Inseriti dati nella tabella aptItemPosition per un pacco RTZ. Codice ["+item.getCode()+"]");
				} else {
					itemPositionF.setFrazionarioGeopost(itemPosition.getFrazionarioGeopost());
					aptItemPositionBL.update(itemPositionF);
					log.info("Aggiornata tabella aptItemPosition per un pacco RTZ. Codice ["+item.getCode()+"]");
				}
			}
			log.info("Inseriti o updatati dati nelle tabelle aptItem e aptItemPosition per un pacco RTZ. Codice ["+item.getCode()+"]");
		} catch (RuntimeException exc){
			log.error(String.format("Errore per l'inserimento degli oggetti in AptItem e AptItemPosition codice %s", item.getCode()),exc);
		} catch (Exception exc){
			log.error(String.format("Errore per l'inserimento degli oggetti in AptItem e AptItemPosition codice %s", item.getCode()),exc);
		}
	}

	public void insertAptItemAndAptItemPositionPreSDA(AptItem item, AptItemPosition itemPosition) {
		try{
			aptItemBL.insertAptItemAndAptItemPosition(item, itemPosition);
		} catch (RuntimeException exc){
			log.error(String.format("Errore per l'inserimento degli oggetti in AptItem e AptItemPosition codice %s", item.getCode()),exc);
		} catch (Exception e) {
			log.error(String.format("Errore per l'inserimento degli oggetti in AptItem e AptItemPosition codice %s", item.getCode()),e);
		}
	}

	private static Boolean servicesInCode(CustomTraceEntity entity){
		Boolean res="PACCO".equals(entity.getTracedEntity()) && entity.getIdTracedEntity()!=null && entity.getIdTracedEntity().length() == 16;
		log.debug("servicesInCode for "+entity.getTracedEntity()+" "+entity.getIdTracedEntity()+" "+res);
		return res;
	}
	protected Trace createObj(CustomTraceEntity entity) {
		Trace obj = new Trace();
		BeanUtils.copyProperties(entity, obj);
		obj.setInternalDetailList(new HashSet<TraceDetail>(0));
		String service = entity.getDetailList().get(ADDITIONAL_SERVICE_DETAIL_CLASS);
		String param = entity.getDetailList().get(PARAMETERS_DETAIL_CLASS);
		if ( servicesInCode(entity)) {
			List<String> generateServiceList = generateServiceList(entity.getIdTracedEntity(), service, obj.getChannel());
			param = generateParams(generateServiceList, param);
			service = additionalServiceManager.encodeTraceListServices(generateServiceList);
			service = service.replaceFirst(";", "");
			obj.setIdTracedEntity(obj.getIdTracedEntity().substring(3));
		}

		if (service != null && service.length() > 0)
			addDetail(ADDITIONAL_SERVICE_DETAIL_CLASS, service, obj);
		if (param != null && param.length() > 0)
			addDetail(PARAMETERS_DETAIL_CLASS, param, obj);
		for (TraceDetail traceDetail : entity.getInternalDetailList()) {
			if (!traceDetail.getParamClass().equals(ADDITIONAL_SERVICE_DETAIL_CLASS)
					&& !traceDetail.getParamClass().equals(PARAMETERS_DETAIL_CLASS))
				addDetail(traceDetail.getParamClass(), traceDetail.getParamValue(), obj);
		}
		return obj;
	}

	private static void addDetail(String pClass, String value, Trace obj) {
		if (value == null || value.trim().equals("")) {
			return;
		}
		TraceDetail newTraceDetail = new TraceDetail();
		newTraceDetail.setParamClass(pClass);
		newTraceDetail.setParamValue(value);
		newTraceDetail.setTrace(obj);
		obj.getInternalDetailList().add(newTraceDetail);
	}

	protected List<Trace> createObjs(List<CustomTraceEntity> entities) {
		List<Trace> objs = new ArrayList<Trace>(entities.size());
		for (CustomTraceEntity entity : entities)
			objs.add(createObj(entity));
		return objs;
	}

	// AGGIUNTO R.L. - CruscottoPacchi - 05.2012
	/**
	 * Metodo che invia una traccia alla coda che carica i dati per il cruscotto pacchi. La traccia arriva a questo metodo soltanto se �
	 * stata correttametne persistita sul DB
	 *
	 * Prima di inviare la traccia vengono effettuati due controlli: Il primo riguarda il flag sender: indica se e' abilitato il canale di
	 * comunicazione con la coda per il cruscotto pacchi. Vale <b>true</b> se il canale � attivo, <b>false</b> altrimenti. Il parametro �
	 * letto dal DB (tale controllo � stato implementato nel chiamante - valore del flagSender). Il secondo controllo � invece relativo alla
	 * configurazione dei prodotti. Superato il primo controllo si fa una cernita delle tracce da inviare al cruscotto. Solo quelle di
	 * accettazione piuttosto che di smistamento ecc.
	 *
	 * Il flag sender e la configurazione dei prodotti vengono lette da db ad intervalli regolari e configurabili.
	 *
	 */
	// R.L. - 30.01.2013 modificato per inviare una lista di tracce e non una
	// traccia come capitava prima
	// private boolean sendSerialTrace(Trace trace){
	//
	// ProductKey key = new ProductKey(trace.getLabelTracedEntity(),
	// trace.getChannel(), trace.getWhatHappened());
	// boolean isAttivata= messageDestination.getProductConfig(key);
	//
	// if(!isAttivata) return false;
	//
	// try{
	// com.selexelsag.tt.dbtrace.bridge.serial.object.Trace serialTrace =
	// convertTrace2SerialTrace(trace);
	// messageDestination.send(serialTrace,"DbTraceBridge.DbTraceBridgeBL");
	// } catch (Exception e){
	// log.error("Problemi nell'invio della traccia al CruscottoPacchi - ObjectQueue",e);
	// return false;
	// }
	// return true;
	// }

	private boolean sendListOfSerialTrace(ArrayList<com.selexelsag.tt.dbtrace.bridge.serial.object.Trace> serialTraceList) {

		try {
			messageDestination.send(serialTraceList, "DbTraceBridge.DbTraceBridgeBL-SerialList");
		} catch (Exception e) {
			log.error("Problemi nell'invio della traccia al CruscottoPacchi - ObjectQueue", e);
			return false;
		}
		return true;
	}

	/**
	 * Questo metodo mappa una traccia memorizzata in trace e tracedetails (entioty di hibernate) in una struttura simmetrica che poi sara'
	 * inviata su una coda jms - quella relativa al cruscotto pacchi
	 *
	 * @param Trace
	 *            - entity del trackDbWs
	 * @return Trace - entity serializzata per la objectQueue
	 */
	private com.selexelsag.tt.dbtrace.bridge.serial.object.Trace convertTrace2SerialTrace(Trace trace) {
		com.selexelsag.tt.dbtrace.bridge.serial.object.Trace serialTrace = new com.selexelsag.tt.dbtrace.bridge.serial.object.Trace();
		serialTrace.setTracedEntity(trace.getTracedEntity());
		serialTrace.setIdTracedEntity(trace.getIdTracedEntity());
		serialTrace.setWhatHappened(trace.getWhatHappened());
		serialTrace.setWhenHappened(trace.getWhenHappened());
		serialTrace.setWhenRegistered(trace.getWhenRegistered());
		serialTrace.setWhereHappened(trace.getWhereHappened());
		serialTrace.setChannel(trace.getChannel());
		serialTrace.setIdChannel(trace.getIdChannel());
		serialTrace.setServiceName(trace.getServiceName());
		serialTrace.setLabelTracedEntity(trace.getLabelTracedEntity());
		serialTrace.setIdStatus(trace.getIdStatus());
		serialTrace.setIdCorrelazione(trace.getIdCorrelazione());
		serialTrace.setIdTracedExternal(trace.getIdTracedExternal());
		serialTrace.setIdForwardTo(trace.getIdForwardTo());
		serialTrace.setSysForwardTo(trace.getSysForwardTo());

		Set<TraceDetail> tds = trace.getInternalDetailList();
		Set<com.selexelsag.tt.dbtrace.bridge.serial.object.TraceDetail> serialTds = new HashSet<com.selexelsag.tt.dbtrace.bridge.serial.object.TraceDetail>(0);
		for (TraceDetail traceDetail : tds) {
			com.selexelsag.tt.dbtrace.bridge.serial.object.TraceDetail serialTd = new com.selexelsag.tt.dbtrace.bridge.serial.object.TraceDetail();
			serialTd.setParamClass(traceDetail.getParamClass());
			serialTd.setParamValue(traceDetail.getParamValue());
			serialTds.add(serialTd);
		}
		serialTrace.setInternalDetailList(serialTds);

		return serialTrace;
	}

	private List<String> generateServiceList(String code, String services, String channel) {
		additionalServiceManager.decodeTraceListServices(services);
		List<String> decodedServices = additionalServiceManager.decodeTraceListServices(services);
		decodedServices.addAll(additionalServiceManager.findAdditionalServiceByDictionary(code.substring(0, 3)));
		return decodedServices;
	}

	private String generateParams(List<String> decodedServices, String params) {
		Set<String> servicesSet = new HashSet<String>();
		servicesSet.addAll(decodedServices);
		if (params == null)
			params = "";
		return mailpieceManager.setDefaultForCustomData(servicesSet, params);
	}

	private AptItem getAptItemFromTrace(Trace trace){
		AptItem item = new AptItem();

		//2015/02/09 D.P. tronco i caratteri a 13
		String code = trace.getIdTracedEntity();
		if (code.length()==16){
			code = code.substring(3);
		}

		//item.setItemCode(trace.getIdTracedEntity());
		item.setCode(code);
		item.setSubPtype(aptItemBL.getSubPType(item.getCode()));
		item.setPtype(trace.getLabelTracedEntity());
		Map<String, String> details = trace.getDetailList();
		//ST_TTRACE-5934 - TT 15.1 - CR APT - Messaggio PRE_SDA e campi numerici nulli
		if (details.get("LUNGHEZZA") != null)
			item.setDepth(Integer.valueOf(details.get("LUNGHEZZA")));
		else
			item.setDepth(new Integer(0));
		if (details.get("LARGHEZZA") != null)
			item.setWidth(Integer.valueOf(details.get("LARGHEZZA")));
		else
			item.setWidth(new Integer(0));
		if (details.get("ALTEZZA") != null)
			item.setHeight(Integer.valueOf(details.get("ALTEZZA")));
		else
			item.setHeight(new Integer(0));
		if (details.get("PESO") != null)
			item.setWeight(Integer.valueOf(details.get("PESO")));
		else
			item.setWeight(new Integer(0));
		item.setReceiverName(details.get("DESTR"));
		item.setAddressee(details.get("DESTR"));
		item.setReceiverEmail(details.get("EMAIL"));
		item.setReceiverPhoneNumber(details.get("NUM_TEL"));
		return item;
	}
}
