/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.message.trans;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

import javax.jms.BytesMessage;
import javax.jms.JMSException;

/**
 * @author arodriguez
 * 
 */
public class BytesMessageOutputStream extends OutputStream implements Serializable {

	private BytesMessage message;

	/**
	 * 
	 */
	private BytesMessageOutputStream() {
		super();
	}

	/**
	 * @param message
	 */
	public BytesMessageOutputStream(BytesMessage message) {
		this.message = message;
	}

	@Override
	public void write(int b) throws IOException {
		try {
			this.message.writeInt(b);
		} catch (JMSException ex) {
			throw new IOException(ex);
		}
	}

	@Override
	public void write(byte[] buf) throws IOException {
		try {
			this.message.writeBytes(buf);
		} catch (JMSException ex) {
			throw new IOException(ex);
		}
	}

	@Override
	public void write(byte[] buf, int off, int len) throws IOException {
		try {
			this.message.writeBytes(buf, off, len);
		} catch (JMSException ex) {
			throw new IOException(ex);
		}
	}
}
