package com.elsagdatamat.dbtrace.bridge.totgc.manager;

import java.util.List;

import com.elsagdatamat.dbtrace.bridge.totgc.entities.TraceTypesToSend;

public interface ITraceTypesToSendManager {

	public List<TraceTypesToSend> findByDestinationTracedEntityChannel(String destination,String tracedEntity,String channel);
}
