package com.selexelsag.xte.persistence.dao;


import org.springframework.transaction.annotation.Transactional;

import com.selexelsag.xte.persistence.entities.AsyncProcessed;

@Transactional
public class AsyncProcessedDAO extends BaseDAO<AsyncProcessed>
{
	//private static final Log log = LogFactory.getLog(AsyncProcessedDAO.class);
	
	public AsyncProcessedDAO() 
	{
		super(AsyncProcessed.class);
	}
}
