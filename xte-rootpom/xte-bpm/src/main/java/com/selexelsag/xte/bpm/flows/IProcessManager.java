package com.selexelsag.xte.bpm.flows;

import org.drools.runtime.StatefulKnowledgeSession;

import com.selexelsag.xte.bpm.exceptions.BpmException;
import com.selexelsag.xte.model.flows.FlowParams;

/**
 * Il Process manager gestisce i processi eseguibili per mezzo 
 * del FlowExecutor
 * @author Motta
 *
 */
public interface IProcessManager {

	/**
	 * Interroga la mappa di associazione fra flowId e processi
	 * @param flowParams l'identificativo del flusso che si vuole processare
	 * @return l'id processo che gestisce il dato flusso
	 * @throws BpmException in caso di errori nel recupero del processId
	 */
	public String getProcessId(FlowParams flowParams) throws BpmException;
	
	
	/**
	 * ritorna l'oggetto sessione JBPM per eseguire i flussi
	 * @return
	 */
	public StatefulKnowledgeSession getKsession();
	
}
