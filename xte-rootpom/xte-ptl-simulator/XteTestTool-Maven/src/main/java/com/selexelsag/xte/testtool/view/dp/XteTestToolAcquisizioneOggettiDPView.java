/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.dp;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.view.XteTestToolPanelView;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolMailPieceCodeDocument;
import com.selexelsag.xte.testtool.view.objectmodel.XteListSelectionModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteAcquisizioneOggettiTableModel;
import com.selexelsag.xte.testtool.view.objectrenderer.XteMailPieceTableCellRenderer;
import com.selexelsag.xte.testtool.view.objectrenderer.XteTableHeaderRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.PlainDocument;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author Tassara
 */
public class XteTestToolAcquisizioneOggettiDPView extends XteTestToolPanelView{
    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolAcquisizioneOggettiDPView.class);
    private XteAcquisizioneOggettiTableModel xteAcquisizioneOggettiTableModel = new XteAcquisizioneOggettiTableModel();;
    
    public XteTestToolAcquisizioneOggettiDPView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try {
            logger.info("XteTestToolAcquisizioneOggettiDPView start class creation");
            this.messages = ResourceBundle.getBundle("bundles/messages");
            initComponents();
            logger.info("XteTestToolAcquisizioneOggettiDPView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolAcquisizioneOggettiDPView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Data Posta view creation failed", ex);
        }
    }

    public JPanel getAcquisizioneOggettiPanelRef(){ return this; }        
    public JLabel getAcquisizioneOggettiTitleLabelRef(){ return this.panelTitlejLabel; }
    
    public JPanel getBundleDataPaneRef(){ return this.bundleDatajPanel; }
    public JTextField getBundleIdTextRef(){ return this.bundleIdjTextField; }
    public JTextField getBundleTPIdTextRef(){ return this.bundleTPIdjTextField; }
    
    public JPanel getAcquisizioneOggettoDataPaneRef(){ return this.AcquisizioneOggettoDatajPanel; }
    public JComboBox getAcquisizioneOggettoTypeComboBoxRef(){ return this.AcquisizioneOggettoTypejComboBox; }
    public JTextField getAcquisizioneOggettoCodeTextRef(){ return this.AcquisizioneOggettoCodejTextField; }
    public JTextField getAcquisizioneOggettoOfficeTextRef(){ return this.AcquisizioneOggettoOfficejTextField; }
    public JXDatePicker getAcquisizioneOggettoDatePickerRef(){ return this.AcquisizioneOggettoDatejXDatePicker; }
    public JSpinner getAcquisizioneOggettoTimeSpinnerRef(){ return this.AcquisizioneOggettoTimejSpinner; }
    public JTable getAcquisizioneOggettoTableRef(){ return this.AcquisizioneOggettojTable; }
    public XteAcquisizioneOggettiTableModel getAcquisizioneOggettoTableModelRef(){ return (XteAcquisizioneOggettiTableModel) this.AcquisizioneOggettojTable.getModel(); }
    public JPanel getAcquisizioneOggettoDataCommandPaneRef(){ return this.mailPieceDataCommandjPanel; }
    public JButton getAggiungiItemButtonRef(){ return this.addAcquisizioneOggettojButton; }
    public JButton getEliminaItemButtonRef(){ return this.removeAcquisizioneOggettojButton; }
    
    public JPanel getXmlDataPaneRef(){ return this.xmlDatajPanel; }
    public JScrollPane getXmlDataScrollPaneRef(){ return this.xmlDatajScrollPane; }
    public JTextArea getXmlDataTextAreaRef(){ return this.xmlDatajTextArea; }
    public JPanel getXmlDataCommandPaneRef(){ return this.xmlDataCommandjPanel; }    
    public JButton getGeneraXmlDataButtonRef(){ return this.generaXmlDatajButton; }
    public JButton getImpostaDatiWSButtonRef(){ return this.impostaDatiWSjButton; }
    public JButton getSalvaXmlDataButtonRef(){ return this.salvaXmlDatajButton; }
    
    public JButton getBackButtonRef(){ return this.backjButton; }
    public JButton getHomeButtonRef(){ return this.homejButton; }
    public JButton getExitButtonRef(){ return this.exitjButton; }
    public JTextPane getMessageBarTextPaneRef(){ return this.messageBarjTextPane; }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolAcquisizioneOggettiDPView start customizeView");
            //CUSTOMIZE TITLE
            this.panelTitlejLabel .setText(messages.getString("dataposta.acquisizioneoggetti.view.title.label"));

            //CUSTOMIZE BUTTON
            this.addAcquisizioneOggettojButton.setText(messages.getString("dataposta.acquisizioneoggetti.view.additem.button"));
            this.removeAcquisizioneOggettojButton.setText(messages.getString("dataposta.acquisizioneoggetti.view.removeitem.button"));
            this.generaXmlDatajButton.setText(messages.getString("dataposta.acquisizioneoggetti.view.generaxmldata.button"));
            this.impostaDatiWSjButton.setText(messages.getString("dataposta.acquisizioneoggetti.view.impostadatiws.button"));
            this.salvaXmlDatajButton.setText(messages.getString("dataposta.acquisizioneoggetti.view.salvaxmldata.button"));
            this.backjButton.setText(messages.getString("view.back.button"));
            this.homejButton.setText(messages.getString("view.home.button"));
            this.exitjButton.setText(messages.getString("view.exit.button"));

            logger.info("XteTestToolAcquisizioneOggettiDPView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolAcquisizioneOggettiDPView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Data Posta view customization failed", ex);
        }
    }

    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolAcquisizioneOggettiDPView start initializeView");

//            buildComponentSortedMap();
//
//            this.setFocusCycleRoot(true);
//            this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

            defineTabPolicyForTextArea(xmlDatajTextArea);
            
            JLabel cornelLabel = new JLabel("");
            cornelLabel.setOpaque(true);
            cornelLabel.setBackground(new Color(240, 240, 240));

            this.AcquisizioneOggettoDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.AcquisizioneOggettoDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.AcquisizioneOggettoDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.AcquisizioneOggettoDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.AcquisizioneOggettoDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            customizeTable(this.AcquisizioneOggettojTable);
            this.AcquisizioneOggettojTable.setModel(xteAcquisizioneOggettiTableModel);

            List<String> AcquisizioneOggettoColumnWidthValues = new ArrayList<String>();
            AcquisizioneOggettoColumnWidthValues.add("80");
            AcquisizioneOggettoColumnWidthValues.add("135");
            AcquisizioneOggettoColumnWidthValues.add("135");
            AcquisizioneOggettoColumnWidthValues.add("180");
            AcquisizioneOggettoColumnWidthValues.add("125");
            AcquisizioneOggettoColumnWidthValues.add("125");
            AcquisizioneOggettoColumnWidthValues.add("300");

            this.AcquisizioneOggettojTable.getTableHeader().setDefaultRenderer(new XteTableHeaderRenderer(AcquisizioneOggettoColumnWidthValues));
            this.AcquisizioneOggettojTable.setDefaultRenderer(Object.class, new XteMailPieceTableCellRenderer(AcquisizioneOggettoColumnWidthValues));
            
            this.xmlDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.xmlDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.xmlDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.xmlDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.xmlDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            JFormattedTextField AcquisizioneOggettoTimeJFormattedTextField = ((JSpinner.DateEditor)AcquisizioneOggettoTimejSpinner.getEditor()).getTextField();
            DefaultFormatter AcquisizioneOggettoTimeFormatter = (DefaultFormatter) AcquisizioneOggettoTimeJFormattedTextField.getFormatter();
            AcquisizioneOggettoTimeFormatter.setCommitsOnValidEdit(true);
            this.AcquisizioneOggettoCodejTextField.setDocument(new XteTestToolMailPieceCodeDocument());            
            this.AcquisizioneOggettoCodejTextField.getDocument().putProperty("owner", AcquisizioneOggettoCodejTextField);       
            this.AcquisizioneOggettoDatejXDatePicker.getEditor().setDocument(new PlainDocument());            
            this.AcquisizioneOggettoDatejXDatePicker.getEditor().getDocument().putProperty("owner", AcquisizioneOggettoDatejXDatePicker.getEditor());                        
            ((JSpinner.DateEditor)AcquisizioneOggettoTimejSpinner.getEditor()).getTextField().setDocument(new PlainDocument());            
            ((JSpinner.DateEditor)AcquisizioneOggettoTimejSpinner.getEditor()).getTextField().getDocument().putProperty("owner", ((JSpinner.DateEditor)AcquisizioneOggettoTimejSpinner.getEditor()).getTextField());            
            this.AcquisizioneOggettoOfficejTextField.setDocument(new PlainDocument());            
            this.AcquisizioneOggettoOfficejTextField.getDocument().putProperty("owner", AcquisizioneOggettoOfficejTextField);

            
            logger.info("XteTestToolAcquisizioneOggettiDPView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolAcquisizioneOggettiDPView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Data Posta view initialization failed", ex);
        }
    }

    @Override
    public void restoreView() {
        logger.debug("XteTestToolAcquisizioneOggettiDPView restoreView");
        this.getExitButtonRef().doClick();
    }

    private void customizeTable(JTable table) {
        table.getTableHeader().setReorderingAllowed(false);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setRowHeight(20);
        table.setSelectionModel(new XteListSelectionModel());
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    private  static void defineTabPolicyForTextArea(final JTextArea textarea) {

        final Object tabActionKey = new Object();
        final Object shiftTabActionKey = new Object();
        KeyStroke tabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0, false);
        KeyStroke shiftTabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_DOWN_MASK, false);

        Action tabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusNextComponent((Component) source);
                }
            }
        };

        Action shiftTabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent((Component) source);
                }
            }
        };

        textarea.getActionMap().put(tabActionKey, tabAction);
        textarea.getInputMap().put(tabKeyStroke, tabActionKey);

        textarea.getActionMap().put(shiftTabActionKey, shiftTabAction);
        textarea.getInputMap().put(shiftTabKeyStroke, shiftTabActionKey);

    }
    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.bundleIdjTextField);
        componentSortedMap.put(2, this.bundleTPIdjTextField);
        componentSortedMap.put(3, this.AcquisizioneOggettoTypejComboBox);
        componentSortedMap.put(4, this.AcquisizioneOggettoCodejTextField);
        componentSortedMap.put(5, this.AcquisizioneOggettoDatejXDatePicker.getEditor());
        componentSortedMap.put(6, ((JSpinner.DateEditor)this.AcquisizioneOggettoTimejSpinner.getEditor()).getTextField());
        componentSortedMap.put(7, this.AcquisizioneOggettoOfficejTextField);
        componentSortedMap.put(8, this.AcquisizioneOggettojTable);
        componentSortedMap.put(9, this.xmlDatajTextArea);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        commandjPanel = new javax.swing.JPanel();
        messageBarjScrollPane = new javax.swing.JScrollPane();
        messageBarjTextPane = new javax.swing.JTextPane();
        homejButton = new javax.swing.JButton();
        exitjButton = new javax.swing.JButton();
        backjButton = new javax.swing.JButton();
        workingjPanel = new javax.swing.JPanel();
        panelTitlejLabel = new javax.swing.JLabel();
        xmlDatajPanel = new javax.swing.JPanel();
        xmlDatajScrollPane = new javax.swing.JScrollPane();
        xmlDatajTextArea = new javax.swing.JTextArea();
        xmlDataCommandjPanel = new javax.swing.JPanel();
        generaXmlDatajButton = new javax.swing.JButton();
        impostaDatiWSjButton = new javax.swing.JButton();
        salvaXmlDatajButton = new javax.swing.JButton();
        bundleDatajPanel = new javax.swing.JPanel();
        bundleTPIdjLabel = new javax.swing.JLabel();
        bundleTPIdjTextField = new javax.swing.JTextField();
        bundleIdjTextField = new javax.swing.JTextField();
        bundleIdjLabel = new javax.swing.JLabel();
        AcquisizioneOggettoDatajPanel = new javax.swing.JPanel();
        AcquisizioneOggettoDatajScrollPane = new javax.swing.JScrollPane();
        AcquisizioneOggettojTable = new javax.swing.JTable();
        mailPieceDataCommandjPanel = new javax.swing.JPanel();
        addAcquisizioneOggettojButton = new javax.swing.JButton();
        removeAcquisizioneOggettojButton = new javax.swing.JButton();
        mailPieceCodejLabel = new javax.swing.JLabel();
        AcquisizioneOggettoCodejTextField = new javax.swing.JTextField();
        mailPieceOfficejLabel = new javax.swing.JLabel();
        AcquisizioneOggettoOfficejTextField = new javax.swing.JTextField();
        AcquisizioneOggettoDatejXDatePicker = new org.jdesktop.swingx.JXDatePicker();
        mailPieceDateTimejLabel = new javax.swing.JLabel();
        AcquisizioneOggettoTimejSpinner = new javax.swing.JSpinner();
        AcquisizioneOggettoTypejComboBox = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setMaximumSize(new java.awt.Dimension(1000, 690));
        setMinimumSize(new java.awt.Dimension(1000, 690));
        setPreferredSize(new java.awt.Dimension(1000, 690));

        commandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        commandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        commandjPanel.setMaximumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setMinimumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setPreferredSize(new java.awt.Dimension(980, 80));

        messageBarjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageBarjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        messageBarjScrollPane.setMaximumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setMinimumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setPreferredSize(new java.awt.Dimension(350, 50));

        messageBarjTextPane.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        messageBarjTextPane.setFocusable(false);
        messageBarjScrollPane.setViewportView(messageBarjTextPane);

        homejButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        homejButton.setForeground(new java.awt.Color(51, 51, 51));
        homejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Home3.png"))); // NOI18N
        homejButton.setText("<html><body>&nbsp;Home<body><html>");
        homejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        homejButton.setFocusPainted(false);
        homejButton.setFocusable(false);
        homejButton.setIconTextGap(5);
        homejButton.setMaximumSize(new java.awt.Dimension(100, 50));
        homejButton.setMinimumSize(new java.awt.Dimension(100, 50));
        homejButton.setPreferredSize(new java.awt.Dimension(100, 50));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("<html><body>&nbsp;Exit<body><html>");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setFocusPainted(false);
        exitjButton.setFocusable(false);
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIconTextGap(5);
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        backjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        backjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/BackToMenu.png"))); // NOI18N
        backjButton.setText("<html><body>&nbsp;Back<body><html>");
        backjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        backjButton.setFocusPainted(false);
        backjButton.setFocusable(false);
        backjButton.setForeground(new java.awt.Color(51, 51, 51));
        backjButton.setIconTextGap(5);
        backjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        backjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        backjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout commandjPanelLayout = new javax.swing.GroupLayout(commandjPanel);
        commandjPanel.setLayout(commandjPanelLayout);
        commandjPanelLayout.setHorizontalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 268, Short.MAX_VALUE)
                .addComponent(backjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandjPanelLayout.setVerticalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandjPanelLayout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homejButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exitjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        workingjPanel.setBackground(new java.awt.Color(230, 250, 0));
        workingjPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20)));
        workingjPanel.setMaximumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setMinimumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setPreferredSize(new java.awt.Dimension(980, 580));

        panelTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        panelTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        panelTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelTitlejLabel.setText("Notifica Mazzetti");
        panelTitlejLabel.setFocusable(false);
        panelTitlejLabel.setIconTextGap(30);
        panelTitlejLabel.setMaximumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setMinimumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setPreferredSize(new java.awt.Dimension(920, 20));

        xmlDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "XML Data", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        xmlDatajPanel.setFocusable(false);
        xmlDatajPanel.setMaximumSize(new java.awt.Dimension(860, 180));
        xmlDatajPanel.setMinimumSize(new java.awt.Dimension(860, 180));
        xmlDatajPanel.setOpaque(false);
        xmlDatajPanel.setPreferredSize(new java.awt.Dimension(860, 180));

        xmlDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        xmlDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        xmlDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 125));
        xmlDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 125));
        xmlDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 125));

        xmlDatajTextArea.setColumns(20);
        xmlDatajTextArea.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        xmlDatajTextArea.setForeground(new java.awt.Color(51, 51, 51));
        xmlDatajTextArea.setLineWrap(true);
        xmlDatajTextArea.setRows(5);
        xmlDatajTextArea.setWrapStyleWord(true);
        xmlDatajTextArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setViewportView(xmlDatajTextArea);

        xmlDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        xmlDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        xmlDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        generaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        generaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        generaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml6.png"))); // NOI18N
        generaXmlDatajButton.setText("<html><body>&nbsp;Genera<br>XML Data<body><html>");
        generaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        generaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        generaXmlDatajButton.setFocusPainted(false);
        generaXmlDatajButton.setFocusable(false);
        generaXmlDatajButton.setIconTextGap(2);
        generaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        xmlDataCommandjPanel.add(generaXmlDatajButton, gridBagConstraints);

        impostaDatiWSjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        impostaDatiWSjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/WebService5.png.png"))); // NOI18N
        impostaDatiWSjButton.setText("<html><body>&nbsp;Imposta<br>&nbsp;Dati WS<body><html>");
        impostaDatiWSjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        impostaDatiWSjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        impostaDatiWSjButton.setFocusPainted(false);
        impostaDatiWSjButton.setFocusable(false);
        impostaDatiWSjButton.setForeground(new java.awt.Color(51, 51, 51));
        impostaDatiWSjButton.setIconTextGap(1);
        impostaDatiWSjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        xmlDataCommandjPanel.add(impostaDatiWSjButton, gridBagConstraints);

        salvaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        salvaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml7.png"))); // NOI18N
        salvaXmlDatajButton.setText("<html><body>&nbsp;Salva<br>XML Data<body><html>");
        salvaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        salvaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        salvaXmlDatajButton.setFocusPainted(false);
        salvaXmlDatajButton.setFocusable(false);
        salvaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        salvaXmlDatajButton.setIconTextGap(2);
        salvaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        xmlDataCommandjPanel.add(salvaXmlDatajButton, gridBagConstraints);

        javax.swing.GroupLayout xmlDatajPanelLayout = new javax.swing.GroupLayout(xmlDatajPanel);
        xmlDatajPanel.setLayout(xmlDatajPanelLayout);
        xmlDatajPanelLayout.setHorizontalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        xmlDatajPanelLayout.setVerticalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                    .addGroup(xmlDatajPanelLayout.createSequentialGroup()
                        .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        bundleDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Informazioni Mazzetto", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        bundleDatajPanel.setMaximumSize(new java.awt.Dimension(860, 70));
        bundleDatajPanel.setMinimumSize(new java.awt.Dimension(860, 70));
        bundleDatajPanel.setOpaque(false);
        bundleDatajPanel.setPreferredSize(new java.awt.Dimension(860, 70));

        bundleTPIdjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleTPIdjLabel.setText("ID Operatore TP:");
        bundleTPIdjLabel.setFocusable(false);
        bundleTPIdjLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        bundleTPIdjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjTextField.setText("Mario Rossi");
        bundleTPIdjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        bundleTPIdjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        bundleTPIdjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        bundleTPIdjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        bundleIdjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleIdjTextField.setText("38999123456789012789");
        bundleIdjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        bundleIdjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        bundleIdjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        bundleIdjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        bundleIdjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleIdjLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleIdjLabel.setText("Codice Identificativo:");
        bundleIdjLabel.setFocusable(false);
        bundleIdjLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        bundleIdjLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        bundleIdjLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        javax.swing.GroupLayout bundleDatajPanelLayout = new javax.swing.GroupLayout(bundleDatajPanel);
        bundleDatajPanel.setLayout(bundleDatajPanelLayout);
        bundleDatajPanelLayout.setHorizontalGroup(
            bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bundleIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addComponent(bundleTPIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleTPIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        bundleDatajPanelLayout.setVerticalGroup(
            bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bundleTPIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bundleTPIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bundleIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bundleIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(7, Short.MAX_VALUE))
        );

        AcquisizioneOggettoDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Informazioni Invio", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        AcquisizioneOggettoDatajPanel.setMaximumSize(new java.awt.Dimension(860, 260));
        AcquisizioneOggettoDatajPanel.setMinimumSize(new java.awt.Dimension(860, 260));
        AcquisizioneOggettoDatajPanel.setOpaque(false);
        AcquisizioneOggettoDatajPanel.setPreferredSize(new java.awt.Dimension(860, 260));

        AcquisizioneOggettoDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        AcquisizioneOggettoDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        AcquisizioneOggettoDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        AcquisizioneOggettoDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 120));
        AcquisizioneOggettoDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 120));
        AcquisizioneOggettoDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 120));

        AcquisizioneOggettojTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        AcquisizioneOggettoDatajScrollPane.setViewportView(AcquisizioneOggettojTable);

        mailPieceDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        mailPieceDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        mailPieceDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 85));
        mailPieceDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 85));
        mailPieceDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 85));
        mailPieceDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        addAcquisizioneOggettojButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        addAcquisizioneOggettojButton.setForeground(new java.awt.Color(51, 51, 51));
        addAcquisizioneOggettojButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Add1.png"))); // NOI18N
        addAcquisizioneOggettojButton.setText("<html><body>&nbsp;Aggiungi<br>&nbsp;&nbsp;&nbsp;Invio<body><html>");
        addAcquisizioneOggettojButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        addAcquisizioneOggettojButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        addAcquisizioneOggettojButton.setFocusPainted(false);
        addAcquisizioneOggettojButton.setFocusable(false);
        addAcquisizioneOggettojButton.setIconTextGap(1);
        addAcquisizioneOggettojButton.setMaximumSize(new java.awt.Dimension(100, 35));
        addAcquisizioneOggettojButton.setMinimumSize(new java.awt.Dimension(100, 35));
        addAcquisizioneOggettojButton.setPreferredSize(new java.awt.Dimension(100, 35));
        mailPieceDataCommandjPanel.add(addAcquisizioneOggettojButton, new java.awt.GridBagConstraints());

        removeAcquisizioneOggettojButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        removeAcquisizioneOggettojButton.setForeground(new java.awt.Color(51, 51, 51));
        removeAcquisizioneOggettojButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Remove1.png"))); // NOI18N
        removeAcquisizioneOggettojButton.setText("<html><body>&nbsp;Elimina<br>&nbsp;&nbsp;&nbsp;Invio<body><html>");
        removeAcquisizioneOggettojButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        removeAcquisizioneOggettojButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        removeAcquisizioneOggettojButton.setFocusPainted(false);
        removeAcquisizioneOggettojButton.setFocusable(false);
        removeAcquisizioneOggettojButton.setIconTextGap(2);
        removeAcquisizioneOggettojButton.setMaximumSize(new java.awt.Dimension(100, 35));
        removeAcquisizioneOggettojButton.setMinimumSize(new java.awt.Dimension(100, 35));
        removeAcquisizioneOggettojButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        mailPieceDataCommandjPanel.add(removeAcquisizioneOggettojButton, gridBagConstraints);

        mailPieceCodejLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel.setText("Tipo/Codice:");
        mailPieceCodejLabel.setFocusable(false);
        mailPieceCodejLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        AcquisizioneOggettoCodejTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        AcquisizioneOggettoCodejTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        AcquisizioneOggettoCodejTextField.setMaximumSize(new java.awt.Dimension(180, 20));
        AcquisizioneOggettoCodejTextField.setMinimumSize(new java.awt.Dimension(180, 20));
        AcquisizioneOggettoCodejTextField.setPreferredSize(new java.awt.Dimension(180, 20));

        mailPieceOfficejLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceOfficejLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceOfficejLabel.setText("Altro Ufficio:");
        mailPieceOfficejLabel.setFocusable(false);
        mailPieceOfficejLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceOfficejLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceOfficejLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        AcquisizioneOggettoOfficejTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        AcquisizioneOggettoOfficejTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        AcquisizioneOggettoOfficejTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        AcquisizioneOggettoOfficejTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        AcquisizioneOggettoOfficejTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        AcquisizioneOggettoDatejXDatePicker.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        AcquisizioneOggettoDatejXDatePicker.setBackground(new java.awt.Color(255, 255, 255));
        AcquisizioneOggettoDatejXDatePicker.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        AcquisizioneOggettoDatejXDatePicker.setMaximumSize(new java.awt.Dimension(150, 20));
        AcquisizioneOggettoDatejXDatePicker.setMinimumSize(new java.awt.Dimension(150, 20));
        AcquisizioneOggettoDatejXDatePicker.setOpaque(true);
        AcquisizioneOggettoDatejXDatePicker.setPreferredSize(new java.awt.Dimension(150, 20));

        mailPieceDateTimejLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel.setText("Data/Ora:");
        mailPieceDateTimejLabel.setFocusable(false);
        mailPieceDateTimejLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        AcquisizioneOggettoTimejSpinner.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        AcquisizioneOggettoTimejSpinner.setModel(new javax.swing.SpinnerDateModel());
        AcquisizioneOggettoTimejSpinner.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        AcquisizioneOggettoTimejSpinner.setEditor(new javax.swing.JSpinner.DateEditor(AcquisizioneOggettoTimejSpinner, "HH:mm:ss"));
        AcquisizioneOggettoTimejSpinner.setFocusable(false);
        AcquisizioneOggettoTimejSpinner.setMaximumSize(new java.awt.Dimension(80, 20));
        AcquisizioneOggettoTimejSpinner.setMinimumSize(new java.awt.Dimension(80, 20));
        AcquisizioneOggettoTimejSpinner.setPreferredSize(new java.awt.Dimension(80, 20));

        AcquisizioneOggettoTypejComboBox.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        AcquisizioneOggettoTypejComboBox.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        AcquisizioneOggettoTypejComboBox.setMaximumSize(new java.awt.Dimension(50, 20));
        AcquisizioneOggettoTypejComboBox.setMinimumSize(new java.awt.Dimension(50, 20));
        AcquisizioneOggettoTypejComboBox.setPreferredSize(new java.awt.Dimension(50, 20));

        javax.swing.GroupLayout AcquisizioneOggettoDatajPanelLayout = new javax.swing.GroupLayout(AcquisizioneOggettoDatajPanel);
        AcquisizioneOggettoDatajPanel.setLayout(AcquisizioneOggettoDatajPanelLayout);
        AcquisizioneOggettoDatajPanelLayout.setHorizontalGroup(
            AcquisizioneOggettoDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AcquisizioneOggettoDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AcquisizioneOggettoDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AcquisizioneOggettoDatajPanelLayout.createSequentialGroup()
                        .addComponent(mailPieceDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                        .addComponent(AcquisizioneOggettoDatajScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(AcquisizioneOggettoDatajPanelLayout.createSequentialGroup()
                        .addGroup(AcquisizioneOggettoDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mailPieceCodejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceDateTimejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(AcquisizioneOggettoDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(AcquisizioneOggettoDatajPanelLayout.createSequentialGroup()
                                .addComponent(AcquisizioneOggettoDatejXDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(AcquisizioneOggettoTimejSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(AcquisizioneOggettoDatajPanelLayout.createSequentialGroup()
                                .addComponent(AcquisizioneOggettoTypejComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(AcquisizioneOggettoCodejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(mailPieceOfficejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(AcquisizioneOggettoOfficejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        AcquisizioneOggettoDatajPanelLayout.setVerticalGroup(
            AcquisizioneOggettoDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AcquisizioneOggettoDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AcquisizioneOggettoDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mailPieceCodejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AcquisizioneOggettoCodejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AcquisizioneOggettoTypejComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceOfficejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AcquisizioneOggettoOfficejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(AcquisizioneOggettoDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(AcquisizioneOggettoDatejXDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDateTimejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AcquisizioneOggettoTimejSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(AcquisizioneOggettoDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(AcquisizioneOggettoDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        AcquisizioneOggettoOfficejTextField.getAccessibleContext().setAccessibleName("OfficeField");

        javax.swing.GroupLayout workingjPanelLayout = new javax.swing.GroupLayout(workingjPanel);
        workingjPanel.setLayout(workingjPanelLayout);
        workingjPanelLayout.setHorizontalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workingjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(workingjPanelLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bundleDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(AcquisizioneOggettoDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        workingjPanelLayout.setVerticalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(AcquisizioneOggettoDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField AcquisizioneOggettoCodejTextField;
    private javax.swing.JPanel AcquisizioneOggettoDatajPanel;
    private javax.swing.JScrollPane AcquisizioneOggettoDatajScrollPane;
    private org.jdesktop.swingx.JXDatePicker AcquisizioneOggettoDatejXDatePicker;
    private javax.swing.JTextField AcquisizioneOggettoOfficejTextField;
    private javax.swing.JSpinner AcquisizioneOggettoTimejSpinner;
    private javax.swing.JComboBox AcquisizioneOggettoTypejComboBox;
    private javax.swing.JTable AcquisizioneOggettojTable;
    private javax.swing.JButton addAcquisizioneOggettojButton;
    private javax.swing.JButton backjButton;
    private javax.swing.JPanel bundleDatajPanel;
    private javax.swing.JLabel bundleIdjLabel;
    private javax.swing.JTextField bundleIdjTextField;
    private javax.swing.JLabel bundleTPIdjLabel;
    private javax.swing.JTextField bundleTPIdjTextField;
    private javax.swing.JPanel commandjPanel;
    private javax.swing.JButton exitjButton;
    private javax.swing.JButton generaXmlDatajButton;
    private javax.swing.JButton homejButton;
    private javax.swing.JButton impostaDatiWSjButton;
    private javax.swing.JLabel mailPieceCodejLabel;
    private javax.swing.JPanel mailPieceDataCommandjPanel;
    private javax.swing.JLabel mailPieceDateTimejLabel;
    private javax.swing.JLabel mailPieceOfficejLabel;
    private javax.swing.JScrollPane messageBarjScrollPane;
    private javax.swing.JTextPane messageBarjTextPane;
    private javax.swing.JLabel panelTitlejLabel;
    private javax.swing.JButton removeAcquisizioneOggettojButton;
    private javax.swing.JButton salvaXmlDatajButton;
    private javax.swing.JPanel workingjPanel;
    private javax.swing.JPanel xmlDataCommandjPanel;
    private javax.swing.JPanel xmlDatajPanel;
    private javax.swing.JScrollPane xmlDatajScrollPane;
    private javax.swing.JTextArea xmlDatajTextArea;
    // End of variables declaration//GEN-END:variables


}
