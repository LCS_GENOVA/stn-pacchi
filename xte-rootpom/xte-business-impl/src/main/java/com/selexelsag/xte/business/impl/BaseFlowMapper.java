package com.selexelsag.xte.business.impl;

import java.util.HashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.xte.business.exceptions.BusinessFlowMapperException;
import com.selexelsag.xte.business.tracking.IFlowMapper;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.service.system.IAppConfig;

public class BaseFlowMapper implements IFlowMapper 
{
	private Logger logger = LogManager.getLogger(BaseFlowMapper.class.getName());

	@Autowired
	private IAppConfig appConfig;
	public IAppConfig getAppConfig(){return appConfig;}
	public void setAppConfig(IAppConfig appConfig) {this.appConfig = appConfig;}
	
	KnowledgeBase knowledgeBase = null;
	
	public void init() 
	{
		String flowResolver = appConfig.getGeneralParam(IAppConfig.GENERAL_FLOW_RESOLVER);
		
		KnowledgeBuilder builder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		builder.add(ResourceFactory.newFileResource(flowResolver),ResourceType.DRL);
		if (builder.hasErrors()) 
		{
			throw new RuntimeException(builder.getErrors().toString());
		}
		knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
		knowledgeBase.addKnowledgePackages(builder.getKnowledgePackages());		
	}
	
	@Override
	public FlowParams getFlowParams(TrackRequest trackRequest) throws BusinessFlowMapperException 
	{
		StatefulKnowledgeSession session = knowledgeBase.newStatefulKnowledgeSession();
		
		FlowParams params = new FlowParams();
		try 
		{
			session.insert(trackRequest);

			session.insert(params);
			
			HashMap<String, String> baseKeys = new HashMap<String, String>();
			baseKeys.put(IAppConfig.BPM_SYNC_VALIDATION_BASE_PATH_KEY, 
					appConfig.getBpmParam(IAppConfig.BPM_SYNC_VALIDATION_BASE_PATH_KEY));
			baseKeys.put(IAppConfig.BPM_SYNC_TRANSFORMATION_BASE_PATH_KEY, 
					appConfig.getBpmParam(IAppConfig.BPM_SYNC_TRANSFORMATION_BASE_PATH_KEY));
					
			session.insert(baseKeys); 
			
			session.fireAllRules();
		}
		catch (Throwable e) 
		{
			BusinessFlowMapperException bfme = new BusinessFlowMapperException("Errore in recupero parametri ", e);
			throw bfme;
		}
		finally 
		{
			session.dispose();
		}
		if (params.getFlowId()==null) 
		{
			BusinessFlowMapperException bfme = new BusinessFlowMapperException("Flusso non gestito ", null);
			throw bfme;
		}
		return params;
	}
}
