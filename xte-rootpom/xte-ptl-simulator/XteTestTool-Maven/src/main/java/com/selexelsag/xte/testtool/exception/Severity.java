/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.exception;

/**
 *
 * @author Da Procida
 */
public enum Severity {

    WARNING(0),
    ERROR(1),
    FATAL(2);
    private final int code;

    private Severity(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
