package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.FromNspToCmp;
import com.elsagdatamat.dbtrace.bridge.engine.entities.FromNspToCmpPK;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class FromNspToCmpDAO extends SpringHibernateJpaDAOBase<FromNspToCmp, FromNspToCmpPK> implements IFromNspToCmpDAO{

}
