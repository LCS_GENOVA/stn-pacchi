package com.elsagdatamat.trackdb.toedwh.dao;

import java.util.List;

import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.framework.search.Filter;
import com.elsagdatamat.framework.search.Search;
import com.elsagdatamat.trackdb.toedwh.entities.EdwhTracesReadyToSendVW;

public class EdwhTracesReadyToSendVWDAO extends SpringHibernateJpaDAOBase<EdwhTracesReadyToSendVW, Long> implements IEdwhTracesReadyToSendVWDAO {

	@Override
	public List<EdwhTracesReadyToSendVW> executeTraceQuery(Long id) {
		Search search = new Search(EdwhTracesReadyToSendVW.class);
		Filter f = Filter.greaterThan("id", id);
		search.addFilter(f);

		search.addSort("id");
		search.setMaxResults(1);
		List<EdwhTracesReadyToSendVW> l = this.search(search);
		return l;
	}
}
