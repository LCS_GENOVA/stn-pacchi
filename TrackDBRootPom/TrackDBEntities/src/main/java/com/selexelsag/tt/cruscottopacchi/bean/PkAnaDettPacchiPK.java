package com.selexelsag.tt.cruscottopacchi.bean;


import java.io.Serializable;

import javax.persistence.Column;


/** 
* R.L. BeanGenerator - 28.05.2012 16:22:59
* R.L. - Inseriti per il nuovo metodo del trackDbWS - packDataRetrieve
*/ 
public class PkAnaDettPacchiPK implements Serializable {

	private static final long serialVersionUID = 2991649696276408497L;
	
	@Column(name = "NOME", nullable =false)
	private String nome;
	
	@Column(name = "ID_PACCO", nullable =false)
	private String idPacco;

	@Column(name = "COD_TRACCIA", nullable =false)
	private String whatHappened;
	
	public String getNome(){
		return nome;
	}

	public String getIdPacco(){
		return idPacco;
	}

	public void setNome(String nome){
		this.nome=nome;
	}

	public void setIdPacco(String idPacco){
		this.idPacco=idPacco;
	}

	public String getWhatHappened() {
		return whatHappened;
	}

	public void setWhatHappened(String whatHappened) {
		this.whatHappened = whatHappened;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPacco == null) ? 0 : idPacco.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((whatHappened == null) ? 0 : whatHappened.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PkAnaDettPacchiPK other = (PkAnaDettPacchiPK) obj;
		if (idPacco == null) {
			if (other.idPacco != null)
				return false;
		} else if (!idPacco.equals(other.idPacco))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (whatHappened == null) {
			if (other.whatHappened != null)
				return false;
		} else if (!whatHappened.equals(other.whatHappened))
			return false;
		return true;
	}

}