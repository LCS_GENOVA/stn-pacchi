package com.elsagdatamat.dbtrace.bridge.engine.dao;
 
import com.elsagdatamat.dbtrace.bridge.engine.entities.FromTo;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class FromToDAO extends SpringHibernateJpaDAOBase<FromTo, String> implements IFromToDAO{

}
