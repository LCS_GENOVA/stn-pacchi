/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.objectdocument;

/**
 *
 * @author Tassara
 */
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;


public class XteTestToolStringDocument extends PlainDocument {
     private int maxLenght;

    public XteTestToolStringDocument(int maxLenght) {
        super();
        this.maxLenght = maxLenght;
    }

    @Override
    public void insertString(int offs, String str, AttributeSet a)
            throws BadLocationException {

        if (str == null) {
            return;
        }

        char[] buffer = str.toCharArray();
        /*char[] digit = new char[buffer.length];
        int j = 0;

        for (int i = 0; i < buffer.length; i++) {
                        
            if (Character.isDigit(buffer[i])) {                
                digit[j++] = buffer[i];
            }            
        }*/

        String added = new String(buffer, 0, buffer.length);

        if ((getLength() + added.length()) <= maxLenght) {
            super.insertString(offs, added, a);
        }

    }
    
}
