package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.OfficeMapper;
import com.elsagdatamat.dbtrace.bridge.engine.entities.OfficeMapperPK;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface IOfficeMapperDAO  extends IGenericDAO<OfficeMapper, OfficeMapperPK>{

}
