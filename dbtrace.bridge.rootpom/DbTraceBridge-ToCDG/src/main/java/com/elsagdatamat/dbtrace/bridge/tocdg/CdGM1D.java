package com.elsagdatamat.dbtrace.bridge.tocdg;

import javax.xml.bind.annotation.XmlElement;


public class CdGM1D {
	@XmlElement(name = "OBJ")
	public CdGOBJ cdgobj;

	@XmlElement(name = "ACC")
	public CdGACC cdgacc;
	
}
