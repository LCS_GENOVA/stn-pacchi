package com.elsagdatamat.dbtrace.fromsda.bean;


import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
"PRE_SDA"
})
public class TRACE_ {


/**
* 
* (Required)
* 
*/
@JsonProperty("PRE_SDA")
private PRESDA pRESDA;



/**
* 
* (Required)
* 
*/
@JsonProperty("PRE_SDA")
public PRESDA getPRESDA() {
return pRESDA;
 }

/**
* 
* (Required)
* 
*/
@JsonProperty("PRE_SDA")
public void setPRESDA(PRESDA pRESDA) {
this.pRESDA = pRESDA;
 }

}
