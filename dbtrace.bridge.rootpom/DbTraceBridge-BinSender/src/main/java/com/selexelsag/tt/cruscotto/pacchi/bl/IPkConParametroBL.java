package com.selexelsag.tt.cruscotto.pacchi.bl;

import com.selexelsag.tt.cruscotto.pacchi.bean.PkConParametro;


public interface IPkConParametroBL {
	public PkConParametro getElemento(String pNome);
}
