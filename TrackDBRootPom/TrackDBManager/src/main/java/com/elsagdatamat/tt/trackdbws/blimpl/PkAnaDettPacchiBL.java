package com.elsagdatamat.tt.trackdbws.blimpl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.elsagdatamat.tt.trackdbws.beans.VO.PackDataRetrieveVO;
import com.elsagdatamat.tt.trackdbws.bl.IPkAnaDettPacchiBL;
import com.elsagdatamat.tt.trackdbws.dao.IPkAnaDettPacchiDAO;
import com.selexelsag.tt.cruscottopacchi.bean.PkAnaDettPacchi;
/**
* R.L. BeanGenerator - 27.05.2012 18:17:43
*/
public class PkAnaDettPacchiBL implements IPkAnaDettPacchiBL {

	@Autowired
	private IPkAnaDettPacchiDAO pkAnaDettPacchiDAO;

	@Override
	public void insert(PkAnaDettPacchi dettaglio) {
		pkAnaDettPacchiDAO.insert(dettaglio);
	}


	@Override
	public List<PackDataRetrieveVO> findConfDetsByIdPacco(String idPacco) {
//		String hqlQuery ="SELECT new com.elsagdatamat.tt.trackdbws.beans.VO.PackDataRetrieveVO(det,con)" +
//				   				  "FROM PkAnaDettPacchi det, PkConMappingDettTr con " +
//				   				  "WHERE det.whatHappened = con.codTraccia " +
//				   				  "AND det.nome = con.dettOrig " +
//				   				  "AND det.idPacco = '" + idPacco+"'" ;
//
//
//		return pkAnaDettPacchiDAO.getListOfPackDataRetrieveVO(hqlQuery);
		// modifica per usare named query

		return pkAnaDettPacchiDAO.getListOfPackDataRetrieveVONQ(idPacco);

	}

}