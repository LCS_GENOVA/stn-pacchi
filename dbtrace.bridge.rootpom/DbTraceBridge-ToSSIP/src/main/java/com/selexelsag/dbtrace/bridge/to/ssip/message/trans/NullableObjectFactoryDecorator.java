/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.ssip.message.trans;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.bind.JAXBElement;

import com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Traces.Trace.WHEREHAPPENEDDETAIL.GPS;
import com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory;

/**
 * @author arodriguez
 *
 */
public class NullableObjectFactoryDecorator extends ObjectFactory {

	private ObjectFactory innerFactory;
	
	/**
	 * 
	 */
	private NullableObjectFactoryDecorator() {
		super();
	}

	/**
	 * @param innerFactory
	 */
	public NullableObjectFactoryDecorator(ObjectFactory innerFactory) {
		this.innerFactory = innerFactory;
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTONERIDOGANALI(java.math.BigDecimal)
	 */
	public JAXBElement<BigDecimal> createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTONERIDOGANALI(BigDecimal value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTONERIDOGANALI(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTDIRITTIPOSTALI(java.math.BigDecimal)
	 */
	public JAXBElement<BigDecimal> createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTDIRITTIPOSTALI(BigDecimal value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTDIRITTIPOSTALI(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTDESCCAUSALE(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTDESCCAUSALE(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTDESCCAUSALE(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTDESCSTATUS(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTDESCSTATUS(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTDESCSTATUS(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTNOTE(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTNOTE(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTNOTE(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTIDCAUSALE(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTIDCAUSALE(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTIDCAUSALE(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTIMPORTOCONTRASSEGNO(java.math.BigDecimal)
	 */
	public JAXBElement<BigDecimal> createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTIMPORTOCONTRASSEGNO(BigDecimal value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILSSIPTOTTIMPORTOCONTRASSEGNO(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceIDDISPACCIO(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceIDDISPACCIO(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceIDDISPACCIO(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHEREHAPPENEDDETAILOFFICEWHEREDESCRIPTION(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHEREHAPPENEDDETAILOFFICEWHEREDESCRIPTION(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHEREHAPPENEDDETAILOFFICEWHEREDESCRIPTION(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCNTRS(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCNTRS(BigInteger value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCNTRS(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDESTR(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDESTR(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDESTR(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPFRM(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPFRM(BigInteger value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPFRM(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPVARTR(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPVARTR(BigInteger value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPVARTR(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPLOCALID(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPLOCALID(BigInteger value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPLOCALID(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCONTID(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCONTID(BigInteger value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCONTID(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPPARTITAIVA(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPPARTITAIVA(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPPARTITAIVA(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDESTZ(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDESTZ(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDESTZ(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDIRD(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDIRD(BigInteger value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDIRD(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCODICESAP(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCODICESAP(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCODICESAP(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDIST(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDIST(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPDIST(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPZIP(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPZIP(BigInteger value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPZIP(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPPROV(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPPROV(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPPROV(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPASSIC(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPASSIC(BigInteger value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPASSIC(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPFSC(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPFSC(BigInteger value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPFSC(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPPREPGID(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPPREPGID(BigInteger value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPPREPGID(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCODICEFISCALE(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCODICEFISCALE(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPCODICEFISCALE(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPMRESID(java.math.BigInteger)
	 */
	public JAXBElement<BigInteger> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPMRESID(BigInteger value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPMRESID(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPIRES(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPIRES(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPIRES(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPADDRS(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPADDRS(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPADDRS(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPNAZ(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPNAZ(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPNAZ(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILACCNSPSA(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILACCNSPSA(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILACCNSPSA(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHEREHAPPENEDDETAILGPS(com.selexelsag.dbtrace.bridge.to.ssip.message.map.Message.Traces.Trace.WHEREHAPPENEDDETAIL.GPS)
	 */
	public JAXBElement<GPS> createMessageTracesTraceWHEREHAPPENEDDETAILGPS(GPS value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHEREHAPPENEDDETAILGPS(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPDESCCAUSALE(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPDESCCAUSALE(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPDESCCAUSALE(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPDESCSTATUS(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPDESCSTATUS(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPDESCSTATUS(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPNOTE(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPNOTE(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPNOTE(value));
	}

	/**
	 * @param value
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.message.map.ObjectFactory#createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPIDCAUSALE(java.lang.String)
	 */
	public JAXBElement<String> createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPIDCAUSALE(String value) {
		return (value == null ? null : innerFactory.createMessageTracesTraceWHATHAPPENEDDETAILTTTOSSIPIDCAUSALE(value));
	}

	
}
