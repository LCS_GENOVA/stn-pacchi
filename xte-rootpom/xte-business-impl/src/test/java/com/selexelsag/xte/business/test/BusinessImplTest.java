package com.selexelsag.xte.business.test;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.selexelsag.xte.business.exceptions.BusinessPersistentAccessException;
import com.selexelsag.xte.business.exceptions.BusinessServiceRequestException;
import com.selexelsag.xte.business.impl.BaseServiceRequest;
import com.selexelsag.xte.business.tracking.IPersistentAccess;
import com.selexelsag.xte.business.tracking.IServiceRequest;
import com.selexelsag.xte.business.tracking.TrackTraceConstants;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.service.context.SpringApplicationContext;
import com.selexelsag.xte.service.system.IAppConfig;

@ContextConfiguration(locations = { 
		"/applicationContext_test.xml", 
		"/businessConf_test.xml", 
		"/dbConf_test.xml",
		"/serviceConf_test.xml"})
public class BusinessImplTest extends AbstractTransactionalJUnit4SpringContextTests {
	
	private Logger logger = LogManager.getLogger(BusinessImplTest.class.getName());
	
	/*
	{
		DOMConfigurator.configure("C:/development/log4j_conf/log4jConfig.xml");
	}
	*/
	
	@Test
	public void testBaseServiceRequest() {
		
		IServiceRequest sr = (IServiceRequest)SpringApplicationContext.getBean("baseServiceRequest");
		
		BaseServiceRequest bsr = new BaseServiceRequest();
		FlowParams params = new FlowParams();
		try {
			params.setHttpServiceUrl(new URL("http://webpub-ge.edg.grptop.net/pac/proxy.pac"));
			sr.getHttpResponse(params, new StringBuilder());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BusinessServiceRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void testBasePersistentAccess() {
		
		IPersistentAccess pa = (IPersistentAccess)SpringApplicationContext.getBean("basePersistentAccess");
	
		TrackRequest tr = new TrackRequest();
		FlowParams fp;
		StringBuilder result;
		
		tr.setService(new StringBuilder(TrackTraceConstants.SERVICE_POSTA_REGISTRATA));
		tr.setChannel(new StringBuilder(TrackTraceConstants.CHANNEL_TERMINALE_PORTALETTERE));
		logger.debug("Requesting parameters for service " + tr.getService().toString());
		fp = new FlowParams();
		try {
			pa.fillConnectionParameters(fp);
		} catch (BusinessPersistentAccessException e) {
			logger.error("Errore nel recupero delle informazioni dal db per la richiesta " + tr);
			e.printStackTrace();
			return;
		}
		logger.debug("parametri ottenuti: OffRefID=" + fp.getOffRefID() + ", HttpServiceUrl=" + fp.getHttpServiceUrl());
		BaseServiceRequest sreq = new BaseServiceRequest();
		
		try {
			result = sreq.getHttpResponse(fp, new StringBuilder(tr.getService().toString()));
		} catch (BusinessServiceRequestException e1) {
			logger.error("Errore sulla richiesta http " + tr);
			e1.printStackTrace();
			return;
		}
		
		logger.debug("Result is: " + result);
		
		tr.setService(new StringBuilder(TrackTraceConstants.SERVICE_DATA_POSTA));
		logger.debug("Requesting parameters for service " + tr.getService().toString());
		fp = new FlowParams();
		try {
			pa.fillConnectionParameters(fp);
		} catch (BusinessPersistentAccessException e) {
			logger.error("Errore nel recupero delle informazioni dal db per la richiesta " + tr);
			e.printStackTrace();
			return;
		}
		logger.debug("parametri ottenuti: OffRefID=" + fp.getOffRefID() + ", HttpServiceUrl=" + fp.getHttpServiceUrl());

		try {
			result = sreq.getHttpResponse(fp, new StringBuilder(tr.getService().toString()));
		} catch (BusinessServiceRequestException e1) {
			logger.error("Errore sulla richiesta http " + tr);
			e1.printStackTrace();
			return;
		}
		
		logger.debug("Result is: " + result);

		/*
		tr.setService(new StringBuilder(TrackTraceConstants.SERVICE_TERMINALE_PORTALETTERE));
		logger.debug("Requesting parameters for service " + tr.getService().toString());
		fp = new FlowParams();
		try {
			pa.fillConnectionParameters(tr, fp);
		} catch (BusinessPersistentAccessException e) {
			logger.error("Errore nel recupero delle informazioni dal db per la richiesta " + tr);
			e.printStackTrace();
			return;
		}
		logger.debug("parametri ottenuti: OffRefID=" + fp.getOffRefID() + ", HttpServiceUrl=" + fp.getHttpServiceUrl());

		try {
			result = sreq.getHttpResponse(fp, new StringBuilder(tr.getService().toString()));
		} catch (BusinessServiceRequestException e1) {
			logger.error("Errore sulla richiesta http " + tr);
			e1.printStackTrace();
		}
		
		logger.debug("Result is: " + result);
		*/
	}

}
