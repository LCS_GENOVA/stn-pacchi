/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectrenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Da Procida
 */
public class XteGeneraMazzettiObjTableCellRenderer extends DefaultTableCellRenderer {

    public XteGeneraMazzettiObjTableCellRenderer() {
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

        defineTableFocus(table);

        JLabel renderer = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);

        if(table.isEnabled())
        {
            renderer = new JLabel();
            renderer.setOpaque(true);
            renderer.setBackground(Color.WHITE);

            renderer.setFont(new Font("Tahoma", Font.BOLD, 12));
            renderer.setHorizontalAlignment(JLabel.CENTER);
            renderer.setForeground(new Color(51,51,51));

            if(value instanceof String)
            {
                if(value!=null)
                {
                    renderer.setText((String) value);
                }
                else
                {
                    renderer.setText("");

                }
            }

            if(value instanceof Long)
            {
                if(value!=null)
                {
                    Long lValue = (Long) value;
                    renderer.setText(Long.toString(lValue));
                }
                else
                {
                    renderer.setText("");

                }
            }

            if(isSelected)
            {
                renderer.setBackground(new Color(153, 204, 255));
            }
            else
            {
                renderer.setBackground(Color.white);
            }

        }
        else
        {
            renderer.setForeground(new Color(204,204,204));
        }
        
        return renderer;
    }

    private void defineTableFocus(JTable table)
    {
        Action tabOut = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
            }
        };

        Action stabOut = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent();
            }
        };

        table.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabOut");
        table.getActionMap().put("tabOut",tabOut);

        table.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, java.awt.event.InputEvent.SHIFT_DOWN_MASK ), "stabOut");
        table.getActionMap().put("stabOut",stabOut);


    }
    
}
