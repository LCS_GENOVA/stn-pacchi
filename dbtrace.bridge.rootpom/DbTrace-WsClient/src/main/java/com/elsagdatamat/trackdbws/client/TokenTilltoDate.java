
package com.elsagdatamat.trackdbws.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Token_TilltoDate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Token_TilltoDate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}Token"/>
 *         &lt;element name="tilltoDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="traceFilterByExampleEqual" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}Trace"/>
 *         &lt;element name="traceFilterByExampleNotEqual" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}Trace"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Token_TilltoDate", propOrder = {
    "token",
    "tilltoDate",
    "traceFilterByExampleEqual",
    "traceFilterByExampleNotEqual"
})
public class TokenTilltoDate {

    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected XMLGregorianCalendar tilltoDate;
    @XmlElement(required = true, nillable = true)
    protected Trace traceFilterByExampleEqual;
    @XmlElement(required = true, nillable = true)
    protected Trace traceFilterByExampleNotEqual;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the tilltoDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTilltoDate() {
        return tilltoDate;
    }

    /**
     * Sets the value of the tilltoDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTilltoDate(XMLGregorianCalendar value) {
        this.tilltoDate = value;
    }

    /**
     * Gets the value of the traceFilterByExampleEqual property.
     * 
     * @return
     *     possible object is
     *     {@link Trace }
     *     
     */
    public Trace getTraceFilterByExampleEqual() {
        return traceFilterByExampleEqual;
    }

    /**
     * Sets the value of the traceFilterByExampleEqual property.
     * 
     * @param value
     *     allowed object is
     *     {@link Trace }
     *     
     */
    public void setTraceFilterByExampleEqual(Trace value) {
        this.traceFilterByExampleEqual = value;
    }

    /**
     * Gets the value of the traceFilterByExampleNotEqual property.
     * 
     * @return
     *     possible object is
     *     {@link Trace }
     *     
     */
    public Trace getTraceFilterByExampleNotEqual() {
        return traceFilterByExampleNotEqual;
    }

    /**
     * Sets the value of the traceFilterByExampleNotEqual property.
     * 
     * @param value
     *     allowed object is
     *     {@link Trace }
     *     
     */
    public void setTraceFilterByExampleNotEqual(Trace value) {
        this.traceFilterByExampleNotEqual = value;
    }

}
