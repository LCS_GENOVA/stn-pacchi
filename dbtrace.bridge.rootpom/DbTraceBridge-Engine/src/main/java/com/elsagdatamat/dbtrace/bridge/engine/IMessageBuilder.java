package com.elsagdatamat.dbtrace.bridge.engine;

import java.util.List;

import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public interface IMessageBuilder {

	public List<String> create(TracesList input) throws Exception;
}
