
package com.elsagdatamat.tt.trackdbws.trackdbrwservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Token_TillToDate_ForEsiPack complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Token_TillToDate_ForEsiPack">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tillToDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="typeSelector" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}TypeSelector"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Token_TillToDate_ForEsiPack", propOrder = {
    "token",
    "tillToDate",
    "typeSelector"
})
public class TokenTillToDateForEsiPack {

    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar tillToDate;
    @XmlElement(required = true)
    protected TypeSelector typeSelector;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the tillToDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTillToDate() {
        return tillToDate;
    }

    /**
     * Sets the value of the tillToDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTillToDate(XMLGregorianCalendar value) {
        this.tillToDate = value;
    }

    /**
     * Gets the value of the typeSelector property.
     * 
     * @return
     *     possible object is
     *     {@link TypeSelector }
     *     
     */
    public TypeSelector getTypeSelector() {
        return typeSelector;
    }

    /**
     * Sets the value of the typeSelector property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeSelector }
     *     
     */
    public void setTypeSelector(TypeSelector value) {
        this.typeSelector = value;
    }

}
