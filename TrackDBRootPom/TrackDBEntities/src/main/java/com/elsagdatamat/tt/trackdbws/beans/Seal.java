package com.elsagdatamat.tt.trackdbws.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SEAL")
public class Seal implements Serializable {

	private static final long serialVersionUID = 1L;
	private String number;
	private Date lastUse;
	
	@Id
	@Column(name="SEAL_NUMBER", nullable=false)
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	@Column(name="LAST_USE", nullable=false)
	public Date getLastUse() {
		return lastUse;
	}
	public void setLastUse(Date lastUse) {
		this.lastUse = lastUse;
	}
	
	
}
