package com.selexelsag.xte.extension.probing.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

public class XteProbeBaseServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5547437820225964250L;
	protected static final String ERROR_VALIDATION = "Request validation error";
	protected static final String ERROR_GENERIC = "Probe application Internal error";
	protected static final String ERROR_JSP_FORWARD = "Error forwarding to error JSP";
	protected static Integer WSTIMEOUT_SCALEFACTOR = 1000;
	protected static final String TIMEOUT_REQ_PARAM = "wsTimeoutSec";
	
	
	public static String getUrl(HttpServletRequest req) {
	    String reqUrl = req.getRequestURL().toString();
	    String queryString = req.getQueryString(); 
	    if (queryString != null) {
	        reqUrl += "?"+queryString;
	    }
	    return reqUrl;
	}
	
	
	
}
