/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.message;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author arodriguez
 *
 */
public class D1PMessageCreator extends OMPAbstractMessageCreator implements Serializable {

	private String destinationOfficeCode = "OMP";
	private String containerCode = null;
	
	/**
	 * @param textMessage
	 */
	public D1PMessageCreator(String textMessage) {
		super(textMessage);
	}

	/**
	 * @param destinationOfficeCode
	 * @param containerCode
	 */
	public D1PMessageCreator(String textMessage, String destinationOfficeCode, String containerCode) {
		this(textMessage);
		this.destinationOfficeCode = destinationOfficeCode;
		this.containerCode = containerCode;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#getSentDate()
	 */
	@Override
	protected String getSentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		return sdf.format(new Date());
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#getSourceOfficeCode()
	 */
	@Override
	protected String getSourceOfficeCode() {
		return DESTINATION_DBTRACE;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#getDestinationOfficeCode()
	 */
	@Override
	protected String getDestinationOfficeCode() {
		return destinationOfficeCode;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#getMessageType()
	 */
	@Override
	protected String getMessageType() {
		return "D1P";
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#isTestMessage()
	 */
	@Override
	protected boolean isTestMessage() {
		return false;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#getContainerCode()
	 */
	@Override
	protected String getContainerCode() {
		return containerCode;
	}

	/**
	 * @see com.selexelsag.dbtrace.bridge.to.OMPAbstractMessageCreator.message.SSIPAbstractMessageCreator#isCompressed()
	 */
	@Override
	protected boolean isCompressed() {
		return false;
	}

}
