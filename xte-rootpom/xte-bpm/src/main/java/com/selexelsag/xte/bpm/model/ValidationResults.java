package com.selexelsag.xte.bpm.model;

import java.util.HashMap;

public class ValidationResults {

	private boolean sintatticSuccess;
	
	private boolean semanticSuccess;
	
	private HashMap<String, String> dataExtract;

	public ValidationResults() {
		sintatticSuccess = true;
		semanticSuccess = true;
	}
	
	public boolean isSintatticSuccess() {
		return sintatticSuccess;
	}

	public void setSintatticSuccess(boolean sintatticSuccess) {
		this.sintatticSuccess = sintatticSuccess;
	}

	public boolean isSemanticSuccess() {
		return semanticSuccess;
	}

	public void setSemanticSuccess(boolean semanticSuccess) {
		this.semanticSuccess = semanticSuccess;
	}

	public HashMap<String, String> getDataExtract() {
		return dataExtract;
	}

	public void setDataExtract(HashMap<String, String> dataExtract) {
		this.dataExtract = dataExtract;
	}
	
	
}
