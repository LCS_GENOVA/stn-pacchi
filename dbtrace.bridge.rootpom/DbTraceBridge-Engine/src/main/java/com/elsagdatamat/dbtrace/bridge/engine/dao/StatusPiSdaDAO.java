package com.elsagdatamat.dbtrace.bridge.engine.dao;


import com.elsagdatamat.dbtrace.bridge.engine.entities.StatusPiSda;
import com.elsagdatamat.dbtrace.bridge.engine.entities.StatusPiSdaPK;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
/** 
* R.L. BeanGenerator - 22.05.2012 03:10:16
*/ 
public class StatusPiSdaDAO extends SpringHibernateJpaDAOBase<StatusPiSda,StatusPiSdaPK> implements IStatusPiSdaDAO{

}