package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.elsagdatamat.framework.dao.IGeneralDAO;
import com.elsagdatamat.framework.dao.IGenericDAO;
import com.elsagdatamat.framework.search.Search;
import com.elsagdatamat.framework.utils.Pair;

public abstract class ExtBaseManager<Z, T, K extends Serializable> implements IExtManager<Z, K> {

	protected Log log = LogFactory.getLog(getClass());
	protected IGenericDAO<T, K> dao;
	protected IGeneralDAO generalDao;

	public IGenericDAO<T, K> getDao() {
		return dao;
	}

	public void setDao(IGenericDAO<T, K> dao) {
		this.dao = dao;
	}

	public IGeneralDAO getGeneralDao() {
		return generalDao;
	}

	public void setGeneralDao(IGeneralDAO generalDao) {
		this.generalDao = generalDao;
	}

	@Override
	public List<Z> findByProperties(Collection<Pair<String, Object>> properties) {
		List<T> tList = this.dao.findByProperties(properties);
		List<Z> zList = new ArrayList<Z>();
		for (T t : tList) {
			Z z = convertTtoZ(t);
			zList.add(z);
		}
		return zList;
	}

	@Override
	public Z findByID(K objectID) {
		T t = dao.findById(objectID, false);
		Z z = convertTtoZ(t);
		return z;
	}

	@Override
	public void delete(Z entity) {
		T t = convertZtoT(entity);
		dao.delete(t);
	}

	@Override
	public void update(Z entity) {
		T t = convertZtoT(entity);
		dao.update(t);
	}

	@Override
	public void insert(Z entity) {
		T t = convertZtoT(entity);
		dao.insert(t);
	}

	@Override
	public Collection<Z> find(Search search) {
		List<T> tList = dao.search(search);
		List<Z> zList = new ArrayList<Z>();
		for (T t : tList) {
			Z z = convertTtoZ(t);
			zList.add(z);
		}
		return zList;
	}

	@Override
	public int updateBatch(Collection<Pair<String, Object>> setProperties, Collection<Pair<String, Object>> conditionProperties) {
		return dao.updateBatch(setProperties, conditionProperties);
	}

	protected abstract Z convertTtoZ(T t);

	protected abstract T convertZtoT(Z z);
}
