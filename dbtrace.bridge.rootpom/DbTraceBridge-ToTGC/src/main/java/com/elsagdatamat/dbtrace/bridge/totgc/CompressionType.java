package com.elsagdatamat.dbtrace.bridge.totgc;

public enum CompressionType {

	NOT_COMPRESSED('N'),
	LEGACY_COMPRESSION('L'),
	STANDARD_COMPRESSION('S');
	
	private char code;

	private CompressionType(char code) {
		this.code = code;
	}
	
	public char getCode() {
		return code;
	}
	
}