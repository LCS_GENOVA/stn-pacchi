
package com.elsagdatamat.tt.trackdbws.trackdbrwservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TraceDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TraceDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paramClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paramValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TraceDetail", propOrder = {
    "paramClass",
    "paramValue"
})
public class TraceDetail {

    @XmlElement(required = true)
    protected String paramClass;
    @XmlElement(required = true)
    protected String paramValue;

    /**
     * Gets the value of the paramClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParamClass() {
        return paramClass;
    }

    /**
     * Sets the value of the paramClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParamClass(String value) {
        this.paramClass = value;
    }

    /**
     * Gets the value of the paramValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParamValue() {
        return paramValue;
    }

    /**
     * Sets the value of the paramValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParamValue(String value) {
        this.paramValue = value;
    }

}
