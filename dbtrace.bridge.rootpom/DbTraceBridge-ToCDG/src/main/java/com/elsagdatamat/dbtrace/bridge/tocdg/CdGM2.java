package com.elsagdatamat.dbtrace.bridge.tocdg;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "MSG")
public class CdGM2 extends CdGMessage {
/*
<?xml version="1.0"?>
<!DOCTYPE MSG SYSTEM "STARMSG.DTD">
<MSG>
<HDR SWREL="A0000" OFCID="11980"/>
<M2>
	<OBJ>
	<OBJID>200000004450</OBJID>
	<PH>D</PH>
	<DF>D</DF>
	<PSTF>1</PSTF>
	<OP>10</OP>
	<TDT>04/03/2004 15:50:00</TDT>
	<SUBC>ACV</SUBC>
	</OBJ>

	<OBJ>
	<OBJID>200000004461</OBJID>
	<PH>D</PH>
	<PSTF>4</PSTF>
	<OP>10</OP>
	<TDT>04/03/2004 15:50:00</TDT>
	<SUBC>ACV</SUBC>
	</OBJ>
</M2>
</MSG>

*/

//	@XmlElement(name = "HDR")
//	public CdGHeader header;
	
	@XmlElementWrapper(name= "M2")
	@XmlElement(name = "OBJ")
	public List<CdGOBJ> objList;
}
