package com.elsagdatamat.dbtrace.dao;

import com.elsagdatamat.dbtrace.entity.PkPrdProdottiSdaVW;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface IPkPrdProdottiSdaVWDAO extends IGenericDAO<PkPrdProdottiSdaVW, String>{

}
