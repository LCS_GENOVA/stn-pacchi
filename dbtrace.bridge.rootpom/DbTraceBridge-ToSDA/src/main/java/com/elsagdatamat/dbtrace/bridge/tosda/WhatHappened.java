package com.elsagdatamat.dbtrace.bridge.tosda;

public enum WhatHappened {
	SSIP_TO_TT,
	ESG_TT,
	ESM_TT,
	ESI_TT,
	ESI_OMP,
	PAR_ICC,
	PAR_CMP,
	PAR_SDA,
	SMI_CMP,
	ACC_CMP,
	AVV_NSP,
	RES_NSP,
	ACC_NSP,
	ACC_PTL,
	ACC_ESI,
	PIC_ICC,
	ARR_ICC,
	CON_MZT,
	UTP_MZT,
	FOR_MRV,
	EXP_TT, ARR_CMP, SDA_UFI, FROM_SSIP, CON_SPE;


	public boolean isIn(WhatHappened ...happeneds){
		for (WhatHappened whatHappened : happeneds)
			if(this==whatHappened)
				return true;
		return false;
	}
}
