/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.main;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolApplicationFrameController;
import com.selexelsag.xte.testtool.datamodel.XteServer;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.XteTestToolUncaughtExceptionHandler;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.xml.Function;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.KeyStroke;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolMain {

    private XteTestToolConfiguration configuration = null;
    private static JFrame applicationFrame= null;
    private static Logger logger = Logger.getLogger(XteTestToolMain.class);

    static {
        Thread.setDefaultUncaughtExceptionHandler(new XteTestToolUncaughtExceptionHandler());
    }

    public XteTestToolMain() {

        try {
            logger.info("XteTestToolMain start");

            //LOAD CONFIGURATION            
            logger.debug("XteTestToolMain load configuration");
            this.configuration = XteTestToolConfiguration.getInstance();
            
            //LOAD GUI            
            logger.debug("XteTestToolMain create and show GUI");
            createGui();
            
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolMain failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolMain failed", ex);
            System.exit(-1);
        }
    }

    private void createGui(){
        XteTestToolDataModel model = new XteTestToolDataModel();
        model.setApplicationID("XTE TEST TOOL");
        
        List<XteServer> xteServerList = new ArrayList<XteServer>();
        
        String[] xteServers;
        xteServers = configuration.getConfiguration().getStringArray("XteServer.BaseUrl");
        for(int i=0; i < xteServers.length; i++){                       
            //check if string array contains the string
            if(!xteServers[i].isEmpty()){
                XteServer xteServer = new XteServer();
                xteServer.setServerBaseURL(xteServers[i]);  
                xteServer.setServerVersion("4.0.0.0");
                xteServer.setServerDescription("XTE Server " + Integer.toString(i+1));
                xteServer.setActive(true);                
                xteServerList.add(xteServer);
            }
        }        
                       
        model.setXteServerList(xteServerList);
      
        List<Function> postalFunctions = configuration.getPostalFunctions();        
        model.setPostalFunctionsList(postalFunctions);
        
        XteTestToolApplicationFrameView view = new XteTestToolApplicationFrameView(model);
        XteTestToolApplicationFrameController controller = new XteTestToolApplicationFrameController(model, view);
        defineExit(view);
        view.setVisible(true);        
    }

    private  static void defineExit(final JFrame applicationFrameView) {

        KeyStroke escapeKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,
        0, false);
        Action escapeAction = new AbstractAction() {
            @Override
        public void actionPerformed(ActionEvent e) {
                //close the frame
                applicationFrameView.dispose();
                System.exit(0);
            }
            };
        applicationFrameView.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escapeKeyStroke,
        "ESCAPE");
        applicationFrameView.getRootPane().getActionMap().put("ESCAPE", escapeAction);

    }

    public static void main(String[] args) {
        new XteTestToolMain();
    }
}
