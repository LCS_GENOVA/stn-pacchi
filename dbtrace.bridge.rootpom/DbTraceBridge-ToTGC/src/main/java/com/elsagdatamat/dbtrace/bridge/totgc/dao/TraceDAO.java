package com.elsagdatamat.dbtrace.bridge.totgc.dao;

import java.util.List;

import com.elsagdatamat.dbtrace.bridge.totgc.vo.AcceptanceInfoVO;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.tt.trackdbws.beans.Trace;


public class TraceDAO extends SpringHibernateJpaDAOBase<Trace, Long> implements ITraceDAO {

	private final static String GET_ACCEPTANCE_INFO_HQL_FORMAT = 
			 "select new com.elsagdatamat.dbtrace.bridge.totgc.vo.AcceptanceInfoVO"
    		+ "(t.whatHappened, td.paramValue) "
    		+ "from Trace t, TraceDetail td "
    		+ "where t.idTracedEntity = '%s' "
    		+ "and t.whatHappened in ('ACC_CMP', 'ACC_NSP') "
    		+ "and t.id=td.trace.id "
    		+ "and td.paramClass='SA'";

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<AcceptanceInfoVO> getAcceptanceInfo(String mpCode) {
		List l = executeHqlQuery(String.format(GET_ACCEPTANCE_INFO_HQL_FORMAT, mpCode));
		return l;
	}
}
