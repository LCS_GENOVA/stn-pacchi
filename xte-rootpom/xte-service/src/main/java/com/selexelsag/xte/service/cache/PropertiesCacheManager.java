package com.selexelsag.xte.service.cache;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.selexelsag.xte.service.cache.CacheMap;
import com.selexelsag.xte.service.cache.ICacheManager;
import com.selexelsag.xte.service.system.IAppConfig;

/**
 * Cache con policy LRU.
 * 
 * La chiave contiene le indicazioni per recuperare dal "base path" il file associato.
 * La chiave avra' la forma del path assoluto del file richiesto
 * 
 * La cache gestisce oggetti properties caricati dai file relativi alle chiavi
 *
 */
public class PropertiesCacheManager implements ICacheManager 
{
	@Autowired
	private IAppConfig appConfig;

	private static Log log = LogFactory.getLog(PropertiesCacheManager.class); 

	private Integer maxItem;
	public Integer getMaxItem() {return maxItem;}
	@Override
	public void setMaxItem(Integer maxItem) {this.maxItem = maxItem;}
	
	private Map<String, Object> cache;
	public Map<String, Object> getCache() {return cache;}
	public void setCache(Map<String, Object> cache) {this.cache = cache;}

	/**
	 * nome del parametro che contiene nella configurazione dell'applicazione il numero di items massimo per la singola istanza
	 */
	private String maxItemsInCacheParam;
	public String getMaxItemsInCacheParam() {return maxItemsInCacheParam;}
	public void setMaxItemsInCacheParam(String maxItemsInCacheParam) {this.maxItemsInCacheParam = maxItemsInCacheParam;}

	/**
	 * nome della cache 
	 */
	private String cacheId;
	public String getCacheId() {return cacheId;}
	public void setCacheId(String cacheId) {this.cacheId = cacheId;}
	
	
	@Override
	public Properties getItem(String key) throws Exception 
	{
		if (cache.containsKey(key))
		{
			if (log.isDebugEnabled())
				log.debug(String.format("getting Item from cache: key=[%1$s]", key));
			return (Properties)cache.get(key);
		}
		Properties  p =  new Properties();
		try 
		{
			p.load(new FileInputStream(key));
		}
		catch (IOException ioe) 
		{
			throw new Exception(String.format("Error putting item in cache: %1$s", key));
		}
		log.debug(String.format("putting Item from cache: key=[%1$s]", key));
		if (log.isDebugEnabled()) 
		{
			log.debug("PROPERTIES LIST");
			Enumeration keys = p.keys();
			while (keys.hasMoreElements()) 
			{
				String savedKey = (String)keys.nextElement();
				log.debug(savedKey + "=" + p.getProperty(savedKey));
			}
			log.debug("END PROPERTIES LIST");
		}
		cache.put(key, p);
		return p;
	}

	@Override
	public void init() throws Exception
	{
		log.info(String.format("init LRU Cache - %1$s BEGINS", cacheId));
		maxItem =  Integer.parseInt(appConfig.getGeneralParam(maxItemsInCacheParam));
		CacheMap<String, Object> cacheMap = new CacheMap<String, Object> (maxItem+1, .75F, true);
		cacheMap.setMaxItems(maxItem);
		cache = (Map<String, Object>)Collections.synchronizedMap(cacheMap);
		log.info("init LRU Cache ENDS");
	}
}
