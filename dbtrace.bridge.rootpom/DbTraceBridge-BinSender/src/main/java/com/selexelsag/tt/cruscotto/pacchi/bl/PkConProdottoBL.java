package com.selexelsag.tt.cruscotto.pacchi.bl;

import java.util.List;

import com.selexelsag.tt.cruscotto.pacchi.bean.PkConProdotto;
import com.selexelsag.tt.cruscotto.pacchi.bean.PkConProdottoPK;
import com.selexelsag.tt.cruscotto.pacchi.dao.IPkConProdottoDAO;

public class PkConProdottoBL implements IPkConProdottoBL{

	IPkConProdottoDAO pkConProdottoDAO;

	@Override
	public Boolean getElemento(PkConProdottoPK pk) {
		PkConProdotto conProdotto = pkConProdottoDAO.findById(pk, false);
		if(conProdotto==null) return null;
		
		return conProdotto.isFlag();
	}

	@Override
	public Boolean getElemento(String prodotto, String canale, String tipoTraccia){ 
		return getElemento(new PkConProdottoPK(canale, prodotto, tipoTraccia));
	}
	
	@Override
	public List<PkConProdotto> getLista() {
		return pkConProdottoDAO.findAll();
	}

	public IPkConProdottoDAO getPkConProdottoDAO() {
		return pkConProdottoDAO;
	}

	public void setPkConProdottoDAO(IPkConProdottoDAO pkConProdottoDAO) {
		this.pkConProdottoDAO = pkConProdottoDAO;
	}
	
	
}
