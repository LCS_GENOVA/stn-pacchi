package com.selexelsag.xte.bpm.orchestrator;

import com.selexelsag.xte.bpm.exceptions.BpmException;
import com.selexelsag.xte.bpm.flows.IProcessManager;
import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.model.track.TrackResponse;

/**
 * Orchestrator che si occupa di indirizzare le richieste nei flussi JBPM
 * @author Frosi
 *
 */
public interface IBPMOrchestrator {
	
	/**
	 * nome del bean dell'orchestrator
	 */
	public static final String BPM_ORCH_BEAN_NAME = "bPMOrchestator";
	
	/**
	 * Innesca l'azione da eseguire in base ai parametri della richiesta
	 * @param trackRequest
	 */
	// TODO: prevedere i tipi eccezione
	public TrackResponse processRequest(TrackRequest trackRequest) 
			throws BpmException;

	
	/**
	 * Il ProcessManager gestisce i processi
	 * attivi nel sistema
	 * @return l'istanza attualmente in uso del ProcessManager
	 */
	public IProcessManager getProcessManager();
	
	
	
	
}
