<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ED="http://www.elsagdatamat.com/Tracking/Messages/xslt/Common">
   <xsl:output indent="yes" method="xml" omit-xml-declaration="no" encoding="UTF-8"/>

	<!-- VARIABILI GLOBALI ESTERNE PASSATE DALL'APPLICATIVO CHIAMANTE -->
	<xsl:param name="gExtSwRel" select="'A0000'" />
	<xsl:param name="gExtDispatchOperator" select="'123'" />
	<xsl:param name="gExtDispatchPhase" select="'D'" />
	
	<xsl:param name="gExtPstf" select="'0'" />
	<xsl:param name="gExtMailpieceOperator" select="$gExtDispatchOperator" />
	<xsl:param name="gExtMailpieceAreaDest" select="'AS'" />
	
	<xsl:param name="gExtBundleOperator" select="$gExtDispatchOperator" />
	<xsl:param name="gExtBundlePhase" select="$gExtDispatchPhase" />
	
	<!-- VARIABILI GLOBALI ESTERNE PASSATE DALL'APPLICATIVO CHIAMANTE RELATIVO ALL'AMBIENTE PACCHI -->
	<xsl:param name="gExtPacchiP2PstfRestituito" select="'4'" />
	<xsl:param name="gExtPacchiP2Pstf" select="'5'" />
	<xsl:param name="gExtPacchiP2Df" select="'D'" />	
	<xsl:param name="gExtPacchiPhaseAcc" select="'W'" />
	<xsl:param name="gExtPacchiPhaseMov" select="'M'" />
	<xsl:param name="gExtPacchiCurr" select="'E'" />
	<xsl:param name="gExtPacchiPstf" select="'0'" />
	<xsl:param name="gExtPacchiOperator" select="'123'" />
	<xsl:param name="gExtPacchiOFCOTH" select="'00000'" />
	
	<!-- Di default il campo REGT contiene il valore S = Registrazione da Sportello Automatizzato (NSP) -->
	<xsl:param name="gExtPacchiRegt" select="'S'" />
	
	<xsl:param name="gExtPacchiOperazioneSportello" select="'1'" />
	<xsl:param name="gExtPacchiTariffaBase" select="'0'" />
	<xsl:param name="gExtPacchiTariffaServiziAggiuntivi" select="'0'" />
	
	
</xsl:stylesheet>
