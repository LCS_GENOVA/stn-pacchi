package com.elsagdatamat.dbtrace.bridge.tosda;

import java.math.BigInteger;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
public class SDASdaUfi {
	
    @XmlElement(name = "STATUS")
    protected String status;
    
    @XmlElement(name = "MODALITA_PAGAMENTO", required = true)
    protected String modalitapagamento;
    
    @XmlElement(name = "CNTRS")
    protected BigInteger cntrs;
    
    @XmlElement(name = "DATA_RIMESSA_CNTRS", required = true)
    protected XMLGregorianCalendar datarimessacntrs;
    
    @XmlElement(name = "NUM_CONTO_CORRENTE")
    protected String numcontocorrente;
    
    @XmlElement(name = "NUM_ASSICURATA")
    protected String numassicurata;

    // R.L.
    @XmlElement(name = "ID_OPERATORE")
    protected String idOperatore;
    @XmlElement(name = "ID_CHIOSCO")
    protected String idChiosco;
    @XmlElement(name = "PIN")
    protected String pin;
    @XmlElement(name = "QR_CODE")
    protected String qrCode;
    @XmlElement(name = "EXPIRATION_DATE")
    protected XMLGregorianCalendar expirationDate;

	public SDASdaUfi() {
		super();
	}
	
    public SDASdaUfi(Map<String, String> map) {
		
		setSTATUS(map.get("STATUS"));
		
		if (map.containsKey("CODVAL"))
			try {
				setCNTRS(new BigInteger(map.get("CODVAL")));  
			} catch(Exception fe) {
			}
		
		if (map.containsKey("WHEN_HAPPENED")) 
			setDATARIMESSACNTRS(ConversionUtility.getInstance().toXMLGregorian(map.get("WHEN_HAPPENED")));
		
		setMODALITAPAGAMENTO(map.get("MOD_RIMBORSO_CONTRASSEGNO")); // default
		
		// below keys are not provided, (may 2012)
		if (map.containsKey("NUM_ASSICURATA"))
			setNUMASSICURATA(map.get("NUM_ASSICURATA")); 
		
		if (map.containsKey("NUM_CONTO_CORRENTE"))
			setNUMCONTOCORRENTE(map.get("NUM_CONTO_CORRENTE")); 
		
		if (map.containsKey("ID_OPERATORE"))
			setIdOperatore(map.get("ID_OPERATORE"));
		if (map.containsKey("ID_CHIOSCO"))
			setIdChiosco(map.get("ID_CHIOSCO"));
        if (map.containsKey("CODICE_CONSEGNA"))
            setPin(map.get("CODICE_CONSEGNA"));
		if (map.containsKey("QR_CODE"))
			setQrCode(map.get("QR_CODE"));
	}

    /**
     * Gets the value of the status property.
     * @return  possible object is {@code <}{@link String }{@code >}
     */
    public String getSTATUS() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * @param value  allowed object is {@code <}{@link String }{@code >}
     */
    protected void setSTATUS(String value) {
        this.status = value;
    }


    /**
     * Gets the value of the modalitapagamento property.
     * @return  possible object is {@link String }
     */
    public String getMODALITAPAGAMENTO() {
        return modalitapagamento;
    }

    /**
     * Sets the value of the modalitapagamento property.
     * @param value  allowed object is {@link String }
     */
    protected void setMODALITAPAGAMENTO(String value) {
        this.modalitapagamento = value;
    }

    /**
     * Gets the value of the cntrs property.
     * @return possible object is {@code <}{@link BigInteger }{@code >}
     */
    public BigInteger getCNTRS() {
        return cntrs;
    }

    /**
     * Sets the value of the cntrs property.
     * @param value  allowed object is {@code <}{@link BigInteger }{@code >}
     */
    protected void setCNTRS(BigInteger value) {
        this.cntrs = value;
    }

    /**
     * Gets the value of the datarimessacntrs property.
     * @return  possible object is {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDATARIMESSACNTRS() {
        return datarimessacntrs;
    }

    /**
     * Sets the value of the datarimessacntrs property.
     * @param value  allowed object is {@link XMLGregorianCalendar }
     */
    protected void setDATARIMESSACNTRS(XMLGregorianCalendar value) {
        this.datarimessacntrs = value;
    }

    /**
     * Gets the value of the numcontocorrente property.
     * @return  possible object is {@code <}{@link String }{@code >}
     */
    public String getNUMCONTOCORRENTE() {
        return numcontocorrente;
    }

    /**
     * Sets the value of the numcontocorrente property.
     * @param value  allowed object is {@code <}{@link String }{@code >}
     */
    protected void setNUMCONTOCORRENTE(String value) {
        this.numcontocorrente = value;
    }

    /**
     * Gets the value of the numassicurata property.
     * @return  possible object is {@code <}{@link String }{@code >}
     */
    public String getNUMASSICURATA() {
        return numassicurata;
    }

    /**
     * Sets the value of the numassicurata property.
     * @param value  allowed object is {@code <}{@link String }{@code >}
     */
    protected void setNUMASSICURATA(String value) {
        this.numassicurata = value;
    }

    // chiosco
    public String getIdOperatore() {
		return idOperatore;
	}

	public void setIdOperatore(String idOperatore) {
		this.idOperatore = idOperatore;
	}

	public String getIdChiosco() {
		return idChiosco;
	}

	public void setIdChiosco(String idChiosco) {
		this.idChiosco = idChiosco;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public XMLGregorianCalendar getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(XMLGregorianCalendar expirationDate) {
		this.expirationDate = expirationDate;
	}
    
}