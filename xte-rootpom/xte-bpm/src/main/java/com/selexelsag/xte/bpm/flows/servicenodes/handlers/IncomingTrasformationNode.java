package com.selexelsag.xte.bpm.flows.servicenodes.handlers;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.drools.process.instance.WorkItemHandler;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemManager;

import com.selexelsag.xte.business.exceptions.BusinessTrasformationException;
import com.selexelsag.xte.business.tracking.ITrasformation;
import com.selexelsag.xte.model.flows.FlowParams;
import com.selexelsag.xte.service.context.SpringApplicationContext;

public class IncomingTrasformationNode  implements WorkItemHandler{

	private Logger logger = LogManager.getLogger(IncomingTrasformationNode.class.getName());
	
	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		logger.debug("executeWorkItem START");
		
		Map<String, Object> results = null;
		
		try{
			
			StringBuilder data = (StringBuilder)workItem.getParameter("data");
			if (data==null) {
				logger.debug("data parameter is null!");
			}
			
			FlowParams flowParams = (FlowParams)workItem.getParameter("flowParams");
			if (flowParams==null) {
				logger.debug("flowParams parameter is null!");
			}
			
			ITrasformation trasformation = (ITrasformation)SpringApplicationContext.
					getBean(ITrasformation.TRASFORMATION_BEAN_NAME);

			results = new HashMap<String, Object>();
			
			StringBuffer trasformationAfterValidation = null;
			
			trasformationAfterValidation = trasformation.requestTrasformation(flowParams, data, flowParams.getParamTransf());
			
			data.delete(0, data.length());
			data.append(trasformationAfterValidation);
			
			results.put("data", data);
			
		}catch(Throwable ex){
			// logger.error(ex, ex);
			results = new HashMap<String, Object>();
			results.put("error", ex);
			results.put("data", (StringBuilder)workItem.getParameter("data"));
		}finally {
			manager.completeWorkItem(workItem.getId(), results);
			logger.debug("executeWorkItem END");

		}

	}

	@Override
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager){
		
		
	}

}
