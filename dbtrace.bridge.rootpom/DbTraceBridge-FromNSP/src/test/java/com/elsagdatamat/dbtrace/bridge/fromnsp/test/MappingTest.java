package com.elsagdatamat.dbtrace.bridge.fromnsp.test;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.List;
import java.util.Scanner;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elsagdatamat.dbtrace.bridge.engine.bl.DbTraceBridgeBL;
import com.elsagdatamat.dbtrace.bridge.engine.exceptions.InsertTracesException;
import com.elsagdatamat.dbtrace.bridge.engine.ws.CustomTraceWs;
import com.elsagdatamat.dbtrace.bridge.engine.ws.ITrackDBRWServiceManager;
import com.elsagdatamat.dbtrace.bridge.jms.JmsToTraceBridge;
import com.elsagdatamat.framework.entities.EntityCreationException;
import com.elsagdatamat.framework.io.StringInputStream;
import com.elsagdatamat.framework.xml.entities.XmlEntityBuilder;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetailsList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext.xml" }) 
public class MappingTest {

	@Resource
	private XmlEntityBuilder<CustomTraceWs> traceBuilder;

	@Resource
	private JmsToTraceBridge jmsToTraceBridge;
	
	
	@Test
	public void messageToTrace()throws Exception{
	    MockTextMessage msg = new MockTextMessage(readFile("p1.xml"));
	   
	    jmsToTraceBridge.onMessage(msg);
	}
	@Test
	public void createMessageTest(){
		try {
			MockTextMessage msg = new MockTextMessage(readFile("p1.xml"));
					
			TracesList traceList = createTraces(msg);
			printToConsole(traceList);
			
			msg = new MockTextMessage(readFile("p2.xml"));
			
			traceList = createTraces(msg);
			printToConsole(traceList);

			msg = new MockTextMessage(readFile("p3.xml"));
			
			traceList = createTraces(msg);
			printToConsole(traceList);
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	
	
	
	private void printToConsole(TracesList lst) throws IllegalAccessException,InvocationTargetException {
		System.out.println("RISULTATO TEST:");
		List<Trace> traces = lst.getTraces();
		for (Trace trace : traces) {
			System.out.println("	TRACE: ");
			String get = "get";
			Method[] getters = trace.getClass().getMethods();
			for (Method getter : getters) {
				if(getter.getName().contains(get)){
					String value = String.valueOf(getter.invoke(trace));
					if(!getter.getName().contentEquals("getClass")){
						if(getter.getName().contentEquals("getTraceDetailsList")){
							TraceDetailsList traceDetailsList = (TraceDetailsList) getter.invoke(trace);
							System.out.println("		TRACEDETAIL: ");
							List<TraceDetail> traceDetails = traceDetailsList.getTraceDetails();
							for (TraceDetail traceDetail : traceDetails) {
								System.out.println("			"+traceDetail.getParamClass()+": " + traceDetail.getParamValue());
							}
							System.out.println("		TRACEDETAIL FINE");
						}else{
							System.out.println("		"+getter.getName()+": " + value);
						}
					}
				}
			}
			System.out.println("	TRACE FINE");
		}
		System.out.println("FINE");
	}
	
	private String readFile(String fileName){
		InputStream is = ClassLoader.getSystemResourceAsStream(fileName);
		StringBuffer sb = new StringBuffer();
		Scanner s = new Scanner(is);
		while(s.hasNext())
			sb.append(s.nextLine());
		return sb.toString();
	}
	
	
	private TracesList createTraces(TextMessage message) throws EntityCreationException, JMSException{
		List<CustomTraceWs> lst = traceBuilder.create(new StringInputStream(message.getText()));
		Assert.assertNotNull(lst);
		TracesList traceList = new TracesList();
		traceList.getTraces().addAll(lst);
		return traceList;
	}
}
