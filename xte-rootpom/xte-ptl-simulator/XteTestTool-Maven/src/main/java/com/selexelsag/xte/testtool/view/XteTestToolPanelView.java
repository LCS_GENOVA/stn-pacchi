/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view;

/**
 *
 * @author Da Procida
 */
public abstract class XteTestToolPanelView extends javax.swing.JPanel implements XteTestToolView{

    private XteTestToolApplicationFrameView parentFrame;

    public XteTestToolPanelView(XteTestToolApplicationFrameView applicationFrame) {
        this.parentFrame = applicationFrame;
    }

    public XteTestToolApplicationFrameView getParentFrame() {
        return parentFrame;
    }

    @Override
    public void customizeView(){};

    @Override
    public void initializeView(){};

    @Override
    public void restoreView(){};

    public void exit()
    {
        parentFrame.dispose();
        System.exit(0);
    };

}
