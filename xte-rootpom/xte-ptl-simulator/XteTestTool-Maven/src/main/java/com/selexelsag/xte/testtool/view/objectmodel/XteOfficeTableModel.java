/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectmodel;

 
import com.selexelsag.xte.testtool.datamodel.XteOffice;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Da Procida
 */
public class XteOfficeTableModel extends DefaultTableModel {

    private String[] columnNames = new String[]{
        "",
        ResourceBundle.getBundle("bundles/messages").getString("office.table.header.ufficio"),
        ResourceBundle.getBundle("bundles/messages").getString("office.table.header.numeromazzetti"),
        ResourceBundle.getBundle("bundles/messages").getString("office.table.header.numeroinvii"),
        ResourceBundle.getBundle("bundles/messages").getString("office.table.header.stato"),
        ResourceBundle.getBundle("bundles/messages").getString("office.table.header.progresso"),
    };

    
    private Map<String, XteOffice> map = new HashMap<String, XteOffice>();
    
    public XteOfficeTableModel() {

        super();
        this.setColumnCount(columnNames.length);
        this.setColumnIdentifiers(columnNames);

    }

    public void addOfficeItem(XteOffice item) throws XteTestToolDuplicateKeyException{
        if(map.containsKey(item.getUfficio()))
        {
            throw new XteTestToolDuplicateKeyException(Group.GENERIC, Severity.WARNING, "Invii Causale Table Model duplicate key exception");
        }
        else
        {
            addRow(new Object[]{"", item.getUfficio(), Long.toString(item.getNumeroMazzetti()), Long.toString(item.getNumeroInvii()), item.getStato(), item.getProgresso()});
            map.put(item.getUfficio(), item);            
        }        
    }
    

    public void setModelData(Map<String, XteOffice> items) {
        getDataVector().clear();
        map.clear();                
        for (XteOffice item : items.values()) {
            addRow(new Object[]{"", item.getUfficio(), Long.toString(item.getNumeroMazzetti()), Long.toString(item.getNumeroInvii()), item.getStato(), item.getProgresso()});
            map.put(item.getUfficio(), item);            
        }
    }

    public XteOffice getSelectedItem(int rowIndex) {
        
        String ufficio = (String) getValueAt(rowIndex, 0);
        if(map.containsKey(ufficio))
        {
            return map.get(ufficio);
        }
        else
        {
            return null;
        }
    }

    public void clearModel() {
        getDataVector().clear();
        map.clear();
    }

    public Map<String, XteOffice> getMap() {
        return map;
    }

    
    @Override
    public boolean isCellEditable(int row, int column) {
        return (false);
    }
    
}
