package com.selexelsag.xte.extension.probing.servlet.tracking;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.selexelsag.commons.XmlUtils;
import com.selexelsag.xte.extension.probing.conf.ConfigurationParams;
import com.selexelsag.xte.extension.probing.listener.ProbeServletContextListener;
import com.selexelsag.xte.extension.probing.model.XteProbeResponse;
import com.selexelsag.xte.extension.probing.service.tracking.ProbeTracking;
import com.selexelsag.xte.extension.probing.servlet.XteProbeBaseServlet;
import com.selexelsag.xte.extension.probing.servlet.exception.ProbeInvalidRequestException;
import com.selexelsag.xte.extension.probing.tracking.IProbeTracking;
import com.selexelsag.xte.extension.probing.tracking.model.XteProbeRequestTracking;

/**
 * Servlet punto di ingresso per le richieste riguardanti il flusso Tracking (TT)
 * @author Frosi
 *
 */
public class XteProbeTrackingServlet extends XteProbeBaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7436742667186287277L;
	private static final String FRAZIONARIO_REQ_PARAM = "frazionario";
	private static Log _log = LogFactory.getLog(XteProbeTrackingServlet.class); 
	
	private IProbeTracking _probeTracking;
	
	public IProbeTracking getProbeTracking() {
		return _probeTracking;
	}

	public void setProbeTracking(IProbeTracking probeTracking) {
		_probeTracking = probeTracking;
	}

	protected XteProbeRequestTracking validateRequest(HttpServletRequest request) throws ProbeInvalidRequestException{
		XteProbeRequestTracking ans = new XteProbeRequestTracking();
		String timeoutParam =  request.getParameter(TIMEOUT_REQ_PARAM);
		try{
			if (timeoutParam != null)
				ans.setTimeout(Integer.parseInt(timeoutParam) * WSTIMEOUT_SCALEFACTOR);
		}catch (NumberFormatException nfe){
			try{
				float msec = (Float.parseFloat(timeoutParam)* (float)WSTIMEOUT_SCALEFACTOR);
				if (msec != (int)msec)
					throw new Exception();
				ans.setTimeout((int)msec);
			}catch (Throwable t){			
				throw new ProbeInvalidRequestException(String.format("Tracking servlet - Invalid request param: %1$s=[%2$s] - Timeout param must be an integer or float with max %3$s decimal digits", TIMEOUT_REQ_PARAM, timeoutParam, String.valueOf((String.valueOf(WSTIMEOUT_SCALEFACTOR).length() -1))));
			}
		}
		String frazionario = request.getParameter(FRAZIONARIO_REQ_PARAM);
		if (frazionario == null || frazionario.trim().equals("")){
			frazionario = ((Properties)getServletContext().getAttribute(ProbeServletContextListener.ATTRIBUTE_NAME)).getProperty(ConfigurationParams.TT_CALL_CHANNELID_KEY);
			if (_log.isDebugEnabled())
				_log.debug(String.format("Tracking servlet, frazionario default value frazionario=[%1$s]", frazionario));
		}
		
		ans.setFrazionario(frazionario);
		return ans;
	}
	
	protected void service(HttpServletRequest request, HttpServletResponse response) {
		PrintWriter out = null;
		try{
			out = response.getWriter();
		}catch (IOException e){
			_log.error("Tracking servlet - Error getting servlet writer", e);
			return;
		}
		try{
			_log.info(String.format("Tracking servlet BEGINS: requestid=[%1$s],  url=[%2$s]",request.hashCode(), getUrl(request)));
			request.setCharacterEncoding("UTF-8");
			XteProbeRequestTracking inputWs = validateRequest(request);
			XteProbeResponse outputWs = new XteProbeResponse();
			IProbeTracking service = new ProbeTracking();
			if (_log.isDebugEnabled()){
				_log.debug("setting properties BEGINS");
				_log.debug((Properties)getServletContext().getAttribute(ProbeServletContextListener.ATTRIBUTE_NAME));
			}
			service.setConfigurationProperties((Properties)getServletContext().getAttribute(ProbeServletContextListener.ATTRIBUTE_NAME));
			if (_log.isDebugEnabled())
				_log.debug("setting properties ENDS");

			
			_log.info(String.format("Tracking servlet ws call BEGINS: requestid=[%1$s]", request.hashCode()));
			outputWs = service.probe(inputWs, outputWs);
			_log.info(String.format("Tracking servlet ws call ENDS: requestid=[%1$s]", request.hashCode()));
			
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/xml");
			_log.info(String.format("Tracking servlet send response BEGINS: requestid=[%1$s]", request.hashCode()));
			out.println(XmlUtils.serializeJaxbClass(XteProbeResponse.class, outputWs));
			_log.info(String.format("Tracking servlet send response ENDS: requestid=[%1$s]", request.hashCode()));
		} 
		catch (ProbeInvalidRequestException reqExc){
			_log.error(ERROR_VALIDATION, reqExc);
			try{
				request.setAttribute("errorDesc", reqExc.getMessage());
			  getServletContext().getRequestDispatcher("/WEB-INF/exceptionError.jsp").forward(request, response);
			}catch(Throwable t){
				_log.error(ERROR_JSP_FORWARD, t);
			}
		}catch (Throwable e){
			_log.error(ERROR_GENERIC, e);
			try{
				request.setAttribute("errorDesc", ERROR_GENERIC);
			  getServletContext().getRequestDispatcher("/WEB-INF/exceptionError.jsp").forward(request, response);
			}catch(Throwable t){
				_log.error("Tracking servlet - Error forwarding to error response", t);
			}
		}finally{
			if (out != null){
				out.flush();
				out.close();
			}
		}
		_log.info(String.format("Tracking servlet ENDS: requestid=[%1$s]", request.hashCode()));
	}
}