package com.elsagdatamat.tt.trackdbws.daoimpl;


import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.tt.trackdbws.dao.IPkConMappingDettTrDAO;
import com.selexelsag.tt.cruscottopacchi.bean.PkConMappingDettTr;
import com.selexelsag.tt.cruscottopacchi.bean.PkConMappingDettTrPK;
/** 
* R.L. BeanGenerator - 27.05.2012 18:17:43
* R.L. - Inseriti per il nuovo metodo del trackDbWS - packDataRetrieve
*/ 
public class PkConMappingDettTrDAO extends SpringHibernateJpaDAOBase<PkConMappingDettTr,PkConMappingDettTrPK> implements IPkConMappingDettTrDAO{

}