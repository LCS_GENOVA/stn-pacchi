/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.common;

import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author Da Procida
 */
public class XteTestToolFocusTraversalPolicy extends FocusTraversalPolicy {

    private SortedMap<Integer, Component> componentSortedMap;

    public XteTestToolFocusTraversalPolicy(SortedMap componentSortedMap) {

        this.componentSortedMap = componentSortedMap;
    }

    @Override
    public Component getFirstComponent(Container focusCycleRoot) {
        SortedMap<Integer, Component> first = componentSortedMap;
        return (Component) first.get(first.firstKey());
    }

    @Override
    public Component getLastComponent(Container focusCycleRoot) {
        SortedMap<Integer, Component> last = componentSortedMap;
        return (Component) last.get(last.lastKey());
    }

    @Override
    public Component getDefaultComponent(Container focusCycleRoot) {
        return getFirstComponent(focusCycleRoot);
    }

    @Override
    public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {

        SortedMap<Integer, Component> after = componentSortedMap;

        SortedMap nextComponents = null;
        SortedMap nextFocusableComponents = new TreeMap();
        Iterator iterator = after.keySet().iterator();
        while (iterator.hasNext()) {
            Object key = iterator.next();
            if (((Component) after.get((Integer) key)).equals(aComponent)) {
                nextComponents = after.tailMap((Integer) key + 1);
                break;
            }
        }

        Iterator iteratorF = nextComponents.keySet().iterator();
        while (iteratorF.hasNext()) {
            Object keyF = iteratorF.next();
            if (((Component) nextComponents.get((Integer) keyF)).isFocusable()) {
                nextFocusableComponents.put(keyF, nextComponents.get((Integer) keyF));
            }
        }

        if (nextFocusableComponents.isEmpty() || nextFocusableComponents == null) {
            if (!after.isEmpty()) {
                return (Component) after.get(after.firstKey());
            }
            return null;
        }
        return (Component) nextFocusableComponents.get(nextFocusableComponents.firstKey());

    }

    @Override
    public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {

        SortedMap<Integer, Component> before = componentSortedMap;
        SortedMap prevComponents = null;
        SortedMap prevFocusableComponents = new TreeMap();
        Iterator iterator = before.keySet().iterator();
        while (iterator.hasNext()) {
            Object key = iterator.next();
            if (((Component) before.get((Integer) key)).equals(aComponent)) {
                prevComponents = before.headMap((Integer) key);
                break;
            }
        }
        Iterator iteratorP = prevComponents.keySet().iterator();
        while (iteratorP.hasNext()) {
            Object keyP = iteratorP.next();
            if (((Component) prevComponents.get((Integer) keyP)).isFocusable()) {
                prevFocusableComponents.put(keyP, prevComponents.get((Integer) keyP));
            }
        }

        if (prevFocusableComponents.isEmpty() || prevFocusableComponents == null) {
            if (!before.isEmpty()) {
                return (Component) before.get(before.lastKey());
            }
            return null;
        }
        return (Component) prevFocusableComponents.get(prevFocusableComponents.lastKey());

    }
}

