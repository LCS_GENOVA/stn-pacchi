package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.CodiciProdottoPICodiciProdottoSDA;
import com.elsagdatamat.dbtrace.bridge.engine.entities.CodiciProdottoPICodiciProdottoSDAPK;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class CodiciProdottoPICodiciProdottoSDADAO extends SpringHibernateJpaDAOBase<CodiciProdottoPICodiciProdottoSDA, CodiciProdottoPICodiciProdottoSDAPK> implements ICodiciProdottoPICodiciProdottoSDADAO{

}
