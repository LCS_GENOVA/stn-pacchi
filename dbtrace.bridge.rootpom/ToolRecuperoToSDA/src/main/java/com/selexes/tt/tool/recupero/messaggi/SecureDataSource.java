package com.selexes.tt.tool.recupero.messaggi;

import java.io.IOException;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import sun.misc.BASE64Decoder;

	public class SecureDataSource extends DriverManagerDataSource{

		@Override
		public String getPassword() {
			String password = super.getPassword();
			return decode(password);
		}

	    private String decode(String password) {
	        BASE64Decoder decoder = new BASE64Decoder();
	        String decodedPassword = null;
	        try {
	            decodedPassword = new String(decoder.decodeBuffer(password));
	        } catch (IOException e) {
	            e.printStackTrace();
	        }       
	        return decodedPassword;
	    }
}