package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.Parameter;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface IParameterDAO extends IGenericDAO<Parameter, String>{
}
