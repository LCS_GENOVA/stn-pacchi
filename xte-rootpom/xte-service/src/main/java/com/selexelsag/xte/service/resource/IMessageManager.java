package com.selexelsag.xte.service.resource;

/**
 * classe per recuperare dei messaggi per comunicare con l'esterno (ad esempio messaggi di errore).
 *
 */
public interface IMessageManager 
{
	String getMessageText(String errorMessageCode);
	
	public static final String TRACK_MSG_MANAGER_BEAN = "trackMessageManager";	
	public static String RESULT_OK_KEY = "OkKey";
	public static String RESULT_POSITIVO_KEY = "PositivoKey";
	public static String RESULT_NEGATIVO_KEY = "NegativoKey";
	public static String RESULT_REITERATION_N_KEY = "ReiterationNKey";
	// TODO:  nessuna evidenza che se si deve reiterare sia Y e non S. verificare il valore
	public static String RESULT_REITERATION_Y_KEY = "ReiterationYKey";
	public static String RESULT_ERROR_NOK_KEY = "ErrorNokKey";
	public static String RESULT_ERROR_INTERNAL_KEY = "ErrorInternalKey";
	public static String RESULT_ERROR_INTERNAL_CONFIGURATION_KEY = "ErrorInternalConfiguationKey";
	
}
