/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.xml;

import com.selexelsag.xtetesttool.xml.accettazioneonline.AccettazioneOnline;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta;
import com.selexelsag.xtetesttool.xml.lookup.Lookup;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

/**
 *
 * @author Da Procida
 */
public class XmlFileManager {

    public static final File POSTAL_FUNCTIONS_SCHEMA_FILE = new File("xml/XtePostalFunctions.xsd");
    public static final File POSTA_REGISTRATA_CONFIG_SCHEMA_FILE = new File("xml/PostaRegistrata/PostaRegistrataConfig.xsd");
    public static final File DATA_POSTA_CONFIG_SCHEMA_FILE = new File("xml/DataPosta/DataPostaConfig.xsd");
    public static final File ACCETTAZIONE_ONLINE_CONFIG_SCHEMA_URL = new File("xml/AccettazioneOnline/AccettazioneOnlineConfig.xsd");
    public static final File LOOKUP_CONFIG_SCHEMA_URL = new File("xml/Lookup/LookupConfig.xsd");
    private Logger logger = Logger.getLogger(XmlFileManager.class);

    public XmlFileManager() {
    }

    public List<Function> createPostalFunctionsMenuFromXML(String fileName) {

        FileReader fileReader = null;
        try {
            logger.info("XmlFileManager start createPostalFunctionsMenuFromXML");
            JAXBContext jaxbContext = JAXBContext.newInstance(Menu.class);
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(POSTAL_FUNCTIONS_SCHEMA_FILE);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setSchema(schema);

            fileReader = new FileReader(fileName);
            Menu functionsMenu = (Menu) unmarshaller.unmarshal(fileReader);

            List<Function> postalFunctions = functionsMenu.getFunction();
            logger.info("XmlFileManager createPostalFunctionsMenuFromXML completed successfully");

            return postalFunctions;

        } catch (FileNotFoundException ex) {
            logger.error("XmlFileManager createPostalFunctionsMenuFromXML failed: unable to find xml file: " + fileName, ex);
            return null;
        } catch (SAXException ex) {
            logger.error("XmlFileManager createPostalFunctionsMenuFromXML failed: unable to read xml file (SAX Exception): " + fileName, ex);
            return null;
        } catch (JAXBException ex) {
            logger.error("XmlFileManager createPostalFunctionsMenuFromXML failed: unable to read xml file (JAXB Exception): " + fileName, ex);
            return null;
        } catch (Exception ex) {
            logger.error("XmlFileManager createPostalFunctionsMenuFromXML failed: unable to read xml file (Generic Exception): " + fileName, ex);
            return null;
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException ex) {
            }
        }

    }

    public PostaRegistrata loadPostaRegistrataConfigFromXML(String fileName) {

        FileReader fileReader = null;
        try {
            logger.info("XmlFileManager start loadPostaRegistrataConfigFromXML");
            JAXBContext jaxbContext = JAXBContext.newInstance(PostaRegistrata.class);
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(POSTA_REGISTRATA_CONFIG_SCHEMA_FILE);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setSchema(schema);

            fileReader = new FileReader(fileName);
            PostaRegistrata postaRegistrataConfig = (PostaRegistrata) unmarshaller.unmarshal(fileReader);

            logger.info("XmlFileManager loadPostaRegistrataConfigFromXML completed successfully");

            return postaRegistrataConfig;

        } catch (FileNotFoundException ex) {
            logger.error("XmlFileManager loadPostaRegistrataConfigFromXML failed: unable to find xml file: " + fileName, ex);
            return null;
        } catch (SAXException ex) {
            logger.error("XmlFileManager loadPostaRegistrataConfigFromXML failed: unable to read xml file (SAX Exception): " + fileName, ex);
            return null;
        } catch (JAXBException ex) {
            logger.error("XmlFileManager loadPostaRegistrataConfigFromXML failed: unable to read xml file (JAXB Exception): " + fileName, ex);
            return null;
        } catch (Exception ex) {
            logger.error("XmlFileManager loadPostaRegistrataConfigFromXML failed: unable to read xml file (Generic Exception): " + fileName, ex);
            return null;
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException ex) {
            }
        }

    }
    
    public DataPosta loadDataPostaConfigFromXML(String fileName) {

        FileReader fileReader = null;
        try {
            logger.info("XmlFileManager start loadDataPostaConfigFromXML");
            JAXBContext jaxbContext = JAXBContext.newInstance(DataPosta.class);
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(DATA_POSTA_CONFIG_SCHEMA_FILE);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setSchema(schema);

            fileReader = new FileReader(fileName);
            DataPosta dataPostaConfig = (DataPosta) unmarshaller.unmarshal(fileReader);

            logger.info("XmlFileManager loadDataPostaConfigFromXML completed successfully");

            return dataPostaConfig;

        } catch (FileNotFoundException ex) {
            logger.error("XmlFileManager loadDataPostaConfigFromXML failed: unable to find xml file: " + fileName, ex);
            return null;
        } catch (SAXException ex) {
            logger.error("XmlFileManager loadDataPostaConfigFromXML failed: unable to read xml file (SAX Exception): " + fileName, ex);
            return null;
        } catch (JAXBException ex) {
            logger.error("XmlFileManager loadDataPostaConfigFromXML failed: unable to read xml file (JAXB Exception): " + fileName, ex);
            return null;
        } catch (Exception ex) {
            logger.error("XmlFileManager loadDataPostaConfigFromXML failed: unable to read xml file (Generic Exception): " + fileName, ex);
            return null;
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException ex) {
            }
        }

    }
    
    public AccettazioneOnline loadAccettazioneOnlineConfigFromXML(String fileName) {

        FileReader fileReader = null;
        try {
            logger.info("XmlFileManager start loadAccettazioneOnlineConfigFromXML");
            JAXBContext jaxbContext = JAXBContext.newInstance(AccettazioneOnline.class);
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(ACCETTAZIONE_ONLINE_CONFIG_SCHEMA_URL);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setSchema(schema);

            fileReader = new FileReader(fileName);
            AccettazioneOnline accettazioneOnlineConfig = (AccettazioneOnline) unmarshaller.unmarshal(fileReader);

            logger.info("XmlFileManager loadAccettazioneOnlineConfigFromXML completed successfully");

            return accettazioneOnlineConfig;

        } catch (FileNotFoundException ex) {
            logger.error("XmlFileManager loadAccettazioneOnlineConfigFromXML failed: unable to find xml file: " + fileName, ex);
            return null;
        } catch (SAXException ex) {
            logger.error("XmlFileManager loadAccettazioneOnlineConfigFromXML failed: unable to read xml file (SAX Exception): " + fileName, ex);
            return null;
        } catch (JAXBException ex) {
            logger.error("XmlFileManager loadAccettazioneOnlineConfigFromXML failed: unable to read xml file (JAXB Exception): " + fileName, ex);
            return null;
        } catch (Exception ex) {
            logger.error("XmlFileManager loadAccettazioneOnlineConfigFromXML failed: unable to read xml file (Generic Exception): " + fileName, ex);
            return null;
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException ex) {
            }
        }

    }
    
    public Lookup loadLookupConfigFromXML(String fileName) {

        FileReader fileReader = null;
        try {
            logger.info("XmlFileManager start loadLookupConfigFromXML");
            JAXBContext jaxbContext = JAXBContext.newInstance(Lookup.class);
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(LOOKUP_CONFIG_SCHEMA_URL);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setSchema(schema);

            fileReader = new FileReader(fileName);
            Lookup lookupConfig = (Lookup) unmarshaller.unmarshal(fileReader);

            logger.info("XmlFileManager loadLookupConfigFromXML completed successfully");

            return lookupConfig;

        } catch (FileNotFoundException ex) {
            logger.error("XmlFileManager loadLookupConfigFromXML failed: unable to find xml file: " + fileName, ex);
            return null;
        } catch (SAXException ex) {
            logger.error("XmlFileManager loadLookupConfigFromXML failed: unable to read xml file (SAX Exception): " + fileName, ex);
            return null;
        } catch (JAXBException ex) {
            logger.error("XmlFileManager loadLookupConfigFromXML failed: unable to read xml file (JAXB Exception): " + fileName, ex);
            return null;
        } catch (Exception ex) {
            logger.error("XmlFileManager loadLookupConfigFromXML failed: unable to read xml file (Generic Exception): " + fileName, ex);
            return null;
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException ex) {
            }
        }

    }

}
