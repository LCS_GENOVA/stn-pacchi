package com.elsagdatamat.dbtrace.bridge.tocdg;

public class ToCDGM1Token extends ToCDGToken {
	public static final String TOKEN_ID="DBTraceToCDGM1Bridge";
	
	@Override
	public String getServiceId() {
		return TOKEN_ID;
	}
}
