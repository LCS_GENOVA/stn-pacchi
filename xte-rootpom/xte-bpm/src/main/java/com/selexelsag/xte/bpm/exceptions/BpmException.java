package com.selexelsag.xte.bpm.exceptions;

import com.selexelsag.xte.service.exceptions.BaseException;

public class BpmException extends BaseException {

	public BpmException(String message, Throwable cause) {
		super(message, cause);
		setExceptionId(BPM_ID);
	}
	
	public BpmException(String msg) {
		super(msg);
	}
}
