/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.pr;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XteMailPiece;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;
import com.selexelsag.xte.testtool.view.pr.XteTestToolLasciaMazzettiPRView;
import com.selexelsag.xte.testtool.view.pr.XteTestToolWebServicePRView;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata.LM;
import com.selexelsag.xtetesttool.xml.postaregistrata.lm.ObjectFactory;
import com.selexelsag.xtetesttool.xml.postaregistrata.lm.Traccia;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolLasciaMazzettiPRController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener {

    private XteTestToolLasciaMazzettiPRView lasciaMazzettiPRView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolLasciaMazzettiPRController.class);
    private XteMailPiece selectedMailPiece = null;
    private int currentSelectedRow = -1;
    private List<JTextComponent> mailPieceFieldList = new ArrayList<JTextComponent>();
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    
    private JAXBContext context;
    
    private static Traccia lasciaMazzettiTraccia;
    
    static {
        logger.debug("Creating Config Object Factory and Traccia");
        PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        LM config = postaRegistrataConfig.getLM();

        lasciaMazzettiTraccia = objectFactory.createTraccia();
        lasciaMazzettiTraccia.setEvento(config.getEventName());
    }
  

    public XteTestToolLasciaMazzettiPRController(JPanel lasciaMazzettiPRView) {
        try {
            logger.info("XteTestToolLasciaMazzettiPRController start class creation");
            this.lasciaMazzettiPRView = (XteTestToolLasciaMazzettiPRView) lasciaMazzettiPRView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.lasciaMazzettiPRView.initializeView();

            updateView();

            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.postaregistrata.lm");
            
            logger.info("XteTestToolLasciaMazzettiPRController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolLasciaMazzettiPRController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolLasciaMazzettiPRController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Posta Registrata controller creation failed", ex);
        }

    }
    
    private void updateDataModel() {
        String id = lasciaMazzettiPRView.getBundleIdTextRef().getText();
        String opid = lasciaMazzettiPRView.getBundleTPIdTextRef().getText();
        logger.debug("updateDataModel - " + id + "," + opid);
        
        lasciaMazzettiTraccia.setCodiceIdentificativo(new String(id));
        lasciaMazzettiTraccia.setIdOperatoreTP(new String(opid));
    }
    
    
    private void updateView() {  
        String id = lasciaMazzettiTraccia.getCodiceIdentificativo();
        String opid = lasciaMazzettiTraccia.getIdOperatoreTP();
        logger.debug("updateView - " + id + "," + opid);
                
        if (id != null) {
            lasciaMazzettiPRView.getBundleIdTextRef()
                    .setText(id);
        }

        if (opid != null) {
            lasciaMazzettiPRView.getBundleTPIdTextRef()
                    .setText(opid);
        }

    }
    
    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = lasciaMazzettiPRView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        lasciaMazzettiPRView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.lasciaMazzettiPRView.getLasciaMazzettiPanelRef().addAncestorListener(this);        
        this.lasciaMazzettiPRView.getBundleIdTextRef().addFocusListener(this);
        this.lasciaMazzettiPRView.getBundleTPIdTextRef().addFocusListener(this);
        this.lasciaMazzettiPRView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.lasciaMazzettiPRView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.lasciaMazzettiPRView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.lasciaMazzettiPRView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.lasciaMazzettiPRView.getBackButtonRef().addActionListener(this);
        this.lasciaMazzettiPRView.getHomeButtonRef().addActionListener(this);
        this.lasciaMazzettiPRView.getExitButtonRef().addActionListener(this);        

        this.salvaXmlFileChooser.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(lasciaMazzettiPRView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolLasciaMazzettiPRController actionPerformed invoke generaXmlData");

            updateDataModel();

            generaXmlData();
        }

        if(source.equals(lasciaMazzettiPRView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolLasciaMazzettiPRController actionPerformed invoke impostaDatiWebService");

            updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(lasciaMazzettiPRView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolLasciaMazzettiPRController actionPerformed invoke salvaXmlData");

            updateDataModel();

            salvaXmlData();
        }

        if(source.equals(lasciaMazzettiPRView.getBackButtonRef()))
        {
            logger.debug("XteTestToolLasciaMazzettiPRController actionPerformed invoke back");

            updateDataModel();

            back();
        }

        if(source.equals(lasciaMazzettiPRView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolLasciaMazzettiPRController actionPerformed invoke home");

            updateDataModel();

            home();
        }

        if(source.equals(lasciaMazzettiPRView.getExitButtonRef()))
        {
            logger.debug("XteTestToolLasciaMazzettiPRController actionPerformed invoke exit");

            updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(lasciaMazzettiPRView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolLasciaMazzettiPRView)
        {
            logger.debug("XteTestToolLasciaMazzettiPRController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiPRController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Posta Registrata controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiPRController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Posta Registrata controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }    
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        lasciaMazzettiPRView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolLasciaMazzettiPRController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.lasciaMazzettiPRView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(lasciaMazzettiPRView);
            updateXteTestToolDataModel(dataModel);

            clearMessageBar();

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(lasciaMazzettiPRView.getImpostaDatiWSButtonRef());
            disabledComponents.add(lasciaMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            lasciaMazzettiPRView.getBundleIdTextRef().requestFocus();

            logger.info("XteTestToolLasciaMazzettiPRController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolLasciaMazzettiPRController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiPRController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Posta Registrata controller init failed", ex);
        }

    }
    
    private void exit()
    {
        logger.info("XteTestToolLasciaMazzettiPRController start exit");

        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(lasciaMazzettiPRView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolLasciaMazzettiPRController exit in progress");
            lasciaMazzettiPRView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolLasciaMazzettiPRController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(lasciaMazzettiPRView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(lasciaMazzettiPRView.getLasciaMazzettiPanelRef().getParent(), lasciaMazzettiPRView.getLasciaMazzettiPanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolLasciaMazzettiPRController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(lasciaMazzettiPRView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(lasciaMazzettiPRView.getLasciaMazzettiPanelRef().getParent(), lasciaMazzettiPRView.getLasciaMazzettiPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }
    
    private void generaXmlData()
    {
        logger.info("XteTestToolLasciaMazzettiPRController start generaXmlData");

        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();
        
        lasciaMazzettiTraccia.setCodiceIdentificativo(lasciaMazzettiPRView.getBundleIdTextRef().getText());
        lasciaMazzettiTraccia.setIdOperatoreTP(lasciaMazzettiPRView.getBundleTPIdTextRef().getText());
        
        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            logger.error("Error creating marshaller", ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            lasciaMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(lasciaMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(lasciaMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            logger.error("Error setting marshaller property", ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            lasciaMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(lasciaMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(lasciaMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(lasciaMazzettiTraccia, out);
        } catch (JAXBException ex) {
            logger.error("error marshalling", ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            lasciaMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(lasciaMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(lasciaMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
            
        try
        {            
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString());
            lasciaMazzettiPRView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("XteTestToolLasciaMazzettiPRController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolLasciaMazzettiPRController generaXmlData failed", baseEx);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(baseEx.toString(), MessageType.ERROR);
            lasciaMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(lasciaMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(lasciaMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiPRController generaXmlData failed", ex);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            lasciaMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(lasciaMazzettiPRView.getImpostaDatiWSButtonRef());
            disableComponent.add(lasciaMazzettiPRView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        lasciaMazzettiPRView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(lasciaMazzettiPRView.getImpostaDatiWSButtonRef());
        enableComponent.add(lasciaMazzettiPRView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolLasciaMazzettiPRController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServicePRView(lasciaMazzettiPRView.getParentFrame());
            XteTestToolWebServicePRController controller = new XteTestToolWebServicePRController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
			PostaRegistrata postaRegistrataConfig = XteTestToolConfiguration.getInstance().getPostaRegistrataConfig();
            LM config = postaRegistrataConfig.getLM();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(lasciaMazzettiPRView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(lasciaMazzettiPRView.getLasciaMazzettiPanelRef().getParent(), lasciaMazzettiPRView.getLasciaMazzettiPanelRef(), webserviceview, new Rectangle(0,70,1000,690));

            logger.info("XteTestToolLasciaMazzettiPRController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolLasciaMazzettiPRController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiPRController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Posta Registrata controller impostaDatiWebService failed", ex);
        }
    }

    class XmlFileFilter extends FileFilter {
        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolLasciaMazzettiPRController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolLasciaMazzettiPRController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(lasciaMazzettiPRView.getParentFrame());

            logger.info("XteTestToolLasciaMazzettiPRController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolLasciaMazzettiPRController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolLasciaMazzettiPRController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Lascia Mazzetti Posta Registrata controller salvaXmlData failed", ex);
        }
    }
}
