/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.pr;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.view.XteTestToolPanelView;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolCodeDocument;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolIntegerDocument;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolStringDocument;
import com.selexelsag.xte.testtool.view.objectmodel.XteListSelectionModel;
import com.selexelsag.xte.testtool.view.objectmodel.XtePreAccettazioneTableModel;
import com.selexelsag.xte.testtool.view.objectrenderer.XteMailPieceTableCellRenderer;
import com.selexelsag.xte.testtool.view.objectrenderer.XteTableHeaderRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.PlainDocument;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author Tassara
 */
public class XteTestToolPreAccettazionePRView extends XteTestToolPanelView{
    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolPreAccettazionePRView.class);
    private XtePreAccettazioneTableModel xtePreAccettazionePRTableModel = new XtePreAccettazioneTableModel();;
    
    public XteTestToolPreAccettazionePRView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try {
            logger.info("XteTestToolPreAccettazionePRView start class creation");
            this.messages = ResourceBundle.getBundle("bundles/messages");
            initComponents();
            logger.info("XteTestToolPreAccettazionePRView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolPreAccettazionePRView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata view creation failed", ex);
        }
    }

    public JPanel getPreAccettazionePanelRef(){ return this; }        
    public JLabel getPreAccettazioneTitleLabelRef(){ return this.panelTitlejLabel; }
    
    public JPanel getBundleDataPaneRef(){ return this.bundleDatajPanel; }
    public JTextField getPATipoTextRef(){ return this.PATipojTextField; }
    public JTextField getPANumSpedTextRef(){ return this.PANumSpedjTextField; }
    public JTextField getPACodeClienteTextRef(){ return this.PACodeClienteJTextField; }
    public JTextField getPANumOggTextRef(){ return this.PANumOggjTextField; }
    public JXDatePicker getPADatePickerRef(){ return this.PADatejXDatePicker; }
    public JSpinner getPATimeSpinnerRef(){ return this.PATimejSpinner; }
    public JTextField getPAFrazTextRef(){ return this.PAFrazjTextField; }
    public JComboBox getPATipoMexJComboBoxRef(){ return this.PATipoMexjComboBox; }
    
    public JPanel getPADatPAaneRef(){ return this.PreAccettazionDatajPanel; }
    public JTextField getPACodiceInvioTextRef(){ return this.PACodeInviojTextField; }
    public JTextField getPACausaleTextRef(){ return this.PACausalejTextField; }
    public JTextField getPADestinazioneTextRef(){ return this.PADestinazionejTextField; }
    public JTextField getPADestinatarioTextRef(){ return this.PADestinatariojTextField; }
    public JTextField getPAValAssTextRef(){ return this.PAValAssjTextField; }
    public JTextField getPAPeso1TextRef(){ return this.PAPeso1jTextField; }
    public JTextField getPAPeso2TextRef(){ return this.PAPeso2jTextField; }
    public JTextField getPANumSigilliTextRef(){ return this.PANumSigillijTextField; }
    public JTextField getPANumVagliaTextRef(){ return this.PANumVagliajTextField; }
    public JTextField getPAEspressoTextRef(){ return this.PAEspressojTextField; }
    public JTextField getPACAPTextRef(){ return this.PACAPjTextField; }
    public JTextField getPAIndirizzoTextRef(){ return this.PAIndirizzojTextField; }
    public JTextField getPAImpContrTextRef(){ return this.PAImpContrjTextField; }
    public JTextField getPACodeARTextRef(){ return this.PACodeARjTextField; }
    public JTextField getPASATextRef(){ return this.PASAjTextField; }
    public JTextField getPAPARAMTextRef(){ return this.PAParametrijTextField; }
    
    public void setPAFields(){  
        Calendar date = Calendar.getInstance();
        this.PATipojTextField.setText("A");
        this.PANumSpedjTextField.setText("S1206");
        this.PACodeClienteJTextField.setText("PR12345"); 
        this.PANumOggjTextField.setText("5");
        this.PADatejXDatePicker.transferFocus();
        this.PADatejXDatePicker.setDate(date.getTime());
        this.PATimejSpinner.setValue(date.getTime());
        this.PAFrazjTextField.setText("28428");  
        this.PATipoMexjComboBox.setSelectedIndex(0);
        
        this.PACodeInviojTextField.setText("000000000544"); 
        this.PACausalejTextField.setText("AVL"); 
        this.PADestinazionejTextField.setText("SANTA MARGHERITA ME"); 
        this.PADestinatariojTextField.setText("MAGAZZU' CONCETTA"); 
        this.PAValAssjTextField.setText("2360"); 
        this.PAPeso1jTextField.setText("50"); 
        this.PAPeso2jTextField.setText("120"); 
        this.PANumSigillijTextField.setText("2"); 
        this.PANumVagliajTextField.setText("1");  
        this.PAEspressojTextField.setText("E"); 
        this.PACAPjTextField.setText("98135"); 
        this.PAIndirizzojTextField.setText("VIA COMUNALE 209"); 
        this.PAImpContrjTextField.setText("1250"); 
        this.PACodeARjTextField.setText("231054698775"); 
        this.PASAjTextField.setText("ASC");
    }
    
    public JButton getTestButtonRef(){ return this.TestjButton;}
    
    public JTable getPreAccettazioneTableRef(){ return this.PreAccettazionejTable; }
    public XtePreAccettazioneTableModel getPreAccettazioneTableModelRef(){ return (XtePreAccettazioneTableModel) this.PreAccettazionejTable.getModel(); }
    public JPanel getMailPieceDataCommandPaneRef(){ return this.mailPieceDataCommandjPanel; }
    public JButton getAggiungiItemButtonRef(){ return this.addPreAccettazionejButton; }
    public JButton getEliminaItemButtonRef(){ return this.removePreAccettazionejButton; }
    
    public JPanel getXmlDataPaneRef(){ return this.xmlDatajPanel; }
    public JScrollPane getXmlDataScrollPaneRef(){ return this.xmlDatajScrollPane; }
    public JTextArea getXmlDataTextAreaRef(){ return this.xmlDatajTextArea; }
    public JPanel getXmlDataCommandPaneRef(){ return this.xmlDataCommandjPanel; }    
    public JButton getGeneraXmlDataButtonRef(){ return this.generaXmlDatajButton; }
    public JButton getImpostaDatiWSButtonRef(){ return this.impostaDatiWSjButton; }
    public JButton getSalvaXmlDataButtonRef(){ return this.salvaXmlDatajButton; }
    
    public JButton getBackButtonRef(){ return this.backjButton; }
    public JButton getHomeButtonRef(){ return this.homejButton; }
    public JButton getExitButtonRef(){ return this.exitjButton; }
    public JTextPane getMessageBarTextPaneRef(){ return this.messageBarjTextPane; }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolPreAccettazionePRView start customizeView");
            //CUSTOMIZE TITLE
            this.panelTitlejLabel .setText(messages.getString("postaregistrata.preaccettazione.view.title.label"));

            //CUSTOMIZE BUTTON
            this.addPreAccettazionejButton.setText(messages.getString("postaregistrata.preaccettazione.view.additem.button"));
            this.removePreAccettazionejButton.setText(messages.getString("postaregistrata.preaccettazione.view.removeitem.button"));
            this.generaXmlDatajButton.setText(messages.getString("postaregistrata.preaccettazione.view.generaxmldata.button"));
            this.impostaDatiWSjButton.setText(messages.getString("postaregistrata.preaccettazione.view.impostadatiws.button"));
            this.salvaXmlDatajButton.setText(messages.getString("postaregistrata.preaccettazione.view.salvaxmldata.button"));
            this.backjButton.setText(messages.getString("view.back.button"));
            this.homejButton.setText(messages.getString("view.home.button"));
            this.exitjButton.setText(messages.getString("view.exit.button"));

            logger.info("XteTestToolPreAccettazionePRView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolPreAccettazionePRView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Pre Accettazione Posta Registrata view customization failed", ex);
        }
    }

    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolPreAccettazionePRView start initializeView");

//            buildComponentSortedMap();
//
//            this.setFocusCycleRoot(true);
//            this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

            defineTabPolicyForTextArea(xmlDatajTextArea);
            
            JLabel cornelLabel = new JLabel("");
            cornelLabel.setOpaque(true);
            cornelLabel.setBackground(new Color(240, 240, 240));

            this.PreAccettazioneDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.PreAccettazioneDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.PreAccettazioneDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.PreAccettazioneDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.PreAccettazioneDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            customizeTable(this.PreAccettazionejTable);
            this.PreAccettazionejTable.setModel(xtePreAccettazionePRTableModel);

            List<String> eventColumnWidthValues = new ArrayList<String>();
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");

            this.PreAccettazionejTable.getTableHeader().setDefaultRenderer(new XteTableHeaderRenderer(eventColumnWidthValues));
            this.PreAccettazionejTable.setDefaultRenderer(Object.class, new XteMailPieceTableCellRenderer(eventColumnWidthValues));
            
            this.xmlDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.xmlDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.xmlDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.xmlDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.xmlDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            JFormattedTextField mailPieceTimeJFormattedTextField = ((JSpinner.DateEditor)PATimejSpinner.getEditor()).getTextField();
            DefaultFormatter mailPieceTimeFormatter = (DefaultFormatter) mailPieceTimeJFormattedTextField.getFormatter();
            mailPieceTimeFormatter.setCommitsOnValidEdit(true);
            
            this.PATipojTextField.setDocument(new XteTestToolStringDocument(100));            
            this.PATipojTextField.getDocument().putProperty("owner", PATipojTextField);
            this.PANumSpedjTextField.setDocument(new XteTestToolStringDocument(100));            
            this.PANumSpedjTextField.getDocument().putProperty("owner", PANumSpedjTextField); 
            this.PACodeClienteJTextField.setDocument(new XteTestToolStringDocument(100));            
            this.PACodeClienteJTextField.getDocument().putProperty("owner", PACodeClienteJTextField);
            this.PANumOggjTextField.setDocument(new XteTestToolIntegerDocument(1000000));            
            this.PANumOggjTextField.getDocument().putProperty("owner", PANumOggjTextField);
            this.PAFrazjTextField.setDocument(new XteTestToolStringDocument(100));            
            this.PAFrazjTextField.getDocument().putProperty("owner", PAFrazjTextField);
            this.PACodeInviojTextField.setDocument(new XteTestToolStringDocument(100));            
            this.PACodeInviojTextField.getDocument().putProperty("owner", PACodeInviojTextField);
            this.PACausalejTextField.setDocument(new XteTestToolStringDocument(100));            
            this.PACausalejTextField.getDocument().putProperty("owner", PACausalejTextField);
            this.PADestinazionejTextField.setDocument(new XteTestToolStringDocument(20));            
            this.PADestinazionejTextField.getDocument().putProperty("owner", PADestinazionejTextField);
            this.PADestinatariojTextField.setDocument(new XteTestToolStringDocument(20));            
            this.PADestinatariojTextField.getDocument().putProperty("owner", PADestinatariojTextField);
            this.PAValAssjTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.PAValAssjTextField.getDocument().putProperty("owner", PAValAssjTextField);
            this.PAPeso1jTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.PAPeso1jTextField.getDocument().putProperty("owner", PAPeso1jTextField);
            this.PAPeso2jTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.PAPeso2jTextField.getDocument().putProperty("owner", PAPeso2jTextField);
            this.PANumSigillijTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.PANumSigillijTextField.getDocument().putProperty("owner", PANumSigillijTextField);
            this.PANumVagliajTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.PANumVagliajTextField.getDocument().putProperty("owner", PANumVagliajTextField);
            this.PAEspressojTextField.setDocument(new XteTestToolStringDocument(100));            
            this.PAEspressojTextField.getDocument().putProperty("owner", PAEspressojTextField);
            this.PACAPjTextField.setDocument(new XteTestToolIntegerDocument(9));            
            this.PACAPjTextField.getDocument().putProperty("owner", PACAPjTextField);
            this.PAIndirizzojTextField.setDocument(new XteTestToolStringDocument(100));            
            this.PAIndirizzojTextField.getDocument().putProperty("owner", PAIndirizzojTextField);
            this.PAImpContrjTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.PAImpContrjTextField.getDocument().putProperty("owner", PAImpContrjTextField);
            this.PACodeARjTextField.setDocument(new XteTestToolStringDocument(100));            
            this.PACodeARjTextField.getDocument().putProperty("owner", PACodeARjTextField);
            this.PADatejXDatePicker.getEditor().setDocument(new PlainDocument());            
            this.PADatejXDatePicker.getEditor().getDocument().putProperty("owner", PADatejXDatePicker.getEditor());                        
            ((JSpinner.DateEditor)PATimejSpinner.getEditor()).getTextField().setDocument(new PlainDocument());            
            ((JSpinner.DateEditor)PATimejSpinner.getEditor()).getTextField().getDocument().putProperty("owner", ((JSpinner.DateEditor)PATimejSpinner.getEditor()).getTextField());            


            
            logger.info("XteTestToolPreAccettazionePRView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolPreAccettazionePRView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata view initialization failed", ex);
        }
    }

    @Override
    public void restoreView() {
        logger.debug("XteTestToolPreAccettazionePRView restoreView");
        this.getExitButtonRef().doClick();
    }

    private void customizeTable(JTable table) {
        table.getTableHeader().setReorderingAllowed(false);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setRowHeight(20);
        table.setSelectionModel(new XteListSelectionModel());
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    private  static void defineTabPolicyForTextArea(final JTextArea textarea) {

        final Object tabActionKey = new Object();
        final Object shiftTabActionKey = new Object();
        KeyStroke tabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0, false);
        KeyStroke shiftTabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_DOWN_MASK, false);

        Action tabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusNextComponent((Component) source);
                }
            }
        };

        Action shiftTabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent((Component) source);
                }
            }
        };

        textarea.getActionMap().put(tabActionKey, tabAction);
        textarea.getInputMap().put(tabKeyStroke, tabActionKey);

        textarea.getActionMap().put(shiftTabActionKey, shiftTabAction);
        textarea.getInputMap().put(shiftTabKeyStroke, shiftTabActionKey);

    }
    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.PATipojTextField);
        componentSortedMap.put(2, this.PANumSpedjTextField);
        componentSortedMap.put(3, this.PACodeInviojTextField);
        componentSortedMap.put(4, this.PADatejXDatePicker.getEditor());
        componentSortedMap.put(5, ((JSpinner.DateEditor)this.PATimejSpinner.getEditor()).getTextField());
        componentSortedMap.put(6, this.PreAccettazionejTable);
        componentSortedMap.put(7, this.xmlDatajTextArea);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        commandjPanel = new javax.swing.JPanel();
        messageBarjScrollPane = new javax.swing.JScrollPane();
        messageBarjTextPane = new javax.swing.JTextPane();
        homejButton = new javax.swing.JButton();
        exitjButton = new javax.swing.JButton();
        backjButton = new javax.swing.JButton();
        workingjPanel = new javax.swing.JPanel();
        panelTitlejLabel = new javax.swing.JLabel();
        xmlDatajPanel = new javax.swing.JPanel();
        xmlDatajScrollPane = new javax.swing.JScrollPane();
        xmlDatajTextArea = new javax.swing.JTextArea();
        xmlDataCommandjPanel = new javax.swing.JPanel();
        generaXmlDatajButton = new javax.swing.JButton();
        impostaDatiWSjButton = new javax.swing.JButton();
        salvaXmlDatajButton = new javax.swing.JButton();
        bundleDatajPanel = new javax.swing.JPanel();
        bundleTPIdjLabel = new javax.swing.JLabel();
        PANumSpedjTextField = new javax.swing.JTextField();
        PATipojTextField = new javax.swing.JTextField();
        bundleIdjLabel = new javax.swing.JLabel();
        bundleTPIdjLabel1 = new javax.swing.JLabel();
        TestjButton = new javax.swing.JButton();
        PACodeClienteJTextField = new javax.swing.JTextField();
        mailPieceCodejLabel = new javax.swing.JLabel();
        PANumOggjTextField = new javax.swing.JTextField();
        mailPieceCodejLabel1 = new javax.swing.JLabel();
        PAFrazjTextField = new javax.swing.JTextField();
        mailPieceDateTimejLabel = new javax.swing.JLabel();
        PADatejXDatePicker = new org.jdesktop.swingx.JXDatePicker();
        PATimejSpinner = new javax.swing.JSpinner();
        mailPieceProductjLabel2 = new javax.swing.JLabel();
        PATipoMexjComboBox = new javax.swing.JComboBox();
        PreAccettazionDatajPanel = new javax.swing.JPanel();
        PreAccettazioneDatajScrollPane = new javax.swing.JScrollPane();
        PreAccettazionejTable = new javax.swing.JTable();
        mailPieceDataCommandjPanel = new javax.swing.JPanel();
        addPreAccettazionejButton = new javax.swing.JButton();
        removePreAccettazionejButton = new javax.swing.JButton();
        mailPieceLatjLabel = new javax.swing.JLabel();
        mailPieceProductjLabel = new javax.swing.JLabel();
        PACodeInviojTextField = new javax.swing.JTextField();
        mailPieceLongjLabel1 = new javax.swing.JLabel();
        PANumVagliajTextField = new javax.swing.JTextField();
        PAEspressojTextField = new javax.swing.JTextField();
        mailPieceDateTimejLabel1 = new javax.swing.JLabel();
        mailPieceLongjLabel2 = new javax.swing.JLabel();
        mailPieceLongjLabel3 = new javax.swing.JLabel();
        PAValAssjTextField = new javax.swing.JTextField();
        PADestinatariojTextField = new javax.swing.JTextField();
        mailPieceLatjLabel2 = new javax.swing.JLabel();
        PANumSigillijTextField = new javax.swing.JTextField();
        PAPeso1jTextField = new javax.swing.JTextField();
        mailPieceLatjLabel3 = new javax.swing.JLabel();
        mailPieceDateTimejLabel3 = new javax.swing.JLabel();
        PAPeso2jTextField = new javax.swing.JTextField();
        mailPieceDateTimejLabel4 = new javax.swing.JLabel();
        mailPieceDateTimejLabel5 = new javax.swing.JLabel();
        mailPieceDateTimejLabel6 = new javax.swing.JLabel();
        mailPieceDateTimejLabel7 = new javax.swing.JLabel();
        mailPieceDateTimejLabel8 = new javax.swing.JLabel();
        PACAPjTextField = new javax.swing.JTextField();
        mailPieceDateTimejLabel9 = new javax.swing.JLabel();
        PACausalejTextField = new javax.swing.JTextField();
        PADestinazionejTextField = new javax.swing.JTextField();
        PAIndirizzojTextField = new javax.swing.JTextField();
        PAImpContrjTextField = new javax.swing.JTextField();
        PACodeARjTextField = new javax.swing.JTextField();
        PASAjTextField = new javax.swing.JTextField();
        PAParametrijTextField = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setMaximumSize(new java.awt.Dimension(1000, 690));
        setMinimumSize(new java.awt.Dimension(1000, 690));
        setPreferredSize(new java.awt.Dimension(1000, 690));

        commandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        commandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        commandjPanel.setMaximumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setMinimumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setPreferredSize(new java.awt.Dimension(980, 80));

        messageBarjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageBarjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        messageBarjScrollPane.setMaximumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setMinimumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setPreferredSize(new java.awt.Dimension(350, 50));

        messageBarjTextPane.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        messageBarjTextPane.setFocusable(false);
        messageBarjScrollPane.setViewportView(messageBarjTextPane);

        homejButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        homejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Home3.png"))); // NOI18N
        homejButton.setText("<html><body>&nbsp;Home<body><html>");
        homejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        homejButton.setFocusPainted(false);
        homejButton.setFocusable(false);
        homejButton.setForeground(new java.awt.Color(51, 51, 51));
        homejButton.setIconTextGap(5);
        homejButton.setMaximumSize(new java.awt.Dimension(100, 50));
        homejButton.setMinimumSize(new java.awt.Dimension(100, 50));
        homejButton.setPreferredSize(new java.awt.Dimension(100, 50));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("<html><body>&nbsp;Exit<body><html>");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setFocusPainted(false);
        exitjButton.setFocusable(false);
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIconTextGap(5);
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        backjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        backjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/BackToMenu.png"))); // NOI18N
        backjButton.setText("<html><body>&nbsp;Back<body><html>");
        backjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        backjButton.setFocusPainted(false);
        backjButton.setFocusable(false);
        backjButton.setForeground(new java.awt.Color(51, 51, 51));
        backjButton.setIconTextGap(5);
        backjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        backjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        backjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout commandjPanelLayout = new javax.swing.GroupLayout(commandjPanel);
        commandjPanel.setLayout(commandjPanelLayout);
        commandjPanelLayout.setHorizontalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 268, Short.MAX_VALUE)
                .addComponent(backjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandjPanelLayout.setVerticalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandjPanelLayout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homejButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exitjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        workingjPanel.setBackground(new java.awt.Color(230, 250, 0));
        workingjPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20)));
        workingjPanel.setMaximumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setMinimumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setPreferredSize(new java.awt.Dimension(980, 580));

        panelTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        panelTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        panelTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelTitlejLabel.setText("Pre Accettazione");
        panelTitlejLabel.setFocusable(false);
        panelTitlejLabel.setIconTextGap(30);
        panelTitlejLabel.setMaximumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setMinimumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setPreferredSize(new java.awt.Dimension(920, 20));

        xmlDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "XML Data", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        xmlDatajPanel.setFocusable(false);
        xmlDatajPanel.setMaximumSize(new java.awt.Dimension(860, 180));
        xmlDatajPanel.setMinimumSize(new java.awt.Dimension(860, 180));
        xmlDatajPanel.setOpaque(false);
        xmlDatajPanel.setPreferredSize(new java.awt.Dimension(860, 180));

        xmlDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        xmlDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        xmlDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 125));
        xmlDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 125));
        xmlDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 125));

        xmlDatajTextArea.setColumns(20);
        xmlDatajTextArea.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        xmlDatajTextArea.setForeground(new java.awt.Color(51, 51, 51));
        xmlDatajTextArea.setLineWrap(true);
        xmlDatajTextArea.setRows(5);
        xmlDatajTextArea.setWrapStyleWord(true);
        xmlDatajTextArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setViewportView(xmlDatajTextArea);

        xmlDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        xmlDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        xmlDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        generaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        generaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml6.png"))); // NOI18N
        generaXmlDatajButton.setText("<html><body>&nbsp;Genera<br>XML Data<body><html>");
        generaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        generaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        generaXmlDatajButton.setFocusPainted(false);
        generaXmlDatajButton.setFocusable(false);
        generaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        generaXmlDatajButton.setIconTextGap(2);
        generaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        xmlDataCommandjPanel.add(generaXmlDatajButton, gridBagConstraints);

        impostaDatiWSjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        impostaDatiWSjButton.setForeground(new java.awt.Color(51, 51, 51));
        impostaDatiWSjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/WebService5.png.png"))); // NOI18N
        impostaDatiWSjButton.setText("<html><body>&nbsp;Imposta<br>&nbsp;Dati WS<body><html>");
        impostaDatiWSjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        impostaDatiWSjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        impostaDatiWSjButton.setFocusPainted(false);
        impostaDatiWSjButton.setFocusable(false);
        impostaDatiWSjButton.setIconTextGap(1);
        impostaDatiWSjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        xmlDataCommandjPanel.add(impostaDatiWSjButton, gridBagConstraints);

        salvaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        salvaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml7.png"))); // NOI18N
        salvaXmlDatajButton.setText("<html><body>&nbsp;Salva<br>XML Data<body><html>");
        salvaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        salvaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        salvaXmlDatajButton.setFocusPainted(false);
        salvaXmlDatajButton.setFocusable(false);
        salvaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        salvaXmlDatajButton.setIconTextGap(2);
        salvaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        xmlDataCommandjPanel.add(salvaXmlDatajButton, gridBagConstraints);

        javax.swing.GroupLayout xmlDatajPanelLayout = new javax.swing.GroupLayout(xmlDatajPanel);
        xmlDatajPanel.setLayout(xmlDatajPanelLayout);
        xmlDatajPanelLayout.setHorizontalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        xmlDatajPanelLayout.setVerticalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(xmlDatajPanelLayout.createSequentialGroup()
                .addGroup(xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        bundleDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Informazioni Mazzetto", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        bundleDatajPanel.setMaximumSize(new java.awt.Dimension(860, 70));
        bundleDatajPanel.setMinimumSize(new java.awt.Dimension(860, 70));
        bundleDatajPanel.setOpaque(false);
        bundleDatajPanel.setPreferredSize(new java.awt.Dimension(860, 70));

        bundleTPIdjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleTPIdjLabel.setText("Codice cliente:");
        bundleTPIdjLabel.setFocusable(false);
        bundleTPIdjLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        PANumSpedjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PANumSpedjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PANumSpedjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PANumSpedjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PANumSpedjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        PATipojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PATipojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PATipojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PATipojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PATipojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        bundleIdjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleIdjLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleIdjLabel.setText("Tipologia oggetti:");
        bundleIdjLabel.setFocusable(false);
        bundleIdjLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        bundleIdjLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        bundleIdjLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        bundleTPIdjLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjLabel1.setForeground(new java.awt.Color(51, 51, 51));
        bundleTPIdjLabel1.setText("Numero spedizione:");
        bundleTPIdjLabel1.setFocusable(false);
        bundleTPIdjLabel1.setMaximumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel1.setMinimumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel1.setPreferredSize(new java.awt.Dimension(120, 20));

        TestjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TestjButton.setForeground(new java.awt.Color(51, 51, 51));
        TestjButton.setText("<html><body>Test<body><html>");
        TestjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        TestjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        TestjButton.setFocusPainted(false);
        TestjButton.setFocusable(false);
        TestjButton.setIconTextGap(1);
        TestjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        TestjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        TestjButton.setPreferredSize(new java.awt.Dimension(100, 35));

        PACodeClienteJTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PACodeClienteJTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PACodeClienteJTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PACodeClienteJTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PACodeClienteJTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceCodejLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel.setText("Numero oggetti:");
        mailPieceCodejLabel.setFocusable(false);
        mailPieceCodejLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        PANumOggjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PANumOggjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PANumOggjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PANumOggjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PANumOggjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceCodejLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel1.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel1.setText("Frazionario ufficio:");
        mailPieceCodejLabel1.setFocusable(false);
        mailPieceCodejLabel1.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel1.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel1.setPreferredSize(new java.awt.Dimension(140, 20));

        PAFrazjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PAFrazjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PAFrazjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PAFrazjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PAFrazjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceDateTimejLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel.setText("Data spedizione:");
        mailPieceDateTimejLabel.setFocusable(false);
        mailPieceDateTimejLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        PADatejXDatePicker.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PADatejXDatePicker.setBackground(new java.awt.Color(255, 255, 255));
        PADatejXDatePicker.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PADatejXDatePicker.setMaximumSize(new java.awt.Dimension(150, 20));
        PADatejXDatePicker.setMinimumSize(new java.awt.Dimension(150, 20));
        PADatejXDatePicker.setOpaque(true);
        PADatejXDatePicker.setPreferredSize(new java.awt.Dimension(150, 20));

        PATimejSpinner.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        PATimejSpinner.setModel(new javax.swing.SpinnerDateModel());
        PATimejSpinner.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PATimejSpinner.setEditor(new javax.swing.JSpinner.DateEditor(PATimejSpinner, "HH:mm:ss"));
        PATimejSpinner.setFocusable(false);
        PATimejSpinner.setMaximumSize(new java.awt.Dimension(80, 20));
        PATimejSpinner.setMinimumSize(new java.awt.Dimension(80, 20));
        PATimejSpinner.setPreferredSize(new java.awt.Dimension(80, 20));

        mailPieceProductjLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceProductjLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceProductjLabel2.setText("Tipo messaggio:");
        mailPieceProductjLabel2.setFocusable(false);
        mailPieceProductjLabel2.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel2.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel2.setPreferredSize(new java.awt.Dimension(140, 20));

        PATipoMexjComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout bundleDatajPanelLayout = new javax.swing.GroupLayout(bundleDatajPanel);
        bundleDatajPanel.setLayout(bundleDatajPanelLayout);
        bundleDatajPanelLayout.setHorizontalGroup(
            bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(mailPieceDateTimejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                    .addComponent(bundleIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                        .addComponent(PATipojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(bundleTPIdjLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(PANumSpedjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(bundleTPIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(PACodeClienteJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(mailPieceCodejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(PANumOggjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                    .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                        .addComponent(PADatejXDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(PATimejSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(mailPieceCodejLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(PAFrazjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(mailPieceProductjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(PATipoMexjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(27, 27, 27)
                .addComponent(TestjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        bundleDatajPanelLayout.setVerticalGroup(
            bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                        .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(bundleIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PATipojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(bundleTPIdjLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PANumSpedjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(bundleTPIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PACodeClienteJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceCodejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PANumOggjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mailPieceCodejLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PAFrazjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceDateTimejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PADatejXDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PATimejSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceProductjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PATipoMexjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(TestjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(6, Short.MAX_VALUE))
        );

        TestjButton.getAccessibleContext().setAccessibleParent(panelTitlejLabel);

        PreAccettazionDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Informazioni Invio", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        PreAccettazionDatajPanel.setMaximumSize(new java.awt.Dimension(860, 260));
        PreAccettazionDatajPanel.setMinimumSize(new java.awt.Dimension(860, 260));
        PreAccettazionDatajPanel.setOpaque(false);
        PreAccettazionDatajPanel.setPreferredSize(new java.awt.Dimension(860, 260));

        PreAccettazioneDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        PreAccettazioneDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        PreAccettazioneDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        PreAccettazioneDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 120));
        PreAccettazioneDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 120));
        PreAccettazioneDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 120));

        PreAccettazionejTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        PreAccettazioneDatajScrollPane.setViewportView(PreAccettazionejTable);

        mailPieceDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        mailPieceDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        mailPieceDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 85));
        mailPieceDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 85));
        mailPieceDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 85));
        mailPieceDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        addPreAccettazionejButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        addPreAccettazionejButton.setForeground(new java.awt.Color(51, 51, 51));
        addPreAccettazionejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Add1.png"))); // NOI18N
        addPreAccettazionejButton.setText("<html><body>&nbsp;Aggiungi<br>&nbsp;&nbsp;&nbsp;Invio<body><html>");
        addPreAccettazionejButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        addPreAccettazionejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        addPreAccettazionejButton.setFocusPainted(false);
        addPreAccettazionejButton.setFocusable(false);
        addPreAccettazionejButton.setIconTextGap(1);
        addPreAccettazionejButton.setMaximumSize(new java.awt.Dimension(100, 35));
        addPreAccettazionejButton.setMinimumSize(new java.awt.Dimension(100, 35));
        addPreAccettazionejButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        mailPieceDataCommandjPanel.add(addPreAccettazionejButton, gridBagConstraints);

        removePreAccettazionejButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        removePreAccettazionejButton.setForeground(new java.awt.Color(51, 51, 51));
        removePreAccettazionejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Remove1.png"))); // NOI18N
        removePreAccettazionejButton.setText("<html><body>&nbsp;Elimina<br>&nbsp;&nbsp;&nbsp;Invio<body><html>");
        removePreAccettazionejButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        removePreAccettazionejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        removePreAccettazionejButton.setFocusPainted(false);
        removePreAccettazionejButton.setFocusable(false);
        removePreAccettazionejButton.setIconTextGap(2);
        removePreAccettazionejButton.setMaximumSize(new java.awt.Dimension(100, 35));
        removePreAccettazionejButton.setMinimumSize(new java.awt.Dimension(100, 35));
        removePreAccettazionejButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        mailPieceDataCommandjPanel.add(removePreAccettazionejButton, gridBagConstraints);

        mailPieceLatjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLatjLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLatjLabel.setText("Causale:");
        mailPieceLatjLabel.setFocusable(false);
        mailPieceLatjLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        mailPieceProductjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceProductjLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceProductjLabel.setText("Codice invio:");
        mailPieceProductjLabel.setFocusable(false);
        mailPieceProductjLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        PACodeInviojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PACodeInviojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PACodeInviojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PACodeInviojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PACodeInviojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceLongjLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel1.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel1.setText("Destinazione:");
        mailPieceLongjLabel1.setFocusable(false);
        mailPieceLongjLabel1.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel1.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel1.setPreferredSize(new java.awt.Dimension(120, 20));

        PANumVagliajTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PANumVagliajTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PANumVagliajTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PANumVagliajTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PANumVagliajTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        PAEspressojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PAEspressojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PAEspressojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PAEspressojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PAEspressojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceDateTimejLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel1.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel1.setText("Numero sigilli:");
        mailPieceDateTimejLabel1.setFocusable(false);
        mailPieceDateTimejLabel1.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel1.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel1.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceLongjLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel2.setText("Destinatario:");
        mailPieceLongjLabel2.setFocusable(false);
        mailPieceLongjLabel2.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel2.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel2.setPreferredSize(new java.awt.Dimension(120, 20));

        mailPieceLongjLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel3.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel3.setText("Valore assicurato:");
        mailPieceLongjLabel3.setFocusable(false);
        mailPieceLongjLabel3.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel3.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel3.setPreferredSize(new java.awt.Dimension(120, 20));

        PAValAssjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PAValAssjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PAValAssjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PAValAssjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PAValAssjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        PADestinatariojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PADestinatariojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PADestinatariojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PADestinatariojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PADestinatariojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceLatjLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLatjLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLatjLabel2.setText("Numero vaglia:");
        mailPieceLatjLabel2.setFocusable(false);
        mailPieceLatjLabel2.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel2.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel2.setPreferredSize(new java.awt.Dimension(120, 20));

        PANumSigillijTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PANumSigillijTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PANumSigillijTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PANumSigillijTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PANumSigillijTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        PAPeso1jTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PAPeso1jTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PAPeso1jTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PAPeso1jTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PAPeso1jTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceLatjLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLatjLabel3.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLatjLabel3.setText("Espresso:");
        mailPieceLatjLabel3.setFocusable(false);
        mailPieceLatjLabel3.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel3.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel3.setPreferredSize(new java.awt.Dimension(120, 20));

        mailPieceDateTimejLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel3.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel3.setText("Pesi:");
        mailPieceDateTimejLabel3.setFocusable(false);
        mailPieceDateTimejLabel3.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel3.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel3.setPreferredSize(new java.awt.Dimension(140, 20));

        PAPeso2jTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PAPeso2jTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PAPeso2jTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PAPeso2jTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PAPeso2jTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceDateTimejLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel4.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel4.setText("Indirizzo destinatario:");
        mailPieceDateTimejLabel4.setFocusable(false);
        mailPieceDateTimejLabel4.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel4.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel4.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceDateTimejLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel5.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel5.setText("Importo contrassegno:");
        mailPieceDateTimejLabel5.setFocusable(false);
        mailPieceDateTimejLabel5.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel5.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel5.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceDateTimejLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel6.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel6.setText("SA:");
        mailPieceDateTimejLabel6.setFocusable(false);
        mailPieceDateTimejLabel6.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel6.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel6.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceDateTimejLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel7.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel7.setText("Codice AR:");
        mailPieceDateTimejLabel7.setFocusable(false);
        mailPieceDateTimejLabel7.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel7.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel7.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceDateTimejLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel8.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel8.setText("CAP:");
        mailPieceDateTimejLabel8.setFocusable(false);
        mailPieceDateTimejLabel8.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel8.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel8.setPreferredSize(new java.awt.Dimension(140, 20));

        PACAPjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PACAPjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PACAPjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PACAPjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PACAPjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceDateTimejLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel9.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel9.setText("Parametri:");
        mailPieceDateTimejLabel9.setFocusable(false);
        mailPieceDateTimejLabel9.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel9.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel9.setPreferredSize(new java.awt.Dimension(140, 20));

        PACausalejTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PACausalejTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PACausalejTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PACausalejTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PACausalejTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        PADestinazionejTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PADestinazionejTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PADestinazionejTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PADestinazionejTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PADestinazionejTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        PAIndirizzojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PAIndirizzojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PAIndirizzojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PAIndirizzojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PAIndirizzojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        PAImpContrjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PAImpContrjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PAImpContrjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PAImpContrjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PAImpContrjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        PACodeARjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PACodeARjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PACodeARjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PACodeARjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PACodeARjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        PASAjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PASAjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PASAjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PASAjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PASAjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        PAParametrijTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PAParametrijTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        PAParametrijTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        PAParametrijTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        PAParametrijTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        javax.swing.GroupLayout PreAccettazionDatajPanelLayout = new javax.swing.GroupLayout(PreAccettazionDatajPanel);
        PreAccettazionDatajPanel.setLayout(PreAccettazionDatajPanelLayout);
        PreAccettazionDatajPanelLayout.setHorizontalGroup(
            PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PreAccettazionDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PreAccettazionDatajPanelLayout.createSequentialGroup()
                        .addComponent(mailPieceDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(PreAccettazioneDatajScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 696, Short.MAX_VALUE))
                    .addGroup(PreAccettazionDatajPanelLayout.createSequentialGroup()
                        .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mailPieceLatjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                            .addComponent(mailPieceLongjLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceLongjLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(mailPieceLongjLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                            .addComponent(mailPieceProductjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(PACodeInviojTextField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                            .addComponent(PADestinatariojTextField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PAValAssjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                            .addComponent(PACausalejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                            .addComponent(PADestinazionejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PreAccettazionDatajPanelLayout.createSequentialGroup()
                                .addComponent(mailPieceDateTimejLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(PACAPjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(PreAccettazionDatajPanelLayout.createSequentialGroup()
                                .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(mailPieceDateTimejLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(mailPieceDateTimejLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(mailPieceLatjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(mailPieceLatjLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(PANumSigillijTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(PreAccettazionDatajPanelLayout.createSequentialGroup()
                                        .addComponent(PAPeso1jTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(PAPeso2jTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                                    .addComponent(PANumVagliajTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(PAEspressojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PreAccettazionDatajPanelLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(mailPieceDateTimejLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(mailPieceDateTimejLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(mailPieceDateTimejLabel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(PreAccettazionDatajPanelLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(mailPieceDateTimejLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(mailPieceDateTimejLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(PAIndirizzojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PAImpContrjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PACodeARjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PASAjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PAParametrijTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        PreAccettazionDatajPanelLayout.setVerticalGroup(
            PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PreAccettazionDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PreAccettazionDatajPanelLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mailPieceDateTimejLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PANumSigillijTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceDateTimejLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PAImpContrjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mailPieceDateTimejLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PANumVagliajTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceLatjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PACodeARjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(PADestinatariojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(mailPieceDateTimejLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(PAEspressojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(mailPieceLatjLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(PASAjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(PreAccettazionDatajPanelLayout.createSequentialGroup()
                        .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mailPieceProductjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PACodeInviojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceDateTimejLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PAPeso1jTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PAPeso2jTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceDateTimejLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PAIndirizzojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mailPieceLatjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PACausalejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mailPieceLongjLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PADestinazionejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mailPieceLongjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mailPieceLongjLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PAValAssjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDateTimejLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PACAPjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDateTimejLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PAParametrijTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 9, Short.MAX_VALUE)
                .addGroup(PreAccettazionDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mailPieceDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PreAccettazioneDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout workingjPanelLayout = new javax.swing.GroupLayout(workingjPanel);
        workingjPanel.setLayout(workingjPanelLayout);
        workingjPanelLayout.setHorizontalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workingjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(workingjPanelLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bundleDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PreAccettazionDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        workingjPanelLayout.setVerticalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PreAccettazionDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 171, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField PACAPjTextField;
    private javax.swing.JTextField PACausalejTextField;
    private javax.swing.JTextField PACodeARjTextField;
    private javax.swing.JTextField PACodeClienteJTextField;
    private javax.swing.JTextField PACodeInviojTextField;
    private org.jdesktop.swingx.JXDatePicker PADatejXDatePicker;
    private javax.swing.JTextField PADestinatariojTextField;
    private javax.swing.JTextField PADestinazionejTextField;
    private javax.swing.JTextField PAEspressojTextField;
    private javax.swing.JTextField PAFrazjTextField;
    private javax.swing.JTextField PAImpContrjTextField;
    private javax.swing.JTextField PAIndirizzojTextField;
    private javax.swing.JTextField PANumOggjTextField;
    private javax.swing.JTextField PANumSigillijTextField;
    private javax.swing.JTextField PANumSpedjTextField;
    private javax.swing.JTextField PANumVagliajTextField;
    private javax.swing.JTextField PAParametrijTextField;
    private javax.swing.JTextField PAPeso1jTextField;
    private javax.swing.JTextField PAPeso2jTextField;
    private javax.swing.JTextField PASAjTextField;
    private javax.swing.JSpinner PATimejSpinner;
    private javax.swing.JComboBox PATipoMexjComboBox;
    private javax.swing.JTextField PATipojTextField;
    private javax.swing.JTextField PAValAssjTextField;
    private javax.swing.JPanel PreAccettazionDatajPanel;
    private javax.swing.JScrollPane PreAccettazioneDatajScrollPane;
    private javax.swing.JTable PreAccettazionejTable;
    private javax.swing.JButton TestjButton;
    private javax.swing.JButton addPreAccettazionejButton;
    private javax.swing.JButton backjButton;
    private javax.swing.JPanel bundleDatajPanel;
    private javax.swing.JLabel bundleIdjLabel;
    private javax.swing.JLabel bundleTPIdjLabel;
    private javax.swing.JLabel bundleTPIdjLabel1;
    private javax.swing.JPanel commandjPanel;
    private javax.swing.JButton exitjButton;
    private javax.swing.JButton generaXmlDatajButton;
    private javax.swing.JButton homejButton;
    private javax.swing.JButton impostaDatiWSjButton;
    private javax.swing.JLabel mailPieceCodejLabel;
    private javax.swing.JLabel mailPieceCodejLabel1;
    private javax.swing.JPanel mailPieceDataCommandjPanel;
    private javax.swing.JLabel mailPieceDateTimejLabel;
    private javax.swing.JLabel mailPieceDateTimejLabel1;
    private javax.swing.JLabel mailPieceDateTimejLabel3;
    private javax.swing.JLabel mailPieceDateTimejLabel4;
    private javax.swing.JLabel mailPieceDateTimejLabel5;
    private javax.swing.JLabel mailPieceDateTimejLabel6;
    private javax.swing.JLabel mailPieceDateTimejLabel7;
    private javax.swing.JLabel mailPieceDateTimejLabel8;
    private javax.swing.JLabel mailPieceDateTimejLabel9;
    private javax.swing.JLabel mailPieceLatjLabel;
    private javax.swing.JLabel mailPieceLatjLabel2;
    private javax.swing.JLabel mailPieceLatjLabel3;
    private javax.swing.JLabel mailPieceLongjLabel1;
    private javax.swing.JLabel mailPieceLongjLabel2;
    private javax.swing.JLabel mailPieceLongjLabel3;
    private javax.swing.JLabel mailPieceProductjLabel;
    private javax.swing.JLabel mailPieceProductjLabel2;
    private javax.swing.JScrollPane messageBarjScrollPane;
    private javax.swing.JTextPane messageBarjTextPane;
    private javax.swing.JLabel panelTitlejLabel;
    private javax.swing.JButton removePreAccettazionejButton;
    private javax.swing.JButton salvaXmlDatajButton;
    private javax.swing.JPanel workingjPanel;
    private javax.swing.JPanel xmlDataCommandjPanel;
    private javax.swing.JPanel xmlDatajPanel;
    private javax.swing.JScrollPane xmlDatajScrollPane;
    private javax.swing.JTextArea xmlDatajTextArea;
    // End of variables declaration//GEN-END:variables


}
