package com.elsagdatamat.dbtrace.bridge.tosda;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class SDAHeader {
	@XmlElement(name = "SERVICE")
	public String service;
	
	@XmlElement(name = "CHANNEL")
	public String channel;
	
	@XmlElement(name = "MSG_ID")
	public String msgId;
	
	@XmlElement(name = "MSG_TYPE")
	public String msgType;
	
	@XmlElement(name = "MSG_DATE")
	public Date msgDate;
	
	@XmlElement(name = "VERSION")
	public String version;
}
