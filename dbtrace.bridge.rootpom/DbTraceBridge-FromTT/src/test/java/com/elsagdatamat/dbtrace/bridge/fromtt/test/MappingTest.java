package com.elsagdatamat.dbtrace.bridge.fromtt.test;

import com.elsagdatamat.dbtrace.bridge.engine.ws.CustomTraceWs;
import com.elsagdatamat.dbtrace.bridge.engine.ws.XmlTraceWsBuilder;
import com.elsagdatamat.framework.entities.EntityCreationException;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetailsList;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.TextMessage;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Scanner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
public class MappingTest {

	@Resource
	private XmlTraceWsBuilder traceBuilder;

  private void mapMessage(String folder,String name)throws Exception{
    String fullname=folder+"/"+name;
    if(folder.isEmpty())
      fullname=name;
    System.out.println("Messaggio "+fullname+" - inizio");
    MockTextMessage msg = new MockTextMessage(readFile(fullname+".xml"));
    TracesList traceList = createTraces(msg);
    printToConsole(traceList);
    System.out.println("Messaggio "+fullname+" - fine");
  }

  @Test
  public void createMessageRtz(){
    try {
      mapMessage("RTZ","D1_RTZ");
      mapMessage("RTZ","M1_RTZ");
      mapMessage("RTZ","M2_RTZ");
      mapMessage("RTZ","O2_RTZ");
      mapMessage("RTZ","B2_RTZ");
    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }
  @Test
  public void createMessageApt(){
    try {
      mapMessage("APT","D1_APT");
      mapMessage("APT","M1_APT");
      mapMessage("APT","M2_APT");
      mapMessage("APT","O2_APT");
      mapMessage("APT","B2_APT");
      mapMessage("APT","B2_APT2");

    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail(e.getMessage());
    }
  }

	@Test
	public void createMessageTest() {
		try {
      mapMessage("","d1");
      mapMessage("","D1_000000000599");
      mapMessage("","M1");
      mapMessage("","M1_bis");
    } catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	private void printToConsole(TracesList lst) throws IllegalAccessException, InvocationTargetException {
		System.out.println("RISULTATO TEST:");
		List<Trace> traces = lst.getTraces();
		for (Trace trace : traces) {
			System.out.println("	TRACE: ");
			String get = "get";
			Method[] getters = trace.getClass().getMethods();
			for (Method getter : getters) {
				if (getter.getName().contains(get)) {
					String value = String.valueOf(getter.invoke(trace));
					if (!getter.getName().contentEquals("getClass")) {
						if (getter.getName().contentEquals("getTraceDetailsList")) {
							TraceDetailsList traceDetailsList = (TraceDetailsList) getter.invoke(trace);
							System.out.println("		TRACEDETAIL: ");
							List<TraceDetail> traceDetails = traceDetailsList.getTraceDetails();
							for (TraceDetail traceDetail : traceDetails) {
								System.out.println("			" + traceDetail.getParamClass() + ": " + traceDetail.getParamValue());
							}
							System.out.println("		TRACEDETAIL FINE");
						} else {
							System.out.println("		" + getter.getName() + ": " + value);
						}
					}
				}
			}
			System.out.println("	TRACE FINE");
		}
		System.out.println("FINE");
	}

	private String readFile(String fileName) {
		InputStream is = ClassLoader.getSystemResourceAsStream(fileName);
		StringBuffer sb = new StringBuffer();
		Scanner s = new Scanner(is);
		while (s.hasNext())
			sb.append(s.nextLine());
		return sb.toString();
	}

	private TracesList createTraces(TextMessage message) throws EntityCreationException, JMSException {
		List<CustomTraceWs> lst = traceBuilder.create(new StringReader(message.getText()));
		Assert.assertNotNull(lst);
		TracesList traceList = new TracesList();
		traceList.getTraces().addAll(lst);
		return traceList;
	}
}
