/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view;

/**
 *
 * @author Da Procida
 */
public interface XteTestToolView {

    public void customizeView();

    public void initializeView();

    public void restoreView();

}
