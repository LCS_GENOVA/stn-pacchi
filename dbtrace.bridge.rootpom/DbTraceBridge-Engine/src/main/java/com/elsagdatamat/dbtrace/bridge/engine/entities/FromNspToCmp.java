package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

//@Entity
//@Table(name="FROM_NSP_TO_CMP")
//@IdClass(FromNspToCmpPK.class)
public class FromNspToCmp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -385902249706841565L;


	public static final String FRAZIONARIO_NSP = "FrazionarioNsp";
	public static final String FRAZIONARIO_CMP = "FrazionarioCmp";

	private String FrazionarioNsp;
	private String FrazionarioCmp;

	@Id
	@Column(name="FRAZIONARIO_NSP")
	public String getFrazionarioNsp() {
		return FrazionarioNsp;
	}
	public void setFrazionarioNsp(String frazionarioNsp) {
		FrazionarioNsp = frazionarioNsp;
	}

	@Id
	@Column(name="FRAZIONARIO_CMP")
	public String getFrazionarioCmp() {
		return FrazionarioCmp;
	}
	public void setFrazionarioCmp(String frazionarioCmp) {
		FrazionarioCmp = frazionarioCmp;
	}
}
