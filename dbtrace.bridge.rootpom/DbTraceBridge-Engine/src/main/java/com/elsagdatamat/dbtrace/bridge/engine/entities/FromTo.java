package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="FROM_TO")
public class FromTo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -385902249706841565L;

	public static final String FRAZIONARIO_FROM = "frazionarioFrom";
	public static final String FRAZIONARIO_TO = "frazionarioTo";

	private String frazionarioFrom;
	private String frazionarioTo;

	@Id
	@Column(name="FRAZIONARIO_FROM")
	public String getFrazionarioFrom() {
		return frazionarioFrom;
	}
	public void setFrazionarioFrom(String frazionarioFrom) {
		this.frazionarioFrom = frazionarioFrom;
	}


	@Column(name="FRAZIONARIO_TO")
	public String getFrazionarioTo() {
		return frazionarioTo;
	}
	public void setFrazionarioTo(String frazionarioTo) {
		this.frazionarioTo = frazionarioTo;
	}

}
