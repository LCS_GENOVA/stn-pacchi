package com.elsagdatamat.dbtrace.bridge.totgc.test;

import static org.junit.Assert.assertNotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.elsagdatamat.dbtrace.bridge.totgc.IToTGCMessageBuilder;
import com.elsagdatamat.dbtrace.bridge.totgc.manager.ITraceManager;
import com.elsagdatamat.tracktrace.core.infrastructure.trackmessages.converter.Tgc23iMessage;
import com.leonardo.tt.tgc.Codici;
import com.leonardo.tt.tgc.ListaInvii;
import com.leonardo.tt.tgc.ObjectFactory;
import com.leonardo.tt.tgc.TTTGC;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/applicationContext.xml"
		})
public class TgcSenderJobTest {
//	@Autowired
//	ApplicationContext context;
	
	@Autowired
	IToTGCMessageBuilder toTGCMessageBuilder;

	@Autowired
	ITraceManager traceManager;
	
	@Test
	public void checkApplicationContext() {
		
	}
	

	@Test
	public void testMarshall() throws JAXBException {
		ObjectFactory of = new ObjectFactory();
		TTTGC obj = of.createTTTGC();
		ListaInvii listaInvii = of.createListaInvii();
		obj.setListaInvii(listaInvii);
		
		List<Codici> codici = listaInvii.getCodici();
		
		Codici c = of.createCodici();
		c.setCanale("CANALE");
		c.setCodiceOggetto("123456789");
		c.setDataTraccia(null);
		c.setFrazionarioUfficio("28449");
		c.setProdotto("AG");
		codici.add(c);
		
		System.out.println(Tgc23iMessage.createTgcXmlMessageAsString(obj));
	}
	
	@Test
	public void getTraces() throws DatatypeConfigurationException {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.YEAR, 2014);
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		
		Date lastFetchedRecordDate = cal.getTime();
		Long lastFetchedRecordId = 0L;
		Date dateTillto = new Date();
		int maxRows = 100;
		List<Trace> tl = traceManager.loadNextTraces(lastFetchedRecordDate, lastFetchedRecordId, dateTillto, maxRows);
		assertNotNull(tl);
	}
	
	@Test
	public void getAndMarshall() throws Exception {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.YEAR, 2014);
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		long epoch = cal.getTime().getTime();

//		IToTGCMessageBuilder bean = (IToTGCMessageBuilder) context.getBean("toTGCMessageBuilder");
		assertNotNull(toTGCMessageBuilder);
		
		
		ToToken token = toTGCMessageBuilder.getToken();
		token.setWsToken(String.format("%d;0", epoch));
		toTGCMessageBuilder.setToken(token);
		TracesList traces = toTGCMessageBuilder.getTraces(null, new Date());
		List<String> create = toTGCMessageBuilder.create(traces);
		for (String s : create) {
			System.out.println(s);
		}
		toTGCMessageBuilder.send(create);
	}
}
