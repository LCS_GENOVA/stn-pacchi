package com.selexelsag.tt.dbtrace.bridge.sender.update.core;


public class ProductKey {
	
	private String prodotto; // cod_prodotto
	private String canale; // cod_Canale - e' chiave dell'hashmap per la configurazione dei prodotti
	private String traccia; // cod_traccia
	
	public String getProdotto() {
		return prodotto;
	}
	public void setProdotto(String prodotto) {
		this.prodotto = prodotto;
	}
	public String getCanale() {
		return canale;
	}
	public void setCanale(String canale) {
		this.canale = canale;
	}
	public String getTraccia() {
		return traccia;
	}
	public void setTraccia(String traccia) {
		this.traccia = traccia;
	}
	public ProductKey(String prodotto, String canale, String traccia) {
		super();
		this.prodotto = prodotto;
		this.canale = canale;
		this.traccia = traccia;
	}
	public ProductKey(){
	}
	public void clone (ProductKey pk){
		this.prodotto=pk.getProdotto();
		this.canale=pk.getCanale();
		this.traccia=pk.getTraccia();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((canale == null) ? 0 : canale.hashCode());
		result = prime * result
				+ ((prodotto == null) ? 0 : prodotto.hashCode());
		result = prime * result + ((traccia == null) ? 0 : traccia.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductKey other = (ProductKey) obj;
		if (canale == null) {
			if (other.canale != null)
				return false;
		} else if (!canale.equals(other.canale))
			return false;
		if (prodotto == null) {
			if (other.prodotto != null)
				return false;
		} else if (!prodotto.equals(other.prodotto))
			return false;
		if (traccia == null) {
			if (other.traccia != null)
				return false;
		} else if (!traccia.equals(other.traccia))
			return false;
		return true;
	}
	

	
}
