package com.elsagdatamat.dbtrace.bridge.tosda;

import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.server.UID;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.helpers.DefaultValidationEventHandler;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ICodiciProdottoPICodiciProdottoSDAManager;
import com.elsagdatamat.dbtrace.bridge.engine.dao.IModPagContrPiSdaDAO;
import com.elsagdatamat.dbtrace.bridge.engine.dao.IOfficeMapperDAO;
import com.elsagdatamat.dbtrace.bridge.engine.dao.IStatusPiSdaDAO;
import com.elsagdatamat.dbtrace.bridge.engine.entities.ModPagContrPiSda;
import com.elsagdatamat.dbtrace.bridge.engine.entities.ModPagContrPiSdaPK;
import com.elsagdatamat.dbtrace.bridge.engine.entities.OfficeMapper;
import com.elsagdatamat.dbtrace.bridge.engine.entities.StatusPiSda;
import com.elsagdatamat.dbtrace.bridge.engine.entities.StatusPiSdaPK;
import com.elsagdatamat.dbtrace.validation.event.NonCompliantTrace;
import com.elsagdatamat.dbtrace.validation.event.TraceValidator;
import com.elsagdatamat.framework.utils.Pair;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;

public class ConversionUtility {

	private static final String CHANNEL_INTERNAZIONALI = "PacchiInternazionali";

	private static final String CHANNEL_CHIOSCO = "Chiosco";

	//Aggiunto - Nello - gen2016
	private static final String CHANNEL_RTZ = "RTZ";

	private static final String VERSION = "1.0";

	private static final String SERVICE = "PACCHI";

	private static final String MESSAGE_TYPE = "TPA";

	private static final String CHANNEL = "PosteItaliane";

	protected final static Log log = LogFactory.getLog(ConversionUtility.class);

	private IModPagContrPiSdaDAO modPagDao;
	private IStatusPiSdaDAO idStatusDao;
	private IOfficeMapperDAO officeMapperDAO;

	private static ConversionUtility instance = null;
	List<NonCompliantTrace> nonCompliantTraces = null;

	/**
	 * hided default constructor
	 */
	private ConversionUtility() {
		super();
	}

	public static ConversionUtility getInstance() {
		if (instance == null)
			instance = new ConversionUtility();
		return instance;
	}

	public static ConversionUtility getInstance(IModPagContrPiSdaDAO modPagDao, IStatusPiSdaDAO idStatusDao) {
		if (instance == null)
			instance = new ConversionUtility();
		return instance.withModPag(modPagDao).withStatusPi(idStatusDao);
	}

	public ConversionUtility withModPag(IModPagContrPiSdaDAO modPagDao) {
		this.modPagDao = modPagDao;
		return this;
	}

	public ConversionUtility withStatusPi(IStatusPiSdaDAO idStatusDao) {
		this.idStatusDao = idStatusDao;
		return this;
	}

	public SDAMessage convertToSDA(List<Trace> traceList, IModPagContrPiSdaDAO modPagDao,
			ICodiciProdottoPICodiciProdottoSDAManager subPManager, IOfficeMapperDAO officeManager) {
		SDAHeader header = new SDAHeader();
		header.channel = CHANNEL;
		header.msgDate = (new Date());
		header.msgType = MESSAGE_TYPE;
		header.service = SERVICE;
		header.version = VERSION;
		UID uid = new UID();
		header.msgId = uid.toString();

		List<SDATrace> sdaTraceList = new ArrayList<SDATrace>();
		for (Trace t : traceList) {
			log.debug(String.format("convertToSDA per la traccia '%s' (%s)", t.getTracedEntity(), t.getWhatHappened()));
			Map<String, String> map = new HashMap<String, String>();
			if ((t.getTraceDetailsList() != null) && (t.getTraceDetailsList().getTraceDetails() != null)) {
				for (TraceDetail d : t.getTraceDetailsList().getTraceDetails()) {
					map.put(d.getParamClass(), d.getParamValue());
				}
			}

			SDATrace sdaTrace = new SDATrace();
			sdaTrace.tracedEntity = t.getTracedEntity();
			sdaTrace.idTracedEntity = t.getIdTracedEntity();
			sdaTrace.whenHappened = t.getWhenHappened();
			sdaTrace.whereHappened = t.getWhereHappened();
			sdaTrace.whatHappened = t.getWhatHappened();
			sdaTrace.labelTracedEntity = t.getLabelTracedEntity();
			sdaTrace.whatHappenedDetail = new SDATraceWhatDetail();

			// TODO: REFACTORING
			WhatHappened whatHappened = Enum.valueOf(WhatHappened.class, (t.getWhatHappened()));
			switch (whatHappened) {
			case ACC_NSP:
				sdaTrace.whatHappened = WhatHappened.ACC_NSP.name();
				sdaTrace.whatHappenedDetail.accNsp = (new SDAAccNsp(map));
				break;
			case RES_NSP:
				sdaTrace.whatHappened = WhatHappened.RES_NSP.name();
				sdaTrace.whatHappenedDetail.resNsp = (new SDAResNsp(map));
				break;
			case AVV_NSP:
				sdaTrace.whatHappened = WhatHappened.AVV_NSP.name();
				sdaTrace.whatHappenedDetail.avvNsp = new SDAAvvNsp(map);
				break;
			case ACC_CMP:
			case ACC_PTL:
				sdaTrace.whatHappened = WhatHappened.ACC_CMP.name();
				sdaTrace.whatHappenedDetail.accCmp = new SDAAccCmp(map);
				if (sdaTrace.whatHappenedDetail.accCmp.subp == null) {
					sdaTrace.whatHappenedDetail.accCmp.subp = subPManager.getPiCode(sdaTrace.idTracedEntity);
				}
				if (sdaTrace.whatHappenedDetail.accCmp.regt != null && !"A".equals(sdaTrace.whatHappenedDetail.accCmp.regt)) {
					// Il valore di REGT non e' valido, lo resetto
					sdaTrace.whatHappenedDetail.accCmp.regt = null;
				}
				break;
			case PIC_ICC:
			case SMI_CMP:
				sdaTrace.whatHappened = WhatHappened.SMI_CMP.name();
				sdaTrace.whatHappenedDetail.smiCmp = new SDASmiCmp(map);
				sdaTrace.whatHappenedDetail.smiCmp.status = tryToFindIdStatusIfNull(t, map);
				String backToSender = map.get(MapKeyEnum.BTSF.name());
				sdaTrace.whatHappenedDetail.smiCmp.flagrrr = getFlagRRR(backToSender);
				sdaTrace.whatHappenedDetail.smiCmp.codistr = getCodIstr(backToSender);
				break;
			case PAR_SDA:
			case PAR_ICC:
			case PAR_CMP:
				log.debug(WhatHappened.PAR_CMP);
				sdaTrace.whatHappened = WhatHappened.PAR_CMP.name();
				sdaTrace.whatHappenedDetail.parCmp = new SDAParCmp(map);

				List<Pair<String, Object>> params = new LinkedList<Pair<String, Object>>();
				params.add(new Pair<String, Object>(OfficeMapper.FAMILY, "SDA"));
				params.add(new Pair<String, Object>(OfficeMapper.FRAZIONARIO_INTERNO, map.get(MapKeyEnum.OFCOTH.name())));
				officeMapperDAO = officeManager;
				List<OfficeMapper> offices = officeMapperDAO.findByProperties(params);

				log.debug("Offices found: " + offices.size());
				if (offices.isEmpty()) {
					sdaTrace.whatHappenedDetail.parCmp.destinationType = "UP";
					sdaTrace.whatHappenedDetail.parCmp.destinationId = map.get(MapKeyEnum.OFCOTH.name());
					sdaTrace.whatHappenedDetail.parCmp.destinationDescr = " ";
				} else {
					sdaTrace.whatHappenedDetail.parCmp.destinationType = "FS";
					sdaTrace.whatHappenedDetail.parCmp.destinationId = offices.get(0).getFrazionarioEsterno();
				}
				sdaTrace.whatHappenedDetail.parCmp.destinationDescr = map.get(MapKeyEnum.DESTINATION_DESCR.name());
				if (sdaTrace.whatHappenedDetail.parCmp.destinationDescr == null
						|| sdaTrace.whatHappenedDetail.parCmp.destinationDescr.isEmpty()) {
					sdaTrace.whatHappenedDetail.parCmp.destinationDescr = "...";
				}
				sdaTrace.whatHappenedDetail.parCmp.vettore = map.get(MapKeyEnum.VECTOR.name());
				if (sdaTrace.whatHappenedDetail.parCmp.vettore == null || sdaTrace.whatHappenedDetail.parCmp.vettore.isEmpty()) {
					sdaTrace.whatHappenedDetail.parCmp.vettore = "...";
				}

				log.debug("DestinationType " + sdaTrace.whatHappenedDetail.parCmp.destinationType);

				break;
			case ARR_ICC:
				sdaTrace.whatHappened = WhatHappened.ARR_CMP.name();
				sdaTrace.whatHappenedDetail.arrCmp = new SDAArrCmp(map);
				sdaTrace.whatHappenedDetail.arrCmp.destinationType = "UP";
				sdaTrace.whatHappenedDetail.arrCmp.destinationId = map.get(MapKeyEnum.OFCOTH.name());
				sdaTrace.whatHappenedDetail.arrCmp.destinationDescr = " ";
				break;
			case UTP_MZT:
			case CON_MZT:
				sdaTrace.whatHappened = WhatHappened.CON_SPE.name();
				sdaTrace.whatHappenedDetail.conSpe = new SDAConSpe(map);
				if(map.get(MapKeyEnum.BID.name())!=null) {
					sdaTrace.whatHappenedDetail.conSpe.bunchId = Long.parseLong(map.get(MapKeyEnum.BID.name()));
				}
				break;
			case ESI_OMP:
			case ESI_TT:
			case ESM_TT:
			case ESG_TT:
			case ACC_ESI:
				sdaTrace.whatHappened = WhatHappened.SDA_UFI.name();
				sdaTrace.whatHappenedDetail.sdaUfi = new SDASdaUfi(map);
				if (sdaTrace.whatHappenedDetail.sdaUfi.getDATARIMESSACNTRS() == null)
					sdaTrace.whatHappenedDetail.sdaUfi.setDATARIMESSACNTRS(sdaTrace.whenHappened);
				sdaTrace.whatHappenedDetail.sdaUfi.setMODALITAPAGAMENTO(getBillModeCode(map.get(MapKeyEnum.MOD_RIMBORSO_CONTRASSEGNO.name())));
				sdaTrace.whatHappenedDetail.sdaUfi.setSTATUS(tryToFindIdStatusIfNull(t, map));
				break;
			case SSIP_TO_TT:
				header.channel = CHANNEL_INTERNAZIONALI;
				sdaTrace.whatHappened = WhatHappened.FROM_SSIP.name();
				sdaTrace.whatHappenedDetail.fromSsip = new SDAFromSsip(map);
				sdaTrace.whatHappenedDetail.fromSsip.setIDSTATUS(tryToFindIdStatusIfNull(t, map));
				// create where_happen detail
				sdaTrace.whereHappenedDetail = new SDATraceWhereDetail();
				sdaTrace.whereHappenedDetail.office = new SDAOffice(t.getIdChannel(), map.get(MapKeyEnum.WHERE_DESCRIPTION.name()));
				break;

			case FOR_MRV:
				sdaTrace.whatHappened = WhatHappened.SDA_UFI.name();
				sdaTrace.whatHappenedDetail.sdaUfi = new SDASdaUfi(map);
				if (sdaTrace.whatHappenedDetail.sdaUfi.getDATARIMESSACNTRS() == null)
					sdaTrace.whatHappenedDetail.sdaUfi.setDATARIMESSACNTRS(sdaTrace.whenHappened);
				sdaTrace.whatHappenedDetail.sdaUfi.setMODALITAPAGAMENTO(getBillModeCode(map.get(MapKeyEnum.MOD_RIMBORSO_CONTRASSEGNO.name())));
				sdaTrace.whatHappenedDetail.sdaUfi.setSTATUS(StatusEnum.ARP.name());
				break;
			case EXP_TT:
				sdaTrace.whatHappened = WhatHappened.SDA_UFI.name();
				sdaTrace.whatHappenedDetail.sdaUfi = new SDASdaUfi(map);
				if (sdaTrace.whatHappenedDetail.sdaUfi.getDATARIMESSACNTRS() == null)
					sdaTrace.whatHappenedDetail.sdaUfi.setDATARIMESSACNTRS(sdaTrace.whenHappened);
				sdaTrace.whatHappenedDetail.sdaUfi.setMODALITAPAGAMENTO(getBillModeCode(map.get(MapKeyEnum.MOD_RIMBORSO_CONTRASSEGNO.name())));
				sdaTrace.whatHappenedDetail.sdaUfi.setSTATUS(StatusEnum.GCP.name());
				break;
			default:
				log.warn(String.format("WhatHappened '%s' non valido per la traccia %s", whatHappened, t.getTracedEntity()));
				break;
			}
			if (t.getWhereHappened().equals("UNSP") || t.getWhereHappened().equals("UP")) {
				sdaTrace.whereHappenedDetail = new SDATraceWhereDetail();
				sdaTrace.whereHappenedDetail.office = new SDAOffice(t.getIdChannel(), "...");
			}

			// TraceEntityDetail
			sdaTrace.traceEntityDetail = new SDATraceEntityDetail();
			sdaTraceList.add(sdaTrace);
		}

		SDAMessage message = new SDAMessage();
		message.header = (header);
		message.traces = (sdaTraceList);

		return message;
	}

	public SDAMessage convertAptTracesToSDATraces(List<Trace> traceList, IModPagContrPiSdaDAO modPagDao,
			ICodiciProdottoPICodiciProdottoSDAManager subPManager, IOfficeMapperDAO officeManager) {
		SDAHeader header = new SDAHeader();
		header.channel = CHANNEL_CHIOSCO;
		header.msgDate = (new Date());
		header.msgType = MESSAGE_TYPE;
		header.service = SERVICE;
		header.version = VERSION;
		UID uid = new UID();
		header.msgId = uid.toString();

		List<SDATrace> sdaTraceList = new ArrayList<SDATrace>();
		for (Trace t : traceList) {
			log.debug(String.format("convertAptTracesToSDATraces per la traccia '%s' (%s)", t.getTracedEntity(), t.getWhatHappened()));
			Map<String, String> map = new HashMap<String, String>();
			if ((t.getTraceDetailsList() != null) && (t.getTraceDetailsList().getTraceDetails() != null)) {
				for (TraceDetail d : t.getTraceDetailsList().getTraceDetails()) {
					map.put(d.getParamClass(), d.getParamValue());
				}
			}

			SDATrace sdaTrace = new SDATrace();
			sdaTrace.tracedEntity = t.getTracedEntity();
			sdaTrace.idTracedEntity = t.getIdTracedEntity();
			sdaTrace.whenHappened = t.getWhenHappened();
			sdaTrace.whereHappened = t.getWhereHappened();
			sdaTrace.whatHappened = t.getWhatHappened();
			sdaTrace.labelTracedEntity = t.getLabelTracedEntity();
			sdaTrace.whatHappenedDetail = new SDATraceWhatDetail();

			WhatHappened whatHappened = Enum.valueOf(WhatHappened.class, (t.getWhatHappened()));
			switch (whatHappened) {
			case ACC_NSP:
				sdaTrace.whatHappened = WhatHappened.ACC_NSP.name();
				sdaTrace.whatHappenedDetail.accNsp = (new SDAAccNsp(map));
				break;
			case AVV_NSP:
				sdaTrace.whatHappened = WhatHappened.AVV_NSP.name();
				sdaTrace.whatHappenedDetail.avvNsp = new SDAAvvNsp(map);
				break;
			case FOR_MRV:
				sdaTrace.whatHappened = WhatHappened.SDA_UFI.name();
				sdaTrace.whatHappenedDetail.sdaUfi = new SDASdaUfi(map);
				if (sdaTrace.whatHappenedDetail.sdaUfi.getDATARIMESSACNTRS() == null)
					sdaTrace.whatHappenedDetail.sdaUfi.setDATARIMESSACNTRS(sdaTrace.whenHappened);
				sdaTrace.whatHappenedDetail.sdaUfi.setMODALITAPAGAMENTO(getBillModeCode(map.get(MapKeyEnum.MOD_RIMBORSO_CONTRASSEGNO.name())));
				sdaTrace.whatHappenedDetail.sdaUfi.setSTATUS(StatusEnum.ARP.name());

				if(sdaTrace.whatHappenedDetail.sdaUfi.getIdOperatore()==null || sdaTrace.whatHappenedDetail.sdaUfi.getIdOperatore().equals(""))
					sdaTrace.whatHappenedDetail.sdaUfi.setIdOperatore(map.get(MapKeyEnum.OP.name()));
				if(sdaTrace.whatHappenedDetail.sdaUfi.getIdChiosco()==null || sdaTrace.whatHappenedDetail.sdaUfi.getIdChiosco().equals(""))
					sdaTrace.whatHappenedDetail.sdaUfi.setIdChiosco(t.getIdChannel());

				if (map.containsKey(MapKeyEnum.EXPIRATION_DATE.name()))
					sdaTrace.whatHappenedDetail.sdaUfi.setExpirationDate(toXMLGregorian(map.get(MapKeyEnum.EXPIRATION_DATE.name()), "dd/MM/yyyy HH:mm:ss"));
				break;
			case EXP_TT:
				sdaTrace.whatHappened = WhatHappened.SDA_UFI.name();
				sdaTrace.whatHappenedDetail.sdaUfi = new SDASdaUfi(map);
				if (sdaTrace.whatHappenedDetail.sdaUfi.getDATARIMESSACNTRS() == null)
					sdaTrace.whatHappenedDetail.sdaUfi.setDATARIMESSACNTRS(sdaTrace.whenHappened);
				sdaTrace.whatHappenedDetail.sdaUfi.setMODALITAPAGAMENTO(getBillModeCode(map.get(MapKeyEnum.MOD_RIMBORSO_CONTRASSEGNO.name())));
				sdaTrace.whatHappenedDetail.sdaUfi.setSTATUS(StatusEnum.GCP.name());

				if(sdaTrace.whatHappenedDetail.sdaUfi.getIdOperatore()==null || sdaTrace.whatHappenedDetail.sdaUfi.getIdOperatore().equals(""))
					sdaTrace.whatHappenedDetail.sdaUfi.setIdOperatore(map.get(MapKeyEnum.OP.name()));
				if(sdaTrace.whatHappenedDetail.sdaUfi.getIdChiosco()==null || sdaTrace.whatHappenedDetail.sdaUfi.getIdChiosco().equals(""))
					sdaTrace.whatHappenedDetail.sdaUfi.setIdChiosco(t.getIdChannel());

				if (map.containsKey(MapKeyEnum.EXPIRATION_DATE.name()))
					sdaTrace.whatHappenedDetail.sdaUfi.setExpirationDate(toXMLGregorian(map.get(MapKeyEnum.EXPIRATION_DATE.name()), "dd/MM/yyyy HH:mm:ss"));
				break;
			case ESI_OMP:
				sdaTrace.whatHappened = WhatHappened.SDA_UFI.name();
				sdaTrace.whatHappenedDetail.sdaUfi = new SDASdaUfi(map);
				if (sdaTrace.whatHappenedDetail.sdaUfi.getDATARIMESSACNTRS() == null)
					sdaTrace.whatHappenedDetail.sdaUfi.setDATARIMESSACNTRS(sdaTrace.whenHappened);
				sdaTrace.whatHappenedDetail.sdaUfi.setMODALITAPAGAMENTO(getBillModeCode(map.get(MapKeyEnum.MOD_RIMBORSO_CONTRASSEGNO.name())));
				//2015/01/20 D.P. CR APT: si richiede di inviare status CNP se esito positivo
				//sdaTrace.whatHappenedDetail.sdaUfi.setSTATUS("000");
				sdaTrace.whatHappenedDetail.sdaUfi.setSTATUS(StatusEnum.CNP.name());

				if(sdaTrace.whatHappenedDetail.sdaUfi.getIdOperatore()==null || sdaTrace.whatHappenedDetail.sdaUfi.getIdOperatore().equals(""))
					sdaTrace.whatHappenedDetail.sdaUfi.setIdOperatore(map.get(MapKeyEnum.OP.name()));
				if(sdaTrace.whatHappenedDetail.sdaUfi.getIdChiosco()==null || sdaTrace.whatHappenedDetail.sdaUfi.getIdChiosco().equals(""))
					sdaTrace.whatHappenedDetail.sdaUfi.setIdChiosco(t.getIdChannel());

				break;
			default:
				log.warn(String.format("WhatHappened '%s' non previsto per la traccia %s", whatHappened, t.getTracedEntity()));
				break;
			}

			// WhereHappenedDetail
			if (t.getWhereHappened().equals("UNSP") || t.getWhereHappened().equals("UP")) {
				sdaTrace.whereHappenedDetail = new SDATraceWhereDetail();
				sdaTrace.whereHappenedDetail.office = new SDAOffice(t.getIdChannel(), "...");
			}

			// TraceEntityDetail
			sdaTrace.traceEntityDetail = new SDATraceEntityDetail();
			sdaTraceList.add(sdaTrace);
		}

		SDAMessage message = new SDAMessage();
		message.header = (header);
		message.traces = (sdaTraceList);

		return message;
	}

	public SDAMessage convertRtzTracesToSDATraces(List<Trace> traceList, IModPagContrPiSdaDAO modPagDao,
			ICodiciProdottoPICodiciProdottoSDAManager subPManager, IOfficeMapperDAO officeManager) {
		SDAHeader header = new SDAHeader();
		header.channel = CHANNEL_RTZ;
		header.msgDate = (new Date());
		header.msgType = MESSAGE_TYPE;
		header.service = SERVICE;
		header.version = VERSION;
		UID uid = new UID();
		header.msgId = uid.toString();

		List<SDATrace> sdaTraceList = new ArrayList<SDATrace>();
		for (Trace t : traceList) {
			log.debug(String.format("convertAptTracesToSDATraces per la traccia '%s' (%s)", t.getTracedEntity(), t.getWhatHappened()));
			Map<String, String> map = new HashMap<String, String>();
			if ((t.getTraceDetailsList() != null) && (t.getTraceDetailsList().getTraceDetails() != null)) {
				for (TraceDetail d : t.getTraceDetailsList().getTraceDetails()) {
					map.put(d.getParamClass(), d.getParamValue());
				}
			}

			SDATrace sdaTrace = new SDATrace();
			sdaTrace.tracedEntity = t.getTracedEntity();
			sdaTrace.idTracedEntity = t.getIdTracedEntity();
			sdaTrace.whenHappened = t.getWhenHappened();
			sdaTrace.whereHappened = t.getWhereHappened();
			sdaTrace.whatHappened = t.getWhatHappened();
			sdaTrace.labelTracedEntity = t.getLabelTracedEntity();
			sdaTrace.whatHappenedDetail = new SDATraceWhatDetail();

			WhatHappened whatHappened = Enum.valueOf(WhatHappened.class, (t.getWhatHappened()));
			switch (whatHappened) {
			case ACC_NSP:
				sdaTrace.whatHappened = WhatHappened.ACC_NSP.name();
				sdaTrace.whatHappenedDetail.accNsp = (new SDAAccNsp(map));
				break;
			case AVV_NSP:
				sdaTrace.whatHappened = WhatHappened.AVV_NSP.name();
				sdaTrace.whatHappenedDetail.avvNsp = new SDAAvvNsp(map);
				break;
			case FOR_MRV:
				sdaTrace.whatHappened = WhatHappened.SDA_UFI.name();
				sdaTrace.whatHappenedDetail.sdaUfi = new SDASdaUfi(map);
				if (sdaTrace.whatHappenedDetail.sdaUfi.getDATARIMESSACNTRS() == null)
					sdaTrace.whatHappenedDetail.sdaUfi.setDATARIMESSACNTRS(sdaTrace.whenHappened);
				sdaTrace.whatHappenedDetail.sdaUfi.setMODALITAPAGAMENTO(getBillModeCode(map.get(MapKeyEnum.MOD_RIMBORSO_CONTRASSEGNO.name())));
				sdaTrace.whatHappenedDetail.sdaUfi.setSTATUS(StatusEnum.ARP.name());

				if(sdaTrace.whatHappenedDetail.sdaUfi.getIdOperatore()==null || sdaTrace.whatHappenedDetail.sdaUfi.getIdOperatore().equals(""))
					sdaTrace.whatHappenedDetail.sdaUfi.setIdOperatore(map.get(MapKeyEnum.OP.name()));
				if(sdaTrace.whatHappenedDetail.sdaUfi.getIdChiosco()==null || sdaTrace.whatHappenedDetail.sdaUfi.getIdChiosco().equals(""))
					sdaTrace.whatHappenedDetail.sdaUfi.setIdChiosco(t.getIdChannel());

				if (map.containsKey(MapKeyEnum.EXPIRATION_DATE.name()))
					sdaTrace.whatHappenedDetail.sdaUfi.setExpirationDate(toXMLGregorian(map.get(MapKeyEnum.EXPIRATION_DATE.name()), "dd/MM/yyyy HH:mm:ss"));
				break;
			case EXP_TT:
				sdaTrace.whatHappened = WhatHappened.SDA_UFI.name();
				sdaTrace.whatHappenedDetail.sdaUfi = new SDASdaUfi(map);
				if (sdaTrace.whatHappenedDetail.sdaUfi.getDATARIMESSACNTRS() == null)
					sdaTrace.whatHappenedDetail.sdaUfi.setDATARIMESSACNTRS(sdaTrace.whenHappened);
				sdaTrace.whatHappenedDetail.sdaUfi.setMODALITAPAGAMENTO(getBillModeCode(map.get(MapKeyEnum.MOD_RIMBORSO_CONTRASSEGNO.name())));
				sdaTrace.whatHappenedDetail.sdaUfi.setSTATUS(StatusEnum.GCP.name());

				if(sdaTrace.whatHappenedDetail.sdaUfi.getIdOperatore()==null || sdaTrace.whatHappenedDetail.sdaUfi.getIdOperatore().equals(""))
					sdaTrace.whatHappenedDetail.sdaUfi.setIdOperatore(map.get(MapKeyEnum.OP.name()));
				if(sdaTrace.whatHappenedDetail.sdaUfi.getIdChiosco()==null || sdaTrace.whatHappenedDetail.sdaUfi.getIdChiosco().equals(""))
					sdaTrace.whatHappenedDetail.sdaUfi.setIdChiosco(t.getIdChannel());

				if (map.containsKey(MapKeyEnum.EXPIRATION_DATE.name()))
					sdaTrace.whatHappenedDetail.sdaUfi.setExpirationDate(toXMLGregorian(map.get(MapKeyEnum.EXPIRATION_DATE.name()), "dd/MM/yyyy HH:mm:ss"));
				break;
			case ESI_OMP:
				sdaTrace.whatHappened = WhatHappened.SDA_UFI.name();
				sdaTrace.whatHappenedDetail.sdaUfi = new SDASdaUfi(map);
				if (sdaTrace.whatHappenedDetail.sdaUfi.getDATARIMESSACNTRS() == null)
					sdaTrace.whatHappenedDetail.sdaUfi.setDATARIMESSACNTRS(sdaTrace.whenHappened);
				sdaTrace.whatHappenedDetail.sdaUfi.setMODALITAPAGAMENTO(getBillModeCode(map.get(MapKeyEnum.MOD_RIMBORSO_CONTRASSEGNO.name())));
				/*
				per lo status da impostare verifico la propriet� CAUNOTIF della traccia
				pacco ritirato dal cliente
					channel "RTZ"
					Status "CNP"
				pacco rifiutato dal cliente
					channel "RTZ"
					Status "RFP"
				pacco smarrito
					channel "RTZ"
					Status "SMA"
				*/
				String CAUNOTIF = map.get("CAUNOTIF");
				if (CAUNOTIF.isEmpty() || CAUNOTIF == null) {
					//salto la traccia
					log.warn(String.format("CAUNOTIF non valorizzato per la traccia id=%s traccia=%s", t.getTracedEntity(), t.toString()));
					sdaTrace = null;
					continue;
				}
				CaunotifEnum tCaunotif = Enum.valueOf(CaunotifEnum.class, CAUNOTIF);
				switch (tCaunotif) {
				case PC1:
					sdaTrace.whatHappenedDetail.sdaUfi.setSTATUS(StatusEnum.CNP.name());
					break;
				case PT6:
					sdaTrace.whatHappenedDetail.sdaUfi.setSTATUS(StatusEnum.SMA.name());
					break;
				case PC2:
					sdaTrace.whatHappenedDetail.sdaUfi.setSTATUS(StatusEnum.RFP.name());
					break;
				}

				if(sdaTrace.whatHappenedDetail.sdaUfi.getIdOperatore()==null || sdaTrace.whatHappenedDetail.sdaUfi.getIdOperatore().equals(""))
					sdaTrace.whatHappenedDetail.sdaUfi.setIdOperatore(map.get(MapKeyEnum.OP.name()));
				if(sdaTrace.whatHappenedDetail.sdaUfi.getIdChiosco()==null || sdaTrace.whatHappenedDetail.sdaUfi.getIdChiosco().equals(""))
					sdaTrace.whatHappenedDetail.sdaUfi.setIdChiosco(t.getIdChannel());

				break;
			default:
				log.warn(String.format("WhatHappened '%s' non previsto per la traccia %s", whatHappened, t.getTracedEntity()));
				break;
			}

			// WhereHappenedDetail
			if (t.getWhereHappened().equals("UNSP") || t.getWhereHappened().equals("UP")) {
				sdaTrace.whereHappenedDetail = new SDATraceWhereDetail();
				sdaTrace.whereHappenedDetail.office = new SDAOffice(t.getIdChannel(), "...");
			}

			// TraceEntityDetail
			sdaTrace.traceEntityDetail = new SDATraceEntityDetail();
			sdaTraceList.add(sdaTrace);
		}

		SDAMessage message = new SDAMessage();
		message.header = (header);
		message.traces = (sdaTraceList);

		return message;
	}

	public String marshal(Object o, Schema schema) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(o.getClass());
		Marshaller marshaller = context.createMarshaller();
		StringWriter sw = new StringWriter();
		if (schema != null)
			marshaller.setSchema(schema);
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(o, sw);

		return sw.toString();
	}

	/**
	 * @param o
	 * @param schema
	 * @param veh
	 * @return
	 * @throws ToSDAException 
	 * @throws SAXNotSupportedException
	 * @throws SAXNotRecognizedException
	 * @throws JAXBException
	 */
	public String marshal(Object o, Schema schema, ValidationEventHandler veh) throws ToSDAException {
		nonCompliantTraces = new ArrayList<NonCompliantTrace>(0);
		Marshaller marshaller = null;
		StringWriter sw = new StringWriter();
		try {
			log.debug("Numero di tracce prima delle operazione di validazione: " + ((SDAMessage) o).traces.size());

			JAXBContext context = JAXBContext.newInstance(o.getClass());
			marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			if (schema != null)
				marshaller.setSchema(schema);

			((TraceValidator) veh).resetEventHanlder();
			marshaller.setEventHandler(veh);
			marshaller.setListener((Marshaller.Listener) veh);

			marshaller.marshal(o, sw);

			nonCompliantTraces = ((TraceValidator) veh).getNonCompliantTraces();
			// se non ci sono messaggi in errore � inutile ricreare l'xml
			if (nonCompliantTraces == null || nonCompliantTraces.size() == 0)
				return sw.toString();
			// Scorro l'array dalla fine per eliminare tutti i nodi errati
			for (int i = nonCompliantTraces.size(); i > 0; i--) {
				NonCompliantTrace nonCompliantTrace = nonCompliantTraces.get(i - 1);
				((SDAMessage) o).traces.remove(nonCompliantTrace.getPosition());
				log.debug("Rimossa traccia [" + nonCompliantTrace.getPosition() + "] per il pacco ["
						+ nonCompliantTrace.getSdaTrace().idTracedEntity + "] con evento [" + nonCompliantTrace.getSdaTrace().whatHappened
						+ "] - Motivo [" + nonCompliantTrace.getReason() + "]");
			}

			log.debug("Numero di tracce dopo la validazione [" + ((SDAMessage) o).traces.size() + "] - sono state eliminare ["
					+ nonCompliantTraces.size() + "] tracce");

			if (((SDAMessage) o).traces.size() <= 0)
//				throw new RuntimeException("Nessun traccia contenuta nel messaggio ha superato la validazione con l'xsd");
				throw new ToSDAException("Nessun traccia contenuta nel messaggio ha superato la validazione con l'xsd");

			sw = new StringWriter();

			marshaller.setEventHandler(new DefaultValidationEventHandler());
			marshaller.marshal(o, sw);

		} catch (JAXBException saxe) {
			saxe.printStackTrace();
		}

		return sw.toString();
	}

	public List<NonCompliantTrace> getNonCompliantTraces() {
		return nonCompliantTraces;
	}

	public Schema getSchemaFromXSD(String xsdPath) throws SAXException {
		log.debug("XSD: " + xsdPath);
		URL url = null;
		try {
			url = new URL(xsdPath);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SchemaFactory sf = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
		// Schema schema = sf.newSchema(new File(xsdPath));
		Schema schema = sf.newSchema(url);
		return schema;
	}

	/**
	 * date conversion from GregorianCalendar to XML notation
	 *
	 * @param gregorianCal
	 *            the GregorianCalendar formatted date to convert
	 * @return the XMLGregorianCalendar date formatted
	 */
	public XMLGregorianCalendar toXMLGregorian(GregorianCalendar gregorianCal) {
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCal);
		} catch (DatatypeConfigurationException e) {
			log.debug("Date conversion failed ", e);
		}
		return null;
	}

	/**
	 * date conversion from Date to XML notation
	 *
	 * @param date
	 *            the Date to convert
	 * @return the XMLGregorianCalendar date formatted
	 */
	public XMLGregorianCalendar toXMLGregorian(Date date) {
		GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance();
		calendar.setTime(date);
		return toXMLGregorian(calendar);
	}

	/**
	 * date conversion from Formattes String to XML notation
	 *
	 * @param date
	 *            the String to convert
	 * @return the XMLGregorianCalendar date formatted
	 */
	public XMLGregorianCalendar toXMLGregorian(String formattedStr, String formatStr) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
			return toXMLGregorian(sdf.parse(formattedStr));
		} catch (ParseException e) {
			log.debug("Date conversion failed ", e);
		}
		return null;
	}

	/**
	 * date conversion from String to XML notation
	 *
	 * @param date
	 *            the String to convert
	 * @return the XMLGregorianCalendar date formatted
	 */
	public XMLGregorianCalendar toXMLGregorian(String dateStr) {
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(dateStr);
		} catch (DatatypeConfigurationException e) {
			log.debug("Date conversion failed ", e);
		}
		return null;
	}

	/**
	 * try to seek SDA_CODE field from DB translations
	 *
	 * @param keyCode
	 *            String the code to seek
	 * @param mpDao
	 *            the referred dao for search
	 * @return transcodified SDA_CODE field if found, null otherwise
	 */
	private String getBillModeCode(String keyCode) {
		try {
			ModPagContrPiSdaPK seekKey = new ModPagContrPiSdaPK() {
				{
					setPiChannel("TT");
				}
			};
			seekKey.setPiCode(keyCode);
			ModPagContrPiSda modPag = modPagDao.findById(seekKey, false);
			if (modPag != null)
				return modPag.getSdaCode();
		} catch (Exception ex) {
			log.warn("Unable to load mod. pag. codes from DB: " + ex.getMessage());
		}
		log.warn("Unable to find translation for code: " + keyCode + "; used as is");
		return keyCode;
	}

	/**
	 * try to seek ID_STATUS field from trace details if absent
	 *
	 * @param seekTrace
	 *            the Trace to search in
	 * @param detailMap
	 *            Map of trace details
	 * @return String ID_STATUS field if found, null otherwise
	 */
	private String tryToFindIdStatusIfNull(Trace seekTrace, Map<String, String> detailMap) {
		if (seekTrace.getIdStatus() == null) {
			if (detailMap.containsKey(MapKeyEnum.ID_STATUS.name()))
				return getIdStatusTranscoded(detailMap.get(MapKeyEnum.ID_STATUS.name()), seekTrace.getChannel());
			if (detailMap.containsKey(MapKeyEnum.STATUS.name()))
				return getIdStatusTranscoded(detailMap.get(MapKeyEnum.STATUS.name()), seekTrace.getChannel());
			if (detailMap.containsKey(MapKeyEnum.DELREASON.name()))
				return getIdStatusTranscoded(detailMap.get(MapKeyEnum.DELREASON.name()), seekTrace.getChannel());
			if (detailMap.containsKey(MapKeyEnum.PSTF.name())) {
				String dfValue = (detailMap.containsKey(MapKeyEnum.DF.name()) ? detailMap.get(MapKeyEnum.DF.name()) : "");
				return getIdStatusTranscoded("DF_" + dfValue + detailMap.get(MapKeyEnum.PSTF.name()), seekTrace.getChannel());
			}
		} else {
			if("OMP".equals(seekTrace.getChannel())) {
				return getIdStatusTranscoded(seekTrace.getIdStatus(), seekTrace.getChannel());
			}
		}
		return seekTrace.getIdStatus();
	}

	/**
	 * try to get transcoded ID_STATUS field from DB
	 *
	 * @param keyCode
	 *            String the code to seek
	 * @param keyChannel
	 *            String the channel to seek
	 * @return String transcodified ID_STATUS field if found, null otherwise
	 */
	private String getIdStatusTranscoded(String keyCode, String keyChannel) {
		try {
			StatusPiSdaPK seekKey = new StatusPiSdaPK();
			seekKey.setPiChannel(keyChannel);
			seekKey.setStatusPI(keyCode);
			StatusPiSda statusSda = idStatusDao.findById(seekKey, false);
			if (statusSda != null)
				return statusSda.getStatusSDA();
		} catch (Exception ex) {
			log.warn("Unable to load status codes from DB: " + ex.getMessage());
		}
		log.warn("Unable to find translation for code: " + keyCode + "; used as is");
		return keyCode;
	}

	/**
	 * Convert backToSender information in SDA Cod Istr
	 *
	 * @param backToSender
	 *            T rappresents True
	 * @return SDA code
	 */
	private String getCodIstr(String backToSender) {
		return "B".equals(backToSender) ? "R" : null;
	}

	/**
	 * Convert backToSender information in SDA RRR Flag
	 *
	 * @param backToSender
	 *            T rappresents True
	 * @return SDA code
	 */
	private String getFlagRRR(String backToSender) {
		return "B".equals(backToSender) ? "RRR" : null;
	}
}
