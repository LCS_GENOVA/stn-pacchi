/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.omp.test.token;


import junit.framework.Assert;

import org.junit.Test;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.elsagdatamat.dbtrace.bridge.engine.entities.Token;
import com.selexelsag.dbtrace.bridge.to.omp.manager.token.OMPTokenDecorator;
import com.selexelsag.dbtrace.bridge.to.omp.manager.token.TokenDecorator;

/**
 * @author arodriguez
 *
 */
public class TokenDecoratorTest {

	@Test
	public void tokenDecoratorIsTokenWithToTokenValues() {
		ToToken ompToken = new ToToken();
		ompToken.setWsToken("testWsToken");
		TokenDecorator tokenDecorator = new TokenDecorator(new Token(), ompToken);
		Assert.assertNotNull(tokenDecorator);
		Assert.assertEquals(tokenDecorator.getServiceId(), ompToken.getServiceId());
		Assert.assertEquals(tokenDecorator.getWsToken(), ompToken.getWsToken());
	}
	
	@Test
	public void ompTokenDecoratorIsToTokenWithTokenValues() {
		Token token = new Token() {{
			setServiceId("testServiceId");
			setWsToken("testWsToken");
		}};
		OMPTokenDecorator ompTokenDecorator = new OMPTokenDecorator(new ToToken(), token);
		Assert.assertNotNull(ompTokenDecorator);
		Assert.assertEquals(ompTokenDecorator.getServiceId(), token.getServiceId());
		Assert.assertEquals(ompTokenDecorator.getWsToken(), token.getWsToken());
	}
	
}
