package com.selexelsag.tt.cruscotto.pacchi.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "PK_CON_ATT_TRAC_CAN")
@IdClass(PkConProdottoPK.class)
public class PkConProdotto implements Serializable {

	private static final long serialVersionUID = 6234831928024454303L;

	@Id // TRACE.LABEL_TRACED_ENTITY
	@Column(name = "COD_PRODOTTO", nullable = false)
	private String prodotto;

	@Id // TRACE.ID_CHANNEL
	@Column(name = "COD_CANALE", nullable = false)
	private String canale;

	@Id // TRACE.WHAT_HAPPENED
	@Column(name = "COD_TRACCIA", nullable = false)
	private String tipoTraccia;


	@Column(name = "ATTIVATA", nullable =false)
	private String attivata_string;
	
	public String getTipoTraccia(){
		return tipoTraccia;
	}

	public String getProdotto(){
		return prodotto;
	}

	public String getCanale(){
		return canale;
	}

	public void setTipoTraccia(String tipoTraccia){
		this.tipoTraccia=tipoTraccia;
	}

	public void setProdotto(String prodotto){
		this.prodotto=prodotto;
	}

	public void setCanale(String canale){
		this.canale=canale;
	}

	public void setAttivata(String attivata){
		this.attivata_string=attivata;
	}

	public String getAttivata(){
		return attivata_string;
	}
	
	// sul DB questo campo � diventato un char. Adesso uso una variabile transient per ottenere un boolean.
	// Sar� da modificare poi
	public boolean isFlag(){
		if(attivata_string.equalsIgnoreCase("s"))
			return true;
		return false;
	}
}
