package com.elsagdatamat.dbtrace.dao;

import com.elsagdatamat.dbtrace.entity.PkPrdProdottiSdaVW;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class PkPrdProdottiSdaVWDAO extends SpringHibernateJpaDAOBase<PkPrdProdottiSdaVW, String> 
implements IPkPrdProdottiSdaVWDAO{

}
