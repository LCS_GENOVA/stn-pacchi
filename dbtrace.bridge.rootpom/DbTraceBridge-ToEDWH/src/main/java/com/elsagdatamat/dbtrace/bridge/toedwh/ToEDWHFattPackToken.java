package com.elsagdatamat.dbtrace.bridge.toedwh;

public class ToEDWHFattPackToken extends ToEDWHToken {
	public static final String TOKEN_ID="DBTraceToEDWHFattPackBridge";
	
	@Override
	public String getServiceId() {
		return TOKEN_ID;
	}
}
