package com.elsagdatamat.dbtrace.fromsda.bean;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
"CHANNEL",
"MSG_DATE",
"MSG_TYPE",
"SERVICE",
"VERSION"
})
public class HEADER {

/**
* The Channel Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("CHANNEL")
private String cHANNEL = "SDA";
/**
* The Msg_date Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("MSG_DATE")
private Date mSGDATE;
/**
* The Msg_type Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("MSG_TYPE")
private String mSGTYPE = "TPA";
/**
* The Service Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("SERVICE")
private String sERVICE = "PACCHI";
/**
* The Version Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("VERSION")
private String vERSION = "1.0";

/**
* The Channel Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("CHANNEL")
public String getCHANNEL() {
return cHANNEL;
 }

/**
* The Channel Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("CHANNEL")
public void setCHANNEL(String cHANNEL) {
this.cHANNEL = cHANNEL;
 }

/**
* The Msg_date Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("MSG_DATE")
@JsonSerialize(using = CustomJsonDateSerializer.class)
public Date getMSGDATE() {
return mSGDATE;
 }

/**
* The Msg_date Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("MSG_DATE")
@JsonDeserialize(using = CustomJsonDateDeserializer .class)
public void setMSGDATE(Date mSGDATE) {
this.mSGDATE = mSGDATE;
 }

/**
* The Msg_type Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("MSG_TYPE")
public String getMSGTYPE() {
return mSGTYPE;
 }

/**
* The Msg_type Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("MSG_TYPE")
public void setMSGTYPE(String mSGTYPE) {
this.mSGTYPE = mSGTYPE;
 }

/**
* The Service Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("SERVICE")
public String getSERVICE() {
return sERVICE;
 }

/**
* The Service Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("SERVICE")
public void setSERVICE(String sERVICE) {
this.sERVICE = sERVICE;
 }

/**
* The Version Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("VERSION")
public String getVERSION() {
return vERSION;
 }

/**
* The Version Schema
* <p>
* An explanation about the purpose of this instance.
* (Required)
* 
*/
@JsonProperty("VERSION")
public void setVERSION(String vERSION) {
this.vERSION = vERSION;
 }

}