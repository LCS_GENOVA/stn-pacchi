package com.selexelsag.dbtrace.bridge.to.ssip.message;


public interface SSIPMessageBuilderInterface {

	public static final String DEFAULT_NAME = "ssipMessageBuilder";

	public long[] getNumberOfRuns();
	
	public void setNumberOfRuns(long[] numberOfRuns);
	
	public void sendTestMessage();
	
	public void loadQueryParams();
}