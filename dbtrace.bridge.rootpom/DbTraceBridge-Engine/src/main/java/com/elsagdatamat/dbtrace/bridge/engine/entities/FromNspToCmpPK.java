package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;

import javax.persistence.Column;

public class FromNspToCmpPK implements Serializable {

	private static final long serialVersionUID = -820429331673620874L;

	public static final String FRAZIONARIO_NSP = "FrazionarioNsp";
	public static final String FRAZIONARIO_CMP = "FrazionarioCmp";

	private String FrazionarioNsp;
	private String FrazionarioCmp;

	@Column(name = "FRAZIONARIO_NSP")
	public String getFrazionarioNsp() {
		return FrazionarioNsp;
	}

	public void setFrazionarioNsp(String frazionarioNsp) {
		FrazionarioNsp = frazionarioNsp;
	}

	@Column(name = "FRAZIONARIO_CMP")
	public String getFrazionarioCmp() {
		return FrazionarioCmp;
	}

	public void setFrazionarioCmp(String frazionarioCmp) {
		FrazionarioCmp = frazionarioCmp;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((FrazionarioCmp == null) ? 0 : FrazionarioCmp.hashCode());
		result = prime * result + ((FrazionarioNsp == null) ? 0 : FrazionarioNsp.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FromNspToCmpPK other = (FromNspToCmpPK) obj;
		if (FrazionarioCmp == null) {
			if (other.FrazionarioCmp != null)
				return false;
		} else if (!FrazionarioCmp.equals(other.FrazionarioCmp))
			return false;
		if (FrazionarioNsp == null) {
			if (other.FrazionarioNsp != null)
				return false;
		} else if (!FrazionarioNsp.equals(other.FrazionarioNsp))
			return false;
		return true;
	}
}
