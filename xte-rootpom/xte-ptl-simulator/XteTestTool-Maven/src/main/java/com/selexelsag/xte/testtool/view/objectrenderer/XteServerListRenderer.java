/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.objectrenderer;

import com.selexelsag.xte.testtool.datamodel.XteServer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;

/**
 *
 * @author Da Procida
 */
public class XteServerListRenderer implements ListCellRenderer {

    protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index,
            boolean isSelected, boolean cellHasFocus) {

        JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
                isSelected, cellHasFocus);

        Object obj = (Object) list.getModel().getElementAt(index);
        if(obj != null)
        {
            if(obj instanceof XteServer)
            {
                XteServer xteServer = (XteServer) obj;
                if (xteServer.getServerBaseURL()!= null && xteServer.getServerDescription()!=null) {                    
                    String xteServerString = " " + xteServer.getServerDescription() + " - " + xteServer.getServerBaseURL();
                    renderer.setText(xteServerString);
                        if(xteServer.isActive())
                        {
                            renderer.setIcon(new ImageIcon(getClass().getResource("/images/GreenLight.png")));
                        }
                        else if(!xteServer.isActive())
                        {
                            renderer.setIcon(new ImageIcon(getClass().getResource("/images/RedLight.png")));
                        }
                        else
                            renderer.setIcon(null);
                }
            }
        }

        renderer.setPreferredSize(new Dimension(600,50));
        renderer.setFont(new Font("Tahoma", Font.BOLD, 20));

        if(list.isEnabled())
        {
            renderer.setForeground(new Color(51,51,51));
        }
        else
        {
            renderer.setForeground(new Color(204,204,204));
        }

        if(list.hasFocus())
        {
            ((JScrollPane)list.getParent().getParent()).setBorder(BorderFactory.createLineBorder(new Color(255,204,51),2));
        }
        else
        {
            ((JScrollPane)list.getParent().getParent()).setBorder(BorderFactory.createLineBorder(new Color(102,102,102),1));
        }

        if(isSelected)
        {
            renderer.setBackground(new Color(102,102,255));
            renderer.setForeground(new Color(255,255,255));
        }
        else
        {
            renderer.setBackground(Color.WHITE);
            renderer.setForeground(new Color(51,51,51));
        }

        return renderer;
    }

}
