package com.selexelsag.xte.business.exceptions;

public class BusinessServiceRequestException extends BusinessException {

	public BusinessServiceRequestException(String message, Throwable cause) {
		super(message, cause);
		setExceptionId(BUSINESS_VALIDATION_ID);
	}
	
}
