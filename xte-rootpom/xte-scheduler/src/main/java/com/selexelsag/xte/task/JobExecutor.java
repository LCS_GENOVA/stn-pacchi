package com.selexelsag.xte.task;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.quartz.StatefulJob;
import org.springframework.context.ApplicationContext;

public class JobExecutor implements StatefulJob {

	private static final String APPLICATION_CONTEXT_KEY = "task-service-context";
//	private static final Log log = LogFactory.getLog(JobExecutor.class);
	private static Logger _log = Logger.getLogger(JobExecutor.class);
	
	private ApplicationContext getApplicationContext(JobExecutionContext context ) throws SchedulerException
	{
        ApplicationContext appCtx = null;
        appCtx = (ApplicationContext)context.getScheduler().getContext().get(APPLICATION_CONTEXT_KEY);
        if (appCtx == null) {
            throw new RuntimeException(
                    "No application context available in scheduler context for key \"" + APPLICATION_CONTEXT_KEY + "\"");
        }

		return appCtx;
	}
	
	//restituisce il nome della classe che implementa il  task
	private String getClassName(JobExecutionContext context )
	{
        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        if(dataMap == null)
        	 throw new RuntimeException("No job data map available");

		return (String) dataMap.get("beanName");
	}
	
	@Override
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		String beanName = getClassName(jobExecutionContext);	
		
		try {
			ApplicationContext appCtx = getApplicationContext(jobExecutionContext);
			IRunOnce runOnceTask = (IRunOnce) appCtx.getBean(beanName);
			Date startDate = new Date();
			_log.info(String.format("Start '%s' execution", beanName));
			runOnceTask.runOnce();
			
			long milliseconds = new Date().getTime()-startDate.getTime();
			
			_log.info(String.format("End '%s' execution; Time spent: %d ms", beanName, milliseconds));
		} catch (Exception e) {
			_log.error("ERROR JobExecutor: " + new String[] {e.getMessage()});
			throw new JobExecutionException(String.format("Error executing '%s': %s", beanName, e.getMessage()), e, false);
		}
	}
}
