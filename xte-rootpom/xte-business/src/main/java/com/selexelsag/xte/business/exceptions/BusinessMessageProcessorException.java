package com.selexelsag.xte.business.exceptions;

public class BusinessMessageProcessorException extends BusinessException {

	public BusinessMessageProcessorException(String message, Throwable cause) {
		super(message, cause);
		setExceptionId(BUSINESS_MESSAGE_PROCESSOR_ID);
	}
}
