package com.selexelsag.dbtrace.bridge.to.ssip.manager;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.elsagdatamat.dbtrace.bridge.engine.dao.StatusPiSdaDAO;
import com.elsagdatamat.dbtrace.bridge.engine.entities.StatusPiSda;
import com.elsagdatamat.dbtrace.bridge.engine.entities.StatusPiSdaPK;

/**
 * 
 * @author arodriguez
 * 
 */
public class SSIPManager extends SSIPAbstractManager implements SSIPManagerInterface {
	
	private static final Log log = LogFactory.getLog(SSIPManager.class);

	private Integer minutesToRecoveryMinValue = 60;
	private Integer minutesToRestartMinValue = 15;

	private boolean releaseDateNotExpired(Date releaseDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -1 * minutesToRestartMinValue);
		if (releaseDate != null && releaseDate.compareTo(calendar.getTime()) > 0) {
			return true;
		}
		return false;
	}

	private boolean accessDateExpired(Date accessDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -1 * minutesToRecoveryMinValue);
		if (accessDate == null || accessDate.compareTo(calendar.getTime()) < 0) {
			return true;
		}
		return false;
	}

	@Override
	public void setMinutesToRecoveryMinValue(Integer minutesToRecoveryMinValue) {
		this.minutesToRecoveryMinValue = minutesToRecoveryMinValue;
	}

	@Override
	public void setMinutesToRestartMinValue(Integer minutesToRestartMinValue) {
		this.minutesToRestartMinValue = minutesToRestartMinValue;
	}

	@Override
	public Integer getMinutesToRecoveryMinValue() {
		return minutesToRecoveryMinValue;
	}

	@Override
	public Integer getMinutesToRestartMinValue() {
		return minutesToRestartMinValue;
	}

	/**
	 * try to get transcoded ID_STATUS field from DB
	 * @param keyCode  String the code to seek
	 * @param keyChannel  String the channel to seek
	 * @return String  transcodified ID_STATUS field if found, null otherwise
	 */
	@Override
	public String getIdStatusTranscoded(String keyCode, String keyChannel) {
		try {
			StatusPiSdaDAO idStatusDao = new StatusPiSdaDAO();
			StatusPiSdaPK seekKey = new StatusPiSdaPK();
			seekKey.setPiChannel(keyChannel);
			seekKey.setStatusPI(keyCode);
			StatusPiSda statusSda = idStatusDao.findById(seekKey, false);
			if (statusSda != null)
				return statusSda.getStatusSDA();
		} catch(Exception ex) {
			log.warn("Unable to load status codes from DB: " + ex.getMessage());
		}
		log.warn("Unable to find translation for code: " + keyCode + "; used as is");
		return keyCode;
	}

}
