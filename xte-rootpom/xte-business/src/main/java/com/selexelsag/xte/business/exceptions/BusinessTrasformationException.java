package com.selexelsag.xte.business.exceptions;

public class BusinessTrasformationException extends BusinessException {

	public BusinessTrasformationException(String message, Throwable cause) {
		super(message, cause);
		setExceptionId(BUSINESS_TRANSFORM_ID);
	}
	
}
