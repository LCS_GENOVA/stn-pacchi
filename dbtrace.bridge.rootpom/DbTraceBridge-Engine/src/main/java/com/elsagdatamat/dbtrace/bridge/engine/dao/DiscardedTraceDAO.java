package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.DiscardedTrace;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class DiscardedTraceDAO  extends SpringHibernateJpaDAOBase <DiscardedTrace, Long>
	implements IDiscardedTraceDAO {

}
