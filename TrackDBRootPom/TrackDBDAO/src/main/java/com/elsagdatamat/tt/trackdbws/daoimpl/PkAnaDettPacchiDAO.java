package com.elsagdatamat.tt.trackdbws.daoimpl;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.framework.dao.hibernate.HibernatePrimitiveDAO;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.tt.trackdbws.beans.VO.PackDataRetrieveVO;
import com.elsagdatamat.tt.trackdbws.dao.IPkAnaDettPacchiDAO;
import com.selexelsag.tt.cruscottopacchi.bean.PkAnaDettPacchi;
import com.selexelsag.tt.cruscottopacchi.bean.PkAnaDettPacchiPK;
/**
* R.L. BeanGenerator - 27.05.2012 18:17:43
* R.L. - Inseriti per il nuovo metodo del trackDbWS - packDataRetrieve
*/
public class PkAnaDettPacchiDAO extends SpringHibernateJpaDAOBase<PkAnaDettPacchi,PkAnaDettPacchiPK> implements IPkAnaDettPacchiDAO{

	class ParametricHQLQueryResult<T> {

		@SuppressWarnings("unchecked")
		@Transactional(propagation=Propagation.SUPPORTS)
		public List<T> executeHqlQuery(String hqlQuery) {

			class MyJpaCallback<T1> implements JpaCallback {
				String hqlQuery;

				public MyJpaCallback(String hqlQuery){
					this.hqlQuery = hqlQuery;
				}

				@Override
				@Transactional(propagation=Propagation.SUPPORTS)
				public List<T1> doInJpa(EntityManager em) throws PersistenceException {
					return (List<T1>)HibernatePrimitiveDAO._executeHqlQuery(
							((HibernateEntityManager) em).getSession(),
							hqlQuery
							);
				}
			}

			MyJpaCallback<T> myJpaCallbak = new MyJpaCallback<T>(hqlQuery);
			List<T> listOfTObject = (List<T>) getJpaTemplate().execute(myJpaCallbak,true);

			return listOfTObject;
		}
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<PackDataRetrieveVO> getListOfPackDataRetrieveVO(String hqlQuery) {
		ParametricHQLQueryResult<PackDataRetrieveVO> parametricHQLQueryResult = new ParametricHQLQueryResult<PackDataRetrieveVO>();
		return parametricHQLQueryResult.executeHqlQuery(hqlQuery);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<PackDataRetrieveVO> getListOfPackDataRetrieveVONQ(String idPacco) {

		List<Object> tmp = executeNamedQuery_obj(PkAnaDettPacchi.GET_PACK_DATA_RETREIVE,new String[] { "idPacco" }, new Object[] { idPacco });
		List<PackDataRetrieveVO> converted = new ArrayList<PackDataRetrieveVO>();
		if(tmp!=null && tmp.size()>0){
			for(Object obj : tmp){
				converted.add((PackDataRetrieveVO)obj);
			}
		}
		return converted;
	}



}