package com.selexelsag.xte.service.cache;


/**
 * Interfaccia per la cache per risorse. Prevede un numero di item massimo, raggiunto il quale i 
 * nuovi elementi richiesti prendono il posto secondo la policy definita dall'implementazione.
 * @author Frosi
 *
 */
public interface ICacheManager 
{
	public void init()  throws Exception;
	public void setCacheId(String cacheId);
	public Object getItem(String key) throws Exception;
	public void setMaxItem(Integer maxItem);
}
