package com.selexelsag.tt.cruscottopacchi.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
* R.L. BeanGenerator - 28.05.2012 16:22:59
* R.L. - Inseriti per il nuovo metodo del trackDbWS - packDataRetrieve
*/


@Entity
@Table(name="PK_ANA_DETT_PACCHI")
@IdClass(PkAnaDettPacchiPK.class)
@NamedQueries(value={
		@NamedQuery(name="PkAnaDettPacchi.GET_PACK_DATA_RETREIVE", query="SELECT new com.elsagdatamat.tt.trackdbws.beans.VO.PackDataRetrieveVO(det,con)" +
				   				  "FROM PkAnaDettPacchi det, PkConMappingDettTr con " +
				   				  "WHERE det.whatHappened = con.codTraccia " +
				   				  "AND det.nome = con.dettOrig " +
				   				  "AND det.idPacco =  :idPacco ")
})
public class PkAnaDettPacchi implements Serializable {

	private static final long serialVersionUID = 2991649696276408497L;

	public static final String GET_PACK_DATA_RETREIVE = "GET_PACK_DATA_RETREIVE";

	@Id
	@Column(name = "COD_TRACCIA", nullable =false)
	private String whatHappened;

	@Column(name = "VALORE", nullable =true)
	private String valore;

	@Id
	@Column(name = "NOME", nullable =false)
	private String nome;

	@Id
	@Column(name = "ID_PACCO", nullable =false)
	private String idPacco;

	public PkAnaDettPacchi(){};

	public PkAnaDettPacchi(String valore, String nome, String idPacco, String whatHappened) {
		super();
		this.valore = valore;
		this.nome = nome;
		this.idPacco = idPacco;
		this.whatHappened=whatHappened;
	}

	public String getValore(){
		return valore;
	}

	public String getNome(){
		return nome;
	}

	public String getIdPacco(){
		return idPacco;
	}

	public void setValore(String valore){
		this.valore=valore;
	}

	public void setNome(String nome){
		this.nome=nome;
	}

	public void setIdPacco(String idPacco){
		this.idPacco=idPacco;
	}

	public String getWhatHappened() {
		return whatHappened;
	}

	public void setWhatHappened(String whatHappened) {
		this.whatHappened = whatHappened;
	}

}