/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.config;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.logging.LoggerConfig;
import com.selexelsag.xte.testtool.xml.Function;
import com.selexelsag.xte.testtool.xml.XmlFileManager;
import com.selexelsag.xtetesttool.xml.accettazioneonline.AccettazioneOnline;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta;
import com.selexelsag.xtetesttool.xml.lookup.Lookup;
import com.selexelsag.xtetesttool.xml.postaregistrata.PostaRegistrata;
import java.util.List;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.ConfigurationUtils;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolConfiguration {

    public static final String INIT_CONFIGURATION_FILE = "config/init-configuration.properties";
    public static final String CONFIGURATION_FILE = "config/configuration.properties";
    public static final String POSTAL_FUNCTIONS_FILE = "xml/XtePostalFunctions.xml";
    public static final String POSTA_REGISTRATA_FILE = "xml/PostaRegistrata/PostaRegistrataConfig.xml";
    public static final String DATA_POSTA_FILE = "xml/DataPosta/DataPostaConfig.xml";
    public static final String ACCETTAZIONE_ONLINE_FILE = "xml/AccettazioneOnline/AccettazioneOnlineConfig.xml";
    public static final String LOOKUP_FILE = "xml/Lookup/LookupConfig.xml";
    private Logger logger = Logger.getLogger(XteTestToolConfiguration.class);
    private PropertiesConfiguration initConfiguration = null;
    private PropertiesConfiguration localConfiguration = null;
    private Configuration globalConfiguration = null;
    private List<Function>  postalFunctions= null;
    private PostaRegistrata  postaRegistrataConfig= null;
    private DataPosta  dataPostaConfig= null;
    private AccettazioneOnline  accettazioneOnlineConfig= null;
    private Lookup  lookupConfig= null;
    private static XteTestToolConfiguration instance = null;

    private XteTestToolConfiguration() {
        
        loadConfiguration();
        LoggerConfig.configureLogger("XTE Test Tool");
    }

    public static synchronized XteTestToolConfiguration getInstance() {
        return (instance == null) ? instance = new XteTestToolConfiguration() : instance;
    }

    public Configuration getConfiguration() {
        return globalConfiguration;
    }

    public List<Function> getPostalFunctions() {
        return postalFunctions;
    }
    
    public PostaRegistrata getPostaRegistrataConfig() {
        return postaRegistrataConfig;
    }

    public DataPosta getDataPostaConfig() {
        return dataPostaConfig;
    }
    
    public AccettazioneOnline getAccettazioneOnlineConfigConfig() {
        return accettazioneOnlineConfig;
    }
    
    public void updateConfiguration() {

        loadConfiguration();
        logger.info("XteTestToolConfiguration Global Configuration updated");
    
    }
            
    private void loadConfiguration() {
        logger.info("XteTestToolConfiguration start loadConfiguration");

        FileChangedReloadingStrategy reloadStrategy = new FileChangedReloadingStrategy();
        reloadStrategy.setRefreshDelay(10);        
        try {
            initConfiguration = new PropertiesConfiguration(INIT_CONFIGURATION_FILE);
            initConfiguration.setReloadingStrategy(reloadStrategy);
            initConfiguration.setAutoSave(true);
        } catch (ConfigurationException ex) {
            logger.error("XteTestToolConfiguration loadConfiguration failed: unable to load init configuration file: " + INIT_CONFIGURATION_FILE);
            throw new XteTestToolConfigurationException(Group.CONFIGURATION, Severity.FATAL, "Unable to load init configuration file: " + INIT_CONFIGURATION_FILE, ex);
        }
        try {
            localConfiguration = new PropertiesConfiguration(CONFIGURATION_FILE);
            localConfiguration.setReloadingStrategy(reloadStrategy);
            localConfiguration.setAutoSave(true);
        } catch (ConfigurationException ex) {
            logger.error("XteTestToolConfiguration loadConfiguration failed: unable to load configuration file: " + CONFIGURATION_FILE);
            throw new XteTestToolConfigurationException(Group.CONFIGURATION, Severity.FATAL, "Unable to load configuration file: " + CONFIGURATION_FILE, ex);
        }        
        
        this.globalConfiguration = createGlobalConfiguration(initConfiguration, localConfiguration);
        ConfigurationUtils.dump(globalConfiguration, System.out);
        System.out.println("\n");
        
        XmlFileManager xmlFileManager = new XmlFileManager();
        try {
            this.postalFunctions = xmlFileManager.createPostalFunctionsMenuFromXML(POSTAL_FUNCTIONS_FILE);
        } catch (Exception ex) {
            logger.error("XteTestToolConfiguration loadConfiguration failed: unable to load configuration file: " + POSTAL_FUNCTIONS_FILE);
            throw new XteTestToolConfigurationException(Group.CONFIGURATION, Severity.FATAL, "Unable to load configuration file: " + POSTAL_FUNCTIONS_FILE, ex);
        }        
        try {
            this.postaRegistrataConfig = xmlFileManager.loadPostaRegistrataConfigFromXML(POSTA_REGISTRATA_FILE);
        } catch (Exception ex) {
            logger.error("XteTestToolConfiguration loadConfiguration failed: unable to load configuration file: " + POSTA_REGISTRATA_FILE);
            throw new XteTestToolConfigurationException(Group.CONFIGURATION, Severity.FATAL, "Unable to load configuration file: " + POSTA_REGISTRATA_FILE, ex);
        }        
        try {
            this.dataPostaConfig = xmlFileManager.loadDataPostaConfigFromXML(DATA_POSTA_FILE);
        } catch (Exception ex) {
            logger.error("XteTestToolConfiguration loadConfiguration failed: unable to load configuration file: " + DATA_POSTA_FILE);
            throw new XteTestToolConfigurationException(Group.CONFIGURATION, Severity.FATAL, "Unable to load configuration file: " + DATA_POSTA_FILE, ex);
        }        
        try {
            this.accettazioneOnlineConfig = xmlFileManager.loadAccettazioneOnlineConfigFromXML(ACCETTAZIONE_ONLINE_FILE);
        } catch (Exception ex) {
            logger.error("XteTestToolConfiguration loadConfiguration failed: unable to load configuration file: " + ACCETTAZIONE_ONLINE_FILE);
            throw new XteTestToolConfigurationException(Group.CONFIGURATION, Severity.FATAL, "Unable to load configuration file: " + ACCETTAZIONE_ONLINE_FILE, ex);
        }        
        try {
            this.lookupConfig = xmlFileManager.loadLookupConfigFromXML(LOOKUP_FILE);
        } catch (Exception ex) {
            logger.error("XteTestToolConfiguration loadConfiguration failed: unable to load configuration file: " + LOOKUP_FILE);
            throw new XteTestToolConfigurationException(Group.CONFIGURATION, Severity.FATAL, "Unable to load configuration file: " + LOOKUP_FILE, ex);
        }        
        
        logger.info("XteTestToolConfiguration loadConfiguration completed successfully!");
    }

    private Configuration createGlobalConfiguration(PropertiesConfiguration initConfiguration, PropertiesConfiguration localConfiguration) {
        logger.info("XteTestToolConfiguration start createGlobalConfiguration");
        CompositeConfiguration compositeConfiguration = new CompositeConfiguration();
        compositeConfiguration.addConfiguration(localConfiguration);
        compositeConfiguration.addConfiguration(initConfiguration);
        logger.info("XteTestToolConfiguration createGlobalConfiguration completed successfully!");
        return compositeConfiguration;
    }
}
