/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.datamodel;

/**
 *
 * @author Da Procida
 */
public enum PostalContext {

    POSTA_REGISTRATA("PostaRegistrata"),
    DATA_POSTA("DataPosta"),
    ACQUISIZIONE_MAZZETTI("AcquisizioneMazzetti"),
    ACCETTAZIONE_ONLINE("AccettazioneOnline");
    private final String code;

    PostalContext(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static String getCode(PostalContext postalContext) {
        if (postalContext == null) {
            return null;
        }
        return postalContext.getCode();
    }

    public static PostalContext getByCode(String code) {
        if (POSTA_REGISTRATA.getCode().equalsIgnoreCase(code)) {
            return POSTA_REGISTRATA;
        }
        if (DATA_POSTA.getCode().equalsIgnoreCase(code)) {
            return DATA_POSTA;
        }
        if (ACQUISIZIONE_MAZZETTI.getCode().equalsIgnoreCase(code)) {
            return ACQUISIZIONE_MAZZETTI;
        }
        if (ACCETTAZIONE_ONLINE.getCode().equalsIgnoreCase(code)) {
            return ACCETTAZIONE_ONLINE;
        }
        return null;
    }
}
