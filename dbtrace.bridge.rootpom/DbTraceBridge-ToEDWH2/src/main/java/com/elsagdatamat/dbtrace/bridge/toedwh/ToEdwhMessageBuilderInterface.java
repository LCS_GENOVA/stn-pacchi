package com.elsagdatamat.dbtrace.bridge.toedwh;

import java.io.IOException;
import java.util.List;

import com.elsagdatamat.trackdb.toedwh.vo.EdwhTracesList;
import com.elsagdatamat.trackdb.toedwh.vo.TraceVO;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public interface ToEdwhMessageBuilderInterface {

	public abstract List<String> create(TracesList input);

	public abstract List<String> create(EdwhTracesList input);

	// IMessageSender
	public abstract void send(List<String> messageList) throws IOException;

	public abstract List<TraceVO> findTraces(Long id, Long maxRows, Boolean allTypes);

	public abstract void insertSentTraces(List<TraceVO> traces);

	void loadParams();

}