/**
 * 
 */
package com.elsagdatamat.dbtrace.bridge.toedwh;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.GZIPOutputStream;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.core.MessageCreator;

/**
 * @author arodriguez
 *
 */
public class EDWHCompressedMessageCreator implements Serializable, MessageCreator, JmsHeaderConstantsInterface {

	private String textMessage;

	/**
	 * @param destinationOfficeCode
	 * @param containerCode
	 */
	public EDWHCompressedMessageCreator(String textMessage) {
	    this.textMessage=textMessage;
	}

	
	protected String getSentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		return sdf.format(new Date());
	}

	
	protected String getSourceOfficeCode() {
		return DESTINATION_DBTRACE;
	}


	
	protected String getMessageType() {
		return "D1P";
	}


	
	protected boolean isTestMessage() {
		return false;
	}

	
	protected boolean isCompressed() {
		return true;
	}

	@Override
	public Message createMessage(Session jmsSession) throws JMSException {
		BytesMessage jmsBytesMessage = jmsSession.createBytesMessage();
		buildMessageHeader(jmsBytesMessage);
		try {
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			GZIPOutputStream gzipOut = new GZIPOutputStream(bout);
			gzipOut.write(textMessage.getBytes());
			gzipOut.finish();
			jmsBytesMessage.writeBytes(bout.toByteArray());
			gzipOut.close();
		} catch (IOException e) {
			throw new JMSException(e.getMessage());
		}
		return jmsBytesMessage;
	}
	
	private void buildMessageHeader(BytesMessage message) throws JMSException {
		message.setStringProperty(SENT_DATE_KEY, getSentDate());
		message.setStringProperty(MESSAGE_TYPE_KEY, getMessageType());
		if (getSourceOfficeCode() != null)
			message.setStringProperty(SOURCE_OFFICE_CODE_KEY, getSourceOfficeCode());
		if (isTestMessage())
			message.setBooleanProperty(TEST_MESSAGE_KEY, isTestMessage());
		if (isCompressed())
			message.setBooleanProperty(COMPRESSED_KEY, isCompressed());
	}	

}
