set lines 300
set serveroutput on
set verify off
spool 162_STNP_main.lst
col nome new_value  nomefile
select 'Schema_Connesso_'||user nome from dual;
WHENEVER OSERROR EXIT ROLLBACK; 
WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK;

set echo off

-- main a
@162_STNP_InsertToken.sql

spool off
