package com.selexelsag.xte.bpm.flows.testflow;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.drools.KnowledgeBase;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.junit.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.selexelsag.xte.bpm.flows.servicenodes.handlers.SintatticValidationNode;
import com.selexelsag.xte.model.flows.FlowParams;

public class SintatticValidationHandlerTest {

	@Test
	public void test() {
		
		KnowledgeBase kbase = null;
		ApplicationContext appContext;
		try {

//			appContext = new ClassPathXmlApplicationContext(
//					new String[]{"applicationContextTest.xml",
//							"trackConfTest.xml"
//					});
			
			kbase = readKnowledgeBase();
			
			FlowParams flowParams = new FlowParams();
			flowParams.setRequestValidationSchema("pr\\acq\\1.0.xsd");
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("flowParams", flowParams);
			
			StringBuilder data = null;
//					new StringBuilder(FileUtils.readFileToString(new File("D:\\workspaceWorking\\TrasformationXML\\src\\com\\elsagdatamat\\tt\\" +
//																		"tracksystem\\posteitaliane\\postaregistrata\\terminaleportalettere\\" +
//																		"xsd\\tp_acq_f1.xml")));
			params.put("data", data);
			
			StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
			ksession.getWorkItemManager().registerWorkItemHandler("SintatticValidationNode", new SintatticValidationNode());
			ksession.startProcess("com.selexelsag.xte.bpm.validazione", params);
			
			
		}catch (BeansException be) {

			System.err.println("eccezione " + be);
			be.printStackTrace();
			throw new ApplicationContextException("Error loading context", be);
			
		}catch(Throwable e) {
			e.printStackTrace();
			return;
		}
		
	
		
		fail("Not yet implemented");
	}
	
	
	private static KnowledgeBase readKnowledgeBase() throws Exception {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		kbuilder.add(ResourceFactory.newClassPathResource("validazione.bpmn"), ResourceType.BPMN2);
		return kbuilder.newKnowledgeBase();
	}

}
