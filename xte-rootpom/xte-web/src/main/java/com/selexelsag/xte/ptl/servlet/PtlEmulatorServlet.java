package com.selexelsag.xte.ptl.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.model.track.TrackResponse;
import com.selexelsag.xte.service.context.SpringApplicationContext;
import com.selexelsag.xte.xte_presentation_logic.ITrackService;

public class PtlEmulatorServlet extends HttpServlet{

	private static final long serialVersionUID = 2843726402900387511L;
	private static Log _log = LogFactory.getLog(PtlEmulatorServlet.class);
	
	private ITrackService trackService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{
		try
		{
			HashMap<String, Object> mapParams = getParameters(req);

			if (mapParams.containsKey("XML"))
			{
				String sXML = (String)mapParams.get("XML");

				if (sXML == null || sXML.length() == 0)
				{
					_log.error("PtlEmulatorServlet - Parametro nullo !!");
	
					TrackResponse ans = new TrackResponse();
					ans.setErrorCode(ITrackService.RESULT_ERROR_INTERNAL_KEY);
					Post(ans, 500, req, resp);		
					return;
				}
				
				_log.debug("PtlEmulatorServlet - Prima del getBean di ITrackService");
				trackService = (ITrackService)SpringApplicationContext.getBean(ITrackService.TRACK_SRV_BEAN_NAME);
				_log.debug("PtlEmulatorServlet - Dopo la getBean di ITrackService");
				
				TrackRequestArgsClass tra = getTrackRequestArgs(sXML);
				if (tra==null)
				{
					_log.error("PtlEmulatorServlet - TrackRequestArgs nulla !");
					
					TrackResponse ans = new TrackResponse();
					ans.setErrorCode(ITrackService.RESULT_ERROR_INTERNAL_KEY);
					Post(ans, 500, req, resp);
					return;
				}
				
				TrackRequest tr = getTrackRequest(tra.getService(), tra.getChannel(), tra.getChannelId(), tra.getEventName(), tra.getVersion(), tra.getTrackData());
				TrackResponse trackResponse = trackService.processRequest(tr);
				Post(trackResponse, 200, req, resp);
				return;
			}

			_log.error("PtlEmulatorServlet - Parametro non trovato !");

			TrackResponse ans = new TrackResponse();
			ans.setErrorCode(ITrackService.RESULT_ERROR_INTERNAL_KEY);
			Post(ans, 500, req, resp);
			return;
		}
		catch (Throwable t)
		{
			_log.error("PtlEmulatorServlet - Eccezione !! " + t.getMessage());
			
			TrackResponse ans = new TrackResponse();
			ans.setErrorCode(ITrackService.RESULT_ERROR_INTERNAL_KEY);
			Post(ans, 500, req, resp);
		}
	}
	
	private TrackRequestArgsClass getTrackRequestArgs(String sXML)
	{
		TrackRequestArgsClass tra = null; 
		
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		try 
		{
			SAXParser saxParser = saxParserFactory.newSAXParser();
		    MyHandler handler = new MyHandler();
		    saxParser.parse(new InputSource(new StringReader(sXML)), handler);
		    tra = handler.getTr();	
		    if (tra!=null)
		    {
		    	tra.setTrackData(sXML);
		    }
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return tra;
	}

	public class MyHandler extends DefaultHandler 
	{
		private TrackRequestArgsClass tr = null;
		
		public TrackRequestArgsClass getTr() 
		{
			return tr;
		}

		@Override
	    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException 
		{
            if (qName.equalsIgnoreCase("Traccia")) 
            {
                String evento = attributes.getValue("Evento");
                String channel = attributes.getValue("channel");
                String channelID = attributes.getValue("channelID");
                String eventName = attributes.getValue("eventName");
                String serviceName = attributes.getValue("serviceName");
                String version = attributes.getValue("version");
                
                tr = new TrackRequestArgsClass();
                tr.setEventName(evento);
                tr.setChannel(channel);
                tr.setChannelId(channelID);
                tr.setEventName(eventName);
                tr.setService(serviceName);
                tr.setVersion(version);
            }
        }
	}
	
	private TrackRequest getTrackRequest(String service,String channel,String channelId,String eventName,String version,String trackData){
		TrackRequest ans = new TrackRequest();
		ans.setService(new StringBuilder(service));
		ans.setChannel(new StringBuilder(channel));
		ans.setDestinationOfficeId(new StringBuilder(channelId));
		if (channelId != null && !channelId.isEmpty()) {
			ans.setSourceOfficeId(new StringBuilder(channelId));
		} else {
			ans.setSourceOfficeId(new StringBuilder("EMPTL"));
		}
		ans.setEventName(new StringBuilder(eventName));
		ans.setVersion(new StringBuilder(version));
		ans.setTrackData(new StringBuilder(trackData));
		return ans;
	}
	
	private void Post(TrackResponse trackResponse, int httpStatus, HttpServletRequest req, HttpServletResponse resp)
	{
		Response response = new Response();
//		response.setEsito(trackResponse.getErrorCode());
//		response.setDescrizioneEsito(trackResponse.getErrorDesc());
//		response.setCodiceEsito(trackResponse.getErrorId());
//		response.setReiterabilita(trackResponse.getRetryFlag());
//		response.setCodiceIdentificativo(trackResponse.getObjectId());
		response.setRisultato(trackResponse.getOutputParam());
	
		resp.setStatus(httpStatus);
		resp.setContentType("text/html");
		resp.setCharacterEncoding("ISO-8859-1");
		PrintWriter printWriter;
		try 
		{
			printWriter = resp.getWriter();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
			return;
		}
		printWriter.println(response.toString());
		printWriter.close();
		return ;
	} 
	
	
	@Override
	protected void doOptions(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException 
	{
		/*
		// 1. If the Origin header is not present terminate this set of steps. 
		// The request is outside the scope of this specification.
		String origin = request.getHeader("Origin");
		if(origin==null || origin.length() == 0){
			
			return;
		}

		response.setHeader("Access-Control-Allow-Origin", origin);
		response.setHeader("Access-Control-Allow-Credentials", "true");
		
		//2. If the value of the Origin header is not a case-sensitive match for any of the values 
		//   in list of origins do not set any additional headers and terminate this set of steps.
		   
		//   Note : Always matching is acceptable since the list of origins can be unbounded.
		//	 Note : The Origin header can only contain a single origin as the user agent will not follow redirects. 
		  
		//3. Let method be the value as result of parsing the Access-Control-Request-Method header.
		//   If there is no Access-Control-Request-Method header or if parsing failed, 
		//   do not set any additional headers and terminate this set of steps. 
		//   The request is outside the scope of this specification.
		String methods = request.getHeader("Access-Control-Request-Method");
		if(methods!=null && methods.length()>0){
			response.setHeader("Access-Control-Allow-Methods", "GET, POST");
		}
		
		//4. Let header field-names be the values as result of parsing the Access-Control-Request-Headers headers.
		//   If there are no Access-Control-Request-Headers headers let header field-names be the empty list.
		//   If parsing failed do not set any additional headers and terminate this set of steps. 
		//   The request is outside the scope of this specification.
		String headers = request.getHeader("Access-Control-Request-Headers");
		if(headers != null && headers.length()>0){
			response.setHeader("Access-Control-Allow-Headers", headers);
		}
		
		PrintWriter printWriter;
		try {
			printWriter = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		printWriter.println("CROSS DOMAIN");
		printWriter.close();
		return ;
		
		 * */
		super.doOptions(arg0, arg1);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{
		doGet(req, resp);
	}
	
	private HashMap<String, Object> getParameters(HttpServletRequest request)
	{
		HashMap<String, Object> _map = new HashMap<String, Object>();  
		
		@SuppressWarnings("unchecked")
		Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) 
        {
        	String paramName = parameterNames.nextElement();
        	String[] paramValues = request.getParameterValues(paramName);
        	_map.put(paramName, paramValues[0]);
        }
		return _map;
	}

	public class TrackRequestArgsClass
	{
		private String service;
		public String getService() {return service;}
		public void setService(String service) {this.service = service;}

		private String channel;
		public String getChannel() {return channel;}
		public void setChannel(String channel) {this.channel = channel;}

		private String channelId;
		public String getChannelId() {return channelId;}
		public void setChannelId(String channelId) {this.channelId = channelId;}

		private String eventName;
		public String getEventName() {return eventName;}
		public void setEventName(String eventName) {this.eventName = eventName;}

		private String version;
		public String getVersion() {return version;}
		public void setVersion(String version) {this.version = version;}

		private String trackData;
		public String getTrackData() {return trackData;}
		public void setTrackData(String trackData) {this.trackData = trackData;}
		
		public TrackRequestArgsClass(){}
	}

	public class Response
	{
		private String Esito;
		public String getEsito() {return Esito;}
		public void setEsito(String esito) {Esito = esito;}

		private String DescrizioneEsito;
		public String getDescrizioneEsito() {return DescrizioneEsito;}
		public void setDescrizioneEsito(String descrizioneEsito) {DescrizioneEsito = descrizioneEsito;}

		private String CodiceEsito;
		public String getCodiceEsito() {return CodiceEsito;}
		public void setCodiceEsito(String codiceEsito) {CodiceEsito = codiceEsito;}

		private String Reiterabilita;
		public String getReiterabilita() {return Reiterabilita;}
		public void setReiterabilita(String reiterabilita) {Reiterabilita = reiterabilita;}

		private String CodiceIdentificativo;
		public String getCodiceIdentificativo() {return CodiceIdentificativo;}
		public void setCodiceIdentificativo(String codiceIdentificativo) {CodiceIdentificativo = codiceIdentificativo;}

		private String risultato;
		
		public String getRisultato() {
			return risultato;
		}
		public void setRisultato(String risultato) {
			this.risultato = risultato;
		}
		
		public Response(){}
		
		public Response(String esito, String descrizioneEsito, String codiceEsito, String reiterabilita, String codiceIdentificativo) 
		{
			super();
			Esito = esito;
			DescrizioneEsito = descrizioneEsito;
			CodiceEsito = codiceEsito;
			Reiterabilita = reiterabilita;
			CodiceIdentificativo = codiceIdentificativo;
		}

		@Override
		public String toString() {
//			String respXML = "<Risultato Esito=\'" + Esito + "\' DescrizioneEsito=\'" + DescrizioneEsito + "\' CodiceEsito=\'" + CodiceEsito + "\'";
//			respXML += " Reiterabilita=\'" + Reiterabilita +"\'><CodiceIdentificativo>" + CodiceIdentificativo + "</CodiceIdentificativo></Risultato>";
//			return respXML;
			return risultato;
		}
	}	
}
