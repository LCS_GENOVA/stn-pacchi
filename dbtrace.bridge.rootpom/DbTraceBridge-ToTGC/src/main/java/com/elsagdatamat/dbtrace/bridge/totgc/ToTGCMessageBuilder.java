package com.elsagdatamat.dbtrace.bridge.totgc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.elsagdatamat.dbtrace.bridge.engine.MessageBuilderBase;
import com.elsagdatamat.dbtrace.bridge.totgc.manager.ITraceManager;
import com.elsagdatamat.tracktrace.core.infrastructure.trackmessages.converter.Tgc23iMessage;
import com.leonardo.tt.tgc.Codici;
import com.leonardo.tt.tgc.ListaInvii;
import com.leonardo.tt.tgc.ObjectFactory;
import com.leonardo.tt.tgc.TTTGC;
import com.selexelsag.tracktrace.extension.managers.MessageToBeDeliveredManager;
import com.selexelsag.tracktrace.extension.objectmodel.MessageDestinationType;
import com.selexelsag.tracktrace.extension.objectmodel.MessageToBeDelivered;
import com.selexelsag.tracktrace.extension.objectmodel.MessageToBeDelivered.Status;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;


public class ToTGCMessageBuilder extends MessageBuilderBase implements IToTGCMessageBuilder {
	
	protected final Log log = LogFactory.getLog(getClass());

	private static final String TOKEN_SPLITTER = ";";
    private static final String NO_FRAZIONARIO = "NO_FRAZIONARIO";
    
	protected Long maxRecordNumber = 100L;
	public void setMaxRecordNumber(Long maxRecordNumber) {
		this.maxRecordNumber = maxRecordNumber;
	}
	
	protected Integer guardDays = 1;
	public void setGuardDays(Integer guardDays) {
		this.guardDays = guardDays;
	}
	
	private ITraceManager traceManager;
	public void setTraceManager(ITraceManager traceManager) {
		this.traceManager = traceManager;
	}

	private MessageToBeDeliveredManager messageToBeDeliveredManager;
	public void setMessageToBeDeliveredManager(MessageToBeDeliveredManager messageToBeDeliveredManager) {
		this.messageToBeDeliveredManager = messageToBeDeliveredManager;
	}
	
    private String messageDestination;
	public void setMessageDestination(String messageDestination) {
		this.messageDestination = messageDestination;
	}

	// IMessageBuilder
	@Override
	public List<String> create(TracesList input) throws JAXBException {
		
		List<String> ret = new  ArrayList<String>();
	
		ObjectFactory of = new ObjectFactory();
		ListaInvii li = of.createListaInvii();
		
		for (Trace t:input.getTraces())	{
			Codici code = new Codici();
			code.setCanale(t.getChannel());
			code.setCodiceOggetto(t.getIdTracedEntity());
			code.setDataTraccia(t.getWhenHappened());
			code.setFrazionarioUfficio(t.getIdChannel());
			code.setProdotto(t.getLabelTracedEntity());
			
			li.getCodici().add(code);
		}

		TTTGC tttgc = of.createTTTGC();		
		tttgc.setListaInvii(li);

		String s = Tgc23iMessage.createTgcXmlMessageAsStringWithoutSerialization(tttgc);
		ret.add(s);
		
		log.debug(String.format("tgc xml message created : '%s'", s));
		
		return ret;
	}

	// IMessageSender
	@Override
	public void send(final List<String> strList) {
		
		log.info(String.format("Entering send(), body message '%s'", strList));
		
		for (String str : strList) {
			
	        MessageToBeDelivered msg = new MessageToBeDelivered();
	        msg.setBody(str);
	        msg.setCompressed(CompressionType.NOT_COMPRESSED.getCode());
	        msg.setCreationDate(new Date());
	        msg.setDestination(messageDestination);
	        msg.setDestinationType(MessageDestinationType.INT.getCode());
	        msg.setFrazionario(NO_FRAZIONARIO);
	        msg.setMessageType("TGC_PA23I"); 
	        msg.setStatus(Status.T);                
	        messageToBeDeliveredManager.insertMessage(msg);
		}
		
//		log.debug("Entering send");
//		for (final String str : strList) {
//			jmsTemplate.send(new MessageCreator() {
//				public Message createMessage(Session session) throws JMSException {
//					TextMessage tm = session.createTextMessage();
//					tm.setText(str);
//					return tm;
//				}
//			});
//		}
//		log.debug("Leaving send");
	}

	// ITracesReader
	@Override
	public TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate) {
		
		log.info(String.format("Entering getTraces() for service '%s' (wsToken='%s'): stopDate=%s;", 
					token.getServiceId(),
					token.getWsToken(), 
					stopDate));

		String[] split = token.getWsToken().split(TOKEN_SPLITTER);
		Long lastTokenId = Long.valueOf(split[1]);
		Date lastTokenDate = new Date(Long.valueOf(split[0]));
		log.debug(String.format("Splitted token : id = '%s' - date = '%s'", lastTokenId.toString(), lastTokenDate.toString()));

		Calendar c = Calendar.getInstance();
		c.setTime(lastTokenDate);
//		c.add(Calendar.DATE, 1);
		c.add(Calendar.DATE, guardDays);
		Date guardDate = c.getTime();
		if (guardDate.before(stopDate)) {
			stopDate = guardDate;
		}
				
		List<Trace> traces = null;
		List<com.elsagdatamat.tt.trackdbws.beans.Trace> newTraces = null;
		try {
			newTraces = traceManager.newLoadNextTraces(lastTokenDate, lastTokenId, stopDate, 10);
			
			String nuovoToken;
			if (newTraces != null && newTraces.size() > 0) {
				
				log.debug(String.format("found '%s' traces", newTraces.size()));
				
				// aggiornamento del token
				com.elsagdatamat.tt.trackdbws.beans.Trace lastTrace = newTraces.get(newTraces.size() - 1);
	
				nuovoToken = String.valueOf(lastTrace.getWhenRegistered().getTime());
				nuovoToken += ";";
				nuovoToken += String.valueOf(lastTrace.getIdTrace());
	
				// interessano solo le tracce nate sul T&T e/o su OMP
				traces = traceManager.filterByAdditionalServices(newTraces);
				log.debug(String.format("filtred traces, new size '%s'", traces.size()));
			} else {
				
				log.debug("no traces found");

				nuovoToken = String.valueOf(stopDate.getTime());
				nuovoToken += ";";
				nuovoToken += String.valueOf(0);

				traces = new ArrayList<Trace>();
			}

			token.setWsToken(nuovoToken);
			log.debug(String.format("updated token : '%s' ", nuovoToken));
			
			
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
						
		TracesList ret = null;
		ret = ConvertUtil.tracesToTraceList(traces);

		return ret;
	}
}
