package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.util.List;

import com.elsagdatamat.dbtrace.bridge.engine.entities.ConsumerProduct;
import com.elsagdatamat.dbtrace.bridge.engine.entities.ConsumerProductPK;
import com.elsagdatamat.framework.utils.PairLinkedList;

public class ConsumerProductManager extends BaseManager<ConsumerProduct, ConsumerProductPK> implements IConsumerProductManager {
	@Override
	public List<ConsumerProduct> listInvolvedProducts(String service)
	{
		PairLinkedList<String, Object> properties = new PairLinkedList<String, Object>();
		properties.add(ConsumerProduct.SERVICE_NAME, service);
		return findByProperties(properties);
	}
}
