<?xml version="1.0" encoding="UTF-8"?>
<Risultato Reiterabilita="N" CodiceEsito="AM000" DescrizioneEsito="OK" Esito="Positivo">
    <CodiceIdentificativo>259990002001296</CodiceIdentificativo>
    <FrazionarioUfficio>25999</FrazionarioUfficio>
    <FrazionarioUfficioProvenienza>25999</FrazionarioUfficioProvenienza>
    <CodicePortalettere>2</CodicePortalettere>
    <ProgressivoGiornaliero>1</ProgressivoGiornaliero>
    <DataFormazione>2012-04-05T15:04:06Z</DataFormazione>
    <ListaInvii>
        <DatiInvio>
            <Codice>100000000045</Codice>
            <Prodotto>R</Prodotto>
            <ProdottoNotifica>R</ProdottoNotifica>
            <NomeUfficioAccettazione>FIRENZE CMP WINDOWS</NomeUfficioAccettazione>
            <ValoreAssicurato>0</ValoreAssicurato>
            <ImportoContrassegno>0</ImportoContrassegno>
            <GiorniDiGiacenza>35</GiorniDiGiacenza>
        </DatiInvio>
        <DatiInvio>
            <Codice>100000000034</Codice>
            <Prodotto>R</Prodotto>
            <ProdottoNotifica>R</ProdottoNotifica>
            <NomeUfficioAccettazione>FIRENZE CMP WINDOWS</NomeUfficioAccettazione>
            <ValoreAssicurato>0</ValoreAssicurato>
            <ImportoContrassegno>0</ImportoContrassegno>
            <GiorniDiGiacenza>35</GiorniDiGiacenza>
        </DatiInvio>
    </ListaInvii>
</Risultato>