/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectrenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Da Procida
 */
public class XteOfficeTableCellRenderer extends DefaultTableCellRenderer {

    private List<String> columnWidthValues;
    private ImageIcon tableCursorIcon = new ImageIcon(getClass().getResource("/images/TableCursor1.png"));
    private ImageIcon runningIcon = new ImageIcon(getClass().getResource("/images/Run1.png"));
    private ImageIcon stoppedIcon = new ImageIcon(getClass().getResource("/images/Stop1.png"));

    public XteOfficeTableCellRenderer(List<String> initialColumnWidthValues) {

        this.columnWidthValues = initialColumnWidthValues;
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

        defineTableFocus(table);

        for (int i = 0; i < table.getModel().getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setMinWidth(Integer.parseInt(columnWidthValues.get(i)));
            table.getColumnModel().getColumn(i).setMaxWidth(Integer.parseInt(columnWidthValues.get(i)));
            table.getColumnModel().getColumn(i).setPreferredWidth(Integer.parseInt(columnWidthValues.get(i)));
        }

        JLabel renderer = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);

        if(col == 0)
        {
            renderer = new JLabel();
            if(table.isEnabled())
            {
                renderer.setBackground(new Color(220,220, 255));
                renderer.setForeground(new Color(51,51,51));
            }
            else
            {
                renderer.setBackground(new Color(240,240,240));
                renderer.setForeground(new Color(204,204,204));
            }

            renderer.setOpaque(true);
            renderer.setFont(new Font("Tahoma", Font.BOLD, 14));
            renderer.setText((value == null) ? "" : value.toString());
            renderer.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
            renderer.setHorizontalAlignment(JLabel.LEFT);

            if(isSelected)
            {
                renderer.setIcon(tableCursorIcon);
            }
            else
            {
                renderer.setIcon(null);
            }
            
        }
        else
        {
            if(table.isEnabled())
            {
                renderer = new JLabel();
                renderer.setOpaque(true);
                renderer.setBackground(Color.WHITE);

                renderer.setFont(new Font("Tahoma", Font.BOLD, 12));
                renderer.setHorizontalAlignment(JLabel.CENTER);
                renderer.setForeground(new Color(51,51,51));

                if(col == 4)
                {
                    if(value instanceof Boolean && value!=null)
                    {
                                                
                        if(((Boolean)value).equals(true))
                        {
                            renderer.setIcon(runningIcon);
                        }
                        else
                        {
                            renderer.setIcon(stoppedIcon);
                        }
                        
                        renderer.setText("");
                    }

                }
                else
                {
                    if(value!=null)
                    {
                        renderer.setText((String) value);
                    }
                    else
                    {
                        renderer.setText("");

                    }
                }

                if(isSelected)
                {
                    renderer.setBackground(new Color(153, 204, 255));
                }
                else
                {
                    renderer.setBackground(Color.white);
                }

            }
            else
            {
                renderer.setForeground(new Color(204,204,204));
            }
        }
        
        return renderer;
    }

    private void defineTableFocus(JTable table)
    {
        Action tabOut = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
            }
        };

        Action stabOut = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent();
            }
        };

        table.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabOut");
        table.getActionMap().put("tabOut",tabOut);

        table.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, java.awt.event.InputEvent.SHIFT_DOWN_MASK ), "stabOut");
        table.getActionMap().put("stabOut",stabOut);


    }
	
    
}
