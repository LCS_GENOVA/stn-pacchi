package com.elsagdatamat.dbtrace.bridge.jms;

import java.io.Reader;
import java.io.StringReader;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.elsagdatamat.dbtrace.bridge.engine.IDbTraceBridge;
import com.elsagdatamat.dbtrace.bridge.engine.bl.IDiscardedTraceManager;
import com.elsagdatamat.dbtrace.bridge.engine.exceptions.InsertTracesException;
import com.elsagdatamat.dbtrace.bridge.engine.exceptions.TrackDBRWException;

public class JmsToTraceBridge implements MessageListener {

	private Log log = LogFactory.getLog(this.getClass());

	IDbTraceBridge<Reader> dbTraceBridge;

	public IDbTraceBridge<Reader> getDbTraceBridge() {
		return dbTraceBridge;
	}

	public void setDbTraceBridge(IDbTraceBridge<Reader> dbTraceBridge) {
		this.dbTraceBridge = dbTraceBridge;
	}

	private IDiscardedTraceManager discardedTraceManager;

	public void setDiscardedTraceManager(IDiscardedTraceManager discardedTraceManager) {
		this.discardedTraceManager = discardedTraceManager;
	}

	public IDiscardedTraceManager getDiscardedTraceManager() {
		return discardedTraceManager;
	}

	private String callingProcess;

	public void setCallingProcess(String callingProcess) {
		this.callingProcess = callingProcess;
	}

	public String getCallingProcess() {
		return callingProcess;
	}

	private boolean saveTraces = false;

	public boolean isSaveTraces() {
		return saveTraces;
	}

	public void setSaveTraces(boolean saveTraces) {
		this.saveTraces = saveTraces;
	}

	private String encoding = null;

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	private int chunkSize = 10000;

	public int getChunkSize() {
		return chunkSize;
	}

	public void setChunkSize(int chunkSize) {
		this.chunkSize = chunkSize;
	}

	public void init() {
		log.info("Initializing JmsToWsBridge...");

		if (dbTraceBridge == null)
			throw new NullPointerException("dbTraceBridge not set");

		log.info("JmsToWsBridge initialized");
	}

	@Override
	public void onMessage(Message message) {
		String strMessage = null;
		try {
			log.info("Message received: " + message.getJMSCorrelationID());

			strMessage = ((TextMessage) message).getText();
			StringReader reader = new StringReader(strMessage);
			dbTraceBridge.insertTraces(reader);

			if (isSaveTraces()) {
				discardedTraceManager.saveDiscardedTraces(callingProcess, strMessage, null);
			}

		} catch (TrackDBRWException e) {
			log.error("Error in onMessage() - TrackDBRWException", e);
			throw e;
		} catch (InsertTracesException e) {
			log.error("Error in onMessage()", e);
			discardedTraceManager.saveDiscardedTraces(callingProcess, strMessage, e);
		} catch (Exception e) {
			log.error("Error in onMessage()", e);
			discardedTraceManager.saveDiscardedTraces(callingProcess, strMessage, e);
		}
	}

}
