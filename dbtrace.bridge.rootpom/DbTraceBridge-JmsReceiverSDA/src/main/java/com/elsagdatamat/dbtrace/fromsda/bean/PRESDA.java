package com.elsagdatamat.dbtrace.fromsda.bean;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
"ID_LDV",
"ID_SERVIZIO",
"ID_PRODOTTO",
"SERVIZI",
"IMPORTO_CONTRASS",
"IMPORTO_ASS",
"DATA_EMISSIONE",
"NUM_ORDINE",
"MITT_INTESTATARIO",
"MITT_REFERENTE",
"MITT_INDIRIZZO",
"MITT_CAP",
"MITT_CITTA",
"MITT_PROVINCIA",
"MITT_NAZIONE",
"MITT_TELEFONO",
"MITT_FAX",
"MITT_CELLULARE",
"MITT_EMAIL",
"MITT_INFO1",
"MITT_INFO2",
"DEST_INTESTATARIO",
"DEST_REFERENTE",
"DEST_INDIRIZZO",
"DEST_CAP",
"DEST_CITTA",
"DEST_PROVINCIA",
"DEST_NAZIONE",
"DEST_TELEFONO",
"DEST_FAX",
"DEST_CELLULARE",
"DEST_EMAIL",
"DEST_INFO1",
"DEST_INFO2",
"NUMERO_COLLI",
"PESO_COLLI",
"DIM_H",
"DIM_L",
"DIM_W",
"CONTENUTO",
"PAG_ONERI",
"VALORE_DICH",
"COD_TIPO_PAGAMENTO",
"NOTE",
"COD_RIF_COLLO",
"BARCODE_SERVIZIO",
"DATA_INS",
"IS_ASS_PERCENTUALE",
"MANCATA_CONSEGNA",
"COD_UFF_FRAZIONARIO",
"DETTAGLIO_BS",
"CASELLAPOSTALE_CLIENTE",
"CC_POSTE_CLIENTE",
"RAGIONE_SOCIALE_CLIENTE",
"CODICE_CLIENTE_SAP",
"CODICE_CONTRATTO_SDA",
"CODICE_CONTRATTO_POSTE",
"PIVA",
"CODICE_FISCALE"
})
public class PRESDA {

/**
* numero ldv
* (Required)
* 
*/
@JsonProperty("ID_LDV")
public String iDLDV;
/**
* Id servizio SDA
* (Required)
* 
*/
@JsonProperty("ID_SERVIZIO")
public String iDSERVIZIO;
/**
* codice prodotto Poste
* (Required)
* 
*/
@JsonProperty("ID_PRODOTTO")
public String iDPRODOTTO;
/**
* servizi accessori
* 
*/
@JsonProperty("SERVIZI")
public String sERVIZI;
/**
* importo in centesimi di euro del contrassegno
* 
*/
@JsonProperty("IMPORTO_CONTRASS")
public Double iMPORTOCONTRASS;
/**
* importo in centesimi di euro della assicurata
* 
*/
@JsonProperty("IMPORTO_ASS")
public Double iMPORTOASS;
/**
* data stampa ldv
* (Required)
* 
*/
@JsonProperty("DATA_EMISSIONE")
public Date dATAEMISSIONE;
/**
* Riferimento della spedizione per il cliente (alias Rif.Sped.)
* 
*/
@JsonProperty("NUM_ORDINE")
public String nUMORDINE;
/**
* Ragione sociale del mittente
* (Required)
* 
*/
@JsonProperty("MITT_INTESTATARIO")
public String mITTINTESTATARIO;
/**
* Referenti del mittente
* 
*/
@JsonProperty("MITT_REFERENTE")
public String mITTREFERENTE;
/**
* Indirizzo del mittente
* (Required)
* 
*/
@JsonProperty("MITT_INDIRIZZO")
public String mITTINDIRIZZO;
/**
* Cap del mittente
* (Required)
* 
*/
@JsonProperty("MITT_CAP")
public String mITTCAP;
/**
* Citta del mittente
* (Required)
* 
*/
@JsonProperty("MITT_CITTA")
public String mITTCITTA;
/**
* Provincia del mittente
* (Required)
* 
*/
@JsonProperty("MITT_PROVINCIA")
public String mITTPROVINCIA;
/**
* Nazione del mittente
* (Required)
* 
*/
@JsonProperty("MITT_NAZIONE")
public String mITTNAZIONE;
/**
* Telefono del mittente
* 
*/
@JsonProperty("MITT_TELEFONO")
public String mITTTELEFONO;
/**
* Fax del mittente
* 
*/
@JsonProperty("MITT_FAX")
public String mITTFAX;
/**
* Cellulare del mittente
* 
*/
@JsonProperty("MITT_CELLULARE")
public String mITTCELLULARE;
/**
* E-mail del mittente
* 
*/
@JsonProperty("MITT_EMAIL")
public String mITTEMAIL;
/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("MITT_INFO1")
public String mITTINFO1;
/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("MITT_INFO2")
public String mITTINFO2;
/**
* Ragione sociale del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_INTESTATARIO")
public String dESTINTESTATARIO;
/**
* Referenti del destinatario
* 
*/
@JsonProperty("DEST_REFERENTE")
public String dESTREFERENTE;
/**
* Indirizzo del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_INDIRIZZO")
public String dESTINDIRIZZO;
/**
* Cap del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_CAP")
public String dESTCAP;
/**
* Citta del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_CITTA")
public String dESTCITTA;
/**
* Provincia del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_PROVINCIA")
public String dESTPROVINCIA;
/**
* Nazione del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_NAZIONE")
public String dESTNAZIONE;
/**
* Telefono del destinatario
* 
*/
@JsonProperty("DEST_TELEFONO")
public String dESTTELEFONO;
/**
* Fax del destinatario
* 
*/
@JsonProperty("DEST_FAX")
public String dESTFAX;
/**
* Cellulare del destinatario
* 
*/
@JsonProperty("DEST_CELLULARE")
public String dESTCELLULARE;
/**
* E-mail del destinatario
* 
*/
@JsonProperty("DEST_EMAIL")
public String dESTEMAIL;
/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("DEST_INFO1")
public String dESTINFO1;
/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("DEST_INFO2")
public String dESTINFO2;
/**
* Numero colli della spedizione
* 
*/
@JsonProperty("NUMERO_COLLI")
public String nUMEROCOLLI;
/**
* Peso espresso in grammi del singolo collo componente la spedizione
* (Required)
* 
*/
@JsonProperty("PESO_COLLI")
public Double pESOCOLLI;
/**
* Altezza espressa in centimetri del singolo collo
* (Required)
* 
*/
@JsonProperty("DIM_H")
public Double dIMH;
/**
* Larghezza espressa in centimetri del singolo collo
* (Required)
* 
*/
@JsonProperty("DIM_L")
public Double dIML;
/**
* Profondità espressa in centimetri del singolo collo
* (Required)
* 
*/
@JsonProperty("DIM_W")
public Double dIMW;
/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("CONTENUTO")
public String cONTENUTO;
/**
* Indica il se il pagamento degli oneri doganali e’ a cura del M:Mittente o D: Destinatario
* 
*/
@JsonProperty("PAG_ONERI")
public String pAGONERI;
/**
* Valore spedizione, in centesimi di euro
* 
*/
@JsonProperty("VALORE_DICH")
public Double vALOREDICH;
/**
* Indica il tipo di pagamento previsto per il contrassegno Poste
* 
*/
@JsonProperty("COD_TIPO_PAGAMENTO")
public String cODTIPOPAGAMENTO;
/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("NOTE")
public String nOTE;
/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("COD_RIF_COLLO")
public String cODRIFCOLLO;
/**
* Codice accessori di consegna (vedi foglio seguente)
* 
*/
@JsonProperty("BARCODE_SERVIZIO")
public String bARCODESERVIZIO;
/**
* Data inserimento ldv sul DB SDA
* 
*/
@JsonProperty("DATA_INS")
public Date dATAINS;
/**
* S: spedizione assicurata in %, N: spedizione non assicurata in %
* 
*/
@JsonProperty("IS_ASS_PERCENTUALE")
public String iSASSPERCENTUALE;
/**
* Indicazioni in caso di mancata consegna al destinatario: R (Restituire) o A (Abbandonare)
* 
*/
@JsonProperty("MANCATA_CONSEGNA")
public String mANCATACONSEGNA;
/**
* Codice frazionario dell'UP. Utilizzato sia per FMP, CPT, APT, RTZ che in caso di inesito per l'home delivery
* 
*/
@JsonProperty("COD_UFF_FRAZIONARIO")
public String cODUFFFRAZIONARIO;
/**
* Dettaglio del barcode_servizio per l'accessorio di consegna A Giorno Definito
* 
*/
@JsonProperty("DETTAGLIO_BS")
public String dETTAGLIOBS;
/**
* Numero casella postale
* 
*/
@JsonProperty("CASELLAPOSTALE_CLIENTE")
public String cASELLAPOSTALECLIENTE;
/**
* The Cc_poste_cliente Schema
* <p>
* Numero conto Banco Posta per rimessa contrassegno
* 
*/
@JsonProperty("CC_POSTE_CLIENTE")
public String cCPOSTECLIENTE;
/**
* Ragione sociale cliente
* 
*/
@JsonProperty("RAGIONE_SOCIALE_CLIENTE")
public String rAGIONESOCIALECLIENTE;
/**
* identificavo del conto in sap
* 
*/
@JsonProperty("CODICE_CLIENTE_SAP")
public String cODICECLIENTESAP;
@JsonProperty("CODICE_CONTRATTO_SDA")
public String cODICECONTRATTOSDA;
/**
* contratto CRM
* 
*/
@JsonProperty("CODICE_CONTRATTO_POSTE")
public String cODICECONTRATTOPOSTE;
/**
* partita iva cliente contratto
* 
*/
@JsonProperty("PIVA")
public String pIVA;
/**
* codice fiscale cliente contratto
* 
*/
@JsonProperty("CODICE_FISCALE")
public String cODICEFISCALE;

/**
* numero ldv
* (Required)
* 
*/
@JsonProperty("ID_LDV")
public String getIDLDV() {
return iDLDV;
 }

/**
* numero ldv
* (Required)
* 
*/
@JsonProperty("ID_LDV")
public void setIDLDV(String iDLDV) {
this.iDLDV = iDLDV;
 }

/**
* Id servizio SDA
* (Required)
* 
*/
@JsonProperty("ID_SERVIZIO")
public String getIDSERVIZIO() {
return iDSERVIZIO;
 }

/**
* Id servizio SDA
* (Required)
* 
*/
@JsonProperty("ID_SERVIZIO")
public void setIDSERVIZIO(String iDSERVIZIO) {
this.iDSERVIZIO = iDSERVIZIO;
 }

/**
* codice prodotto Poste
* (Required)
* 
*/
@JsonProperty("ID_PRODOTTO")
public String getIDPRODOTTO() {
return iDPRODOTTO;
 }

/**
* codice prodotto Poste
* (Required)
* 
*/
@JsonProperty("ID_PRODOTTO")
public void setIDPRODOTTO(String iDPRODOTTO) {
this.iDPRODOTTO = iDPRODOTTO;
 }

/**
* servizi accessori
* 
*/
@JsonProperty("SERVIZI")
public String getSERVIZI() {
return sERVIZI;
 }

/**
* servizi accessori
* 
*/
@JsonProperty("SERVIZI")
public void setSERVIZI(String sERVIZI) {
this.sERVIZI = sERVIZI;
 }

/**
* importo in centesimi di euro del contrassegno
* 
*/
@JsonProperty("IMPORTO_CONTRASS")
public Double getIMPORTOCONTRASS() {
return iMPORTOCONTRASS;
 }

/**
* importo in centesimi di euro del contrassegno
* 
*/
@JsonProperty("IMPORTO_CONTRASS")
public void setIMPORTOCONTRASS(Double iMPORTOCONTRASS) {
this.iMPORTOCONTRASS = iMPORTOCONTRASS;
 }

/**
* importo in centesimi di euro della assicurata
* 
*/
@JsonProperty("IMPORTO_ASS")
public Double getIMPORTOASS() {
return iMPORTOASS;
 }

/**
* importo in centesimi di euro della assicurata
* 
*/
@JsonProperty("IMPORTO_ASS")
public void setIMPORTOASS(Double iMPORTOASS) {
this.iMPORTOASS = iMPORTOASS;
 }

/**
* data stampa ldv
* 
* 
*/
@JsonProperty("DATA_EMISSIONE")
@JsonSerialize(using = CustomJsonDateSerializer.class)
public Date getDATAEMISSIONE() {
return dATAEMISSIONE;
 }

/**
* data stampa ldv
* 
*/
@JsonProperty("DATA_EMISSIONE")
@JsonDeserialize(using = CustomJsonDateDeserializer .class)
public void setDATAEMISSIONE(Date dATAEMISSIONE) {
this.dATAEMISSIONE = dATAEMISSIONE;
 }

/**
* Riferimento della spedizione per il cliente (alias Rif.Sped.)
* 
*/
@JsonProperty("NUM_ORDINE")
public String getNUMORDINE() {
return nUMORDINE;
 }

/**
* Riferimento della spedizione per il cliente (alias Rif.Sped.)
* 
*/
@JsonProperty("NUM_ORDINE")
public void setNUMORDINE(String nUMORDINE) {
this.nUMORDINE = nUMORDINE;
 }

/**
* Ragione sociale del mittente
* (Required)
* 
*/
@JsonProperty("MITT_INTESTATARIO")
public String getMITTINTESTATARIO() {
return mITTINTESTATARIO;
 }

/**
* Ragione sociale del mittente
* (Required)
* 
*/
@JsonProperty("MITT_INTESTATARIO")
public void setMITTINTESTATARIO(String mITTINTESTATARIO) {
this.mITTINTESTATARIO = mITTINTESTATARIO;
 }

/**
* Referenti del mittente
* 
*/
@JsonProperty("MITT_REFERENTE")
public String getMITTREFERENTE() {
return mITTREFERENTE;
 }

/**
* Referenti del mittente
* 
*/
@JsonProperty("MITT_REFERENTE")
public void setMITTREFERENTE(String mITTREFERENTE) {
this.mITTREFERENTE = mITTREFERENTE;
 }

/**
* Indirizzo del mittente
* (Required)
* 
*/
@JsonProperty("MITT_INDIRIZZO")
public String getMITTINDIRIZZO() {
return mITTINDIRIZZO;
 }

/**
* Indirizzo del mittente
* (Required)
* 
*/
@JsonProperty("MITT_INDIRIZZO")
public void setMITTINDIRIZZO(String mITTINDIRIZZO) {
this.mITTINDIRIZZO = mITTINDIRIZZO;
 }

/**
* Cap del mittente
* (Required)
* 
*/
@JsonProperty("MITT_CAP")
public String getMITTCAP() {
return mITTCAP;
 }

/**
* Cap del mittente
* (Required)
* 
*/
@JsonProperty("MITT_CAP")
public void setMITTCAP(String mITTCAP) {
this.mITTCAP = mITTCAP;
 }

/**
* Citta del mittente
* (Required)
* 
*/
@JsonProperty("MITT_CITTA")
public String getMITTCITTA() {
return mITTCITTA;
 }

/**
* Citta del mittente
* (Required)
* 
*/
@JsonProperty("MITT_CITTA")
public void setMITTCITTA(String mITTCITTA) {
this.mITTCITTA = mITTCITTA;
 }

/**
* Provincia del mittente
* (Required)
* 
*/
@JsonProperty("MITT_PROVINCIA")
public String getMITTPROVINCIA() {
return mITTPROVINCIA;
 }

/**
* Provincia del mittente
* (Required)
* 
*/
@JsonProperty("MITT_PROVINCIA")
public void setMITTPROVINCIA(String mITTPROVINCIA) {
this.mITTPROVINCIA = mITTPROVINCIA;
 }

/**
* Nazione del mittente
* (Required)
* 
*/
@JsonProperty("MITT_NAZIONE")
public String getMITTNAZIONE() {
return mITTNAZIONE;
 }

/**
* Nazione del mittente
* (Required)
* 
*/
@JsonProperty("MITT_NAZIONE")
public void setMITTNAZIONE(String mITTNAZIONE) {
this.mITTNAZIONE = mITTNAZIONE;
 }

/**
* Telefono del mittente
* 
*/
@JsonProperty("MITT_TELEFONO")
public String getMITTTELEFONO() {
return mITTTELEFONO;
 }

/**
* Telefono del mittente
* 
*/
@JsonProperty("MITT_TELEFONO")
public void setMITTTELEFONO(String mITTTELEFONO) {
this.mITTTELEFONO = mITTTELEFONO;
 }

/**
* Fax del mittente
* 
*/
@JsonProperty("MITT_FAX")
public String getMITTFAX() {
return mITTFAX;
 }

/**
* Fax del mittente
* 
*/
@JsonProperty("MITT_FAX")
public void setMITTFAX(String mITTFAX) {
this.mITTFAX = mITTFAX;
 }

/**
* Cellulare del mittente
* 
*/
@JsonProperty("MITT_CELLULARE")
public String getMITTCELLULARE() {
return mITTCELLULARE;
 }

/**
* Cellulare del mittente
* 
*/
@JsonProperty("MITT_CELLULARE")
public void setMITTCELLULARE(String mITTCELLULARE) {
this.mITTCELLULARE = mITTCELLULARE;
 }

/**
* E-mail del mittente
* 
*/
@JsonProperty("MITT_EMAIL")
public String getMITTEMAIL() {
return mITTEMAIL;
 }

/**
* E-mail del mittente
* 
*/
@JsonProperty("MITT_EMAIL")
public void setMITTEMAIL(String mITTEMAIL) {
this.mITTEMAIL = mITTEMAIL;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("MITT_INFO1")
public String getMITTINFO1() {
return mITTINFO1;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("MITT_INFO1")
public void setMITTINFO1(String mITTINFO1) {
this.mITTINFO1 = mITTINFO1;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("MITT_INFO2")
public String getMITTINFO2() {
return mITTINFO2;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("MITT_INFO2")
public void setMITTINFO2(String mITTINFO2) {
this.mITTINFO2 = mITTINFO2;
 }

/**
* Ragione sociale del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_INTESTATARIO")
public String getDESTINTESTATARIO() {
return dESTINTESTATARIO;
 }

/**
* Ragione sociale del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_INTESTATARIO")
public void setDESTINTESTATARIO(String dESTINTESTATARIO) {
this.dESTINTESTATARIO = dESTINTESTATARIO;
 }

/**
* Referenti del destinatario
* 
*/
@JsonProperty("DEST_REFERENTE")
public String getDESTREFERENTE() {
return dESTREFERENTE;
 }

/**
* Referenti del destinatario
* 
*/
@JsonProperty("DEST_REFERENTE")
public void setDESTREFERENTE(String dESTREFERENTE) {
this.dESTREFERENTE = dESTREFERENTE;
 }

/**
* Indirizzo del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_INDIRIZZO")
public String getDESTINDIRIZZO() {
return dESTINDIRIZZO;
 }

/**
* Indirizzo del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_INDIRIZZO")
public void setDESTINDIRIZZO(String dESTINDIRIZZO) {
this.dESTINDIRIZZO = dESTINDIRIZZO;
 }

/**
* Cap del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_CAP")
public String getDESTCAP() {
return dESTCAP;
 }

/**
* Cap del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_CAP")
public void setDESTCAP(String dESTCAP) {
this.dESTCAP = dESTCAP;
 }

/**
* Citta del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_CITTA")
public String getDESTCITTA() {
return dESTCITTA;
 }

/**
* Citta del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_CITTA")
public void setDESTCITTA(String dESTCITTA) {
this.dESTCITTA = dESTCITTA;
 }

/**
* Provincia del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_PROVINCIA")
public String getDESTPROVINCIA() {
return dESTPROVINCIA;
 }

/**
* Provincia del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_PROVINCIA")
public void setDESTPROVINCIA(String dESTPROVINCIA) {
this.dESTPROVINCIA = dESTPROVINCIA;
 }

/**
* Nazione del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_NAZIONE")
public String getDESTNAZIONE() {
return dESTNAZIONE;
 }

/**
* Nazione del destinatario
* (Required)
* 
*/
@JsonProperty("DEST_NAZIONE")
public void setDESTNAZIONE(String dESTNAZIONE) {
this.dESTNAZIONE = dESTNAZIONE;
 }

/**
* Telefono del destinatario
* 
*/
@JsonProperty("DEST_TELEFONO")
public String getDESTTELEFONO() {
return dESTTELEFONO;
 }

/**
* Telefono del destinatario
* 
*/
@JsonProperty("DEST_TELEFONO")
public void setDESTTELEFONO(String dESTTELEFONO) {
this.dESTTELEFONO = dESTTELEFONO;
 }

/**
* Fax del destinatario
* 
*/
@JsonProperty("DEST_FAX")
public String getDESTFAX() {
return dESTFAX;
 }

/**
* Fax del destinatario
* 
*/
@JsonProperty("DEST_FAX")
public void setDESTFAX(String dESTFAX) {
this.dESTFAX = dESTFAX;
 }

/**
* Cellulare del destinatario
* 
*/
@JsonProperty("DEST_CELLULARE")
public String getDESTCELLULARE() {
return dESTCELLULARE;
 }

/**
* Cellulare del destinatario
* 
*/
@JsonProperty("DEST_CELLULARE")
public void setDESTCELLULARE(String dESTCELLULARE) {
this.dESTCELLULARE = dESTCELLULARE;
 }

/**
* E-mail del destinatario
* 
*/
@JsonProperty("DEST_EMAIL")
public String getDESTEMAIL() {
return dESTEMAIL;
 }

/**
* E-mail del destinatario
* 
*/
@JsonProperty("DEST_EMAIL")
public void setDESTEMAIL(String dESTEMAIL) {
this.dESTEMAIL = dESTEMAIL;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("DEST_INFO1")
public String getDESTINFO1() {
return dESTINFO1;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("DEST_INFO1")
public void setDESTINFO1(String dESTINFO1) {
this.dESTINFO1 = dESTINFO1;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("DEST_INFO2")
public String getDESTINFO2() {
return dESTINFO2;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("DEST_INFO2")
public void setDESTINFO2(String dESTINFO2) {
this.dESTINFO2 = dESTINFO2;
 }

/**
* Numero colli della spedizione
* 
*/
@JsonProperty("NUMERO_COLLI")
public String getNUMEROCOLLI() {
return nUMEROCOLLI;
 }

/**
* Numero colli della spedizione
* 
*/
@JsonProperty("NUMERO_COLLI")
public void setNUMEROCOLLI(String nUMEROCOLLI) {
this.nUMEROCOLLI = nUMEROCOLLI;
 }

/**
* Peso espresso in grammi del singolo collo componente la spedizione
* (Required)
* 
*/
@JsonProperty("PESO_COLLI")
public Double getPESOCOLLI() {
return pESOCOLLI;
 }

/**
* Peso espresso in grammi del singolo collo componente la spedizione
* (Required)
* 
*/
@JsonProperty("PESO_COLLI")
public void setPESOCOLLI(Double pESOCOLLI) {
this.pESOCOLLI = pESOCOLLI;
 }

/**
* Altezza espressa in centimetri del singolo collo
* (Required)
* 
*/
@JsonProperty("DIM_H")
public Double getDIMH() {
return dIMH;
 }

/**
* Altezza espressa in centimetri del singolo collo
* (Required)
* 
*/
@JsonProperty("DIM_H")
public void setDIMH(Double dIMH) {
this.dIMH = dIMH;
 }

/**
* Larghezza espressa in centimetri del singolo collo
* (Required)
* 
*/
@JsonProperty("DIM_L")
public Double getDIML() {
return dIML;
 }

/**
* Larghezza espressa in centimetri del singolo collo
* (Required)
* 
*/
@JsonProperty("DIM_L")
public void setDIML(Double dIML) {
this.dIML = dIML;
 }

/**
* Profondità espressa in centimetri del singolo collo
* (Required)
* 
*/
@JsonProperty("DIM_W")
public Double getDIMW() {
return dIMW;
 }

/**
* Profondità espressa in centimetri del singolo collo
* (Required)
* 
*/
@JsonProperty("DIM_W")
public void setDIMW(Double dIMW) {
this.dIMW = dIMW;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("CONTENUTO")
public String getCONTENUTO() {
return cONTENUTO;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("CONTENUTO")
public void setCONTENUTO(String cONTENUTO) {
this.cONTENUTO = cONTENUTO;
 }

/**
* Indica il se il pagamento degli oneri doganali e’ a cura del M:Mittente o D: Destinatario
* 
*/
@JsonProperty("PAG_ONERI")
public String getPAGONERI() {
return pAGONERI;
 }

/**
* Indica il se il pagamento degli oneri doganali e’ a cura del M:Mittente o D: Destinatario
* 
*/
@JsonProperty("PAG_ONERI")
public void setPAGONERI(String pAGONERI) {
this.pAGONERI = pAGONERI;
 }

/**
* Valore spedizione, in centesimi di euro
* 
*/
@JsonProperty("VALORE_DICH")
public Double getVALOREDICH() {
return vALOREDICH;
 }

/**
* Valore spedizione, in centesimi di euro
* 
*/
@JsonProperty("VALORE_DICH")
public void setVALOREDICH(Double vALOREDICH) {
this.vALOREDICH = vALOREDICH;
 }

/**
* Indica il tipo di pagamento previsto per il contrassegno Poste
* 
*/
@JsonProperty("COD_TIPO_PAGAMENTO")
public String getCODTIPOPAGAMENTO() {
return cODTIPOPAGAMENTO;
 }

/**
* Indica il tipo di pagamento previsto per il contrassegno Poste
* 
*/
@JsonProperty("COD_TIPO_PAGAMENTO")
public void setCODTIPOPAGAMENTO(String cODTIPOPAGAMENTO) {
this.cODTIPOPAGAMENTO = cODTIPOPAGAMENTO;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("NOTE")
public String getNOTE() {
return nOTE;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("NOTE")
public void setNOTE(String nOTE) {
this.nOTE = nOTE;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("COD_RIF_COLLO")
public String getCODRIFCOLLO() {
return cODRIFCOLLO;
 }

/**
* Campo libero a disposizione del cliente
* 
*/
@JsonProperty("COD_RIF_COLLO")
public void setCODRIFCOLLO(String cODRIFCOLLO) {
this.cODRIFCOLLO = cODRIFCOLLO;
 }

/**
* Codice accessori di consegna (vedi foglio seguente)
* 
*/
@JsonProperty("BARCODE_SERVIZIO")
public String getBARCODESERVIZIO() {
return bARCODESERVIZIO;
 }

/**
* Codice accessori di consegna (vedi foglio seguente)
* 
*/
@JsonProperty("BARCODE_SERVIZIO")
public void setBARCODESERVIZIO(String bARCODESERVIZIO) {
this.bARCODESERVIZIO = bARCODESERVIZIO;
 }

/**
* Data inserimento ldv sul DB SDA
* 
*/
@JsonProperty("DATA_INS")
@JsonSerialize(using = CustomJsonDateSerializer.class)
public Date getDATAINS() {
return dATAINS;
 }

/**
* Data inserimento ldv sul DB SDA
* 
*/
@JsonProperty("DATA_INS")
@JsonDeserialize(using = CustomJsonDateDeserializer .class)
public void setDATAINS(Date dATAINS) {
this.dATAINS = dATAINS;
 }

/**
* S: spedizione assicurata in %, N: spedizione non assicurata in %
* 
*/
@JsonProperty("IS_ASS_PERCENTUALE")
public String getISASSPERCENTUALE() {
return iSASSPERCENTUALE;
 }

/**
* S: spedizione assicurata in %, N: spedizione non assicurata in %
* 
*/
@JsonProperty("IS_ASS_PERCENTUALE")
public void setISASSPERCENTUALE(String iSASSPERCENTUALE) {
this.iSASSPERCENTUALE = iSASSPERCENTUALE;
 }

/**
* Indicazioni in caso di mancata consegna al destinatario: R (Restituire) o A (Abbandonare)
* 
*/
@JsonProperty("MANCATA_CONSEGNA")
public String getMANCATACONSEGNA() {
return mANCATACONSEGNA;
 }

/**
* Indicazioni in caso di mancata consegna al destinatario: R (Restituire) o A (Abbandonare)
* 
*/
@JsonProperty("MANCATA_CONSEGNA")
public void setMANCATACONSEGNA(String mANCATACONSEGNA) {
this.mANCATACONSEGNA = mANCATACONSEGNA;
 }

/**
* Codice frazionario dell'UP. Utilizzato sia per FMP, CPT, APT, RTZ che in caso di inesito per l'home delivery
* 
*/
@JsonProperty("COD_UFF_FRAZIONARIO")
public String getCODUFFFRAZIONARIO() {
return cODUFFFRAZIONARIO;
 }

/**
* Codice frazionario dell'UP. Utilizzato sia per FMP, CPT, APT, RTZ che in caso di inesito per l'home delivery
* 
*/
@JsonProperty("COD_UFF_FRAZIONARIO")
public void setCODUFFFRAZIONARIO(String cODUFFFRAZIONARIO) {
this.cODUFFFRAZIONARIO = cODUFFFRAZIONARIO;
 }

/**
* Dettaglio del barcode_servizio per l'accessorio di consegna A Giorno Definito
* 
*/
@JsonProperty("DETTAGLIO_BS")
public String getDETTAGLIOBS() {
return dETTAGLIOBS;
 }

/**
* Dettaglio del barcode_servizio per l'accessorio di consegna A Giorno Definito
* 
*/
@JsonProperty("DETTAGLIO_BS")
public void setDETTAGLIOBS(String dETTAGLIOBS) {
this.dETTAGLIOBS = dETTAGLIOBS;
 }

/**
* Numero casella postale
* 
*/
@JsonProperty("CASELLAPOSTALE_CLIENTE")
public String getCASELLAPOSTALECLIENTE() {
return cASELLAPOSTALECLIENTE;
 }

/**
* Numero casella postale
* 
*/
@JsonProperty("CASELLAPOSTALE_CLIENTE")
public void setCASELLAPOSTALECLIENTE(String cASELLAPOSTALECLIENTE) {
this.cASELLAPOSTALECLIENTE = cASELLAPOSTALECLIENTE;
 }

/**
* The Cc_poste_cliente Schema
* <p>
* Numero conto Banco Posta per rimessa contrassegno
* 
*/
@JsonProperty("CC_POSTE_CLIENTE")
public String getCCPOSTECLIENTE() {
return cCPOSTECLIENTE;
 }

/**
* The Cc_poste_cliente Schema
* <p>
* Numero conto Banco Posta per rimessa contrassegno
* 
*/
@JsonProperty("CC_POSTE_CLIENTE")
public void setCCPOSTECLIENTE(String cCPOSTECLIENTE) {
this.cCPOSTECLIENTE = cCPOSTECLIENTE;
 }

/**
* Ragione sociale cliente
* 
*/
@JsonProperty("RAGIONE_SOCIALE_CLIENTE")
public String getRAGIONESOCIALECLIENTE() {
return rAGIONESOCIALECLIENTE;
 }

/**
* Ragione sociale cliente
* 
*/
@JsonProperty("RAGIONE_SOCIALE_CLIENTE")
public void setRAGIONESOCIALECLIENTE(String rAGIONESOCIALECLIENTE) {
this.rAGIONESOCIALECLIENTE = rAGIONESOCIALECLIENTE;
 }

/**
* identificavo del conto in sap
* 
*/
@JsonProperty("CODICE_CLIENTE_SAP")
public String getCODICECLIENTESAP() {
return cODICECLIENTESAP;
 }

/**
* identificavo del conto in sap
* 
*/
@JsonProperty("CODICE_CLIENTE_SAP")
public void setCODICECLIENTESAP(String cODICECLIENTESAP) {
this.cODICECLIENTESAP = cODICECLIENTESAP;
 }

@JsonProperty("CODICE_CONTRATTO_SDA")
public String getCODICECONTRATTOSDA() {
return cODICECONTRATTOSDA;
 }

@JsonProperty("CODICE_CONTRATTO_SDA")
public void setCODICECONTRATTOSDA(String cODICECONTRATTOSDA) {
this.cODICECONTRATTOSDA = cODICECONTRATTOSDA;
 }

/**
* contratto CRM
* 
*/
@JsonProperty("CODICE_CONTRATTO_POSTE")
public String getCODICECONTRATTOPOSTE() {
return cODICECONTRATTOPOSTE;
 }

/**
* contratto CRM
* 
*/
@JsonProperty("CODICE_CONTRATTO_POSTE")
public void setCODICECONTRATTOPOSTE(String cODICECONTRATTOPOSTE) {
this.cODICECONTRATTOPOSTE = cODICECONTRATTOPOSTE;
 }

/**
* partita iva cliente contratto
* 
*/
@JsonProperty("PIVA")
public String getPIVA() {
return pIVA;
 }

/**
* partita iva cliente contratto
* 
*/
@JsonProperty("PIVA")
public void setPIVA(String pIVA) {
this.pIVA = pIVA;
 }

/**
* codice fiscale cliente contratto
* 
*/
@JsonProperty("CODICE_FISCALE")
public String getCODICEFISCALE() {
return cODICEFISCALE;
 }

/**
* codice fiscale cliente contratto
* 
*/
@JsonProperty("CODICE_FISCALE")
public void setCODICEFISCALE(String cODICEFISCALE) {
this.cODICEFISCALE = cODICEFISCALE;
 }

}
