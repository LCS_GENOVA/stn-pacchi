package com.selexelsag.xte.service.system;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.Resource;

import com.selexelsag.xte.service.exceptions.ServiceException;

public class AppConfig extends PropertyPlaceholderConfigurer implements IAppConfig 
{
	private Properties generalConfiguration;
	private Properties bpmConfiguration;

	private Properties urlMapConfiguration;
	
	private Properties messageProcessorConfiguration;
	
	private Properties probingConfiguration;

	private static Log log = LogFactory.getLog(AppConfig.class); 

	@Override
	public void init() throws ServiceException
	{
		setIgnoreResourceNotFound(true);
		setIgnoreUnresolvablePlaceholders(true);
		
		/* Con questo comando vengono caricate le variabili
		 * d'ambiente del sistema
		 */
		setLocations(new Resource[0]);

		try
		{
			log.info("Init application configuration BEGINS");
			//carico configurazione generale
			log.info(String.format("Loading general configuration BEGINS - filename=[%1$s])", IAppConfig.GENERAL_CONFIGURATION_FILENAME));
			
			generalConfiguration = new Properties();
			loadProperties(IAppConfig.GENERAL_CONFIGURATION_FILENAME, IAppConfig.DEFAULT_GENERAL_CONFIGURATION_FILENAME, generalConfiguration);
			
			if (log.isDebugEnabled())
				log.debug(String.format("GENERAL PARAMS=[%1$s]", generalConfiguration));

			log.info("Loading general configuration ENDS");
			log.info(String.format("Loading JBPM configuration BEGINS- filename=[%1$s])", IAppConfig.BPM_CONFIGURATION_FILENAME));
			
			//carico configurazione bpm
			bpmConfiguration = new Properties();
			loadProperties(IAppConfig.BPM_CONFIGURATION_FILENAME, IAppConfig.DEFAULT_BPM_CONFIGURATION_FILENAME, bpmConfiguration);
			
			if (log.isDebugEnabled())
				log.debug(String.format("BPM PARAMS=[%1$s]", bpmConfiguration));
			log.info("Loading JBPM configuration ENDS");
			
			//carico configurazione del mapping url
			urlMapConfiguration = new Properties();
			loadProperties(IAppConfig.URLMAP_CONFIGURATION_FILENAME, IAppConfig.DEFAULT_URLMAP_CONFIGURATION_FILENAME, urlMapConfiguration);
			
			if (log.isDebugEnabled())
				log.debug(String.format("URLMAP PARAMS=[%1$s]", urlMapConfiguration));
			log.info("Loading URLMAP configuration ENDS");
			
			//carico configurazione message processor
			log.info(String.format("Loading message processor configuration BEGINS - filename=[%1$s])", IAppConfig.MESSAGE_PROCESSOR_FILENAME));
			
			messageProcessorConfiguration = new Properties();
			loadProperties(IAppConfig.MESSAGE_PROCESSOR_FILENAME, IAppConfig.DEFAULT_GENERAL_CONFIGURATION_FILENAME, messageProcessorConfiguration);
			
			if (log.isDebugEnabled())
				log.debug(String.format("MESSAGE PROCESSORS PARAMS=[%1$s]", messageProcessorConfiguration));

			log.info("Loading message processor configuration ENDS");
			
			probingConfiguration = new Properties();
			loadProperties(IAppConfig.PROBING_FILENAME, IAppConfig.DEFAULT_PROBING_FILENAME, probingConfiguration);
			
			if (log.isDebugEnabled())
				log.debug(String.format("PROBING PARAMS=[%1$s]", probingConfiguration));

			log.info("Loading probing configuration ENDS");
			
			log.info("Init application configuration ENDS");
		}
		catch (Exception e)
		{
			log.error("Error initializing application", e);
			throw new ServiceException(e, "Error initializing application", true);
		}
	}

	private void loadProperties(String filename, String defaultfilename, Properties prop) throws Exception
	{
		logger.debug("loadProperties " + filename);
		InputStream inStream = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(filename);
		if (inStream == null)
		{
			inStream=Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(defaultfilename);
			log.info(String.format("Filename=[%1$s] not found,  default settings loaded", filename));
		}
		
		Properties tmp = new Properties();
		tmp.load(inStream);

		for (Object key : tmp.keySet()) 
		{
			String keyStr = key.toString();
			String oldValue = (String)tmp.get(keyStr);
			String valueStr = parseStringValue(oldValue, tmp, new HashSet());
			prop.put(keyStr, valueStr);
		}
	}

	public Properties getGeneralConfiguration() 
	{
		return generalConfiguration;
	}

	public void setGeneralConfiguration(Properties generalConfiguration) 
	{
		this.generalConfiguration = generalConfiguration;
	}

	public Properties getUrlMapConfiguration() 
	{
		return urlMapConfiguration;
	}

	public void setUrlMapConfiguration(Properties urlMapConfiguration) 
	{
		this.urlMapConfiguration = urlMapConfiguration;
	}

	public Properties getBpmConfiguration() 
	{
		return bpmConfiguration;
	}

	public void setBpmConfiguration(Properties bpmConfiguration) 
	{
		this.bpmConfiguration = bpmConfiguration;
	}

	@Override
	public String getGeneralParam(String key) 
	{
		if (generalConfiguration != null)
			return generalConfiguration.getProperty(key);
		return null;
	}

	@Override
	public String getBpmParam(String key) 
	{
		if (bpmConfiguration != null)
			return bpmConfiguration.getProperty(key);
		return null;
	}

	@Override
	public String getUrlMapParam(String key) 
	{
		if (urlMapConfiguration != null)
			return urlMapConfiguration.getProperty(key);
		return null;
	}

	@Override
	public String getMessageProcessorParam(String key) 
	{
		if (messageProcessorConfiguration != null)
			return messageProcessorConfiguration.getProperty(key);
		return null;
	}
	
	@Override
	public Properties getMessageProcessorProperties() {
		
		return messageProcessorConfiguration;
	}
	
	@Override
	public String getProbingParam(String key) 
	{
		if (probingConfiguration != null)
			return probingConfiguration.getProperty(key);
		return null;
	}
	
	@Override
	public Properties getProbingProperties() 
	{
		return probingConfiguration;
	}
}
