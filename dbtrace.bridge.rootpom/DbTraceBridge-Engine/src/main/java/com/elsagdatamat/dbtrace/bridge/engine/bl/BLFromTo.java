package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.util.List;

import com.elsagdatamat.dbtrace.bridge.engine.entities.FromTo;
import com.elsagdatamat.framework.utils.PairLinkedList;


public class BLFromTo extends BaseManager<FromTo, String> implements IBLFromTo {

	@Override
	public String getCmpDestinationFromNspSource (String nspSource)
	{
		PairLinkedList<String, Object> properties = new PairLinkedList<String, Object>();
		properties.add(FromTo.FRAZIONARIO_FROM, nspSource);
		//Search s=new Search();
		List<FromTo>  resultList = findByProperties(properties);
		if ((resultList !=null) && (resultList.size()>0))
			return resultList.get(0).getFrazionarioTo();
		else
			return null;
	}

}
