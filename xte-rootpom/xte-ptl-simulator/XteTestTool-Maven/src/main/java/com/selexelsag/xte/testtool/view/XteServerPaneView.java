/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.common.XteTestToolFocusTraversalPolicy;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.JTextComponent;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteServerPaneView extends javax.swing.JPanel implements FocusListener {

    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages = ResourceBundle.getBundle("bundles/messages");
    private XteServerJDialog xteServerDialog = null;
    private static Logger logger = Logger.getLogger(XteServerPaneView.class);

    public XteServerPaneView(XteServerJDialog xteServerDialog) {
        this.xteServerDialog = xteServerDialog;
        initComponents();
        initializeView();
    }

    public JPanel getXteServerSettingsPanelRef(){ return this; }
    public JLabel getXteServerSettingsTitleLabelRef(){ return this.xteServerSettingsTitlejLabel; }
    public JTextArea getXteServerPropertiesTextAreaRef(){ return this.propertiesjTextArea; }    
    public JButton getSaveButtonRef(){ return this.savejButton; }

    private void customizeView() {
        
        this.xteServerSettingsTitlejLabel.setText(messages.getString("settings.dialog.view.title.label"));
        this.savejButton.setText(messages.getString("view.save.button"));
        this.exitjButton.setText(messages.getString("view.exit.button"));
    }

    private void initializeView() {

        //buildComponentSortedMap();

        //this.setFocusCycleRoot(true);
        //this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

        defineTabPolicyForTextArea(this.propertiesjTextArea);

        JLabel cornelLabel = new JLabel("");
        cornelLabel.setOpaque(true);
        cornelLabel.setBackground(new Color(240, 240, 240));

        this.propertiesjScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
        this.propertiesjScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        this.propertiesjScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(35,35)));
        this.propertiesjScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        this.propertiesjScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(35,35)));
                        
        this.propertiesjTextArea.addFocusListener(this);
    }

    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.propertiesjTextArea);
        componentSortedMap.put(2, this.savejButton);
        componentSortedMap.put(3, this.exitjButton);
    }

    private  static void defineTabPolicyForTextArea(final JTextArea textarea) {

        final Object tabActionKey = new Object();
        final Object shiftTabActionKey = new Object();
        KeyStroke tabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0, false);
        KeyStroke shiftTabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_DOWN_MASK, false);

        Action tabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusNextComponent((Component) source);
                }
            }
        };

        Action shiftTabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent((Component) source);
                }
            }
        };

        textarea.getActionMap().put(tabActionKey, tabAction);
        textarea.getInputMap().put(tabKeyStroke, tabActionKey);

        textarea.getActionMap().put(shiftTabActionKey, shiftTabAction);
        textarea.getInputMap().put(shiftTabKeyStroke, shiftTabActionKey);

    }

    private boolean readPropertiesFile(String fn, JTextComponent pane) {
        FileReader fr = null;
        try {
            fr = new FileReader(fn);
            pane.read(fr, null);
            fr.close();
            return true;
        }
        catch (IOException e) {
            return false;
        }
        finally {
            try {
                fr.close();
            } catch (IOException ex) {
            }
        }                
    }

    private boolean writePropertiesFile(String fn, JTextComponent pane) {
        FileWriter fw = null;
        try {
            fw = new FileWriter(fn);
            fw.write(pane.getText());
            fw.close();
            return true;
        }
        catch (IOException e) {
            return false;
        }
        finally {
            try {
                fw.close();
            } catch (IOException ex) {
            }
        }
    }
    
       
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        xteServerSettingsjPanel = new javax.swing.JPanel();
        savejButton = new javax.swing.JButton();
        propertiesjPanel = new javax.swing.JPanel();
        propertiesjScrollPane = new javax.swing.JScrollPane();
        propertiesjTextArea = new javax.swing.JTextArea();
        xteServerSettingsTitlejLabel = new javax.swing.JLabel();
        exitjButton = new javax.swing.JButton();

        setMaximumSize(new java.awt.Dimension(500, 500));
        setMinimumSize(new java.awt.Dimension(500, 500));
        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(500, 500));
        addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                formAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        xteServerSettingsjPanel.setBackground(new java.awt.Color(255, 255, 204));
        xteServerSettingsjPanel.setMaximumSize(new java.awt.Dimension(500, 500));
        xteServerSettingsjPanel.setMinimumSize(new java.awt.Dimension(500, 500));
        xteServerSettingsjPanel.setOpaque(false);
        xteServerSettingsjPanel.setPreferredSize(new java.awt.Dimension(500, 500));

        savejButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        savejButton.setForeground(new java.awt.Color(51, 51, 51));
        savejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Save1.png"))); // NOI18N
        savejButton.setText("Save");
        savejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        savejButton.setMaximumSize(new java.awt.Dimension(100, 50));
        savejButton.setMinimumSize(new java.awt.Dimension(100, 50));
        savejButton.setPreferredSize(new java.awt.Dimension(100, 50));
        savejButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savejButtonActionPerformed(evt);
            }
        });

        propertiesjPanel.setBackground(new java.awt.Color(220, 220, 180));
        propertiesjPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        propertiesjPanel.setMaximumSize(new java.awt.Dimension(480, 350));
        propertiesjPanel.setMinimumSize(new java.awt.Dimension(480, 350));
        propertiesjPanel.setOpaque(false);
        propertiesjPanel.setPreferredSize(new java.awt.Dimension(480, 350));

        propertiesjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        propertiesjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        propertiesjScrollPane.setMaximumSize(new java.awt.Dimension(450, 330));
        propertiesjScrollPane.setMinimumSize(new java.awt.Dimension(450, 330));
        propertiesjScrollPane.setPreferredSize(new java.awt.Dimension(450, 330));

        propertiesjTextArea.setColumns(20);
        propertiesjTextArea.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        propertiesjTextArea.setForeground(new java.awt.Color(0, 51, 204));
        propertiesjTextArea.setLineWrap(true);
        propertiesjTextArea.setRows(5);
        propertiesjTextArea.setWrapStyleWord(true);
        propertiesjTextArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        propertiesjScrollPane.setViewportView(propertiesjTextArea);

        javax.swing.GroupLayout propertiesjPanelLayout = new javax.swing.GroupLayout(propertiesjPanel);
        propertiesjPanel.setLayout(propertiesjPanelLayout);
        propertiesjPanelLayout.setHorizontalGroup(
            propertiesjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(propertiesjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(propertiesjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );
        propertiesjPanelLayout.setVerticalGroup(
            propertiesjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(propertiesjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(propertiesjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        xteServerSettingsTitlejLabel.setBackground(new java.awt.Color(255, 255, 255));
        xteServerSettingsTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        xteServerSettingsTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        xteServerSettingsTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        xteServerSettingsTitlejLabel.setText("Settings");
        xteServerSettingsTitlejLabel.setIconTextGap(5);
        xteServerSettingsTitlejLabel.setMaximumSize(new java.awt.Dimension(480, 30));
        xteServerSettingsTitlejLabel.setMinimumSize(new java.awt.Dimension(480, 30));
        xteServerSettingsTitlejLabel.setPreferredSize(new java.awt.Dimension(480, 30));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("Exit");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));
        exitjButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitjButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout xteServerSettingsjPanelLayout = new javax.swing.GroupLayout(xteServerSettingsjPanel);
        xteServerSettingsjPanel.setLayout(xteServerSettingsjPanelLayout);
        xteServerSettingsjPanelLayout.setHorizontalGroup(
            xteServerSettingsjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(xteServerSettingsjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(xteServerSettingsjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(xteServerSettingsjPanelLayout.createSequentialGroup()
                        .addComponent(propertiesjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(xteServerSettingsjPanelLayout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(savejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(60, 60, 60))
                    .addGroup(xteServerSettingsjPanelLayout.createSequentialGroup()
                        .addComponent(xteServerSettingsTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        xteServerSettingsjPanelLayout.setVerticalGroup(
            xteServerSettingsjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(xteServerSettingsjPanelLayout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(xteServerSettingsTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(propertiesjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(xteServerSettingsjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(savejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(xteServerSettingsjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(xteServerSettingsjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void formAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_formAncestorAdded
        try{
            logger.info("XteServerPaneView start formAncestorAdded");
            customizeView();
            
            File f = new File(XteTestToolConfiguration.INIT_CONFIGURATION_FILE);

            readPropertiesFile(f.toString(), this.propertiesjTextArea);            
            this.propertiesjTextArea.requestFocus();
            logger.info("XteServerPaneView formAncestorAdded completed successfully");
        } 
        catch (Exception ex) {
            logger.error("XteServerPaneView formAncestorAdded failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Xte Test Tool Settings view formAncestorAdded failed", ex);
        }
    }//GEN-LAST:event_formAncestorAdded

    private void savejButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savejButtonActionPerformed

        logger.info("XteServerPaneView start savejButtonActionPerformed");
        try 
        {
            Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
            String title = messages.getString("xtetesttool.changeconfig.confirm.title");
            String message = messages.getString("xtetesttool.changeconfig.confirm.question");
            int answer = JOptionPane.showConfirmDialog(this, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (answer == JOptionPane.NO_OPTION)
            {
                return;
            }
            
            File f = new File(XteTestToolConfiguration.INIT_CONFIGURATION_FILE);

            writePropertiesFile(f.toString(), this.propertiesjTextArea);
                        
            logger.info("XteServerPaneView savejButtonActionPerformed completed successfully");

        }
        catch (Exception ex) {
            logger.error("XteServerPaneView savejButtonActionPerformed failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Xte Test Tool Settings view savejButtonActionPerformed failed", ex);
        }
    }//GEN-LAST:event_savejButtonActionPerformed

    private void exitjButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitjButtonActionPerformed

        logger.info("XteServerPaneView start exitjButtonActionPerformed");        
        this.xteServerDialog.dispose();
        logger.info("XteServerPaneView exitjButtonActionPerformed completed successfully");
        
    }//GEN-LAST:event_exitjButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton exitjButton;
    private javax.swing.JPanel propertiesjPanel;
    private javax.swing.JScrollPane propertiesjScrollPane;
    private javax.swing.JTextArea propertiesjTextArea;
    private javax.swing.JButton savejButton;
    private javax.swing.JLabel xteServerSettingsTitlejLabel;
    private javax.swing.JPanel xteServerSettingsjPanel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        if(comp instanceof JTextField)
        {
            ((JTextField)comp).setBackground(new Color(255,204,102));
        }
        if(comp instanceof JTextArea)
        {
            ((JTextArea)comp).setBackground(new Color(255,204,102));
        }
        if(comp instanceof JPasswordField)
        {
            ((JPasswordField)comp).setBackground(new Color(255,204,102));
        }

    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        if(comp instanceof JTextField)
        {

            if(((JTextField)comp).isEditable()) {
                ((JTextField)comp).setBackground(Color.WHITE);
            }
            else {
                ((JTextField)comp).setBackground(new Color(220, 220, 220));
            }
        }
        if(comp instanceof JTextArea)
        {
            if(((JTextArea)comp).isEditable()) {
                ((JTextArea)comp).setBackground(Color.WHITE);
            }
            else {
                ((JTextArea)comp).setBackground(new Color(220, 220, 220));
            }
        }
        if(comp instanceof JPasswordField)
        {
            if(((JPasswordField)comp).isEditable()) {
                ((JPasswordField)comp).setBackground(Color.WHITE);
            }
            else {
                ((JPasswordField)comp).setBackground(new Color(220, 220, 220));
            }
        }

    }

}
