
package com.elsagdatamat.tt.trackdbws.trackdbrwservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TracesList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TracesList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="traces" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}Trace" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TracesList", propOrder = {
    "traces"
})
public class TracesList {

    @XmlElement(required = true)
    protected List<Trace> traces;

    /**
     * Gets the value of the traces property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the traces property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTraces().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Trace }
     * 
     * 
     */
    public List<Trace> getTraces() {
        if (traces == null) {
            traces = new ArrayList<Trace>();
        }
        return this.traces;
    }

}
