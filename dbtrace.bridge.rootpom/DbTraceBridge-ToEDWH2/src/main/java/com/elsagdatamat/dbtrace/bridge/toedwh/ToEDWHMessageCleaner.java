package com.elsagdatamat.dbtrace.bridge.toedwh;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.dbtrace.bridge.engine.MessageBuilderBase;
import com.elsagdatamat.dbtrace.bridge.engine.bl.IParameterManager;
import com.elsagdatamat.dbtrace.bridge.engine.bl.ParameterManager;
import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSend;
import com.elsagdatamat.trackdb.toedwh.manager.ISentTraceManager;
import com.elsagdatamat.trackdb.toedwh.manager.ITraceManager;
import com.elsagdatamat.trackdb.toedwh.manager.ITraceTypesToSendManager;
import com.elsagdatamat.trackdb.toedwh.vo.EdwhTracesList;
import com.elsagdatamat.trackdb.toedwh.vo.TraceVO;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWService;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public class ToEDWHMessageCleaner extends MessageBuilderBase implements ToEdwhMessageCleanerInterface {

	private static final String PACCO = "PACCO";
	private static final String EDWH = "EDWH";
	private static final String TOKEN_SPLITTER = ";";

	private static final String RECORD_ENABLE_EDWH = "DBTraceToEDWHBridgeClean";
	private static final String RECORD_MASSIVE_MODE = "DBTraceToEDWHBridgeMassiveMode";
	private static final String RECORD_TIME_HORIZON = "DBTraceToEDWHBridgeTimeHorizon";

	protected static final Log log = LogFactory.getLog(ToEDWHMessageCleaner.class);

	private IParameterManager parameterManager;

	private static Integer timeHorizon;
	private static String isActive;
	private static String isMassiveMode;
	private ITraceManager traceManager;

	private ITraceTypesToSendManager traceTypesToSendManager;
	private ISentTraceManager sentTraceManager;

	public ITraceManager getTraceManager() {
		return traceManager;
	}

	public void setTraceManager(ITraceManager traceManager) {
		this.traceManager = traceManager;
	}

	public ITraceTypesToSendManager getTraceTypesToSendManager() {
		return traceTypesToSendManager;
	}

	public void setTraceTypesToSendManager(ITraceTypesToSendManager traceTypesToSendManager) {
		this.traceTypesToSendManager = traceTypesToSendManager;
	}

	public ISentTraceManager getSentTraceManager() {
		return sentTraceManager;
	}

	public void setSentTraceManager(ISentTraceManager sentTraceManager) {
		this.sentTraceManager = sentTraceManager;
	}

	public IParameterManager getParameterManager() {
		return parameterManager;
	}

	public void setParameterManager(IParameterManager parameterManager) {
		this.parameterManager = parameterManager;
	}

	private String wsUrl;

	public void setWsUrl(String wsUrl) {
		this.wsUrl = wsUrl;
	}

	private String wsNamespaceURI;

	public void setWsNamespaceURI(String wsNamespaceURI) {
		this.wsNamespaceURI = wsNamespaceURI;
	}

	private String wsLocalpart;

	public void setWsLocalpart(String wsLocalpart) {
		this.wsLocalpart = wsLocalpart;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.elsagdatamat.dbtrace.bridge.toedwh.ToEdwhMessageCleanerInterface#
	 * create(com.selexelsag.tracktrace.trackdbws.xml.TracesList)
	 */

	@Override
	public List<String> create(TracesList input) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.elsagdatamat.dbtrace.bridge.toedwh.ToEdwhMessageCleanerInterface#
	 * create(com.elsagdatamat.dbtrace.bridge.toedwh.EdwhTracesList)
	 */
	@Override
	public List<String> create(EdwhTracesList input) {

		return create(input.getTraceList());
	}

	private static List<String> create(List<TraceVO> traceList) {
		List<String> toWrite = new ArrayList<String>();
		for (TraceVO t : traceList)
			toWrite.add(CsvUtils.toCsv(t).toString());
		return toWrite;
	}

	// IMessageSender
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.elsagdatamat.dbtrace.bridge.toedwh.ToEdwhMessageCleanerInterface#
	 * send(java.util.List)
	 */

	@Override
	public void send(final List<String> messageList) throws IOException {
		return;
	}

	private static void loadParams(IParameterManager parameterManager) {
		if (timeHorizon == null)
			timeHorizon = new Integer(parameterManager.findByID(RECORD_TIME_HORIZON).getValue());
		if (isMassiveMode == null)
			isMassiveMode = parameterManager.findByID(RECORD_MASSIVE_MODE).getValue();
		isActive = parameterManager.findByID(RECORD_ENABLE_EDWH).getValue();

	}


	@Override
	@Transactional(value = "dbTraceBridge-transactionManager", propagation = Propagation.REQUIRED)
	public Integer deleteTraces() throws Exception{
		Integer result =null;
		loadParams(parameterManager);
		if (!"T".equals(isActive)) {
			log.debug("Servizio non attivo");
			return null;
		}

		Boolean allTypes = "T".equals(isMassiveMode);

		if (!allTypes) {
				// deletes only traces that have been sent and older that 180 days
			result = traceManager.deleteTraces(timeHorizon);
		} else{
			// recuper token
			TrackDBRWServicePortType twsp = loadToken();
			log.debug(String.format("After deleteTraces for service '%s' (wsToken='%s'): stopDate=%s;", token.getServiceId(),token.getWsToken(), null));
			if(token.getWsToken()==null){
				System.out.print("Errore: token non caricato");
				log.fatal("Errore: token non caricato");
				throw new Exception("Errore: token non caricato - sent_trace non aggiornato");
			}

			// deletes all traces older that 180 days (it supposes that all have been sent)
			result = traceManager.bulkDeleteTraces(timeHorizon, PACCO);

			String[] split = token.getWsToken().split(TOKEN_SPLITTER);
			Long id = new Long(split[1]);

			// insert in sentTraces per gestire correttamente lo svecchiamento giornaliero
			log.debug("Aggiornamento sent_traces START");
			fillSentTraces(id);
			log.debug("Aggiornamento sent_traces END");
			release(twsp);
		}
		log.debug("delete complete: "+result);
		return result;
	}

	private TrackDBRWServicePortType loadToken(){

		URL url = null;
		try {
			url = new URL(wsUrl);
		} catch (MalformedURLException e) {
			log.fatal("Malformed URL", e);
		}
		QName name = new QName(wsNamespaceURI, wsLocalpart);
		log.debug("Instantiate ws");
		TrackDBRWService tws = new TrackDBRWService(url, name);
		log.debug("Getting port");
		TrackDBRWServicePortType twsp = tws.getTrackDBRWServicePort();

		// Lettura chunk di tracce da Ws
		Object token = null;
		log.debug("Getting token");
		token = this.getToken(twsp);// imposta l'oggetto token nel builderBase
		if (token == null) {
			log.debug("error during getToken");
		}
		return twsp;
	}


	public static void main(String[] args) throws Exception {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		ToEdwhMessageCleanerInterface ToEdwhMessageCleaner = context.getBean("toEDWHCleaner", ToEdwhMessageCleanerInterface.class);
		ParameterManager parameterManager = context.getBean("parameterManager", ParameterManager.class);

		loadParams(parameterManager);

		long currentTimeMillis = System.currentTimeMillis();
		Integer result = ToEdwhMessageCleaner.deleteTraces();
		currentTimeMillis = System.currentTimeMillis() - currentTimeMillis;
		System.out.print("Elapsed time for " + result + " deleted traces:" + currentTimeMillis);


//		ToEdwhMessageCleaner.fillSentTraces(288166415L);
//		currentTimeMillis = System.currentTimeMillis() - currentTimeMillis;
//		System.out.print("Elapsed time for insert traces:" + currentTimeMillis);

		System.exit(0);
	}


	/**
	 * Fills the table sent_traces for all traces with id under the token id
	 * and configured to be sent
	 * @param id
	 */
	@Override
	public void fillSentTraces(Long id) {
		log.debug("fillSentTraces START");

		List<TraceTypesToSend> typesToSend = traceTypesToSendManager.findByDestinationTracedEntityChannel(EDWH, PACCO, null);
		if (typesToSend != null && typesToSend.size() > 0) {
			sentTraceManager.fillSentTraces(id, typesToSend);
		} else {
			log.error(String.format("No record found on trace_type_to_send for destination '%s', tracedEntity '%s'", EDWH, PACCO));
		}

		log.debug("fillSentTraces END");
	}

	@Override
	public TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate) {
		// TODO Auto-generated method stub
		return null;
	}




}
