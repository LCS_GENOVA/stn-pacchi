package com.elsagdatamat.dbtrace.bridge.toedwh;

import java.util.List;

public class EDWHFattPock {
	public EDWHHeader header;
	public List<EDWHFatturazioni> fatturazioni;
	public EDWHFooter footer;
	
	/*
	public String toCsv()
	{
		String s = header.toCsv();
		for (EDWHFatturazioni f : fatturazioni)
		{
			s+=f.toCsv();
		}
		s+=footer.toCsv();
		return s;
	}
	*/
	//SCOTT: INCREASING PERFORMANCE!
	public String toCsv()
	{
		
		StringBuilder stringBuilder = new StringBuilder(1024*1024*2);//2M
		
		stringBuilder.append(header.toCsv());
		for (EDWHFatturazioni f : fatturazioni)
		{
			stringBuilder.append(f.toCsv());
		}
		stringBuilder.append(footer.toCsv());
		
		return stringBuilder.toString();
	}

}
