/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectmodel;

 
import com.selexelsag.xte.testtool.datamodel.XteAccettazionePezzi;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Tassara
 */
public class XteAccettazionePezziTableModel extends DefaultTableModel {

    private String[] columnNames = new String[]{
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.tipo"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.idoperatore"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.officefrazionario"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.numerospedizione"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.codicecliente"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.codice"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.prodotto"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.dataora"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.peso"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.prezzo"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.percetto"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.destinazione"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.destinatario"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.indirizzo"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.cap"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.nomemittente"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.destinazionemittente"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.indirizzomittente"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.capmittente"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.sa"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.codicear"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.valoreassicurato"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.importocontrassegno"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.iban"),
        ResourceBundle.getBundle("bundles/messages").getString("accettazionepezzi.table.header.oratimedefinite"),
    };

    private Map<String, XteAccettazionePezzi> map = new HashMap<String, XteAccettazionePezzi>();
    
    public XteAccettazionePezziTableModel() {

        super();
        this.setColumnCount(columnNames.length);
        this.setColumnIdentifiers(columnNames);

    }

    public void addAccettazionePezziItem(XteAccettazionePezzi item) throws XteTestToolDuplicateKeyException{
        if(map.containsKey(item.getAccettazionePezziCode()))
        {
            throw new XteTestToolDuplicateKeyException(Group.GENERIC, Severity.WARNING, "Accettazione Pezzi Table Model duplicate key exception");
        }
        else
        {
            addRow(new Object[]{item.getAccettazionePezziTipo(), item.getAccettazionePezziIdOp(), item.getAccettazionePezziOffFraz(), item.getAccettazionePezziNumSped(), item.getAccettazionePezziCodeCliente(), item.getAccettazionePezziCode(), item.getAccettazionePezziProduct(), item.getAccettazionePezziDateTime(), item.getAccettazionePezziPeso(), item.getAccettazionePezziPrezzo(), item.getAccettazionePezziPercetto(), item.getAccettazionePezziDestinazione(), item.getAccettazionePezziDestinatario(), item.getAccettazionePezziIndirizzo(), item.getAccettazionePezziCAP(), item.getAccettazionePezziNomeMitt(), item.getAccettazionePezziDestMitt(), item.getAccettazionePezziIndMitt(), item.getAccettazionePezziCAPMitt(), item.getAccettazionePezziSA(), item.getAccettazionePezziCodAR(), item.getAccettazionePezziValAss(), item.getAccettazionePezziImpContr(), item.getAccettazionePezziIBAN(), item.getAccettazionePezziOraTimeDef()});
            map.put(item.getAccettazionePezziCode(), item);            
        }        
    }
    

    public void setModelData(Map<String, XteAccettazionePezzi> items) {
        getDataVector().clear();
        map.clear();                
        for (XteAccettazionePezzi item : items.values()) {
            addRow(new Object[]{item.getAccettazionePezziTipo(), item.getAccettazionePezziIdOp(), item.getAccettazionePezziOffFraz(), item.getAccettazionePezziNumSped(), item.getAccettazionePezziCodeCliente(), item.getAccettazionePezziCode(), item.getAccettazionePezziProduct(), item.getAccettazionePezziDateTime(), item.getAccettazionePezziPeso(), item.getAccettazionePezziPrezzo(), item.getAccettazionePezziPercetto(), item.getAccettazionePezziDestinazione(), item.getAccettazionePezziDestinatario(), item.getAccettazionePezziIndirizzo(), item.getAccettazionePezziCAP(), item.getAccettazionePezziNomeMitt(), item.getAccettazionePezziDestMitt(), item.getAccettazionePezziIndMitt(), item.getAccettazionePezziCAPMitt(), item.getAccettazionePezziSA(), item.getAccettazionePezziCodAR(), item.getAccettazionePezziValAss(), item.getAccettazionePezziImpContr(), item.getAccettazionePezziIBAN(), item.getAccettazionePezziOraTimeDef()});
            map.put(item.getAccettazionePezziCode(), item);
        }
    }

    public XteAccettazionePezzi getSelectedItem(int rowIndex) {
        
        String AccettazionePezziCode = (String) getValueAt(rowIndex, 0);
        if(map.containsKey(AccettazionePezziCode))
        {
            return map.get(AccettazionePezziCode);
        }
        else
        {
            return null;
        }
    }

    @Override
    public void removeRow(int row) {
        String AccettazionePezziCode = (String) getValueAt(row, 5);
        if(map.containsKey(AccettazionePezziCode))
        {
            map.remove(AccettazionePezziCode);
        }        
        getDataVector().removeElementAt(row);
        fireTableDataChanged();
    }

    public void clearModel() {
        getDataVector().clear();
        map.clear();
    }

    public Map<String, XteAccettazionePezzi> getMap() {
        return map;
    }

    
    @Override
    public boolean isCellEditable(int row, int column) {
        return (false);
    }
    
}
