set lines 300
set serveroutput on
set verify off
spool 1715_STNPACCHI_Main.a.populate_db_track_owner.lst
col nome new_value  nomefile
select 'Schema_Connesso_'||user nome from dual;
WHENEVER OSERROR EXIT ROLLBACK; 
WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK;

set echo off
prompt ===>>>>1715_STNPACCHI_insert_pk_con_parametri
@1715_STNPACCHI_insert_pk_con_parametri.sql;
@1715_STNPACCHI_create_view_pk_prd_prodotti_sda_vw.sql
commit;
spool off



