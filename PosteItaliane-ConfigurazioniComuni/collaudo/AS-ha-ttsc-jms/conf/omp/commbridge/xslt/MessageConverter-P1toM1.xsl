<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ED="http://www.elsagdatamat.com/Tracking/Messages/xslt/P1toM1Transform">
   <xsl:output indent="yes" method="xml" omit-xml-declaration="no" encoding="UTF-8"/>
	
	<!-- INCLUDED STYLESHEET -->
	<xsl:include href="MessageConverter-Common.xsl" />
	
	<xsl:template match="/MSG">
      <xsl:element name="MSG">
		   <xsl:apply-templates select="HDR" mode="HDRTemplate" />
		   <xsl:apply-templates select="P1" mode="P1Template" />
      </xsl:element>
   </xsl:template>
   
   <!-- TAG HDR -->
	<xsl:template mode="HDRTemplate" match="HDR">
      <xsl:element name="HDR">
         <xsl:call-template name="HDRAttributesTemplate">
			<xsl:with-param name="frazionarioParam" select="@AGNZ" />            
            <xsl:with-param name="swrelParam" select="$gExtSwRel" />            
         </xsl:call-template>
      </xsl:element>      
	</xsl:template>
	
	<!-- Attributi TAG HDR -->
	<xsl:template name="HDRAttributesTemplate">		
		<xsl:param name="frazionarioParam"/>		
		<xsl:param name="swrelParam"/>
			<xsl:attribute name="OFCID"><xsl:value-of select="$frazionarioParam" />
			</xsl:attribute>
			<xsl:attribute name="SWREL"><xsl:value-of select="$swrelParam" />
			</xsl:attribute>
	</xsl:template>   

	<xsl:template mode="ACCTemplate" match="ACC" xmlns:p1ExtFun="xalan://com.elsagdatamat.tracktrace.core.infrastructure.trackmessages.converter.P1TrackMessageConverter">
		<xsl:element name="M1D">
			<xsl:element name="OBJ">
				<xsl:element name="OBJID"><xsl:value-of select="OBJID" /></xsl:element>
				<xsl:element name="PH"><xsl:value-of select="$gExtPacchiPhaseAcc" /></xsl:element>
				<xsl:element name="PSTF"><xsl:value-of select="$gExtPacchiPstf" /></xsl:element>
				<xsl:element name="OP"><xsl:value-of select="$gExtPacchiOperator" /></xsl:element>
				<xsl:element name="TDT"><xsl:value-of select="TDA" /></xsl:element>
				<xsl:element name="SUBC"><xsl:value-of select="PRD" /></xsl:element>
			</xsl:element>
			<xsl:element name="ACC">
				<xsl:element name="AOFC"><xsl:value-of select="../../HDR/@AGNZ" /></xsl:element>
				<!-- Estraggo dalla data/ora in questo formato 03/03/2004 15:50:00 solo la parte relativa alla data 03/03/2004 -->				
				<xsl:if test="string-length(TDA/text())!=0">
					<xsl:element name="ADT"><xsl:value-of select="substring-before(TDA,' ')"/></xsl:element>	
				</xsl:if>				
				<xsl:if test="string-length(ZIP/text())!=0">
					<xsl:element name="ZIP"><xsl:value-of select="concat(ZIP, DIST)"/></xsl:element>
				</xsl:if>	
				<xsl:choose>
					<xsl:when test="string-length(./TAS/text())!=0">
						<xsl:element name="PERC"><xsl:value-of select="TAS"/></xsl:element>
						<xsl:choose>    
							<xsl:when test="string-length(./IMPP) !=0">
								<xsl:element name="FARE"><xsl:value-of select="TAS + IMPP"/></xsl:element>
							</xsl:when>
							<xsl:otherwise>
								  <xsl:element name="FARE"><xsl:value-of select="TAS"/></xsl:element>
							</xsl:otherwise>					
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<!-- Se c'e' solo importo preaffrancato il PERC e' uguale a 0 (ho gia' pagato con la preaffrancatura e non incasso niente) -->
						<xsl:if test="string-length(./IMPP) != 0">
							<xsl:element name="PERC">0</xsl:element>
							<xsl:choose>    
								<xsl:when test="string-length(./TAS) != 0">
									<xsl:element name="FARE"><xsl:value-of select="TAS + IMPP"/></xsl:element>
								</xsl:when>
								<xsl:otherwise>
									  <xsl:element name="FARE"><xsl:value-of select="IMPP"/></xsl:element>
								</xsl:otherwise>					
							</xsl:choose>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>					
				<xsl:element name="ACURR"><xsl:value-of select="$gExtPacchiCurr"/></xsl:element>	
				<xsl:element name="REGT"><xsl:value-of select="$gExtPacchiRegt"/></xsl:element>
				<!-- La gestione dei Servizi Aggiuntivi e' delegata ad una classe Java ad hoc. -->
				
				<xsl:if test="string-length(SA/text())!=0">
					<xsl:element name="SA"><xsl:value-of select="p1ExtFun:convertSA(SA)"/></xsl:element>		
				</xsl:if>
				<xsl:if test="string-length(P/text())!=0">
					<xsl:element name="W"><xsl:value-of select="P"/></xsl:element>			
				</xsl:if>							
			</xsl:element>
			<xsl:element name="REG">
				<xsl:if test="string-length(ASSIC/text())!=0">
					<xsl:element name="INSVAL"><xsl:value-of select="ASSIC"/></xsl:element>
				</xsl:if>
				<xsl:if test="string-length(CNTRS/text())!=0">
					<xsl:element name="CODVAL"><xsl:value-of select="CNTRS"/></xsl:element>
				</xsl:if>				
				<xsl:element name="RCURR"><xsl:value-of select="$gExtPacchiCurr"/></xsl:element>
				<!--Da ICD CdG -->
				<xsl:if test="string-length(DESTR/text())!=0">
					<xsl:element name="ADDR"><xsl:value-of select="substring(DESTR, 1, 20)"/></xsl:element>
				</xsl:if>				
				<xsl:if test="string-length(ADDRS/text())!=0">					
					<xsl:element name="DADDR"><xsl:value-of select="substring(ADDRS, 1, 100)"/></xsl:element>
				</xsl:if>				
				<xsl:if test="string-length(DESTZ/text())!=0">
					<xsl:element name="DEST"><xsl:value-of select="substring(DESTZ, 1, 100)"/></xsl:element>
				</xsl:if>
				<xsl:if test="string-length(VARTR/text())!=0">								
					<xsl:element name="VARTAR"><xsl:value-of select="VARTR"/></xsl:element>
				</xsl:if>
				<xsl:if test="string-length(NAZ/text())!=0">
					<xsl:element name="NAZ"><xsl:value-of select="NAZ"/></xsl:element>
				</xsl:if>
				<!--Da ICD CdG -->
				<xsl:if test="string-length(STMAF/text())!=0">
					<xsl:choose>
						<xsl:when test="STMAF/text() = 'AFUP'">
							<xsl:element name="MODPAG"><xsl:value-of select="STMAF"/></xsl:element>
						</xsl:when>
						<xsl:otherwise>
							<xsl:element name="MODPAG">FRAN</xsl:element>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!-- TAG P1 -->   
	<xsl:template mode="P1Template" match="P1">
	 	<xsl:element name="M1">
	 		<xsl:apply-templates select="ACC" mode="ACCTemplate" />		
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
