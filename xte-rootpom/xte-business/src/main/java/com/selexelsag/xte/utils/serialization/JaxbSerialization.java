package com.selexelsag.xte.utils.serialization;

import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


/**
 * Classe di utilita' per operazioni su XML e binding JAXB.
 * @author Frosi
 *
 */
public class JaxbSerialization{



	/**
	 * Metodo per trasformare un file xml in una classe jaxb
	 * @param <T> 
	 * @param jaxbClass classe JAXB atta a rappresentare l'xml in input
	 * @param inputStream xml da trasformare
	 * @return la rappresentazione in oggetto dell'xml in input
	 * @throws JAXBException
	 * @throws ClassCastException
	 */
	@SuppressWarnings("unchecked")
	public static <T> T unmarshalXml( Class<T> jaxbClass, InputStream inputStream )
	throws JAXBException, ClassCastException {
		String packageName = jaxbClass.getPackage().getName();
		JAXBContext jc = JAXBContext.newInstance( packageName );
		Unmarshaller u = jc.createUnmarshaller();
		T doc = (T)u.unmarshal(inputStream);
		return doc;
	}

	/**
	 * Metodo per trasformare una classe jaxb in una string contenente l'xml associato
	 * @param <T>
	 * @param jaxbClass classe JAXB rappresentante l'xml da produrre
	 * @param object oggetto da serializzare
	 * @return
	 * @throws Throwable
	 */
	@SuppressWarnings("unchecked")
	public static <T> String serializeJaxbClass(Class<T> jaxbClass, Object object) throws Throwable{
		JAXBContext context = JAXBContext.newInstance(jaxbClass);
		StringWriter sw = new StringWriter();
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		T input = (T)object;
		marshaller.marshal(input, sw);
		return sw.toString();
	}

}
