package com.elsagdatamat.dbtrace.bridge.engine.dao;


import com.elsagdatamat.dbtrace.bridge.engine.entities.ModPagContrPiSda;
import com.elsagdatamat.dbtrace.bridge.engine.entities.ModPagContrPiSdaPK;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
/** 
* R.L. BeanGenerator - 22.05.2012 03:10:16
*/ 
public class ModPagContrPiSdaDAO extends SpringHibernateJpaDAOBase<ModPagContrPiSda,ModPagContrPiSdaPK> implements IModPagContrPiSdaDAO{

}