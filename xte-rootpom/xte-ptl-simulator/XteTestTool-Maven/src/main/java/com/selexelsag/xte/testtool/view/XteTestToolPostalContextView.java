/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolTabbedUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import com.selexelsag.xte.testtool.view.objectmodel.XteListSelectionModel;
import com.selexelsag.xte.testtool.view.objectrenderer.XtePostalEventsListRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolPostalContextView extends XteTestToolPanelView{
    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolPostalContextView.class);
    private DefaultListModel postaRegistrataListModel = new DefaultListModel();
    private DefaultListModel dataPostaListModel = new DefaultListModel();
    private DefaultListModel acquisizioneMazzettiListModel = new DefaultListModel();
    private DefaultListModel accettazioneOnlineListModel = new DefaultListModel();
    
    public XteTestToolPostalContextView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try {
            logger.info("XteTestToolPostalContextView start class creation");
            this.messages = ResourceBundle.getBundle("bundles/messages");
            initComponents();
            logger.info("XteTestToolPostalContextView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolPostalContextView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Xte Test Tool Home view creation failed", ex);
        }
    }

    public JPanel getPostalContextPanelRef(){ return this; }    

    public JTabbedPane getPostalContextTabbedPaneRef(){ return this.postalContextjTabbedPane; }
    
    public JPanel getPostaRegistrataPanelRef(){ return this.postaRegistratajPanel; }    
    public JLabel getPostaRegistrataTitleLabelRef(){ return this.postaRegistrataTitlejLabel; }
    public JList getPostaRegistrataEventsListRef(){ return this.postaRegistrataEventsjList; }
    
    public JPanel getDataPostaPanelRef(){ return this.dataPostajPanel; }    
    public JLabel getDataPostaTitleLabelRef(){ return this.dataPostaTitlejLabel; }
    public JList getDataPostaEventsListRef(){ return this.dataPostaEventsjList; }

    public JPanel getAcquisizioneMazzettiPanelRef(){ return this.acquisizioneMazzettijPanel; }    
    public JLabel getAcquisizioneMazzettiTitleLabelRef(){ return this.acquisizioneMazzettiTitlejLabel; }
    public JList getAcquisizioneMazzettiEventsListRef(){ return this.acquisizioneMazzettiEventsjList; }

    public JPanel getAccettazioneOnlinePanelRef(){ return this.accettazioneOnlinejPanel; }    
    public JLabel getAccettazioneOnlineTitleLabelRef(){ return this.accettazioneOnlineTitlejLabel; }
    public JList getAccettazioneOnlineEventsListRef(){ return this.accettazioneOnlineEventsjList; }

    public JButton getOkButtonRef(){ return this.okjButton; }
    public JButton getHomeButtonRef(){ return this.homejButton; }
    public JButton getExitButtonRef(){ return this.exitjButton; }
    public JTextPane getMessageTextPaneRef(){ return this.messageBarjTextPane; }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolPostalContextView start customizeView");

            //CUSTOMIZE TITLE
            this.postaRegistrataTitlejLabel .setText(messages.getString("postalcontext.view.postaregistrata.title.label"));
            this.dataPostaTitlejLabel.setText(messages.getString("postalcontext.view.dataposta.title.label"));
            this.acquisizioneMazzettiTitlejLabel .setText(messages.getString("postalcontext.view.acquisizionemazzetti.title.label"));
            this.accettazioneOnlineTitlejLabel.setText(messages.getString("postalcontext.view.accettazioneonline.title.label"));
            
            //CUSTOMIZE BUTTON
            this.okjButton.setText(messages.getString("view.ok.button"));
            this.homejButton.setText(messages.getString("view.home.button"));
            this.exitjButton.setText(messages.getString("view.exit.button"));

            logger.info("XteTestToolPostalContextView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolPostalContextView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Xte Test Tool Home List view customization failed", ex);
        }
    }

    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolPostalContextView start initializeView");

//            buildComponentSortedMap();
//
//            this.setFocusCycleRoot(true);
//            this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

            JLabel cornelLabel = new JLabel("");
            cornelLabel.setOpaque(true);
            cornelLabel.setBackground(new Color(240, 240, 240));

            
            postalContextjTabbedPane.setUI(new XteTestToolTabbedUI());
            
            //POSTA REGISTRATA
            this.postaRegistratajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.postaRegistratajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.postaRegistratajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.postaRegistratajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.postaRegistratajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            postaRegistrataEventsjList.setSelectionModel(new XteListSelectionModel());
            postaRegistrataEventsjList.setModel(postaRegistrataListModel);
            postaRegistrataEventsjList.setCellRenderer(new XtePostalEventsListRenderer());

            //DATA POSTA
            this.dataPostajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.dataPostajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.dataPostajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.dataPostajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.dataPostajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            dataPostaEventsjList.setSelectionModel(new XteListSelectionModel());
            dataPostaEventsjList.setModel(dataPostaListModel);
            dataPostaEventsjList.setCellRenderer(new XtePostalEventsListRenderer());

            //ACQUISIZIONE MAZZETTI
            this.acquisizioneMazzettijScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.acquisizioneMazzettijScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.acquisizioneMazzettijScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.acquisizioneMazzettijScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.acquisizioneMazzettijScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            acquisizioneMazzettiEventsjList.setSelectionModel(new XteListSelectionModel());
            acquisizioneMazzettiEventsjList.setModel(acquisizioneMazzettiListModel);
            acquisizioneMazzettiEventsjList.setCellRenderer(new XtePostalEventsListRenderer());

            //ACCETTAZIONE ONLINE
            this.accettazioneOnlinejScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.accettazioneOnlinejScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.accettazioneOnlinejScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.accettazioneOnlinejScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.accettazioneOnlinejScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            accettazioneOnlineEventsjList.setSelectionModel(new XteListSelectionModel());
            accettazioneOnlineEventsjList.setModel(accettazioneOnlineListModel);
            accettazioneOnlineEventsjList.setCellRenderer(new XtePostalEventsListRenderer());

            logger.info("XteTestToolPostalContextView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolPostalContextView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Xte Test Tool Home List view initialization failed", ex);
        }
    }

    @Override
    public void restoreView() {
        logger.debug("XteTestToolPostalContextView restoreView");
        this.getExitButtonRef().doClick();
    }

    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.postaRegistrataEventsjList);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        commandjPanel = new javax.swing.JPanel();
        okjButton = new javax.swing.JButton();
        messageBarjScrollPane = new javax.swing.JScrollPane();
        messageBarjTextPane = new javax.swing.JTextPane();
        homejButton = new javax.swing.JButton();
        exitjButton = new javax.swing.JButton();
        postalContextjTabbedPane = new javax.swing.JTabbedPane();
        postaRegistratajPanel = new javax.swing.JPanel();
        postaRegistrataTitlejLabel = new javax.swing.JLabel();
        postaRegistratajScrollPane = new javax.swing.JScrollPane();
        postaRegistrataEventsjList = new javax.swing.JList();
        dataPostajPanel = new javax.swing.JPanel();
        dataPostaTitlejLabel = new javax.swing.JLabel();
        dataPostajScrollPane = new javax.swing.JScrollPane();
        dataPostaEventsjList = new javax.swing.JList();
        acquisizioneMazzettijPanel = new javax.swing.JPanel();
        acquisizioneMazzettiTitlejLabel = new javax.swing.JLabel();
        acquisizioneMazzettijScrollPane = new javax.swing.JScrollPane();
        acquisizioneMazzettiEventsjList = new javax.swing.JList();
        accettazioneOnlinejPanel = new javax.swing.JPanel();
        accettazioneOnlineTitlejLabel = new javax.swing.JLabel();
        accettazioneOnlinejScrollPane = new javax.swing.JScrollPane();
        accettazioneOnlineEventsjList = new javax.swing.JList();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setMaximumSize(new java.awt.Dimension(1000, 690));
        setMinimumSize(new java.awt.Dimension(1000, 690));
        setPreferredSize(new java.awt.Dimension(1000, 690));

        commandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        commandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        commandjPanel.setMaximumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setMinimumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setPreferredSize(new java.awt.Dimension(980, 80));

        okjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        okjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/OK.png"))); // NOI18N
        okjButton.setText("OK");
        okjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        okjButton.setFocusPainted(false);
        okjButton.setFocusable(false);
        okjButton.setForeground(new java.awt.Color(51, 51, 51));
        okjButton.setIconTextGap(5);
        okjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        okjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        okjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        messageBarjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        messageBarjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageBarjScrollPane.setMaximumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setMinimumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setPreferredSize(new java.awt.Dimension(350, 50));

        messageBarjTextPane.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        messageBarjTextPane.setFocusable(false);
        messageBarjScrollPane.setViewportView(messageBarjTextPane);

        homejButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        homejButton.setForeground(new java.awt.Color(51, 51, 51));
        homejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Home3.png"))); // NOI18N
        homejButton.setText("<html><body>&nbsp;Home<body><html>");
        homejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        homejButton.setFocusPainted(false);
        homejButton.setFocusable(false);
        homejButton.setIconTextGap(5);
        homejButton.setMaximumSize(new java.awt.Dimension(100, 50));
        homejButton.setMinimumSize(new java.awt.Dimension(100, 50));
        homejButton.setPreferredSize(new java.awt.Dimension(100, 50));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("<html><body>&nbsp;Exit<body><html>");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setFocusPainted(false);
        exitjButton.setFocusable(false);
        exitjButton.setIconTextGap(5);
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout commandjPanelLayout = new javax.swing.GroupLayout(commandjPanel);
        commandjPanel.setLayout(commandjPanelLayout);
        commandjPanelLayout.setHorizontalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(okjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 274, Short.MAX_VALUE)
                .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandjPanelLayout.setVerticalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(okjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        postalContextjTabbedPane.setBackground(new java.awt.Color(255, 255, 255));
        postalContextjTabbedPane.setForeground(new java.awt.Color(0, 51, 204));
        postalContextjTabbedPane.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        postalContextjTabbedPane.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        postalContextjTabbedPane.setMaximumSize(new java.awt.Dimension(980, 580));
        postalContextjTabbedPane.setMinimumSize(new java.awt.Dimension(980, 580));
        postalContextjTabbedPane.setOpaque(true);
        postalContextjTabbedPane.setPreferredSize(new java.awt.Dimension(980, 580));

        postaRegistratajPanel.setBackground(new java.awt.Color(230, 250, 0));
        postaRegistratajPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        postaRegistrataTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        postaRegistrataTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        postaRegistrataTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        postaRegistrataTitlejLabel.setText("EVENTI POSTA REGISTRATA");
        postaRegistrataTitlejLabel.setFocusable(false);
        postaRegistrataTitlejLabel.setIconTextGap(30);
        postaRegistrataTitlejLabel.setMaximumSize(new java.awt.Dimension(950, 20));
        postaRegistrataTitlejLabel.setMinimumSize(new java.awt.Dimension(950, 20));
        postaRegistrataTitlejLabel.setPreferredSize(new java.awt.Dimension(950, 20));

        postaRegistratajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        postaRegistratajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        postaRegistratajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        postaRegistratajScrollPane.setMaximumSize(new java.awt.Dimension(700, 440));
        postaRegistratajScrollPane.setMinimumSize(new java.awt.Dimension(700, 440));
        postaRegistratajScrollPane.setPreferredSize(new java.awt.Dimension(700, 440));

        postaRegistrataEventsjList.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        postaRegistrataEventsjList.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        postaRegistrataEventsjList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Acquisizione Configurazione", "Notifica Mazzetti", "Acquisizione Mazzetti", "Richieste Massive", "Lascia Mazzetti", "Lascia Oggetti", "Consegna Mazzetti", "Web Service" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        postaRegistrataEventsjList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        postaRegistratajScrollPane.setViewportView(postaRegistrataEventsjList);

        javax.swing.GroupLayout postaRegistratajPanelLayout = new javax.swing.GroupLayout(postaRegistratajPanel);
        postaRegistratajPanel.setLayout(postaRegistratajPanelLayout);
        postaRegistratajPanelLayout.setHorizontalGroup(
            postaRegistratajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(postaRegistratajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(postaRegistrataTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, postaRegistratajPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(postaRegistratajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(134, 134, 134))
        );
        postaRegistratajPanelLayout.setVerticalGroup(
            postaRegistratajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(postaRegistratajPanelLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(postaRegistrataTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(postaRegistratajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        postalContextjTabbedPane.addTab("WS Posta Registrata", new javax.swing.ImageIcon(getClass().getResource("/images/Posta Registrata1.png")), postaRegistratajPanel); // NOI18N

        dataPostajPanel.setBackground(new java.awt.Color(230, 250, 0));
        dataPostajPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        dataPostaTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        dataPostaTitlejLabel.setForeground(new java.awt.Color(0, 51, 204));
        dataPostaTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        dataPostaTitlejLabel.setText("EVENTI DATA POSTA");
        dataPostaTitlejLabel.setFocusable(false);
        dataPostaTitlejLabel.setIconTextGap(30);
        dataPostaTitlejLabel.setMaximumSize(new java.awt.Dimension(950, 20));
        dataPostaTitlejLabel.setMinimumSize(new java.awt.Dimension(950, 20));
        dataPostaTitlejLabel.setPreferredSize(new java.awt.Dimension(950, 20));

        dataPostajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        dataPostajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        dataPostajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        dataPostajScrollPane.setMaximumSize(new java.awt.Dimension(700, 440));
        dataPostajScrollPane.setMinimumSize(new java.awt.Dimension(700, 440));
        dataPostajScrollPane.setPreferredSize(new java.awt.Dimension(700, 440));

        dataPostaEventsjList.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        dataPostaEventsjList.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        dataPostaEventsjList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Notifica Mazzetti", "Acquisizione Oggetti", "Acquisizione Mazzetti", "Pronto per il Recapito", "Item da Oracle", "Subentra Mazzetto", "Lascia Mazzetto", "Web Service" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        dataPostaEventsjList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dataPostajScrollPane.setViewportView(dataPostaEventsjList);

        javax.swing.GroupLayout dataPostajPanelLayout = new javax.swing.GroupLayout(dataPostajPanel);
        dataPostajPanel.setLayout(dataPostajPanelLayout);
        dataPostajPanelLayout.setHorizontalGroup(
            dataPostajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dataPostajPanelLayout.createSequentialGroup()
                .addGroup(dataPostajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dataPostajPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(dataPostaTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(dataPostajPanelLayout.createSequentialGroup()
                        .addGap(133, 133, 133)
                        .addComponent(dataPostajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        dataPostajPanelLayout.setVerticalGroup(
            dataPostajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dataPostajPanelLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(dataPostaTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(dataPostajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
        );

        postalContextjTabbedPane.addTab("WS Data Posta", new javax.swing.ImageIcon(getClass().getResource("/images/Distribuzione4.png")), dataPostajPanel); // NOI18N

        acquisizioneMazzettijPanel.setBackground(new java.awt.Color(230, 250, 0));
        acquisizioneMazzettijPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        acquisizioneMazzettiTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        acquisizioneMazzettiTitlejLabel.setForeground(new java.awt.Color(0, 51, 204));
        acquisizioneMazzettiTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        acquisizioneMazzettiTitlejLabel.setText("EVENTI ACQUISIZIONE MAZZETTI PR");
        acquisizioneMazzettiTitlejLabel.setFocusable(false);
        acquisizioneMazzettiTitlejLabel.setIconTextGap(30);
        acquisizioneMazzettiTitlejLabel.setMaximumSize(new java.awt.Dimension(950, 20));
        acquisizioneMazzettiTitlejLabel.setMinimumSize(new java.awt.Dimension(950, 20));
        acquisizioneMazzettiTitlejLabel.setPreferredSize(new java.awt.Dimension(950, 20));

        acquisizioneMazzettijScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        acquisizioneMazzettijScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        acquisizioneMazzettijScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        acquisizioneMazzettijScrollPane.setMaximumSize(new java.awt.Dimension(700, 440));
        acquisizioneMazzettijScrollPane.setMinimumSize(new java.awt.Dimension(700, 440));
        acquisizioneMazzettijScrollPane.setPreferredSize(new java.awt.Dimension(700, 440));

        acquisizioneMazzettiEventsjList.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        acquisizioneMazzettiEventsjList.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        acquisizioneMazzettiEventsjList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Genera Mazzetti" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        acquisizioneMazzettiEventsjList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        acquisizioneMazzettijScrollPane.setViewportView(acquisizioneMazzettiEventsjList);

        javax.swing.GroupLayout acquisizioneMazzettijPanelLayout = new javax.swing.GroupLayout(acquisizioneMazzettijPanel);
        acquisizioneMazzettijPanel.setLayout(acquisizioneMazzettijPanelLayout);
        acquisizioneMazzettijPanelLayout.setHorizontalGroup(
            acquisizioneMazzettijPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(acquisizioneMazzettijPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(acquisizioneMazzettiTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, acquisizioneMazzettijPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(acquisizioneMazzettijScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(136, 136, 136))
        );
        acquisizioneMazzettijPanelLayout.setVerticalGroup(
            acquisizioneMazzettijPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(acquisizioneMazzettijPanelLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(acquisizioneMazzettiTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(acquisizioneMazzettijScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
        );

        postalContextjTabbedPane.addTab("Acquisizione Mazzetti PR", new javax.swing.ImageIcon(getClass().getResource("/images/Acquisizione2.png")), acquisizioneMazzettijPanel); // NOI18N

        accettazioneOnlinejPanel.setBackground(new java.awt.Color(230, 250, 0));
        accettazioneOnlinejPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        accettazioneOnlineTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        accettazioneOnlineTitlejLabel.setForeground(new java.awt.Color(0, 51, 204));
        accettazioneOnlineTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        accettazioneOnlineTitlejLabel.setText("EVENTI ACCETTAZIONE ONLINE");
        accettazioneOnlineTitlejLabel.setFocusable(false);
        accettazioneOnlineTitlejLabel.setIconTextGap(30);
        accettazioneOnlineTitlejLabel.setMaximumSize(new java.awt.Dimension(950, 20));
        accettazioneOnlineTitlejLabel.setMinimumSize(new java.awt.Dimension(950, 20));
        accettazioneOnlineTitlejLabel.setPreferredSize(new java.awt.Dimension(950, 20));

        accettazioneOnlinejScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        accettazioneOnlinejScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        accettazioneOnlinejScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        accettazioneOnlinejScrollPane.setMaximumSize(new java.awt.Dimension(700, 440));
        accettazioneOnlinejScrollPane.setMinimumSize(new java.awt.Dimension(700, 440));
        accettazioneOnlinejScrollPane.setPreferredSize(new java.awt.Dimension(700, 440));

        accettazioneOnlineEventsjList.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        accettazioneOnlineEventsjList.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        accettazioneOnlineEventsjList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Acquisizione Configurazione", "Pre-Accettazione", "Web Service" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        accettazioneOnlineEventsjList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        accettazioneOnlinejScrollPane.setViewportView(accettazioneOnlineEventsjList);

        javax.swing.GroupLayout accettazioneOnlinejPanelLayout = new javax.swing.GroupLayout(accettazioneOnlinejPanel);
        accettazioneOnlinejPanel.setLayout(accettazioneOnlinejPanelLayout);
        accettazioneOnlinejPanelLayout.setHorizontalGroup(
            accettazioneOnlinejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(accettazioneOnlinejPanelLayout.createSequentialGroup()
                .addGroup(accettazioneOnlinejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(accettazioneOnlinejPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(accettazioneOnlineTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(accettazioneOnlinejPanelLayout.createSequentialGroup()
                        .addGap(136, 136, 136)
                        .addComponent(accettazioneOnlinejScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        accettazioneOnlinejPanelLayout.setVerticalGroup(
            accettazioneOnlinejPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(accettazioneOnlinejPanelLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(accettazioneOnlineTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(accettazioneOnlinejScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );

        postalContextjTabbedPane.addTab("Accettazione Online", new javax.swing.ImageIcon(getClass().getResource("/images/Accettazione2.png")), accettazioneOnlinejPanel); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(postalContextjTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(postalContextjTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList accettazioneOnlineEventsjList;
    private javax.swing.JLabel accettazioneOnlineTitlejLabel;
    private javax.swing.JPanel accettazioneOnlinejPanel;
    private javax.swing.JScrollPane accettazioneOnlinejScrollPane;
    private javax.swing.JList acquisizioneMazzettiEventsjList;
    private javax.swing.JLabel acquisizioneMazzettiTitlejLabel;
    private javax.swing.JPanel acquisizioneMazzettijPanel;
    private javax.swing.JScrollPane acquisizioneMazzettijScrollPane;
    private javax.swing.JPanel commandjPanel;
    private javax.swing.JList dataPostaEventsjList;
    private javax.swing.JLabel dataPostaTitlejLabel;
    private javax.swing.JPanel dataPostajPanel;
    private javax.swing.JScrollPane dataPostajScrollPane;
    private javax.swing.JButton exitjButton;
    private javax.swing.JButton homejButton;
    private javax.swing.JScrollPane messageBarjScrollPane;
    private javax.swing.JTextPane messageBarjTextPane;
    private javax.swing.JButton okjButton;
    private javax.swing.JList postaRegistrataEventsjList;
    private javax.swing.JLabel postaRegistrataTitlejLabel;
    private javax.swing.JPanel postaRegistratajPanel;
    private javax.swing.JScrollPane postaRegistratajScrollPane;
    private javax.swing.JTabbedPane postalContextjTabbedPane;
    // End of variables declaration//GEN-END:variables


}
