package com.selexelsag.xte.persistence;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.selexelsag.xte.persistence.dao.AdditionalServicesDAO;
import com.selexelsag.xte.persistence.dao.AsyncMessagesDAO;
import com.selexelsag.xte.persistence.dao.ProductsDAO;
import com.selexelsag.xte.persistence.entities.AdditionalServices;
import com.selexelsag.xte.persistence.entities.AsyncMessages;
import com.selexelsag.xte.persistence.exceptions.PersistenceException;

public class XteMessagesDBAccess 
{
	private static Logger logger = LogManager.getLogger(XteMessagesDBAccess.class);
	
	public static final String BEAN_NAME = "messagesDbAccess";
	
	@Autowired
	private AsyncMessagesDAO asyncMessagesDAO;
	public AsyncMessagesDAO getAsyncMessagesDAO() {return asyncMessagesDAO;}
	public void setAsyncMessagesDAO(AsyncMessagesDAO asyncMessagesDAO) {this.asyncMessagesDAO = asyncMessagesDAO;}

	@Autowired
	private AdditionalServicesDAO additionalServicesDAO;
	public AdditionalServicesDAO getAdditionalServicesDAO() {return additionalServicesDAO;}
	public void setAdditionalServicesDAO(AdditionalServicesDAO additionalServicesDAO) {this.additionalServicesDAO = additionalServicesDAO;}

	@Autowired
	private ProductsDAO productsDAO;
	public ProductsDAO getProductsDAO() {return productsDAO;}
	public void setProductsDAO(ProductsDAO productsDAO) {this.productsDAO = productsDAO;}
	
	private static String getLockId() 
	{
		StringBuilder sb = new StringBuilder();
		try 
		{
			sb.append(InetAddress.getLocalHost().getHostName() + "@" + InetAddress.getLocalHost().getHostAddress() + " - ");
		}
		catch (UnknownHostException e) 
		{
			sb.append("UNKNOWN - ");
			logger.error("Unable to identify address", e);
		}
		sb.append(SystemUtils.OS_NAME + "." + SystemUtils.OS_VERSION);
		
		return sb.toString();
	}
	
	@Transactional
	public List<AsyncMessages> getMessages(String flowId, int maxResults) throws PersistenceException 
	{
		// ripulisco il db da eventuali vecchi blocchi
		int stalledMessages = asyncMessagesDAO.freeStalledMessagesId(flowId);
		logger.debug("Liberati " + stalledMessages + " record bloccati");
		String name = getLockId();
		
		List<AsyncMessages> lockedList = asyncMessagesDAO.getLockedIdList(flowId, name, maxResults);
		
		return lockedList;
	}
	
	public Character getCdGCode(char saCode) throws PersistenceException
	{
		return additionalServicesDAO.findByPerif(saCode);
	}
	
	public List<AdditionalServices> getAdditionalServices() throws PersistenceException
	{
		return additionalServicesDAO.findAll();
	}
	
	public Boolean getIsAPack(String product) throws PersistenceException 
	{
		return productsDAO.findByPType(product);
	}
	
	public void saveMessage(AsyncMessages message) throws PersistenceException 
	{
		Timestamp now = new Timestamp(System.currentTimeMillis());
		logger.debug("saveMessage at " + now);
		message.setReceivedTimestamp(now);
		logger.debug("persisting: " + message);
		// asyncMessagesDAO.persist(message);
		asyncMessagesDAO.attachDirty(message);
	}
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public void updateMessages(AsyncMessages messages) throws PersistenceException 
	{
		logger.debug("updateMessage");
		/* decommentare per eliminare l'informazione
		 * sull'ultima istanza ad aver lavorato sul messaggio
		messages.setLockId(null);
		 */
		messages.setLockTimestamp(null);
		asyncMessagesDAO.merge(messages);
		asyncMessagesDAO.flush();
	}
	
	@Transactional(propagation = Propagation.SUPPORTS)
	public void deleteMessages(AsyncMessages messages) throws PersistenceException 
	{
		asyncMessagesDAO.delete(messages);
		asyncMessagesDAO.flush();
	}
}
