package com.selexelsag.xte.extension.probing.servlet.exception;

public class ProbeInvalidRequestException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2782299665953553516L;

	public ProbeInvalidRequestException(String message){
			super(message);
		}
		
		
}
