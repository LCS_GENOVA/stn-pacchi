/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectrenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Hashtable;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Da Procida
 */
public class XteTableHeaderButtonRenderer extends JButton implements TableCellRenderer {

    private List<String> columnWidthValues;
    private ImageIcon sortDescendingIcon = new ImageIcon(getClass().getResource("/images/SortDescending3.png"));
    private ImageIcon sortAscendingIcon = new ImageIcon(getClass().getResource("/images/SortAscending3.png"));
    
    public static final int NONE = 0;
    public static final int DOWN = 1;
    public static final int UP = 2;
    int pushedColumn;
    Hashtable state;
    JButton noneButton, downButton, upButton;

    public XteTableHeaderButtonRenderer(List<String> initialColumnWidthValues) {

        this.columnWidthValues = initialColumnWidthValues;
        pushedColumn = -1;
        state = new Hashtable();

        setMargin(new Insets(0, 0, 0, 0));
        setHorizontalTextPosition(LEFT);
        setIcon(null);
        setFont(new Font("Tahoma", Font.BOLD, 14));
        setBorder(UIManager.getBorder("TableHeader.cellBorder"));
        setPreferredSize(new Dimension(getWidth(), 22));

        noneButton = new JButton();
        noneButton.setMargin(new Insets(0, 0, 0, 0));
        noneButton.setHorizontalTextPosition(LEFT);
        noneButton.setIcon(null);
        noneButton.setPressedIcon(null);
        noneButton.setFont(new Font("Tahoma", Font.BOLD, 14));
        noneButton.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
        noneButton.setPreferredSize(new Dimension(noneButton.getWidth(), 22));

        downButton = new JButton();
        downButton.setMargin(new Insets(0, 0, 0, 0));
        downButton.setHorizontalTextPosition(LEFT);
        downButton.setIcon(new ImageIcon(getClass().getResource("/images/SortAscending3.png")));
        downButton.setPressedIcon(null);
        downButton.setFont(new Font("Tahoma", Font.BOLD, 14));
        downButton.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
        downButton.setPreferredSize(new Dimension(noneButton.getWidth(), 22));

        upButton = new JButton();
        upButton.setMargin(new Insets(0, 0, 0, 0));
        upButton.setHorizontalTextPosition(LEFT);
        upButton.setIcon(new ImageIcon(getClass().getResource("/images/SortDescending3.png")));
        upButton.setPressedIcon(null);
        upButton.setFont(new Font("Tahoma", Font.BOLD, 14));
        upButton.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
        upButton.setPreferredSize(new Dimension(noneButton.getWidth(), 22));
        
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

        defineTableFocus(table);

        for (int i = 0; i < table.getModel().getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setMinWidth(Integer.parseInt(columnWidthValues.get(i)));
            table.getColumnModel().getColumn(i).setMaxWidth(Integer.parseInt(columnWidthValues.get(i)));
            table.getColumnModel().getColumn(i).setPreferredWidth(Integer.parseInt(columnWidthValues.get(i)));
        }

        JButton button = this;
        Object obj = state.get(new Integer(col));
        if (obj != null) {
            if (((Integer) obj).intValue() == DOWN) {
                button = downButton;
            } else if(((Integer) obj).intValue() == UP) {
                button = upButton;
            } else {
                button = noneButton;
            }
        }
        button.setText((value == null) ? "" : value.toString());
        boolean isPressed = (col == pushedColumn);
        button.getModel().setPressed(isPressed);
        button.getModel().setArmed(isPressed);        

        if(table.isEnabled())
        {
            button.setBackground(new Color(220,220, 255));
            button.setForeground(new Color(51,51,51));
        }
        else
        {
            button.setBackground(new Color(240,240,240));
            button.setForeground(new Color(204,204,204));
        }

        return button;
    }

    public void setPressedColumn(int col) {
        pushedColumn = col;
    }

    public void setSelectedColumn(int col) {
        if (col < 0) {
            return;
        }
        Integer value = null;
        Object obj = state.get(new Integer(col));
        if (obj == null) {
            value = new Integer(DOWN);
        } else {
            if (((Integer) obj).intValue() == DOWN) {
                value = new Integer(UP);
            } else {
                value = new Integer(DOWN);
            }
        }
        state.clear();
        state.put(new Integer(col), value);
    }

    public int getState(int col) {
        int retValue;
        Object obj = state.get(new Integer(col));
        if (obj == null) {
            retValue = NONE;
        } else {
            if (((Integer) obj).intValue() == DOWN) {
                retValue = DOWN;
            } else {
                retValue = UP;
            }
        }
        return retValue;
    }

    private void defineTableFocus(JTable table)
    {
        Action tabOut = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
            }
        };

        Action stabOut = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent();
            }
        };

        table.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabOut");
        table.getActionMap().put("tabOut",tabOut);

        table.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, java.awt.event.InputEvent.SHIFT_DOWN_MASK ), "stabOut");
        table.getActionMap().put("stabOut",stabOut);


    }
	
    
}
