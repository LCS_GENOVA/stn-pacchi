package com.selexelsag.dbtrace.bridge.to.omp.message.trans;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;
import org.xml.sax.SAXException;

import com.selexelsag.dbtrace.bridge.to.omp.manager.OMPManagerInterface;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1.DHDR;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1.MOBJ;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1.MOBJ.ACC;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1.MOBJ.OBJ;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1.MOBJ.PAC;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.D1.MOBJ.REG;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.MSG.HDR;
import com.selexelsag.dbtrace.bridge.to.omp.message.map.ObjectFactory;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;


public class Transformations {
	
	private static final Log log = LogFactory.getLog(Transformations.class);
	public static SimpleDateFormat XMLDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	private OMPManagerInterface ompManager;
	
	
	/**
	 * default constructor 
	 */
	public Transformations() {
		super();
	}

	/**
	 * construct Transformations object binding ompManager 
	 * @param ompManager  the OMPManagerInterface object to bind to
	 */
	public Transformations(OMPManagerInterface ompManager) {
		this.ompManager = ompManager;
	}
	
	/**
	 * set ompManager reference
	 * @param ompManager  the OMPManagerInterface object to set
	 */
	public void setOmpManager(OMPManagerInterface ompManager) {
		this.ompManager = ompManager;
	}

	/**
	 * create and fill OMP Jaxb MSG from WS List of Trace() objects
	 * @param poCode  the String value of postal office code
	 * @param tracesList the WS List<Trace> response object list
	 * @return a Jaxb OMP MSG object, filled with response values
	 */
	public MSG traceListToMessage(String poCode, List<Trace> tracesList) {
		
		log.info("Creating message for " + poCode + " ...");
		NullableObjectFactoryDecorator objFactory = new NullableObjectFactoryDecorator(new ObjectFactory());
		MSG ompMessage = objFactory.createMSG();
		
		Trace topTrace = tracesList.get(0);
		Map<String, String> topDetailsMap = loadDetailValues(topTrace);
		
		HDR msgHeader = objFactory.createMSGHDR();
		msgHeader.setOFCID("SDA01");  // in attesa di frazionario fittizio
		msgHeader.setSWREL("SDA01");

		DHDR dhdrHeader = objFactory.createMSGD1DHDR();
		dhdrHeader.setDID(padDIDCode(ompManager.getNextSequenceCode()));
		//dhdrHeader.setOFCOTH(topTrace.getIdForwardTo());  // get first trace ID		
		//dhdrHeader.setOFCOTH(D1PTransformationValues.WHERE_ID.map(topDetailsMap).value()); // temporary solution; remove when WS get correct data
		dhdrHeader.setOFCOTH(topTrace.getIdChannel());
		dhdrHeader.setCOURIER(objFactory.createMSGD1DHDRCOURIER(D1PTransformationValues.COURIER.map(topDetailsMap).value()));
		dhdrHeader.setMC(objFactory.createMSGD1DHDRMC(D1PTransformationValues.MC.map(topDetailsMap).value()));
		
		GregorianCalendar now = (GregorianCalendar)GregorianCalendar.getInstance();
		
		dhdrHeader.setDT(XMLDateFormat.format(now.getTime()));
		dhdrHeader.setOFNAME(D1PTransformationValues.OFNAME.map(topDetailsMap).value());
		
		D1 d1Container = objFactory.createMSGD1();
		d1Container.getDHDR().add(dhdrHeader);

		log.debug("Headers created; add " + tracesList.size() + " traces");

		for (Trace responseTrace : tracesList) {
			Map<String, String> traceDetailsMap = loadDetailValues(responseTrace);
			
			MOBJ mobjContainer = objFactory.createMSGD1MOBJ();
			
			OBJ objTrace = objFactory.createMSGD1MOBJOBJ();
			
			objTrace.setOBJID(responseTrace.getIdTracedEntity());
			objTrace.setPH("1");
			objTrace.setBTSF(objFactory.createMSGD1MOBJOBJBTSF(D1PTransformationValues.BTSF.map(traceDetailsMap).value()));
			objTrace.setSUBC(responseTrace.getLabelTracedEntity());
			objTrace.setFAWAIT(objFactory.createMSGD1MOBJOBJFAWAIT("1"));
			
			objTrace.setSCANTIME(objFactory.createMSGD1MOBJOBJSCANTIME(dateToStr(responseTrace.getWhenHappened(), XMLDateFormat)));
			
			ACC accTrace = createACCtrace(objFactory, traceDetailsMap);
			
			REG regTrace = createREGtrace(objFactory, traceDetailsMap);
			
			PAC pacTrace = createPACtrace(objFactory, traceDetailsMap);
			pacTrace.setSTATUS(objFactory.createMSGD1MOBJPACSTATUS(tryToFindIdStatusIfNull(responseTrace, traceDetailsMap)));

			mobjContainer.getOBJ().add(objTrace);
			mobjContainer.getACC().add(accTrace);
			mobjContainer.getREG().add(regTrace);
			mobjContainer.getPAC().add(pacTrace);
			
			d1Container.getMOBJ().add(mobjContainer);
		}

		log.debug("All traces added");

		ompMessage.getHDROrD1().add(msgHeader);
		ompMessage.getHDROrD1().add(d1Container);
		
		log.info("D1 message successfully created");
        return ompMessage;
    }

	/**
	 * create PAC sub-section Jaxb record
	 * @param objFactory  Jaxb objects creation factory
	 * @param details  Map of Trace details
	 * @return PAC filled with details values
	 */
	private PAC createPACtrace(ObjectFactory objFactory, Map<String, String> details) {
		PAC pacTrace = objFactory.createMSGD1MOBJPAC();
		
		pacTrace.setNUMMANCATACONSEGNA(objFactory.createMSGD1MOBJPACNUMMANCATACONSEGNA(D1PTransformationValues.NUM_MANCATA_CONSEGNA.map(details).value()));
		pacTrace.setFILIALESDA(objFactory.createMSGD1MOBJPACFILIALESDA(D1PTransformationValues.FILIALE_SDA.map(details).value()));
		pacTrace.setCONTOCORRENTEDEST(objFactory.createMSGD1MOBJPACCONTOCORRENTEDEST(D1PTransformationValues.CONTO_CORRENTE_DEST.map(details).value()));
		pacTrace.setNUMGIORNIGIACENZA(objFactory.createMSGD1MOBJPACNUMGIORNIGIACENZA(D1PTransformationValues.NUM_GIORNI_GIACENZA.map(details).value()));
		pacTrace.setCODISTR(objFactory.createMSGD1MOBJPACCODISTR(D1PTransformationValues.COD_ISTR.map(details).value()));
		pacTrace.setFLAGRRR(objFactory.createMSGD1MOBJPACFLAGRRR(D1PTransformationValues.FLAG_RRR.map(details).value()));
		pacTrace.setFLAGANN(objFactory.createMSGD1MOBJPACFLAGANN(D1PTransformationValues.FLAG_ANN.map(details).value()));
		pacTrace.setIMPDIRITTIPOSTALI(objFactory.createMSGD1MOBJPACIMPDIRITTIPOSTALI(D1PTransformationValues.IMP_DIRITTI_POSTALI.map(details).intValue()));
		pacTrace.setIMPONERIDOGANALI(objFactory.createMSGD1MOBJPACIMPONERIDOGANALI(D1PTransformationValues.IMP_ONERI_DOGANALI.map(details).intValue()));
		log.debug("... PAC section created");
		
		return pacTrace;
	}

	/**
	 * create REG sub-section Jaxb record
	 * @param objFactory  Jaxb objects creation factory
	 * @param details  Map of Trace details
	 * @return REG filled with details values
	 */
	private REG createREGtrace(ObjectFactory objFactory, Map<String, String> details) {
		REG regTrace = objFactory.createMSGD1MOBJREG();
		
		regTrace.setAOFCNAME(D1PTransformationValues.AOFCNAME.map(details).value());
		regTrace.setADEST(objFactory.createMSGD1MOBJREGADEST(D1PTransformationValues.DESTZ.map(details).value()));
		regTrace.setADDR(objFactory.createMSGD1MOBJREGADDR(D1PTransformationValues.DESTR.map(details).value()));
		regTrace.setDADDR(objFactory.createMSGD1MOBJREGDADDR(D1PTransformationValues.ADDRS.map(details).value()));
		regTrace.setSA(objFactory.createMSGD1MOBJREGSA(StringUtils.deleteAny(D1PTransformationValues.SA.map(details).value(),"<>")));
		regTrace.setREGT(objFactory.createMSGD1MOBJREGREGT(D1PTransformationValues.REGT.map(details).value()));
		regTrace.setCODVAL(objFactory.createMSGD1MOBJREGCODVAL(D1PTransformationValues.CNTRS.map(details).intValue()));
		regTrace.setINSVAL(objFactory.createMSGD1MOBJREGINSVAL(D1PTransformationValues.INSVAL.map(details).intValue()));
		regTrace.setRCURR(objFactory.createMSGD1MOBJREGRCURR(D1PTransformationValues.RCURR.map(details).value()));
		regTrace.setWB(objFactory.createMSGD1MOBJREGWB(D1PTransformationValues.WB.map(details).intValue()));
		regTrace.setWA(objFactory.createMSGD1MOBJREGWA(D1PTransformationValues.WA.map(details).intValue()));
		regTrace.setNSEALS(objFactory.createMSGD1MOBJREGNSEALS(D1PTransformationValues.NSEALS.map(details).intValue()));
		regTrace.setNCHECK(objFactory.createMSGD1MOBJREGNCHECK(D1PTransformationValues.NCHECK.map(details).intValue()));
		regTrace.setCUSTCODE(objFactory.createMSGD1MOBJREGCUSTCODE(D1PTransformationValues.CUSTCODE.map(details).value()));
		regTrace.setCODEAR(objFactory.createMSGD1MOBJREGCODEAR(D1PTransformationValues.CODEAR.map(details).value()));
		// L.C. 26/05/2015 R15.2 - Inizio
		String val = D1PTransformationValues.SANEW.map(details).value();
		if(val!=null && val.length()>0) {
			String saNew = ompManager.getBillModeCode(val);
			if(saNew!=null && saNew.length()>0) {
				regTrace.setSANEW(objFactory.createMSGD1MOBJREGSANEW(saNew));
			}
		}
		// L.C. 26/05/2015 R15.2 - Fine
		log.debug("... REG section created");

		/* Patch for solving defect ST_TTRACE-6364 */
		
		if (regTrace.getCODVAL() != null && regTrace.getCODVAL().getValue() != null) {
			regTrace.setRCURR(objFactory.createMSGD1MOBJREGRCURR("E"));
		}
		
		return regTrace;
	}

	/**
	 * create ACC sub-section Jaxb record
	 * @param objFactory  Jaxb objects creation factory
	 * @param details  Map of Trace details
	 * @return ACC filled with details values
	 */
	private ACC createACCtrace(ObjectFactory objFactory, Map<String, String> details) {
		ACC accTrace = objFactory.createMSGD1MOBJACC();
		
		accTrace.setAOFC(D1PTransformationValues.AOFC.map(details).value());
		accTrace.setADT(D1PTransformationValues.ADT.map(details).value());
		accTrace.setZIP(objFactory.createMSGD1MOBJACCZIP(D1PTransformationValues.ZIP_DIST.map(details).value()));
		accTrace.setPERC(objFactory.createMSGD1MOBJACCPERC(D1PTransformationValues.PERC.map(details).intValue()));
		accTrace.setFARE(objFactory.createMSGD1MOBJACCFARE(D1PTransformationValues.FARE.map(details).intValue()));
		accTrace.setACURR(objFactory.createMSGD1MOBJACCACURR(D1PTransformationValues.ACURR.map(details).value()));
		log.debug("... ACC section created");
		
		return accTrace;
	}
	
	/**
	 * unmarshal a formatted XML String and parse in an OMP Jaxb MSG, applying Schema validation if present
	 * @param xmlDocument the String XML formatted document source
	 * @param schema the javax.xml.validation.Schema if present, null otherwise
	 * @return Jaxb OMP MSG object
	 * @throws JAXBException
	 */
    public MSG unmarshallXmlToMessage(String xmlDocument, Schema schema) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(MSG.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        StringReader reader = new StringReader(xmlDocument);
        if (schema != null) 
        	unmarshaller.setSchema(schema);
        MSG unmarshalledMessage = (MSG) unmarshaller.unmarshal(reader);
        return unmarshalledMessage;
    } 

    /**
     * marshal an OMP Jaxb MSG object to formatted XML String, applying Schema validation if present
     * @param ompMessage the OMP Jaxb MSG source
     * @param schema the javax.xml.validation.Schema if present, null otherwise
     * @return XML formatted String
     * @throws JAXBException
     */
    public String marshallMessageToXml(MSG ompMessage, Schema schema) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(MSG.class);
        Marshaller marshaller = context.createMarshaller();
        StringWriter writer = new StringWriter();
        if (schema != null) 
        	marshaller.setSchema(schema);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(ompMessage, writer);
        
        String result = writer.toString();
        result = adjustCDATAField(result, "SERVIZI");
        return result;
    }

    /**
     * adjust CDATA field
     * @param str CDATA field
     * @param externalTag tag root CDATA field
     * @return CDATA field correct
     * @throws JAXBException
     */
    private String adjustCDATAField(String str, String externalTag) {
    	str = str.replaceAll("&lt;", "<");
    	str = str.replaceAll("&gt;", ">");
    	str = str.replaceAll("<"+externalTag, "<![CDATA[<"+externalTag);
    	str = str.replaceAll("/"+externalTag+">", "/"+externalTag+">]]>");
    	return str;
    }
    
    /**
     * load XML Schema validation file .XSD
     * @param xsdSchemaPath full path (url) to xsd file
     * @return javax.xml.validation.Schema if a valid xsd file was loaded, null otherwise
     * @throws MalformedURLException
     * @throws SAXException
     */
	public Schema loadSchemaFrom(String xsdSchemaPath) throws MalformedURLException, SAXException {
		URL pathUrl = new URL(xsdSchemaPath);
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		return schemaFactory.newSchema(pathUrl);
	}

	/**
	 * date conversion from GregorianCalendar to XML notation
	 * @param gregorianCal  the GregorianCalendar formatted date to convert
	 * @return the XMLGregorianCalendar date formatted
	 */
	public XMLGregorianCalendar toXMLGregorian(GregorianCalendar gregorianCal) {
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCal);
		} catch (DatatypeConfigurationException e) {
			log.debug("Date conversion failed ", e);
		}	
		return null;
	}

	/**
	 * date conversion from Date to XML notation
	 * @param date  the Date to convert
	 * @return the XMLGregorianCalendar date formatted
	 */
	public XMLGregorianCalendar toXMLGregorian(Date date) {
		GregorianCalendar calendar = (GregorianCalendar)GregorianCalendar.getInstance();
		calendar.setTime(date);
		return toXMLGregorian(calendar);
	}

	/**
	 * fill a mapped details list from Trace object
	 * @param trace  Trace object top of details list
	 * @return the Map details list 
	 */
	private Map<String, String> loadDetailValues(Trace trace) {
		Map<String, String> paramsMap = new HashMap<String, String>();

		if (trace.getTraceDetailsList() != null && trace.getTraceDetailsList().getTraceDetails() != null) 
			for (TraceDetail detail : trace.getTraceDetailsList().getTraceDetails())
				paramsMap.put(detail.getParamClass(), detail.getParamValue());

		return paramsMap;
	}

	/**
	 * left zero-pad DID code to 14 char length 
	 * @param didCode  the DID code to be padded
	 * @return a String converted DID code
	 */ 
	private String padDIDCode(Long didCode) {
		DecimalFormat df = new DecimalFormat("0000000000000");
		if (didCode != null)
			return df.format(didCode);
		return df.format(0);
	}
	
	/**
	 * try to seek ID_STATUS field from trace details if absent
	 * @param seekTrace  the Trace to search in
	 * @param detailMap  Map of trace details
	 * @return  String ID_STATUS field if found, null otherwise
	 */
	private String tryToFindIdStatusIfNull(Trace seekTrace, Map<String, String> detailMap) {
		if (seekTrace.getIdStatus() == null) {
			if (detailMap.containsKey("ID_STATUS"))
				return ompManager.getIdStatusTranscoded(detailMap.get("ID_STATUS"), seekTrace.getChannel());
			if (detailMap.containsKey("DELREASON")) 
				return ompManager.getIdStatusTranscoded(detailMap.get("DELREASON"), seekTrace.getChannel());	
		}
		return seekTrace.getIdStatus();
	}

	/**
	 * Transform a XMLGregorianCalendar in a formatted String.
	 * @param calendar Date to transform
	 * @param sdf Date Formatter
	 * @return A formatted string if calendar is not null, null otherwise
	 */
	private String dateToStr(XMLGregorianCalendar calendar, DateFormat sdf) {
		if (calendar != null) {
			Date date = calendar.toGregorianCalendar().getTime();
			return sdf.format(date);
		}
		return null;
	}

}
