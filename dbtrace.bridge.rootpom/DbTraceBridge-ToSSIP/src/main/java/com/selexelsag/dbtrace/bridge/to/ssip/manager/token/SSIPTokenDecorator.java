/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.ssip.manager.token;

import java.io.Serializable;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.elsagdatamat.dbtrace.bridge.engine.entities.Token;


/**
 * @author arodriguez
 *
 */
public class SSIPTokenDecorator extends ToToken implements Serializable {

	private ToToken decoratedToToken = null;
	
	/**
	 * hided default constructor
	 */
	private SSIPTokenDecorator() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param decoratedToToken
	 * @param token
	 */
	public SSIPTokenDecorator(ToToken decoratedToToken, Token token) {
		this.decoratedToToken = decoratedToToken;
		this.decoratedToToken.setServiceId(token.getServiceId());
		this.decoratedToToken.setWsToken(token.getWsToken());
	}

	/**
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.manager.token.manager.ToToken#getWsToken()
	 */
	public String getWsToken() {
		return decoratedToToken.getWsToken();
	}

	/**
	 * @param wsToken
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.manager.token.manager.ToToken#setWsToken(java.lang.String)
	 */
	public void setWsToken(String wsToken) {
		decoratedToToken.setWsToken(wsToken);
	}

	/**
	 * @return
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.manager.token.manager.ToToken#getServiceId()
	 */
	public String getServiceId() {
		return decoratedToToken.getServiceId();
	}

	/**
	 * @param serviceId
	 * @see com.selexelsag.dbtrace.bridge.to.ssip.manager.token.manager.ToToken#setServiceId(java.lang.String)
	 */
	public void setServiceId(String serviceId) {
		decoratedToToken.setServiceId(serviceId);
	}

}
