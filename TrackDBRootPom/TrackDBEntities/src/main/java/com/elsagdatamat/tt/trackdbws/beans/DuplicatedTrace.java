package com.elsagdatamat.tt.trackdbws.beans;


import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DUPLICATED_TRACES")

public class DuplicatedTrace implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID", nullable=false)
	@SequenceGenerator(name="DTR_SEQ", sequenceName="DTR_SEQ", allocationSize=50)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DTR_SEQ")
	private Long traceId;

	@Column(name="LABEL_TRACED_ENTITY", nullable=true)
	private String labelTracedEntity;
	
	@Column(name="TRACED_ENTITY", nullable=false)
	private String tracedEntity;
	
	@Column(name="ID_TRACED_ENTITY", nullable=false)
	private String idTracedEntity;
	
	@Column(name="WHEN_HAPPENED", nullable=false)
	private Date whenHappened;
	
	@Column(name="WHEN_REGISTERED", nullable=false)
	private Date whenRegistered;
	
	@Column(name="WHERE_HAPPENED", nullable=false)
	private String whereHappened;
	
	@Column(name="CHANNEL", nullable=false)
	private String channel;
	
	@Column(name="ID_CHANNEL", nullable=false)
	private String idChannel;
	
	@Column(name="WHAT_HAPPENED", nullable=false)
	private String whatHappened;
	
	@Column(name="SERVICE_NAME", nullable=false)
	private String serviceName;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="duplicatedTrace", fetch=FetchType.EAGER)
	private Set<DuplicatedTraceDetail> internalDetailList;

	public Long getTraceId() {
		return traceId;
	}

	public void setTraceId(Long traceId) {
		this.traceId = traceId;
	}

	public String getLabelTracedEntity() {
		return labelTracedEntity;
	}

	public void setLabelTracedEntity(String labelTracedEntity) {
		this.labelTracedEntity = labelTracedEntity;
	}

	public String getTracedEntity() {
		return tracedEntity;
	}

	public void setTracedEntity(String tracedEntity) {
		this.tracedEntity = tracedEntity;
	}

	public String getIdTracedEntity() {
		return idTracedEntity;
	}

	public void setIdTracedEntity(String idTracedEntity) {
		this.idTracedEntity = idTracedEntity;
	}

	public Date getWhenHappened() {
		return whenHappened;
	}

	public void setWhenHappened(Date whenHappened) {
		this.whenHappened = whenHappened;
	}

	public Date getWhenRegistered() {
		return whenRegistered;
	}

	public void setWhenRegistered(Date whenRegistered) {
		this.whenRegistered = whenRegistered;
	}

	public String getWhereHappened() {
		return whereHappened;
	}

	public void setWhereHappened(String whereHappened) {
		this.whereHappened = whereHappened;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getIdChannel() {
		return idChannel;
	}

	public void setIdChannel(String idChannel) {
		this.idChannel = idChannel;
	}

	public String getWhatHappened() {
		return whatHappened;
	}

	public void setWhatHappened(String whatHappened) {
		this.whatHappened = whatHappened;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Set<DuplicatedTraceDetail> getInternalDetailList() {
		return internalDetailList;
	}

	public void setInternalDetailList(Set<DuplicatedTraceDetail> internalDetailList) {
		this.internalDetailList = internalDetailList;
	}
	
}
