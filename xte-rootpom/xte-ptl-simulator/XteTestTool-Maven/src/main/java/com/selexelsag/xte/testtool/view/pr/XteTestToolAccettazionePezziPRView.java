/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.pr;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.view.XteTestToolPanelView;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolCodeDocument;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolIntegerDocument;
import com.selexelsag.xte.testtool.view.objectdocument.XteTestToolStringDocument;
import com.selexelsag.xte.testtool.view.objectmodel.XteListSelectionModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteAccettazionePezziTableModel;
import com.selexelsag.xte.testtool.view.objectrenderer.XteMailPieceTableCellRenderer;
import com.selexelsag.xte.testtool.view.objectrenderer.XteTableHeaderRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.PlainDocument;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author Tassara
 */
public class XteTestToolAccettazionePezziPRView extends XteTestToolPanelView{
    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolAccettazionePezziPRView.class);
    private XteAccettazionePezziTableModel xteAccettazionePezziPRTableModel = new XteAccettazionePezziTableModel();;
    
    public XteTestToolAccettazionePezziPRView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try {
            logger.info("XteTestToolAccettazionePezziPRView start class creation");
            this.messages = ResourceBundle.getBundle("bundles/messages");
            initComponents();
            logger.info("XteTestToolAccettazionePezziPRView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolAccettazionePezziPRView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Accettazione Pezzi Posta Registrata view creation failed", ex);
        }
    }

    public JPanel getAccettazionePezziPanelRef(){ return this; }        
    public JLabel getAccettazionePezziTitleLabelRef(){ return this.panelTitlejLabel; }
   
    public JTextField getAPIdOpTextRef(){ return this.APIdOpjTextField; }
    public JTextField getAPFrazionariTextRef(){ return this.APFrazjTextField; }
    public JTextField getAPNumSpedTextRef(){ return this.APNumSpedjTextField; }
    public JTextField getAPCodiceClienteTextRef(){ return this.APCodClientejTextField; }
    public JComboBox getAPTypeComboBoxRef(){ return this.APTypejComboBox; }
    
    public JPanel getAccettazionePezziDataPaneRef(){ return this.mailPieceDatajPanel; }
    public JTextField getAPCodiceTextRef(){ return this.APCodejTextField; }
    public JTextField getAPProductTextRef(){ return this.APProductjTextField; }
    public JXDatePicker getAccettazionePezziDatePickerRef(){ return this.APDatejXDatePicker; }
    public JSpinner getAccettazionePezziTimeSpinnerRef(){ return this.APTimejSpinner; }
    public JTextField getAPPesoTextRef(){ return this.APPesojTextField; }
    public JTextField getAPPrezzoTextRef(){ return this.APPrezzojTextField; }
    public JTextField getAPPercettoTextRef(){ return this.APPercettojTextField; }
    public JTextField getAPDestinazioneTextRef(){ return this.APDestinazionejTextField; }
    public JTextField getAPDestinatarioTextRef(){ return this.APDestinatariojTextField; }
    public JTextField getAPIndirizzoTextRef(){ return this.APIndirizzojTextField; }
    public JTextField getAPCAPTextRef(){ return this.APCAPjTextField; }
    public JTextField getAPNomeMittenteTextRef(){ return this.APNomeMittjTextField; }
    public JTextField getAPDestMittenteTextRef(){ return this.APDestMittjTextField; }
    public JTextField getAPIndMittenteTextRef(){ return this.APIndMittjTextField; }
    public JTextField getAPCAPMittTextRef(){ return this.APCAPMittjTextField; }
    public JComboBox getAPSAJComboBoxRef(){ return this.APSAjComboBox; }
    public JTextField getAPValAssTextRef(){ return this.APValAssjTextField; }
    public JTextField getAPImpContrTextRef(){ return this.APImpContrjTextField; }
    public JTextField getAPCodARTextRef(){ return this.APCodARjTextField; }
    public JTextField getAPIBANTextRef(){ return this.APIBANjTextField; }
    public JTextField getAPOraTimeDefTextRef(){ return this.APOraTimeDefjTextField; }
    
    public void setAPFields(){  
        Calendar date = Calendar.getInstance();
        this.APIdOpjTextField.setText("RossiM");
        this.APNumSpedjTextField.setText("S1206");
        this.APCodClientejTextField.setText("PR12345");
        this.APFrazjTextField.setText("28428");
        this.APCodejTextField.setText("KA00000000000"); 
        this.APProductjTextField.setText("CI1");
        this.APDatejXDatePicker.transferFocus();
        this.APDatejXDatePicker.setDate(date.getTime());
        this.APTimejSpinner.setValue(date.getTime());
        this.APPesojTextField.setText("1100");  
        this.APPrezzojTextField.setText("1200"); 
        this.APPercettojTextField.setText("700"); 
        this.APDestinazionejTextField.setText("Corleone"); 
        this.APDestinatariojTextField.setText("Romolo Puccini"); 
        this.APIndirizzojTextField.setText("Via dei Mille 999"); 
        this.APCAPjTextField.setText("16124"); 
        this.APNomeMittjTextField.setText("Mario Rossi"); 
        this.APDestMittjTextField.setText("Bogliasco"); 
        this.APIndMittjTextField.setText("Via Mazzini 4"); 
        this.APCAPMittjTextField.setText("16034");  
        this.APValAssjTextField.setText("10000"); 
        this.APImpContrjTextField.setText("5000"); 
        this.APOraTimeDefjTextField.setText("10"); 
        this.APCodARjTextField.setText("100000000012");
        this.APSAjComboBox.setSelectedIndex(0);
        this.APTypejComboBox.setSelectedIndex(0);
    }
    
    public JButton getTestButtonRef(){ return this.TestjButton;}
    
    public JTable getAccettazionePezziTableRef(){ return this.mailPiecejTable; }
    public XteAccettazionePezziTableModel getAccettazionePezziTableModelRef(){ return (XteAccettazionePezziTableModel) this.mailPiecejTable.getModel(); }
    public JPanel getAccettazionePezziDataCommandPaneRef(){ return this.mailPieceDataCommandjPanel; }
    public JButton getAggiungiItemButtonRef(){ return this.addAccettazionePezzijButton; }
    public JButton getEliminaItemButtonRef(){ return this.removeAccettazionePezzijButton; }
    
    public JPanel getXmlDataPaneRef(){ return this.xmlDatajPanel; }
    public JScrollPane getXmlDataScrollPaneRef(){ return this.xmlDatajScrollPane; }
    public JTextArea getXmlDataTextAreaRef(){ return this.xmlDatajTextArea; }
    public JPanel getXmlDataCommandPaneRef(){ return this.xmlDataCommandjPanel; }    
    public JButton getGeneraXmlDataButtonRef(){ return this.generaXmlDatajButton; }
    public JButton getImpostaDatiWSButtonRef(){ return this.impostaDatiWSjButton; }
    public JButton getSalvaXmlDataButtonRef(){ return this.salvaXmlDatajButton; }
    
    public JButton getBackButtonRef(){ return this.backjButton; }
    public JButton getHomeButtonRef(){ return this.homejButton; }
    public JButton getExitButtonRef(){ return this.exitjButton; }
    public JTextPane getMessageBarTextPaneRef(){ return this.messageBarjTextPane; }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolAccettazionePezziPRView start customizeView");
            //CUSTOMIZE TITLE
            this.panelTitlejLabel .setText(messages.getString("postaregistrata.accettazionepezzi.view.title.label"));

            //CUSTOMIZE BUTTON
            this.addAccettazionePezzijButton.setText(messages.getString("postaregistrata.accettazionepezzi.view.additem.button"));
            this.removeAccettazionePezzijButton.setText(messages.getString("postaregistrata.accettazionepezzi.view.removeitem.button"));
            this.generaXmlDatajButton.setText(messages.getString("postaregistrata.accettazionepezzi.view.generaxmldata.button"));
            this.impostaDatiWSjButton.setText(messages.getString("postaregistrata.accettazionepezzi.view.impostadatiws.button"));
            this.salvaXmlDatajButton.setText(messages.getString("postaregistrata.accettazionepezzi.view.salvaxmldata.button"));
            this.backjButton.setText(messages.getString("view.back.button"));
            this.homejButton.setText(messages.getString("view.home.button"));
            this.exitjButton.setText(messages.getString("view.exit.button"));

            logger.info("XteTestToolAccettazionePezziPRView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolAccettazionePezziPRView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata view customization failed", ex);
        }
    }

    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolAccettazionePezziPRView start initializeView");

//            buildComponentSortedMap();
//
//            this.setFocusCycleRoot(true);
//            this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

            defineTabPolicyForTextArea(xmlDatajTextArea);
            
            JLabel cornelLabel = new JLabel("");
            cornelLabel.setOpaque(true);
            cornelLabel.setBackground(new Color(240, 240, 240));

            this.mailPieceDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.mailPieceDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.mailPieceDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.mailPieceDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.mailPieceDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            customizeTable(this.mailPiecejTable);
            this.mailPiecejTable.setModel(xteAccettazionePezziPRTableModel);

            List<String> eventColumnWidthValues = new ArrayList<String>();
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");

            this.mailPiecejTable.getTableHeader().setDefaultRenderer(new XteTableHeaderRenderer(eventColumnWidthValues));
            this.mailPiecejTable.setDefaultRenderer(Object.class, new XteMailPieceTableCellRenderer(eventColumnWidthValues));
            
            this.xmlDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.xmlDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.xmlDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.xmlDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.xmlDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            JFormattedTextField mailPieceTimeJFormattedTextField = ((JSpinner.DateEditor)APTimejSpinner.getEditor()).getTextField();
            DefaultFormatter mailPieceTimeFormatter = (DefaultFormatter) mailPieceTimeJFormattedTextField.getFormatter();
            mailPieceTimeFormatter.setCommitsOnValidEdit(true);
            
            this.APCodejTextField.setDocument(new XteTestToolCodeDocument(30));            
            this.APCodejTextField.getDocument().putProperty("owner", APCodejTextField);
            this.APProductjTextField.setDocument(new XteTestToolStringDocument(3));            
            this.APProductjTextField.getDocument().putProperty("owner", APProductjTextField);
            this.APIdOpjTextField.setDocument(new XteTestToolStringDocument(40));            
            this.APIdOpjTextField.getDocument().putProperty("owner", APIdOpjTextField); 
            this.APFrazjTextField.setDocument(new XteTestToolStringDocument(5));            
            this.APFrazjTextField.getDocument().putProperty("owner", APFrazjTextField); 
            this.APCodClientejTextField.setDocument(new PlainDocument());            
            this.APCodClientejTextField.getDocument().putProperty("owner", APCodClientejTextField); 
            this.APPesojTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.APPesojTextField.getDocument().putProperty("owner", APPesojTextField);
            this.APPrezzojTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.APPrezzojTextField.getDocument().putProperty("owner", APPrezzojTextField);
            this.APPercettojTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.APPercettojTextField.getDocument().putProperty("owner", APPercettojTextField);
            this.APDestinazionejTextField.setDocument(new XteTestToolStringDocument(20));            
            this.APDestinazionejTextField.getDocument().putProperty("owner", APDestinazionejTextField);
            this.APDestinatariojTextField.setDocument(new XteTestToolStringDocument(20));            
            this.APDestinatariojTextField.getDocument().putProperty("owner", APDestinatariojTextField);
            this.APIndirizzojTextField.setDocument(new XteTestToolStringDocument(100));            
            this.APIndirizzojTextField.getDocument().putProperty("owner", APIndirizzojTextField);
            this.APCAPjTextField.setDocument(new XteTestToolStringDocument(9));            
            this.APIndirizzojTextField.getDocument().putProperty("owner", APIndirizzojTextField);
            this.APNomeMittjTextField.setDocument(new XteTestToolStringDocument(20));            
            this.APNomeMittjTextField.getDocument().putProperty("owner", APNomeMittjTextField);
            this.APDestMittjTextField.setDocument(new XteTestToolStringDocument(20));            
            this.APDestMittjTextField.getDocument().putProperty("owner", APDestMittjTextField);
            this.APIndMittjTextField.setDocument(new XteTestToolStringDocument(100));            
            this.APIndMittjTextField.getDocument().putProperty("owner", APIndMittjTextField);
            this.APCAPMittjTextField.setDocument(new XteTestToolStringDocument(9));            
            this.APCAPMittjTextField.getDocument().putProperty("owner", APCAPMittjTextField);
            this.APCodARjTextField.setDocument(new XteTestToolStringDocument(30));            
            this.APCodARjTextField.getDocument().putProperty("owner", APCodARjTextField);
            this.APValAssjTextField.setDocument(new XteTestToolIntegerDocument(100));            
            this.APValAssjTextField.getDocument().putProperty("owner", APValAssjTextField);
            this.APIBANjTextField.setDocument(new XteTestToolStringDocument(27));            
            this.APIBANjTextField.getDocument().putProperty("owner", APIBANjTextField);
            this.APImpContrjTextField.setDocument(new XteTestToolIntegerDocument(8));            
            this.APImpContrjTextField.getDocument().putProperty("owner", APImpContrjTextField);
            this.APOraTimeDefjTextField.setDocument(new XteTestToolIntegerDocument(2));            
            this.APOraTimeDefjTextField.getDocument().putProperty("owner", APOraTimeDefjTextField);
            this.APDatejXDatePicker.getEditor().setDocument(new PlainDocument());            
            this.APDatejXDatePicker.getEditor().getDocument().putProperty("owner", APDatejXDatePicker.getEditor());                        
            ((JSpinner.DateEditor)APTimejSpinner.getEditor()).getTextField().setDocument(new PlainDocument());            
            ((JSpinner.DateEditor)APTimejSpinner.getEditor()).getTextField().getDocument().putProperty("owner", ((JSpinner.DateEditor)APTimejSpinner.getEditor()).getTextField());            


            
            logger.info("XteTestToolAccettazionePezziPRView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolAccettazionePezziPRView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Scarico Pezzi Posta Registrata view initialization failed", ex);
        }
    }

    @Override
    public void restoreView() {
        logger.debug("XteTestToolAccettazionePezziPRView restoreView");
        this.getExitButtonRef().doClick();
    }

    private void customizeTable(JTable table) {
        table.getTableHeader().setReorderingAllowed(false);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setRowHeight(20);
        table.setSelectionModel(new XteListSelectionModel());
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    private  static void defineTabPolicyForTextArea(final JTextArea textarea) {

        final Object tabActionKey = new Object();
        final Object shiftTabActionKey = new Object();
        KeyStroke tabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0, false);
        KeyStroke shiftTabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_DOWN_MASK, false);

        Action tabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusNextComponent((Component) source);
                }
            }
        };

        Action shiftTabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent((Component) source);
                }
            }
        };

        textarea.getActionMap().put(tabActionKey, tabAction);
        textarea.getInputMap().put(tabKeyStroke, tabActionKey);

        textarea.getActionMap().put(shiftTabActionKey, shiftTabAction);
        textarea.getInputMap().put(shiftTabKeyStroke, shiftTabActionKey);

    }
    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.APCAPjTextField);
        componentSortedMap.put(2, this.APProductjTextField);
        componentSortedMap.put(3, this.APDatejXDatePicker.getEditor());
        componentSortedMap.put(4, ((JSpinner.DateEditor)this.APTimejSpinner.getEditor()).getTextField());
        componentSortedMap.put(5, this.mailPiecejTable);
        componentSortedMap.put(6, this.xmlDatajTextArea);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        commandjPanel = new javax.swing.JPanel();
        messageBarjScrollPane = new javax.swing.JScrollPane();
        messageBarjTextPane = new javax.swing.JTextPane();
        homejButton = new javax.swing.JButton();
        exitjButton = new javax.swing.JButton();
        backjButton = new javax.swing.JButton();
        workingjPanel = new javax.swing.JPanel();
        panelTitlejLabel = new javax.swing.JLabel();
        xmlDatajPanel = new javax.swing.JPanel();
        xmlDatajScrollPane = new javax.swing.JScrollPane();
        xmlDatajTextArea = new javax.swing.JTextArea();
        xmlDataCommandjPanel = new javax.swing.JPanel();
        generaXmlDatajButton = new javax.swing.JButton();
        impostaDatiWSjButton = new javax.swing.JButton();
        salvaXmlDatajButton = new javax.swing.JButton();
        mailPieceDatajPanel = new javax.swing.JPanel();
        mailPieceDatajScrollPane = new javax.swing.JScrollPane();
        mailPiecejTable = new javax.swing.JTable();
        mailPieceDataCommandjPanel = new javax.swing.JPanel();
        addAccettazionePezzijButton = new javax.swing.JButton();
        removeAccettazionePezzijButton = new javax.swing.JButton();
        mailPieceCodejLabel = new javax.swing.JLabel();
        APCAPjTextField = new javax.swing.JTextField();
        mailPieceLatjLabel = new javax.swing.JLabel();
        mailPieceProductjLabel = new javax.swing.JLabel();
        APProductjTextField = new javax.swing.JTextField();
        mailPieceLongjLabel = new javax.swing.JLabel();
        APDatejXDatePicker = new org.jdesktop.swingx.JXDatePicker();
        mailPieceDateTimejLabel = new javax.swing.JLabel();
        APTimejSpinner = new javax.swing.JSpinner();
        mailPieceLongjLabel1 = new javax.swing.JLabel();
        APCodejTextField = new javax.swing.JTextField();
        APNomeMittjTextField = new javax.swing.JTextField();
        APDestMittjTextField = new javax.swing.JTextField();
        mailPieceCodejLabel1 = new javax.swing.JLabel();
        mailPieceProductjLabel1 = new javax.swing.JLabel();
        mailPieceDateTimejLabel1 = new javax.swing.JLabel();
        mailPieceLongjLabel2 = new javax.swing.JLabel();
        mailPieceLongjLabel3 = new javax.swing.JLabel();
        APPercettojTextField = new javax.swing.JTextField();
        APPrezzojTextField = new javax.swing.JTextField();
        APPesojTextField = new javax.swing.JTextField();
        APDestinatariojTextField = new javax.swing.JTextField();
        APDestinazionejTextField = new javax.swing.JTextField();
        mailPieceCodejLabel2 = new javax.swing.JLabel();
        mailPieceProductjLabel2 = new javax.swing.JLabel();
        mailPieceDateTimejLabel2 = new javax.swing.JLabel();
        mailPieceLatjLabel2 = new javax.swing.JLabel();
        mailPieceLongjLabel4 = new javax.swing.JLabel();
        APOraTimeDefjTextField = new javax.swing.JTextField();
        APValAssjTextField = new javax.swing.JTextField();
        APIndirizzojTextField = new javax.swing.JTextField();
        APImpContrjTextField = new javax.swing.JTextField();
        APIndMittjTextField = new javax.swing.JTextField();
        APCAPMittjTextField = new javax.swing.JTextField();
        APSAjComboBox = new javax.swing.JComboBox();
        mailPieceLatjLabel3 = new javax.swing.JLabel();
        mailPieceDateTimejLabel3 = new javax.swing.JLabel();
        APCodARjTextField = new javax.swing.JTextField();
        APIBANjTextField = new javax.swing.JTextField();
        APTypejComboBox = new javax.swing.JComboBox();
        mailPieceCodejLabel3 = new javax.swing.JLabel();
        APIdOpjTextField = new javax.swing.JTextField();
        mailPieceCodejLabel4 = new javax.swing.JLabel();
        mailPieceCodejLabel5 = new javax.swing.JLabel();
        mailPieceCodejLabel6 = new javax.swing.JLabel();
        APNumSpedjTextField = new javax.swing.JTextField();
        APFrazjTextField = new javax.swing.JTextField();
        APCodClientejTextField = new javax.swing.JTextField();
        TestjButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setMaximumSize(new java.awt.Dimension(1000, 690));
        setMinimumSize(new java.awt.Dimension(1000, 690));
        setPreferredSize(new java.awt.Dimension(1000, 690));

        commandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        commandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        commandjPanel.setMaximumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setMinimumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setPreferredSize(new java.awt.Dimension(980, 80));

        messageBarjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageBarjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        messageBarjScrollPane.setMaximumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setMinimumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setPreferredSize(new java.awt.Dimension(350, 50));

        messageBarjTextPane.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        messageBarjTextPane.setFocusable(false);
        messageBarjScrollPane.setViewportView(messageBarjTextPane);

        homejButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        homejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Home3.png"))); // NOI18N
        homejButton.setText("<html><body>&nbsp;Home<body><html>");
        homejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        homejButton.setFocusPainted(false);
        homejButton.setFocusable(false);
        homejButton.setForeground(new java.awt.Color(51, 51, 51));
        homejButton.setIconTextGap(5);
        homejButton.setMaximumSize(new java.awt.Dimension(100, 50));
        homejButton.setMinimumSize(new java.awt.Dimension(100, 50));
        homejButton.setPreferredSize(new java.awt.Dimension(100, 50));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("<html><body>&nbsp;Exit<body><html>");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setFocusPainted(false);
        exitjButton.setFocusable(false);
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIconTextGap(5);
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        backjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        backjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/BackToMenu.png"))); // NOI18N
        backjButton.setText("<html><body>&nbsp;Back<body><html>");
        backjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        backjButton.setFocusPainted(false);
        backjButton.setFocusable(false);
        backjButton.setForeground(new java.awt.Color(51, 51, 51));
        backjButton.setIconTextGap(5);
        backjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        backjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        backjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout commandjPanelLayout = new javax.swing.GroupLayout(commandjPanel);
        commandjPanel.setLayout(commandjPanelLayout);
        commandjPanelLayout.setHorizontalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 268, Short.MAX_VALUE)
                .addComponent(backjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandjPanelLayout.setVerticalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandjPanelLayout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homejButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exitjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        workingjPanel.setBackground(new java.awt.Color(230, 250, 0));
        workingjPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20)));
        workingjPanel.setMaximumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setMinimumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setPreferredSize(new java.awt.Dimension(980, 580));

        panelTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        panelTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        panelTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelTitlejLabel.setText("Accettazione Pezzi");
        panelTitlejLabel.setFocusable(false);
        panelTitlejLabel.setIconTextGap(30);
        panelTitlejLabel.setMaximumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setMinimumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setPreferredSize(new java.awt.Dimension(920, 20));

        xmlDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "XML Data", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        xmlDatajPanel.setFocusable(false);
        xmlDatajPanel.setMaximumSize(new java.awt.Dimension(860, 180));
        xmlDatajPanel.setMinimumSize(new java.awt.Dimension(860, 180));
        xmlDatajPanel.setOpaque(false);
        xmlDatajPanel.setPreferredSize(new java.awt.Dimension(860, 180));

        xmlDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        xmlDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        xmlDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 125));
        xmlDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 125));
        xmlDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 125));

        xmlDatajTextArea.setColumns(20);
        xmlDatajTextArea.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        xmlDatajTextArea.setForeground(new java.awt.Color(51, 51, 51));
        xmlDatajTextArea.setLineWrap(true);
        xmlDatajTextArea.setRows(5);
        xmlDatajTextArea.setWrapStyleWord(true);
        xmlDatajTextArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setViewportView(xmlDatajTextArea);

        xmlDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        xmlDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        xmlDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 125));
        xmlDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        generaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        generaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        generaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml6.png"))); // NOI18N
        generaXmlDatajButton.setText("<html><body>&nbsp;Genera<br>XML Data<body><html>");
        generaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        generaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        generaXmlDatajButton.setFocusPainted(false);
        generaXmlDatajButton.setFocusable(false);
        generaXmlDatajButton.setIconTextGap(2);
        generaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        xmlDataCommandjPanel.add(generaXmlDatajButton, gridBagConstraints);

        impostaDatiWSjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        impostaDatiWSjButton.setForeground(new java.awt.Color(51, 51, 51));
        impostaDatiWSjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/WebService5.png.png"))); // NOI18N
        impostaDatiWSjButton.setText("<html><body>&nbsp;Imposta<br>&nbsp;Dati WS<body><html>");
        impostaDatiWSjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        impostaDatiWSjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        impostaDatiWSjButton.setFocusPainted(false);
        impostaDatiWSjButton.setFocusable(false);
        impostaDatiWSjButton.setIconTextGap(1);
        impostaDatiWSjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        impostaDatiWSjButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        xmlDataCommandjPanel.add(impostaDatiWSjButton, gridBagConstraints);

        salvaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        salvaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml7.png"))); // NOI18N
        salvaXmlDatajButton.setText("<html><body>&nbsp;Salva<br>XML Data<body><html>");
        salvaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        salvaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        salvaXmlDatajButton.setFocusPainted(false);
        salvaXmlDatajButton.setFocusable(false);
        salvaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        salvaXmlDatajButton.setIconTextGap(2);
        salvaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        salvaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        xmlDataCommandjPanel.add(salvaXmlDatajButton, gridBagConstraints);

        javax.swing.GroupLayout xmlDatajPanelLayout = new javax.swing.GroupLayout(xmlDatajPanel);
        xmlDatajPanel.setLayout(xmlDatajPanelLayout);
        xmlDatajPanelLayout.setHorizontalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        xmlDatajPanelLayout.setVerticalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(xmlDatajPanelLayout.createSequentialGroup()
                .addGroup(xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        mailPieceDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Informazioni Invio", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        mailPieceDatajPanel.setMaximumSize(new java.awt.Dimension(860, 260));
        mailPieceDatajPanel.setMinimumSize(new java.awt.Dimension(860, 260));
        mailPieceDatajPanel.setOpaque(false);
        mailPieceDatajPanel.setPreferredSize(new java.awt.Dimension(860, 260));

        mailPieceDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        mailPieceDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        mailPieceDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        mailPieceDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 120));
        mailPieceDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 120));
        mailPieceDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 120));

        mailPiecejTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        mailPieceDatajScrollPane.setViewportView(mailPiecejTable);

        mailPieceDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        mailPieceDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        mailPieceDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 85));
        mailPieceDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 85));
        mailPieceDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 85));
        mailPieceDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        addAccettazionePezzijButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        addAccettazionePezzijButton.setForeground(new java.awt.Color(51, 51, 51));
        addAccettazionePezzijButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Add1.png"))); // NOI18N
        addAccettazionePezzijButton.setText("<html><body>&nbsp;Aggiungi<br>&nbsp;&nbsp;&nbsp;Invio<body><html>");
        addAccettazionePezzijButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        addAccettazionePezzijButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        addAccettazionePezzijButton.setFocusPainted(false);
        addAccettazionePezzijButton.setFocusable(false);
        addAccettazionePezzijButton.setIconTextGap(1);
        addAccettazionePezzijButton.setMaximumSize(new java.awt.Dimension(100, 35));
        addAccettazionePezzijButton.setMinimumSize(new java.awt.Dimension(100, 35));
        addAccettazionePezzijButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        mailPieceDataCommandjPanel.add(addAccettazionePezzijButton, gridBagConstraints);

        removeAccettazionePezzijButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        removeAccettazionePezzijButton.setForeground(new java.awt.Color(51, 51, 51));
        removeAccettazionePezzijButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Remove1.png"))); // NOI18N
        removeAccettazionePezzijButton.setText("<html><body>&nbsp;Elimina<br>&nbsp;&nbsp;&nbsp;Invio<body><html>");
        removeAccettazionePezzijButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        removeAccettazionePezzijButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        removeAccettazionePezzijButton.setFocusPainted(false);
        removeAccettazionePezzijButton.setFocusable(false);
        removeAccettazionePezzijButton.setIconTextGap(2);
        removeAccettazionePezzijButton.setMaximumSize(new java.awt.Dimension(100, 35));
        removeAccettazionePezzijButton.setMinimumSize(new java.awt.Dimension(100, 35));
        removeAccettazionePezzijButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        mailPieceDataCommandjPanel.add(removeAccettazionePezzijButton, gridBagConstraints);

        mailPieceCodejLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel.setText("Codice:");
        mailPieceCodejLabel.setFocusable(false);
        mailPieceCodejLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        APCAPjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APCAPjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APCAPjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APCAPjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APCAPjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceLatjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLatjLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLatjLabel.setText("Peso:");
        mailPieceLatjLabel.setFocusable(false);
        mailPieceLatjLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        mailPieceProductjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceProductjLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceProductjLabel.setText("Prodotto:");
        mailPieceProductjLabel.setFocusable(false);
        mailPieceProductjLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        APProductjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APProductjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APProductjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APProductjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APProductjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceLongjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel.setText("Percetto:");
        mailPieceLongjLabel.setFocusable(false);
        mailPieceLongjLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        APDatejXDatePicker.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APDatejXDatePicker.setBackground(new java.awt.Color(255, 255, 255));
        APDatejXDatePicker.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APDatejXDatePicker.setMaximumSize(new java.awt.Dimension(150, 20));
        APDatejXDatePicker.setMinimumSize(new java.awt.Dimension(150, 20));
        APDatejXDatePicker.setOpaque(true);
        APDatejXDatePicker.setPreferredSize(new java.awt.Dimension(150, 20));

        mailPieceDateTimejLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel.setText("Data/Ora:");
        mailPieceDateTimejLabel.setFocusable(false);
        mailPieceDateTimejLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        APTimejSpinner.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        APTimejSpinner.setModel(new javax.swing.SpinnerDateModel());
        APTimejSpinner.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APTimejSpinner.setEditor(new javax.swing.JSpinner.DateEditor(APTimejSpinner, "HH:mm:ss"));
        APTimejSpinner.setFocusable(false);
        APTimejSpinner.setMaximumSize(new java.awt.Dimension(80, 20));
        APTimejSpinner.setMinimumSize(new java.awt.Dimension(80, 20));
        APTimejSpinner.setPreferredSize(new java.awt.Dimension(80, 20));

        mailPieceLongjLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel1.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel1.setText("Prezzo:");
        mailPieceLongjLabel1.setFocusable(false);
        mailPieceLongjLabel1.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel1.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel1.setPreferredSize(new java.awt.Dimension(120, 20));

        APCodejTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APCodejTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APCodejTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APCodejTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APCodejTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APNomeMittjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APNomeMittjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APNomeMittjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APNomeMittjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APNomeMittjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APDestMittjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APDestMittjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APDestMittjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APDestMittjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APDestMittjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceCodejLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel1.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel1.setText("Destinazione:");
        mailPieceCodejLabel1.setFocusable(false);
        mailPieceCodejLabel1.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel1.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel1.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceProductjLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceProductjLabel1.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceProductjLabel1.setText("Destinatario:");
        mailPieceProductjLabel1.setFocusable(false);
        mailPieceProductjLabel1.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel1.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel1.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceDateTimejLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel1.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel1.setText("Indirizzo:");
        mailPieceDateTimejLabel1.setFocusable(false);
        mailPieceDateTimejLabel1.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel1.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel1.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceLongjLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel2.setText("Nome mittente:");
        mailPieceLongjLabel2.setFocusable(false);
        mailPieceLongjLabel2.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel2.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel2.setPreferredSize(new java.awt.Dimension(120, 20));

        mailPieceLongjLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel3.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel3.setText("Dest. mittente:");
        mailPieceLongjLabel3.setFocusable(false);
        mailPieceLongjLabel3.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel3.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel3.setPreferredSize(new java.awt.Dimension(120, 20));

        APPercettojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APPercettojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APPercettojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APPercettojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APPercettojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APPrezzojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APPrezzojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APPrezzojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APPrezzojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APPrezzojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APPesojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APPesojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APPesojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APPesojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APPesojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APDestinatariojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APDestinatariojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APDestinatariojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APDestinatariojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APDestinatariojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APDestinazionejTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APDestinazionejTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APDestinazionejTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APDestinazionejTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APDestinazionejTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceCodejLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel2.setText("Ind. mittente:");
        mailPieceCodejLabel2.setFocusable(false);
        mailPieceCodejLabel2.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel2.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel2.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceProductjLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceProductjLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceProductjLabel2.setText("SA:");
        mailPieceProductjLabel2.setFocusable(false);
        mailPieceProductjLabel2.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel2.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceProductjLabel2.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceDateTimejLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel2.setText("Valore assicurato:");
        mailPieceDateTimejLabel2.setFocusable(false);
        mailPieceDateTimejLabel2.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel2.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel2.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceLatjLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLatjLabel2.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLatjLabel2.setText("Imp. contrassegno:");
        mailPieceLatjLabel2.setFocusable(false);
        mailPieceLatjLabel2.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel2.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel2.setPreferredSize(new java.awt.Dimension(120, 20));

        mailPieceLongjLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLongjLabel4.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLongjLabel4.setText("OraTimeDefinite:");
        mailPieceLongjLabel4.setFocusable(false);
        mailPieceLongjLabel4.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel4.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLongjLabel4.setPreferredSize(new java.awt.Dimension(120, 20));

        APOraTimeDefjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APOraTimeDefjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APOraTimeDefjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APOraTimeDefjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APOraTimeDefjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APValAssjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APValAssjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APValAssjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APValAssjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APValAssjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APIndirizzojTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APIndirizzojTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APIndirizzojTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APIndirizzojTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APIndirizzojTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APImpContrjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APImpContrjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APImpContrjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APImpContrjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APImpContrjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APIndMittjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APIndMittjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APIndMittjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APIndMittjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APIndMittjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APCAPMittjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APCAPMittjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APCAPMittjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APCAPMittjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APCAPMittjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APSAjComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        mailPieceLatjLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceLatjLabel3.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceLatjLabel3.setText("IBAN:");
        mailPieceLatjLabel3.setFocusable(false);
        mailPieceLatjLabel3.setMaximumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel3.setMinimumSize(new java.awt.Dimension(120, 20));
        mailPieceLatjLabel3.setPreferredSize(new java.awt.Dimension(120, 20));

        mailPieceDateTimejLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceDateTimejLabel3.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceDateTimejLabel3.setText("Codice AR:");
        mailPieceDateTimejLabel3.setFocusable(false);
        mailPieceDateTimejLabel3.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel3.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceDateTimejLabel3.setPreferredSize(new java.awt.Dimension(140, 20));

        APCodARjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APCodARjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APCodARjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APCodARjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APCodARjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APIBANjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APIBANjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APIBANjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APIBANjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APIBANjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APTypejComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        mailPieceCodejLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel3.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel3.setText("ID Operatore:");
        mailPieceCodejLabel3.setFocusable(false);
        mailPieceCodejLabel3.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel3.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel3.setPreferredSize(new java.awt.Dimension(140, 20));

        APIdOpjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APIdOpjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APIdOpjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APIdOpjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APIdOpjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        mailPieceCodejLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel4.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel4.setText("Office frazionario:");
        mailPieceCodejLabel4.setFocusable(false);
        mailPieceCodejLabel4.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel4.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel4.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceCodejLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel5.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel5.setText("Num. spedizione:");
        mailPieceCodejLabel5.setFocusable(false);
        mailPieceCodejLabel5.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel5.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel5.setPreferredSize(new java.awt.Dimension(140, 20));

        mailPieceCodejLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mailPieceCodejLabel6.setForeground(new java.awt.Color(51, 51, 51));
        mailPieceCodejLabel6.setText("Codice cliente:");
        mailPieceCodejLabel6.setFocusable(false);
        mailPieceCodejLabel6.setMaximumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel6.setMinimumSize(new java.awt.Dimension(140, 20));
        mailPieceCodejLabel6.setPreferredSize(new java.awt.Dimension(140, 20));

        APNumSpedjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APNumSpedjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APNumSpedjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APNumSpedjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APNumSpedjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APFrazjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APFrazjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APFrazjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APFrazjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APFrazjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        APCodClientejTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        APCodClientejTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        APCodClientejTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        APCodClientejTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        APCodClientejTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        TestjButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TestjButton.setForeground(new java.awt.Color(51, 51, 51));
        TestjButton.setText("<html><body>Test<body><html>");
        TestjButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        TestjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        TestjButton.setFocusPainted(false);
        TestjButton.setFocusable(false);
        TestjButton.setIconTextGap(1);
        TestjButton.setMaximumSize(new java.awt.Dimension(100, 35));
        TestjButton.setMinimumSize(new java.awt.Dimension(100, 35));
        TestjButton.setPreferredSize(new java.awt.Dimension(100, 35));

        javax.swing.GroupLayout mailPieceDatajPanelLayout = new javax.swing.GroupLayout(mailPieceDatajPanel);
        mailPieceDatajPanel.setLayout(mailPieceDatajPanelLayout);
        mailPieceDatajPanelLayout.setHorizontalGroup(
            mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                        .addComponent(mailPieceDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mailPieceDatajScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 696, Short.MAX_VALUE)
                        .addGap(11, 11, 11))
                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mailPieceDatajPanelLayout.createSequentialGroup()
                                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(mailPieceCodejLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(mailPieceCodejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                                .addComponent(APTypejComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(APCodejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(APCodClientejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(mailPieceProductjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(mailPieceDateTimejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                                .addComponent(APDatejXDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(APTimejSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(APProductjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(APPercettojTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                            .addComponent(APPrezzojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(11, 11, 11))
                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addComponent(mailPieceCodejLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(APIdOpjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addComponent(mailPieceLatjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(APPesojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(mailPieceLongjLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(mailPieceLongjLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                .addComponent(mailPieceCodejLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(APFrazjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(mailPieceCodejLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(APNumSpedjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(7, 7, 7))
                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(mailPieceCodejLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                            .addComponent(mailPieceLongjLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 114, Short.MAX_VALUE)
                                            .addComponent(mailPieceLongjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                            .addComponent(mailPieceProductjLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                            .addComponent(mailPieceDateTimejLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addComponent(mailPieceCodejLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                        .addGap(1, 1, 1)))
                                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mailPieceDatajPanelLayout.createSequentialGroup()
                                                    .addComponent(APIndirizzojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(APCAPjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(APNomeMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(APDestMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(APDestinatariojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(APDestinazionejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(APIndMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(APCAPMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(18, 18, 18)))
                                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(mailPieceDateTimejLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                            .addComponent(mailPieceLatjLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(APCodARjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(APImpContrjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)))
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(mailPieceLatjLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                                            .addComponent(mailPieceLongjLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                                .addComponent(TestjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                            .addComponent(APOraTimeDefjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                            .addComponent(APIBANjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)))
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addComponent(mailPieceProductjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(APSAjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                                        .addComponent(mailPieceDateTimejLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(APValAssjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)))))))
                .addContainerGap())
        );
        mailPieceDatajPanelLayout.setVerticalGroup(
            mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mailPieceDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mailPieceCodejLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APIdOpjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceCodejLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APNumSpedjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceCodejLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APFrazjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mailPieceProductjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APSAjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceCodejLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APCodClientejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceCodejLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APDestinazionejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mailPieceDateTimejLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APCodARjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceCodejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APTypejComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APCodejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceProductjLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APDestinatariojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(APProductjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceProductjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDateTimejLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APIndirizzojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APCAPjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDateTimejLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APValAssjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(APDatejXDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APTimejSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDateTimejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceLongjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APNomeMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceLatjLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APImpContrjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mailPieceLatjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APPesojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceLongjLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APDestMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceLatjLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(APIBANjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mailPieceLongjLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(APPrezzojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceCodejLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(APIndMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(APCAPMittjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mailPieceLongjLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mailPieceLongjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(APPercettojTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(mailPieceDatajPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(APOraTimeDefjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TestjButton, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(mailPieceDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mailPieceDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mailPieceDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(42, 42, 42))
        );

        TestjButton.getAccessibleContext().setAccessibleParent(panelTitlejLabel);

        javax.swing.GroupLayout workingjPanelLayout = new javax.swing.GroupLayout(workingjPanel);
        workingjPanel.setLayout(workingjPanelLayout);
        workingjPanelLayout.setHorizontalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workingjPanelLayout.createSequentialGroup()
                .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(workingjPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(workingjPanelLayout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mailPieceDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(6, Short.MAX_VALUE))
        );
        workingjPanelLayout.setVerticalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mailPieceDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 365, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 147, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField APCAPMittjTextField;
    private javax.swing.JTextField APCAPjTextField;
    private javax.swing.JTextField APCodARjTextField;
    private javax.swing.JTextField APCodClientejTextField;
    private javax.swing.JTextField APCodejTextField;
    private org.jdesktop.swingx.JXDatePicker APDatejXDatePicker;
    private javax.swing.JTextField APDestMittjTextField;
    private javax.swing.JTextField APDestinatariojTextField;
    private javax.swing.JTextField APDestinazionejTextField;
    private javax.swing.JTextField APFrazjTextField;
    private javax.swing.JTextField APIBANjTextField;
    private javax.swing.JTextField APIdOpjTextField;
    private javax.swing.JTextField APImpContrjTextField;
    private javax.swing.JTextField APIndMittjTextField;
    private javax.swing.JTextField APIndirizzojTextField;
    private javax.swing.JTextField APNomeMittjTextField;
    private javax.swing.JTextField APNumSpedjTextField;
    private javax.swing.JTextField APOraTimeDefjTextField;
    private javax.swing.JTextField APPercettojTextField;
    private javax.swing.JTextField APPesojTextField;
    private javax.swing.JTextField APPrezzojTextField;
    private javax.swing.JTextField APProductjTextField;
    private javax.swing.JComboBox APSAjComboBox;
    private javax.swing.JSpinner APTimejSpinner;
    private javax.swing.JComboBox APTypejComboBox;
    private javax.swing.JTextField APValAssjTextField;
    private javax.swing.JButton TestjButton;
    private javax.swing.JButton addAccettazionePezzijButton;
    private javax.swing.JButton backjButton;
    private javax.swing.JPanel commandjPanel;
    private javax.swing.JButton exitjButton;
    private javax.swing.JButton generaXmlDatajButton;
    private javax.swing.JButton homejButton;
    private javax.swing.JButton impostaDatiWSjButton;
    private javax.swing.JLabel mailPieceCodejLabel;
    private javax.swing.JLabel mailPieceCodejLabel1;
    private javax.swing.JLabel mailPieceCodejLabel2;
    private javax.swing.JLabel mailPieceCodejLabel3;
    private javax.swing.JLabel mailPieceCodejLabel4;
    private javax.swing.JLabel mailPieceCodejLabel5;
    private javax.swing.JLabel mailPieceCodejLabel6;
    private javax.swing.JPanel mailPieceDataCommandjPanel;
    private javax.swing.JPanel mailPieceDatajPanel;
    private javax.swing.JScrollPane mailPieceDatajScrollPane;
    private javax.swing.JLabel mailPieceDateTimejLabel;
    private javax.swing.JLabel mailPieceDateTimejLabel1;
    private javax.swing.JLabel mailPieceDateTimejLabel2;
    private javax.swing.JLabel mailPieceDateTimejLabel3;
    private javax.swing.JLabel mailPieceLatjLabel;
    private javax.swing.JLabel mailPieceLatjLabel2;
    private javax.swing.JLabel mailPieceLatjLabel3;
    private javax.swing.JLabel mailPieceLongjLabel;
    private javax.swing.JLabel mailPieceLongjLabel1;
    private javax.swing.JLabel mailPieceLongjLabel2;
    private javax.swing.JLabel mailPieceLongjLabel3;
    private javax.swing.JLabel mailPieceLongjLabel4;
    private javax.swing.JLabel mailPieceProductjLabel;
    private javax.swing.JLabel mailPieceProductjLabel1;
    private javax.swing.JLabel mailPieceProductjLabel2;
    private javax.swing.JTable mailPiecejTable;
    private javax.swing.JScrollPane messageBarjScrollPane;
    private javax.swing.JTextPane messageBarjTextPane;
    private javax.swing.JLabel panelTitlejLabel;
    private javax.swing.JButton removeAccettazionePezzijButton;
    private javax.swing.JButton salvaXmlDatajButton;
    private javax.swing.JPanel workingjPanel;
    private javax.swing.JPanel xmlDataCommandjPanel;
    private javax.swing.JPanel xmlDatajPanel;
    private javax.swing.JScrollPane xmlDatajScrollPane;
    private javax.swing.JTextArea xmlDatajTextArea;
    // End of variables declaration//GEN-END:variables


}
