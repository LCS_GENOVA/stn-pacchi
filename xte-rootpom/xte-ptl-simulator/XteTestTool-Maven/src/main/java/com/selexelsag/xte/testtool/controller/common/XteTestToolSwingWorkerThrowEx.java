/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.common;

import com.selexelsag.xte.testtool.exception.BaseException;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public abstract class XteTestToolSwingWorkerThrowEx {
    private final SwingWorker<Void, Void> worker = new SimpleSwingWorker();
    private static Logger logger = Logger.getLogger(XteTestToolSwingWorkerThrowEx.class);

    private BaseException baseException = null;

    public BaseException getBaseException() {
        return baseException;
    }

    public void setBaseException(BaseException baseException) {
        this.baseException = baseException;
    }

    public void execute() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                before();
            }
        });

        worker.execute();
    }

    public void cancel(boolean mayInterruptIfRunning) {

        worker.cancel(mayInterruptIfRunning);
    }

    protected void before() {
        //Nothing by default
    }

    protected abstract void doInBackground() throws Exception;

    protected abstract void done();

    private class SimpleSwingWorker extends SwingWorker<Void, Void> {
        @Override
        protected Void doInBackground() throws Exception {
            XteTestToolSwingWorkerThrowEx.this.doInBackground();
            return null;
        }

        @Override
        protected void done() {
            try {
                get();
            } catch (final InterruptedException ex) {
                logger.error("XteTestToolSwingWorkerThrowEx done InterruptedException", ex);
                throw new RuntimeException(ex);
            } catch (final ExecutionException ex) {
                logger.error("XteTestToolSwingWorkerThrowEx done ExecutionException", ex);
                if(XteTestToolSwingWorkerThrowEx.this.getBaseException()!=null) {
                    throw XteTestToolSwingWorkerThrowEx.this.getBaseException();
                }
                else {
                    throw new RuntimeException(ex.getCause());
                }
            }

            XteTestToolSwingWorkerThrowEx.this.done();
        }

    }
}