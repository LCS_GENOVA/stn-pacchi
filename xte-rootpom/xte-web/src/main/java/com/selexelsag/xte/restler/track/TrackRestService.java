package com.selexelsag.xte.restler.track;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.model.track.TrackResponse;
import com.selexelsag.xte.restler.IBaseRestService;
import com.selexelsag.xte.service.context.SpringApplicationContext;
import com.selexelsag.xte.xte_presentation_logic.ITrackService;

/**
 * Implementazione del servizio REST di tracciatura
 * @author Frosi
 *
 */
@Path(IBaseRestService.REST_CONTEXT + ITrackRestService.TRACK_CONTEXT)
public class TrackRestService implements ITrackRestService{

	private ITrackService trackService;
	private static Log log = LogFactory.getLog(TrackRestService.class); 
	
	@POST
	@Path(ITrackRestService.EXECFLOW_SERVICE_NAME + "/{service}/{channel}/{sourceOfficeId}/{destinationOfficeId}/{objectId}/{eventName}/{version}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response executeFlow(
			@PathParam("service") StringBuilder service,
			@PathParam("channel") StringBuilder channel,
			@PathParam("sourceOfficeId") StringBuilder sourceOfficeId,
			@PathParam("destinationOfficeId") StringBuilder destinationOfficeId,
			@PathParam("objectId") StringBuilder objectId,
			@PathParam("eventName") StringBuilder eventName,
			@PathParam("version") StringBuilder version,
			@FormParam("dataTrack") StringBuilder dataTrack,
			@QueryParam("requestId") StringBuilder requestId
			)  {
		try{
			log.info(String.format("REST Service BEGINS: service=[%1$s], channel=[%2$s], sourceOfficeId=[%3$s], destinationOfficeId=[%4$s], objectId=[%5$s], eventName=[%6$s], version=[%7$s], requestid=[%8$s]", service, channel, sourceOfficeId, destinationOfficeId, objectId, eventName, version, requestId));
			TrackRequest trackRequest = getTrackRequest(service, channel, sourceOfficeId, destinationOfficeId, objectId, eventName, version, dataTrack, requestId);
			trackService = (ITrackService)SpringApplicationContext.getBean(ITrackService.TRACK_SRV_BEAN_NAME);
			TrackResponse trackResponse = trackService.processRequest(trackRequest);
			log.info(String.format("REST Service ENDS: trackResponse=[%1$s]", trackResponse));
			return Response.status(200).entity(trackResponse).build();
		}catch (Throwable t){
			log.info(String.format("Unexpected error processing rest request: service=[%1$s], channel=[%2$s], sourceOfficeId=[%3$s], destinationOfficeId=[%4$s], objectId=[%5$s], eventName=[%6$s], version=[%7$s], requestid=[%8$s]", service, channel, sourceOfficeId, destinationOfficeId, objectId, eventName, version, requestId));
			TrackResponse ans = new TrackResponse(service.toString(), channel.toString(), sourceOfficeId.toString(), destinationOfficeId.toString(), objectId.toString(), eventName.toString());
			ans.setErrorCode(ITrackService.RESULT_ERROR_INTERNAL_KEY);
			return Response.status(500).entity(ans).build();
		}
	}

	private TrackRequest getTrackRequest(StringBuilder service,StringBuilder channel,StringBuilder sourceOfficeId,StringBuilder destinationOfficeId,StringBuilder objectId,StringBuilder eventName,StringBuilder version,StringBuilder trackData, StringBuilder requestId){
		TrackRequest ans = new TrackRequest();
		ans.setService(service);
		ans.setChannel(channel);
		ans.setDestinationOfficeId(destinationOfficeId);
		ans.setSourceOfficeId(sourceOfficeId);		
		ans.setObjectId(objectId);
		ans.setEventName(eventName);
		ans.setVersion(version);
		ans.setTrackData(trackData);
		if (requestId != null && !requestId.toString().isEmpty())
			ans.setRequestId(requestId);
		return ans;
	}


	
	
	

}
