/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.ssip.test.token;


import junit.framework.Assert;

import org.junit.Test;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.elsagdatamat.dbtrace.bridge.engine.entities.Token;
import com.selexelsag.dbtrace.bridge.to.ssip.manager.token.SSIPTokenDecorator;
import com.selexelsag.dbtrace.bridge.to.ssip.manager.token.TokenDecorator;

/**
 * @author arodriguez
 *
 */
public class TokenDecoratorTest {

	@Test
	public void tokenDecoratorIsTokenWithToTokenValues() {
		ToToken ToToken = new ToToken();
		ToToken.setWsToken("testWsToken");
		TokenDecorator tokenDecorator = new TokenDecorator(new Token(), ToToken);
		Assert.assertNotNull(tokenDecorator);
		Assert.assertEquals(tokenDecorator.getServiceId(), ToToken.getServiceId());
		Assert.assertEquals(tokenDecorator.getWsToken(), ToToken.getWsToken());
	}
	
	@Test
	public void ToTokenDecoratorIsToTokenWithTokenValues() {
		Token token = new Token() {{
			setServiceId("testServiceId");
			setWsToken("testWsToken");
		}};
		SSIPTokenDecorator ToTokenDecorator = new SSIPTokenDecorator(new ToToken(), token);
		Assert.assertNotNull(ToTokenDecorator);
		Assert.assertEquals(ToTokenDecorator.getServiceId(), token.getServiceId());
		Assert.assertEquals(ToTokenDecorator.getWsToken(), token.getWsToken());
	}
	
}
