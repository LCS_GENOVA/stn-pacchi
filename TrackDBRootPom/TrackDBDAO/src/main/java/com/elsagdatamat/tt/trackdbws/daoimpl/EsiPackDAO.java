package com.elsagdatamat.tt.trackdbws.daoimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.framework.dao.hibernate.HibernatePrimitiveDAO;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;
import com.elsagdatamat.tt.trackdbws.beans.EsiPackPIFilter;
import com.elsagdatamat.tt.trackdbws.dao.IEsiPackDAO;

public class EsiPackDAO extends SpringHibernateJpaDAOBase<EsiPackPIFilter, String> implements IEsiPackDAO {

	class ParametricTraceDAO<T> {
		
		@SuppressWarnings("unchecked")
		@Transactional(propagation=Propagation.SUPPORTS)
		public List<T> executeHqlQuery(String hqlQuery) { 
			
			class MyJpaCallback<T1> implements JpaCallback {
				String hqlQuery; 
				
				public MyJpaCallback(String hqlQuery){
					this.hqlQuery = hqlQuery;
				}
				
				@Transactional(propagation=Propagation.SUPPORTS)
				public List<T1> doInJpa(EntityManager em) throws PersistenceException {
					return (List<T1>)HibernatePrimitiveDAO._executeHqlQuery(
																((HibernateEntityManager) em).getSession(),
																hqlQuery
																);
				}
			}
			
			MyJpaCallback<T> myJpaCallbak = new MyJpaCallback<T>(hqlQuery);
			List<T> listOfTObject = (List<T>) getJpaTemplate().execute(myJpaCallbak,true);
			
			return listOfTObject;
		}

	}

	@Transactional(propagation=Propagation.SUPPORTS)
	public List<EsiPackPIFilter> returnEsiPackListExecuteHqlQuery(String hqlQuery) { 
		Log log = LogFactory.getLog(getClass());	
		log.info("returnTraceListExecuteHqlQuery. Inizio");
		
		ParametricTraceDAO<EsiPackPIFilter> esiPackEsiPackDAO = new ParametricTraceDAO<EsiPackPIFilter>();
	
		log.info("returnTraceListExecuteHqlQuery. Fine");
		return esiPackEsiPackDAO.executeHqlQuery(hqlQuery);
	}

}
