package com.elsagdatamat.dbtrace.bridge.tosda;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.datatype.XMLGregorianCalendar;

public class SDATrace {
	@XmlElement(name = "TRACED_ENTITY")
	public String tracedEntity;

	@XmlElement(name = "ID_TRACED_ENTITY")
	public String idTracedEntity;

	@XmlElement(name = "LABEL_TRACED_ENTITY")
	public String labelTracedEntity;
    
	@XmlElement(name = "WHEN_HAPPENED")
	public XMLGregorianCalendar whenHappened;
    
	@XmlElement(name = "WHERE_HAPPENED")
	public String whereHappened;
    
	@XmlElement(name = "WHAT_HAPPENED")
	public String whatHappened;
    
	@XmlElement(name = "WHAT_HAPPENED_DETAIL")
	public SDATraceWhatDetail whatHappenedDetail;
    
	@XmlElement(name = "WHERE_HAPPENED_DETAIL")
	public SDATraceWhereDetail whereHappenedDetail;
    
	@XmlElement(name = "TRACE_ENTITY_DETAIL")
	public SDATraceEntityDetail traceEntityDetail;
}
