/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.view.objectrenderer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableCellRenderer;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

/**
 *
 * @author Da Procida
 */
public class XteMailPieceTableCellRenderer extends DefaultTableCellRenderer {

    private List<String> columnWidthValues;

    public XteMailPieceTableCellRenderer(List<String> initialColumnWidthValues) {

        this.columnWidthValues = initialColumnWidthValues;
    }

    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

        defineTableFocus(table);

        for (int i = 0; i < table.getModel().getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setMinWidth(Integer.parseInt(columnWidthValues.get(i)));
            table.getColumnModel().getColumn(i).setMaxWidth(Integer.parseInt(columnWidthValues.get(i)));
            table.getColumnModel().getColumn(i).setPreferredWidth(Integer.parseInt(columnWidthValues.get(i)));
        }

        JLabel renderer = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);

        if(table.isEnabled())
        {
            renderer = new JLabel();
            renderer.setOpaque(true);
            renderer.setBackground(Color.WHITE);

            renderer.setFont(new Font("Tahoma", Font.BOLD, 12));
            renderer.setHorizontalAlignment(JLabel.CENTER);
            renderer.setForeground(new Color(51,51,51));
            
            String valueString;
            if (value == null) {
                valueString = "";
            }else if (value instanceof Date) {
                GregorianCalendar c = new GregorianCalendar();
                c.setTime((Date)value);
                try {
                    valueString = DatatypeFactory.newInstance().newXMLGregorianCalendar(c).toXMLFormat();
                } catch (DatatypeConfigurationException ex) {
                    valueString = value.toString();
                }
            }else {
                valueString = value.toString();
            }
            
            renderer.setText(valueString);
            
            if(isSelected)
            {
                renderer.setBackground(new Color(153, 204, 255));
            }
            else
            {
                renderer.setBackground(Color.white);
            }

        }
        else
        {
            renderer.setForeground(new Color(204,204,204));
        }
        
        return renderer;
    }

    private void defineTableFocus(JTable table)
    {
        Action tabOut = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
            }
        };

        Action stabOut = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                KeyboardFocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent();
            }
        };

        table.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabOut");
        table.getActionMap().put("tabOut",tabOut);

        table.getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, java.awt.event.InputEvent.SHIFT_DOWN_MASK ), "stabOut");
        table.getActionMap().put("stabOut",stabOut);


    }
    
}
