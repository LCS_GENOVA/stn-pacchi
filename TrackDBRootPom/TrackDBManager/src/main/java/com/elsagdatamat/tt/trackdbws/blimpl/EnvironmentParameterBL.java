package com.elsagdatamat.tt.trackdbws.blimpl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.tt.trackdbws.beans.EnvironmentParameter;
import com.elsagdatamat.tt.trackdbws.bl.IEnvironmentParameterBL;
import com.elsagdatamat.tt.trackdbws.dao.IEnvironmentParameterDAO;

public class EnvironmentParameterBL implements IEnvironmentParameterBL {

	@Autowired
	IEnvironmentParameterDAO environmentParameterDAO;

	public static enum DuplicateManagementStrategy {
		SAFE("SAFE"), UNSAFE("UNSAFE");

		private String code;

		private DuplicateManagementStrategy(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public static DuplicateManagementStrategy fromCode(String code) {
			if (code != null && code.equalsIgnoreCase("SAFE")) {
				return SAFE;
			}
			return UNSAFE; // DEFAULT VALUE
		}

	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public String getParamValue(String paramName) {
		Log log = LogFactory.getLog(getClass());
		log.info("getParamValue. Inizio");

		EnvironmentParameter configuration = environmentParameterDAO.findById(paramName, false);

		log.info("getParamValue. Fine");
		return (configuration == null ? null : configuration.getParamValue());
	}

	@Override
	public DuplicateManagementStrategy getDuplicateManagementStrategy() {
		String duplicateManagementStrategy = getParamValue("DuplicateManagementStrategy");
		return DuplicateManagementStrategy.fromCode(duplicateManagementStrategy);
	}

}
