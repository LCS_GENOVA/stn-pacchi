package com.elsagdatamat.dbtrace.bridge.engine.bl;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItem;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition;

public interface AptItemBL {

	@Transactional(value = "tt-transactionManager", propagation = Propagation.REQUIRED)
	public void insertOrUpdate(AptItem item);

	public String getSubPType(String codPacco);

	public void insert(AptItem item);

	public AptItem find(String itemCode);

	public void insertAptItemAndAptItemPosition(AptItem item, AptItemPosition itemPosition);

	public void insertAptItemAndUpdateAptItemPosition(AptItem item, AptItemPosition itemPosition);
}
