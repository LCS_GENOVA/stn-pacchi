/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.ao;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.ao.XteTestToolWebServiceAOView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;

import com.elsagdatamat.tt.webservices.TrackResponse;
import com.selexelsag.xte.testtool.wsclient.TrackingClient;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolWebServiceAOController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener {

    private XteTestToolWebServiceAOView webServiceAOView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolWebServiceAOController.class);
    private List<JTextComponent> mailPieceFieldList = new ArrayList<JTextComponent>();
    private JFileChooser caricaXmlFileChooser = new JFileChooser();;
    
    private XteRequestDataModel dataModel;

    public XteTestToolWebServiceAOController(JPanel webServiceAOView) {
        try {
            logger.info("XteTestToolWebServiceAOController start class creation");
            this.webServiceAOView = (XteTestToolWebServiceAOView) webServiceAOView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.webServiceAOView.initializeView();
            addViewObjectsListeners();
            logger.info("XteTestToolWebServiceAOController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolWebServiceAOController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolWebServiceAOController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Web Service Accettazione Online controller creation failed", ex);
        }
    }

    public XteRequestDataModel getDataModel() {
        return dataModel;
    }

    public void setDataModel(XteRequestDataModel dataModel) {
        this.dataModel = dataModel;
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = webServiceAOView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        webServiceAOView.getParentFrame().setApplicationFrameModel(dataModel);
    }
    
    private void updateXteRequestDataModel() {
      if(dataModel != null)
        {
        webServiceAOView.getServiceTextRef().setText(dataModel.getServiceName());
        webServiceAOView.getChannelTextRef().setText(dataModel.getChannel());
        webServiceAOView.getEventTextRef().setText(dataModel.getEvent());
        webServiceAOView.getChannelIdTextRef().setText(dataModel.getChannelId());
        webServiceAOView.getVersionTextRef().setText(dataModel.getVersion());
        webServiceAOView.getXmlDataTextAreaRef().setText(dataModel.getXmlData());
        }
        
    }

    @Override
    public void addViewObjectsListeners() {
        this.webServiceAOView.getWebServicePanelRef().addAncestorListener(this);        
        
        this.webServiceAOView.getServiceTextRef().addFocusListener(this);
        this.webServiceAOView.getEventTextRef().addFocusListener(this);
        this.webServiceAOView.getChannelTextRef().addFocusListener(this);
        this.webServiceAOView.getChannelIdTextRef().addFocusListener(this);
        this.webServiceAOView.getVersionTextRef().addFocusListener(this);
        this.webServiceAOView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.webServiceAOView.getWsDataTextAreaRef().addFocusListener(this);    
        
        this.webServiceAOView.getCaricaXmlButtonRef().addActionListener(this);
        this.webServiceAOView.getChiamaWSButtonRef().addActionListener(this);
        
        this.webServiceAOView.getBackButtonRef().addActionListener(this);
        this.webServiceAOView.getHomeButtonRef().addActionListener(this);
        this.webServiceAOView.getExitButtonRef().addActionListener(this);
        
        this.caricaXmlFileChooser.addActionListener(this);
                    
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(webServiceAOView.getCaricaXmlButtonRef()))
        {
            logger.debug("XteTestToolWebServiceAOController actionPerformed invoke caricaXml");
            caricaXml();
        }

        if(source.equals(webServiceAOView.getChiamaWSButtonRef()))
        {
            logger.debug("XteTestToolWebServiceAOController actionPerformed invoke chiamaWebService");
            chiamaWebService();
        }

        if(source.equals(webServiceAOView.getBackButtonRef()))
        {
            logger.debug("XteTestToolWebServiceAOController actionPerformed invoke back");
            back();
        }

        if(source.equals(webServiceAOView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolWebServiceAOController actionPerformed invoke home");
            home();
        }

        if(source.equals(webServiceAOView.getExitButtonRef()))
        {
            logger.debug("XteTestToolWebServiceAOController actionPerformed invoke exit");
            exit();
        }

        if(source.equals(caricaXmlFileChooser))
        {
            logger.debug("XteTestToolWebServiceAOController actionPerformed invoke carica XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                if (selectedFile == null) {
                    return;
                }
                OutputStream out = null;
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    if (!readXmlFile(selectedFile, webServiceAOView.getXmlDataTextAreaRef())) {
                        logger.error("XteTestToolWebServiceAOController caricaXml failed load XML file");
                        throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Accettazione Online controller caricaXml failed load XML file");
                    }
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolWebServiceAOView)
        {
            logger.debug("XteTestToolWebServiceAOController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolWebServiceAOController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Web Service Accettazione Online controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolWebServiceAOController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Web Service Accettazione Online controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }    
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        webServiceAOView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolWebServiceAOController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.webServiceAOView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(webServiceAOView);
            updateXteTestToolDataModel(dataModel);
            
            updateXteRequestDataModel();

            clearMessageBar();
            
            /*
            Configuration configuration = XteTestToolConfiguration.getInstance().getConfiguration();
            List<String> frazList = configuration.getList("PostaRegistrata.OfficeId.List");
            DefaultComboBoxModel model = new DefaultComboBoxModel(frazList.toArray());                                    
            webServiceDPView.getFrazionariComboBoxRef().setModel(model);
            webServicePRView.getFrazionariComboBoxRef().setSelectedIndex(-1);
            */

            webServiceAOView.getServiceTextRef().requestFocus();

            logger.info("XteTestToolWebServiceAOController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolWebServiceAOController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolWebServiceAOController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Web Service Accettazione Online controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolWebServiceAOController start exit");
        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(webServiceAOView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolWebServiceAOController exit in progress");
            webServiceAOView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolWebServiceAOController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(webServiceAOView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(webServiceAOView.getWebServicePanelRef().getParent(), webServiceAOView.getWebServicePanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolWebServiceAOController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(webServiceAOView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(webServiceAOView.getWebServicePanelRef().getParent(), webServiceAOView.getWebServicePanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private void impostaDatiUfficio()
    {
        logger.info("XteTestToolWebServiceAOController start impostaDatiUfficio");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            /*
            String selectedOffice = (String)webServiceAOView.getFrazionariComboBoxRef().getSelectedItem();
            webServiceAOView.getChannelIdTextRef().setText(selectedOffice);
            */
            logger.info("XteTestToolWebServicePRController impostaDatiUfficio executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolWebServicePRController impostaDatiUfficio failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolWebServiceAOController impostaDatiUfficio failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Web Service Posta Registrata controller impostaDatiUfficio failed", ex);
        }
    }
    
    private void caricaXml()
    {
        try {
            logger.info("XteTestToolWebServiceAOController start caricaXml");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            caricaXmlFileChooser.setCurrentDirectory(new File("."));
            caricaXmlFileChooser.setSize(new Dimension(350, 200));
            caricaXmlFileChooser.setFileFilter(new XmlFileFilter());
            caricaXmlFileChooser.showOpenDialog(webServiceAOView.getParentFrame());
                                    
            logger.info("XteTestToolWebServiceAOController caricaXml executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolWebServiceAOController caricaXml Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolWebServiceAOController caricaXml failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Web Service Accettazione Online controller caricaXml failed", ex);
        }
    }
    
    private boolean readXmlFile(File xmlFile, JTextComponent pane) {
        FileReader fr = null;
        try {
            fr = new FileReader(xmlFile);
            pane.read(fr, null);
            fr.close();
            return true;
        }
        catch (IOException e) {
            return false;
        }
        finally {
            try {
                fr.close();
            } catch (IOException ex) {
            }
        }                
    }

    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }
    
    private static String trackResponseToString(TrackResponse response) {
        StringBuilder sb = new StringBuilder();
        sb.append("ServiceName: " + response.getServiceName() + "\n");
        sb.append("Channel: " + response.getChannel() + "\n");
        sb.append("ChannelId: " + response.getChannelId()+ "\n");
        sb.append("EventName: " + response.getEventName() + "\n");
        sb.append("ErrorCode: " + response.getErrorCode() + "\n");
        sb.append("ErrorDesc: " + response.getErrorDesc() + "\n");
        sb.append("ErrorId: " + response.getErrorId() + "\n");
        sb.append("RetryFlag: " + response.getRetryFlag() + "\n");
        sb.append("OutputParam:\n" + response.getOutputParam());
        return sb.toString();
    }

    private void chiamaWebService()
    {
        logger.info("XteTestToolWebServiceAOController start chiamaWebService");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            dataModel.setChannelId(webServiceAOView.getChannelIdTextRef().getText());
            XteTestToolDataModel serverData = webServiceAOView.getParentFrame().getApplicationFrameModel();
            String serverUrl = serverData.getCurrentXteServer().getServerBaseURL();
            TrackingClient client = new TrackingClient(serverUrl);
            TrackResponse response = client.trackMessage(dataModel.getServiceName(), 
                    dataModel.getChannel(), 
                    dataModel.getChannelId(), 
                    dataModel.getEvent(),
                    dataModel.getVersion(), 
                    dataModel.getXmlData());
            webServiceAOView.getWsDataTextAreaRef().setText(trackResponseToString(response));

            logger.info("XteTestToolWebServiceAOController chiamaWebService executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolWebServiceAOController chiamaWebService failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolWebServiceAOController chiamaWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Web Service Accettazione Online controller chiamaWebService failed", ex);
        }
    }
}
