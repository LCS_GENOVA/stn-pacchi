package com.selexelsag.xte.xte_presentation_logic;

import com.selexelsag.xte.model.track.TrackRequest;
import com.selexelsag.xte.model.track.TrackResponse;

/**
 * Servizio di tracking oggetti postali
 *
 */
public interface ITrackService 
{
	public static final String TRACK_SRV_BEAN_NAME = "TrackService";
	
	/**
	 * Innesca il processamento di tracciatura dell'evento specificato
	 * @param trackRequest contiene tutti gli elementi dell'evento da tracciare
	 * @return l'output da inviare al client
	 */
	public TrackResponse processRequest(TrackRequest trackRequest);
	
	public static String RESULT_OK_KEY = "OkKey";
	public static String RESULT_ERROR_NOK_KEY = "ErrorNokKey";
	public static String RESULT_ERROR_INTERNAL_KEY = "ErrorInternalKey";
	public static String RESULT_ERROR_INTERNAL_CONFIGURATION_KEY = "ErrorInternalConfiguationKey";
}
