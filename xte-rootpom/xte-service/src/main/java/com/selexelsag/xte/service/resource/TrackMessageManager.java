package com.selexelsag.xte.service.resource;

import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TrackMessageManager implements IMessageManager 
{
	private ResourceBundle resources = null;
	private final static String bundleName= "trackMessages";
	
	private static Log log = LogFactory.getLog(TrackMessageManager.class); 
	
	public void init() throws Exception
	{
		try
		{
			resources = ResourceBundle.getBundle(bundleName);
		}
		catch (Throwable t)
		{
			log.error(String.format("Error initializing track messaging: bundleName=[%1$s]", bundleName));
			throw new Exception(String.format("Error initializing track messaging: bundleName=[%1$s]", bundleName), t);
		}
	}
	
	@Override
	public String getMessageText(String errorMessageCode) 
	{
		return resources.getString(errorMessageCode);
	}
}