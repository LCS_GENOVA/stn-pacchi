package com.elsagdatamat.dbtrace.bridge.toedwh;

public class EDWHStati {
	public String codice="";
	public String descrizione="";
	public String toCsv()
	{
		return 
			"\""+((codice!=null)?codice:"")+"\","+
			"\""+((descrizione!=null)?descrizione:"")+"\"\n";
	}
}
