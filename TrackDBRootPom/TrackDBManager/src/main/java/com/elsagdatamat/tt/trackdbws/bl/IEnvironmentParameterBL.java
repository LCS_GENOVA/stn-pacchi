package com.elsagdatamat.tt.trackdbws.bl;

import com.elsagdatamat.tt.trackdbws.blimpl.EnvironmentParameterBL.DuplicateManagementStrategy;

public interface IEnvironmentParameterBL {

	public String getParamValue(String paramName);

	public DuplicateManagementStrategy getDuplicateManagementStrategy();

}
