package com.elsagdatamat.dbtrace.validation.event;

import java.io.Serializable;

import com.elsagdatamat.dbtrace.bridge.tosda.SDATrace;

public class NonCompliantTrace implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private SDATrace sdaTrace;
	private String reason;
	private int position;
	
	public NonCompliantTrace(SDATrace sdaTrace, String reason, int position) {
		super();
		this.sdaTrace = sdaTrace;
		this.reason = reason;
		this.position = position;
	}
	public SDATrace getSdaTrace() {
		return sdaTrace;
	}
	public void setSdaTrace(SDATrace sdaTrace) {
		this.sdaTrace = sdaTrace;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
}
