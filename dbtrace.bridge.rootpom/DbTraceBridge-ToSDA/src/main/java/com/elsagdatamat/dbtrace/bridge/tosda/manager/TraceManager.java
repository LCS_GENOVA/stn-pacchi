package com.elsagdatamat.dbtrace.bridge.tosda.manager;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.elsagdatamat.dbtrace.bridge.engine.bl.ToToken;
import com.elsagdatamat.dbtrace.bridge.tosda.dao.ITraceDAO;
import com.elsagdatamat.tt.trackdbws.beans.Trace;

@Service
public class TraceManager implements ITraceManager {
	
	private static final Log log = LogFactory.getLog(TraceManager.class);

	private ITraceDAO traceDAO;
	public void setTraceDAO(ITraceDAO traceDAO) {
		this.traceDAO = traceDAO;
	}
	
	private ToSDACache cache;

	@Override
	public List<Trace> loadNextTraces(
				ToToken token,
				Date stopDate,
				Integer chunkSize,
				Integer cacheSize,
				Integer queryMinutesInterval) {
		
		if (cache == null) {
			cache = new ToSDACache(token, stopDate, chunkSize, cacheSize, queryMinutesInterval, traceDAO);
		}
		cache.setStopDate(stopDate);
		List<Trace> list = cache.getNextChunk();
		
		return list;
	}
}
