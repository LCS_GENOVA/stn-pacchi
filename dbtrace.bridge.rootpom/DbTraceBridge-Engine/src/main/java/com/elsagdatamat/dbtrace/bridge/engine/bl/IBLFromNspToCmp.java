package com.elsagdatamat.dbtrace.bridge.engine.bl;

import com.elsagdatamat.dbtrace.bridge.engine.entities.FromNspToCmp;
import com.elsagdatamat.dbtrace.bridge.engine.entities.FromNspToCmpPK;

public interface IBLFromNspToCmp extends IManager<FromNspToCmp, FromNspToCmpPK> {

}
