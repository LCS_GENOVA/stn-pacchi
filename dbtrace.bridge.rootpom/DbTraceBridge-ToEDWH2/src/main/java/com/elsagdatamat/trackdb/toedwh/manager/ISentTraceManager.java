package com.elsagdatamat.trackdb.toedwh.manager;

import java.util.List;

import com.elsagdatamat.trackdb.toedwh.entities.SentTrace;
import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSend;

public interface ISentTraceManager {

	void insertSentTraces(List<SentTrace> listSentTraces);
	void insertSentTracesBatch(List<SentTrace> listSentTraces);

	void fillSentTraces(Long id, List<TraceTypesToSend> typesToSend);
}
