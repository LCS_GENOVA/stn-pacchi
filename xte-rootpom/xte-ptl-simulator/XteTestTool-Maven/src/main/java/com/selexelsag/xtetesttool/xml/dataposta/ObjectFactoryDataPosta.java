package com.selexelsag.xtetesttool.xml.dataposta;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.selexelsag.xtetesttool.xml.dataposta package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactoryDataPosta {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.selexelsag.xtetesttool.xml.dataposta
     * 
     */
    public ObjectFactoryDataPosta() {
    }

    /**
     * Create an instance of {@link DataPosta.SM }
     * 
     */
    public DataPosta.SM createDataPostaSM() {
        return new DataPosta.SM();
    }

    /**
     * Create an instance of {@link DataPosta }
     * 
     */
    public DataPosta createDataPosta() {
        return new DataPosta();
    }

    /**
     * Create an instance of {@link DataPosta.LM }
     * 
     */
    public DataPosta.LM createDataPostaLM() {
        return new DataPosta.LM();
    }

    /**
     * Create an instance of {@link DataPosta.AM }
     * 
     */
    public DataPosta.AM createDataPostaAM() {
        return new DataPosta.AM();
    }

    /**
     * Create an instance of {@link DataPosta.NM }
     * 
     */
    public DataPosta.NM createDataPostaNM() {
        return new DataPosta.NM();
    }
    
    /**
     * Create an instance of {@link DataPosta.AO }
     * 
     */
    public DataPosta.AO createDataPostaAO() {
        return new DataPosta.AO();
    }

    /**
     * Create an instance of {@link DataPosta.PR }
     * 
     */
    public DataPosta.PR createDataPostaPR() {
        return new DataPosta.PR();
    }

}
