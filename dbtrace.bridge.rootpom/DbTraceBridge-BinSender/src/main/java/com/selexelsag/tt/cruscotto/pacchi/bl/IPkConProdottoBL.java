package com.selexelsag.tt.cruscotto.pacchi.bl;

import java.util.List;

import com.selexelsag.tt.cruscotto.pacchi.bean.PkConProdotto;
import com.selexelsag.tt.cruscotto.pacchi.bean.PkConProdottoPK;


public interface IPkConProdottoBL {
	public Boolean getElemento(String prodotto, String canale, String tipoTraccia);
	public Boolean getElemento(PkConProdottoPK pk);
	public List<PkConProdotto> getLista();
}
