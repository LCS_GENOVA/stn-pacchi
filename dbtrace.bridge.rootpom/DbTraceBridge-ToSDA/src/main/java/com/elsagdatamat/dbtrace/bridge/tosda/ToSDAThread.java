package com.elsagdatamat.dbtrace.bridge.tosda;

import java.util.Date;
import java.util.logging.Logger;

public class ToSDAThread extends Thread {
	
	private final static Logger log = Logger.getLogger("ToSDA-query-check");
	
	private final IToSDAThread callback;
	
	public ToSDAThread(IToSDAThread callback) {
		this.callback = callback;
	}
	
	@Override
	public void start() {
		Date lastAckDate = new Date();
		Date lastLogDate = new Date();
		while(!callback.checkStop()) {
			try {
				Thread.sleep(1000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Date currentDate = new Date();
			if (currentDate.getTime() - lastLogDate.getTime() > 1000*60) {
				lastLogDate = new Date();
				//TODO: un log furbo...
			}
			if (currentDate.getTime() - lastAckDate.getTime() > 1000*60*10) {
				lastAckDate = new Date();
				callback.onStep();
			}
		}
	}
	
	public static interface IToSDAThread {
		boolean checkStop();
		void onStep();
	}
}
