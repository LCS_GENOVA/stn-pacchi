package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItem;
import com.elsagdatamat.dbtrace.bridge.engine.entities.AptItemPosition;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface AptItemDao extends IGenericDAO<AptItem, String> {

	void insertAptItemAndAptItemPosition(AptItem item, AptItemPosition itemPosition);

	void insertAptItemAndUpdateAptItemPosition(AptItem item, AptItemPosition itemPosition);

}
