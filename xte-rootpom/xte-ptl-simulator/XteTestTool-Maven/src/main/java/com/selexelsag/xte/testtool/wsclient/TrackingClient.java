/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.wsclient;

import com.elsagdatamat.tt.webservices.TrackResponse;
import com.elsagdatamat.tt.webservices.Tracking;
import com.elsagdatamat.tt.webservices.TrackingService;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;

/**
 *
 * @author xte-user
 */
public class TrackingClient {
    
    public TrackingClient(String serviceLocation) {
        String wsdlLocation = serviceLocation + "?wsdl";
        URL wsdlUrl;
        try {
            wsdlUrl = new URL(wsdlLocation);
        } catch (MalformedURLException ex) {
            Logger.getLogger(TrackingClient.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
        
        //trackingService = new TrackingService(wsdlUrl);
        trackingService = new TrackingService(wsdlUrl, new QName("http://webservices.tt.elsagdatamat.com/", "TrackingService"));
        trackingStub = trackingService.getTrackingPort();
    }
    
    private TrackingService trackingService;
    private Tracking trackingStub;
    
    public TrackResponse trackMessage(
        String serviceName,
        String channel,
        String channelID,
        String eventName,
        String version,
        String trackData) {
        
        TrackResponse response = trackingStub.trackMessage(serviceName, channel, channelID, eventName, version, trackData);
        
        return response;
    }
    
    public static void main(String[] args) {
        TrackingClient client = new TrackingClient("http://test-platform:8080/XTE/soap/Tracking");
        TrackResponse response = client.trackMessage("PR", "AO", "12340", "AC", "1.1", "<Track></Track>");
        Logger.getLogger(TrackingClient.class.getName()).log(Level.INFO, "errcode: " + response.getErrorCode() + ", errdesc: " + response.getErrorDesc() + ", errid: " + response.getErrorId());
        
    }
}
