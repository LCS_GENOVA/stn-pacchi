package com.elsagdatamat.tt.trackdbws.bl;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.tt.trackdbws.beans.Trace;

public interface ITraceBLSupport {

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public long insertTrace(List<Trace> listOfTraces);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean insertTrace(Trace trace);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean insertTrace(Trace trace, boolean mergeDetachedEntity);

}
