<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ED="http://www.elsagdatamat.com/Tracking/Messages/xslt/P2toM2Transform">
   <xsl:output indent="yes" method="xml" omit-xml-declaration="no" encoding="UTF-8"/>
	
	<!-- INCLUDED STYLESHEET -->
	<xsl:include href="MessageConverter-Common.xsl" />
	
	<xsl:template match="/MSG">
      <xsl:element name="MSG">
		   <xsl:apply-templates select="HDR" mode="HDRTemplate" />
		   <xsl:apply-templates select="P2" mode="P2Template" />
      </xsl:element>
   </xsl:template>
   
   <!-- TAG HDR -->
	<xsl:template mode="HDRTemplate" match="HDR">
      <xsl:element name="HDR">
         <xsl:call-template name="HDRAttributesTemplate">
			<xsl:with-param name="frazionarioParam" select="@AGNZ" />            
            <xsl:with-param name="swrelParam" select="$gExtSwRel" />            
         </xsl:call-template>
      </xsl:element>      
	</xsl:template>

	<!-- TAG P2 -->   
	<xsl:template mode="P2Template" match="P2">
	   <xsl:element name="M2">			
			<xsl:apply-templates select="OBJ" mode="OBJTemplate" />			
	   </xsl:element>
	</xsl:template>
   	
	<!-- Attributi TAG HDR -->
	<xsl:template name="HDRAttributesTemplate">		
		<xsl:param name="frazionarioParam"/>		
		<xsl:param name="swrelParam"/>
			<xsl:attribute name="OFCID"><xsl:value-of select="$frazionarioParam" />
			</xsl:attribute>
			<xsl:attribute name="SWREL"><xsl:value-of select="$swrelParam" />
			</xsl:attribute>
	</xsl:template>   
	
	<xsl:template mode="OBJTemplate" match="OBJ" xmlns:p2ExtFun="xalan://com.elsagdatamat.tracktrace.core.infrastructure.trackmessages.converter.P2TrackMessageConverter">
		<xsl:element name="OBJ">
			<xsl:element name="OBJID"><xsl:value-of select="OBJID" /></xsl:element>
			<xsl:element name="PH"><xsl:value-of select="$gExtPacchiPhaseAcc" /></xsl:element>			
			<xsl:element name="PSTF"><xsl:value-of select="$gExtPacchiP2Pstf" /></xsl:element>
			<xsl:element name="DF"><xsl:value-of select="$gExtPacchiP2Df" /></xsl:element>			
			<xsl:element name="OP"><xsl:value-of select="$gExtPacchiOperator" /></xsl:element>
			
			<xsl:if test="string-length(TDA/text())!=0 ">
				<xsl:element name="TDT"><xsl:value-of select="TDA" /></xsl:element>
			</xsl:if>
			<xsl:if test="string-length(TDA/text())=0 ">
				<xsl:element name="TDT"><xsl:value-of select="p2ExtFun:getCurrentDateTime()" /></xsl:element>
			</xsl:if>
			
			<xsl:element name="SUBC"><xsl:value-of select="PRD" /></xsl:element>
			<!-- Se e' restituito non ha senso il Flag Rinviato al mittente???  
			<xsl:if test="FRM/text() = '1'">
				<xsl:element name="BTSF">B</xsl:element>				
			</xsl:if>
			-->		
		</xsl:element>
		
	</xsl:template>   
</xsl:stylesheet>
