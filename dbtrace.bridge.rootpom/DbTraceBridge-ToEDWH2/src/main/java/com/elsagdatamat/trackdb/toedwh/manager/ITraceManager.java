package com.elsagdatamat.trackdb.toedwh.manager;

import java.util.List;

import com.elsagdatamat.trackdb.toedwh.entities.TraceTypesToSend;
import com.elsagdatamat.trackdb.toedwh.vo.TraceVO;


public interface ITraceManager {

	public List<TraceVO> find(Long id, Long maxRows,String traceType);

	public List<TraceVO> findByListOfType(Long id, List<TraceTypesToSend> findByDestinationTracedEntityChannel, Long maxRows);

	public int deleteTraces(Integer timeHorizon);

	public int bulkDeleteTraces(Integer timeHorizon, String traceType);

}
