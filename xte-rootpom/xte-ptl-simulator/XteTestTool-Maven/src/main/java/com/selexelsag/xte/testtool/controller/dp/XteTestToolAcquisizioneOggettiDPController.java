/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.controller.dp;

import com.selexelsag.xte.testtool.config.XteTestToolConfiguration;
import com.selexelsag.xte.testtool.controller.XteTestToolController;
import com.selexelsag.xte.testtool.controller.XteTestToolHomeController;
import com.selexelsag.xte.testtool.controller.XteTestToolPostalContextController;
import com.selexelsag.xte.testtool.controller.common.XteTestToolControllerException;
import com.selexelsag.xte.testtool.controller.common.XteTestToolFieldValidator;
import com.selexelsag.xte.testtool.datamodel.MessageType;
import com.selexelsag.xte.testtool.datamodel.XteAcquisizioneOggetto;
import com.selexelsag.xte.testtool.datamodel.XteRequestDataModel;
import com.selexelsag.xte.testtool.datamodel.XteTestToolDataModel;
import com.selexelsag.xte.testtool.exception.BaseException;
import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolHomeView;
import com.selexelsag.xte.testtool.view.dp.XteTestToolAcquisizioneOggettiDPView;
import com.selexelsag.xte.testtool.view.XteTestToolPostalContextView;
import com.selexelsag.xte.testtool.view.common.PanelLoader;
import com.selexelsag.xte.testtool.view.common.ViewComponentStatusManager;
import com.selexelsag.xte.testtool.view.common.XteTestToolMessageBar;
import com.selexelsag.xte.testtool.view.common.XteTestToolPostalContext;
import com.selexelsag.xte.testtool.view.dp.XteTestToolWebServiceDPView;
import com.selexelsag.xte.testtool.view.objectmodel.XteAcquisizioneOggettiTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteTestToolDuplicateKeyException;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta;
import com.selexelsag.xtetesttool.xml.dataposta.DataPosta.AO;
import com.selexelsag.xtetesttool.xml.dataposta.ao.DatiInvio;
import com.selexelsag.xtetesttool.xml.dataposta.ao.ObjectFactory;
import com.selexelsag.xtetesttool.xml.dataposta.ao.Traccia;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import org.apache.commons.configuration.Configuration;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.log4j.Logger;

/**
 *
 * @author Tassara
 */
public class XteTestToolAcquisizioneOggettiDPController implements XteTestToolController, AncestorListener, ActionListener, KeyListener, FocusListener, MouseListener, PropertyChangeListener, DocumentListener, ListSelectionListener, TableModelListener {

    private XteTestToolAcquisizioneOggettiDPView acquisizioneOggettiDPView;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolAcquisizioneOggettiDPController.class);
    private XteAcquisizioneOggetto selectedAcquisizioneOggetto = null;
    private int currentSelectedRow = -1;
    private List<JTextComponent> AcquisizioneOggettoFieldList = new ArrayList<JTextComponent>();
    private JFileChooser salvaXmlFileChooser = new JFileChooser();
    
    
    private JAXBContext context;
    
    private static Traccia acquisizioneOggettiTraccia;

    static {
        logger.debug("Creating Config Object Factory and Traccia");
        DataPosta dataPostaConfig = XteTestToolConfiguration.getInstance().getDataPostaConfig();
        ObjectFactory objectFactory = new ObjectFactory();
        AO config = dataPostaConfig.getAO();

        acquisizioneOggettiTraccia = objectFactory.createTraccia();
        acquisizioneOggettiTraccia.setEvento(config.getEventName());
        acquisizioneOggettiTraccia.setHeaderMazzetto(objectFactory.createHeaderMazzetto());
        acquisizioneOggettiTraccia.setListaInvii(objectFactory.createListaInvii());
    }

    public XteTestToolAcquisizioneOggettiDPController(JPanel acquisizioneOggettiDPView) {
        try {
            logger.info("XteTestToolAcquisizioneOggettiDPController start class creation");
            this.acquisizioneOggettiDPView = (XteTestToolAcquisizioneOggettiDPView) acquisizioneOggettiDPView;
            this.messages = ResourceBundle.getBundle("bundles/messages");
            this.acquisizioneOggettiDPView.initializeView();
	
            this.acquisizioneOggettiDPView.getAcquisizioneOggettoTypeComboBoxRef().addItem("S");
            this.acquisizioneOggettiDPView.getAcquisizioneOggettoTypeComboBoxRef().addItem("N");

            updateView();

            addViewObjectsListeners();

            context = JAXBContext.newInstance("com.selexelsag.xtetesttool.xml.dataposta.ao");

            logger.info("XteTestToolAcquisizioneOggettiDPController created successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController class creation failed", baseEx);
            throw baseEx;
        } catch (Exception ex) {
            logger.error("XteTestToolAcquisizioneOggettiDPController class creation failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Data Posta controller creation failed", ex);
        }
    }
    
    private void updateDataModel() {
        String id = acquisizioneOggettiDPView.getBundleIdTextRef().getText();
        String opid = acquisizioneOggettiDPView.getBundleTPIdTextRef().getText();
        logger.debug("updateDataModel - " + id + "," + opid);
        acquisizioneOggettiTraccia.getHeaderMazzetto().setCodiceIdentificativo(new String(id));
        acquisizioneOggettiTraccia.getHeaderMazzetto().setIdOperatoreTP(new String(opid));
        ObjectFactory objectFactory = new ObjectFactory();
        XteAcquisizioneOggetto[] entries = acquisizioneOggettiDPView.getAcquisizioneOggettoTableModelRef().getMap().values().toArray(new XteAcquisizioneOggetto[0]);
        logger.debug("entries count: " + entries.length);        
        acquisizioneOggettiTraccia.getListaInvii().getDatiInvio().clear();
        for (int i=0;i<entries.length;i++) {
        
            XteAcquisizioneOggetto item = entries[i];
            logger.debug("item " + i + ": " + item);
            if (item == null) 
                continue;
            DatiInvio obj = objectFactory.createDatiInvio();
            obj.setCodice(item.getAcquisizioneOggettoCode());
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(item.getAcquisizioneOggettoDateTime());
            try {
                obj.setDataOraNotifica(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            } catch (DatatypeConfigurationException ex) {
                logger.error("Error converting date " + c, ex);
                obj.setDataOraNotifica(null);
            }
            // obj.setAltroUfficio(item.getAcquisizioneOggettoOffice());
            obj.setTipoCodice(item.getAcquisizioneOggettoType());
            acquisizioneOggettiTraccia.getListaInvii().getDatiInvio().add(obj);
        }
    }
    
    
    private void updateView() {  
        String id = acquisizioneOggettiTraccia.getHeaderMazzetto().getCodiceIdentificativo();
        String opid = acquisizioneOggettiTraccia.getHeaderMazzetto().getIdOperatoreTP();
        logger.debug("updateView - " + id + "," + opid);
                
        if (id != null) {
            acquisizioneOggettiDPView.getBundleIdTextRef()
                    .setText(id);
        }

        if (opid != null) {
            acquisizioneOggettiDPView.getBundleTPIdTextRef()
                    .setText(opid);
        }

        acquisizioneOggettiDPView.getAcquisizioneOggettoTableModelRef().clearModel();
            
        for (int i=0;i<acquisizioneOggettiTraccia.getListaInvii().getDatiInvio().size();i++) {
            DatiInvio item = acquisizioneOggettiTraccia.getListaInvii().getDatiInvio().get(i);
            XteAcquisizioneOggetto obj = new XteAcquisizioneOggetto();
            obj.setAcquisizioneOggettoCode(item.getCodice());
            obj.setAcquisizioneOggettoDateTime(item.getDataOraNotifica().toGregorianCalendar().getTime());
            obj.setAcquisizioneOggettoType(item.getTipoCodice());
            acquisizioneOggettiDPView.getAcquisizioneOggettoTableModelRef().addAcquisizioneOggettoItem(obj);
        }
    }

    private XteTestToolDataModel getXteTestToolDataModel() {
        XteTestToolDataModel dataModel = acquisizioneOggettiDPView.getParentFrame().getApplicationFrameModel();
        return dataModel;
    }

    private void updateXteTestToolDataModel(XteTestToolDataModel dataModel) {
        acquisizioneOggettiDPView.getParentFrame().setApplicationFrameModel(dataModel);
    }

    @Override
    public void addViewObjectsListeners() {
        this.acquisizioneOggettiDPView.getAcquisizioneOggettiPanelRef().addAncestorListener(this);        
        this.acquisizioneOggettiDPView.getBundleIdTextRef().addFocusListener(this);
        this.acquisizioneOggettiDPView.getBundleTPIdTextRef().addFocusListener(this);
        //this.acquisizioneOggettiDPView.getAcquisizioneOggettoTypeComboBoxRef().addFocusListener(this);
        this.acquisizioneOggettiDPView.getAcquisizioneOggettoCodeTextRef().addFocusListener(this);
        this.acquisizioneOggettiDPView.getAcquisizioneOggettoTypeComboBoxRef().addActionListener(this);
        this.acquisizioneOggettiDPView.getAcquisizioneOggettoDatePickerRef().getEditor().addFocusListener(this);
        JFormattedTextField startTimejFormattedTextField = ((JSpinner.DateEditor)acquisizioneOggettiDPView.getAcquisizioneOggettoTimeSpinnerRef().getEditor()).getTextField();
        startTimejFormattedTextField.addFocusListener(this);
        this.acquisizioneOggettiDPView.getAcquisizioneOggettoOfficeTextRef().addFocusListener(this);
        this.acquisizioneOggettiDPView.getAcquisizioneOggettoTableRef().getSelectionModel().addListSelectionListener(this);
        this.acquisizioneOggettiDPView.getAcquisizioneOggettoTableRef().getModel().addTableModelListener(this);
        this.acquisizioneOggettiDPView.getXmlDataTextAreaRef().addFocusListener(this);        
        this.acquisizioneOggettiDPView.getAggiungiItemButtonRef().addActionListener(this);
        this.acquisizioneOggettiDPView.getEliminaItemButtonRef().addActionListener(this);
        this.acquisizioneOggettiDPView.getGeneraXmlDataButtonRef().addActionListener(this);
        this.acquisizioneOggettiDPView.getImpostaDatiWSButtonRef().addActionListener(this);
        this.acquisizioneOggettiDPView.getSalvaXmlDataButtonRef().addActionListener(this);
        this.acquisizioneOggettiDPView.getBackButtonRef().addActionListener(this);
        this.acquisizioneOggettiDPView.getHomeButtonRef().addActionListener(this);
        this.acquisizioneOggettiDPView.getExitButtonRef().addActionListener(this);
                
        this.acquisizioneOggettiDPView.getAcquisizioneOggettoCodeTextRef().getDocument().addDocumentListener(this);
        this.acquisizioneOggettiDPView.getAcquisizioneOggettoDatePickerRef().getEditor().getDocument().addDocumentListener(this);
        ((JSpinner.DateEditor)acquisizioneOggettiDPView.getAcquisizioneOggettoTimeSpinnerRef().getEditor()).getTextField().getDocument().addDocumentListener(this);
        this.acquisizioneOggettiDPView.getAcquisizioneOggettoOfficeTextRef().getDocument().addDocumentListener(this);
        
        AcquisizioneOggettoFieldList.add(acquisizioneOggettiDPView.getAcquisizioneOggettoCodeTextRef());
        AcquisizioneOggettoFieldList.add(acquisizioneOggettiDPView.getAcquisizioneOggettoDatePickerRef().getEditor());
        AcquisizioneOggettoFieldList.add(((JSpinner.DateEditor)acquisizioneOggettiDPView.getAcquisizioneOggettoTimeSpinnerRef().getEditor()).getTextField());
        AcquisizioneOggettoFieldList.add(acquisizioneOggettiDPView.getAcquisizioneOggettoOfficeTextRef());
        
		this.salvaXmlFileChooser.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source.equals(acquisizioneOggettiDPView.getAcquisizioneOggettoTypeComboBoxRef()))
        {
            checkAcquisizioneOggettoFieldsFull();
        }
        if(source.equals(acquisizioneOggettiDPView.getAggiungiItemButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneOggettiDPController actionPerformed invoke addAcquisizioneOggettoItem");
			
            updateDataModel();

            addAcquisizioneOggettoItem();
        }
        
        if(source.equals(acquisizioneOggettiDPView.getEliminaItemButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneOggettiDPController actionPerformed invoke removeAcquisizioneOggettoItem");

            updateDataModel();

            removeAcquisizioneOggettoItem();
        }

        if(source.equals(acquisizioneOggettiDPView.getGeneraXmlDataButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneOggettiDPController actionPerformed invoke generaXmlData");

			updateDataModel();

            generaXmlData();
        }

        if(source.equals(acquisizioneOggettiDPView.getImpostaDatiWSButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneOggettiDPController actionPerformed invoke impostaDatiWebService");

			updateDataModel();

            impostaDatiWebService();
        }

        if(source.equals(acquisizioneOggettiDPView.getSalvaXmlDataButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneOggettiDPController actionPerformed invoke salvaXmlData");

			updateDataModel();

            salvaXmlData();
        }

        if(source.equals(acquisizioneOggettiDPView.getBackButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneOggettiDPController actionPerformed invoke back");

			updateDataModel();

            back();
        }

        if(source.equals(acquisizioneOggettiDPView.getHomeButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneOggettiDPController actionPerformed invoke home");

			updateDataModel();

            home();
        }

        if(source.equals(acquisizioneOggettiDPView.getExitButtonRef()))
        {
            logger.debug("XteTestToolAcquisizioneOggettiDPController actionPerformed invoke exit");

			updateDataModel();

            exit();
        }
        
        if(source.equals(salvaXmlFileChooser))
        {
            logger.debug("XteTestToolAcquisizioneMazzettiPRController actionPerformed invoke salva XML File Chooser");
            JFileChooser theFileChooser = (JFileChooser) e.getSource();
            String command = e.getActionCommand();
            logger.debug("command: " + command);
            if (command.equals(JFileChooser.APPROVE_SELECTION)) {
                File selectedFile = theFileChooser.getSelectedFile();
                logger.debug("saving on: " + selectedFile);
                if (selectedFile == null) {
                    return;
                }
                PrintStream out = null;
                    
                if (selectedFile.getName().endsWith("xml") || selectedFile.getName().endsWith("XML")) {
                    try {
                        out = new PrintStream(selectedFile);
                        out.print(acquisizioneOggettiDPView.getXmlDataTextAreaRef().getText());
                        logger.debug("xml saved");
                    } catch (FileNotFoundException ex) {
                       logger.error("xml saving failed");
                            throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Web Service Posta Registrata controller xml save failed");
                    } finally {
                        out.close();
                    }
                }else {
                    throw new XteTestToolControllerException(Group.GENERIC, Severity.ERROR, "Wrong file extension");
                }
            } 
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        Object source = event.getSource();
        if(source instanceof XteTestToolAcquisizioneOggettiDPView)
        {
            logger.debug("XteTestToolAcquisizioneOggettiDPController ancestorAdded invoke init");
            init();
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {

    }

    @Override
    public void ancestorMoved(AncestorEvent event) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
          Component comp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
          int keycode = e.getKeyCode();

          if(comp instanceof JTextField)
          {
              if(keycode == KeyEvent.VK_CLEAR){
                ((JTextField) comp).setText("");
              }
          }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                ((JTextField)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JTextArea)
            {
                ((JTextArea)comp).setBackground(new Color(255,204,102));
            }
            if(comp instanceof JFormattedTextField)
            {
                ((JFormattedTextField)comp).setBackground(new Color(255,204,102));
            }

        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController focus gained failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Data Posta controller focus gained failed", ex);
        }
        
    }

    @Override
    public void focusLost(FocusEvent e) {
        Component comp = e.getComponent();
        try{
            if(comp instanceof JTextField)
            {
                if(((JTextField)comp).isEditable()) {
                    ((JTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JTextArea)
            {
                if(((JTextArea)comp).isEditable()) {
                    ((JTextArea)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JTextArea)comp).setBackground(new Color(220, 220, 220));
                }
            }
            if(comp instanceof JFormattedTextField)
            {
                if(((JFormattedTextField)comp).isEditable()) {
                    ((JFormattedTextField)comp).setBackground(Color.WHITE);
                }
                else {
                    ((JFormattedTextField)comp).setBackground(new Color(220, 220, 220));
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController focus lost failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Data Posta controller focus lost failed", ex);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if(source instanceof JTextField)
        {
            if(evt.getPropertyName().equals("editable"))
            {
                if(((JTextField)source).isEditable())
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(Color.WHITE);
                    }

                    ((JTextField)source).setForeground(new Color(51, 51, 51));
                }
                else
                {
                    if(((JTextField)source).hasFocus()) {
                        ((JTextField)source).setBackground(new Color(255,204,102));
                    }
                    else {
                        ((JTextField)source).setBackground(new Color(240, 240, 240));
                    }
                    ((JTextField)source).setForeground(new Color(0, 153, 51));
                }
            }
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        checkAcquisizioneOggettoFieldsFull();  
    }    
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        Object source = e.getSource();
        if(source.equals(acquisizioneOggettiDPView.getAcquisizioneOggettoTableRef().getSelectionModel()))
        {               
            if (e.getValueIsAdjusting()) {
                    return;
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
                        currentSelectedRow = acquisizioneOggettiDPView.getAcquisizioneOggettoTableRef().getSelectedRow();
                        logger.debug("XteTestToolAcquisizioneOggettiDPController valueChanged currentSelectedRow = " + currentSelectedRow);
                        logger.debug("XteTestToolAcquisizioneOggettiDPController valueChanged: " + ((XteAcquisizioneOggettiTableModel)acquisizioneOggettiDPView.getAcquisizioneOggettoTableRef().getModel()).getMap().size());
                        if(currentSelectedRow!=-1)
                        {
                            List<Component> enableComponents = new ArrayList<Component>();
                            enableComponents.add(acquisizioneOggettiDPView.getEliminaItemButtonRef());
                            componentStatusManager.enableComponent(enableComponents);
                            selectedAcquisizioneOggetto = ((XteAcquisizioneOggettiTableModel)acquisizioneOggettiDPView.getAcquisizioneOggettoTableRef().getModel()).getSelectedItem(currentSelectedRow);
                        }
                        else
                        {
                            List<Component> disableComponents = new ArrayList<Component>();
                            disableComponents.add(acquisizioneOggettiDPView.getEliminaItemButtonRef());
                            componentStatusManager.disableComponent(disableComponents);                                                        
                            selectedAcquisizioneOggetto = null;
                        }
                    }
                } );
        }
        
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int eventType = e.getType();
        DefaultTableModel model = (DefaultTableModel) e.getSource();
        if(model.equals(acquisizioneOggettiDPView.getAcquisizioneOggettoTableRef().getModel()))
        {               
            logger.debug("XteTestToolAcquisizioneOggettiDPController tableChanged: " + ((XteAcquisizioneOggettiTableModel)acquisizioneOggettiDPView.getAcquisizioneOggettoTableRef().getModel()).getMap().size());
        }
    }

    private void checkAcquisizioneOggettoFieldsFull() {

        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        for (JTextComponent field : AcquisizioneOggettoFieldList) {
             String fieldName = field.getAccessibleContext().getAccessibleName();
            if (field.getText().trim().isEmpty() && fieldName == null)
            {
                List<Component> disableComponents = new ArrayList<Component>();
                disableComponents.add(acquisizioneOggettiDPView.getAggiungiItemButtonRef());
                componentStatusManager.disableComponent(disableComponents);                                                        
                return;
            }
        }
        
        if(acquisizioneOggettiDPView.getAcquisizioneOggettoTypeComboBoxRef().getSelectedIndex()==-1)
        {
            List<Component> disableComponents = new ArrayList<Component>();
            disableComponents.add(acquisizioneOggettiDPView.getAggiungiItemButtonRef());
            componentStatusManager.disableComponent(disableComponents);                                                        
            return;            
        }

        List<Component> enableComponents = new ArrayList<Component>();
        enableComponents.add(acquisizioneOggettiDPView.getAggiungiItemButtonRef());
        componentStatusManager.enableComponent(enableComponents);
    }

    private void clearAcquisizioneOggettoFields() {

        acquisizioneOggettiDPView.getAcquisizioneOggettoTypeComboBoxRef().setSelectedIndex(-1);

        for (JTextComponent field : AcquisizioneOggettoFieldList) {
            if (!field.getText().trim().isEmpty()) {
                field.setText("");
            }
        }
    }
    
    private void clearMessageBar()
    {
        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        StyledDocument document = messageBar.removeContentFromMessageBar();
        acquisizioneOggettiDPView.getMessageBarTextPaneRef().setDocument(document);    
    }
    
    private void init()
    {
        try{
            logger.info("XteTestToolAcquisizioneOggettiDPController start init");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();

            this.acquisizioneOggettiDPView.customizeView();

            XteTestToolDataModel dataModel = getXteTestToolDataModel();
            dataModel.setCurrentXteTestToolPanelView(acquisizioneOggettiDPView);
            updateXteTestToolDataModel(dataModel);

            clearAcquisizioneOggettoFields();
            clearMessageBar();
            
            Configuration configuration = XteTestToolConfiguration.getInstance().getConfiguration();
            List<String> AcquisizioneOggettoTypeList = configuration.getList("DataPosta.MailPieceType.List");
            DefaultComboBoxModel model = new DefaultComboBoxModel(AcquisizioneOggettoTypeList.toArray());                                    
            acquisizioneOggettiDPView.getAcquisizioneOggettoTypeComboBoxRef().setModel(model);
            acquisizioneOggettiDPView.getAcquisizioneOggettoTypeComboBoxRef().setSelectedIndex(-1);

            List<Component> disabledComponents = new ArrayList<Component>();
            disabledComponents.add(acquisizioneOggettiDPView.getAggiungiItemButtonRef());
            disabledComponents.add(acquisizioneOggettiDPView.getEliminaItemButtonRef());
            disabledComponents.add(acquisizioneOggettiDPView.getImpostaDatiWSButtonRef());
            disabledComponents.add(acquisizioneOggettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disabledComponents);

            Calendar date = Calendar.getInstance();
            this.acquisizioneOggettiDPView.getAcquisizioneOggettoDatePickerRef().setDate(date.getTime());
            this.acquisizioneOggettiDPView.getAcquisizioneOggettoTimeSpinnerRef().setValue(date.getTime());

            /*
            XteMailPiece item = new XteMailPiece();
            item.setMailPieceType("N");
            item.setMailPieceCode("1234567890121");
            item.setMailPieceProduct("Racc");
            
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss.SSS");
            //SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            
            Date startDate = this.acquisizioneOggettiDPView.getMailPieceDatePickerRef().getDate();

            SpinnerDateModel startTimeSpinnerDateModel = (SpinnerDateModel) this.acquisizioneOggettiDPView.getMailPieceTimeSpinnerRef().getModel();
            Date startTime = startTimeSpinnerDateModel.getDate();

            String startDateStr = dateFormatter.format(startDate);
            String startTimeStr = timeFormatter.format(startTime);
            String startDateTimeStr = startDateStr + "T" + startTimeStr;
            
            item.setMailPieceDateTime(startDate);
            item.setMailPieceLatitude(123.123f);
            item.setMailPieceLongitude(12.2f);
            item.setMailPieceOffice("Altro ufficio 1");

            XteMailPiece item2 = new XteMailPiece();
            item2.setMailPieceType("N");
            item2.setMailPieceCode("1234567890122");
            item2.setMailPieceProduct("Racc");                        
            item2.setMailPieceDateTime(startDate);
            item2.setMailPieceLatitude(123.123f);
            item2.setMailPieceLongitude(12.2f);
            item2.setMailPieceOffice("Altro ufficio 2");

            XteMailPiece item3 = new XteMailPiece();
            item3.setMailPieceType("P");
            item3.setMailPieceCode("1234567890123");
            item3.setMailPieceProduct("Racc");                        
            item3.setMailPieceDateTime(startDate);
            item3.setMailPieceLatitude(123.123f);
            item3.setMailPieceLongitude(12.2f);
            item3.setMailPieceOffice("Altro ufficio 3");
            
            this.acquisizioneOggettiDPView.getMailPieceTableModelRef().addMailPieceItem(item);
            this.acquisizioneOggettiDPView.getMailPieceTableModelRef().addMailPieceItem(item2);
            this.acquisizioneOggettiDPView.getMailPieceTableModelRef().addMailPieceItem(item3);
 
            */
            acquisizioneOggettiDPView.getBundleIdTextRef().requestFocus();

            logger.info("XteTestToolAcquisizioneOggettiDPController init executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController init failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController init failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Data Posta controller init failed", ex);
        }
    }
    
    private void exit()
    {
        logger.info("XteTestToolAcquisizioneOggettiDPController start exit");
        clearMessageBar();
        Object[] dialogOptions = {String.valueOf(messages.getString("dialog.confirm.button")), String.valueOf(messages.getString("dialog.cancel.button"))};
        String title = messages.getString("xtetesttool.exit.confirm.title");
        String message = messages.getString("xtetesttool.exit.confirm.question");
        int answer = JOptionPane.showConfirmDialog(acquisizioneOggettiDPView, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION)
        {
            logger.debug("XteTestToolAcquisizioneOggettiDPController exit in progress");
            acquisizioneOggettiDPView.exit();
        }
    }

    private void back()
    {
        logger.info("XteTestToolAcquisizioneOggettiDPController start back");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);
        
        clearMessageBar();

        XteTestToolPostalContextView postalContextView = new XteTestToolPostalContextView(acquisizioneOggettiDPView.getParentFrame());
        XteTestToolPostalContextController postalContextController = new XteTestToolPostalContextController(postalContextView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(acquisizioneOggettiDPView.getAcquisizioneOggettiPanelRef().getParent(), acquisizioneOggettiDPView.getAcquisizioneOggettiPanelRef(), postalContextView, new Rectangle(0,70,1000,690));
    }

    private void home()
    {
        logger.info("XteTestToolAcquisizioneOggettiDPController start home");
        XteTestToolDataModel dataModel = getXteTestToolDataModel();
        dataModel.setCurrentPostalContext(null);
        dataModel.setCurrentPostalFunction(null);
        updateXteTestToolDataModel(dataModel);

        clearMessageBar();
        
        XteTestToolHomeView homeView = new XteTestToolHomeView(acquisizioneOggettiDPView.getParentFrame());
        XteTestToolHomeController homeController = new XteTestToolHomeController(homeView);
        PanelLoader panelLoader = PanelLoader.getInstance();
        panelLoader.changePanel(acquisizioneOggettiDPView.getAcquisizioneOggettiPanelRef().getParent(), acquisizioneOggettiDPView.getAcquisizioneOggettiPanelRef(), homeView, new Rectangle(0,70,1000,690));
    }

    private void addAcquisizioneOggettoItem()
    {
        logger.info("XteTestToolAcquisizioneOggettiDPController start addAcquisizioneOggettoItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            
            clearMessageBar();
            
            XteTestToolMessageBar messageBar = new XteTestToolMessageBar();

            XteTestToolFieldValidator validator = new XteTestToolFieldValidator();            
                                    
            XteAcquisizioneOggetto item = new XteAcquisizioneOggetto();
            
            String selectedItem = (String) acquisizioneOggettiDPView.getAcquisizioneOggettoTypeComboBoxRef().getModel().getSelectedItem();
            
            item.setAcquisizioneOggettoType(selectedItem);
            item.setAcquisizioneOggettoCode(acquisizioneOggettiDPView.getAcquisizioneOggettoCodeTextRef().getText());
            
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
            //SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");            
            Date startDate = this.acquisizioneOggettiDPView.getAcquisizioneOggettoDatePickerRef().getDate();
            SpinnerDateModel startTimeSpinnerDateModel = (SpinnerDateModel) this.acquisizioneOggettiDPView.getAcquisizioneOggettoTimeSpinnerRef().getModel();
            Date startTime = startTimeSpinnerDateModel.getDate();
            String startDateStr = dateFormatter.format(startDate);
            String startTimeStr = timeFormatter.format(startTime);
            String startDateTimeStr = startDateStr + "T" + startTimeStr;
            SimpleDateFormat lasttimeFormatter = new SimpleDateFormat("yyyy-MM-dd" + "'T'" + "HH:mm:ss");
            item.setAcquisizioneOggettoDateTime((Date) lasttimeFormatter.parse(startDateTimeStr));           

            item.setAcquisizioneOggettoOffice(acquisizioneOggettiDPView.getAcquisizioneOggettoOfficeTextRef().getText());

            try 
            {
                this.acquisizioneOggettiDPView.getAcquisizioneOggettoTableModelRef().addAcquisizioneOggettoItem(item);
            }
            catch (XteTestToolDuplicateKeyException duplicateEx)
            {
                StyledDocument document = messageBar.putContentOnMessageBar("Attemp to insert duplicate key Item!", MessageType.WARNING);
                acquisizioneOggettiDPView.getMessageBarTextPaneRef().setDocument(document);
                this.acquisizioneOggettiDPView.getAcquisizioneOggettoCodeTextRef().requestFocus();
                return;
            }
            
            StyledDocument document = messageBar.putContentOnMessageBar("Item added successfully!", MessageType.CORRECT_PROCESSING);
            acquisizioneOggettiDPView.getMessageBarTextPaneRef().setDocument(document);
                        
            clearAcquisizioneOggettoFields();
            acquisizioneOggettiDPView.getAcquisizioneOggettoCodeTextRef().requestFocus();            
            
            logger.info("XteTestToolAcquisizioneOggettiDPController addAcquisizioneOggettoItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController addAcquisizioneOggettoItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController addAcquisizioneOggettoItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Data Posta controller addAcquisizioneOggettoItem failed", ex);
        }
    }
    
    private void removeAcquisizioneOggettoItem()
    {
        logger.info("XteTestToolAcquisizioneOggettiDPController start removeAcquisizioneOggettoItem");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            if(currentSelectedRow!=-1)
            {                
                XteAcquisizioneOggettiTableModel model = ((XteAcquisizioneOggettiTableModel)acquisizioneOggettiDPView.getAcquisizioneOggettoTableRef().getModel());
                model.removeRow(currentSelectedRow);
                logger.debug("XteTestToolAcquisizioneOggettiDPController removeAcquisizioneOggettoItem: " + ((XteAcquisizioneOggettiTableModel)acquisizioneOggettiDPView.getAcquisizioneOggettoTableRef().getModel()).getMap().size());
            }                                                                    

            logger.info("XteTestToolAcquisizioneOggettiDPController removeAcquisizioneOggettoItem executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController removeAcquisizioneOggettoItem failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController removeAcquisizioneOggettoItem failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Data Posta controller removeAcquisizioneOggettoItem failed", ex);
        }
    }

	private Style getGeneratedXmlStyle(StyleContext styleContext) {
        Style style = styleContext.addStyle("XteGeneratedXmlStyle", null);
        style.addAttribute(StyleConstants.FontFamily, "tahoma");
        style.addAttribute(StyleConstants.Foreground, new Color(0,0,0));
        style.addAttribute(StyleConstants.FontSize, new Integer(12));
        style.addAttribute(StyleConstants.Bold, true);
        style.addAttribute(StyleConstants.Italic, false);
        style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        return style;
    }

    private void generaXmlData()
    {
        logger.info("XteTestToolAcquisizioneOggettiDPController start generaXmlData");

        XteTestToolMessageBar messageBar = new XteTestToolMessageBar();
        ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
        
        ArrayList<Component> enableComponent = new ArrayList<Component>();
        ArrayList<Component> disableComponent = new ArrayList<Component>();

        Marshaller marshaller;
        try {
            marshaller = context.createMarshaller();
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneOggettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneOggettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneOggettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Posta Registrata controller generaXmlData failed", ex);
        }
        try {
            marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
        } catch (PropertyException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneOggettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneOggettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneOggettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            marshaller.marshal(acquisizioneOggettiTraccia, out);
        } catch (JAXBException ex) {
            logger.error(null, ex);
            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneOggettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneOggettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneOggettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Mazzetti Posta Registrata controller generaXmlData failed", ex);
        }
        
            
        try
        {            
            clearMessageBar();
            XteTestToolPostalContext postalContext = new XteTestToolPostalContext();
            StyledDocument document = postalContext.putContentOnGeneratedXml(out.toString());
            acquisizioneOggettiDPView.getXmlDataTextAreaRef().setDocument(document);

            logger.info("XteTestToolAcquisizioneOggettiDPController generaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController generaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController generaXmlData failed", ex);

            StyledDocument messageBarDocument = messageBar.putContentOnMessageBar(ex.toString(), MessageType.FATAL);
            acquisizioneOggettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
            disableComponent.add(acquisizioneOggettiDPView.getImpostaDatiWSButtonRef());
            disableComponent.add(acquisizioneOggettiDPView.getSalvaXmlDataButtonRef());
            componentStatusManager.disableComponent(disableComponent);

            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Data Posta controller generaXmlData failed", ex);
        }
        
        StyledDocument messageBarDocument = messageBar.putContentOnMessageBar("OK", MessageType.CORRECT_PROCESSING);
        acquisizioneOggettiDPView.getMessageBarTextPaneRef().setDocument(messageBarDocument);
        enableComponent.add(acquisizioneOggettiDPView.getImpostaDatiWSButtonRef());
        enableComponent.add(acquisizioneOggettiDPView.getSalvaXmlDataButtonRef());
        
        componentStatusManager.enableComponent(enableComponent);
    }
    
    private void impostaDatiWebService()
    {
        try {
            logger.info("XteTestToolAcquisizioneOggettiDPController start impostaDatiWebService");
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();
            
            PanelLoader panelLoader = PanelLoader.getInstance();
            JPanel webserviceview = new XteTestToolWebServiceDPView(acquisizioneOggettiDPView.getParentFrame());
            XteTestToolWebServiceDPController controller = new XteTestToolWebServiceDPController(webserviceview);
            XteRequestDataModel dataModel = new XteRequestDataModel();
            DataPosta dataPostaConfig = XteTestToolConfiguration.getInstance().getDataPostaConfig();
            AO config = dataPostaConfig.getAO();
            dataModel.setServiceName(config.getService());
            dataModel.setChannel(config.getChannel());
            dataModel.setEvent(config.getEventNameTag());
            dataModel.setVersion(String.format(Locale.US, "%.1f", config.getVersion()));
            dataModel.setXmlData(acquisizioneOggettiDPView.getXmlDataTextAreaRef().getText());
            controller.setDataModel(dataModel);
            panelLoader.changePanel(acquisizioneOggettiDPView.getAcquisizioneOggettiPanelRef().getParent(), acquisizioneOggettiDPView.getAcquisizioneOggettiPanelRef(), webserviceview, new Rectangle(0,70,1000,690));

            logger.info("XteTestToolAcquisizioneOggettiDPController impostaDatiWebService executed successfully");
        } catch (BaseException baseEx) {
            logger.error("XteTestToolAcquisizioneOggettiDPController impostaDatiWebService Malformed URL error", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController impostaDatiWebService failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Data Posta controller impostaDatiWebService failed", ex);
        }
    }
    
    class XmlFileFilter extends FileFilter {

        @Override
        public boolean accept(File pathname) {
            return pathname.isDirectory() ||
                    pathname.getName().toLowerCase().endsWith(".xml");
        }

        @Override
        public String getDescription() {
            return "xml files (.xml)";
        }
    }

    private void salvaXmlData()
    {
        logger.info("XteTestToolAcquisizioneOggettiDPController start salvaXmlData");
        try
        {            
            ViewComponentStatusManager componentStatusManager = ViewComponentStatusManager.getInstance();
            clearMessageBar();

            salvaXmlFileChooser.setCurrentDirectory(new File("."));
            salvaXmlFileChooser.setSize(new Dimension(350, 200));
            salvaXmlFileChooser.setFileFilter(new XteTestToolAcquisizioneOggettiDPController.XmlFileFilter());
            salvaXmlFileChooser.showOpenDialog(acquisizioneOggettiDPView.getParentFrame());

            logger.info("XteTestToolAcquisizioneOggettiDPController salvaXmlData executed successfully");
        } catch (BaseException baseEx)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController salvaXmlData failed", baseEx);
            throw baseEx;
        }
        catch (Exception ex)
        {
            logger.error("XteTestToolAcquisizioneOggettiDPController salvaXmlData failed", ex);
            throw new XteTestToolControllerException(Group.GENERIC, Severity.FATAL, "Acquisizione Oggetti Data Posta controller salvaXmlData failed", ex);
        }
    }
}
