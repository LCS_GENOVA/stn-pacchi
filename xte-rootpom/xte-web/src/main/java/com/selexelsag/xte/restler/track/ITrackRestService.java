package com.selexelsag.xte.restler.track;

import javax.ws.rs.core.Response;

import com.selexelsag.xte.restler.IBaseRestService;


/**
 * Servizio REST per tracciatura oggetti postali
 * @author Frosi
 *
 */
public interface ITrackRestService extends IBaseRestService {
	
	/**
	 * in base al contesto della request, si crea un oggetto TrackRequest
	 * e si invoca il flusso relativo.
	 * @param request
	 * @return
	 */
	public Response executeFlow(StringBuilder service, StringBuilder channel, StringBuilder sourceOfficeId, StringBuilder destinationOfficeId, StringBuilder objectId, StringBuilder eventName, StringBuilder version,  StringBuilder dataTrack, StringBuilder requestId);
	
	public static final String TRACK_CONTEXT = "/track";
	public static final String EXECFLOW_SERVICE_NAME = "/execFlow";

}
