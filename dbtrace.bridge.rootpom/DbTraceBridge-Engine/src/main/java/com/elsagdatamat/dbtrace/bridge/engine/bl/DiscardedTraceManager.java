package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.elsagdatamat.dbtrace.bridge.engine.entities.DiscardedTrace;

public class DiscardedTraceManager extends BaseManager<DiscardedTrace, Long> implements IDiscardedTraceManager {

	@Override
	public List find(String process, Date from, Date to, String reason, Boolean single) {
		String query = "from DiscardedTrace dt";
		String conditions = "";
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (process != null)
			conditions = addCondition(DiscardedTrace.DISCARDING_PROCESS + "='" + process + "'", conditions);
		if (from != null)
			conditions = addCondition(DiscardedTrace.INSERT_DATE + ">to_date('" + s.format(from) + "','YYYY-MM-dd HH24:MI:SS')", conditions);
		if (to != null)
			conditions = addCondition(DiscardedTrace.INSERT_DATE + "<to_date('" + s.format(to) + "','YYYY-MM-dd HH24:MI:SS')", conditions);
		if (reason != null)
			conditions = addCondition("dbms_lob.compare(" + DiscardedTrace.DISCARDING_REASON + ", to_clob('" + reason + "')) !=0",
					conditions);
		if (!conditions.equals(""))
			query += " where " + conditions;
		if (single)
			return dao.executeHqlQueryPaging(query, 1, 1);

		return dao.executeHqlQuery(query);
	}

	private static String addCondition(String newCondition, String conditions) {
		if (!conditions.equals(""))
			conditions += " and ";
		conditions += newCondition;
		return conditions;
	}

	@Override
	public void saveDiscardedTraces(String discardingProcess, String traceBody, Exception marshallException) {
		try {
			DiscardedTrace dt = new DiscardedTrace();
			dt.setDiscardingProcess(discardingProcess);

			if (marshallException != null) {
				OutputStream out = new ByteArrayOutputStream();
				PrintStream s = new PrintStream(out);
				marshallException.printStackTrace(s);
				dt.setDiscardingReason(out.toString());
			} else {
				dt.setDiscardingReason("OK");
			}

			dt.setInsertDate(new Date());
			dt.setTraceBody(traceBody);
			dao.insert(dt);

		} catch (Exception e) {
			log.error("error in saveDiscardedTraces(): " + e.getMessage());
		}
	}
	
	@Override
	public long saveDiscardedTraces(String discardingProcess, String traceBody, Exception marshallException, boolean returnId) {
		try {
			DiscardedTrace dt = new DiscardedTrace();
			dt.setDiscardingProcess(discardingProcess);
			if (marshallException != null) {
				OutputStream out = new ByteArrayOutputStream();
				PrintStream s = new PrintStream(out);
				marshallException.printStackTrace(s);
				dt.setDiscardingReason(out.toString());
			} else {
				dt.setDiscardingReason("OK");
			}

			dt.setInsertDate(new Date());
			dt.setTraceBody(traceBody);
			dao.insert(dt);
			return dt.getId();

		} catch (Exception e) {
			log.error("error in saveDiscardedTraces(): " + e.getMessage());
		}
		
		return -1;
	}
	
}
