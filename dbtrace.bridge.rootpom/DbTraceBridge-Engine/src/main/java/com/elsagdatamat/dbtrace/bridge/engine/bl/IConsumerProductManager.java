package com.elsagdatamat.dbtrace.bridge.engine.bl;

import java.util.List;

import com.elsagdatamat.dbtrace.bridge.engine.entities.ConsumerProduct;
import com.elsagdatamat.dbtrace.bridge.engine.entities.ConsumerProductPK;

public interface IConsumerProductManager extends IManager<ConsumerProduct, ConsumerProductPK> {

	List<ConsumerProduct> listInvolvedProducts(String service);

}
