package com.elsagdatamat.dbtrace.bridge.tocdg;

import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;

import com.elsagdatamat.dbtrace.bridge.engine.bl.IBLOfficeMapper;
import com.elsagdatamat.dbtrace.bridge.engine.entities.OfficeMapper;
import com.elsagdatamat.dbtrace.bridge.engine.entities.OfficeMapperPK;
import com.selexelsag.tracktrace.trackdbws.xml.Trace;
import com.selexelsag.tracktrace.trackdbws.xml.TraceDetail;




public class CdGConversionUtility {
	protected final static Log log = LogFactory.getLog(CdGConversionUtility.class);

	static List<CdGMessage> convertToCdGM1(List<Trace> traceList,
			IBLOfficeMapper blOfficeMapper)
//			,
//			SDAOfficesMapper officesMapper)
	{
		Map<String, CdGM1> mMap = new HashMap<String, CdGM1>();
		CdGM1 msg = null;
		for (Trace t : traceList)
		{
			msg = mMap.get(t.getIdChannel());
			if (msg == null)
			{
				CdGM1 m1 = new CdGM1();
				m1.header = new CdGHeader();
				m1.header.swrel = "1.0";
				//m1.header.ofcid = officesMapper.getUfficiSDA().get(t.getIdChannel());
				//if (m1.header.ofcid == null) m1.header.ofcid="00000";
				OfficeMapperPK key = new OfficeMapperPK();
				key.setFamily("SDA");
				key.setFrazionarioEsterno(t.getIdChannel());
				OfficeMapper mapper = blOfficeMapper.findByID(key);
				if (mapper != null) m1.header.ofcid = mapper.getFrazionarioInterno();
				else m1.header.ofcid="00000";
				
				m1.m1dList = new ArrayList<CdGM1D>();
				msg = m1;
				mMap.put(t.getIdChannel(), msg);
			}
			Map<String,String> list2Map = new HashMap<String, String>();
			if ((t.getTraceDetailsList() != null) &&
					(t.getTraceDetailsList().getTraceDetails() != null))
			{
				for(TraceDetail td: t.getTraceDetailsList().getTraceDetails())
				{
					list2Map.put(td.getParamClass(), td.getParamValue());
				}
			}			
			CdGM1D d = new CdGM1D();
			d.cdgobj = new CdGOBJ();
			d.cdgobj.objid = t.getIdTracedEntity();
			d.cdgobj.ph = "W";
			d.cdgobj.pstf = "0";
			d.cdgobj.op = "0";
			//TODO: 
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			d.cdgobj.tdt = sdf.format(t.getWhenHappened().toGregorianCalendar().getTime());
			//d.cdgobj.tdt = t.getWhenHappened().toGregorianCalendar().getTime().toString();
			d.cdgobj.subc = t.getLabelTracedEntity();
			//eliminato a seguito della revisione 1.2
			//d.cdgobj.testf ="N";
			
			d.cdgacc = new CdGACC();
			//eliminato a seguito della revisione 1.2
			//d.cdgacc.perc="0";
			//eliminato a seguito della revisione 1.2
			//d.cdgacc.acurr = "E";
			d.cdgacc.regt="S";
			
			msg.m1dList.add(d);
		}
		List<CdGMessage> mList = new ArrayList<CdGMessage>();
		for(CdGM1 m1 : mMap.values())
		{
			mList.add(m1);
		}
		return mList;
	}
	
	static List<CdGMessage> convertToCdGM2(List<Trace> traceList,
			IBLOfficeMapper blOfficeMapper)
//			, 
//			SDACodesMapper sdaCodesMapper, SDAOfficesMapper officesMapper)
	{
		Map<String, CdGM2> mMap = new HashMap<String, CdGM2>();
		CdGM2 msg = null;
		for (Trace t : traceList)
		{
			msg = mMap.get(t.getIdChannel());
			if (msg == null)
			{
				CdGM2 m2 = new CdGM2();
				m2.header = new CdGHeader();
				m2.header.swrel = "1.0";
//				m2.header.ofcid = officesMapper.getUfficiSDA().get(t.getIdChannel());
//				if (m2.header.ofcid == null) m2.header.ofcid="00000";
				OfficeMapperPK key = new OfficeMapperPK();
				key.setFamily("SDA");
				key.setFrazionarioEsterno(t.getIdChannel());
				OfficeMapper mapper = blOfficeMapper.findByID(key);
				if (mapper != null) m2.header.ofcid = mapper.getFrazionarioInterno();
				else m2.header.ofcid="00000";

				m2.objList = new ArrayList<CdGOBJ>();
				msg = m2;
				mMap.put(t.getIdChannel(), msg);
			}
			Map<String,String> list2Map = new HashMap<String, String>();
			if ((t.getTraceDetailsList() != null) &&
					(t.getTraceDetailsList().getTraceDetails() != null))
			{
				for(TraceDetail td: t.getTraceDetailsList().getTraceDetails())
				{
					list2Map.put(td.getParamClass(), td.getParamValue());
				}
			}
			
			CdGOBJ cdgobj = new CdGOBJ();
			cdgobj = new CdGOBJ();
			cdgobj.objid = t.getIdTracedEntity();
			cdgobj.ph = "D";
			cdgobj.op = "0";
			//TODO: 
			//cdgobj.tdt = t.getWhenHappened().toGregorianCalendar().getTime().toString();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			cdgobj.tdt = sdf.format(t.getWhenHappened().toGregorianCalendar().getTime());
			cdgobj.subc = t.getLabelTracedEntity();
			//eliminato a seguito della revisione 1.2
			//cdgobj.testf ="N";

			if ((list2Map.get("BTSF") == null) || (list2Map.get("BTSF").equals("F")))
				cdgobj.btsf = null;
			else cdgobj.btsf = "B";
			
			if ((list2Map.get("DF") != null) && (list2Map.get("DF").equals("T")))
				cdgobj.df = "D";
			else cdgobj.df = null;

			cdgobj.addr = list2Map.get("ADDR");
			cdgobj.daddr = list2Map.get("DADDR");
			cdgobj.zip = list2Map.get("ZIP");
			cdgobj.dest = list2Map.get("DEST");
			
			//eliminato a seguito della revisione 1.2
//			String delReason = list2Map.get("DELREASON");
//			SDACodes sdaCode = sdaCodesMapper.getProdottiPostali().get(delReason);
//			if (sdaCode == null)
//			{
//				sdaCode = sdaCodesMapper.getProdottiSDA().get(delReason);
//			}
//			if (sdaCode == null)
//			{
//				cdgobj.delreason = "INS";
//			}
//			else
//			{
//				cdgobj.delreason = sdaCode.getCdgCode();
//			}
			
			if ((cdgobj.df != null) && (cdgobj.df.equals("D")))
			{
				cdgobj.pstf = "0";
			}
			else if ((cdgobj.btsf != null) && (cdgobj.btsf.equals("B")))
			{
				cdgobj.pstf = "4";
			}
//			else if (cdgobj.delreason.equals("INS"))
//			{
//				cdgobj.pstf = "8";
//			}
//			else if (cdgobj.delreason.equals("RSV"))
//			{
//				cdgobj.pstf = "4";
//			}
//			else if (cdgobj.delreason.equals("RNT"))
//			{
//				cdgobj.pstf = "10";
//			}
			else
			{
				cdgobj.pstf = "8";
			}

			msg.objList.add(cdgobj);
		}
		List<CdGMessage> mList = new ArrayList<CdGMessage>();
		for(CdGM2 m2 : mMap.values())
		{
			mList.add(m2);
		}
		return mList;
	}

	public static String marshal(CdGMessage o, Schema schema) throws JAXBException
	{
		JAXBContext context = JAXBContext.newInstance(o.getClass());
		Marshaller marshaller = context.createMarshaller();
		StringWriter sw = new StringWriter();
		if (schema != null) marshaller.setSchema(schema);
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(o, sw);
		return sw.toString();
	}
	
	public static Schema getSchemaFromXSD(String xsdPath) throws SAXException
	{
		log.debug("XSD: "+xsdPath);
		URL url = null;
		try {
			url = new URL(xsdPath);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SchemaFactory sf = SchemaFactory.newInstance( 
		        javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI); 
		//Schema schema = sf.newSchema(new File(xsdPath));
		Schema schema = sf.newSchema(url);
		return schema;
	}
}
