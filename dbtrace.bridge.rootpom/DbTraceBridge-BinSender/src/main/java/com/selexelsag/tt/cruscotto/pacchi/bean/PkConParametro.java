package com.selexelsag.tt.cruscotto.pacchi.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PK_CON_PARAMETRI")
public class PkConParametro implements Serializable {

	private static final long serialVersionUID = 6234831928024454303L;

	@Id
	@Column(name = "NOME", nullable = false)
	private String nome;

	@Column(name = "VALORE", nullable = false)
	private String valore;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getValore() {
		return valore;
	}

	public void setValore(String valore) {
		this.valore = valore;
	}

}
