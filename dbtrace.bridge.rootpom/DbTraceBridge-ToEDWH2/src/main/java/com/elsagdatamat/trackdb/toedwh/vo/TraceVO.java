package com.elsagdatamat.trackdb.toedwh.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.elsagdatamat.trackdb.toedwh.entities.Trace;

public class TraceVO implements Serializable, Comparable<TraceVO> {

	private static final long serialVersionUID = 1L;
	private Long traceId;
	private String labelTracedEntity;
	private String tracedEntity;
	private String idTracedEntity;
	private Date whenHappened;
	private Date whenRegistered;
	private String whereHappened;
	private String channel;
	private String idChannel;
	private String whatHappened;
	private String serviceName;
	private List<TraceDetailVO> detailsVO;
	private String idStatus;
	private String idTracedExternal;
	private String idForwardTo;
	private String sysForwardTo;

	public TraceVO() {

	}

	public TraceVO(Trace trace) {
		super();
		BeanUtils.copyProperties(trace, this);
		setDetailsVO(new LinkedList<TraceDetailVO>());
	}

	public long getIdTrace() {
		return traceId;
	}

	public void setTracedEntity(String tracedEntity) {
		this.tracedEntity = tracedEntity;
	}

	public String getTracedEntity() {
		return tracedEntity;
	}

	public void setIdTracedEntity(String idTracedEntity) {
		this.idTracedEntity = idTracedEntity;
	}

	public String getIdTracedEntity() {
		return idTracedEntity;
	}

	public void setWhenHappened(Date whenHappened) {
		this.whenHappened = whenHappened;
	}

	public Date getWhenHappened() {
		return whenHappened;
	}

	public void setWhenRegistered(Date whenRegistered) {
		this.whenRegistered = whenRegistered;
	}

	public Date getWhenRegistered() {
		return whenRegistered;
	}

	public void setWhereHappened(String whereHappened) {
		this.whereHappened = whereHappened;
	}

	public String getWhereHappened() {
		return whereHappened;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getChannel() {
		return channel;
	}

	public void setIdChannel(String idChannel) {
		this.idChannel = idChannel;
	}

	public String getIdChannel() {
		return idChannel;
	}

	public void setWhatHappened(String whatHappened) {
		this.whatHappened = whatHappened;
	}

	public String getWhatHappened() {
		return whatHappened;
	}

	public List<TraceDetailVO> getDetailsVO() {
		return detailsVO;
	}

	public void setDetailsVO(List<TraceDetailVO> details) {
		detailsVO = details;
	}

	public Long getTraceId() {
		return traceId;
	}

	public void setTraceId(Long traceId) {
		this.traceId = traceId;
	}

	public String getLabelTracedEntity() {
		return labelTracedEntity;
	}

	public void setLabelTracedEntity(String labelTracedEntity) {
		this.labelTracedEntity = labelTracedEntity;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	// Aggiunto R.L.- 02-05-2012
	public String getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}

	public String getIdTracedExternal() {
		return idTracedExternal;
	}

	public void setIdTracedExternal(String idTracedExternal) {
		this.idTracedExternal = idTracedExternal;
	}

	// Aggiunto R.L.- 02-05-2012
	public String getIdForwardTo() {
		return idForwardTo;
	}

	public void setIdForwardTo(String idForwardTo) {
		this.idForwardTo = idForwardTo;
	}

	public String getSysForwardTo() {
		return sysForwardTo;
	}

	public void setSysForwardTo(String sysForwardTo) {
		this.sysForwardTo = sysForwardTo;
	}

	@Override
	public int compareTo(TraceVO o) {
		if (traceId > o.traceId)
			return 1;
		if (traceId < o.traceId)
			return -1;
		return 0;
	}
}
