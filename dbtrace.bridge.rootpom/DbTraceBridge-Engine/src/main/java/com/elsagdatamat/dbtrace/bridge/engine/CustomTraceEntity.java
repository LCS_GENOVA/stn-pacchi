package com.elsagdatamat.dbtrace.bridge.engine;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import com.elsagdatamat.framework.entities.Entity;
import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.elsagdatamat.tt.trackdbws.beans.TraceDetail;

public class CustomTraceEntity extends Trace implements Entity {

	private static final long serialVersionUID = 6276793972207350598L;

	private String strUuid;

	public CustomTraceEntity() {
		setInternalDetailList(new HashSet<TraceDetail>(0));
		String tmpUuid = UUID.randomUUID().toString();
		tmpUuid = tmpUuid.replace("-", "");
		this.strUuid = tmpUuid.substring(0, 30);
	}

	public void addDetails(String tag, String value) {
		TraceDetail td = new TraceDetail();
		td.setParamClass(tag);
		td.setParamValue(value);
		td.setTrace(this);
		getInternalDetailList().add(td);
	}

	public void addAndReplaceDetails(String tagToReplace, String value) {

		Set<TraceDetail> traceDetails = getInternalDetailList();
		for (TraceDetail traceDetail : traceDetails) {
			if (traceDetail.getParamClass().equals(tagToReplace)) {
				traceDetails.remove(traceDetail);
				addDetails(tagToReplace, value);
				return;
			}
		}
		addDetails(tagToReplace, value);
	}

	public void removeDetails(String tagToRemove) {

		Set<TraceDetail> traceDetails = getInternalDetailList();
		for (TraceDetail traceDetail : traceDetails) {
			if (traceDetail.getParamClass().equals(tagToRemove)) {
				traceDetails.remove(traceDetail);
				return;
			}
		}
	}

	public void generateUUID(String tag, String methodToInvoke) {
		Method[] methods = this.getClass().getMethods();
		for (Method method : methods) {
			if (method.getName().contentEquals(methodToInvoke)) {
				try {
					method.invoke(this, strUuid);
					return;
				} catch (Exception e) {
					throw new RuntimeException("Invalid method to invoke: " + methodToInvoke);
				}
			}
		}

		TraceDetail td = new TraceDetail();
		td.setParamClass(tag);
		td.setParamValue(strUuid);
		getInternalDetailList().add(td);
		td.setTrace(this);
	}

	@Override
	public CustomTraceEntity clone() {
		CustomTraceEntity trace = new CustomTraceEntity();

		trace.setChannel(getChannel());
		trace.setIdChannel(getIdChannel());
		trace.setIdTracedEntity(getIdTracedEntity());
		trace.setTracedEntity(getTracedEntity());
		trace.setWhatHappened(getWhatHappened());
		trace.setWhenHappened(getWhenHappened());
		trace.setWhereHappened(getWhereHappened());
		trace.setLabelTracedEntity(getLabelTracedEntity());
		trace.setServiceName(getServiceName());
		trace.setStrUuid(getStrUuid());
		// Aggiunti R.L. - 04.05.2012
		trace.setIdCorrelazione(getIdCorrelazione());
		trace.setIdForwardTo(getIdForwardTo());
		trace.setSysForwardTo(getSysForwardTo());
		trace.setIdStatus(getIdStatus());
		trace.setIdTracedExternal(getIdTracedExternal());

		trace.getInternalDetailList().addAll(getInternalDetailList());
		return trace;
	}

	public void setStrUuid(String strUuid) {
		this.strUuid = strUuid;
	}

	public String getStrUuid() {
		return strUuid;
	}

}
