package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="DISCARDED_TRACE")
public class DiscardedTrace implements Serializable {

	private static final long serialVersionUID = 8909281602496909398L;

	public static final String ID = "id";
	public static final String TRACE_BODY = "traceBody";
	public static final String DISCARDING_PROCESS = "discardingProcess";
	public static final String DISCARDING_REASON = "discardingReason";
	public static final String INSERT_DATE = "insertDate";

	private long id;
	private String traceBody; 
	private String discardingProcess;
	private String discardingReason;
	private Date insertDate;
	
	@Id
	@SequenceGenerator(name="DiscardedTraceSeq",sequenceName="DISCARDED_TRACE_SEQ", allocationSize=1, initialValue=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="DiscardedTraceSeq")
    @Column(name = "ID", nullable = false)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Lob
	@Column(name="TRACE_BODY")
	public String getTraceBody() {
		return traceBody;
	}
	public void setTraceBody(String traceBody) {
		this.traceBody = traceBody;
	}
	
	@Column(name="DISCARDING_PROCESS", length=50)
	public String getDiscardingProcess() {
		return discardingProcess;
	}
	public void setDiscardingProcess(String discardingProcess) {
		this.discardingProcess = discardingProcess;
	}

	@Lob
	@Column(name="DISCARDING_REASON")
	public String getDiscardingReason() {
		return discardingReason;
	}
	public void setDiscardingReason(String discardingReason) {
		this.discardingReason = discardingReason;
	}

	@Column(name="INSERT_DATE")
	public Date getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	} 
}
