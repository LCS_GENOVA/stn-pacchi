package com.elsagdatamat.trackdb.toedwh.dao;

import java.util.List;

import com.elsagdatamat.trackdb.toedwh.entities.TraceDetail;


public interface ITraceDetailDAO {

	List<TraceDetail> executeQuery(String hqlQuery);

	

}
