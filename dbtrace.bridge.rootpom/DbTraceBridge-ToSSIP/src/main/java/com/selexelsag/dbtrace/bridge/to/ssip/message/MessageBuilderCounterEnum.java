/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.ssip.message;

import java.io.Serializable;

/**
 * @author arodriguez
 *
 */
public enum MessageBuilderCounterEnum implements Serializable {

	TotalRuns(0),
	SuccessRuns(1),
	FailedRuns(2),
	TimeoutRuns(3);
	
	private int index;

	/**
	 * @param index
	 */
	private MessageBuilderCounterEnum(int index) {
		this.index = index;
	}
	
	public final int index() {
		return index;
	}
}
