/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.datamodel;

import com.selexelsag.xte.testtool.view.XteTestToolPanelView;
import com.selexelsag.xte.testtool.xml.Function;
import java.util.EnumMap;
import java.util.List;

/**
 *
 * @author Da Procida
 */
public class XteTestToolDataModel {
    
    private String applicationID = null;
    private XteTestToolPanelView currentXteTestToolPanelView = null;
    private List<XteServer> xteServerList = null;
    private XteServer currentXteServer = null;
    private List<Function> postalFunctionsList = null;
    private PostalContext currentPostalContext = null;
    private Function currentPostalFunction = null;

    private static final EnumMap<PostalContext, String> PACKAGE_POSTAL_CONTEXT_MAP = new EnumMap<PostalContext, String>(PostalContext.class);
    static {
        PACKAGE_POSTAL_CONTEXT_MAP.put(PostalContext.POSTA_REGISTRATA, "pr");
        PACKAGE_POSTAL_CONTEXT_MAP.put(PostalContext.DATA_POSTA, "dp");
        PACKAGE_POSTAL_CONTEXT_MAP.put(PostalContext.ACQUISIZIONE_MAZZETTI, "am");
        PACKAGE_POSTAL_CONTEXT_MAP.put(PostalContext.ACCETTAZIONE_ONLINE, "ao");
    }
    
    public XteTestToolDataModel() {
    }

    public void reset() {

        xteServerList = null;
        currentXteServer = null;
        currentXteTestToolPanelView = null;
        postalFunctionsList = null;
    }

    public String getApplicationID() {
        return applicationID;
    }

    public void setApplicationID(String applicationID) {
        this.applicationID = applicationID;
    }

    public List<XteServer> getXteServerList() {
        return xteServerList;
    }

    public void setXteServerList(List<XteServer> xteServerList) {
        this.xteServerList = xteServerList;
    }

    public XteServer getCurrentXteServer() {
        return currentXteServer;
    }

    public void setCurrentXteServer(XteServer currentXteServer) {
        this.currentXteServer = currentXteServer;
    }

    public XteTestToolPanelView getCurrentXteTestToolPanelView() {
        return currentXteTestToolPanelView;
    }

    public void setCurrentXteTestToolPanelView(XteTestToolPanelView currentXteTestToolPanelView) {
        this.currentXteTestToolPanelView = currentXteTestToolPanelView;
    }

    public List<Function> getPostalFunctionsList() {
        return postalFunctionsList;
    }

    public void setPostalFunctionsList(List<Function> postalFunctionsList) {
        this.postalFunctionsList = postalFunctionsList;
    }

    public PostalContext getCurrentPostalContext() {
        return currentPostalContext;
    }

    public void setCurrentPostalContext(PostalContext currentPostalContext) {
        this.currentPostalContext = currentPostalContext;
    }

    public Function getCurrentPostalFunction() {
        return currentPostalFunction;
    }

    public void setCurrentPostalFunction(Function currentPostalFunction) {
        this.currentPostalFunction = currentPostalFunction;
    }

    public static String getPackageExtByPostaContext(PostalContext postalContext)
    {
        try{
            String packageExtension = PACKAGE_POSTAL_CONTEXT_MAP.get(postalContext);
            return packageExtension;
        }
        catch(Exception ex)
        {
             return null;
        }
    }

}
