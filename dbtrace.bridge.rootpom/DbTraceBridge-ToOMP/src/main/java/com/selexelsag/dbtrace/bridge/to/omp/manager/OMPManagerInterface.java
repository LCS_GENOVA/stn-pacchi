package com.selexelsag.dbtrace.bridge.to.omp.manager;

import java.util.Map;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.dbtrace.bridge.engine.entities.OfficeMapper;

/**
 * 
 * @author arodriguez
 * 
 */

@Transactional(value = "dbTraceBridge-transactionManager", propagation = Propagation.REQUIRED)
public interface OMPManagerInterface {

	public static final String DEFAULT_NAME = "ompTokenManager";
	
	public void setMinutesToRecoveryMinValue(Integer minutesToRecoveryMinValue);
		
	public void setMinutesToRestartMinValue(Integer minutesToRestartMinValue);

	public Integer getMinutesToRecoveryMinValue();
	
	public Integer getMinutesToRestartMinValue();

	public Long getNextSequenceCode();

	public Map<String, String> loadAllPostOfficeCodes();
	
	public String getBillModeCode(String keyCode);
	
	public String getIdStatusTranscoded(String keyCode, String keyChannel);
	
	public OfficeMapper findOfficeMapperByCode(String code);
}
