package com.elsagdatamat.dbtrace.bridge.engine.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name="PARAMETER")
public class Parameter implements Serializable {

    	/**
     * 
     */
    private static final long serialVersionUID = 3694345913082322143L;
	@Id
	private String name;
	
	@Column
	private String value;

	public String getName() {
	    return name;
	}

	public void setName(String name) {
	    this.name = name;
	}

	public String getValue() {
	    return value;
	}

	public void setValue(String value) {
	    this.value = value;
	}
	
	
	
    	
    	
}
