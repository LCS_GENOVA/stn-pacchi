package com.selexelsag.xte.model.flows;

import java.io.Serializable;

public class FlowResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4597753066270110944L;

	private String flowResult;
	
	private String flowDescResult;
	
	private String flowCodeResult;
	
	private String flowReiteration;
	
	private Throwable error;

	public String getFlowResult() {
		return flowResult;
	}

	public void setFlowResult(String flowResult) {
		this.flowResult = flowResult;
	}

	public String getFlowDescResult() {
		return flowDescResult;
	}

	public void setFlowDescResult(String flowDescResult) {
		this.flowDescResult = flowDescResult;
	}

	public String getFlowCodeResult() {
		return flowCodeResult;
	}

	public void setFlowCodeResult(String flowCodeResult) {
		this.flowCodeResult = flowCodeResult;
	}

	public String getFlowReiteration() {
		return flowReiteration;
	}

	public void setFlowReiteration(String flowReiteration) {
		this.flowReiteration = flowReiteration;
	}

	public Throwable getError() {
		return error;
	}

	public void setError(Throwable error) {
		this.error = error;
	}
	
	@Override
	public String toString() {
		return String
				.format("FlowResponse [flowResult=%s, flowDescResult=%s, flowCodeResult=%s, flowReiteration=%s]",
						flowResult, flowDescResult, flowCodeResult,
						flowReiteration);
	}
	
	
}
