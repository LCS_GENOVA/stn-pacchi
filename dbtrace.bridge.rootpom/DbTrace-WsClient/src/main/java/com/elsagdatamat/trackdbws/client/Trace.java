
package com.elsagdatamat.trackdbws.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Trace complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Trace">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceName" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}ServiceName"/>
 *         &lt;element name="tracedEntity" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}TracedEntity"/>
 *         &lt;element name="idTracedEntity" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}IdTracedEntity"/>
 *         &lt;element name="labelTracedEntity" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}LabelTracedEntity"/>
 *         &lt;element name="whenHappened" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="whenRegistered" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="whereHappened" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}WhereHappened"/>
 *         &lt;element name="channel" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}Channel"/>
 *         &lt;element name="idChannel" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}IdChannel"/>
 *         &lt;element name="whatHappened" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}WhatHappened"/>
 *         &lt;element name="traceDetailsList" type="{http://tt.elsagdatamat.com/trackdbws/trackdbrwservice}TraceDetailsList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Trace", propOrder = {
    "serviceName",
    "tracedEntity",
    "idTracedEntity",
    "labelTracedEntity",
    "whenHappened",
    "whenRegistered",
    "whereHappened",
    "channel",
    "idChannel",
    "whatHappened",
    "idStatus",
    "idCorrelazione",
    "idTracedExternal",
    "idForwardTo",
    "sysForwardTo",
    "traceDetailsList"
})
public class Trace {

    @XmlElement(required = true, nillable = true)
    protected String serviceName;
    @XmlElement(required = true, nillable = true)
    protected String tracedEntity;
    @XmlElement(required = true, nillable = true)
    protected String idTracedEntity;
    @XmlElement(required = true, nillable = true)
    protected String labelTracedEntity;
    @XmlElement(required = true, nillable = true)
    protected XMLGregorianCalendar whenHappened;
    @XmlElement(required = true, nillable = true)
    protected XMLGregorianCalendar whenRegistered;
    @XmlElement(required = true, nillable = true)
    protected String whereHappened;
    @XmlElement(required = true, nillable = true)
    protected String channel;
    @XmlElement(required = true, nillable = true)
    protected String idChannel;
    @XmlElement(required = true, nillable = true)
    protected String whatHappened;
    // Aggiunto R.L. - 02.05.2012
    @XmlElement(required = true, nillable = true)
    protected String idStatus;
    @XmlElement(required = true, nillable = true)
    protected String idCorrelazione;
    @XmlElement(required = true, nillable = true)
    protected String idTracedExternal;
    // Aggiunto R.L. - 03.05.2012
    @XmlElement(required = true, nillable = true)
	private String idForwardTo;
    @XmlElement(required = true, nillable = true)
    private String sysForwardTo;
    
    
    @XmlElement(required = true, nillable = true)
    protected TraceDetailsList traceDetailsList;
    
    

    /**
     * Gets the value of the serviceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Sets the value of the serviceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceName(String value) {
        this.serviceName = value;
    }

    /**
     * Gets the value of the tracedEntity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTracedEntity() {
        return tracedEntity;
    }

    /**
     * Sets the value of the tracedEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTracedEntity(String value) {
        this.tracedEntity = value;
    }

    /**
     * Gets the value of the idTracedEntity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTracedEntity() {
        return idTracedEntity;
    }

    /**
     * Sets the value of the idTracedEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTracedEntity(String value) {
        this.idTracedEntity = value;
    }

    /**
     * Gets the value of the labelTracedEntity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabelTracedEntity() {
        return labelTracedEntity;
    }

    /**
     * Sets the value of the labelTracedEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabelTracedEntity(String value) {
        this.labelTracedEntity = value;
    }

    /**
     * Gets the value of the whenHappened property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWhenHappened() {
        return whenHappened;
    }

    /**
     * Sets the value of the whenHappened property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWhenHappened(XMLGregorianCalendar value) {
        this.whenHappened = value;
    }

    /**
     * Gets the value of the whenRegistered property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWhenRegistered() {
        return whenRegistered;
    }

    /**
     * Sets the value of the whenRegistered property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWhenRegistered(XMLGregorianCalendar value) {
        this.whenRegistered = value;
    }

    /**
     * Gets the value of the whereHappened property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhereHappened() {
        return whereHappened;
    }

    /**
     * Sets the value of the whereHappened property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhereHappened(String value) {
        this.whereHappened = value;
    }

    /**
     * Gets the value of the channel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Sets the value of the channel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannel(String value) {
        this.channel = value;
    }

    /**
     * Gets the value of the idChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdChannel() {
        return idChannel;
    }

    /**
     * Sets the value of the idChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdChannel(String value) {
        this.idChannel = value;
    }

    /**
     * Gets the value of the whatHappened property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhatHappened() {
        return whatHappened;
    }

    /**
     * Sets the value of the whatHappened property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhatHappened(String value) {
        this.whatHappened = value;
    }

    /**
     * Gets the value of the traceDetailsList property.
     * 
     * @return
     *     possible object is
     *     {@link TraceDetailsList }
     *     
     */
    public TraceDetailsList getTraceDetailsList() {
        return traceDetailsList;
    }

    /**
     * Sets the value of the traceDetailsList property.
     * 
     * @param value
     *     allowed object is
     *     {@link TraceDetailsList }
     *     
     */
    public void setTraceDetailsList(TraceDetailsList value) {
        this.traceDetailsList = value;
    }
    
    // Aggiunto R.L. - 02.05.2012
    public String getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(String idStatus) {
		this.idStatus = idStatus;
	}

	public String getIdTracedExternal() {
		return idTracedExternal;
	}

	public void setIdTracedExternal(String idTracedExternal) {
		this.idTracedExternal = idTracedExternal;
	}

	public String getIdCorrelazione() {
		return idCorrelazione;
	}

	public void setIdCorrelazione(String idCorrelazione) {
		this.idCorrelazione = idCorrelazione;
	}

	 // Aggiunto R.L. - 03.05.2012
	public String getIdForwardTo() {
		return idForwardTo;
	}

	public void setIdForwardTo(String idForwardTo) {
		this.idForwardTo = idForwardTo;
	}

	public String getSysForwardTo() {
		return sysForwardTo;
	}

	public void setSysForwardTo(String sysForwardTo) {
		this.sysForwardTo = sysForwardTo;
	}

	
    
}
