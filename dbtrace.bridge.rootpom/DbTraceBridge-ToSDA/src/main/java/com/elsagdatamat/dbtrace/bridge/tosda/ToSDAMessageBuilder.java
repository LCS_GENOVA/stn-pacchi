package com.elsagdatamat.dbtrace.bridge.tosda;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.elsagdatamat.dbtrace.bridge.engine.MessageBuilderBase;
import com.elsagdatamat.dbtrace.bridge.engine.bl.ICodiciProdottoPICodiciProdottoSDAManager;
import com.elsagdatamat.dbtrace.bridge.engine.bl.IRemovedTraceBL;
import com.elsagdatamat.dbtrace.bridge.engine.dao.IModPagContrPiSdaDAO;
import com.elsagdatamat.dbtrace.bridge.engine.dao.IOfficeMapperDAO;
import com.elsagdatamat.dbtrace.bridge.engine.dao.IStatusPiSdaDAO;
import com.elsagdatamat.dbtrace.bridge.engine.entities.RemovedTrace;
import com.elsagdatamat.dbtrace.bridge.tosda.manager.ITraceManager;
import com.elsagdatamat.dbtrace.validation.event.NonCompliantTrace;
import com.elsagdatamat.dbtrace.validation.event.TraceValidator;
import com.elsagdatamat.framework.resources.ClassPathResource;
import com.elsagdatamat.tt.trackdbws.beans.Trace;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;


public class ToSDAMessageBuilder extends MessageBuilderBase {
	protected final Log log = LogFactory.getLog(getClass());

	// Bisognerebbe farselo passare dalla classe WsToJmsBridge ma per mancanza di tempo lo mettiamo uguale a quello definito
	// nel file di contesto di spring: applicationContext.xml, apt-applicationContext.xml, rtz-applicationContext.xml
	private String taskId = "dbTracceToSDABridge";

	private IModPagContrPiSdaDAO modPagDao;
	private IStatusPiSdaDAO idStatusDao;
	private ICodiciProdottoPICodiciProdottoSDAManager subpManager;
	private IOfficeMapperDAO officeMapperDAO;
	private IRemovedTraceBL removedTraceBL;

	
	private ITraceManager traceManager;
	public void setTraceManager(ITraceManager traceManager) {
		this.traceManager = traceManager;
	}
	
	private Integer queryMinutesInterval = 60;
	public String getQueryMinutesInterval() {
		return String.valueOf(queryMinutesInterval);
	}
	public void setQueryMinutesInterval(String queryMinutesInterval) {
		if (queryMinutesInterval != null && !queryMinutesInterval.startsWith("${") && !queryMinutesInterval.trim().isEmpty()) {
			this.queryMinutesInterval = Integer.valueOf(queryMinutesInterval);
		}
		log.info(String.format("[%s] queryMinutesInterval:%d", taskId, this.queryMinutesInterval));
	}

	
	// usata per la dimensione max del file per SDA
	private Integer maxNumOfTracesToSDA = 1000;
	public String getMaxNumOfTracesToSDA() {
		return String.valueOf(maxNumOfTracesToSDA);
	}
	public void setMaxNumOfTracesToSDA(String maxNumOfTracesToSDA) {
		if (maxNumOfTracesToSDA != null && !maxNumOfTracesToSDA.startsWith("${") && !maxNumOfTracesToSDA.trim().isEmpty()) {
			this.maxNumOfTracesToSDA = Integer.valueOf(maxNumOfTracesToSDA);
		}
		log.info(String.format("[%s] maxNumOfTracesToSDA:%d", taskId, this.maxNumOfTracesToSDA));
	}

	
	// usata per il numero massimo di righe della cache
	private Integer maxNumOfTracesFromDB = 10000;
	public String getMaxNumOfTracesFromDB() {
		return String.valueOf(maxNumOfTracesFromDB);
	}
	public void setMaxNumOfTracesFromDB(String maxNumOfTracesFromDB) {
		if (maxNumOfTracesFromDB != null && !maxNumOfTracesFromDB.startsWith("${") && !maxNumOfTracesFromDB.trim().isEmpty()) {
			this.maxNumOfTracesFromDB = Integer.valueOf(maxNumOfTracesFromDB);
		}
		log.info(String.format("[%s] maxNumOfTracesFromDB:%d", taskId, this.maxNumOfTracesFromDB));
	}
	
	
	
	
	public IRemovedTraceBL getRemovedTraceBL() {
		return removedTraceBL;
	}
	public void setRemovedTraceBL(IRemovedTraceBL removedTraceBL) {
		this.removedTraceBL = removedTraceBL;
	}
	public IOfficeMapperDAO getOfficeMapperDAO() {
		return officeMapperDAO;
	}
	public void setOfficeMapperDAO(IOfficeMapperDAO officeMapperDAO) {
		this.officeMapperDAO = officeMapperDAO;
	}

	private JmsTemplate jmsTemplate;

	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	private ClassPathResource xsdPath;

	public void setXsdPath(ClassPathResource xsdPath) {
		this.xsdPath = xsdPath;
	}
	/**
	 * @return the modPagDao
	 */
	public IModPagContrPiSdaDAO getModPagDao() {
		return modPagDao;
	}

	/**
	 * @param modPagDao
	 *            the modPagDao to set
	 */
	public void setModPagDao(IModPagContrPiSdaDAO modPagDao) {
		this.modPagDao = modPagDao;
	}

	/**
	 * @return the idStatusDao
	 */
	public IStatusPiSdaDAO getIdStatusDao() {
		return idStatusDao;
	}

	/**
	 * @param idStatusDao
	 *            the idStatusDao to set
	 */
	public void setIdStatusDao(IStatusPiSdaDAO idStatusDao) {
		this.idStatusDao = idStatusDao;
	}

	/**
	 *
	 * @return subpManager
	 */
	public ICodiciProdottoPICodiciProdottoSDAManager getSubpManager() {
		return subpManager;
	}

	/**
	 *
	 * @param subpManager the subpManager to set
	 */
	public void setSubpManager(ICodiciProdottoPICodiciProdottoSDAManager subpManager) {
		this.subpManager = subpManager;
	}

	// IMessageBuilder
	@Override
	public List<String> create(TracesList input) throws Exception {
		log.debug("Entering create");

		long idMessage = -1;
		Exception exception = null;
		List<String> sList = new ArrayList<String>();
		String sdaStr = null;
		SDAMessage sdaMsg = null;
		try {
			Schema schema = null;
			if (xsdPath != null) {
				log.debug("Calling getSchema");
				schema = ConversionUtility.getInstance().getSchemaFromXSD(xsdPath.getURL());
			}
			log.debug("Calling convertToSDA");
			sdaMsg = ConversionUtility.getInstance(modPagDao, idStatusDao).convertToSDA(input.getTraces(), modPagDao,subpManager,officeMapperDAO);
			log.debug("Calling marshal");
			TraceValidator traceValidator = new TraceValidator();
			try {
				sdaStr = ConversionUtility.getInstance().marshal(sdaMsg, schema , traceValidator);
				sList.add(sdaStr);
				//sendFromCreation(sList);
				log.info(String.format("[%s] sent [%d] traces via JMS.", taskId, input.getTraces().size()));
				if (isSaveTraces()) {
					log.info(String.format("[%s] save traces into Discarded Trace Table.", taskId));
					idMessage = discardedTraceManager.saveDiscardedTraces(token.getServiceId(), sdaStr, null, true);
					insertRemovedTraces(ConversionUtility.getInstance().getNonCompliantTraces(),idMessage, token.getServiceId());
				}
			} catch(ToSDAException e) {
				String str = ConversionUtility.getInstance().marshal(sdaMsg, null);
				log.info(String.format("[%s] save traces into Discarded Trace Table.", taskId));
				idMessage = discardedTraceManager.saveDiscardedTraces(token.getServiceId(), str, exception, true);
				insertRemovedTraces(ConversionUtility.getInstance().getNonCompliantTraces(),idMessage, token.getServiceId());
				log.error(String.format("[%s] Catch Exception with message=[%s]", taskId, e.getMessage()), e);
			}

		} catch (Exception e) {
			exception = e;
			log.error(e.getMessage(),e);
			try {
				if (exception != null) {
					String str = ConversionUtility.getInstance().marshal(sdaMsg, null);
					log.info(String.format("[%s] save traces into Discarded Trace Table.", taskId));
					idMessage = discardedTraceManager.saveDiscardedTraces(token.getServiceId(), str, exception, true);
					insertRemovedTraces(ConversionUtility.getInstance().getNonCompliantTraces(),idMessage, token.getServiceId());
				}
			} catch (Exception ex) {
				log.error(String.format("[%s] Catch Exception with message=[%s]", taskId, ex.getMessage()), ex);
			}
			throw e;
		}
		log.debug("Leaving create");
		return sList;
	}


	private void insertRemovedTraces(List<NonCompliantTrace> traces, long idDiscardedTrace, String serviceId) throws JAXBException, IOException{
		JAXBContext context = JAXBContext.newInstance(SDARemovedTrace.class);
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);


		for (NonCompliantTrace nonCompliantTrace : traces) {
			RemovedTrace removedTrace = new RemovedTrace();
			removedTrace.setDiscardingProcess(serviceId);
			SDARemovedTrace trace = new SDARemovedTrace(nonCompliantTrace.getSdaTrace());
			StringWriter sw = new StringWriter();
			marshaller.marshal(trace, sw);

			removedTrace.setTraced(sw.toString());
			removedTrace.setReason(nonCompliantTrace.getReason());
			removedTrace.setIdDiscardedTrace(idDiscardedTrace);
			removedTraceBL.insert(removedTrace);
		}
	}

	private void sendFromCreation(final List<String> strList) throws IOException {
		log.debug("Entering send");
		if (jmsTemplate != null)
		for (final String str : strList) {
			jmsTemplate.send(new MessageCreator() {
				@Override
				public Message createMessage(Session session) throws JMSException {
					TextMessage tm = session.createTextMessage();
					tm.setText(str);
					return tm;
				}
			});
		}
		log.debug("Leaving send");
	}

	// IMessageSender
	@Override
	public void send(final List<String> strList) throws IOException {
		log.debug("Entering send");
		for (final String str : strList) {
			jmsTemplate.send(new MessageCreator() {
				@Override
				public Message createMessage(Session session) throws JMSException {
					TextMessage tm = session.createTextMessage();
					tm.setText(str);
					return tm;
				}
			});
		}
		log.debug("Leaving send");
	}
	
	
	@Override
	public TracesList getTraces(TrackDBRWServicePortType twsp, Date stopDate) {

		log.debug(String.format("[%s] Entering getTraces for service '%s' (wsToken='%s'): stopDate=%s;",
				taskId, token.getServiceId(), token.getWsToken(), stopDate));

//		boolean stopThread = false;
//		Thread th = new Thread(new Runnable(){
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				while(!stopThread) {
//					
//				}
//			}
//		});
//		th.start();
		List<Trace> traces = traceManager.loadNextTraces(token, stopDate, maxNumOfTracesToSDA, maxNumOfTracesFromDB, queryMinutesInterval);
		
		String nuovoToken;
		if (traces != null && traces.size() > 0) {
			
			log.debug(String.format("[%s] found '%s' traces", taskId, traces.size()));
			
			// aggiornamento del token
			com.elsagdatamat.tt.trackdbws.beans.Trace lastTrace = traces.get(traces.size() - 1);

			nuovoToken = String.valueOf(lastTrace.getWhenRegistered().getTime());
			nuovoToken += ";";
			nuovoToken += String.valueOf(lastTrace.getIdTrace());

		} else {
			
			log.debug(String.format("[%s] no traces found", taskId));
			
			Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			c.add(Calendar.MINUTE, 2*queryMinutesInterval);
			
			nuovoToken = String.valueOf(c.getTime());
			nuovoToken += ";";
			nuovoToken += token.getWsToken().split(";")[1];
		}

		token.setWsToken(nuovoToken);

		
		log.debug(String.format("[%s] updated token : '%s' ", taskId, token.getWsToken()));

						
		TracesList ret = null;
		ret = ConvertUtil.tracesToTraceList(traces);

		return ret;
	}

}
