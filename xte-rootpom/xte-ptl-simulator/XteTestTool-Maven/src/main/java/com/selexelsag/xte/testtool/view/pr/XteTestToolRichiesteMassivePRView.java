/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.selexelsag.xte.testtool.view.pr;

import com.selexelsag.xte.testtool.exception.Group;
import com.selexelsag.xte.testtool.exception.Severity;
import com.selexelsag.xte.testtool.view.XteTestToolApplicationFrameView;
import com.selexelsag.xte.testtool.view.XteTestToolPanelView;
import com.selexelsag.xte.testtool.view.common.XteTestToolScrollBarUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolTabbedUI;
import com.selexelsag.xte.testtool.view.common.XteTestToolViewException;
import com.selexelsag.xte.testtool.view.objectmodel.XteLasciaPrendiTableModel;
import com.selexelsag.xte.testtool.view.objectmodel.XteListSelectionModel;
import com.selexelsag.xte.testtool.view.objectrenderer.XteLasciaPrendiTableCellRenderer;
import com.selexelsag.xte.testtool.view.objectrenderer.XteTableHeaderRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolRichiesteMassivePRView extends XteTestToolPanelView{
    private SortedMap<Integer, Component> componentSortedMap = null;
    private ResourceBundle messages;
    private static Logger logger = Logger.getLogger(XteTestToolRichiesteMassivePRView.class);
    private XteLasciaPrendiTableModel xteLasciaPrendiPRTableModel = new XteLasciaPrendiTableModel();;
    
    public XteTestToolRichiesteMassivePRView(XteTestToolApplicationFrameView applicationFrame) {
        super(applicationFrame);
        try {
            logger.info("XteTestToolRichiesteMassivePRView start class creation");
            this.messages = ResourceBundle.getBundle("bundles/messages");
            initComponents();
            logger.info("XteTestToolRichiesteMassivePRView class created successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolRichiesteMassivePRView class creation failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Richieste Massive Posta Registrata view creation failed", ex);
        }
    }

    public JPanel getRichiesteMassivePanelRef(){ return this; }        
    public JLabel getRichiesteMassiveTitleLabelRef(){ return this.panelTitlejLabel; }
    
    public JPanel getBundleDataPaneRef(){ return this.bundleDatajPanel; }
    public JTextField getBundleIdTextRef(){ return this.bundleIdjTextField; }
    public JTextField getBundleTPIdTextRef(){ return this.bundleTPIdjTextField; }
    
    public JPanel getXmlDataPaneRef(){ return this.xmlDatajPanel; }
    public JScrollPane getXmlDataScrollPaneRef(){ return this.xmlDatajScrollPane; }
    public JTextArea getXmlDataTextAreaRef(){ return this.xmlDatajTextArea; }
    public JPanel getXmlDataCommandPaneRef(){ return this.xmlDataCommandjPanel; }    
    public JButton getGeneraXmlDataButtonRef(){ return this.generaXmlDatajButton; }
    
    public JTabbedPane getRichiesteMassiveDataTabbedPaneRef(){ return this.massiveRequestjTabbedPane; }
    public JPanel getNotificaDataPaneRef(){ return this.notificajPanel; }
    public JScrollPane getNotificaDataScrollPaneRef(){ return this.notificaDatajScrollPane; }
    public JTextArea getNotificaDataTextAreaRef(){ return this.notificaDatajTextArea; }
    public JPanel getNotificaDataCommandPaneRef(){ return this.notificaDataCommandjPanel; }    
    public JButton getNotificaGeneraXmlDataButtonRef(){ return this.notificaGeneraXmlDatajButton; }
    public JPanel getLasciaPrendiDataPaneRef(){ return this.lasciaPrendijPanel; }
        
    
    public JTable getLasciaPrendiTableRef(){ return this.lasciaPrendijTable; }    
    public JPanel getLasciaPrendiDataCommandPaneRef(){ return this.lasciaPrendiDataCommandjPanel; }    
    public JButton getStartLasciaPrendiAutoButtonRef(){ return this.startLasciaPrendiAutojButton; }
    public JButton getLasciaInviiMenoUnoButtonRef(){ return this.lasciaInviiMenoUnojButton; }
    
    public JButton getBackButtonRef(){ return this.backjButton; }
    public JButton getHomeButtonRef(){ return this.homejButton; }
    public JButton getExitButtonRef(){ return this.exitjButton; }
    public JTextPane getMessageBarTextPaneRef(){ return this.messageBarjTextPane; }

    @Override
    public void customizeView() {
        try
        {
            logger.info("XteTestToolRichiesteMassivePRView start customizeView");
            //CUSTOMIZE TITLE
            this.panelTitlejLabel .setText(messages.getString("postaregistrata.richiestemassive.view.title.label"));

            //CUSTOMIZE BUTTON
            this.generaXmlDatajButton.setText(messages.getString("postaregistrata.richiestemassive.view.generaxmldata.button"));                        
            this.notificaGeneraXmlDatajButton.setText(messages.getString("postaregistrata.richiestemassive.view.notificageneraxmldata.button"));
            this.startLasciaPrendiAutojButton.setText(messages.getString("postaregistrata.richiestemassive.view.startlasciaprendiauto.button"));
            this.lasciaInviiMenoUnojButton.setText(messages.getString("postaregistrata.richiestemassive.view.lasciainviimenouno.button"));
            this.backjButton.setText(messages.getString("view.back.button"));
            this.homejButton.setText(messages.getString("view.home.button"));
            this.exitjButton.setText(messages.getString("view.exit.button"));

            logger.info("XteTestToolRichiesteMassivePRView customizeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolRichiesteMassivePRView customizeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Richieste Massive Posta Registrata view customization failed", ex);
        }
    }

    @Override
    public void initializeView() {
        try
        {
            logger.info("XteTestToolRichiesteMassivePRView start initializeView");

//            buildComponentSortedMap();
//
//            this.setFocusCycleRoot(true);
//            this.setFocusTraversalPolicy(new XteTestToolFocusTraversalPolicy(componentSortedMap));

            massiveRequestjTabbedPane.setUI(new XteTestToolTabbedUI());

            defineTabPolicyForTextArea(xmlDatajTextArea);
            defineTabPolicyForTextArea(notificaDatajTextArea);
            
            JLabel cornelLabel = new JLabel("");
            cornelLabel.setOpaque(true);
            cornelLabel.setBackground(new Color(240, 240, 240));

            this.xmlDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.xmlDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.xmlDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.xmlDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.xmlDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            this.lasciaPrendiDatajScrollPane.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, cornelLabel);
            this.lasciaPrendiDatajScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            this.lasciaPrendiDatajScrollPane.getVerticalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.VERTICAL_SCROLLBAR, new Dimension(20,20)));
            this.lasciaPrendiDatajScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            this.lasciaPrendiDatajScrollPane.getHorizontalScrollBar().setUI(new XteTestToolScrollBarUI(XteTestToolScrollBarUI.HORIZONTAL_SCROLLBAR, new Dimension(20,20)));

            customizeTable(this.lasciaPrendijTable);
            this.lasciaPrendijTable.setModel(xteLasciaPrendiPRTableModel);

            List<String> eventColumnWidthValues = new ArrayList<String>();
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("135");
            eventColumnWidthValues.add("180");
            eventColumnWidthValues.add("125");

            this.lasciaPrendijTable.getTableHeader().setDefaultRenderer(new XteTableHeaderRenderer(eventColumnWidthValues));
            this.lasciaPrendijTable.setDefaultRenderer(Object.class, new XteLasciaPrendiTableCellRenderer(eventColumnWidthValues));
            
            logger.info("XteTestToolRichiesteMassivePRView initializeView executed successfully");
        } catch (Exception ex) {
            logger.error("XteTestToolRichiesteMassivePRView initializeView failed", ex);
            throw new XteTestToolViewException(Group.GENERIC, Severity.FATAL, "Richieste Massive Posta Registrata view initialization failed", ex);
        }
    }

    @Override
    public void restoreView() {
        logger.debug("XteTestToolRichiesteMassivePRView restoreView");
        this.getExitButtonRef().doClick();
    }

    private void customizeTable(JTable table) {
        table.getTableHeader().setReorderingAllowed(false);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setRowHeight(20);
        table.setSelectionModel(new XteListSelectionModel());
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    private  static void defineTabPolicyForTextArea(final JTextArea textarea) {

        final Object tabActionKey = new Object();
        final Object shiftTabActionKey = new Object();
        KeyStroke tabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0, false);
        KeyStroke shiftTabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_DOWN_MASK, false);

        Action tabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusNextComponent((Component) source);
                }
            }
        };

        Action shiftTabAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source instanceof Component) {
                    FocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent((Component) source);
                }
            }
        };

        textarea.getActionMap().put(tabActionKey, tabAction);
        textarea.getInputMap().put(tabKeyStroke, tabActionKey);

        textarea.getActionMap().put(shiftTabActionKey, shiftTabAction);
        textarea.getInputMap().put(shiftTabKeyStroke, shiftTabActionKey);

    }
    private void buildComponentSortedMap() {

        componentSortedMap = new TreeMap<Integer, Component>();
        componentSortedMap.put(1, this.bundleIdjTextField);
        componentSortedMap.put(2, this.bundleTPIdjTextField);
        componentSortedMap.put(3, this.xmlDatajTextArea);
        componentSortedMap.put(3, this.xmlDatajTextArea);
        componentSortedMap.put(3, this.xmlDatajTextArea);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        commandjPanel = new javax.swing.JPanel();
        messageBarjScrollPane = new javax.swing.JScrollPane();
        messageBarjTextPane = new javax.swing.JTextPane();
        homejButton = new javax.swing.JButton();
        exitjButton = new javax.swing.JButton();
        backjButton = new javax.swing.JButton();
        workingjPanel = new javax.swing.JPanel();
        panelTitlejLabel = new javax.swing.JLabel();
        xmlDatajPanel = new javax.swing.JPanel();
        xmlDatajScrollPane = new javax.swing.JScrollPane();
        xmlDatajTextArea = new javax.swing.JTextArea();
        xmlDataCommandjPanel = new javax.swing.JPanel();
        generaXmlDatajButton = new javax.swing.JButton();
        bundleDatajPanel = new javax.swing.JPanel();
        bundleTPIdjLabel = new javax.swing.JLabel();
        bundleTPIdjTextField = new javax.swing.JTextField();
        bundleIdjTextField = new javax.swing.JTextField();
        bundleIdjLabel = new javax.swing.JLabel();
        massiveRequestjTabbedPane = new javax.swing.JTabbedPane();
        notificajPanel = new javax.swing.JPanel();
        notificaDatajScrollPane = new javax.swing.JScrollPane();
        notificaDatajTextArea = new javax.swing.JTextArea();
        notificaDataCommandjPanel = new javax.swing.JPanel();
        notificaGeneraXmlDatajButton = new javax.swing.JButton();
        lasciaPrendijPanel = new javax.swing.JPanel();
        lasciaPrendiDatajScrollPane = new javax.swing.JScrollPane();
        lasciaPrendijTable = new javax.swing.JTable();
        lasciaPrendiDataCommandjPanel = new javax.swing.JPanel();
        startLasciaPrendiAutojButton = new javax.swing.JButton();
        lasciaInviiMenoUnojButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        setMaximumSize(new java.awt.Dimension(1000, 690));
        setMinimumSize(new java.awt.Dimension(1000, 690));
        setPreferredSize(new java.awt.Dimension(1000, 690));

        commandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        commandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        commandjPanel.setMaximumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setMinimumSize(new java.awt.Dimension(980, 80));
        commandjPanel.setPreferredSize(new java.awt.Dimension(980, 80));

        messageBarjScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        messageBarjScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, null, new java.awt.Color(51, 51, 0)));
        messageBarjScrollPane.setMaximumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setMinimumSize(new java.awt.Dimension(350, 50));
        messageBarjScrollPane.setPreferredSize(new java.awt.Dimension(350, 50));

        messageBarjTextPane.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        messageBarjTextPane.setFocusable(false);
        messageBarjScrollPane.setViewportView(messageBarjTextPane);

        homejButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        homejButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Home3.png"))); // NOI18N
        homejButton.setText("<html><body>&nbsp;Home<body><html>");
        homejButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        homejButton.setFocusPainted(false);
        homejButton.setFocusable(false);
        homejButton.setForeground(new java.awt.Color(51, 51, 51));
        homejButton.setIconTextGap(5);
        homejButton.setMaximumSize(new java.awt.Dimension(100, 50));
        homejButton.setMinimumSize(new java.awt.Dimension(100, 50));
        homejButton.setPreferredSize(new java.awt.Dimension(100, 50));

        exitjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        exitjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Exit1.png"))); // NOI18N
        exitjButton.setText("<html><body>&nbsp;Exit<body><html>");
        exitjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        exitjButton.setFocusPainted(false);
        exitjButton.setFocusable(false);
        exitjButton.setForeground(new java.awt.Color(51, 51, 51));
        exitjButton.setIconTextGap(5);
        exitjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        exitjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        exitjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        backjButton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        backjButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/BackToMenu.png"))); // NOI18N
        backjButton.setText("<html><body>&nbsp;Back<body><html>");
        backjButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        backjButton.setFocusPainted(false);
        backjButton.setFocusable(false);
        backjButton.setForeground(new java.awt.Color(51, 51, 51));
        backjButton.setIconTextGap(5);
        backjButton.setMaximumSize(new java.awt.Dimension(100, 50));
        backjButton.setMinimumSize(new java.awt.Dimension(100, 50));
        backjButton.setPreferredSize(new java.awt.Dimension(100, 50));

        javax.swing.GroupLayout commandjPanelLayout = new javax.swing.GroupLayout(commandjPanel);
        commandjPanel.setLayout(commandjPanelLayout);
        commandjPanelLayout.setHorizontalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, commandjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 272, Short.MAX_VALUE)
                .addComponent(backjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(homejButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(exitjButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        commandjPanelLayout.setVerticalGroup(
            commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandjPanelLayout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addGroup(commandjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(homejButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exitjButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(messageBarjScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        workingjPanel.setBackground(new java.awt.Color(230, 250, 0));
        workingjPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20)));
        workingjPanel.setMaximumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setMinimumSize(new java.awt.Dimension(980, 580));
        workingjPanel.setPreferredSize(new java.awt.Dimension(980, 580));

        panelTitlejLabel.setBackground(new java.awt.Color(240, 240, 240));
        panelTitlejLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        panelTitlejLabel.setForeground(new java.awt.Color(0, 51, 255));
        panelTitlejLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelTitlejLabel.setText("Richieste Massive");
        panelTitlejLabel.setFocusable(false);
        panelTitlejLabel.setIconTextGap(30);
        panelTitlejLabel.setMaximumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setMinimumSize(new java.awt.Dimension(920, 20));
        panelTitlejLabel.setPreferredSize(new java.awt.Dimension(920, 20));

        xmlDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Risultato Acquisizione", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        xmlDatajPanel.setFocusable(false);
        xmlDatajPanel.setMaximumSize(new java.awt.Dimension(860, 180));
        xmlDatajPanel.setMinimumSize(new java.awt.Dimension(860, 180));
        xmlDatajPanel.setOpaque(false);
        xmlDatajPanel.setPreferredSize(new java.awt.Dimension(860, 180));

        xmlDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        xmlDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        xmlDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 130));
        xmlDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 130));
        xmlDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 130));

        xmlDatajTextArea.setColumns(20);
        xmlDatajTextArea.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        xmlDatajTextArea.setForeground(new java.awt.Color(51, 51, 51));
        xmlDatajTextArea.setLineWrap(true);
        xmlDatajTextArea.setRows(5);
        xmlDatajTextArea.setWrapStyleWord(true);
        xmlDatajTextArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        xmlDatajScrollPane.setViewportView(xmlDatajTextArea);

        xmlDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        xmlDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        xmlDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 45));
        xmlDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 45));
        xmlDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 45));
        xmlDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        generaXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        generaXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        generaXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml6.png"))); // NOI18N
        generaXmlDatajButton.setText("<html><body>&nbsp;Genera<br>&nbsp;&nbsp;XML<body><html>");
        generaXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        generaXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        generaXmlDatajButton.setFocusPainted(false);
        generaXmlDatajButton.setFocusable(false);
        generaXmlDatajButton.setIconTextGap(2);
        generaXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        generaXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        xmlDataCommandjPanel.add(generaXmlDatajButton, gridBagConstraints);

        javax.swing.GroupLayout xmlDatajPanelLayout = new javax.swing.GroupLayout(xmlDatajPanel);
        xmlDatajPanel.setLayout(xmlDatajPanelLayout);
        xmlDatajPanelLayout.setHorizontalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        xmlDatajPanelLayout.setVerticalGroup(
            xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, xmlDatajPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(xmlDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(xmlDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(xmlDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        bundleDatajPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Informazioni Mazzetto", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 51, 255))); // NOI18N
        bundleDatajPanel.setMaximumSize(new java.awt.Dimension(860, 70));
        bundleDatajPanel.setMinimumSize(new java.awt.Dimension(860, 70));
        bundleDatajPanel.setOpaque(false);
        bundleDatajPanel.setPreferredSize(new java.awt.Dimension(860, 70));

        bundleTPIdjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleTPIdjLabel.setText("ID Operatore TP:");
        bundleTPIdjLabel.setFocusable(false);
        bundleTPIdjLabel.setMaximumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel.setMinimumSize(new java.awt.Dimension(120, 20));
        bundleTPIdjLabel.setPreferredSize(new java.awt.Dimension(120, 20));

        bundleTPIdjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleTPIdjTextField.setText("Mario Rossi");
        bundleTPIdjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        bundleTPIdjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        bundleTPIdjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        bundleTPIdjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        bundleIdjTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleIdjTextField.setText("38999123456789012789");
        bundleIdjTextField.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        bundleIdjTextField.setMaximumSize(new java.awt.Dimension(250, 20));
        bundleIdjTextField.setMinimumSize(new java.awt.Dimension(250, 20));
        bundleIdjTextField.setPreferredSize(new java.awt.Dimension(250, 20));

        bundleIdjLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bundleIdjLabel.setForeground(new java.awt.Color(51, 51, 51));
        bundleIdjLabel.setText("Codice Identificativo:");
        bundleIdjLabel.setFocusable(false);
        bundleIdjLabel.setMaximumSize(new java.awt.Dimension(140, 20));
        bundleIdjLabel.setMinimumSize(new java.awt.Dimension(140, 20));
        bundleIdjLabel.setPreferredSize(new java.awt.Dimension(140, 20));

        javax.swing.GroupLayout bundleDatajPanelLayout = new javax.swing.GroupLayout(bundleDatajPanel);
        bundleDatajPanel.setLayout(bundleDatajPanelLayout);
        bundleDatajPanelLayout.setHorizontalGroup(
            bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bundleIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addComponent(bundleTPIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleTPIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        bundleDatajPanelLayout.setVerticalGroup(
            bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bundleDatajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bundleTPIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bundleTPIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(bundleDatajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(bundleIdjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bundleIdjTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(9, Short.MAX_VALUE))
        );

        massiveRequestjTabbedPane.setBackground(new java.awt.Color(255, 255, 255));
        massiveRequestjTabbedPane.setForeground(new java.awt.Color(0, 51, 255));
        massiveRequestjTabbedPane.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        massiveRequestjTabbedPane.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        massiveRequestjTabbedPane.setMaximumSize(new java.awt.Dimension(860, 240));
        massiveRequestjTabbedPane.setMinimumSize(new java.awt.Dimension(860, 240));
        massiveRequestjTabbedPane.setPreferredSize(new java.awt.Dimension(860, 240));

        notificajPanel.setBackground(new java.awt.Color(230, 250, 0));
        notificajPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));

        notificaDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        notificaDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        notificaDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        notificaDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 170));
        notificaDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 170));
        notificaDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 170));

        notificaDatajTextArea.setColumns(20);
        notificaDatajTextArea.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        notificaDatajTextArea.setForeground(new java.awt.Color(51, 51, 51));
        notificaDatajTextArea.setLineWrap(true);
        notificaDatajTextArea.setRows(5);
        notificaDatajTextArea.setWrapStyleWord(true);
        notificaDatajTextArea.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        notificaDatajScrollPane.setViewportView(notificaDatajTextArea);

        notificaDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        notificaDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        notificaDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 45));
        notificaDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 45));
        notificaDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 45));
        notificaDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        notificaGeneraXmlDatajButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        notificaGeneraXmlDatajButton.setForeground(new java.awt.Color(51, 51, 51));
        notificaGeneraXmlDatajButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Xml6.png"))); // NOI18N
        notificaGeneraXmlDatajButton.setText("<html><body>&nbsp;&nbsp;Genera<br>XML Notif.<body><html>");
        notificaGeneraXmlDatajButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        notificaGeneraXmlDatajButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        notificaGeneraXmlDatajButton.setFocusPainted(false);
        notificaGeneraXmlDatajButton.setFocusable(false);
        notificaGeneraXmlDatajButton.setIconTextGap(3);
        notificaGeneraXmlDatajButton.setMaximumSize(new java.awt.Dimension(100, 35));
        notificaGeneraXmlDatajButton.setMinimumSize(new java.awt.Dimension(100, 35));
        notificaGeneraXmlDatajButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        notificaDataCommandjPanel.add(notificaGeneraXmlDatajButton, gridBagConstraints);

        javax.swing.GroupLayout notificajPanelLayout = new javax.swing.GroupLayout(notificajPanel);
        notificajPanel.setLayout(notificajPanelLayout);
        notificajPanelLayout.setHorizontalGroup(
            notificajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, notificajPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(notificaDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addComponent(notificaDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        notificajPanelLayout.setVerticalGroup(
            notificajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, notificajPanelLayout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addGroup(notificajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(notificaDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(notificaDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        massiveRequestjTabbedPane.addTab("Notifica", notificajPanel);

        lasciaPrendijPanel.setBackground(new java.awt.Color(230, 250, 0));
        lasciaPrendijPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));

        lasciaPrendiDatajScrollPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        lasciaPrendiDatajScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        lasciaPrendiDatajScrollPane.setViewportBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lasciaPrendiDatajScrollPane.setMaximumSize(new java.awt.Dimension(684, 170));
        lasciaPrendiDatajScrollPane.setMinimumSize(new java.awt.Dimension(684, 170));
        lasciaPrendiDatajScrollPane.setPreferredSize(new java.awt.Dimension(684, 170));

        lasciaPrendijTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        lasciaPrendiDatajScrollPane.setViewportView(lasciaPrendijTable);

        lasciaPrendiDataCommandjPanel.setBackground(new java.awt.Color(204, 204, 204));
        lasciaPrendiDataCommandjPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        lasciaPrendiDataCommandjPanel.setMaximumSize(new java.awt.Dimension(110, 85));
        lasciaPrendiDataCommandjPanel.setMinimumSize(new java.awt.Dimension(110, 85));
        lasciaPrendiDataCommandjPanel.setPreferredSize(new java.awt.Dimension(110, 85));
        lasciaPrendiDataCommandjPanel.setLayout(new java.awt.GridBagLayout());

        startLasciaPrendiAutojButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        startLasciaPrendiAutojButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Start 1.png"))); // NOI18N
        startLasciaPrendiAutojButton.setText("<html><body>&nbsp;&nbsp;Start<br>&nbsp;L/P Auto<body><html>");
        startLasciaPrendiAutojButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        startLasciaPrendiAutojButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        startLasciaPrendiAutojButton.setFocusPainted(false);
        startLasciaPrendiAutojButton.setFocusable(false);
        startLasciaPrendiAutojButton.setForeground(new java.awt.Color(51, 51, 51));
        startLasciaPrendiAutojButton.setIconTextGap(1);
        startLasciaPrendiAutojButton.setMaximumSize(new java.awt.Dimension(100, 35));
        startLasciaPrendiAutojButton.setMinimumSize(new java.awt.Dimension(100, 35));
        startLasciaPrendiAutojButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 3, 0, 0);
        lasciaPrendiDataCommandjPanel.add(startLasciaPrendiAutojButton, gridBagConstraints);

        lasciaInviiMenoUnojButton.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lasciaInviiMenoUnojButton.setForeground(new java.awt.Color(51, 51, 51));
        lasciaInviiMenoUnojButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Remove1.png"))); // NOI18N
        lasciaInviiMenoUnojButton.setText("<html><body>&nbsp;Lascia<br>&nbsp;Invii -1<body><html>");
        lasciaInviiMenoUnojButton.setActionCommand("<html><body>&nbsp;Genera<br>Xml Data<body><html>");
        lasciaInviiMenoUnojButton.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)), new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        lasciaInviiMenoUnojButton.setFocusPainted(false);
        lasciaInviiMenoUnojButton.setFocusable(false);
        lasciaInviiMenoUnojButton.setIconTextGap(2);
        lasciaInviiMenoUnojButton.setMaximumSize(new java.awt.Dimension(100, 35));
        lasciaInviiMenoUnojButton.setMinimumSize(new java.awt.Dimension(100, 35));
        lasciaInviiMenoUnojButton.setPreferredSize(new java.awt.Dimension(100, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 3, 0, 0);
        lasciaPrendiDataCommandjPanel.add(lasciaInviiMenoUnojButton, gridBagConstraints);

        javax.swing.GroupLayout lasciaPrendijPanelLayout = new javax.swing.GroupLayout(lasciaPrendijPanel);
        lasciaPrendijPanel.setLayout(lasciaPrendijPanelLayout);
        lasciaPrendijPanelLayout.setHorizontalGroup(
            lasciaPrendijPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lasciaPrendijPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lasciaPrendiDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(lasciaPrendiDatajScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        lasciaPrendijPanelLayout.setVerticalGroup(
            lasciaPrendijPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lasciaPrendijPanelLayout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addGroup(lasciaPrendijPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lasciaPrendiDatajScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lasciaPrendiDataCommandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        massiveRequestjTabbedPane.addTab("Lascia - Prendi", lasciaPrendijPanel);

        javax.swing.GroupLayout workingjPanelLayout = new javax.swing.GroupLayout(workingjPanel);
        workingjPanel.setLayout(workingjPanelLayout);
        workingjPanelLayout.setHorizontalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(workingjPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(workingjPanelLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bundleDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(massiveRequestjTabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        workingjPanelLayout.setVerticalGroup(
            workingjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, workingjPanelLayout.createSequentialGroup()
                .addComponent(panelTitlejLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bundleDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(xmlDatajPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(massiveRequestjTabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(workingjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(commandjPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backjButton;
    private javax.swing.JPanel bundleDatajPanel;
    private javax.swing.JLabel bundleIdjLabel;
    private javax.swing.JTextField bundleIdjTextField;
    private javax.swing.JLabel bundleTPIdjLabel;
    private javax.swing.JTextField bundleTPIdjTextField;
    private javax.swing.JPanel commandjPanel;
    private javax.swing.JButton exitjButton;
    private javax.swing.JButton generaXmlDatajButton;
    private javax.swing.JButton homejButton;
    private javax.swing.JButton lasciaInviiMenoUnojButton;
    private javax.swing.JPanel lasciaPrendiDataCommandjPanel;
    private javax.swing.JScrollPane lasciaPrendiDatajScrollPane;
    private javax.swing.JPanel lasciaPrendijPanel;
    private javax.swing.JTable lasciaPrendijTable;
    private javax.swing.JTabbedPane massiveRequestjTabbedPane;
    private javax.swing.JScrollPane messageBarjScrollPane;
    private javax.swing.JTextPane messageBarjTextPane;
    private javax.swing.JPanel notificaDataCommandjPanel;
    private javax.swing.JScrollPane notificaDatajScrollPane;
    private javax.swing.JTextArea notificaDatajTextArea;
    private javax.swing.JButton notificaGeneraXmlDatajButton;
    private javax.swing.JPanel notificajPanel;
    private javax.swing.JLabel panelTitlejLabel;
    private javax.swing.JButton startLasciaPrendiAutojButton;
    private javax.swing.JPanel workingjPanel;
    private javax.swing.JPanel xmlDataCommandjPanel;
    private javax.swing.JPanel xmlDatajPanel;
    private javax.swing.JScrollPane xmlDatajScrollPane;
    private javax.swing.JTextArea xmlDatajTextArea;
    // End of variables declaration//GEN-END:variables


}
