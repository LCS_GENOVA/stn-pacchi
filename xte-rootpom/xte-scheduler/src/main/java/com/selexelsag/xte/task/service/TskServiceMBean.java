package com.selexelsag.xte.task.service;

import org.jboss.system.ServiceMBean;

public interface TskServiceMBean extends ServiceMBean {

	public void setSpringConfigPath(String path);

    public String getSpringConfigPath();

    public boolean isSpringLoaded();

    public boolean isStarted();

    public long getErrorCount();

    public String getLastStartDate();

}