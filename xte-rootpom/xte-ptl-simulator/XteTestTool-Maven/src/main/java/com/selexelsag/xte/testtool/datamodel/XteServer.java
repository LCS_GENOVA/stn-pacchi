/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.datamodel;


/**
 *
 * @author Da Procida
 */
public class XteServer {

    private String serverBaseURL;
    private String serverVersion;
    private String serverDescription;
    private boolean active;

    public String getServerBaseURL() {
        return serverBaseURL;
    }

    public void setServerBaseURL(String serverBaseURL) {
        this.serverBaseURL = serverBaseURL;
    }

    public String getServerDescription() {
        return serverDescription;
    }

    public void setServerDescription(String serverDescription) {
        this.serverDescription = serverDescription;
    }


    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getServerVersion() {
        return serverVersion;
    }

    public void setServerVersion(String serverVersion) {
        this.serverVersion = serverVersion;
    }    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (this.serverBaseURL != null ? this.serverBaseURL.hashCode() : 0);
        hash = 61 * hash + (this.serverVersion != null ? this.serverVersion.hashCode() : 0);
        hash = 61 * hash + (this.serverDescription != null ? this.serverDescription.hashCode() : 0);
        hash = 61 * hash + (this.active ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final XteServer other = (XteServer) obj;
        if ((this.serverBaseURL == null) ? (other.serverBaseURL != null) : !this.serverBaseURL.equals(other.serverBaseURL)) {
            return false;
        }
        if ((this.serverVersion == null) ? (other.serverVersion != null) : !this.serverVersion.equals(other.serverVersion)) {
            return false;
        }
        if ((this.serverDescription == null) ? (other.serverDescription != null) : !this.serverDescription.equals(other.serverDescription)) {
            return false;
        }
        if (this.active != other.active) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "XteServer{" + "serverBaseURL=" + serverBaseURL + ", serverVersion=" + serverVersion + ", serverDescription=" + serverDescription + ", active=" + active + '}';
    }
    
}
