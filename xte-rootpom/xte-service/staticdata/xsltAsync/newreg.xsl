<?xml version="1.0"?>
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" encoding="utf-8" omit-xml-declaration="no" indent="yes"/>
	<xsl:param name="channelID" />
	
	<xsl:template match="/">
		<MSG>
			<!-- HDR SWREL="A0000" OFCID="00000"/-->
			<HDR SWREL="A0000">
			<xsl:attribute name="OFCID">
				<xsl:value-of select="$channelID"/>
			</xsl:attribute>
			</HDR>
			<M1>
					<xsl:apply-templates/>
			</M1>
		</MSG>
	</xsl:template>

	<xsl:template match="OBJ">
	<M1D>
		<OBJ>
			<xsl:apply-templates/>
			<PH>A</PH>
			<PSTF>0</PSTF>
			<TDT>
				<xsl:value-of select="concat(substring(/MSG/HDR/DSPED,9,2),'/',substring(/MSG/HDR/DSPED,6,2),'/',substring(/MSG/HDR/DSPED,1,4), ' ',substring(/MSG/HDR/DSPED, 12,8) )"/>
			</TDT>
			<OP>1</OP>
		</OBJ>
		<ACC>
			<AOFC>
				<xsl:value-of select="/MSG/HDR/OFFID"/>
			</AOFC>
			<ADT>
				<xsl:value-of select="concat(substring(/MSG/HDR/DSPED,9,2),'/',substring(/MSG/HDR/DSPED,6,2),'/',substring(/MSG/HDR/DSPED,1,4), ' ',substring(/MSG/HDR/DSPED, 12,8) )"/>
			</ADT>

			<xsl:choose>
				<xsl:when test="ZIP">
					<ZIP>
					<xsl:value-of select="ZIP"/>
					</ZIP>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>

			<ACURR>E</ACURR>
			<REGT>G</REGT>

			<xsl:choose>
				<xsl:when test="CODEAR">
					<ARID>
					<xsl:value-of select="CODEAR"/>
					</ARID>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>

			<xsl:choose>
				<xsl:when test="SA">
					<SA>
					<xsl:value-of select="SA"/>
					</SA>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>


			<xsl:choose>
				<xsl:when test="WB">
						<W>
							<xsl:value-of select="WB"/>
						</W>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</ACC>

		<REG>
			<xsl:choose>
				<xsl:when test="INSVAL">
						<INSVAL>
							<xsl:value-of select="INSVAL"/>
						</INSVAL>
				</xsl:when>
					<xsl:otherwise/>
			</xsl:choose>
			<RCURR>E</RCURR>
			<CCODE>
				<xsl:value-of select="/MSG/HDR/CUSTCODE"/>
			</CCODE>
			<xsl:choose>
				<xsl:when test="ADDR">
						<ADDR>
							<xsl:value-of select="ADDR"/>
						</ADDR>
				</xsl:when>
					<xsl:otherwise/>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="DEST">
						<DEST>
							<xsl:value-of select="DEST"/>
						</DEST>
				</xsl:when>
					<xsl:otherwise/>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="DADDR">
						<DADDR>
							<xsl:value-of select="DADDR"/>
						</DADDR>
				</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>

			<xsl:choose>
				<xsl:when test="CODVAL">
						<CODVAL>
							<xsl:value-of select="CODVAL"/>
						</CODVAL>
				</xsl:when>
					<xsl:otherwise/>
			</xsl:choose>


		</REG>
		<xsl:choose>
			<xsl:when test="PARAM">
					<PARAM>
						<xsl:value-of select="PARAM" />
					</PARAM>
			</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</M1D>
	</xsl:template>

	<xsl:template match="OBJID|SUBC">
		<xsl:element name="{name()}">
			<xsl:value-of select="."/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="WA|NSEALS|NCHECK|EXPR|HDR|DEST|ADDR|ZIP|DADDR|SA|PARAM"/>
</xsl:transform>

