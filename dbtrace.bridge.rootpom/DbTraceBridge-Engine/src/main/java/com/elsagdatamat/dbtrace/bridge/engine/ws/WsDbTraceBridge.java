package com.elsagdatamat.dbtrace.bridge.engine.ws;


import java.math.BigInteger;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.elsagdatamat.dbtrace.bridge.engine.IDbTraceBridge;
import com.elsagdatamat.dbtrace.bridge.engine.exceptions.InsertTracesException;
import com.elsagdatamat.dbtrace.bridge.engine.exceptions.TrackDBRWException;
import com.elsagdatamat.framework.entities.EntityCreationException;
import com.elsagdatamat.framework.entities.IEntityBuilder;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

public class WsDbTraceBridge<T> implements IDbTraceBridge<T>
{

	private Log log = LogFactory.getLog(this.getClass());
	
	private IEntityBuilder<T,List<CustomTraceWs>> traceBuilder;
	
	private ITrackDBRWServiceManager trackDBRWServiceManager;
	
	public void setTrackDBRWServiceManager(ITrackDBRWServiceManager trackDBRWServiceManager) {
		this.trackDBRWServiceManager = trackDBRWServiceManager;
	}

	public void setTraceBuilder(IEntityBuilder<T,List<CustomTraceWs>> traceBuilder) {
		this.traceBuilder = traceBuilder;
	}

	public IEntityBuilder<T,List<CustomTraceWs>> getTraceBuilder() {
		return traceBuilder;
	}
	
	public void init() {
		log.info("Initializing WsDbTraceBridge...");
		
		if(trackDBRWServiceManager==null)
			throw new NullPointerException("trackDBRWServiceManager not set");
		if(traceBuilder==null)
			throw new NullPointerException("traceBuilder not set");
		
		log.info("WsDbTraceBridge initialized");
	}
	
	@Override
	public void insertTraces(T input) throws InsertTracesException, TrackDBRWException  {
	
		if(input==null)
			throw new IllegalArgumentException("Input is null");
		
		log.info("Message received: " + input.toString());

		List<CustomTraceWs> lst;
		try{
			lst = traceBuilder.create(input);
		}
		catch (EntityCreationException e) {
			throw new InsertTracesException("Impossibile creare le tracce", e);
		} 
		
		if(lst.isEmpty()){
			throw new InsertTracesException("Nessuna traccia estraibile dall'input");
		}
	
		TracesList traceList = new TracesList();
		traceList.getTraces().addAll(lst);
		
		BigInteger bResult = BigInteger.ZERO;
		try {
			bResult = trackDBRWServiceManager.getServicePort().putTraces(traceList);
		} catch (Exception e) {
			throw new TrackDBRWException("Errore durante la chiamata al WebService",e);
		}
		
		if (bResult.intValue() < traceList.getTraces().size()) {
			throw new InsertTracesException(
					"Errore durante l'inserimento delle tracce: inserite "
							+ bResult.intValue() + " su "
							+ traceList.getTraces().size()
							+ " tracce previste");
		}
	}

	
}
