/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.selexelsag.xte.testtool.exception;

//import com.selexelsag.mbda.main.TotemInstallation;
import java.awt.Window;
import java.lang.reflect.Method;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

/**
 *
 * @author Da Procida
 */
public class XteTestToolUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {

    private static final String BUNDLES_MESSAGES = "bundles/messages";
    private static final String RESTORE_METHOD_NAME = "restore";
    private final ResourceBundle messages;
    private Logger logger = Logger.getLogger(XteTestToolUncaughtExceptionHandler.class);

    public XteTestToolUncaughtExceptionHandler() {
        this.messages = ResourceBundle.getBundle(BUNDLES_MESSAGES, Locale.getDefault());
    }

    @Override
    public final void uncaughtException(Thread t, Throwable e) {
        handleException(e);
    }

    protected void handleException(Throwable t) {
        t.printStackTrace();
        logger.error(t);
        if (t instanceof BaseException) {
            handleBaseException((BaseException) t);
        } else {
            handleNotApplicationException(t);
        }


    }

    private void callRestoreMethod(Method restoreMethod, Window activeWindow, Throwable t) {
        if (restoreMethod != null) {
            Class<?>[] parameterTypes = restoreMethod.getParameterTypes();
            if (parameterTypes.length == 1 && Throwable.class.isAssignableFrom(parameterTypes[0])) {
                try {
                    restoreMethod.invoke(activeWindow, t);
                } catch (Exception ex) {
                    logger.error(ex);
                }
            } else {
                try {
                    restoreMethod.invoke(activeWindow);
                } catch (Exception ex) {
                    logger.error(ex);
                }
            }
        }
    }

    private Method getRestoreMethod(Object target) {
        if (target == null) {
            return null;
        }
        Method method = null;
        Method[] methods = target.getClass().getMethods();
        for (Method m : methods) {
            if (RESTORE_METHOD_NAME.equalsIgnoreCase(m.getName())) {
                return m;
            }
        }
        return method;
    }

    private Window getActiveWindow() {
        Window[] windows = Window.getWindows();
        for (Window window : windows) {
            if (window.isActive() && window.isEnabled() && window.isVisible()) {
                return window;
            }
        }
        return null;
    }

    private void handleNotApplicationException(Throwable t) {
        Window activeWindow = getActiveWindow();
        String message = messages.getString("generic.error");
        String title = messages.getString("generic.title");
        JOptionPane.showMessageDialog(activeWindow, message, title, JOptionPane.ERROR_MESSAGE);
        if (activeWindow != null) {
            Method restoreMethod = getRestoreMethod(activeWindow);
            if (restoreMethod != null) {
                callRestoreMethod(restoreMethod, activeWindow, t);
            }
        }
    }

    private void handleBaseException(BaseException baseException) {
        Window activeWindow = getActiveWindow();
        
        if (activeWindow != null) {
            logger.debug("XteTestToolUncaughtExceptionHandler handleBaseException activeWindow: " + activeWindow);
        }
        else {
            logger.debug("XteTestToolUncaughtExceptionHandler handleBaseException activeWindow is null");
        }

        String message = getDialogErrorMessage(baseException);
        String title = getDialogErrorTitle(baseException);
        int messageType = getJOptionPaneMessageType(baseException);
        Severity severity = baseException.getSeverity();
        JOptionPane.showMessageDialog(activeWindow, message, title, messageType);
        if (Severity.WARNING.equals(severity)) {
        } else if (Severity.ERROR.equals(severity)) {
            if (activeWindow != null) {
                Method restoreMethod = getRestoreMethod(activeWindow);
                if (restoreMethod != null) {
                    callRestoreMethod(restoreMethod, activeWindow, baseException);
                }
            }
        } else if (Severity.FATAL.equals(severity)) {
            if (activeWindow != null) {
                activeWindow.dispose();
                System.exit(-1);
            }
            else
            {
                System.exit(-1);
            }
            
        }
    }

    private String getDialogErrorMessage(BaseException e) {
        try {
            String key = (e.getGroup() + "." + e.getSeverity()).toLowerCase();
            return messages.getString(key);
        } catch (Exception ex) {
            return "No message";
        }
    }

    private String getDialogErrorTitle(BaseException e) {
        try {
            String key = (e.getGroup() + "." + "title").toLowerCase();
            return messages.getString(key);
        } catch (Exception ex) {
            return "No title";
        }
    }

    private int getJOptionPaneMessageType(Exception exception) {
        if (exception instanceof BaseException && Severity.WARNING.equals(((BaseException) exception).getSeverity())) {
            return JOptionPane.WARNING_MESSAGE;
        } else {
            return JOptionPane.ERROR_MESSAGE;
        }
    }

}
