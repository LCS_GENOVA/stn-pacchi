/**
 * 
 */
package com.selexelsag.dbtrace.bridge.to.ssip.message.trans;


/**
 * @author arodriguez
 *
 */
public enum TPAHeaderTransformationValues {

	SDA("SDA","FSDA"), NSP("UNSP","UPOS"),
	OMP("UNSP","UPOS"), TT("TT","PI"),
	APT("UNSP","UPOS");
		
	private String channel = null;
	private String whereHappened = null;
	
	/**
	 * private enum constructor
	 * @param key  String key to match value
	 */
	private TPAHeaderTransformationValues(String channel, String whereHappened) {
		this.channel = channel;
		this.whereHappened = whereHappened;
	}

	/**
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * @return the whereHappened
	 */
	public String getWhereHappened() {
		return whereHappened;
	}

}
