package com.elsagdatamat.dbtrace.bridge.totgc.dao;

import com.elsagdatamat.dbtrace.bridge.totgc.entities.TraceTypesToSend;
import com.elsagdatamat.dbtrace.bridge.totgc.entities.TraceTypesToSendPK;
import com.elsagdatamat.framework.dao.IGenericDAO;

public interface ITraceTypesToSendDAO extends IGenericDAO<TraceTypesToSend, TraceTypesToSendPK> {

}