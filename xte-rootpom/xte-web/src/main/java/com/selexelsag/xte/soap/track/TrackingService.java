/**
 * TrackingService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.selexelsag.xte.soap.track;

public interface TrackingService extends javax.xml.rpc.Service {
    public java.lang.String getTrackingPortAddress();

    public com.selexelsag.xte.soap.track.Tracking getTrackingPort() throws javax.xml.rpc.ServiceException;

    public com.selexelsag.xte.soap.track.Tracking getTrackingPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
