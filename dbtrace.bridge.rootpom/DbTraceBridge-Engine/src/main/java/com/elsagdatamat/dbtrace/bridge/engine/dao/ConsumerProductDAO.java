package com.elsagdatamat.dbtrace.bridge.engine.dao;

import com.elsagdatamat.dbtrace.bridge.engine.entities.ConsumerProduct;
import com.elsagdatamat.dbtrace.bridge.engine.entities.ConsumerProductPK;
import com.elsagdatamat.framework.dao.spring.jpa.hibernate.SpringHibernateJpaDAOBase;

public class ConsumerProductDAO extends SpringHibernateJpaDAOBase<ConsumerProduct, ConsumerProductPK> implements IConsumerProductDAO{

}
