package com.selexelsag.xte.persistence.utils;

import com.selexelsag.xte.persistence.entities.AsyncMessages;
import com.selexelsag.xte.persistence.exceptions.PersistenceException;

public class PersistenceUtils 
{
	public static AsyncMessages fillAsyncMessages(String flowId, String sourceOfficeId, String destinationOfficeId, String objectId, StringBuilder data) throws PersistenceException 
	{
		AsyncMessages message = new AsyncMessages();
		message.setFlowId(flowId);
		message.setSourceOfficeId(sourceOfficeId);
		message.setDestinationOfficeId(destinationOfficeId);
		message.setObjectId(objectId);
		message.setOriginalMessage(data.toString());
		return message;
	}
}
