package com.elsagdatamat.dbtrace.bridge.tott.test;

import java.net.URL;
import java.util.Date;

import javax.annotation.Resource;
import javax.xml.namespace.QName;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.elsagdatamat.dbtrace.bridge.engine.IMessageBuilder;
import com.elsagdatamat.dbtrace.bridge.engine.ITracesReader;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWService;
import com.selexelsag.tracktrace.trackdbws.TrackDBRWServicePortType;
import com.selexelsag.tracktrace.trackdbws.xml.TracesList;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" ,
		"classpath:/applicationContext-dao.xml",
"classpath:/applicationContext-core.xml"})
@Transactional
@TransactionConfiguration(defaultRollback=true)

public class ToTTTest {

	@Resource(name="toTTMessageBuilder")
	protected IMessageBuilder messageBuilder;
	@Resource(name="toTTMessageBuilder")
	protected ITracesReader tracesReader;
	@Test
	public void callService() throws Exception{
		URL url = new URL("http://localhost:8080/TrackDBWS/TrackDBRWService?wsdl"); // "http://localhost:8080/pacchi-ws/pacchi.ws"
		QName name = new QName("http://tt.elsagdatamat.com/trackdbws/trackdbrwservice", "TrackDBRWService");
		TrackDBRWServicePortType twsp = new TrackDBRWService(url, name).getTrackDBRWServicePort();
		//tracesReader.getToken(twsp);
		//((ToTTMessageBuilder)tracesReader).getToken().setWsToken("0,-1");
		TracesList traces = tracesReader.getTraces(twsp, new Date());
		if(traces!=null && traces.getTraces()!=null && !traces.getTraces().isEmpty())
			messageBuilder.create(traces);
	}

	public static void main( String[] args ) throws Exception
	{
		new ClassPathXmlApplicationContext("classpath:/applicationContext-test.xml" ,
				"classpath:/applicationContext-dao.xml",
				"classpath:/applicationContext-core.xml");
	}
}
