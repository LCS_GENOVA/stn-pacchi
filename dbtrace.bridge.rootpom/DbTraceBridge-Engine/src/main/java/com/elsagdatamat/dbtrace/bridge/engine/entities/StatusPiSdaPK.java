package com.elsagdatamat.dbtrace.bridge.engine.entities;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;


/** 
* R.L. BeanGenerator - 22.05.2012 03:10:16
*/ 
@Embeddable
public class StatusPiSdaPK implements Serializable {

	private static final long serialVersionUID = 2991649696276408497L;

	@Column(name = "STATUS_PI", nullable =false)
	private String statusPI;

	@Column(name = "PI_CHANNEL", nullable =false)
	private String piChannel;

	public String getStatusPI() {
		return statusPI;
	}

	public void setStatusPI(String statusPI) {
		this.statusPI = statusPI;
	}

	public String getPiChannel() {
		return piChannel;
	}

	public void setPiChannel(String piChannel) {
		this.piChannel = piChannel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((piChannel == null) ? 0 : piChannel.hashCode());
		result = prime * result
				+ ((statusPI == null) ? 0 : statusPI.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatusPiSdaPK other = (StatusPiSdaPK) obj;
		if (piChannel == null) {
			if (other.piChannel != null)
				return false;
		} else if (!piChannel.equals(other.piChannel))
			return false;
		if (statusPI == null) {
			if (other.statusPI != null)
				return false;
		} else if (!statusPI.equals(other.statusPI))
			return false;
		return true;
	}
	
}